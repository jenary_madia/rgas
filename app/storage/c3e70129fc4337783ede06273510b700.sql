INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10182, 1, 'out_of_office', 'edit', 0, 0);
INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10183, 1, 'out_of_office', 'delete', 0, 0);
INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10184, 1, 'out_of_office', 'systemmessage', 0, 0);
INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10185, 1, 'user', 'out_of_office', 0, 0);
INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10186, 1, 'out_of_office', 'action', 0, 0);
INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10187, 1, 'user', 'files', 0, 0);
INSERT INTO `tbl_cd_actions` (`cac_id`, `cac_module`, `cac_controller`, `cac_action`, `cac_type`, `cac_order`) VALUES (10188, 1, 'user', 'comments', 0, 0);

INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10182);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10183);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10184);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10185);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10186);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10187);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10103, 10188);

INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10182);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10183);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10184);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10185);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10186);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10187);
INSERT INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (10104, 10188);

ALTER TABLE `tbl_qstt_emails` ADD COLUMN `qem_cc_original_approver` TINYINT(1) NOT NULL DEFAULT '0' AFTER `qem_bcc`;