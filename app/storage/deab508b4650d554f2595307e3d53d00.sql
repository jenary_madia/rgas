#Service Procedure 
#Description 	: To add and activate page with existing group on CD.
#Creator/Date 	: KBS/10-29-2015
#++++++++++++++++++++++++++++++

DROP PROCEDURE IF EXISTS `add_new_page`;
DELIMITER //
CREATE PROCEDURE `add_new_page`(IN `userGroup` TEXT, IN `newPageName` TEXT, IN `newPageModule` INT, IN `newPageGroup` TEXT, IN `newPageType` INT)
BEGIN
	DECLARE cd_cagID,cd_cac_cnt,cd_cacID,newCacID,maxCacID INT;
	
	SELECT a.cag_id INTO cd_cagID 
	FROM tbl_cd_action_groups a 
	WHERE a.cag_group_name = userGroup;
	# cd_cagID = 10103
	
	SELECT count(a.cac_id),a.cac_id INTO cd_cac_cnt, cd_cacID 
	FROM tbl_cd_actions a 
	WHERE a.cac_action = newPageName 
		AND a.cac_controller = newPageGroup
		AND a.cac_type = newPageType;

	IF cd_cac_cnt = 0 THEN
		SELECT max(a.cac_id) + 1 INTO cd_cacID
		FROM tbl_cd_actions a
		WHERE a.cac_module = newPageModule;
		
		INSERT INTO tbl_cd_actions (`cac_id`,`cac_module`, `cac_controller`, `cac_action`, `cac_type`) VALUES (cd_cacID,newPageModule,newPageGroup, newPageName, newPageType);
		#SET cd_cacID = LAST_INSERT_ID();	
	END IF;
	
	REPLACE INTO tbl_cd_action_groups_actions(cat_cag_id,cat_cac_id) values (cd_cagID, cd_cacID);
	
END//
DELIMITER ;

#Execution
#Format: CALL add_new_page(userGroup,newPageName,newPageModule,newPageGroup,newPageType);
#CALL add_new_page('CD Admin Group','announcement',1,'admin',1);

CALL add_new_page('CD Super User Group','edit',1,'out_of_office',1);
CALL add_new_page('CD Super User Group','delete',1,'out_of_office',1);
CALL add_new_page('CD Super User Group','systemmessage',1,'out_of_office',1);
CALL add_new_page('CD Super User Group','out_of_office',1,'user',1);
CALL add_new_page('CD Super User Group','action',1,'out_of_office',1);
CALL add_new_page('CD Super User Group','files',1,'user',1);
CALL add_new_page('CD Super User Group','comments',1,'user',1);

CALL add_new_page('CD Admin Group','edit',1,'out_of_office',1);
CALL add_new_page('CD Admin Group','delete',1,'out_of_office',1);
CALL add_new_page('CD Admin Group','systemmessage',1,'out_of_office',1);
CALL add_new_page('CD Admin Group','out_of_office',1,'user',1);
CALL add_new_page('CD Admin Group','action',1,'out_of_office',1);
CALL add_new_page('CD Admin Group','files',1,'user',1);
CALL add_new_page('CD Admin Group','comments',1,'user',1);

#Remove After
DROP PROCEDURE IF EXISTS `add_new_page`;

#ALTER TABLE `tbl_qstt_emails` ADD COLUMN `qem_cc_original_approver` TINYINT(1) NOT NULL DEFAULT '0' AFTER `qem_bcc`;
