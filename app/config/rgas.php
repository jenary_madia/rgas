<?php

return array(
	
	'rgas_storage_path' => storage_path() . "/rgas/",

	'rgas_temp_storage_path' => storage_path() . "/rgas/tmp_files_path/",
	
	'rgas_submitted_leaves_notifications_path' => storage_path() . "/submitted_leave_notification/",
	
	'module_id_te' => 13,
	
	'module_id_cbr' => 14,
);
