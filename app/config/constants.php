<?php

define('PRODUCTION_ENCODERS_FILER',json_encode(array('inigo.nieves')));
define('PRODUCTION_ENCODERS_SUPERIOR','lyza.benavidez');
define('PRODUCTION_ENCODERS_HEAD','helga.tanap');
define('DELETABLE',json_encode(array("NEW","DISAPPROVED","APPROVED","RETURNED","CANCELLED")));
define('EDITABLE', json_encode(array("NEW","DISAPPROVED","RETURNED")));
define('CORPORATE',json_encode(['RBC-CORP']));
define('MFC_ETC',json_encode(array('MFC','BBF','SFI-MM')));


define('APPROVERS_CORP',json_encode(array("head","top mngt","president")));
define('LEAVE_CORP_EXECUTIVES_APPROVERS',json_encode(array('president','top mngt')));
define('LEAVE_CORP_OTHER_DEPT_APPROVERS',json_encode(array('supervisor','head')));

define('CLINIC_LEAVE_RECEIVER','CLINIC_LEAVE_RECEIVER');

define('LEAVE_APPROVERS_MFC_ETC',json_encode(array("admin-aom","plant-aom","vpo","om")));
define('LEAVE_APPROVERS_OTHER_ETC',json_encode(array("head")));
define('LEAVE_APPROVERS_OM',json_encode(array("admin-aom","plant-aom","vpo","om")));
define('EXPATRIATES',json_encode([43]));


define('MSR_DELETABLE',json_encode(array("NEW","PROCESSED","DISAPPROVED")));
define('MSR_EDITABLE', json_encode(array("NEW","DISAPPROVED")));
define('MSR_RECEIVER_CODE','PD_MSR_RECEIVER');
define('MSR_NPMD_DEPARTMENTS',json_encode(array("MKTG")));
define('MSR_SMIS_DEPARTMENTS',json_encode(array("SMIS","IQUEST")));
define('MSR_IQUEST_RECEIVER_CODE',"IQUEST_MSR_RECEIVER");

define('MOC_FORM_TITLE','PROCESS SERVICE REQUEST/MANAGEMENT OF CHANGE FORM');
define('MOC_REQ_TYPES_ENABLE',json_encode(array("review_change","process_design")));
define('MOC_DELETABLE',json_encode(array("NEW","ACKNOWLEDGED","DISAPPROVED")));
define('MOC_EDITABLE', json_encode(array("NEW","DISAPPROVED")));

define('CSMD_DEPT_ID', 9); //NOTE please change this upon deployment
define('SSMD_DEPT_ID', 60); //NOTE please change this upon deployment

define('PMARF_ALLOWED_DEPT',json_encode(array(144 , 24,149)));
define('CSMD_BSA','MOC-BSA');
define('CSMD_CORP_DIVHEAD','MOC-CSDVH');



/**MRF**/
define('HRD_DEPT_ID', 16); //NOTE please change this upon deployment
define('VPO', 'TOPMNGT_MRF_RECEIVER'); //NOTE please change this upon deployment
define('SAT_CHRD_RECEIVER', 'SAT_MRF_RECEIVER'); //NOTE please change this upon deployment
define('MRF_FORM_TITLE','MANPOWER REQUEST FORM');
define('MRF_RECRUITMENT_STAFF','MRF_RECRUITMENT_STAFF');
//define('IS_PRESIDENT', Session::get('designation') == 'PRESIDENT');
//define('IS_CORPORATE_EMPLOYEE', Session::get('company') == 'RBC-CORP');

//define('IS_FILER', Session::get('is_mrf_filer'));
//define('IS_HEAD', Session::get('desig_level') == 'head');
//define('IS_DH_ABOVE', Session::get('mrf_is_dh_above'));
//
//define('IS_CSMD_HEAD', Session::get('mrf_is_csmd_head'));
//define('IS_CSMD_ADH', Session::get('mrf_is_csmd_assistant_head'));
//define('IS_CSMD_BA', Session::get('mrf_is_csmd_business_analyst'));
//define('IS_SVP_FOR_CS', Session::get('mrf_is_csdh'));
define('IS_CHRD_HEAD', Session::get('mrf_is_chrd_head'));
//define('IS_CHRD_AS', Session::get('mrf_is_chrd_assigned_staff'));
//define('IS_CHRD_RECIPIENT', Session::get('mrf_is_chrd_recipient'));
//define('IS_SATELLITE_SS', Session::get('mrf_is_satellite_system_staff'));
//define('IS_VPO', Session::get('mrf_is_vpo'));
//define('IS_SATELLITE_HRD', Session::get('mrf_is_satellite_hrd'));
//define('IS_SATELLITE_RS', Session::get('mrf_is_satellite_recruitment_staff'));