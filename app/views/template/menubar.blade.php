
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

	<ul class="nav navbar-nav menu_bar">

		<li class="active">
			<a href="#" onfocus="this.blur()">Home</a>
		</li>
        
        <li>
			<a href="<?php echo $base_url ?>/employees/list" onfocus="this.blur()">Employees List</a>
		</li>

		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  Admin / Gen Doc <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="<?php echo $base_url ?>/lrf/leaves">Leave</a>
					<a href="<?php echo $base_url ?>/ns/notifications">Notification</a>
                    {{--FOR MSR--}}
					@if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)) || in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
						<a href="<?php echo $base_url ?>/msr">MSR</a>
					@endif
                    {{--END MSR--}}

					{{--FOR MRF--}}
						<a href="<?php echo $base_url ?>/mrf">MRF</a>
					{{--END MRF--}}
					<a href="<?php echo $base_url ?>/moc">MOC</a>
					<a href="#">Travel Authorization</a>
				</li>
			</ul>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  CHRD <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">Performance Management Form</a>
				</li>
				<li>
					<a href="<?php echo $base_url ?>/mrf/list-view/my-mrf">My MRF</a>
					<a href="<?php echo $base_url ?>/mrf/list-view/for-endorsement">For Endorsement</a>
					<a href="<?php echo $base_url ?>/mrf/list-view/for-processing">For Processing</a>
				</li>
			</ul>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  CSMD <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">MOC / Process Service Request</a>
				</li>
			</ul>
		</li>
        
        <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  CITD <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">IT Service Request</a>
				</li>
			</ul>
		</li>



		
	</ul>
	
	<ul class="nav navbar-nav navbar-right menu_bar">
		<li>
			<a href="<?php echo $base_url ?>/sign-out">Log out</a>
		</li>
	</ul>

</div>