<!doctype html>
<html>
	<head>
		<title>{{-- Rebisco General Approval System --}}</title>
		
    @section('head')
		
		<!-- General Style -->
		{{ Minify::stylesheet(
			array('/assets/css/bootstrap.css'
				 ,'/assets/css/metro-bootstrap.css'
			     ,'/assets/css/main.css'
			     ,'/assets/css/employee.css'
                 ,'/assets/css/validation_engine.css'
                 ,'/assets/css/datepicker.css'
                 ,'/assets/css/jquery-ui-1.10.4.custom.min.css'
                 ,'/assets/css/jquery.dataTables_themeroller.css'
                 ,'/assets/css/chosen.min.css'
                 ,'/assets/css/bootstrap-timepicker.min.css'
                 ,'/assets/css/bootstrap-multiselect.css'
                 ,'/assets/css/new_style.css'
			))->withFullUrl()
		}}
		
		<base href="<?php echo $base_url; ?>" />
		
    @show
	<link href="<?php echo $base_url; ?>/assets/css/font-awesome/css/font-awesome.css" rel="stylesheet">
  	{{--<link href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />--}}
	</head>
    <body>
		
		<div class="company_logo">
			<!-- <img src="<?php echo $base_url;?>/assets/img/rebisco_logo.png" width="72" height="71" /> -->
		</div>
		
		<div class="system_logo">
			{{-- <h4>Rebisco General Approval System</h4> --}}
		</div>
		
		<?php if( Session::has("employee_id") ): ?>
		
			<div class="login_info">
				Welcome <?php echo ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )); ?>				
				<div class="clear"></div>

				<?php echo Session::get("company_name"); ?>
			</div>
			<div class="clear"></div>
			
			@include('template/menubar')
			
			<hr class="header_hr" />
	
		<?php else: ?>
		
			<div class="clear"></div>
		
			<hr class="header_hr" />
            
		<?php endif; ?>
		
		<div id="wrap">
			@if(Session::get("logged_in"))
			@include('template/sidebar')
			@endif

			
			<div class="container">
				@if (Session::has('message'))
			    <div class="alert alert-info">
			        <p>{{ Session::get('message') }}</p>
			    </div>
			    @endif
				@if (Session::has('successMessage'))
			    <div class="alert alert-success">
			        <p>{{ Session::get('successMessage') }}</p>
			    </div>
			    @endif
				@if (Session::has('errorMessage'))
			    <div class="alert alert-danger">
			        <p>{{ Session::get('errorMessage') }}</p>
			    </div>
			    @endif
				@if ($errors->any())
				<div id="global_message" class="alert alert-danger">
				<ul>
				    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
				</ul>
				</div>
				@endif
				@if(@count($messages) > 0)
				<div id="global_message" class="alert alert-danger">
					<ul>
					@if (isset($messages))
						@foreach ($messages as $message)
							<li>{{ $message }}</li>
						@endforeach
					@endif
					</ul>
				</div>
				@endif
				@yield('content')
				<div class="clear_20"></div>
			</div><!-- container -->
			
			<div class="clear_60"></div>
		</div><!-- wrap -->
		<div class="clear_20"></div>
        
		<div id="footer">
		
			Copyright &copy; REBISCO 2014. All Rights Reserved.
			<br />
			System Provided by Corporate Information Technology Department 
			
		</div>
 
	
	
    @section('js')

		{{ HTML::script('/assets/js/jquery-1.10.1.min.js') }}
		{{ HTML::script('/assets/js/jquery-migrate-1.2.1.min.js') }}
		{{ HTML::script('/assets/js/bootstrap.js') }}
		{{ HTML::script('/assets/js/jquery.mockjax.js') }}
		{{ HTML::script('/assets/js/bootstrap-typeahead.js') }}
		{{ HTML::script('/assets/js/bootstrap-datepicker.js') }}
		{{ HTML::script('/assets/js/bootstrap-timepicker.min.js') }}
		{{ HTML::script('/assets/js/bootstrap-multiselect.js') }}
		{{ HTML::script('/assets/js/chosen.js') }}

		{{ HTML::script('/assets/js/jquery.dataTables.min.js') }}
		{{ HTML::script('/assets/js/datatables.fnReloadAjax.js') }}
		{{ HTML::script('/assets/js/login.js') }}
		{{ HTML::script('/assets/js/employee.js') }}
		{{ HTML::script('/assets/js/jquery-ui-1.10.4.custom.js') }}
		{{ HTML::script('/assets/js/requests_new.js') }}
		{{ HTML::script('/assets/js/requests_my.js') }}
		{{ HTML::script('/assets/js/requests_in_process.js') }}
		{{ HTML::script('/assets/js/requests_acknowledged.js') }}
		{{ HTML::script('/assets/js/endorsements_new.js') }}
		{{ HTML::script('/assets/js/endorsements_for_approval.js') }}
		{{ HTML::script('/assets/js/endorsements_my.js') }}
		{{ HTML::script('/assets/js/endorsements_chrd_head_approval.js') }}
		{{ HTML::script('/assets/js/endorsements_chrd_vp_approval.js') }}
		{{ HTML::script('/assets/js/endorsements_svp_for_cs_approval.js') }}
		{{ HTML::script('/assets/js/endorsements_pres_approval.js') }}
		{{ HTML::script('/assets/js/common.js') }}
		<!-- General JScript -->



		<!-- Script for jquery fileuploader -->
		{{ HTML::script('/assets/js/fileupload/jquery.ui.widget.js') }}
		{{ HTML::script('/assets/js/fileupload/jquery.iframe-transport.js') }}
		{{ HTML::script('/assets/js/fileupload/jquery.fileupload.js') }}

		{{ HTML::script('/assets/js/jquery.validationEngine.js') }}
		{{ HTML::script('/assets/js/jquery.validationEngine-en.js') }}
		{{ HTML::script('/assets/js/text-area-autosize.js') }}

    @show
@yield('js_ko')
</body>
</html>
