<div class="nav_tab_container metro leftfloat">
	<nav class="sidebar light">
		<ul>
			<li>
				<a href="javascript::void(0)" id="create_overtime" onfocus="this.blur();">Create Overtime</a>
			</li>

			<li>
				<a href="javascript::void(0)" id="create_taf" onfocus="this.blur();">Create TAF</a>
			</li>

			<?php if(Session::get("is_taf_receiver")): ?>
			<li>
				<a href="#"></i>Submitted TAF</a>
			</li>
			<?php endif ?>
			<?php if(Session::get("is_svp_for_cs")): ?>
			<li>
				<a href="javascript::void(0)" id="create_itr" onfocus="this.blur();">Create ITR</a>
			</li>
			<?php endif ?>

			<?php if (Session::get("is_itr_bsa") ): ?>

			<li>
				<a href="javascript::void(0)" id="assess_itr" onfocus="this.blur();">Submitted ITR</a>
			</li>

			<?php endif; ?>

			<?php if (Session::get("is_system_satellite") ): ?>

			<li>
				<a href="javascript::void(0)" id="assess_itr_satellite" onfocus="this.blur();">Submitted ITR</a>
			</li>
			<?php endif; ?>


			<?php if (Session::get("is_citd_employee")): ?>

			<li>
				<a href="javascript::void(0)" id="custom_itr" onfocus="this.blur();">Create Custom ITR</a>
			</li>

			<?php endif; ?>

			<li>
				<a href="javascript::void(0)" id="create_sap_access" onfocus="this.blur();">Create SAP Access Request</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/cbr/create" onfocus="this.blur()">Create CBR</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/cbr/submitted-cbr-requests" onfocus="this.blur()">Submitted CBR Request</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/pmarf/create" onfocus="this.blur()">Create PMARF</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/submitted-pmarf" onfocus="this.blur()">Submitted PMARF</a>
			</li>

			<?php if(Session::get("is_head")): ?>
			<li>
				<a href="<?php echo $base_url ?>/te/create" onfocus="this.blur()">Create TE</a>
			</li>
			<?php endif; ?>

			<li>
				<a href="<?php echo $base_url ?>/te/submitted-te-requests" onfocus="this.blur()">Submitted TE</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/pmf/create" onfocus="this.blur()">Create PMF</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/cc/create" onfocus="this.blur()">Create Clearance Certification</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/lrf/create" onfocus="this.blur()">Create Leave</a>
			</li>

			<li>
				<a href="<?php echo $base_url ?>/ns/create" onfocus="this.blur()">Create Notification</a>
			</li>
			@if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)) || in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
				<li>
					<a href="<?php echo $base_url ?>/msr/create" onfocus="this.blur()">Create MSR</a>
				</li>
			@endif
			@if( in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
				<li>
					<a href="<?php echo $base_url ?>/lrf/pe_create" onfocus="this.blur()">Create Leave (Production Encoder)</a>
				</li>

				<li>
					<a href="<?php echo $base_url ?>/ns/pe_create" onfocus="this.blur()">Create Notification (Production Encoder)</a>
				</li>
			@endif
			@if(Session::get("is_lrf_receiver") || Session::get("is_notif_receiver"))
				<li>
					<a href="<?php echo $base_url ?>/submitted/leaves_notifications" onfocus="this.blur()">Submitted Applications</a>
				</li>

				<li>
					<a href="<?php echo $base_url ?>/submitted/pe_leaves_notifications" onfocus="this.blur()">Submitted Applications (Production Encoders)</a>
				</li>
			@endif
			<li>
				<a href="<?php echo $base_url ?>/moc/create" onfocus="this.blur()">Create PSR/MOC</a>
			</li>
			@if(Session::get("is_msr_receiver"))
				<li>
					<a href="<?php echo $base_url ?>/msr/submitted_msr" onfocus="this.blur()">Submitted MSR (NPMD)</a>
				</li>
				<li>
					<a href="<?php echo $base_url ?>/msr/smis/submitted_msr" onfocus="this.blur()">Submitted MSR (SMIS)</a>
				</li>
			@endif
			@if((in_array(Session::get('dept_id'),[CSMD_DEPT_ID,SSMD_DEPT_ID]) && Session::get('desig_level') != 'employee') || Session::get('is_bsa'))
				<li>
					<a href="<?php echo $base_url ?>/moc/submitted-moc" onfocus="this.blur()">Submitted PSR/MOC</a>
				</li>
			@endif
			{{--MRF--}}
			{{--			@if(Session::get("company") != "RBC-CORP" && Session::get("desig_level") == "head")--}}
			<li>
				<a href="<?php echo $base_url ?>/mrf/create" onfocus="this.blur()">Create MRF</a>
			</li>
			{{--@endif--}}
			<li>
				<a href="<?php echo $base_url ?>/mrf/list-view/submitted" onfocus="this.blur()">Submitted MRF</a>
			</li>
			{{--MRF--}}
		</ul>
	</nav>
</div>