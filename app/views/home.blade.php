@extends('template/header')

@section('content')
	<div id="wrap">
        <div class="clear_20"></div>
        <div class="wrapper">
            <h2>Pending</h2>
            <hr>
            {{--MRF--}}
                <div class="clear_20"></div>
                <div class="wrapper">
                    {{ Form::open(array('url' => 'mrf/delete-mrf', 'method' => 'post', 'files' => true,'id' => 'formMyMRF')) }}
                    <table id="myMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMRF">
                        <thead>
                        <tr role="row">
                            <th class="" style="text-align : center" role="columnheader" rowspan="1" colspan="5">My MRF</th>
                        </tr>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                    {{ Form::close() }}
                </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <table id="forEndorsementMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forEndorsementMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ENDORSEMENT</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="wrapper">
                <table id="forProcessingMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forProcessingMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR PROCESSING</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
            {{--END MRF--}}
            @if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)))
                {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMSR')) }}
                <table id="myMSR" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMSR">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">My MSR</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason of Request</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                {{ Form::close() }}
            @elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
                {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMSR')) }}
                <table id="myMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMSR-SMIS">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1">My MSR</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project Type/Name</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                {{ Form::close() }}
            @endif
            {{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formMyleaves')) }}
            <table id="myLeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myLeaves">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">My Leaves</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Leave</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Leave</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            {{ Form::close() }}
            <div class="clear_20"></div>
            @if(in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
                <table id="myPELeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myPELeaves">
                    <thead>
                    <tr role="row">
                        <th class="" role="columnheader" rowspan="1" colspan="5">My Production Encoders Leave Requests</th>
                    </tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                <div class="clear_20"></div>
            @endif
            {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMOC')) }}
            <table id="myMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMOC">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : center" role="columnheader" rowspan="1" colspan="5">My PSR/MOC</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            {{ Form::close() }}
            <h2>Waiting for your approval</h2>
            <hr>
            @if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)))
                <table id="superiorMSR" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorMSR">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR APPROVAL MSR</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason of Request</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            @elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
                <table id="superiorMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorMSR-SMIS">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1">FOR APPROVAL</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project Type/Name</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            @endif
            <table id="forApproveLeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApproveLeaves">
                <thead>
                <tr role="row">
                    <th class="" role="columnheader" rowspan="1">For Approval Leave Request</th>
                </tr>
                <tr role="row">
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Leave</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Leave</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            <div class="clear_20"></div>

            @if(! in_array(Session::get("company"),json_decode(CORPORATE,true)))
                <table id="superiorPELeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorPELeaves">
                    <thead>
                    <tr role="row">
                        <th class="" role="columnheader" rowspan="1" colspan="5">Production Encoders For Approval Leave Requests</th>
                    </tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                <div class="clear_20"></div>
            @endif
            {{--MOC--}}
            <table id="forEndorsementMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forEndorsementMOC">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ENDORSEMENT</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            @if(Session::get('is_div_head') || in_array(Session::get('desig_level'),['admin-aom','plant-aom']))
                <div class="clear_20"></div>
                <div class="wrapper">
                    <table id="forApprovalMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApprovalMOC">
                        <thead>
                        <tr role="row">
                            <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR APPROVAL</th>
                        </tr>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            @endif
            {{--end MOC--}}
            <hr />
        </div>
	</div>
@stop
@section('js_ko')
	{{ HTML::script('/assets/js/leave_notif.js') }}
    {{ HTML::script('/assets/js/msr/home-datatables.js') }}
    {{ HTML::script('/assets/js/moc/home-datatables.js') }}
    {{ HTML::script('/assets/js/mrf/home-datatables.js') }}
@stop
