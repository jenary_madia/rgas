@extends('template/header')

@section('content')
<div id="wrap">
 
    <div class="wrapper">

        <form class="form-inline" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
        
                <label class="form_title">AUDIT TRAIL REPORT</label>
                           
                    <div class="row">   
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">FROM:</label>
                        </div>
                        <div class="col2_form_container">
                            <input  type="text" class="datepicker form-control" id="date_from" name="date_from" value="{{ date('m/d/Y') }}" />
                        </div>
                    </div>
                            
                     <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">TO:</label>
                        </div>
                        <div class="col2_form_container">
                            <input  type="text" class="datepicker form-control" id="date_to" name="date_to" value="{{ date('m/d/Y') }}" />
                        </div>
                    </div>
                        
                    <div class="clear_10"></div>    
                    
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">MODULE:</label>
                        </div>
                        <div class="col2_form_container">
                                  <select class="form-control" id="module" name="module" multiple="multiple">
                                        @foreach($module as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                    </select>
                        </div>
                    </div>
                            
                     <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <select class="employee form-control" id="employee" name="employee" multiple="multiple">
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                            </select>
                        </div>
                    </div>
                        
                    <div class="clear_10"></div>
                    
                    <div class="row_form_container">
                        <div class="col1_form_container">
                           <button type="button" id="search" class="btn btnsearch btn-default">SEARCH</button>
                        </div>
                    </div>
                    
                    <div class="clear_20"></div>    
                    
                    <div class="row_form_container">
                        <div class="col3_form_container">
                            <label class="labels text-warning">Report Date: {{ date('m/d/Y h:i a') }}</label>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>    
                    
                            <div class="datatable_holder">
                            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts" style="table-layout: fixed;">
                                    <thead>
                         
                                            <tr role="row">
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">TIME</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EMPLOYEE NAME</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">MODULE</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >ACTION</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >TABLE NAME</th>
                                                    <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >PARAMETERS</th>
                                                    <th style="text-align:left;word-wrap: break-word" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >OLD VALUE</th>
                                                    <th style="text-align:left;word-wrap: break-word" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >NEW VALUE</th>
                                            </tr>							
                                    </thead>
                            </table>
                                
                                
                            </div>
                    
                    <div class="clear_10"></div>
                    
                        <div class="text-center">
                           <button type="button" id="generate" class="btn btnsearch btn-default">Generate Report</button>
                           <button type="button" onClick="history.go(-1)" class="btn btnsearch btn-default">Back</button>
                        </div>

                    </div>

                     <div class="clear_20"></div>
        </form>

</div>
</div>


@stop

@section('js_ko')
    <link href="{{ url('/assets/css/multiselect/bootstrap-multiselect.css') }}" rel="stylesheet" type="text/css" />
    {{ HTML::script('/assets/js/multiselect/bootstrap-multiselect.js') }}
    <link href="{{ url('/assets/css/chosen.min.css') }}" rel="stylesheet" type="text/css" />
    {{ HTML::script('/assets/js/chosen.jquery.min.js') }}
    {{ HTML::script('/assets/js/audit/index.js') }}
@stop