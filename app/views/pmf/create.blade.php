@extends('template/header')

@section('content')
        <form class="form-inline" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_title ">PERFORMANCE MANAGEMENT FORM</label>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ Session::get('employeeid') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ $reference_number }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
						<div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="employee_name" name="employee_name" value="{{ Session::get('employee_name') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Created By:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control placeholders" name="pm_created_by"/>
                            </div>
                        </div>
						<div class="clear_10"></div>
						<div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Position Title:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="pm_emp_position" name="pm_emp_position" value="{{ Session::get('') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control placeholders" value="{{ date('Y-m-d') }}" placeholder="yyyy-mm-dd" name="cb_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('dept_name') }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Covered:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control date_picker" name="date_covered"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Section:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('sect_name') }}" name="sect_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="New" name="cb_status"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                    </div>
                    <div class="clear_20"></div>
                    <label class="form_label pm_form_title">PERFORMANCE OBJECTIVES</label>
                    <div class="clear_10"></div>
					<div class="row tbl_pm_questions border">
						<div class="col-md-2 pm_text_header">
							Key Result Areas
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							KPI
							<div class="row ">
								<div class="col-md-4 borderright bordertop">
									General Measure
								</div>
								<div class="col-md-4 borderright bordertop">
									Specific Measure
								</div>
								<div class="col-md-4 bordertop" style="height: 30px">
									Attachment
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							Actual Performance (Target's Achieved)
							<div class="row">
								<div class="col-md-3 borderright bordertop"  style="height: 30px">
									Yes
								</div>
								<div class="col-md-3 borderright bordertop"  style="height: 30px">
									No
								</div>
								<div class="col-md-6 bordertop">
									Other Comments (For Exceeds of Below Target)
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							Weight (W)
						</div>
						<div class="col-md-2 borderright pm_text_header">
							<div class="borderbottom ">Rating</div>
							<div class="rows borderright">
								<div class="col-md-6 borderright" style="height: 30px">
									Self
								</div>
								<div class="col-md-6" style="height: 30px">
									Superior(R)
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							Weightened Rating (W*R)
						</div>
					</div>
					
					<div class="row tbl_pm_questions borderbottom borderleft borderright">
						<div class="col-md-2 pm_tbl_labels">
							Critical KRA's<br />
							<button type="buttton" name="expand" class=""><i class="fa fa-plus-circle fa-xs faplus"></i></button>
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							<div class="row ">
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 " style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							<div class="row">
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							<input type="textbox" class="borderright form-control" name="">
						</div>
						<div class="col-md-2  borderright pm_text_header">
							<div class="rows">
								<div class="col-md-6 borderright" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							<input type="textbox" class="borderright form-control" name="">
						</div>
					</div>
					
					<div class="row tbl_pm_questions borderbottom borderleft borderright">
						<div class="col-md-2 pm_tbl_labels">
							KRA Cluster 1:<br />
							Objectives that directly impact business expansion<br />
							<button type="buttton" name="expand" class="faplus"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							<div class="row ">
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 " style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							<div class="row">
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							<input type="textbox" class="borderright form-control" name="">
						</div>
						<div class="col-md-2  borderright pm_text_header">
							<div class="rows">
								<div class="col-md-6 borderright" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							<input type="textbox" class="borderright form-control" name="">
						</div>
					</div>
					
					<div class="row tbl_pm_questions borderbottom borderleft borderright">
						<div class="col-md-2 pm_text_header">
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							<div class="row ">
								<div class="col-md-4 borderright ">
									<input type="checkbox" class="borderright" name="" value="">
									<label class="" style="font-size: 8px">Quality (QL)</label>
									<input type="checkbox" class="borderright" name="" value="">
									<label class="" style="font-size: 8px">Costefficient (CE)</label>
									<input type="checkbox" class="borderright" name="" value="">
									<label class="" style="font-size: 8px">Quantity (QL)</label>
									<input type="checkbox" class="borderright" name="" value="">
									<label class="" style="font-size: 8px">Timelines (T)</label>
								</div>
								
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 " style="height: 30px">
									<button type="button" class="btn btn-xs btnbrowse btn-default" id="btnAddAttachment">Browse</button><br/>
									<div id="attachments"></div>
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							<div class="row">
								<div class="col-md-3 borderright">
									<input type="radio" class="borderright" name="">
								</div>
								<div class="col-md-3 borderright">
									<input type="radio" class="borderright" name="">
								</div>
								<div class="col-md-6">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							<input type="textbox" class="borderright form-control" name="">
						</div>
						<div class="col-md-2  borderright pm_text_header">
							<div class="rows">
								<div class="col-md-6 borderright" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							<input type="textbox" class="borderright form-control" name="">
						</div>
					</div>
					
					<div class="row tbl_pm_questions borderbottom borderleft borderright">
						<div class="col-md-2 pm_tbl_labels">
							KRA Cluster 2:<br />
							Objectives that directly improve productivity & efficiency<br />
							<button type="buttton" name="expand" class=""><i class="fa fa-plus-circle fa-xs faplus"></i></button>
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							<div class="row ">
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 " style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							<div class="row">
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							<input type="textbox" class="borderright form-control" name="">
						</div>
						<div class="col-md-2  borderright pm_text_header">
							<div class="rows">
								<div class="col-md-6 borderright" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							<input type="textbox" class="borderright form-control" name="">
						</div>
					</div>
					
					<div class="row tbl_pm_questions borderbottom borderleft borderright">
						<div class="col-md-2 pm_tbl_labels">
							KRA Cluster 3:<br />
							Objectives that directly support & develop people thru training  & culture development <br />
							<button type="buttton" name="expand" class=""><i class="fa fa-plus-circle fa-xs faplus"></i></button>
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							<div class="row ">
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 " style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							<div class="row">
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							<input type="textbox" class="borderright form-control" name="">
						</div>
						<div class="col-md-2  borderright pm_text_header">
							<div class="rows">
								<div class="col-md-6 borderright" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							<input type="textbox" class="borderright form-control" name="">
						</div>
					</div>
					
					<div class="row tbl_pm_questions borderbottom borderleft borderright">
						<div class="col-md-2 pm_tbl_labels">
							KRA Cluster 4:<br />
							Objectives that support  business expansion, productivity  & efficiency improvement , and people  support/development<br />
							<button type="buttton" name="expand" class=""><i class="fa fa-plus-circle fa-xs faplus"></i></button>
						</div>
						<div class="col-md-3 borderleft borderright pm_text_header">
							<div class="row ">
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 borderright ">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-4 " style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-3 borderright pm_text_header">
							<div class="row">
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-3 borderright"  style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 borderright pm_text_header" style="height:44px">
							<input type="textbox" class="borderright form-control" name="">
						</div>
						<div class="col-md-2  borderright pm_text_header">
							<div class="rows">
								<div class="col-md-6 borderright" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
								<div class="col-md-6" style="height: 30px">
									<input type="textbox" class="borderright form-control" name="">
								</div>
							</div>
						</div>
						<div class="col-md-1 pm_text_header">
							<input type="textbox" class="borderright form-control" name="">
						</div>
					</div>
					<div class="row tbl_pm_questions border th_style th_left">
						<p class="total_weigh">Total Weigh: <input type="textbox" class=""></p> 
						<p class="overall_rate_po">Overall Rating for Performance Objectives: <input type="textbox" class=""></p> 
					</div>
					
					<div class="clear_20"></div>	
                    <label class="form_label pm_form_title">PERFORMANCE BEHAVIORS</label>
					<div class="pcv th_style th_left" style="">Practice of Core Values</div>
					<div class="pcv th_style th_left" style="">
						<i>Rating Scale:</i><br/>
						<p class="p_rate">1 - Almost Never; 2 - Seldom; 3 - Sometimes; 4 - Often; 5 - Always</p>
					</div>
					<table border = "1" cellpadding = "0" class="tbl_pm_questions">
                        <th class="th_style th_left">Value</th>
                        <th class="th_style ">Key Actions or Behaviors</th>
                        <th class="th_style">Critical Incidents (Optional)</th>
                        <th class="th_style">Self Rating</th>
                        <th class="th_style">Superior's Rating</th>
                        <tr>
                            <td class="td_style">Excellence</td>
                            <td class="">
								<div class="td_pm_style border_bottom">Submit accurate and complete reports before the deadline</div>
								<div class="td_pm_style border_bottom">Anticipates possible consequences of action and makes contingency plans</div>
								<div class="td_pm_style border_bottom">Knows how to prioritize when faced with several challenges at the same time</div>
								<div class="td_pm_style ">Sets high but achievable goals and targets for oneself</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox"/></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style "><input type="textbox" id="" class="pm_txtbox" /></div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
                        </tr>
						<tr>
                            <td class="td_style">Innovation</td>
                            <td class="">
								<div class="td_pm_style border_bottom">Recommends new ideas for better work output</div>
								<div class="td_pm_style border_bottom">Simplifies tasks and and finds improved ways of doing things</div>
								<div class="td_pm_style border_bottom">Takes risks in the accomplishments of difficult tasks/goals</div>
								<div class="td_pm_style ">Adapts quickly to the improved policies and procedures of the company</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox"/></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style "><input type="textbox" id="" class="pm_txtbox" /></div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
                        </tr>
						<tr>
                            <td class="td_style">Customer Satisfaction</td>
                            <td class="">
								<div class="td_pm_style border_bottom">Prompt in attending to customer's concerns or needs</div>
								<div class="td_pm_style border_bottom">Answers questions of customers respectfully and appropriately (i.e right tone of voice,appropriate facial reactions,etc.)</div>
								<div class="td_pm_style border_bottom">Coordinates effectively and maintains harmonious relationship with internal and external customers.</div>
								<div class="td_pm_style ">Offers a helping hand to customers even without being asked to</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox"/></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style "><input type="textbox" id="" class="pm_txtbox" /></div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
                        </tr>
						<tr>
                            <td class="td_style">Commitment</td>
                            <td class="">
								<div class="td_pm_style border_bottom">Willingly participate to company endeavors (i.e Corporate Events,Promotion of Products,etc.)</div>
								<div class="td_pm_style border_bottom">Protects the image of the company whether inside of or outside the company premises</div>
								<div class="td_pm_style border_bottom">Engages in change-related initiatives of the company and remians optimistic about change even in the face of adversity</div>
								<div class="td_pm_style ">Performs additional functions willingly and with a sense of responsibility (e.g  stays late in the office when need arises,goes to work on weekends,etc.)</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox"/></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style "><input type="textbox" id="" class="pm_txtbox" /></div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
                        </tr>
						<tr>
                            <td class="td_style">Teamwork</td>
                            <td class="">
								<div class="td_pm_style border_bottom">Respects and listens to the viewpoint of team members</div>
								<div class="td_pm_style border_bottom">Encourages team members to do their best</div>
								<div class="td_pm_style border_bottom">Participates actively in the achievement of goals and objectives of the team and other departments</div>
								<div class="td_pm_style ">Imparts knowledge to team members to help them succeed</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox"/></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style border_bottom"><input type="textbox" id="" class="pm_txtbox" /></div>
								<div class="td_pm_style "><input type="textbox" id="" class="pm_txtbox" /></div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
							<td class="">
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style border_bottom">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
								<div class="td_pm_style">
									<select class="pm_dd" name="">
										<option value=""></option>
									</select>
								</div>
							</td>
                        </tr>						
                    </table>
					<div class="pcv_footer th_style th_left">
						<p class="pcv_footnote">TOTAL SCORE FOR PRACTICE OF CORE VALUES: <input type="textbox" class=""></p> 
						<p class="pcv_rating">RATING: <input type="textbox" class=""></p> 
					</div>
					
					<div class="clear_20"></div>
					<table border = "1" cellpadding = "0" class="tbl_pm_questions">
                        <th class="th_style th_left">Discipline 5%</th>
                        <th class="th_style">Weight</th>
                        <th class="th_style">Demerit</th>
                        <th class="th_style">Score</th>
                        <tr>
                            <td class="td_style">
								Corporate HRD will provide the annual summary of infractions; immediately superiors to validate and compute points for deduction. <br />
								Demerit Schedule: <br />
								1 written warning = 0.5%; 1 day to 1 work week suspension = 1%; more than 1 week suspension = 1.5%;
							</td>
							<td class="td_style"><input type="textbox" name=""></td>
							<td class="td_style"><input type="textbox" name=""></td>
							<td class="td_style"><input type="textbox" name=""></td>
						</tr>
					</table>

					<div class="clear_20"></div>
					<table border = "1" cellpadding = "0" class="tbl_pm_questions">
                        <th class="th_style th_left">Compliance to Policies and Procedures</th>
                        <th class="th_style">Weight</th>
                        <th class="th_style">Rating</th>
                        <th class="th_style">Score</th>
                        <tr>
                            <td class="td_style">
								Corporate SMDD will compute and provide the rating for this factor. This includes Financial and Non-Financial Audit and SLA.<br />
								*For employee's regularization: Repective Department Heads shall provide rating for this factor based on the employee's conformance to policies & procedures within the probationary period.
							</td>
							<td class="td_style"><input type="textbox" name=""></td>
							<td class="td_style"><input type="textbox" name=""></td>
							<td class="td_style"><input type="textbox" name=""></td>
						</tr>
					</table>
					<div class="pcv_footer th_style th_left">
						<p class="overall_rate">OVERALL RATING FOR PERFORMANCE BEHAVIORS: <input type="textbox" class=""></p> 
					</div>
                    <div class="clear_20"></div>
					<label class="form_label pm_form_title">SUMMARY OF PERFORMANCE</label>
					
					<div class="pm_form_container">
						<table border = "1" cellpadding = "0" class="">
							<th class="th_style">Component</th>
							<th class="th_style">Score</th>						
							<tr>
								<td class="td_style">PERFORMANCE OBJECTIVES(80.00%)</td>
								<td class="td_style pm_po" class=""><input type="textbox" class="pm_po" name=""></td>
							</tr>
							<tr>
								<td class="td_style">PERFORMANCE BEHAVIORS(20.00%)</td>
								<td class="td_style pm_po" class=""><input type="textbox" class="pm_po" name=""></td>
							</tr>
							<tr>
								<td class="td_style">PRACTICE OF CORE VALUES(10.00%)</td>
								<td class="td_style pm_po" class=""><input type="textbox" class="pm_po" name=""></td>
							</tr>
							<tr>
								<td class="td_style">DISCIPLINE(5.00%)</td>
								<td class="td_style pm_po" class=""><input type="textbox" class="pm_po" name=""></td>
							</tr>
							<tr>
								<td class="td_style">COMPLIANCE TO POLICIES AND PROCEDURES(5.00%)</td>
								<td class="td_style pm_po" class=""><input type="textbox" class="pm_po" name=""></td>
							</tr>
							<tr>
								<td class="td_style">PERFORMANCE OBJECTIVES(80.00%)</td>
								<td class="td_style pm_po" class=""><input type="textbox" class="pm_po" name=""></td>
							</tr>
						</table>
						<div class="clear_20"></div>
					</div>
					
					<div class="pm1_form_container">
						<table border = "1" cellpadding = "0" class="">
							<div class="border th_style rc">Rating Classifications</div>
							<tr>
								<td class="td_style text-center">A+</td>
								<td class="td_style text-center">100.01% - 120.00%</td>
							</tr>
							<tr>
								<td class="td_style text-center">A</td>
								<td class="td_style text-center">95.00% - 100.00%</td>
							</tr>
							<tr>
								<td class="td_style text-center">B+</td>
								<td class="td_style text-center">90.00% - 94.99%</td>
							</tr>
							<tr>
								<td class="td_style text-center">B</td>
								<td class="td_style text-center">85.00% - 89.99%</td>
							</tr>
							<tr>
								<td class="td_style text-center">C+</td>
								<td class="td_style text-center">80.00% - 84.99%</td>
							</tr>
							<tr>
								<td class="td_style text-center">C</td>
								<td class="td_style text-center">75.00% - 79.99%</td>
							</tr>
							<tr>
								<td class="td_style text-center">D</td>
								<td class="td_style text-center">0.00% - 74.99%</td>
							</tr>
						</table>
						<div class="clear_20"></div>
					</div>
					
	  
                    <div class="clear_20"></div>
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <button type="button" class="btn btnbrowse btn-default" id="btnAddAttachment">ADD</button><br/>
						<div id="attachments"></div>
						<!--
                        <div class="sample_file">SAMPLE 1 FILENAME</div>
                        <div class="delete_file">
                            <button type="submit" class="btn btndefault btn-default" value="">DELETE</button><br/>
                        </div>
						-->
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script type="text/javascript">
    $(function () {
		var date = new Date();
        date.setDate(date.getDate()-1);
		$(".date_picker").datepicker({ format: 'mm/dd/yyyy', startDate: date });
		
		$('.remove_file_attachment').live('click', function() {
			$(this).parent().remove();
		});
		
		$("#btnAddAttachment").click(function(){
			if($("#attachments input:file").length != 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment' type='button'>DELETE</button></div>");
			}
		});
	});
</script>

@stop

