@extends('template/header')

@section('content')
	<div class="clear_10"></div>
	<table id="requests_my_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable" width="100%" aria-describedby="requests_my_list">
		<thead>
			<tr role="row">
				<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company" style="width: 136px;">My CBR Requests</th>
			</tr>
			<tr role="row">
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Company" style="width: 136px;">Reference No.</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Employee Name" style="width: 222px;">Date Filed</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Employee Name" style="width: 222px;">Requested Documents</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Employee Name" style="width: 222px;">Status</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Actions</th>
			</tr>							
		</thead>
	</table>
	<div class="clear_20"></div> 	
@stop