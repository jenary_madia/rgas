@extends('template/header')

@section('content')
        <form class="form-inline" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="employee_name" name="employee_name" value="{{ Session::get('employee_name') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
<<<<<<< HEAD
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ $reference_number }}" />
=======
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ ReferenceNumbers::get_current_reference_number('cbr') }}" />
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ Session::get('employeeid') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control placeholders" value="{{ date('Y-m-d') }}" placeholder="yyyy-mm-dd" name="cb_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Company:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" name="comp_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="New" name="cb_status"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('dept_name') }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Contact Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" value="{{ Session::get('cb_contact_number') }}" name="cb_contact_number" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Section:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('sect_name') }}" name="sect_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Urgency:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="cb_urgency">
									<option value="Normal Priority">Normal Priority</option>
									<option value="Immediate Attention Needed">Immediate Attention Needed</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Needed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control date_picker" name="date_needed"/>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label">REQUESTED DOCUMENTS:</label>

                    <div class="checkbox_container">
                        <div class="col_checkbox">
                            <input type="checkbox" id="chkPagibig" name="requested_documents[pagibig][name]" value="Certificate of Premium Contributions – Pag-IBIG"/>
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkPagibig">
                                Certificate of Premium Contributions – Pag-IBIG (for resigned employees)<i class="itext">(This document is secured directly from Pag-IBIG office by CHRD Representative)</i>
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div class="" id="dvPagibig" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[pagibig][duration]">
                                    <option value="12 months and below">12 months and below</option>
                                    <option value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" id="chkPhilhealth" name="requested_documents[philhealth][name]" value="Certificate of Premium Contributions – Philhealth" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkPhilhealth">
                                Certificate of Premium Contributions – Philhealth
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div class="" id="dvPhilhealth" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[philhealth][duration]">
                                    <option value="Latest 6 months">Latest 6 months</option>
                                    <option value="Latest 12 months">Latest 12 months</option>
                                    <option value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>


                        <div class="col_checkbox">
                            <input type="checkbox" id="chkSss" name="requested_documents[sss][name]" value="Certificate of Premium Contributions – SSS"/>
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkSss">
                                Certificate of Premium Contributions – SSS
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div class="" id="divSss" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[sss][duration]">
                                    <option value="12 months and below">12 months and below</option>
                                    <option value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>


                        <div class="col_checkbox">
                            <input type="checkbox" id="chkCoe" name="requested_documents[coe][name]" value="Certificate of Employment" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkCoe">
                                Certificate of Employment
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divCoe" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">No. of Copies:</label>
                            </div>
                            <div class="textbox_sm">
                                <input type="text" class="form-control" name="requested_documents[coe][number_of_copies]" />
                            </div>
                        </div>

                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" id="chkCoec" name="requested_documents[coec][name]" value="Certificate of Employment with Compensation" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkCoec">
                                Certificate of Employment with Compensation
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divCoec" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">No. of Copies:</label>
                            </div>
                            <div class="textbox_sm">
                                <input type="text" class="form-control" value="" name="requested_documents[coec][number_of_copies]" />
                            </div>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[clearance_certification][name]" value="Clearance Certification" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Clearance Certification (for resigned employees) – <i class="itext">Released only upon receipt of final pay from Accounting Department</i>
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[itr][name]" value="Income Tax Return" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Income Tax Return
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[pagibig_esav][name]" value="Pag-IBIG Employee Statement of Accumulated Value (ESAV)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Pag-IBIG Employee Statement of Accumulated Value (ESAV) (for housing loan application)
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[ph_claim][name]" value="Philhealth Claim Form 1" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Philhealth Claim Form 1
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[ph_mdr][name]" value="Philhealth Member Data Record (MDR)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Philhealth Member Data Record (MDR)
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[copy_of_201_file][name]" value="Copy of 201 File" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Copy of 201 File
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" id="chkEmaf" name="requested_documents[emaf]"/>
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkEmaf">
                                Employee Movement Action Form (EMAF)
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divEmaf" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">Nature of Movement:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px">
                                    <option value="">------</option>
                                </select>
                            </div>

                            <div class="extra_labels">
                                <label class="labels">Effectivity Date:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px">
                                    <option value="">------</option>
                                </select>
                            </div>
                            <div class="clear_10"></div>
                            <div class="tbl">
                                <table border = "1" cellpadding = "0" class="tbl">
                                    <th class="th_style">DETAILS</th>
                                    <th class="th_style">PRESENT STATUS</th>
                                    <th class="th_style">PROPOSED STATUS</th>
                                    <th class="th_style">REMARKS</th>

                                    <tr>
                                        <td class="td_style">Company</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Department</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Designation</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Job Level</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Salary Rate</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear_10"></div>


                        <div class="col_checkbox">
                            <input type="checkbox" id="chkAttendance" name="requested_documents[emp_attendance][name]" value="Employee Attendance" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkAttendance">
                                Employee Attendance
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divAttendance" style="display: none">
                            <div class="extra_labels">
                                <label class="labels">From:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control date_picker" name="requested_documents[emp_attendance][from]"/>
                            </div>
                            <br/>
                            <div class="clear_10"></div>
                            <div class="extra_labels">
                                <label class="labels">To:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control date_picker" name="requested_documents[emp_attendance][to]"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input type="checkbox" name="requested_documents[others][name]" value="Others"  />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Others:  <input type="text" name="requested_documents[others][value]" />
                            </label>
                        </div>
                    </div><!-- end of checkbox_container -->

                    <div class="clear_20"></div>

                    <label class="form_label">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea rows="4" cols="50" class="form-control textarea_width" name="cb_purpose_of_request"> </textarea>
                    </div>
                    <label class="form_note">
                        <strong>(Maximum characters: 5000)</strong> Characters left: 4966
                    </label>


                    <div class="clear_20"></div>
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <button type="button" class="btn btnbrowse btn-default" id="btnCbrAddAttachment">ADD</button><br/>
						<div id="attachments"></div>						
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO CHRD FOR PROCESSING</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script type="text/javascript">
    $(function () {
		var date = new Date();
        date.setDate(date.getDate()-1);
		$(".date_picker").datepicker({ format: 'mm/dd/yyyy', startDate: date });
		
		$('.remove_file_attachment').live('click', function() {
			$(this).parent().remove();
		});
		
		$("#btnCbrAddAttachment").click(function(){
			if($("#attachments input:file").length < 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment' type='button'>DELETE</button></div>");
			}
		});
		
        $("#chkPhilhealth").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPhilhealth").show();
            } else {
                $("#dvPhilhealth").hide();
            }
        });

        $("#chkPagibig").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPagibig").show();
            } else {
                $("#dvPagibig").hide();
            }
        });

        $("#chkSss").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divSss").show();
            } else {
                $("#divSss").hide();
            }
        });

        $("#chkCoe").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoe").show();
            } else {
                $("#divCoe").hide();
            }
        });

        $("#chkCoec").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoec").show();
            } else {
                $("#divCoec").hide();
            }
        });

        $("#chkEmaf").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divEmaf").show();
            } else {
                $("#divEmaf").hide();
            }
        });

        $("#chkAttendance").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divAttendance").show();
            } else {
                $("#divAttendance").hide();
            }
        });

    });
</script>

@stop