<div class="row">
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Employee Name:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_emp_name'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Reference Number:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_ref_num'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Employee Number:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_emp_id'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Date Filed:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_date_filed'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Company:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_company'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Status:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cbrd']['cbrd_status'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Department:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_department'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Contact Number:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb_contact_no'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Section:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_section'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">Urgency:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_urgency'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="col3_form_container">
							<div class="col1_form_container">
								<label class="labels">Date Needed:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_date_needed'] }}" />
							</div>
						</div>
					</div>