@extends('template/header')

@section('content')

<form method="post" action="{{ URL::to('cbr/for-acknowledgement/'.$request['cbrd_ref_num']) }}">
	<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
	<input name="cbrd_id" type="hidden" value="{{ $request['cbrd_id'] }}"/>
    <div class="form_container">
		<label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>
			<div class="row">
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Employee Name:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_emp_name'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Reference Number:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cbrd_ref_num'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Employee Number:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_emp_id'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Date Filed:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control placeholders" value="{{ $request['cb_date_filed'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Company:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_company'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Status:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_status'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Department:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_department'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Contact Number:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_contact_no'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Section:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_section'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Urgency:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_urgency'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="col3_form_container">
					<div class="col1_form_container">
						<label class="labels">Date Needed:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_date_needed'] }}" />
					</div>
				</div>
			</div>

			<div class="clear_20"></div>

			<label class="form_label">REQUESTED DOCUMENTS:</label>

			<table border = "1" cellpadding = "0" class="tbl3">
                        <th class="th_style td_height">Document</th>
                        <th class="th_style td_height">Reference No.</th>
                        <th class="th_style td_height">Assigned To</th>
                        <th class="th_style td_height">CBR Remarks</th>
                        <th class="th_style td_height">Date Document is Endorsed to Requestor</th>

                        <tr>
                            <td class="td_style td_height">{{ json_decode($request['cbrd_req_docs'])->name }}</td>
                            <td class="td_style td_height">{{ $request['cbrd_ref_num'] }}</td>
                            <td class="td_style td_height">{{ $request['cbrd_assigned_to'] }}</td>
                            <td class="td_style td_height">{{ $request['cbrd_remarks'] }}</td>
                            <td class="td_style td_height"><input readonly="readonly" type="text" class="form-control date_picker" name="date_endorsed_to_requestor"/></td>
                        </tr>
                    </table>

			<div class="clear_20"></div>

			<label class="form_label">PURPOSE OF REQUEST:</label>
			<div class="textarea">
				<textarea readonly rows="4" cols="50" class="form-control textarea_width" placeholder="<Purpose of the request goes here>">{{ $request['cb_purpose_of_request'] }}</textarea>
			</div>

			<div class="clear_20"></div>
			<label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
			<div class="attachment_container">
				@if(count(json_decode($request['cb_attachments'])) > 0)
					@foreach(json_decode($request['cb_attachments']) as $attachment)
					<a href="{{ URL::to('/cbr/download/attachment') . '/' . $attachment->random_filename .'/' . $attachment->original_filename }}">{{ $attachment->original_filename }}</a><br />
					@endforeach
				@endif
			</div>

	</div><!-- end of form_container -->

	<div class="clear_20"></div>
	<div class="form_container">
		<div class="textarea_messages_container">
			<div class="row">
				<label class="textarea_inside_label">MESSAGE:</label>
				<textarea readonly name="purpose_request" rows="3" class="form-control textarea_inside_width" placeholder="DEBORAH METRA 08/04/2014 8:30 AM: Comment of requestor.">@if( $request['cb_comments'] != '') @foreach(json_decode($request['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }} {{ $comment->message }}@endforeach @endif</textarea>
			</div>
		</div>    
		<div class="clear_10"></div>
		<div class="textarea_messages_container">
			<div class="row">
				<label class="textarea_inside_label">COMMENT:</label>
				<textarea rows="3" class="form-control textarea_inside_width" name="comment" placeholder="<Comment goes here>"></textarea>
			</div>
		</div>

		<div class="clear_10"></div>
		<div class="row">
			<div class="clear_10"></div>
			<div class="comment_container">
				<div class="comment_notes">
					<label class="button_notes">FORWARD TO REQUESTOR FOR ACKNOWLEDGEMENT</label>
				</div> 
				<div class="">
					<button type="submit" class="btn btn-default btnacknowledge" value="">FOR ACKNOWLEDGEMENT</button>
				</div>
			</div>
		</div>
	</div><!-- end of form_container -->
    <div class="clear_60"></div>
</form>
@stop
@section('js_ko')
<script type="text/javascript">
    $(function () {
		var date = new Date();
        date.setDate(date.getDate()-1);
		$(".date_picker").datepicker({ format: 'yyyy-dd-mm', startDate: date });
	});
</script>

@stop