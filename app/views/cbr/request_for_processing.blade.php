@extends('template/header')

@section('content')    
	
	<div class="form_container">
		<label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

			<div class="row">
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Employee Name:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_emp_name'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Reference Number:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_ref_num'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Employee Number:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_emp_id'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Date Filed:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control placeholders" value="{{ $request['cb_date_filed'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Company:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_company'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Status:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_status'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Department:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_department'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Contact Number:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_contact_no'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Section:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_section'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">Urgency:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_urgency'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="col3_form_container">
					<div class="col1_form_container">
						<label class="labels">Date Needed:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_date_needed'] }}" />
					</div>
				</div>
			</div>

			<div class="clear_20"></div>

			<label class="form_label">REQUESTED DOCUMENTS:</label>

			<table border = "1" cellpadding = "0" class="tbl2">
				<th class="th_style td_height">Document</th>
				<th class="th_style td_height">Reference No.</th>
				<th class="th_style td_height">Assigned To</th>
				<th class="th_style td_height">Date Assigned</th>
				<th class="th_style td_height">Action</th>
				<th class="th_style td_height">CBR Remarks</th>
				@foreach($request['cbrd'] as $cbrd)
				<form action="{{ url('cbr/review-request/'.$request['cb_ref_num']); }}" method="post">
				<input type="hidden" value="{{ csrf_token() }}">
				<input type="hidden" name="cbrd_id" value="{{ $cbrd->cbrd_id }}">
				<input type="hidden" name="cbrd_ref_num" value="{{ $cbrd->cbrd_ref_num }}">
				<input type="hidden" name="cbrd_cb_id" value="{{ $cbrd->cbrd_cb_id }}">
				<tr>
					<td class="td_style td_height">{{ json_decode($cbrd->cbrd_req_docs)->name }}</td>
					<td class="td_style td_height">{{ $cbrd->cbrd_ref_num }}</td>
					<td class="td_style td_height">
						@if($cbrd->cbrd_assigned_to == "")
						<select name="emp_id" class="form-control" style="width: 241px">
							<option value=""> - </option>
							@foreach($chrd as $_chrd)
							<option value="{{ $_chrd['id'] }}">{{ $_chrd['name'] }}</option>
							@endforeach
						</select>
						@else
							{{ $cbrd->cbrd_assigned_to }}
						@endif
					</td>
					<td class="td_style td_height">{{ $cbrd->cbrd_assigned_date }}</td>
					<td class="td_style">
						<button type="submit" class="btn btn2 btn-default" name="action" value="assign">Assign</button>
						<button type="submit" class="btn btn2 btn-default" name="action" value="return">Return</button>
					</td>
					<td class="td_style td_height">
						@if($cbrd->cbrd_remarks == "")
							<input type="text" name="cbrd_remarks" />
						@else
							{{ $cbrd->cbrd_remarks }}
						@endif
					</td>
				</tr>
				</form>
				@endforeach
			</table>

			<div class="clear_20"></div>

			<label class="form_label">PURPOSE OF REQUEST:</label>
			<div class="textarea">
				<textarea readonly rows="4" cols="50" class="form-control textarea_width" placeholder="<Purpose of the request goes here>">{{ $request['cb_purpose_of_request'] }}</textarea>
			</div>

			<div class="clear_20"></div>
			
			<label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
			<div class="attachment_container">
				@if(count(json_decode($request['cb_attachments'])) > 0)
					@foreach(json_decode($request['cb_attachments']) as $attachment)
					<a href="{{ URL::to('/cbr/download/attachment') . '/' . $attachment->random_filename .'/' . $attachment->original_filename }}">{{ $attachment->original_filename }}</a><br />
					@endforeach
				@endif
			</div>

	</div><!-- end of form_container -->

	<div class="clear_20"></div>

	<div class="form_container">
		<div class="textarea_messages_container">
			<div class="row">
				<label class="textarea_inside_label">MESSAGE:</label>
				<textarea readonly name="purpose_request" rows="3" class="form-control textarea_inside_width" placeholder="DEBORAH METRA 08/04/2014 8:30 AM: Comment of requestor.">@if( $request['cb_comments'] != '') @foreach(json_decode($request['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }} {{ $comment->message }} @endforeach @endif</textarea>
			</div>
		</div>                
	</div><!-- end of form_container -->

	
	<div class="clear_20"></div>
@stop