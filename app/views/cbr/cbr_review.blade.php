@extends('template/header')

@section('content')
<link href="<?php echo $base_url; ?>/assets/css/builds/new_style.css" rel="stylesheet">
<div id="wrap">
	@include('template/sidebar')
    <div class="container">
        <form class="form-inline">
            <div class="form_container">
                <label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control placeholders" placeholder="yyyy-mm-dd" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Company:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" placeholder="For Processing"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Contact Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Section:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Urgency:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px">
                                    <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Needed:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px">
                                    <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label">REQUESTED DOCUMENTS:</label>

                    <table border = "1" cellpadding = "0" class="tbl3">
                        <th class="th_style td_height">Document</th>
                        <th class="th_style td_height">Reference No.</th>
                        <th class="th_style td_height">Assigned To</th>
                        <th class="th_style td_height">CBR Remarks</th>
                        <th class="th_style td_height">Date Document is Endorsed to Requestor</th>
                        <th class="th_style td_height">Date Acknowledge</th>

                        <tr>
                            <td class="td_style td_height">Clearance Certification</td>
                            <td class="td_style td_height">2014-00550-3</td>
                            <td class="td_style td_height">Comment</td>
                            <td class="td_style td_height">Sharla May Bautista</td>
                            <td class="td_style td_height">2014-08-14</td>
                            <td class="td_style td_height">2014-08-14</td>
                        </tr>
                    </table>


                    <div class="clear_20"></div>

                    <label class="form_label">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea name="purpose_request" rows="4" cols="50" class="form-control textarea_width" placeholder="<Purpose of the request goes here>"> </textarea>
                    </div>

                    <div class="clear_20"></div>
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
                    <div class="attachment_container">
                        <div class="sample_file">SAMPLE 1 FILENAME</div><br/>
                        <div class="sample_file">SAMPLE 2 FILENAME</div><br/>
                        <div class="sample_file">SAMPLE 3 FILENAME</div><br/>
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" placeholder="DEBORAH METRA 08/04/2014 8:30 AM: Comment of requestor."></textarea>
                    </div>
                </div>
                <div class="clear_10"></div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="">
                            <button type="submit" class="btn btn-default btnback" value="">BACK</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
        <div class="clear_20"></div>
    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop