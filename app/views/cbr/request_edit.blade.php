@extends('template/header')

@section('content')
	<form class="form-inline" action="{{ URL('cbr/resubmit') }}" method="post" enctype="multipart/form-data">
	
		<div class="form_container">
			<label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>
				
			@include('cbr.partials.request_info', $request)
			
			<div class="clear_20"></div>

			@include('cbr.partials.requested_docs1', $request)

			<div class="clear_20"></div>

			@include('cbr.partials.purpose_of_request', $request)

			<div class="clear_20"></div>

			@include('cbr.partials.attachments', array('browse'=>1))

		</div><!-- end of form_container -->

		<div class="clear_20"></div>

		<div class="form_container">
			@include('cbr.partials.messages', $request)

			<div class="clear_10"></div>

			@include('cbr.partials.commentbox', $request)

			<div class="clear_10"></div>

			<div class="row">
				<div class="comment_container">
					<div class="comment_notes">
						<label class="button_notes"><strong>SEND</strong> TO CHRD FOR PROCESSING</label>
					</div>
					<div class="comment_button">
						<input type="hidden" value="{{ $request['cbrd_id'] }}" name="cbrd_id" />
						<input type="hidden" value="{{ $request['cbrd_ref_num'] }}" name="cbrd_ref_num" />
						<button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
					</div>
				</div>
			</div>
		</div><!-- end of form_container -->
	</form>
@stop
@section('js_ko')
<script type="text/javascript">
    $(function () {
		$('.remove_file_attachment').live('click', function() {
			$(this).parent().remove();
		});

		$("#btnCbrAddAttachment").click(function(){
			if($("#attachments input:file").length != 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment' type='button'>DELETE</button></div>");
			}
		});
	});
</script>
@stop