@extends('template/header')

@section('content')
	<div class="clear_20"></div>
	<table id="requests_my_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable" width="100%" aria-describedby="requests_my_list">
		<thead>
			<tr role="row">
				<th align="" width="" class="" role="columnheader" rowspan="1" colspan="5" style="width: 136px;">My CBR Requests</th>
			</tr>
			<tr role="row">
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" style="width: 136px;">Reference No.</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" style="width: 222px;">Date Filed</th>
				<th align="center" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" style="width: 164px;">Requested Document</th>
				<th align="center" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" style="width: 164px;">Status</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"  style="width: 172px;">Action</th>
			</tr>							
		</thead>
	</table>
	<div class="clear_20"></div> 	
	<hr />
	@if(Session::get('is_cbr_receiver'))
	<table id="requests_new_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable" width="100%" aria-describedby="requests_new_list">
		<thead>
			<tr role="row">
				<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company" style="width: 136px;">Received CBR Requests</th>
			</tr>
			<tr role="row">
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Company" style="width: 136px;">Reference No.</th>
				<th align="center" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Department" style="width: 164px;">Requestor</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Employee Name" style="width: 222px;">Date Filed</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Action</th>
			</tr>							
		</thead>
	</table>
	<div class="clear_20"></div> 
	@endif
	@if(Session::get('is_cbr_receiver') || Session::get('is_cbr_staff'))
	<hr />
	<table id="requests_in_process_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable" width="100%" aria-describedby="requests_in_process_list">
		<thead>
			<tr role="row">
				<th align="" width="" class="" role="columnheader" rowspan="1" colspan="8" aria-label="Company" style="width: 136px;">In Process CBR Requests</th>
			</tr>
			<tr role="row">
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Company" style="width: 136px;">Reference No.</th>
				<th align="center" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Department" style="width: 164px;">Requestor</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Employee Name" style="width: 222px;">Date Filed</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Requested Document</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Assigned To</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Date Assigned</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Current</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Status</th>
			</tr>							
		</thead>
	</table>
	<div class="clear_20"></div> 
	<hr />
	<table id="requests_acknowledged_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable" width="100%" aria-describedby="requests_acknowledged_list">
		<thead>
			<tr role="row">
				<th align="" width="" class="" role="columnheader" rowspan="1" colspan="7" aria-label="Company" style="width: 136px;">Acknowledge CBR Requests</th>
			</tr>
			<tr role="row">
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Company" style="width: 136px;">Reference No.</th>
				<th align="center" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Department" style="width: 164px;">Requestor</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Employee Name" style="width: 222px;">Date Filed</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Requested Document</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Assigned To</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Date Completed</th>
				<th align="center" width="" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" aria-label="Designation" style="width: 172px;">Date Acknowledged</th>
				
			</tr>							
		</thead>
	</table>
	<div class="clear_20"></div>
	@endif
@stop