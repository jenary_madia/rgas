<div class="modal fade" id="AddLeaveDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog AddLeaveDetails" role="document">
        <validator name="addLeaveDetails">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Leave Details</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NAME</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control" v-validate:employeeName="['required']" v-model="employeeIndex" v-on:change="parseToAddData">
                                        <option v-for="employee in employees" value="@{{ $index }}">@{{ employee.firstname +' '+ employee.middlename +' '+ employee.lastname }}</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NUMBER</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control input-xs" readonly value="@{{ toAdd.employeeNumber }}">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">LEAVE TYPE</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {{ Form::select('leaveType', $leaveTypes, '', ['id' => 'leaveType','class'=> 'form-control','v-model'=> 'toAdd.leaveType', 'v-validate:leaveType' => '["required"]' ]) }}
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                        <div id="divLeaveDetails">
                            <div class="container-header"><h5 class="text-center lined"><strong>LEAVE DETAILS</strong></h5></div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="labels required">DURATION OF LEAVE:</label>
                                </div>
                                <div class="col-md-9">
                                    <select name="lrfDurationLeave" id="lrfDurationLeave"  v-validate:duration="['required']" v-model="toAdd.duration" class="form-control">
                                        <option value="half">Half Day</option>
                                        <option value="whole">One(1) Day</option>
                                        <option value="multiple">Multiple Days</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <div class="col-md-4 well col-md-offset-1">
                                        <label class="labels">FROM:</label>
                                        <br>
                                        <label class="labels required">DATE:</label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input readonly="readonly" type="text"  v-model="toAdd.fromDate" class="form-control date_picker" id="lrfDateFrom" name="lrfDateFrom"/>
                                            <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                        <div class="clear_10"></div>
                                        <label class="labels required">LEAVE PERIOD:</label>
                                        <select name="lrfLeavePeriodFrom" style="width:100%;" id="lrfLeavePeriodFrom" v-model="toAdd.fromPeriod" :disabled="toAdd.duration != 'multiple'" class="form-control pull-right">
                                            <option value="whole">Whole</option>
                                            <option value="half">Half</option>
                                        </select>
                                </div>
                                <div class="col-md-4 divTo well col-md-offset-1" v-if="toAdd.duration == 'multiple'">
                                        <label class="labels">TO:</label>
                                        <br>
                                        <label class="labels required">DATE:</label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input readonly="readonly" type="text" v-model="toAdd.toDate" class="form-control date_picker" name="lrfDateTo" id="lrfDateTo"/>
                                            <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                        <div class="clear_10"></div>
                                        <label class="labels required">LEAVE PERIOD:</label>
                                        <select name="lrfLeavePeriodTo"  style="width:100%;" id="lrfLeavePeriodTo" v-model="toAdd.toPeriod" class="form-control pull-right">
                                            <option value="whole">Whole</option>
                                            <option value="half">Half</option>
                                        </select>
                                </div>
                                <br>
                                <br>
                                <div class="col-md-12">
                                    <label class="labels required">TOTAL LEAVE DAYS:</label>
                                    <input min="0" type="number" max="99"  readonly name="lrfTotalLeaveDays" v-model="toAdd.totalDays" value="" id="lrfTotalLeaveDays" class="form-control"/>
                                </div>
                                <div class="col-md-12">
                                    <label class="labels required">REASON:</label>
                                    <textarea rows="3" class="form-control" v-validate:reason="['required']" name="lrfReason" v-model="toAdd.reason"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" @click="getToAddData()" :disabled="! checkIfAddValid">Save</button>
                </div>
            </div>
        </validator>
    </div>
</div>

<div class="modal fade" id="EditLeaveDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog EditLeaveDetails" role="document">
        <div class="modal-content">
            <validator name="editLeaveDetails">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Leave Details</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NAME</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly value="@{{ toEdit.employeeName }}">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NUMBER</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control input-xs" readonly value="@{{ toEdit.employeeNumber }}">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">LEAVE TYPE</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {{ Form::select('leaveType', $leaveTypes, '', ['id' => 'leaveType','class'=> 'form-control','v-model'=> 'toEdit.leaveType','v-validate:leaveType' => '["required"]' ]) }}
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                        <div id="divLeaveDetails">
                            <div class="container-header"><h5 class="text-center lined"><strong>LEAVE DETAILS</strong></h5></div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="labels required">DURATION OF LEAVE:</label>
                                </div>
                                <div class="col-md-9">
                                    <select name="lrfDurationLeave" id="lrfDurationLeave" v-model="toEdit.duration" v-validate:duration="['required']" class="form-control">
                                        <option value="">--Choose Duration of Leave--</option>
                                        <option value="half">Half Day</option>
                                        <option value="whole">One(1) Day</option>
                                        <option value="multiple">Multiple Days</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <div class="col-md-4 well col-md-offset-1">
                                    <label class="labels">FROM:</label>
                                    <br>
                                    <label class="labels required">DATE:</label>

                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text"  v-validate:dateFrom="['required']" v-model="toEdit.fromDate" class="form-control date_picker" id="lrfDateFrom" name="lrfDateFrom"/>
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>

                                    <div class="clear_10"></div>
                                    <label class="labels required">LEAVE PERIOD:</label>
                                    <select name="lrfLeavePeriodFrom" style="width:100%;" v-validate:duration="['periodFrom']" id="lrfLeavePeriodFrom" v-model="toEdit.fromPeriod" :disabled="toEdit.duration != 'multiple'" class="form-control pull-right">
                                        <option value="whole">Whole</option>
                                        <option value="half">Half</option>
                                    </select>
                                </div>
                                <div class="col-md-4 divTo well col-md-offset-1" v-show="toEdit.duration == 'multiple'">
                                    <label class="labels">TO:</label>
                                    <br>
                                    <label class="labels required">DATE:</label>

                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text" v-model="toEdit.toDate" v-validate:dateTo="['required']" class="form-control date_picker"/>
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>

                                    <div class="clear_10"></div>
                                    <label class="labels required">LEAVE PERIOD:</label>
                                    <select name="lrfLeavePeriodTo" style="width:100%;" id="lrfLeavePeriodTo" v-validate:periodTo="['required']" v-model="toEdit.toPeriod" class="form-control pull-right">
                                        <option value="whole">Whole</option>
                                        <option value="half">Half</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <div class="col-md-12">
                                    <label class="labels required">TOTAL LEAVE DAYS:</label>
                                    <input min="0" type="number" max="99"  readonly name="lrfTotalLeaveDays" v-validate:total="{ min: 0.5 }" v-model="toEdit.totalDays" value="" id="lrfTotalLeaveDays" class="form-control"/>
                                </div>
                                <div class="col-md-12">
                                    <label class="labels required">REASON:</label>
                                    <textarea rows="3" class="form-control" name="lrfReason" v-validate:reason="['required']" v-model="toEdit.reason"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" @click="saveEdit()" :disabled="! checkIfEditValid">Save</button>
                </div>
            </validator>
        </div>
    </div>
</div>