<div class="modal fade" id="homeVisitError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog homeVisitError" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>YOU HAVE EXCEED THE MAXIMUM NUMBER OF HOME VISITATION LEAVE FOR THE QUARTER. KINDLY FILE THE EXCESS DAYS UNDER A DIFFERENT TYPE OF A LEAVE</p>
                <button type="button" class="btn btn-default btn-xs" id="home-visit-btn-error">EDIT LEAVE DETAILS</button>
            </div>
        </div>
    </div>
</div>