@extends('template.header')

@section('content')
    <div id="peCreate" v-cloak>
        {{ Form::open(array('url' => 'lrf/pe_action', 'method' => 'post', 'files' => true,'id' => 'formCreateLRF')) }}
        <div class="form_container">
            <h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5>
                <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DEPARTMENT</label>
                    </div>
                    <div class="col2_form_container">
                        {{ Form::select('department',$departments, Input::old('department', 0), ['class'=>'form-control otherFields','id'=>'schedule_typeOffset','v-model'=>'departmentID']) }}
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REFERENCE NUMBER</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" value=""/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION</label>
                    </div>
                    <div class="col2_form_container">
                        <select class="form-control" :disabled="sections.length == 0" v-model="sectionID" name="section">
                            <option v-for="section in sections" value="@{{ section.id }}">@{{ section.sect_name }}</option>
                        </select>
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" name="dateFiled" class="form-control" value=""/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center text-uppercase">Employee Name</th>
                                <th class="text-center text-uppercase">Employee Number</th>
                                <th class="text-center text-uppercase">Leave Type</th>
                                <th class="text-center text-uppercase">Duration of Leave</th>
                                <th class="text-center text-uppercase">Total Leave Days</th>
                                <th class="text-center text-uppercase">Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="employee in employeesToFile"  v-bind:class="{ 'active' : employee.active }" @click="chooseEmployee(employee,$index)">
                                <td class="text-center text-uppercase">@{{ employee.employeeName }}</td>
                                <td class="text-center text-uppercase">@{{ employee.employeeNumber }}</td>
                                <td class="text-center text-uppercase">@{{ employee.leaveName }}</td>
                                <td class="text-center text-uppercase">@{{ employee.duration }}</td>
                                <td class="text-center text-uppercase">@{{ employee.totalDays }}</td>
                                <td class="text-center text-uppercase">@{{ employee.reason }}</td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="@{{ employeesToFile | json }}" name="employeesToFile">
                        <input type="hidden" v-model="employeesToFileOld" value='{{ Input::old("employeesToFile") }}'>
                    </div>
                    <div class="clear_10"></div>
                    <div class="col-md-12" style="margin-left: 30px;">
                        <a class="btn btn-default btndefault" data-toggle="modal" :disabled="employees.length == 0" @click="clear" data-target="#AddLeaveDetails">ADD</a>
                        <a class="btn btn-default btndefault"  data-toggle="modal" :disabled="!editable" data-target="#EditLeaveDetails">EDIT</a>
                        <button type="submit" class="btn btn-default btndefault"  :disabled="!deletable" @click.prevent="deleteOffsetReference">DELETE</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>

            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" name="comment" class="form-control textarea_inside_width"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div>
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="toSave">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                    </div>
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="toSend">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

        {{ Form::close() }}
        @include("lrf.modals.pe_create")
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/leaves/vue-pe.js') }}
@stop