@extends('template/header')

@section('content')
	{{ Form::open(array('url' => array('lrf/action',$leaveDetails['id']), 'method' => 'post', 'files' => true,'id' => 'formCreateLRF')) }}
        <div class="form_container">
            <div class="container-header"><h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" id="lrfEmployeeName" name="lrfEmployeeName" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
							<input readonly="readonly" type="text" class="form-control" value="{{ ($leaveDetails['referenceNo'] )}}" />
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfEmployeeId" value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfDateFiled" value="{{ date('Y-m-d') }}"/>
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfCompany" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfStatus" value="NEW" />
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfDepartment" value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="lrfContactNumber" maxlength="20" value="{{ (Input::old('lrfContactNumber') ? Input::old('lrfContactNumber') : $leaveDetails['contactNo']) }}"/>
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" maxlength="25" name="lrfSection" value="{{ (Input::old('lrfSection') ? Input::old('lrfSection') : $leaveDetails['section']) }}" />
                        </div>
                    </div>

					<div class="clear_10"></div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">TYPE OF LEAVE:</label>
                        </div>
                        <div class="col2_form_container">
  							<select name="lrfLeaveType" class="form-control LRFLeaveType">
								@foreach ($leaveTypes as $leaveType)
									@if($leaveType['item'] == "HomeVisit")
										@if(in_array(Session::get("employee_id"),json_decode(EXPATRIATES,true)))
											<option value="{{ $leaveType->item }}" {{ (Input::old("lrfLeaveType",$leaveDetails['leaveType']) == $leaveType->item ? "selected":"") }} >{{ $leaveType->text }}</option>
										@endif
									@else
										<option value="{{ $leaveType->item }}" {{ (Input::old("lrfLeaveType",$leaveDetails['leaveType']) == $leaveType->item ? "selected":"") }} >{{ $leaveType->text }}</option>
									@endif
								@endforeach ?>
  							</select>
                        </div>
                        @include('lrf.modals.home-visit-error')
                    </div>
                </div>
                <div class="clear_20"></div>
                <div class="container-header titleLeaveType" hidden><label class="labels required">SELECT A LEAVE TYPE</label></div>
                <div class="LRFAdditionalFields"
            		@if (Input::old("lrfLeaveType") == 'HomeVisit' || $leaveDetails["leaveType"] == 'HomeVisit')
            			hidden
            		@endif
                	>
					<div class="clear_20"></div>
            		<div class="container-header"><h5 class="text-center lined"><strong>WORK WEEK SCHEDULE DETAILS</strong></h5></div>
					<div class="row">
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels required">TYPE OF SCHEDULE:</label>
	                        </div>
	                        <div class="col2_form_container">
	  							<select name="lrfTypeOfSchedule" onchange="disabledRestDay(this)" id="lrfTypeOfSchedule" class="form-control otherFields" {{ (Input::old("lrfLeaveType",$leaveDetails['scheduleType']) == "HomeVisit" ? "disabled":"") }}>
	  								<option value="compressed" {{ (Input::old("lrfTypeOfSchedule",$leaveDetails['scheduleType']) == "compressed" ? "selected":"") }}>Compressed</option>
	  								<option value="regular"  {{ (Input::old("lrfTypeOfSchedule",$leaveDetails['scheduleType']) == "regular" ? "selected":"") }}>Regular</option>
	  								<option value="special"  {{ (Input::old("lrfTypeOfSchedule",$leaveDetails['scheduleType']) == "special" ? "selected":"") }}>Special</option>
	  							</select>
	                        </div>
	                    </div>
	                    <div class="clear_10"></div>
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels">REST DAY:</label>
	                        </div>
	                        <div class="col2_form_container">
                                <select name="lrfRestDay[]" id="lrfRestDay" multiple="multiple"
									{{ (Input::old('lrfTypeOfSchedule',$leaveDetails['scheduleType']) ? (Input::old('lrfTypeOfSchedule',$leaveDetails['scheduleType']) == 'special' ? "" : "disabled") : "disabled") }}
								>
                                    @for ($i = 0; $i < 7; $i++)
                                        <option value="{{ $i }}"
                                        @if(Input::old('lrfRestDay',$leaveDetails['restDay']))
                                            {{ (in_array($i, Input::old('lrfRestDay',$leaveDetails['restDay'])) == true ? "selected" : "") }}
                                                @else
                                            {{ ($i == 0 || $i == 6 ? "selected" : "") }}
                                                @endif
                                        >
                                            @if( $i == 0) Sunday
                                            @elseif( $i == 1) Monday
                                            @elseif( $i == 2) Tuesday
                                            @elseif( $i == 3) Wednesday
                                            @elseif( $i == 4) Thursday
                                            @elseif( $i == 5) Friday
                                            @elseif( $i == 6) Saturday
                                            @endif
                                        </option>
                                    @endfor
                                </select>
	                        </div>
	                    </div>
	                </div>
                </div>
                <div id="divLeaveDetails">
	             	<div class="clear_20"></div>
            		<div class="container-header"><h5 class="text-center lined"><strong>LEAVE DETAILS</strong></h5></div>
					<div class="row">
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels required">DURATION OF LEAVE:</label>
	                        </div>
	                        <div class="col2_form_container">
	  							<select name="lrfDurationLeave" id="lrfDurationLeave" onchange="ChooseDurationOfLeave(this)" class="form-control">
									<option value="">--Choose Duration of Leave--</option>
	  								<option value="half" {{ (Input::old("lrfDurationLeave") ? (Input::old("lrfDurationLeave") == "half" ? "selected":"") : ($leaveDetails["duration"] == "half" ? "selected":"")) }}>Half Day</option>
	  								<option value="whole" {{ (Input::old("lrfDurationLeave") ? (Input::old("lrfDurationLeave") == "whole" ? "selected":"") : ($leaveDetails["duration"] == "whole" ? "selected":"")) }}>One(1) Day</option>
	  								<option value="multiple" {{ (Input::old("lrfDurationLeave") ? (Input::old("lrfDurationLeave") == "multiple" ? "selected":"") : ($leaveDetails["duration"] == "multiple" ? "selected":"")) }}>Multiple Days</option>

	  							</select>
	                        </div>
	                    </div>
	                    <div class="row_form_container">
	                        <div class="col2_form_container">
								@if(in_array($leaveDetails['ownerId'],json_decode(EXPATRIATES,true)))
									<a class="btn btn-default btn-xs" id="showLeaves" onClick="MyWindow=window.open('{{url('lrf/show_leaves/'.$leaveDetails['ownerId'].'/hv')}}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
								@else
									<a class="btn btn-default btn-xs" id="showLeaves" onClick="MyWindow=window.open('{{url('lrf/show_leaves/'.$leaveDetails['ownerId'])}}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
								@endif
                            </div>
	                    </div>
	                    <div class="clear_10"></div>
	                    <div class="row">
	                    	<div class="col-md-1"></div>
		                    <div class="col-md-4 bordered">
		                        <div class="col2_form_container pull-left">
		                        	<label class="labels">FROM:</label>
			                    	<br>
		                        	<label class="labels required">DATE:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text" class="form-control date_picker" id="lrfDateFrom" name="lrfDateFrom"
                                               @if($leaveDetails["duration"] == "")
                                               @if(Input::old('lrfDurationLeave'))
                                               @if(Input::old('lrfDurationLeave') == "")
                                               disabled
                                               @endif
                                               @else
                                               disabled
                                               @endif
                                               @endif

                                               @if(Input::old('lrfDateFrom'))
                                               value="{{ Input::old('lrfDateFrom') }}"
                                               @else
                                               value="{{ $leaveDetails['from'] }}"
                                                @endif
                                        />
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
		  							<div class="clear_10"></div>
		                        	<label class="labels required">LEAVE PERIOD:</label>
		  							<select name="lrfLeavePeriodFrom" id="lrfLeavePeriodFrom" onchange="computeTotalLeave()" class="form-control pull-right"
		  								@if (Input::old("lrfDurationLeave"))
		  									@if (Input::old("lrfDurationLeave") != "multiple" )
		  										disabled
		  									@endif
		  								@else
		  									@if ($leaveDetails["duration"] != "multiple" )
		  										disabled
		  									@endif
		  								@endif
		  							>
		  								<option>--Choose Leave Period--</option>
		  								<option value="whole" {{ (Input::old("lrfLeavePeriodFrom") ? (Input::old("lrfLeavePeriodFrom") == "whole" ? "selected":"") : ($leaveDetails["periodFrom"] == 2 ? "selected" : "")) }}>Whole</option>
		  								<option value="half" {{ (Input::old("lrfLeavePeriodFrom") ? (Input::old("lrfLeavePeriodFrom") == "half" ? "selected":"") : ($leaveDetails["periodFrom"] == 1 ? "selected" : "")) }}>Half</option>
		  							</select>
		                        </div>
		                    </div>
		                    <div class="col-md-1"></div>
		                    <div class="col-md-4 divTo bordered"
			                    @if($leaveDetails["duration"] != "multiple")
			                    	@if (Input::old("lrfDurationLeave"))
		                    			@if (Input::old('lrfDurationLeave') != "multiple")
		                    			hidden
		                    			@endif
		                    		@else
		                    			hidden
		                    		@endif
			                    @endif
	                    	>
		                        <div class="col2_form_container">
		                        	<label class="labels">TO:</label>
			                    	<br>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text" class="form-control date_picker" name="lrfDateTo" id="lrfDateTo"
                                               @if($leaveDetails["to"])
                                               @if (Input::old("lrfDateTo"))
                                               value="{{ Input::old("lrfDateTo") }}"
                                               @else
                                               value='{{ $leaveDetails["to"] }}'
                                               @endif
                                               @else
                                               disabled
                                                @endif
                                        />
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
		                        	<label class="labels required">DATE:</label>

		  							<div class="clear_10"></div>
		                        	<label class="labels required">LEAVE PERIOD:</label>
		  							<select name="lrfLeavePeriodTo" id="lrfLeavePeriodTo" onchange="computeTotalLeave()" class="form-control pull-right"
		  								@if (Input::old("lrfDurationLeave"))
		  									@if (Input::old("lrfDurationLeave") != "multiple" )
		  										disabled
		  									@endif
		  								@else
		  									@if ($leaveDetails["duration"] != "multiple" )
		  										disabled
		  									@endif
		  								@endif
		  							>
		  								<option>--Choose Leave Period--</option>
		  								<option value="whole" {{ (Input::old("lrfLeavePeriodTo") ? (Input::old("lrfLeavePeriodTo") == "whole" ? "selected":"") : ($leaveDetails["periodTo"] == 2 ? "selected" : "")) }}>Whole</option>
		  								<option value="half" {{ (Input::old("lrfLeavePeriodTo") ? (Input::old("lrfLeavePeriodTo") == "half" ? "selected":"") : ($leaveDetails["periodTo"] == 1 ? "selected" : "")) }}>Half</option>
		  							</select>
		                        </div>
		                    </div>
		                    <div class="col-md-1"></div>
	                    </div>
						<div class="clear_20"></div>
						<div class="clear_20"></div>
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels required">TOTAL LEAVE DAYS:</label>
	                        </div>
	                        <div class="col2_form_container">
	                            <input min="0" type="number" max="99"  readonly name="lrfTotalLeaveDays" value="{{ (Input::old('lrfTotalLeaveDays',$leaveDetails['totalLeaveDays'])) }}" id="lrfTotalLeaveDays" class="form-control"/>
	                        </div>
	                    </div>
	                    <div class="clear_10"></div>
	                    <div class="row_form_container">
						    <div class="textarea_messages_container" style="border: 0;">
						        <div class="row">
						            <label class="textarea_inside_label required">REASON:</label>
						            <textarea rows="3" class="form-control textarea_inside_width" name="lrfReason">{{ (Input::old('lrfReason') ? Input::old('lrfReason') : $leaveDetails["reason"]) }}</textarea>
						        </div>
						    </div>
	                    </div>
	                </div>
					<div class="clear_20"></div>
            		<div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
					<div class="clear_20"></div>
					<div class="row">
						<div class="col-md-6">
                            <p><strong>FILER : </strong></p>
                            <div id="med_cert"
                            @if(in_array(Input::old('lrfLeaveType',$leaveDetails['leaveType']),['Mat/Pat','Sick']) && Input::old('lrfTotalLeaveDays',$leaveDetails['totalLeaveDays']) > 3)

                            @else
                               {{ ($leaveDetails["clinic_attachment"] ? "" : "hidden") }}
                            @endif
                            >
                                <label class="attachment_note required">MEDICAL CERTIFICATE:
                                        <span class="btn btn-success btnbrowse fileinput-button">
                                            <span>BROWSE</span>
                                            <input id="med_fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                                        </span>
                                </label>
                                <div id="med_attachments">
                                    <?php $med_count = 0 ?>
                                    @if(Input::old("med_files",$leaveDetails["clinic_attachment"]))
                                        <?php $med_count = 1 ?>
                                        <p>
                                            {{ Input::old("med_files",$leaveDetails["clinic_attachment"])[1]["original_filename"] }} | 201231KB
                                            <input type='hidden' name='med_files[{{ $med_count }}][random_filename]' value='{{ Input::old("med_files",$leaveDetails["clinic_attachment"])[1]["random_filename"] }}'>
                                            <input type='hidden' name='med_files[{{ $med_count }}][original_filename]' value='{{ Input::old("med_files",$leaveDetails["clinic_attachment"])[1]["original_filename"] }}'>
                                            <input type='hidden' class='attachment_filesize' name='med_files[{{ $med_count }}][filesize]' value='{{ Input::old("med_files",$leaveDetails["clinic_attachment"])[1]["filesize"] }}'>
                                            <input type='hidden' name='med_files[{{ $med_count }}][mime_type]' value='{{ Input::old("med_files",$leaveDetails["clinic_attachment"])[1]["mime_type"] }}'>
                                            <input type='hidden' name='med_files[{{ $med_count }}][original_extension]' value='{{ Input::old("med_files",$leaveDetails["clinic_attachment"])[1]["original_extension"] }}'>
                                            <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                        </p>
                                    @endif
                                </div>
                            </div>
							<label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
							<div id="attachments">
                                <?php $count = 1 ?>
                                    @if(Input::old("files"))
                                        @for ($i = 1; $i <= count(Input::old("files")); $i++)
                                            <p>
                                                {{ Input::old("files")[$i]["original_filename"] }} | {{ Input::old("files")[$i]["filesize"] }}
                                                <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files")[$i]["random_filename"] }}'>
                                                <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files")[$i]["original_filename"] }}'>
                                                <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files")[$i]["filesize"] }}'>
                                                <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files")[$i]["mime_type"] }}'>
                                                <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files")[$i]["original_extension"] }}'>
                                                <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                            </p>

                                        @endfor
                                    @else
                                        @for ($i = 1; $i < 6; $i++)
                                            @if(json_decode($leaveDetails["attach$i"],true) == true)
                                                <p>
                                                    {{ json_decode($leaveDetails["attach$i"],true)["original_filename"] }} | {{ json_decode($leaveDetails["attach$i"],true)["filesize"] }}KB
                                                    <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ json_decode($leaveDetails["attach$i"],true)["random_filename"] }}'>
                                                    <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ json_decode($leaveDetails["attach$i"],true)["original_filename"] }}'>
                                                    <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ json_decode($leaveDetails["attach$i"],true)["filesize"] }}'>
                                                    <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ json_decode($leaveDetails["attach$i"],true)["mime_type"] }}'>
                                                    <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ json_decode($leaveDetails["attach$i"],true)["original_extension"] }}'>
                                                    <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                                    <?php $count++ ?>
                                                </p>
                                            @endif
                                        @endfor
                                    @endif
                            </div>
							<span class="btn btn-success btnbrowse fileinput-button">
                                <span>BROWSE</span>
                                <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                            </span>
						</div>

						@if(in_array($leaveDetails["status"],array("RETURNED","CANCELLED")))
							@if($leaveDetails['receiver_attachments'])
                                <div class="col-md-6">
                                    <p><strong>CHRD : </strong></p>
                                    <div class="attachment_container">
                                        @foreach($leaveDetails['receiver_attachments'] as $key)
                                            <a href="{{ URL::to('/lrf/download'.'/'.$leaveDetails['referenceNo'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key["original_filename"])) }}">{{ $key["original_filename"] }}</a><br />
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endif
					</div>
                </div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>
        <span class="action-label labels">ACTION</span>
		<div class="form_container">
			<div class="clear_20"></div>
            <div class="textarea_messages_container">
				<div class="row">
					<label class="textarea_inside_label">MESSAGE:</label>
					<textarea rows="3" class="form-control textarea_inside_width" readonly> @if($leaveDetails["status"] != "NEW") @foreach ($leaveDetails['message'] as $message) {{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }}&#013; @endforeach @endif</textarea>
				</div>
				<div class="clear_20"></div>
				<div class="row">
					<label class="textarea_inside_label">COMMENT:</label>
					<textarea rows="3" class="form-control textarea_inside_width" name="lrfComment">@if($leaveDetails["status"] == "NEW") @foreach ($leaveDetails['message'] as $message) {{ json_decode($message,true)['message'] }}&#013; @endforeach @endif</textarea>
				</div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div>
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="resave">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        @if(Session::get("desig_level") == "president")
                            <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO HR FOR PROCESSING
                        @else
                            <label class="button_notes"><strong>SEND</strong>TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                        @endif
                    </div>
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="lrfAction" id="btnSend" value="resend">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->


    {{ Form::close() }}
@stop
@section('js_ko')
	{{ HTML::script('/assets/js/jquery-ui-1.10.4.custom.js') }}
	<script>

        var med_file_counter = {{ $med_count }};
        var med_allowed_file_count = 1;
        var med_allowed_total_filesize = 20971520;


        var	file_counter = {{ $count }};
		var allowed_file_count = 5;
		var allowed_total_filesize = 20971520;
		var base_url = $("base").attr("href");

        var ownerId = '{{ Session::get("employee_id") }}';
        var company = '{{ Session::get("company") }}';
        var corporateCompanies = '{{ CORPORATE }}';


		$('body').on('click','.remove-fn',function () {
			$(this).closest( "p" ).remove();
		});
	</script>
	<script type="text/javascript">
        $(document).on('ready',function(){

            var restDays = $('#lrfRestDay').val();

            $('#lrfRestDay').multiselect({
                onInitialized: function () {
                    changeDisableDays(restDays[0],restDays[1]);
                    var selectedOptions = $('#lrfRestDay option:selected');

                    if (selectedOptions.length >= 2) {
                        // Disable all other checkboxes.
                        var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                            return !$(this).is(':selected');
                        });

                        nonSelectedOptions.each(function() {
                            var input = $('input[value="' + $(this).val() + '"]');
                            input.prop('disabled', true);
                            input.parent('li').addClass('disabled');
                        });
                    }
                },
                onChange: function(option, checked) {
                    restDays = $('#lrfRestDay').val();
                    changeDisableDays(restDays[0],restDays[1]);
                    var selectedOptions = $('#lrfRestDay option:selected');

                    if (selectedOptions.length >= 2) {
                        // Disable all other checkboxes.
                        var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                            return !$(this).is(':selected');
                        });

                        nonSelectedOptions.each(function() {
                            var input = $('input[value="' + $(this).val() + '"]');
                            input.prop('disabled', true);
                            input.parent('li').addClass('disabled');
                        });
                    }
                    else {
                        // Enable all checkboxes.
                        $('#lrfRestDay option').each(function() {
                            var input = $('input[value="' + $(this).val() + '"]');
                            input.prop('disabled', false);
                            input.parent('li').addClass('disabled');
                        });
                    }
                    $('#lrfDateTo').val("");
                    $('#lrfDateFrom').val("");
                    $('#lrfTotalLeaveDays').val(0);
                    computeTotalLeave();
                },
                onDropdownShown: function () {
                    var selectedOptions = $('#lrfRestDay option:selected');

                    if (selectedOptions.length >= 2) {
                        // Disable all other checkboxes.
                        var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                            return !$(this).is(':selected');
                        });

                        nonSelectedOptions.each(function() {
                            var input = $('input[value="' + $(this).val() + '"]');
                            input.prop('disabled', true);
                            input.parent('li').addClass('disabled');
                        });
                    }
                }
            });

            if ($('.LRFLeaveType').val() == "HomeVisit") {
                $('#lrfDateFrom').datepicker('option', 'beforeShowDay', function(date) {
                    var day = date.getDay();
                    return [(day != 8 && day != 9)];
                });
            };

            $("#lrfTypeOfSchedule").on("change",function () {

                if ($(this).val() === 'special') {
                    $('.labelRestday').addClass("required");
                    $('#lrfRestDay').val([6,0]);
                    $("#lrfRestDay").multiselect('enable');
                    changeDisableDays(restDays[0],restDays[1]);
                }else if($(this).val() === 'regular'){
                    $('.labelRestday').removeClass("required");
                    $('#lrfRestDay').val(0);
                    $("#lrfRestDay").multiselect('disable');
                    changeDisableDays(0,9);
                }else if($(this).val() === 'compressed'){
                    $('.labelRestday').removeClass("required");
                    $('#lrfRestDay').val([6,0]);
                    $("#lrfRestDay").multiselect('disable');
                    changeDisableDays(6,0);
                }
                $("#lrfRestDay").multiselect("refresh");
                computeTotalLeave();
            });

            $("#lrfDateTo").on("change",function () {
                var startDate = new Date($('#lrfDateFrom').val());
                var endDate = new Date($('#lrfDateTo').val());
                if (startDate >= endDate){
                    if ($('#lrfDurationLeave').val() == 'multiple') {
                        alert("Please check your leave dates!");
                    }
                    $('#lrfDateTo').val("");
                }
            });

            $('#lrfDateFrom').on('change',function(){
                var startDate = new Date($('#lrfDateFrom').val());
                var endDate = new Date($('#lrfDateTo').val());
                if (startDate >= endDate){
                    if ($('#lrfDurationLeave').val() == 'multiple') {
                        alert("Please check your leave dates!");
                    }
                    $('#lrfDateFrom').val("");
                }
            });

            $('body').on('click','#home-visit-btn-error',function () {
                $("#homeVisitError").modal("hide");
                $('.LRFAdditionalFields').fadeOut();
                $('#divLeaveDetails').fadeOut();
                $('.titleLeaveType').fadeIn();
            });


        });
		$(function() {

            $('.date_picker').datepicker({
                dateFormat : 'yy-mm-dd',
                beforeShowDay : function(date) {
                    var day = date.getDay();
                    return [(day != 6 && day != 0)];
                }
            })
            .on('change',function(){
                computeTotalLeave();
            });


			$('#lrfDateTo').on('change',function(){
				$('#lrfLeavePeriodTo').removeAttr('disabled');
			});


            $('.LRFLeaveType').on('change',function(){
                if ($(this).val() == "HomeVisit") {
                    $.ajax({
                        url: base_url+'/lrf/validate_leave',
                        method: "POST",
                        dataType: "JSON",
                    }).done(function(data) {
                        if(! data) {
                            $("#homeVisitError").modal("show");
                        }else{
                            $('.LRFAdditionalFields').fadeOut();
                            changeDisableDays(8,9);
                        }
                    });

                }else{
                    if($(this).val() != "") {
                        $('.LRFAdditionalFields').fadeIn();
                    }else{
                        $('.LRFAdditionalFields').fadeOut();
                    }
                }
                if($(this).val() != "") {
                    $('.titleLeaveType').fadeOut();
                    $('#divLeaveDetails').fadeIn();
                }else{
                    $('.titleLeaveType').fadeIn();
                    $('#divLeaveDetails').fadeOut();
                }

                computeTotalLeave();

            });

		});

        function ChooseDurationOfLeave(input)
        {
            $('#lrfDateFrom').removeAttr("disabled");

            if (input.value == 'whole') {
                $('#lrfTotalLeaveDays').val(1);
                checkIfNeedMedCert(1);
                $('.divTo').fadeOut();
                $('#lrfDateTo').attr('disabled',true);
                $('#lrfLeavePeriodFrom').val("whole");
                $('#lrfLeavePeriodFrom').attr("disabled",true);
            }else if (input.value == 'half') {
                $('#lrfTotalLeaveDays').val(0.5);
                checkIfNeedMedCert(0.5);
                $('.divTo').fadeOut();
                $('#lrfDateTo').attr('disabled',true);
                $('#lrfLeavePeriodFrom').val("half");
                $('#lrfLeavePeriodFrom').attr("disabled",true);
            }else if (input.value == 'multiple'){
                $('.divTo').fadeIn();
                $('#lrfLeavePeriodFrom').removeAttr("disabled");
                $('#lrfDateTo').removeAttr("disabled");
                computeTotalLeave();

            }
        }

        function computeTotalLeave() {
            if ($('#lrfDateFrom').val()) {
                var leavePeriodFrom = $('#lrfLeavePeriodFrom').val();
                var leavePeriodTo = $('#lrfLeavePeriodTo').val();
                var counter = 1;
                if ($('#lrfDateTo').val())
                {
                    var startDate = new Date($('#lrfDateFrom').val());
                    var endDate = new Date($('#lrfDateTo').val());
                    var restDays = $('#lrfRestDay').val();
                    if($(".LRFLeaveType").val() == "HomeVisit") {
                        restDays = ['8','9'];
                    }
                    if (! restDays) {
                        alert("Rest days are required");
                    }else{
                        var dateFrom = $("#lrfDateFrom").datepicker().val();
                        var a = $("#lrfDateFrom").datepicker('getDate').getTime(),
                                b = $("#lrfDateTo").datepicker('getDate').getTime(),
                                c = 24*60*60*1000,
                                diffDays = Math.round(Math.abs((a - b)/(c)));

                        for (var i = 0; i < diffDays; i++) {
                            startDate.setDate(startDate.getDate() + 1); //number  of days to add, e.x. 15 days
                            var dateFormated = startDate.toISOString().substr(0,10);
                            var finalDate = new Date(dateFormated);
                            var day = finalDate.getDay();
                            var checkIfrestDay = restDays.indexOf(day.toString());
                            if (checkIfrestDay == -1)
                            {
                                counter += 1;
                            }
                            startDate = new Date(dateFormated);

                        };

                        if (leavePeriodFrom == 'half') {
                            counter -= .5;
                        }
                        if (leavePeriodTo == 'half') {
                            counter -= .5;
                        }
                        $('#lrfTotalLeaveDays').val(counter);
                        // console.log($.inArray($(".LRFLeaveType").val(),["Mat\/Pat","Sick"]));

                        checkIfNeedMedCert(counter);
                    }

                }else{
                    if (leavePeriodFrom == 'half') {
                        counter -= .5;
                    }
                    $('#lrfTotalLeaveDays').val(counter);
                }

            }
        }

        function checkIfNeedMedCert(counter) {
            if ( $.inArray(company,JSON.parse(corporateCompanies))  === -1 && counter > 3 && $.inArray($(".LRFLeaveType").val(),["Mat\/Pat","Sick"]) != -1) {
                $("#med_cert").show();
            }else{
                $("#med_cert").hide();
            }
        }

        function changeDisableDays(day1,day2)
        {
            $('#lrfDateFrom').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != day1 && day != day2)];
            });

            $('#lrfDateTo').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != day1 && day != day2)];
            });
        }

        $('#btnSend').on('click',function(e){
            $('#lrfLeavePeriodFrom').removeAttr('disabled');
            $('#lrfRestDay').removeAttr('disabled');
            $('#lrfLeavePeriodFrom').attr('readonly',true);
            $('#lrfTotalLeaveDays').removeAttr('disabled');
        });

	</script>
	{{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop