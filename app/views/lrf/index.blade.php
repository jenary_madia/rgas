@extends('template/header')

@section('content')
	<div id="wrap">
		<div class="clear_20"></div>
		<div class="wrapper">
			{{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formMyleaves')) }}
				<table id="myLeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myLeaves">
					<thead>
						<tr role="row">
							<th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">My Leaves</th>
						</tr>
						<tr role="row">
							<th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Leave</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Leave</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
						</tr>
					</thead>
				</table>
			{{ Form::close() }}
			<div class="clear_20"></div>
			@if(in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
				<table id="myPELeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myPELeaves">
					<thead>
					<tr role="row">
						<th class="" role="columnheader" rowspan="1" colspan="5">My Production Encoders Leave Requests</th>
					</tr>
					<tr role="row">
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
						<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
					</tr>
					</thead>
				</table>
			<div class="clear_20"></div>
			@endif
			<hr />
			<div class="clear_20"></div>
			{{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formApproveLeave')) }}
				<table id="forApproveLeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApproveLeaves">
					<thead>
						<tr role="row">
							<th class="" role="columnheader" rowspan="1">For Approval</th>
							<th class="" role="columnheader" rowspan="1"><input type="checkbox" id="chooseAll"> All</th>
						</tr>
						<tr role="row">
							<th style="text-align: left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Leave</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Leave</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
							<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
						</tr>
					</thead>
				</table>
			{{ Form::close() }}
			<div class="clear_20"></div>
			@if(! in_array(Session::get("company"),json_decode(CORPORATE,true)))
			<table id="superiorPELeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorPELeaves">
				<thead>
				<tr role="row">
					<th class="" role="columnheader" rowspan="1" colspan="5">Production Encoders For Approval Leave Requests</th>
				</tr>
				<tr role="row">
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
					<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
				</tr>
				</thead>
			</table>
			<div class="clear_20"></div>
			@endif
			<hr />
		</div>
	</div>
@stop
@section('js_ko')
	{{ HTML::script('/assets/js/leaves/my_pe_leaves.js') }}
	{{ HTML::script('/assets/js/leaves/superior_pe_leaves.js') }}
	{{ HTML::script('/assets/js/leaves/myLeaves.js') }}
	{{ HTML::script('/assets/js/leaves/forApproveLeaves.js') }}
	{{ HTML::script('/assets/js/leaves/clinicApprovalLeaves.js') }}
@stop
