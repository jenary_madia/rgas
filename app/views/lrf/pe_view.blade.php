@extends('template.header')

@section('content')
    <div id="peISView" v-cloak>
        {{ Form::open(array('url' => array('lrf/submit',$leaveBatch['id']), 'method' => 'post', 'files' => true)) }}
        <div class="form_container">
            <h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DEPARTMENT</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['department']['dept_name']}}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REFERENCE NUMBER</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="reference" readonly value="{{ $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']}}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['section']['sect_name']}}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['datecreated']}}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th class="text-center text-uppercase">Employee Name</th>
                                <th class="text-center text-uppercase">Employee Number</th>
                                <th class="text-center text-uppercase">Leave Type</th>
                                <th class="text-center text-uppercase">Duration of Leave</th>
                                <th class="text-center text-uppercase">Total Leave Days</th>
                                <th class="text-center text-uppercase">Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="employee in employeesToFile"  v-bind:class="{ 'active' : employee.active }" @click="chooseEmployee(employee,$index)">
                                <td><input type="checkbox" :checked="employee.noted" disabled></td>
                                <td class="text-center">@{{ employee.employeeName }}</td>
                                <td class="text-center">@{{ employee.employeeNumber }}</td>
                                <td class="text-center">@{{ employee.leaveName }}</td>
                                <td class="text-center">@{{ employee.duration }}</td>
                                <td class="text-center">@{{ employee.totalDays }}</td>
                                <td class="text-center">@{{ employee.reason }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" value="@{{ employeesToFile | json}}" name="employeesToFile">
                    <input type="hidden" value='{{ Input::old("employeesToFile") }}' v-model="employeesToFileOld">
                    <div class="clear_10"></div>
                    <div class="col-md-12" style="margin-left: 30px;">
                        @if($method == "approve" && Session::get("desig_level") != "head")
                        <a class="btn btn-default btndefault" disabled>ADD</a>
                        <a class="btn btn-default btndefault" :disabled="! editable" data-toggle="modal" data-target="#EditLeaveDetails">EDIT</a>
                        <button class="btn btn-default btndefault" disabled>DELETE</button>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>
        @if($method == 'approve')
            @if(Session::get("desig_level") == "supervisor")
                @include("lrf.actions.immediate_superior")
            @else
                @include("lrf.actions.department_head")
            @endif
        @else
            @include("lrf.actions.view")
        @endif
        {{ Form::close() }}
        @include("lrf.modals.pe_view")
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/leaves/vue-pe-ISview.js') }}

@stop