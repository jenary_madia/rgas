@extends('template/header')

@section('content')
	{{ Form::open(array('url' => 'lrf/action', 'method' => 'post', 'files' => true,'id' => 'formCreateLRF')) }}
        <div class="form_container">
                <div class="container-header"><h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" id="lrfEmployeeName" name="lrfEmployeeName" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfEmployeeId" value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfDateFiled" value="{{ date('Y-m-d') }}"/>
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfCompany" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfStatus" value="NEW" />
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="lrfDepartment" value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="lrfContactNumber" maxlength="20" value="{{ Input::old('lrfContactNumber') }}"/>
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" maxlength="25" name="lrfSection" value="{{ Input::old('lrfSection',Session::get('sect_name')) }}" />
                        </div>
                    </div>

					<div class="clear_10"></div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">TYPE OF LEAVE:</label>
                        </div>
                        <div class="col2_form_container">
  							<select name="lrfLeaveType" class="form-control LRFLeaveType">
  								<option value="" selected></option>
    							@foreach ($leaveTypes as $leaveType)
                                    <option value="{{ $leaveType->item }}" {{ (Input::old("lrfLeaveType") == $leaveType->item ? "selected":"") }} >{{ $leaveType->text }}</option>
                                @endforeach ?>
  							</select>
                        </div>
                        @include('lrf.modals.home-visit-error')
                    </div>
                </div>
                <div class="clear_20"></div>
                <div class="container-header titleLeaveType"><label class="labels required">SELECT A LEAVE TYPE</label></div>
                <div class="LRFAdditionalFields"
               		 @if (Input::old("lrfLeaveType"))
	            		@if (Input::old("lrfLeaveType") == 'HomeVisit')
	            			hidden
	            		@endif
            		@else
            			hidden 
            		@endif
                	>
					<div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined"><strong>WORK WEEK SCHEDULE DETAILS</strong></h5></div>
					<div class="row">
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels required">TYPE OF SCHEDULE:</label>
	                        </div>
	                        <div class="col2_form_container">
                                <select name="lrfTypeOfSchedule" id="lrfTypeOfSchedule" class="form-control">
                                    <option value="compressed" {{ (Input::old("lrfTypeOfSchedule") == "compressed" ? "selected":"") }}>Compressed</option>
                                    <option value="regular"  {{ (Input::old("lrfTypeOfSchedule") == "regular" ? "selected":"") }}>Regular</option>
                                    <option value="special"  {{ (Input::old("lrfTypeOfSchedule") == "special" ? "selected":"") }}>Special</option>
                                </select>
	                        </div>
	                    </div>
	                    <div class="clear_10"></div>
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels labelRestday">REST DAY:</label>
	                        </div>
	                        <div class="col2_form_container">
                                <select name="lrfRestDay[]" id="lrfRestDay" multiple="multiple"
                                    {{ (Input::old('lrfTypeOfSchedule') ? (Input::old('lrfTypeOfSchedule') == 'special' ? "" : "disabled") : "disabled") }}
                                >
                                    @for ($i = 0; $i < 7; $i++)
                                        <option value="{{ $i }}"
                                        @if(Input::old('lrfRestDay'))
                                            {{ (in_array($i, Input::old('lrfRestDay')) == true ? "selected" : "") }}
                                                @else
                                            {{ ($i == 0 || $i == 6 ? "selected" : "" ) }}
                                                @endif
                                        >
                                            @if( $i == 0) Sunday
                                            @elseif( $i == 1) Monday
                                            @elseif( $i == 2) Tuesday
                                            @elseif( $i == 3) Wednesday
                                            @elseif( $i == 4) Thursday
                                            @elseif( $i == 5) Friday
                                            @elseif( $i == 6) Saturday
                                            @endif
                                        </option>
                                    @endfor
                                </select>
	                        </div>
	                    </div>
	                </div>
                </div>
             	<div class="clear_20"></div>
            <div id="divLeaveDetails" {{ (Input::old("lrfLeaveType") ? "" : "hidden")  }}>
                <div class="container-header"><h5 class="text-center lined"><strong>LEAVE DETAILS</strong></h5></div>
    				<div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DURATION OF LEAVE:</label>
                            </div>
                            <div class="col2_form_container">
      							<select name="lrfDurationLeave" id="lrfDurationLeave" onchange="ChooseDurationOfLeave(this)" class="form-control">
      								<option value="">--Choose Duration of Leave--</option>
      								<option value="half" {{ (Input::old("lrfDurationLeave") == "half" ? "selected":"") }}>Half Day</option>
      								<option value="whole" {{ (Input::old("lrfDurationLeave") == "whole" ? "selected":"") }}>One(1) Day</option>
      								<option value="multiple" {{ (Input::old("lrfDurationLeave") == "multiple" ? "selected":"") }}>Multiple Days</option>
      							</select>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col2_form_container">
                                @if(in_array(Session::get("employee_id"),json_decode(EXPATRIATES,true)))
                                    <a class="btn btn-default btn-xs" id="showLeaves" onClick="MyWindow=window.open('{{url('lrf/show_leaves/'.Session::get("employee_id").'/hv')}}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
                                @else
                                    <a class="btn btn-default btn-xs" id="showLeaves" onClick="MyWindow=window.open('{{url('lrf/show_leaves/'.Session::get("employee_id"))}}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
                                @endif
                            </div>
                        </div>
                        <div class="clear_10"></div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4 bordered">
                                    <label class="labels">FROM:</label>
                                    <br>
                                    <label class="labels required">DATE:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text" value="{{ Input::old('lrfDateFrom',date('Y-m-d'))}}" {{ Input::old('lrfDurationLeave',"disabled") }} class="date_picker form-control input-small" id="lrfDateFrom" name="lrfDateFrom"/>
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>

                                    <div class="clear_10"></div>
                                    <label class="labels required">LEAVE PERIOD:</label>
                                    <select name="lrfLeavePeriodFrom" id="lrfLeavePeriodFrom" onchange="computeTotalLeave()" class="form-control pull-right"
                                    @if (Input::old("lrfDurationLeave"))
                                        @if (Input::old("lrfDurationLeave") != "multiple" )
                                        disabled
                                        @endif
                                    @else
                                        disabled
                                    @endif
                                    >
                                        <option value="" {{ Input::old("lrfLeavePeriodFrom","selected") }}>--Choose Leave Period--</option>
                                        <option value="whole" {{ (Input::old("lrfLeavePeriodFrom") == "whole" ? "selected":"") }}>Whole</option>
                                        <option value="half" {{ (Input::old("lrfLeavePeriodFrom") == "half" ? "selected":"") }}>Half</option>
                                    </select>
                                </div>
                              <div class="col-md-1"></div>
                              <div class="col-md-4 divTo bordered"
                              @if (Input::old("lrfDurationLeave"))
                              @if (Input::old('lrfDurationLeave') != "multiple")
                              hidden
                              @endif
                              @else
                              hidden
                              @endif
                              >
                                <label class="labels">TO:</label>
                                <br>
                                <label class="labels required">DATE:</label>
                                  <div class="input-group bootstrap-timepicker timepicker">
                                      <input readonly="readonly" type="text" class="form-control date_picker" name="lrfDateTo" id="lrfDateTo"
                                             @if (Input::old("lrfDateTo"))
                                             value="{{ Input::old("lrfDateTo") }}"
                                             @else
                                             disabled
                                              @endif
                                      />
                                      <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                  </div>
                                <div class="clear_10"></div>
                                <label class="labels required">LEAVE PERIOD:</label>
                                <select name="lrfLeavePeriodTo" id="lrfLeavePeriodTo" onchange="computeTotalLeave()" class="form-control pull-right"

                                >
                                <option value="" {{ (Input::old("lrfLeavePeriodTo") ? "" : "selected") }}>--Choose Leave Period--</option>
                                <option value="whole" {{ (Input::old("lrfLeavePeriodTo") == "whole" ? "selected":"") }}>Whole</option>
                                <option value="half" {{ (Input::old("lrfLeavePeriodTo") == "half" ? "selected":"") }}>Half</option>
                                </select>
                              </div>
                              <div class="col-md-1"></div>
                            </div>
    					<div class="clear_20"></div>
    					<div class="clear_20"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">TOTAL LEAVE DAYS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input min="0" type="number" max="99"  readonly step="any" name="lrfTotalLeaveDays" value="{{ Input::old('lrfTotalLeaveDays') }}" id="lrfTotalLeaveDays" class="form-control"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
    					    <div class="textarea_messages_container" style="border: 0;">
    					        <div class="row">
    					            <label class="textarea_inside_label required">REASON:</label>
    					            <textarea rows="3" class="form-control textarea_inside_width" name="lrfReason">{{ Input::old('lrfReason') }}</textarea>
    					        </div>
    					    </div>
                        </div>
                    </div>
    				<div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
    				<div class="clear_20"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>FILER : </strong></p>
                            <div id="med_cert"
                                 @if(in_array(Input::old('lrfLeaveType'),['Mat/Pat','Sick']) && Input::old('lrfTotalLeaveDays') > 3 )

                                 @else
                                 hidden
                                @endif
                            >
                                <label class="attachment_note required">MEDICAL CERTIFICATE:
                                    <span class="btn btn-success btnbrowse fileinput-button">
                                        <span>BROWSE</span>
                                        <input id="med_fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                                    </span>
                                </label>
                                <div id="med_attachments">
                                    @if(Input::old("med_files"))
                                        {{--{{ var_dump(Input::old("files")) }}--}}
                                        @for ($i = 1; $i <= count(Input::old("med_files")); $i++)
                                            <p>
                                                {{ Input::old("med_files")[$i]["original_filename"] }} | {{ Input::old("med_files")[$i]["filesize"] }}
                                                <input type='hidden' name='med_files[{{ $i }}][random_filename]' value='{{ Input::old("med_files")[$i]["random_filename"] }}'>
                                                <input type='hidden' name='med_files[{{ $i }}][original_filename]' value='{{ Input::old("med_files")[$i]["original_filename"] }}'>
                                                <input type='hidden' class='attachment_filesize' name='med_files[{{ $i }}][filesize]' value='{{ Input::old("med_files")[$i]["filesize"] }}'>
                                                <input type='hidden' name='med_files[{{ $i }}][mime_type]' value='{{ Input::old("med_files")[$i]["mime_type"] }}'>
                                                <input type='hidden' name='med_files[{{ $i }}][original_extension]' value='{{ Input::old("med_files")[$i]["original_extension"] }}'>
                                                <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                            </p>
                                        @endfor
                                    @endif
                                </div>
                                <br>
                            </div>
                            <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                            <!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
                            <div id="attachments">
                                @if(Input::old("files"))
                                    {{--{{ var_dump(Input::old("files")) }}--}}
                                    @for ($i = 1; $i <= count(Input::old("files")); $i++)
                                        <p>
                                            {{ Input::old("files")[$i]["original_filename"] }} | {{ Input::old("files")[$i]["filesize"] }}
                                            <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files")[$i]["random_filename"] }}'>
                                            <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files")[$i]["original_filename"] }}'>
                                            <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files")[$i]["filesize"] }}'>
                                            <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files")[$i]["mime_type"] }}'>
                                            <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files")[$i]["original_extension"] }}'>
                                            <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                        </p>
                                    @endfor
                                @endif
                            </div>
                            <span class="btn btn-success btnbrowse fileinput-button">
                                <span>BROWSE</span>
                                <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                            </span>
                        </div>
                    </div>
        </div>
                
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>
            <div class="textarea_messages_container" >
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="lrfComment">{{ Input::old("lrfComment") }}</textarea>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="save">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SEND</strong>
                            @if(Session::get("desig_level") == "president")
                                <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO HR FOR PROCESSING
                            @else
                                <label class="button_notes"><strong>SEND</strong>TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                            @endif
                        </label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" id="btnSend" class="btn btn-default btndefault" name="lrfAction" value="send">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

    {{ Form::close() }}
@stop
@section('js_ko')
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });

        var med_file_counter = 0;
        var med_allowed_file_count = 1;
        var med_allowed_total_filesize = 20971520;


        var file_counter = 0;
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;
        var ownerId = '{{ Session::get("employee_id") }}';
        var company = '{{ Session::get("company") }}';
        var corporateCompanies = '{{ CORPORATE }}';
    </script>
    {{ HTML::script('/assets/js/jquery-ui-1.10.4.custom.js') }}
    {{ HTML::script('/assets/js/leaves/leaveCreateFunctions.js') }}
    {{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop