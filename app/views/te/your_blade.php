

<!--Upon creation / create.blade.php -->
<label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
<div class="attachment_container">
	<!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
	<div id="attachments"></div>
	<span class="btn btn-success btnbrowse fileinput-button">
		<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
	</span>

</div>
	
	
<!--Viewing of request / view.blade.php -->
<label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
	<div class="attachment_container">
		@if(count(json_decode($request['te_attachments'])) > 0)
			@foreach(json_decode($request['te_attachments']) as $attachment)
			<a href="{{ URL::to('/te/download/') . '/' . $request['te_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }}</a><br />
			@endforeach
		@endif
	</div>
	
