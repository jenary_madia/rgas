@extends('template.header')

@section('content')
    <div id="peMyViewEdit" v-cloak>
        {{ Form::open(array('url' => array('ns/pe_action',$notifDetails['id']), 'method' => 'post')) }}
            <div class="form_container">
                <h5 class="text-center"><strong>NOTIFICATION APPLICATION FORM</strong></h5>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT</label>
                        </div>
                        <div class="col2_form_container">
                            {{ Form::select('department',$departments, Input::old('department', $notifDetails->departmentid), ['class'=>'form-control otherFields','id'=>'schedule_typeOffset','v-model'=>'departmentID']) }}
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">REFERENCE NUMBER</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="reference" readonly value="{{ $notifDetails['documentcode'].'-'.$notifDetails['codenumber'] }}">
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION</label>
                        </div>

                        <input type="text" value="{{ Input::old('section',$notifDetails->sectionid) }}" v-model="sectionID" hidden>

                        <div class="col2_form_container">
                            <select class="form-control" :disabled="sections.length == 0" v-model="sectionID" name="section">
                                <option v-for="section in sections" value="@{{ section.id }}">@{{ section.sect_name }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled value="{{ $notifDetails->datecreated }}">
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center text-uppercase">Employee Name</th>
                                    <th class="text-center text-uppercase">Employee Number</th>
                                    <th class="text-center text-uppercase">Notification Type</th>
                                    <th class="text-center text-uppercase">Date</th>
                                    <th class="text-center text-uppercase">Reason</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="employee in employeesToFile"  v-bind:class="{ 'active' : employee.active }" @click="chooseEmployee($index,employee)">
                                    <td>@{{ employee.employeeName }}</td>
                                    <td>@{{ employee.employeeNumber }}</td>
                                    <td>@{{ employee.notifName }}</td>
                                    <td>@{{ employee.date }}</td>
                                    <td>@{{ employee.reason }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <input type="hidden" value="@{{ employeesToFile | json }}" name="employeesToFile">
                            <input type="hidden" v-model="employeesToFileOld" value='{{ Input::old("employeesToFile") }}'>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col-md-12" style="margin-left: 30px;">
                            <a class="btn btn-default btndefault" data-toggle="modal" :disabled="employees.length == 0" @click="clear" data-target="#addNotifDetails">ADD</a>
                            <a class="btn btn-default btndefault"  data-toggle="modal" :disabled="!editable" data-target="#editNotifDetails">EDIT</a>
                            <button type="submit" class="btn btn-default btndefault"  :disabled="!deletable" @click.prevent="deleteOffsetReference">DELETE</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

            <div class="clear_20"></div>
            @include("ns.actions.my_notifications.edit")
        {{ Form::close() }}
        @include("ns.modals.pe_create")
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/notification/vue-pe-edit.js') }}

@stop