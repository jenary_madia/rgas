<span class="action-label labels">ACTION</span>
<div class="form_container">
    <div class="clear_20"></div>
    <div class="textarea_messages_container">
        <div class="row">
            <label class="textarea_inside_label">MESSAGE:</label>
            <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$notification['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
        </div>
        <div class="clear_20"></div>
        <div class="row">
            <label class="textarea_inside_label">COMMENT:</label>
            <textarea rows="3" {{ (!$method ? "disabled" : "") }} class="form-control textarea_inside_width" name="comment"></textarea>
        </div>
    </div>

    <div class="clear_10"></div>
    <div class="row">
        <div class="comment_container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a class="btn btn-default btndefault" href="{{ url('/ns/notifications') }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- end of form_container -->