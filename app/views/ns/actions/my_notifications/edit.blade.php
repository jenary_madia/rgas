<div class="clear_20"></div>

<span class="action-label labels">ACTION</span>
<div class="form_container">
    <div class="clear_20"></div>
    @if($notifDetails['status'] == 'NEW')
        <div class="textarea_messages_container">
            <div class="clear_20"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" {{ ($method == 'view' ? "disabled" : "") }} class="form-control textarea_inside_width" name="comment">@foreach (explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            </div>
        </div>
    @else
        <div class="textarea_messages_container">
            <div class="row">
                <label class="textarea_inside_label">MESSAGE:</label>
                <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            </div>
            <div class="clear_20"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" {{ ($method == 'view' ? "disabled" : "") }} class="form-control textarea_inside_width" name="comment"></textarea>
            </div>
        </div>
    @endif
    <div class="clear_10"></div>
    <div class="row">
        <div class="comment_container">
            <div class="comment_notes">
                <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
            </div>
            <div class="comment_button">
                <button type="submit" class="btn btn-default btndefault" name="action" value="resave">SAVE</button>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="comment_container">
            <div class="comment_notes">
                @if(Session::get("desig_level") == "president")
                    <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO HR FOR PROCESSING
                        @else
                            <label class="button_notes"><strong>SEND</strong>TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                @endif
            </div>
            <div class="comment_button">
                <button type="submit" class="btn btn-default btndefault" name="action" value="resend" :disabled="notificationType == '' ">SEND</button>
            </div>
        </div>
    </div>
</div><!-- end of form_container -->