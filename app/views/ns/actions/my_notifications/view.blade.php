<div class="clear_20"></div>
<span class="action-label labels">ACTION</span>
<div class="form_container">
    <div class="clear_20"></div>
    @if($notifDetails['status'] == 'NEW')
        <div class="textarea_messages_container">
            <div class="clear_20"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" class="form-control textarea_inside_width" name="comment">@foreach (explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            </div>
        </div>
    @else
        <div class="textarea_messages_container">
            <div class="row">
                <label class="textarea_inside_label">MESSAGE:</label>
                <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            </div>
            <div class="clear_20"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" {{ ($notifDetails['status'] != 'FOR APPROVAL' ? "disabled" : "") }} class="form-control textarea_inside_width" name="comment"></textarea>
            </div>
        </div>
    @endif
    <div class="clear_10"></div>
    <div class="row">
        <div class="comment_container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <button value="cancel" name="action" class="btn btn-default btndefault" style="width:100px;" {{ $notifDetails['status'] == 'FOR APPROVAL' ? "" : "disabled" }}>Cancel Request</button>
                </div>
                <div class="col-md-6 text-center">
                    <a class="btn btn-default btndefault" href="{{ url('/ns/notifications') }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>