<span class="action-label labels">ACTION</span>
<div class="form_container">
    <div class="clear_20"></div>
    <div class="textarea_messages_container">
        <div class="row">
            <label class="textarea_inside_label">MESSAGE:</label>
            <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$notification['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
        </div>
        <div class="clear_20"></div>
        <div class="row">
            <label class="textarea_inside_label">COMMENT:</label>
            <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
        </div>
    </div>

    <div class="clear_10"></div>
    <div class="row">
        <div>
            <div class="comment_container">
                <div class="comment_notes">
                    <label class="button_notes pull-left" style=" margin: 5px 0 0 0"><strong>REQUEST</strong> FOR APPROVAL OF </label>
                    <select class="form-control pull-left" style="width:250px; margin: 0 0 0 50px;" name="otherSuperior">
                        <option selected disabled></option>
                        @foreach($approvers as $approver)
                            <option value="{{ $approver['id'] }}"> {{ $approver['firstname'].' '.$approver['middlename'].' '.$approver['lastname'].' ('.$approver['desig_level'].')' }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="comment_button">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="request">REQUEST</button>
                </div>
            </div>
            <div class="comment_container">
                <div class="comment_notes">
                    <label class="button_notes"><strong>APPROVED</strong> AND <strong>SEND</strong>TO HR FOR PROCESSING</label>
                </div>
                <div class="comment_button">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="toHR">SEND</button>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="comment_container">
                <div class="comment_notes">
                    <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
                </div>
                <div class="comment_button">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="return">RETURN</button>
                </div>
            </div>
        </div>
        <div class="comment_container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a class="btn btn-default btndefault" href="{{ url('/ns/notifications') }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- end of form_container -->