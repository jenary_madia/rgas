@extends('template.header')

@section('content')
    <div id="peISView" v-cloak>
        <div class="form_container">
            <h5 class="text-center"><strong>NOTIFICATION APPLICATION FORM</strong></h5>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">DEPARTMENT</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $notifDetails['department']['dept_name']}}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">REFERENCE NUMBER</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="reference" readonly value="{{ $notifDetails['documentcode'].'-'.$notifDetails['codenumber']}}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $notifDetails['section']['sect_name']}}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $notifDetails['datecreated']}}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center text-uppercase">Employee Name</th>
                                <th class="text-center text-uppercase">Employee Number</th>
                                <th class="text-center text-uppercase">Notification Type</th>
                                <th class="text-center text-uppercase">Date</th>
                                <th class="text-center text-uppercase">Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            <input type="hidden" value='{{ $notifDetails['batchDetails']}}' v-model="batchDetails">
                            <tr v-for="employee in employeesToFile" >
                                <td>@{{ employee.employeeName }}</td>
                                <td>@{{ employee.employeeNumber }}</td>
                                <td>@{{ employee.notifName }}</td>
                                <td>@{{ employee.date }}</td>
                                <td>@{{ employee.reason }}</td>
                            </tr>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clear_10"></div>
                </div>
            </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>
            @if($method == 'view')
                {{ Form::open(array('url' => array('ns/pe_action',$notifDetails['id']), 'method' => 'post')) }}
                    @include("ns.actions.my_notifications.view")
                {{ Form::close() }}
            @endif
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/notification/vue-pe-ISview.js') }}

@stop