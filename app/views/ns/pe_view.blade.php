@extends('template.header')

@section('content')
    <div id="peISView" v-cloak>
        {{ Form::open(array('url' => array('ns/submit',$notification['id']), 'method' => 'post', 'files' => true)) }}
            <div class="form_container">
                <h5 class="text-center"><strong>NOTIFICATION APPLICATION FORM</strong></h5>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DEPARTMENT</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled value="{{ $notification['department']['dept_name']}}">
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">REFERENCE NUMBER</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="reference" readonly value="{{ $notification['documentcode'].'-'.$notification['codenumber ']}}">
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled value="{{ $notification['section']['sect_name']}}">
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled value="{{ $notification['datecreated']}}">
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center text-uppercase">#</th>
                                        <th class="text-center text-uppercase">Employee Name</th>
                                        <th class="text-center text-uppercase">Employee Number</th>
                                        <th class="text-center text-uppercase">Notification Type</th>
                                        <th class="text-center text-uppercase">Date</th>
                                        <th class="text-center text-uppercase">Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="employee in employeesToFile"  v-bind:class="{ 'active' : employee.active }" @click="chooseEmployee($index,employee)">
                                        <td><input disabled type="checkbox" v-model="employee.noted"></td>
                                        <td>@{{ employee.employee_name }}</td>
                                        <td>@{{ employee.employee_number }}</td>
                                        <td>@{{ employee.notif.text }}</td>
                                        <td>@{{ employee.from_date }}</td>
                                        <td>@{{ employee.reason }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" value="@{{ employeesToFile | json}}" name="employeesToFile">
                        <input type="hidden" value='{{ Input::old("employeesToFile") }}' v-model="employeesToFileOld">
                        <div class="clear_10"></div>
                        @if($method == 'approve' && Session::get("desig_level") != "head")
                            <div class="col-md-12" style="margin-left: 30px;">
                                <a class="btn btn-default btndefault" disabled>ADD</a>
                                <a class="btn btn-default btndefault" :disabled="! editable" data-toggle="modal" data-target="#editNotifDetails">EDIT</a>
                                <button class="btn btn-default btndefault" disabled>DELETE</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div><!-- end of form_container -->

            <div class="clear_20"></div>
            @if($method == 'approve')
                @if(Session::get("desig_level") == "supervisor")
                    @include("ns.actions.immediate_superior")
                @else
                    @include("ns.actions.department_head")
                @endif
            @else
                @include("ns.actions.view")
            @endif
        {{ Form::close() }}
        @include("ns.modals.pe_view")
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/notification/vue-pe-ISview.js') }}

@stop