@extends('template.header')

@section('content')
    <div id="app">
        {{ Form::open(array('url' => array('ns/action',$notifDetails['id']), 'method' => 'post', 'files' => true)) }}
        <div class="form_container">
            <div class="container-header"><h5 class="text-center"><strong>NOTIFICATION APPLICATION FORM</strong></h5></div>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NAME:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="employeeName" value="{{ Session::get('employee_name') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REFERENCE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" value="{{ $notifDetails['documentcode'].'-'.$notifDetails['codenumber'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="employeeNumber" value="{{ Session::get('employeeid') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="dateFiled" value="{{ date('Y-m-d') }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">COMPANY:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="company" value="{{ Session::get('company') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">STATUS:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="status" value="NEW" />
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DEPARTMENT:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="department" value="{{ Session::get('dept_name') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">CONTACT NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="contactNumber" maxlength="20" value="{{ Input::old('contactNumber', $notifDetails['contact_no']) }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="section" value="{{ Input::old('section', $notifDetails['section']) }}" />
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">TYPE OF NOTIFICATION:</label>
                    </div>
                    <div class="col2_form_container">
                        {{ Form::select('notificationType', $notificationType, $notifDetails['noti_type'], ['v-model' => 'notificationType','id' => 'notificationType','class'=> 'form-control' ]) }}
                    </div>
                </div>
            </div>
            @include("ns.layouts.notifications.edit.undertime")
            @include("ns.layouts.notifications.edit.cut_time")
            @include("ns.layouts.notifications.edit.official_business")
            @include("ns.layouts.notifications.edit.offset")
            @include("ns.layouts.notifications.edit.time_keeping_correction")
            <div class="clear_20"></div>

            <div v-if="notificationType">
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        <p><strong>FILER : </strong></p>
                        <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                        <?php $count = 1 ?>
                        <div id="attachments">
                            @for ($i = 1; $i < 6; $i++)
                                @if(json_decode($notifDetails["attach$i"],true) == true)
                                    <p>
                                        {{ json_decode($notifDetails["attach$i"],true)["original_filename"] }} | {{ json_decode($notifDetails["attach$i"],true)["filesize"] }}KB
                                        <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ json_decode($notifDetails["attach$i"],true)["random_filename"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ json_decode($notifDetails["attach$i"],true)["original_filename"] }}'>
                                        <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ json_decode($notifDetails["attach$i"],true)["filesize"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ json_decode($notifDetails["attach$i"],true)["mime_type"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ json_decode($notifDetails["attach$i"],true)["original_extension"] }}'>
                                        <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                        <?php $count++ ?>
                                    </p>
                                @endif
                            @endfor

                        </div>
							<span class="btn btn-success btnbrowse fileinput-button">

                                <span>BROWSE</span>
                                <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                            </span>
                    </div>
                    @if($notifDetails['receiver_attachments'])
                        <div class="col-md-6">
                            <p><strong>CHRD : </strong></p>
                            @foreach(json_decode($notifDetails['receiver_attachments'],true) as $key)
                                <a href="{{ URL::to('/ns/download'.'/'.$notifDetails['documentcode'].'-'.$notifDetails['codenumber'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key["original_filename"])) }}">{{ $key["original_filename"] }}</a><br />
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>


        </div><!-- end of form_container -->
            @if($method == 'edit')
                @include("ns.actions.my_notifications.edit")
            @endif

        {{ Form::close() }}
        @include("ns.modals.offset")
    </div>

@stop
@section('js_ko')
    <script>
        var dataURL = '{{ url('/ns/offset_details') }}';
        var notif_id = {{ $notifDetails['id'] }};
        var	file_counter = {{ $count }};
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });

    </script>
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-edit.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop