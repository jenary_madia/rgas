@extends('template/header')

@section('content')
    <div id="app" v-cloak>
            {{ Form::open(array('url' => 'ns/action', 'method' => 'post', 'files' => true)) }}
                <div class="form_container">
                    <div class="container-header"><h5 class="text-center"><strong>NOTIFICATION APPLICATION FORM</strong></h5></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeName" value="{{ Session::get('employee_name') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeNumber" value="{{ Session::get('employeeid') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="" value=""/>
                                <input  readonly="readonly" type="hidden" class="form-control" name="dateFiled" value="{{ date('Y-m-d') }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="company" value="{{ Session::get('company') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="status" value="NEW" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="department" value="{{ Session::get('dept_name') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" name="contactNumber" maxlength="20" value="{{ Input::old('contactNumber') }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" name="section" maxlength="25" value="{{ Input::old('section', Session::get('sect_name')) }}" />
                            </div>
                        </div>

                        <div class="clear_10"></div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">TYPE OF NOTIFICATION:</label>
                            </div>
                            <div class="col2_form_container">
                                {{ Form::select('notificationType', $notificationType, Input::old('notificationType', ''), ['v-model' => 'notificationType','id' => 'notificationType','class'=> 'form-control' ]) }}
                            </div>
                        </div>
                    </div>
                    <div class="clear_20"></div>
                    <div class="container-header titleLeaveType" v-if="! notificationType"><label class="labels required">SELECT A NOTIFICATION TYPE</label></div>
                    @include("ns.layouts.undertime")
                    @include("ns.layouts.cut_time")
                    @include("ns.layouts.official_business")
                    @include("ns.layouts.offset")
                    @include("ns.layouts.time_keeping_correction")
                    <div class="clear_20"></div>
                    <div v-show="notificationType">
                        <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                        <div class="clear_20"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <p><strong>FILER : </strong></p>
                                <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                                <!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
                                <div id="attachments">
                                    @if(Input::old("files"))
                                        @for ($i = 1; $i <= count(Input::old("files")); $i++)
                                            <p>
                                                {{ Input::old("files")[$i]["original_filename"] }} | {{ Input::old("files")[$i]["filesize"] }}
                                                <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files")[$i]["random_filename"] }}'>
                                                <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files")[$i]["original_filename"] }}'>
                                                <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files")[$i]["filesize"] }}'>
                                                <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files")[$i]["mime_type"] }}'>
                                                <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files")[$i]["original_extension"] }}'>
                                                <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                            </p>
                                        @endfor
                                    @endif
                                </div>
                                <span class="btn btn-success btnbrowse fileinput-button">
                                    <span>BROWSE</span>
                                    <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                                </span>
                            </div>
                        </div>
                    </div>


                </div><!-- end of form_container -->

                <div class="clear_20"></div>

                <span class="action-label labels">ACTION</span>
                <div class="form_container">
                    <div class="clear_20"></div>
                    <div class="textarea_messages_container" >
                        <div class="row">
                            <label class="textarea_inside_label">COMMENT:</label>
                            <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="comment_container">
                            <div class="comment_notes">
                                <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                            </div>
                            <div class="comment_button">
                                <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="comment_container">
                            <div class="comment_notes">
                                @if(Session::get("desig_level") == "president")
                                    <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO HR FOR PROCESSING
                                        @else
                                            <label class="button_notes"><strong>SEND</strong>TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                                @endif
                            </div>
                            <div class="comment_button">
                                <button type="submit" class="btn btn-default btndefault" name="action" value="send" :disabled="notificationType == '' ">SEND</button>
                            </div>
                        </div>
                    </div>
                </div><!-- end of form_container -->
            {{ Form::close() }}
            @include("ns.modals.offset")
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/notification/vue-scripts.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
        var file_counter = 0;
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;
    </script>
    {{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop
