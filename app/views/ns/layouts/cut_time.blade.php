<div v-if="notificationType == 'cut_time'" class="hello">
	<div class="clear_20"></div>
	<div class="container-header"><h5 class="text-center lined"><strong>CUT-TIME DETAILS</strong></h5></div>
	<div class="row">
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">TOTAL NUMBER OF DAYS:</label>
	        </div>
	        <div class="col2_form_container">
				<input type="number" class="form-control" name="CTTotalDays"  min="0" value="{{ Input::old("CTTotalDays") }}">
	        </div>
	    </div>
	    <div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">TOTAL NUMBER OF HOURS:</label>
	        </div>
	        <div class="col2_form_container">
				<input type="number" class="form-control" name="CTTotalHours"  min="0" value="{{ Input::old("CTTotalHours") }}">
	        </div>
	    </div>



		<div class="clear_10"></div>
		<div class="row">
		  <div class="col-md-4 from bordered">
		    <label class="labels">FROM:</label>
		    <br>
		    <label class="labels required">DATE:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" v-model="cutTime.dateFrom" readonly class="date_picker form-control input-small" name="CTDateFrom" value="{{ Input::old("CTDateFrom") }}">
			    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		    <div class="clear_10"></div>
		    <label class="labels required">TIME IN/OUT:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" readonly class="time_picker form-control input-small" name="CTInOutFrom" value="{{ Input::old("CTInOutFrom") }}">
			    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			</div>
		  </div>
		  <div class="col-md-4 divTo to bordered">
		    <label class="labels">TO:</label>
		    <br>
		    <label class="labels required">DATE:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" v-model="cutTime.dateTo" readonly class="date_picker form-control input-small" name="CTDateTo" value="{{ Input::old("CTDateTo") }}">
			    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		    <div class="clear_10"></div>
		    <label class="labels required">TIME IN/OUT:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" readonly class="time_picker form-control input-small" name="CTInOutTo" value="{{ Input::old("CTInOutTo") }}">
			    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			</div>
		  </div>
		  <div class="col-md-1"></div>
		</div>
	    <div class="clear_10"></div>
		<div class="row3_form_container">
			<div class="col1_form_container">
				<label class="labels required">REASON:</label>
			</div>
			<div class="textarea_form_container">
				<textarea rows="3" class="form-control textarea_inside_width" name="CTReason">{{ Input::old("CTReason") }}</textarea>
			</div>
		</div>
	</div>
</div>