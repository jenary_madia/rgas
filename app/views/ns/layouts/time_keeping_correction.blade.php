<div v-if="notificationType == 'TKCorction'">
	<div class="clear_20"></div>
	<div class="container-header"><h5 class="text-center lined"><strong>TIME KEEPING CORRECTION DETAILS</strong></h5></div>
	<div class="row">
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">DATE:</label>
	        </div>
	        <div class="col2_form_container">
				<div class="input-group bootstrap-timepicker timepicker">
				    <input type="text" readonly class="date_picker form-control input-small" name="TKCDate" value="{{ Input::old("TKCDate") }}">
				    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
	        </div>
	    </div>
	    <div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required"> TO CORRECT:</label>
	        </div>
	        <div class="col2_form_container">
				{{ Form::select('toCorrect', array('no_time_in' => 'NO TIME-IN','no_time_out' => 'NO TIME-OUT', 'both' => 'BOTH'), Input::old('toCorrect', ''), ['class'=>'form-control noselectAll','v-model'=>'timeKeeping.toCorrect']) }}
	        </div>
	    </div>
		<div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
				<label class="labels required">TIME IN:</label>
	        </div>
	        <div class="col2_form_container">
				<div class="input-group bootstrap-timepicker timepicker">
				    <input type="text" name="TKCTimeIn" readonly class="time_picker form-control input-small offsetTimeIn" v-model="timeKeeping.timeIn" value="{{ Input::old("TKCTimeIn") }}" :disabled="timeKeeping.toCorrect == 'no_time_out' || ! timeKeeping.toCorrect">
				    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
				</div>
			</div>
	    </div>
		<div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
				<label class="labels required">TIME OUT:</label>
	        </div>
	        <div class="col2_form_container">
				<div class="input-group bootstrap-timepicker timepicker">
				    <input type="text" name="TKCTimeOut" readonly class="time_picker form-control input-small offsetTimeOut" v-model="timeKeeping.timeOut" value="{{ Input::old("TKCTimeOut") }}" :disabled="timeKeeping.toCorrect == 'no_time_in' || ! timeKeeping.toCorrect">
				    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
				</div>
			</div>
	    </div>
		<div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
				<label class="labels required"></label>
	        </div>
	        <div class="col2_form_container">
				<input type="checkbox" name="salaryAdjustment" value="1"> <label class="labels">SALARY ADJUSTMENT</label>
	    	</div>
	    </div>
	   
	    <div class="clear_10"></div>
		<div class="row3_form_container">
			<div class="col1_form_container">
				<label class="labels required">REASON:</label>
			</div>
			<div class="textarea_form_container">
				<textarea rows="3" class="form-control textarea_inside_width" name="TKCreason">{{ Input::old('TKCreason') }}</textarea>
			</div>
		</div>
	</div>

</div>