<div v-show="notificationType == 'undertime'" class="divNotifType">
	<div class="clear_20"></div>
	<div class="container-header"><h5 class="text-center lined"><strong>WORK WEEK SCHEDULE DETAILS</strong></h5></div>
	<div class="row">
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">SCHEDULE TYPE:</label>
	        </div>
	        <div class="col2_form_container">
				{{ Form::select('UTschedule_type', array('compressed' => 'Compressed','regular' => 'Regular', 'special' => 'Special'), Input::old('schedule_type', 'compressed'), ['class'=>'form-control otherFields','id'=>'schedule_typeUndertime', 'v-model'=>'undertime.scheduleType']) }}
			</div>
	    </div>
	    <div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required"> DAY:</label>
	        </div>
	        <div class="col2_form_container">
				<select v-model="undertime.day" name="UTday[]" id="UTDay" multiple="multiple" class="form-control" :disabled="undertime.scheduleType != 'special'"
						{{ (Input::old('UTschedule_type') ? (Input::old('UTschedule_type') == 'special' ? "" : "disabled") : "disabled") }}
				>
					@for ($i = 0; $i < 7; $i++)
						<option value="{{ $UTRestDays[$i] }}"
                            @if(Input::old('UTschedule_type'))
                                @if(Input::old('UTschedule_type') == "special")
                                    {{ (in_array($UTRestDays[$i], Input::old('UTday')) == true ? "selected" : "") }}
                                @elseif(Input::old('UTschedule_type') == "compressed")
                                    {{ ($i == 0 || $i == 6 ? "selected" : "")}}
                                @elseif(Input::old('UTschedule_type') == "regular")
                                    {{ ($i == 0 ? "selected" : "")}}
                                @endif
                            @else
                                {{ ($i == 0 || $i == 6 ? "selected" : "")}}
                            @endif
                        >
                            {{ $UTRestDays[$i] }}
						</option>
					@endfor
				</select>
			</div>
	    </div>
	    <div class="clear_10"></div>
    	<div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">SCHEDULE TIME OUT:</label>
	        </div>
	        <div class="col2_form_container">
				<div class="input-group bootstrap-timepicker timepicker">
				    <input v-model="undertime.scheduleTimeout" type="text" readonly class="time_picker form-control input-small" id="schedule_time_out" name="UTschedule_time_out" :disabled="undertime.scheduleType != 'special'">
				    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
				</div>
	        </div>
	    </div>
	</div>
	<div class="clear_20"></div>
	<div class="container-header"><h5 class="text-center lined"><strong>UNDERTIME DETAILS</strong></h5></div>
	<div class="row">
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">DATE:</label>
	        </div>
	        <div class="col2_form_container">
				<div class="input-group bootstrap-timepicker timepicker">
				    <input v-model="undertime.date" type="text" name="UTdate" id="UTdate" readonly class="date_picker form-control input-small">
				    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
	        </div>
	    </div>
	    <div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required"> TIME OUT:</label>
	        </div>
	        <div class="col2_form_container">
				<div class="input-group bootstrap-timepicker timepicker">
				    <input v-model="undertime.timeout" type="text" readonly class="time_picker form-control input-small" id="time_out" name="UTtime_out" value="{{ Input::old('UTtime_out') }}">
				    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
				</div>
	        </div>
	    </div>
	    <div class="clear_10"></div>

        <div class="row3_form_container">
			<div class="col1_form_container">
				<label class="labels required">REASON:</label>
			</div>
			<div class="textarea_form_container">
				<textarea rows="3" class="form-control" name="UTreason">{{ Input::old('UTreason') }}</textarea>
			</div>
        </div>
	</div>
</div>





