<div v-if="notificationType == 'official'">
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>OFFICIAL BUSINESS DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">OB DURATION:</label>
            </div>
            <div class="col2_form_container">
                <input disabled type="text" readonly class="form-control text-uppercase" value="{{ $notifDetails['duration_type'] }}">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="col-md-4 from bordered">
                <label class="labels">FROM:</label>
                <br>
                <label class="labels required">DATE:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" readonly class="date_picker form-control input-small" value="{{ $notifDetails['from_date'] }}" name="OBfrom_date" v-model="officialBusiness.dateFrom">
                    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">START TIME:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" value="{{ $notifDetails["from_inout"] }}" readonly name="OBfromStartTime" id="fromStartTime" class="time_picker form-control input-small">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">END TIME:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" value="{{ $notifDetails["from_out"] }}" readonly name="OBfromEndTime" id="fromEndTime" class="time_picker form-control input-small">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
            </div>
            <div class="col-md-4 divTo to bordered" {{ ($notifDetails['duration_type'] == "multiple" ? "" : "hidden") }}>
                <label class="labels">TO:</label>
                <br>
                <label class="labels required">DATE:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" readonly class="date_picker form-control input-small" value="{{ $notifDetails['to_date'] }}" name="OBto_date" v-model="officialBusiness.dateTo">
                    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">START TIME:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" value="{{ $notifDetails["to_inout"] }}" readonly name="OBtoStartTime" id="toStartTime" class="time_picker form-control input-small">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">END TIME:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" value="{{ $notifDetails["to_out"] }}" readonly name="OBtoEndTime" id="toEndTime" class="time_picker form-control input-small">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">TOTAL OB DAYS:</label>
            </div>
            <div class="col2_form_container">
                <input disabled type="text" class="form-control" value="{{ $notifDetails['totaldays'] }}" readonly name="OBtotalDays">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row3_form_container">
            <div class="col1_form_container">
                <label class="labels required">REASON:</label>
            </div>
            <div class="col2_form_container">
                <textarea disabled rows="3" style="width:720px" class="form-control textarea_inside_width" name="OBreason">{{ $notifDetails['reason'] }}</textarea>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">DESTINATION:</label>
            </div>
            <div class="col2_form_container">
                <textarea disabled rows="3" style="width:720px" class="form-control textarea_inside_width" name="OBdestination">{{ $notifDetails['destination'] }}</textarea>
            </div>
        </div>
    </div>
</div>

