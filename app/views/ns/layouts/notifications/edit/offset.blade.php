<div v-show="notificationType == 'offset'">
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>OFFSET DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">SCHEDULE TYPE:</label>
            </div>
            <div class="col2_form_container">
                {{ Form::select('schedule_type', array('' => '--Choose Schedule Type--', 'compressed' => 'Compressed','regular' => 'Regular', 'special' => 'Special'), Input::old('schedule_type', $notifDetails['schedule_type']), ['class'=>'form-control otherFields','id'=>'schedule_typeOffset','v-model'=>'offsetScheduleType']) }}
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> DURATION OF OFFSET:</label>
            </div>
            <div class="col2_form_container">
                {{ Form::select('durationType', array('' => '--Select Duration of Offset--', 'half' => 'HALF DAY','one' => 'ONE (1) DAY', 'multiple' => 'MULTIPLE DAYS'), Input::old('durationType', $notifDetails['duration_type']), ['class'=>'form-control otherFields noselectAll durationType','v-model'=>'offsetDuration']) }}
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row">
            <div class="col-md-4 from bordered">
                <label class="labels">FROM:</label>
                <br>
                <label class="labels required">OFFSET DATE:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input type="text" value="{{ Input::old("dateFrom",$notifDetails['from_date']) }}" readonly name="dateFrom" id="dateFrom" v-model="offsetDateFrom" class="date_picker form-control input-small">
                    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET PERIOD:</label>
                {{ Form::select('periodFrom', array("" => "--Choose Leave Period--","morning" => "MORNING","afternoon" => "AFTERNOON","whole" => "WHOLE"), Input::old('periodFrom', $notifDetails['from_absentperiod']), ['class'=>'form-control pull-right','v-model'=>'offsetPeriodFrom','id'=>'periodFrom']) }}
                <div class="clear_10"></div>
                <label class="labels required">OFFSET HOURS:</label>
                <input name="hoursFrom" value="{{ Input::old('hoursFrom',$notifDetails['from_absenthours']) }}" type="number" max="99"  class="form-control" v-model="offsetHoursFrom"/>
            </div>
            <div class="col-md-4 divTo to bordered"
                 @if(Input::old('durationType', $notifDetails['duration_type']))
                 @if(Input::old('durationType', $notifDetails['duration_type']) != "multiple")
                 hidden
                 @endif
                 @else
                 hidden
                    @endif

            >
                <label class="labels">TO:</label>
                <br>
                <label class="labels required">OFFSET DATE:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input type="text" readonly name="dateTo" value="{{ Input::old('dateTo',$notifDetails['to_date']) }}" id="dateTo" v-model="offsetDateTo" class="date_picker form-control input-small">
                    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET PERIOD:</label>
                {{ Form::select('periodTo', array("" => "--Choose Leave Period--","morning" => "MORNING","afternoon" => "AFTERNOON","whole" => "WHOLE"), Input::old('periodTo', $notifDetails['to_absentperiod']), ['class'=>'form-control pull-right','v-model'=>'offsetPeriodTo','id'=>'periodTo']) }}
                <div class="clear_10"></div>
                <label class="labels required">OFFSET HOURS:</label>
                <input name="hoursTo" value="{{ Input::old('hoursTo',$notifDetails['to_absenthours']) }}" type="number" max="99"  class="form-control" v-model="offsetHoursTo"/>
            </div>
            <div class="col-md-1"></div>
        </div>


        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> TOTAL OFFSET DAYS:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" name="totalDays" id="totalDays" class="form-control" readonly value="@{{ getTotalDays }}">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> TOTAL OFFSET HOURS:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control" name="totalHours"
                            value="@{{ getOffsetTotalHours }}"
                       readonly>
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row3_form_container">
            <div class="col1_form_container">
                <label class="labels required">REASON:</label>
            </div>
            <div class="textarea_form_container">
                <textarea rows="3" class="form-control textarea_inside_width" name="reason">{{ Input::old('reason',$notifDetails['reason']) }}</textarea>
            </div>
        </div>
    </div>
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>TIME OFFSET REFERENCE</strong></h5></div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td>OT/OB DATE</td>
                    <td>TYPE</td>
                    <td>TIME IN/START TIME</td>
                    <td>TIME OUT/END TIME</td>
                    <td>OT/OB DURATION</td>
                    <td>REASON</td>
                    <td>OB DESTINATION</td>
                </tr>
                </thead>
                <tbody>
                    <tr v-for="LOR in listOffsetReferences" v-bind:class="{ 'active' : LOR.active }" @click="chooseOffsetReference(LOR,$index)">
                        <td>@{{ LOR.OTDate }}</td>
                        <td>@{{ LOR.type }}</td>
                        <td>@{{ LOR.timeInStartTime }}</td>
                        <td>@{{ LOR.timeOutEndTime }}</td>
                        <td>@{{ LOR.OTDuration }}</td>
                        <td>@{{ LOR.reason }}</td>
                        <td>@{{ LOR.OBDestination }}</td>
                    </tr>
                </tbody>
                <input type="hidden" value="@{{ listOffsetReferences | json }}" id="username" name="listReference">
                <input type="hidden" v-model="listOffsetReferenceInputOld" value='{{ Input::old("listReference") }}'>
            </table>
        </div>
        <div class="col-md-12">
            <a class="btn btn-default btndefault" v-on:click="clearData" data-toggle="modal" data-target="#offsetAddModal">ADD</a>
            <a class="btn btn-default btndefault" :disabled="! editable" data-toggle="modal" data-target="#offsetEditModal" >EDIT</a>
            <button class="btn btn-default btndefault offsetDelete" :disabled="!deletable" @click.prevent="deleteOffsetReference">DELETE</button>
        </div>
        <div class="clear_20"></div>
        <div class="col-md-3">
            <label class="labels required"> TOTAL NUMBER OF HOURS:</label>
        </div>
        <div class="col-md-1">
            <input type="text" name="totalListOffsetHours" class="form-control" readonly value="@{{ getTotalListOffsetHours }}">
        </div>
    </div>
</div>