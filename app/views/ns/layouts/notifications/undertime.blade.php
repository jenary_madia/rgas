<div>
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>WORK WEEK SCHEDULE DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">SCHEDULE TYPE:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control" value="{{ $notifDetails["schedule_type"] }}" disabled>
            </div>
        </div>

            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels"> DAY:</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control"  value="{{ implode(' & ',json_decode($notifDetails["undertime_day"],true)) }}"
                    disabled>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">SCHEDULE TIME OUT:</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" value="{{ $notifDetails["schedule_time"] }}" disabled>
                </div>
            </div>
    </div>
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>UNDERTIME DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">DATE:</label>
            </div>
            <div class="col2_form_container">
                <div class="col2_form_container">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text" class="form-control" value="{{ $notifDetails["from_date"] }}" disabled>
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels"> TIME OUT:</label>
            </div>
            <div class="col2_form_container">
                <div class="input-group bootstrap-timepicker timepicker">
                    <input type="text" class="form-control" value="{{ $notifDetails["to_inout"] }}" disabled>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row3_form_container">
            <div class="col1_form_container">
                <label class="labels required">REASON:</label>
            </div>
            <div class="textarea_form_container">
                <textarea rows="3" class="form-control textarea_inside_width" name="reason" disabled>{{ $notifDetails['reason'] }}</textarea>
            </div>
        </div>
    </div>
</div>





