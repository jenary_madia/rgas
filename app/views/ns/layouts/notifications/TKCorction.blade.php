<div v-if="notificationType == 'TKCorction'">
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>TIME KEEPING CORRECTION DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">DATE:</label>
            </div>
            <div class="col2_form_container">
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" readonly class="date_picker form-control input-small" name="TKCDate" value="{{ $notifDetails['from_date'] }}">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> TO CORRECT:</label>
            </div>
            <div class="col2_form_container">
                <input disabled type="text" readonly class="form-control input-small" name="" value="{{ $notifDetails['toCorrect']['text'] }}">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">TIME IN:</label>
            </div>
            <div class="col2_form_container">
                <input disabled type="text" readonly class="form-control input-small" name="" value="{{ $notifDetails['from_inout'] }}">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">TIME OUT:</label>
            </div>
            <div class="col2_form_container">
                <div class="input-group bootstrap-timepicker timepicker">
                    <input disabled type="text" name="TKCTimeOut" readonly class="time_picker form-control input-small offsetTimeOut" value="{{ $notifDetails['to_inout'] }}">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"></label>
            </div>
            <div class="col2_form_container">
                <input type="checkbox" name="salaryAdjustment" value="1" disabled {{ ($notifDetails['salary_adjustment'] ? "checked" : "" ) }}> <label class="labels">SALARY ADJUSTMENT</label>
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row3_form_container">
            <div class="col1_form_container">
                <label class="labels required">REASON:</label>
            </div>
            <div class="textarea_form_container">
                <textarea disabled rows="3" class="form-control textarea_inside_width" name="TKCreason">{{ $notifDetails['reason'] }}</textarea>
            </div>
        </div>
    </div>

</div>