@extends('template/header')

@section('content')
    <div id="wrap">
        <div class="clear_20"></div>
        <div class="wrapper">
            {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMOC')) }}
            <table id="myMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMOC">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : center" role="columnheader" rowspan="1" colspan="5">My PSR/MOC</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            {{ Form::close() }}
        </div>

        <div class="clear_20"></div>
        <div class="wrapper">
            <table id="forEndorsementMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forEndorsementMOC">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ENDORSEMENT</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
        </div>
        @if(Session::get('is_div_head') || in_array(Session::get('desig_level'),['admin-aom','plant-aom']))
            <div class="clear_20"></div>
            <div class="wrapper">
                <table id="forApprovalMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApprovalMOC">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR APPROVAL</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        @endif
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/moc/moc_datatables.js') }}
@stop
