<!doctype html>
<html>
<head>
    <title>{{-- Rebisco General Approval System --}}</title>

@section('head')

    <!-- General Style -->
        {{ Minify::stylesheet(
            array('/assets/css/bootstrap.css'
                 ,'/assets/css/metro-bootstrap.css'
                 ,'/assets/css/main.css'
                 ,'/assets/css/employee.css'
                 ,'/assets/css/validation_engine.css'
                 ,'/assets/css/datepicker.css'
                 ,'/assets/css/jquery-ui-1.10.4.custom.min.css'
                 ,'/assets/css/jquery.dataTables_themeroller.css'
                 ,'/assets/css/chosen.min.css'
                 ,'/assets/css/bootstrap-timepicker.min.css'
                 ,'/assets/css/bootstrap-multiselect.css'
                 ,'/assets/css/new_style.css'
            ))->withFullUrl()
        }}

        <base href="<?php echo $base_url; ?>" />

    @show
    <link href="<?php echo $base_url; ?>/assets/css/font-awesome/css/font-awesome.css" rel="stylesheet">
    {{--<link href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />--}}
</head>
    <body>
        <div class="container">
            <div id="moc_edit">
            {{ Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
            <div class="form_container msr-form-container">
                <div class="container-header"><h5 class="text-center"><strong>{{ MOC_FORM_TITLE }}</strong></h5></div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="row employee-details">
                    <div class="col-md-6">
                        <label class="labels required pull-left">EMPLOYEE NAME:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ $mocDetails['firstname'].' '.$mocDetails['middlename'].' '.$mocDetails['lastname'] }}" />
                    </div>
                    <div class="col-md-6 pull-left">
                        <label class="labels required">REFERENCE NUMBER:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" value="{{ $mocDetails['transaction_code'] }}"/>
                    </div>
                    <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ $mocDetails['employeeid'] }}" />
                    </div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">DATE FILED:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" value="{{ $mocDetails['date'] }}"/>
                    </div>
                    <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">COMPANY:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ $mocDetails['company'] }}" />
                    </div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">STATUS:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="{{ $mocDetails['status'] }}" />
                    </div>
                    <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">DEPARTMENT:</label>
                        <input value="{{ json_decode($mocDetails['dept_sec'])[0] }}" readonly="readonly" type="text" class="form-control pull-right" name="department" />
                    </div>
                    <div class="col-md-6">
                        <label class="labels">CONTACT NUMBER:</label>
                        <input value="{{ $mocDetails['contact_no'] }}" disabled type="text" class="form-control pull-right" name="contactNumber" maxlength="20" />
                    </div>
                    <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels pull-left">SECTION:</label>
                        <input value="{{ json_decode($mocDetails['dept_sec'])[1] }}" disabled type="text" class="form-control pull-right" name="section" maxlength="25" />
                    </div>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="container-header"><h5 class="text-center lined"><strong>PSR/MOC DETAILS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="row project-requirements">
                    <div class="col-md-12">
                        <div class="moc-sidebar">
                            <label class="labels required pull-left">TYPE OF REQUEST:</label>
                            <br>
                            <ul class="moc-list list-unstyled">
                                @if($requestTypes)
                                    @foreach($requestTypes as $requestType)
                                        @foreach($mocDetails['rcodes'] as $key)
                                            <li class="text-uppercase"><input disabled type="checkbox" {{ ($key->request_code == $requestType->item ? 'checked' : $key->request_desc) }} value="{{ $requestType->item }}"> {{ $requestType->text }}</li>
                                        @endforeach
                                    @endforeach
                                @endif
                            </ul>
                            <label class="labels required pull-left">URGENCY:</label>
                            <input value="{{ $mocDetails['urgency'] }}" readonly="readonly" type="text" class="form-control pull-right"/>
                            </select>
                        </div>
                        <div class="moc-main">
                            <label class="labels pull-left">FOR REQUEST ON <span>PROCESS DESIGN/IMPROVEMENT</span> AND <span>REVIEW OF CHANGE</span></label>
                            <div class="moc-inner-sidebar">
                                <label class="labels pull-left">CHANGE IN:</label>
                                <br>
                                <ul class="list-unstyled moc-list">
                                    @if($changeIn)
                                        @foreach($changeIn as $change)
                                            @if(count($mocDetails['ccodes']) > 0)
                                                @foreach($mocDetails['ccodes'] as $key)
                                                    @if($change->item == "others")
                                                        <li class="text-uppercase"><input disabled type="checkbox"  v-model="mocDetails.changeIn" {{ ($key->changein_code == $change->item ? 'checked' : '') }} value="{{ $change->item }}"> {{ $change->text }} <input disabled style="width : 140px; text-transform: none !important;" value="{{ $key->changein_desc }}" type="text" class="pull-right form-control"></li>
                                                    @else
                                                        <li class="text-uppercase"><input disabled type="checkbox" v-model="mocDetails.changeIn" {{ ($key->changein_code == $change->item ? 'checked' : '') }} value="{{ $change->item }}"> {{ $change->text }}</li>
                                                    @endif
                                                @endforeach
                                            @else
                                                @if($change->item == "others")
                                                    <li class="text-uppercase"><input disabled type="checkbox"  v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }} <input disabled style="width : 140px; text-transform: none !important;" value="{{ $key->changein_desc }}" type="text" class="pull-right form-control"></li>
                                                @else
                                                    <li class="text-uppercase"><input disabled type="checkbox" v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }}</li>
                                                @endif
                                            @endif

                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div class="moc-inner-main">
                                <div class="moc-inner-sidebar">
                                    <label class="labels pull-left">CHANGE TYPE:</label>
                                    <input value="{{ $mocDetails['change_type'] }}" readonly="readonly" type="text" class="form-control pull-right"/>
                                </div>
                                <div class="moc-inner-main">
                                    <label class="labels pull-right">TARGET IMPLEMENTATION DATE:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input disabled value="{{ ($mocDetails['implementation_date'] == '0000-00-00' ? '' : $mocDetails['implementation_date']) }}" type="text" name="" id="UTdate" readonly class="date_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                                <label style="margin-top: 10px" class="labels pull-left">DEPARTMENTS INVOLVED:</label>
                                <div class="clearfix"></div>
                                <div class="moc-whole" style="border : 1px solid #ccc;">
                                    <ol class="moc-list">
                                        @if($dept_involved)
                                            @foreach($dept_involved as $key)
                                                <li>{{ $key->dept_code .' - '. $key->dept_name }}</li>
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <label class="labels required pull-left" style="padding-top: 7px">TITLE/SUBJECT:</label>
                        <input value="{{ $mocDetails['title'] }}" disabled style="width : 90%;" type="text" class="form-control pull-right" name="employeeName"/>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <label class="labels required pull-left">BACKGROUND INFORMATION &nbsp</label><label class="labels pull-left"> (REASON FOR CHANGE/EFFECTS ON CURRENT OPERATIONS/DESIRED RESULTS)</label>
                        <textarea disabled name="" id="" class="form-control" rows="7">{{ $mocDetails['reason'] }}</textarea>
                    </div>
                </div>
                <div class="clear_20"></div>
                <div>
                    <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="attachment_note"><strong>FILER : </strong></label><br/>
                        <div class="col-md-6">
                            <div id="attachments">
                                @if($mocDetails["attachments"])
                                    @foreach($mocDetails["attachments"] as $key)
                                        <a style="display: block"; v-for="attachment in attachmentsOld"> {{ json_decode($key['fn'])->original_filename }}</a>
                                    @endforeach
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
                @if(count($mocDetails['signatories']) > 0)
                    <div>
                        <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>
                        @foreach($mocDetails['signatories'] as $signatory)
                            @if($mocDetails['company'] == 'RBC-CORP')
                                <div class="row">
                                    <div class="col-md-6">
                                        @if($signatory['signature_type'] == 1)
                                            <label class="labels">
                                                ENDORSED :
                                            </label>
                                        @elseif($signatory['signature_type'] == 2)
                                            <label class="labels">
                                                RECEIVED FOR INITIAL ASSESSMENT
                                            </label>
                                        @elseif($signatory['signature_type'] == 3)
                                            <label class="labels">
                                                SUBMITTED FOR INITIAL ASSESSMENT
                                            </label>
                                        @elseif($signatory['signature_type'] == 4)
                                            <label class="labels">
                                                RECEIVED FOR FINAL ASSESSMENT
                                            </label>
                                        @elseif($signatory['signature_type'] == 5)
                                            <label class="labels">
                                                SUBMITTED FOR REVIEW
                                            </label>
                                        @elseif($signatory['signature_type'] == 6)
                                            <label class="labels">
                                                SUBMITTED FOR APPROVAL
                                            </label>
                                        @endif
                                        <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group pull-right" style="width: 200px">
                                            <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                        </div>
                                    </div>
                                </div>
                            @else()
                                <div class="row">
                                    <div class="col-md-6">
                                        @if($signatory['signature_type'] == 1)
                                            <label class="labels">
                                                ENDORSED :
                                            </label>
                                        @elseif($signatory['signature_type'] == 2)
                                            <label class="labels">
                                                SUBMITTED FOR INITIAL ASSESSMENT
                                            </label>
                                        @elseif($signatory['signature_type'] == 3)
                                            <label class="labels">
                                                SUBMITTED FOR PROCESSING
                                            </label>
                                        @elseif($signatory['signature_type'] == 4)
                                            <label class="labels">
                                                RECEIVED FOR FINAL ASSESSMENT
                                            </label>
                                        @elseif($signatory['signature_type'] == 5)
                                            <label class="labels">
                                                SUBMITTED FOR REVIEW
                                            </label>
                                        @elseif($signatory['signature_type'] == 6)
                                            <label class="labels">
                                                SUBMITTED FOR APPROVAL
                                            </label>
                                        @elseif($signatory['signature_type'] == 7)
                                            <label class="labels">
                                                SUBMITTED FOR ACKNOWLEDGEMENT
                                            </label>
                                        @endif
                                        <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group pull-right" style="width: 200px">
                                            <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div><!-- end of form_container -->
            <div class="clear_20"></div>
            <span class="action-label labels">ACTION</span>
            <div class="form_container msr-form-container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 comment-box">
                        <label class="labels pull-left">MESSAGE:</label>
                        <textarea disabled rows="3" class="form-control pull-left" name="comment">@foreach($mocDetails['comments'] as $key){{ $key['employee']['firstname'].' '.$key['employee']['middlename'].' '.$key['employee']['lastname'].' '.$key['ts_comment'].' : '.$key['comment']}} &#013; @endforeach</textarea>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </div>
        </div>
    </body>
</html>

