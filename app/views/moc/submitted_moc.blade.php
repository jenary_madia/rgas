@extends('template/header')

@section('content')
    <div id="wrap">
        <div class="clear_20"></div>
        {{ Form::open(array('url' => 'moc/export', 'method' => 'post', 'files' => true ,'id' => 'receiverSearch')) }}
        <div class="wrapper" id="submitted-moc" v-cloak>
            <div class="row">
                <div class="col-md-5">
                    <label for="" class="pull-left">Company</label>
                    {{ Form::select('company', $companies,null,['v-model'=> 'company','class' => 'form-control pull-right','style'=>'width:300px;']) }}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-5">
                    <label for="" class="pull-left">Department</label>
                    <select name="department" id="" v-model="department" class="form-control pull-right" style="width : 300px;">
                        <option value="0">all</option>
                        <option v-for='dept in departmentList' value="@{{ dept.dept_name }}">
                            @{{ dept.dept_name }}
                        </option>
                    </select>
                </div>
            </div>
            <br>
            <table id="forAssessmentMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forAssessmentMOC">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ASSESSMENT</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Title</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current employer</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Date assigned</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Date acknowledge</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            @include('moc.modal.generate_report')
        </div>
        {{ Form::close() }}
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/moc/moc_datatables.js') }}
    {{ HTML::script('/assets/js/moc/vue-submitted.js') }}
    <script>
        $('.date_picker').datepicker({
            dateFormat : 'yy-mm-dd'
        });
        $('body').on('click','#btnGenerate',function (e) {
            e.preventDefault();
            $('#generateModal').modal('show');
        });
    </script>
@stop
