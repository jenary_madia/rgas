<div class="col-md-10 col-md-offset-1">
    @if($mocDetails['assessment_type'] == "")
        {{-- 1. To assisstant dept head--}}
        <div class="row actions">
            <div class="col-md-7">
                <label class="button_notes">
                    <span class="pull-left"><strong>SEND</strong> TO ASSISTANT DEPARTMENT HEAD FOR </span>
                    <select name="" id="" v-model="assessmentType" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 100px;">
                        <option value="1">INITIAL</option>
                        <option value="2">FINAL</option>
                    </select>
                    <span class="pull-left"> ASSESSMENT</span>
                </label>
            </div>
            <div class="col-md-5 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendDHtoADH">SEND</button>
            </div>
        </div>
        {{-- 2. To assisstant dept head //enabled when department is csmd --}}
        @if($mocDetails['owner']['departmentid'] === CSMD_DEPT_ID)
            <div class="row actions">
                <div class="col-md-6">
                    <label class="button_notes"><strong>SEND</strong> TO FILER FOR ACKNOWLEDGEMENT</label>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendToFilerAck">SEND</button>
                </div>
            </div>
        @endif
        {{-- 3. Returning to filer // enabled if there is no input needed upon returning --}}
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnMOC">RETURN</button>
            </div>
            <div class="col-md-10 col-md-offset-1 text-center">
                <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
            </div>
        </div>
    @elseif((is_null($lastSignature) || $lastSignature == 2) && $mocDetails['assessment_type'] == 1)
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="saveMOC">SAVE</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> INITIAL ASSESSMENT TO DEPARTMENT HEAD FOR REVIEW</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendADHtoDH(1)">SEND</button>
            </div>
        </div>
    @elseif($lastSignature == 3)
        {{-- 1. To assisstant dept head--}}
        <div class="row actions">
            <div class="col-md-7">
                <label class="button_notes">
                    <span class="pull-left"><strong>SEND</strong> TO ASSISTANT DEPARTMENT HEAD FOR </span>
                    <select name="" id="" v-model="assessmentType" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 100px;">
                        <option value="2">FINAL</option>
                    </select>
                    <span class="pull-left"> ASSESSMENT</span>
                </label>
            </div>
            <div class="col-md-5 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendDHtoADH">SEND</button>
            </div>
        </div>
        @if($mocDetails['requested_by'] != Session::get('employee_id'))
            <div class="row actions">
                <div class="col-md-6">
                    <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnMOC">RETURN</button>
                </div>
                <div class="col-md-10 col-md-offset-1 text-center">
                    <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
                </div>
            </div>
        @endif
    @elseif(($lastSignature == 4 && is_null($mocDetails['assigned_to'])) || (is_null($lastSignature) && $mocDetails['assessment_type'] == 2))
        {{-- 1. To assisstant dept head--}}
        <div class="row actions">
            <div class="col-md-7">
                <label class="button_notes">
                    <span class="pull-left"><strong>ASSIGN</strong> TO </span>
                    <select name="" id="" v-model="moc_bsa" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                        <option value=""></option>\
                        @foreach($bsa as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <span class="pull-left"> FOR ASSESSMENT</span>
                </label>
            </div>
            <div class="col-md-5 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendADHtoBSA">ASSIGN</button>
            </div>
        </div>
        {{-- 3. Returning to filer // enabled if there is no input needed upon returning --}}
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> ASSESSMENT TO DEPARTMENT HEAD FOR REVIEW</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendADHtoDH(2)">SEND</button>
            </div>
            <div class="col-md-10 col-md-offset-1 text-center">
                <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
            </div>
        </div>
    @elseif($lastSignature == 4 && ! is_null($mocDetails['assigned_to']))
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="saveMOC">SAVE</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> TO ASSISTANT DEPARTMENT HEAD FOR REVIEW</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendBSAtoADH">SEND</button>
            </div>
        </div>
    @elseif($lastSignature == 5 && is_null($mocDetails['received_by']))
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR REVIEW</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendADHtoDH(2)">SEND</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO ASSIGNED BSA</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnToBSA">RETURN</button>
            </div>
            <div class="col-md-10 col-md-offset-1 text-center">
                <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
            </div>
        </div>
    @elseif($lastSignature == 5 && ! is_null($mocDetails['received_by']))
        @if($mocDetails['requested_by'] != Session::get('employee_id'))
            <div class="row actions">
                <div class="col-md-6">
                    <label class="button_notes"><strong>SEND</strong> TO FILER FOR ACKNOWLEDGEMENT</label>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendToFilerAck">SEND</button>
                </div>
            </div>
        @endif
        <div class="row actions">
            <div class="col-md-7">
                <label class="button_notes">
                    <span class="pull-left"><strong>SEND</strong> TO DIVISION HEAD FOR APPROVAL</span>
                </label>
            </div>
            <div class="col-md-5 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendtoDivHead">SEND</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes pull-left"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO</label>
                <select name="" id="" v-model="assessmentType" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                    @foreach($lastTwoSigs as $key)
                        <option value="{{ $key['employee']['id'] }}">{{ $key['employee']['firstname'].' '.$key['employee']['middlename'].' '.$key['employee']['lastname']   }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnMOC">RETURN</button>
            </div>
                <div class="col-md-10 col-md-offset-1 text-center">
                    @if($mocDetails['requested_by'] != Session::get('employee_id'))
                        <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
                    @else
                        <br>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <button value="cancel" name="action" class="btn btn-default btndefault" @click.prevent="acknowledgeMOC">ACKNOWLEDGE</button>
                                </div>
                                <div class="col-md-6 text-center">
                                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
        </div>
    @endif
</div>