<div class="col-md-10 col-md-offset-1">
    @if($lastSignature == 6)
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO SSMD FOR ACKNOWLEDGEMENT</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendDivHeadtoSSMD">SEND</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes pull-left"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO</label>
                <select name="" id="" v-model="assessmentType" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                    @foreach($lastTwoSigs as $key)
                        <option value="{{ $key['employee']['id'] }}">{{ $key['employee']['firstname'].' '.$key['employee']['middlename'].' '.$key['employee']['lastname']   }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnMOC">RETURN</button>
            </div>
            <div class="col-md-10 col-md-offset-1 text-center">
                <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
            </div>
        </div>
    @else
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> TO CSMD FOR PROCESSING</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent=sendAOMToMD>SEND</button>
            </div>
        </div>
        <div class="row actions">
            @if($mocDetails['requested_by'] === $mocDetails['curr_emp'])
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <button value="cancel" name="action" class="btn btn-default btndefault" @click.prevent="cancelMOC">CANCEL REQUEST</button>
                        </div>
                        <div class="col-md-6 text-center">
                            <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-6">
                    <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnMOC">RETURN</button>
                </div>
                <div class="col-md-10 col-md-offset-1 text-center">
                    <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
                </div>
            @endif
        </div>
    @endif
</div>