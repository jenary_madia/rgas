<div class="col-md-10 col-md-offset-1">
    <div class="row actions">
        <div class="col-md-6">
            <label class="button_notes"><strong>SEND</strong> TO FILER FOR ACKNOWLEDGEMENT</label>
        </div>
        <div class="col-md-6 text-right">
            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendToFilerAck">SEND</button>
        </div>
        <div class="col-md-10 col-md-offset-1 text-center">
            <a class="btn btn-default btndefault" href="{{ url('/moc') }}">BACK</a>
        </div>
    </div>
</div>