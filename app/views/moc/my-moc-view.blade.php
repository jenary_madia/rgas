@extends('template/header')

@section('content')
    <div id="moc_edit" v-cloak>
        {{--FOR for message prompt--}}
        <div class="alert alert-success msr-form-container" v-if="submitStatus.submitted && submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div class="alert alert-info msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div id="global_message" class="alert alert-danger msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <ul>
                <li v-for="error in submitStatus.errors">@{{ error }}</li>
            </ul>
        </div>
        {{--END for message prompt--}}
        {{ Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
        <div class="form_container msr-form-container">
            <div class="container-header"><h5 class="text-center"><strong>{{ MOC_FORM_TITLE }}</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="@{{ formDetails.employeeName }}" />
                </div>
                <div class="col-md-6 pull-left">
                    <label class="labels required">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" value="@{{ formDetails.referenceNo }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="@{{ formDetails.employeeNumber }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DATE FILED:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" value="@{{ formDetails.dateFiled }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">COMPANY:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="@{{ formDetails.company }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">STATUS:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="@{{ formDetails.status }}" />
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DEPARTMENT:</label>
                    <input value="@{{ formDetails.department }}" readonly="readonly" type="text" class="form-control pull-right" name="department" />
                </div>
                <div class="col-md-6">
                    <label class="labels">CONTACT NUMBER:</label>
                    <input value="@{{ formDetails.contactNumber }}" disabled type="text" class="form-control pull-right" name="contactNumber" maxlength="20" />
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels pull-left">SECTION:</label>
                    <input value="@{{ formDetails.section }}" disabled type="text" class="form-control pull-right" name="section" maxlength="25" />
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>PSR/MOC DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-12">
                    <div class="moc-sidebar">
                        <label class="labels required pull-left">TYPE OF REQUEST:</label>
                        <br>
                        <ul class="moc-list list-unstyled">
                            @foreach($requestTypes as $requestType)
                                <li class="text-uppercase"><input disabled type="checkbox" v-model="mocDetails.requestTypes" value="{{ $requestType->item }}"> {{ $requestType->text }}</li>
                            @endforeach
                        </ul>
                        <label class="labels required pull-left">URGENCY:</label>
                            <input value="@{{ mocDetails.urgency | uppercase }}" readonly="readonly" type="text" class="form-control pull-right"/>
                        </select>
                    </div>
                    <div class="moc-main">
                        <label class="labels pull-left">FOR REQUEST ON <span>PROCESS DESIGN/IMPROVEMENT</span> AND <span>REVIEW OF CHANGE</span></label>
                        <div class="moc-inner-sidebar">
                            <label class="labels pull-left">CHANGE IN:</label>
                            <br>
                            <ul class="list-unstyled moc-list">
                                @foreach($changeIn as $change)
                                    @if($change->item == "others")
                                        <li class="text-uppercase"><input disabled type="checkbox"  v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }} <input disabled style="width : 140px; text-transform: none !important;" value="@{{ mocDetails.changeInOthersInput }}" type="text" class="pull-right form-control"></li>
                                    @else
                                        <li class="text-uppercase"><input disabled type="checkbox" v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }}</li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="moc-inner-main">
                            <div class="moc-inner-sidebar">
                                <label class="labels pull-left">CHANGE TYPE:</label>
                                <input value="@{{ mocDetails.changeType | uppercase }}" readonly="readonly" type="text" class="form-control pull-right"/>
                            </div>
                            <div class="moc-inner-main">
                                <label class="labels pull-right">TARGET IMPLEMENTATION DATE:</label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input disabled value="@{{ mocDetails.targetDate }}" type="text" name="" id="UTdate" readonly class="date_picker form-control input-small">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <label style="margin-top: 10px" class="labels pull-left">DEPARTMENTS INVOLVED:</label>
                            <div class="clearfix"></div>
                            <div class="moc-inner-main">
                                <select disabled class="form-control" name="" id="">
                                </select>
                            </div>
                            <div class="moc-inner-sidebar">
                                <button disabled class="btn btn-default btndefault pull-right" @click.prevent="alert('Invalid request')">ADD</button>
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <div class="moc-whole" style="border : 1px solid #ccc;">
                                <ol class="moc-list">
                                    <li v-for="deptInvolved in mocDetails.departmentsInvolved">@{{ deptInvolved.comp_code }} - @{{ deptInvolved.dept_name }}<a @click="removeDeptInvolved($index)"> Remove</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12">
                    <label class="labels required pull-left" style="padding-top: 7px">TITLE/SUBJECT:</label>
                    <input value="@{{ mocDetails.title }}" disabled style="width : 90%;" type="text" class="form-control pull-right" name="employeeName"/>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12">
                    <label class="labels required pull-left">BACKGROUND INFORMATION &nbsp</label><label class="labels pull-left"> (REASON FOR CHANGE/EFFECTS ON CURRENT OPERATIONS/DESIRED RESULTS)</label>
                    <textarea disabled name="" id="" class="form-control" rows="7">@{{ mocDetails.backgroundInfo }}</textarea>
                </div>
                <div class="clearfix"></div>
                <br>
                @if($mocDetails['company'] != 'RBC-CORP')
                    <div class="col-md-12">
                        <label class="labels required pull-left" style="padding-top: 7px">SSMD ASSESSMENT/ACTION PLAN:</label>
                        <textarea name="" id="" class="form-control" rows="7" disabled>@{{ mocDetails.ssmd_action_plan }}</textarea>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                @endif
                <div class="col-md-12">
                    <label class="labels required pull-left" style="padding-top: 7px">CSMD ASSESSMENT/ACTION PLAN:</label>
                    <textarea disabled name="" id="" class="form-control" rows="7">@{{ mocDetails.csmd_action_plan }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="attachment_note"><strong>FILER : </strong></label><br/>
                        <div id="attachments">
                            <a style="display: block"; v-for="attachment in attachmentsOld" href="@{{ attachment.urlDownload }}"> @{{ attachment.original_filename }}</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="attachment_note"><strong>SSMD : </strong></label><br/>
                        <a style="display: block"; v-for="attachment in SSMDattachmentsOld" href="@{{ attachment.urlDownload }}"> @{{ attachment.original_filename }}</a>
                    </div>
                    <div class="col-md-4">
                        <label class="attachment_note"><strong>CSMD : </strong></label><br/>
                        <a style="display: block"; v-for="attachment in CSMDattachmentsOld" href="@{{ attachment.urlDownload }}"> @{{ attachment.original_filename }}</a>
                    </div>
                </div>
            </div>
            @if(count($mocDetails['signatories']) > 0)
                <div>
                    <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>
                    @foreach($mocDetails['signatories'] as $signatory)
                        @if($mocDetails['company'] == 'RBC-CORP')
                            <div class="row">
                                <div class="col-md-6">
                                    @if($signatory['signature_type'] == 1)
                                        <label class="labels">
                                            ENDORSED :
                                        </label>
                                    @elseif($signatory['signature_type'] == 2)
                                        <label class="labels">
                                            RECEIVED FOR INITIAL ASSESSMENT
                                        </label>
                                    @elseif($signatory['signature_type'] == 3)
                                        <label class="labels">
                                            SUBMITTED FOR INITIAL ASSESSMENT
                                        </label>
                                    @elseif($signatory['signature_type'] == 4)
                                        <label class="labels">
                                            RECEIVED FOR FINAL ASSESSMENT
                                        </label>
                                    @elseif($signatory['signature_type'] == 5)
                                        <label class="labels">
                                            SUBMITTED FOR REVIEW
                                        </label>
                                    @elseif($signatory['signature_type'] == 6)
                                        <label class="labels">
                                            SUBMITTED FOR APPROVAL
                                        </label>
                                    @endif
                                    <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                        @else()
                            <div class="row">
                                <div class="col-md-6">
                                    @if($signatory['signature_type'] == 1)
                                        <label class="labels">
                                            ENDORSED :
                                        </label>
                                    @elseif($signatory['signature_type'] == 2)
                                        <label class="labels">
                                            SUBMITTED FOR INITIAL ASSESSMENT
                                        </label>
                                    @elseif($signatory['signature_type'] == 3)
                                        <label class="labels">
                                            SUBMITTED FOR PROCESSING
                                        </label>
                                    @elseif($signatory['signature_type'] == 4)
                                        <label class="labels">
                                            RECEIVED FOR FINAL ASSESSMENT
                                        </label>
                                    @elseif($signatory['signature_type'] == 5)
                                        <label class="labels">
                                            SUBMITTED FOR REVIEW
                                        </label>
                                    @elseif($signatory['signature_type'] == 6)
                                        <label class="labels">
                                            SUBMITTED FOR APPROVAL
                                        </label>
                                    @elseif($signatory['signature_type'] == 7)
                                        <label class="labels">
                                            SUBMITTED FOR ACKNOWLEDGEMENT
                                        </label>
                                    @endif
                                    <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    <div v-if="formDetails.status != 'NEW'">
                        <label class="labels pull-left">MESSAGE:</label>
                        <textarea disabled rows="3" class="form-control pull-left" name="comment">@{{ messages }}</textarea>
                    </div>
                    <label class="labels pull-left">COMMENT:</label>
                    <textarea rows="3" :disabled="formDetails.status != 'FOR ENDORSEMENT'" class="form-control pull-left" name="comment" v-model="comment"></textarea>
                </div>
                @if($mocDetails['status'] == 'FOR ACKNOWLEDGEMENT')
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <button value="cancel" name="action" class="btn btn-default btndefault" @click.prevent="acknowledgeMOC">ACKNOWLEDGE</button>
                            </div>
                            <div class="col-md-4 text-center">
                                <a value="cancel" name="action" href="{{ url('/moc/print',[$mocID]) }}" class="btn btn-default btndefault" >DOWNLOAD</a>
                            </div>
                            <div class="col-md-4 text-center">
                                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <button value="cancel" name="action" class="btn btn-default btndefault" :disabled="formDetails.status != 'FOR ENDORSEMENT'" @click.prevent="cancelMOC">CANCEL REQUEST</button>
                            </div>
                            <div class="col-md-4 text-center">
                                <a value="cancel" name="action" href="{{ url('/moc/print',[$mocID]) }}" class="btn btn-default btndefault" >DOWNLOAD</a>
                            </div>
                            <div class="col-md-4 text-center">
                                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div><!-- end of form_container -->
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    <script>
        var mocID = {{ $mocID }};
    </script>
    {{ HTML::script('/assets/js/moc/vue-edit.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
                @if(Input::old("files"))
        var file_counter = {{ $i }};
                @else
        var file_counter = 0;
                @endif
        var allowed_file_count = 10;
        var allowed_total_filesize = 20971520;
        $('.date_picker').datepicker({
            dateFormat : 'yy-mm-dd'
        });
        $('body').on('click','.toggleDatePicker',function () {
            $(this).closest( "div" ).find(".date_picker").datepicker("show");
        });
    </script>
    {{ HTML::script('/assets/js/moc/file-upload.js') }}
@stop
