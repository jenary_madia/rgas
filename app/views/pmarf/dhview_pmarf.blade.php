@extends('template/header')

@section('content')
<link href="<?php echo $base_url; ?>/assets/css/builds/new_style.css" rel="stylesheet">
<div id="wrap">
   
    <div class="container">
        <form class="form-inline">

            <div class="form_container">
                <label class="form_title">PMARF FOR APPROVAL</label>
                <div class="row">
                    <table id="saved" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>REFFERENCE NO.</th>
                                <th>DATE FILED</th>
                                <th>REQUESTOR</th>
                                <th>ACTIVITY TYPE</th>
                                <th>STATUS</th>
                                <th>CURRENT</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div> <!-- end of form_container-->  
        </form>
        <div class="clear_20"></div>

    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop
@section('js_ko')
	{{ HTML::script('/assets/js/pmarf_save.js') }}
@stop
