@extends('template/header')

@section('content')
<link href="<?php echo $base_url; ?>/assets/css/builds/new_style.css" rel="stylesheet">
<div id="wrap">
    @include('template/sidebar')
    <div class="container">
        <form class="form-inline">

            <div class="form_container">
                <label class="form_title">PROMO AND MERCHANDISING ACTIVITY REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control placeholders" placeholder="yyyy-mm-dd" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Company:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Contact Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Section:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label pmarf_form_title">REQUEST DETAILS</label>

                    <div class="clear_20"></div>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Type of Activity:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Activity Details:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date of Activity:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Dates:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels1">FROM:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                            <div class="clear_10"></div>
                            <div class="col1_form_container">
                                <label class="labels1">TO:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>

                        <div class="row2_form_container">
                            <div class="col4_form_container">
                                <label class="labels">No. of Days:</label>
                            </div>
                            <div class="col6_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>


                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Area Coverage:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Implementer:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Agency Name:</label>
                            </div>
                            <div class="col5_form_container agency_textbox">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col7_form_container">
                                <label class="labels">Initiated By:</label>
                            </div>
                            <div class="col8_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox4_label">
                                    <label class="labels" for="chkCoe">
                                        Distributor
                                    </label>
                                </div>
                            </div>
                            <div class="col8_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox4_label">
                                    <label class="labels" for="chkCoe">
                                        Trade
                                    </label>
                                </div>
                            </div>
                            <div class="col9_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox5_label">
                                    <label class="labels" for="chkCoe">
                                        Others
                                    </label>
                                </div>
                                <div class="">
                                    <input type="textbox" id="" class="others form-control" />
                                </div>
                            </div>       
                        </div>
                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col10_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox4_label">
                                    <label class="labels" for="chkCoe">
                                        Brand Management
                                    </label>
                                </div>
                            </div>
                            <div class="col8_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox4_label">
                                    <label class="labels" for="chkCoe">
                                        Strategic Corporate Development
                                    </label>
                                </div>
                            </div>   
                        </div>
                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col10_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox4_label">
                                    <label class="labels" for="chkCoe">
                                        Ad and Media
                                    </label>
                                </div>
                            </div>
                            <div class="col8_form_container">
                                <div class="col2_checkbox">
                                    <input type="checkbox" id="chkCoe"/>
                                </div>
                                <div class="checkbox4_label">
                                    <label class="labels" for="chkCoe">
                                        Key Accounts Manager
                                    </label>
                                </div>
                            </div>   
                        </div>
                    </div><!-- end of form_container -->

                    <div class="clear_20"></div>

                    <label class="form_label pmarf_form_title">SPECIFICATIONS</label>

                    <div class="clear_20"></div>

                    <label class="pmarf_textarea_labels">PROPOSAL OBJECTIVES</label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">BRIEF DESCRIPTION OF PROPOSAL</label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="4" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">ADDITIONAL SUPPORT ACTIVITIES</label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="2" cols="10" class="form-control"> </textarea>
                    </div>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">PARTICIPATING BRANDS</label>
                    <div class="pmarf_textarea">
                        <table border = "1" cellpadding = "0" class="">
                            <th class="th_style quantity">QUANTITY</th>
                            <th class="th_style quantity">UNIT</th>
                            <th class="th_style">PRODUCT NAME</th>
                            <tr>
                                <td class="td_style">
                                    <input type="textbox" id="" class="quantity_textbox" />
                                </td>
                                <td class="td_style">
                                    <input type="textbox" id="" class="quantity_textbox" />
                                </td>
                                <td class="td_style">
                                    <input type="textbox" id="" class="product_textbox" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_style">
                                    <input type="textbox" id="" class="quantity_textbox" />
                                </td>
                                <td class="td_style">
                                    <input type="textbox" id="" class="quantity_textbox" />
                                </td>
                                <td class="td_style">
                                    <input type="textbox" id="" class="product_textbox" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td_style">
                                    <input type="textbox" id="" class="quantity_textbox" />
                                </td>
                                <td class="td_style">
                                    <input type="textbox" id="" class="quantity_textbox" />
                                </td>
                                <td class="td_style">
                                    <input type="textbox" id="" class="product_textbox" />
                                </td>
                            </tr>

                        </table>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label pmarf_form_title">ATTACHMENTS</label>

                    <div class="clear_20"></div>

                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <button type="submit" class="btn btnbrowse btn-default" value="">BROWSE</button><br/>
                        <div class="sample_file">SAMPLE 1 FILENAME</div><br/>
                        <div class="sample_file">SAMPLE 2 FILENAME</div><br/>
                        <div class="sample_file">SAMPLE 3 FILENAME</div><br/>
                    </div>

            </div> <!-- end of form_container-->  
            <div class="clear_20"></div>
            <div class="form_container">
                        <div class="textarea_messages_container">
                            <div class="row">
                                <label class="textarea_inside_label">MESSAGE:</label>
                                <textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled></textarea>
                            </div>
                        </div>

                        <div class="clear_10"></div>
                        <div class="row">
                            <div class="comment_container"> 
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>ACKNOWLEDGE </strong> FOR COMPLETION OF REQUEST</label>
                                </div> 
                                <div class="acknowledge_btn_spacing">
                                    <button type="submit" class="btn btn-default acknowledge_btn" value="">ACKNOWLEDGE</button>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                            <div class="back_center_btn"> 
                                <button type="submit" class="btn btn-default btndefault" value="">BACK</button>
                            </div>
                        </div>
                    </div><!-- end of form_container -->
 
        </form>
        <div class="clear_20"></div>

    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop