@extends('template/header')

@section('content')
<link href="<?php echo $base_url; ?>/assets/css/builds/new_style.css" rel="stylesheet">


<div id="wrap">
    <div class="container">
         <form class="form-inline" method="post" enctype="multipart/form-data">

            <div class="form_container">
                <label class="form_title">PROMO AND MERCHANDISING ACTIVITY REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="emp_name" name="emp_name" value="{{ Session::get('employee_name') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
                               <input readonly="readonly" type="text" class="form-control" name="pmarf_refno" value="{{ $reference_number }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
<<<<<<< HEAD
                                <input readonly="readonly" type="text" class="form-control" name="emp_id" value="{{ Session::get('employeeid') }}" />
=======
                                <input readonly="readonly" type="text" class="form-control" name="emp_id" value="{{ Session::get('employee_id') }}" />
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control placeholders" value="{{ date('Y-m-d') }}" placeholder="yyyy-mm-dd" id="pmarf_date_filed" name="pmarf_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Company:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" id="emp_company" name="emp_company"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
                               <input readonly="readonly" type="text" class="form-control" value="New" name="pmarf_status" id="pmarf_status"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('dept_name') }}" name="emp_department" id="emp_department"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Contact Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" name="emp_contactno" id="emp_contactno" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Section:</label>
                            </div>
                            <div class="col2_form_container">
                                 <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('sect_name') }}" id="emp_section" name="emp_section" />
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label pmarf_form_title">REQUEST DETAILS</label>

                    <div class="clear_20"></div>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Type of Activity:</label>
                            </div>
                            <div class="col2_form_container">
                                <select id='req_activity_type' name='req_activity_type' class="form-control" style="width: 241px">
                                    @foreach ($activity_type as $aKey=>$aVal)
                                        <option value="{{ $aVal }}">{{ $aVal }}</p>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row_form_container activity_detail" style="display:none;">
                            <div class="col4_form_container">
                                <label class="labels">Activity Details:</label>
                            </div>
                            <div class="col5_form_container">
                                <input id='req_activity_details' name='req_activity_details' type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date of Activity:</label>
                            </div>
                            <div class="col2_form_container">
                               <select id='req_activity_date' name='req_activity_date' class="form-control" style="width: 241px">
                                    @foreach ($activity_date as $sKey=>$sDates)
                                        <option value="{{ $sDates }}">{{ $sDates }}</p>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row_form_container div_staggered" style="display:none;">
                            <div class="col4_form_container">
                                <label class="labels">Dates:</label>
                            </div>
                            <div class="col5_form_container">
                                <input id="req_date" name="req_date" type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container div_straight">
                            <div class="col1_form_container">
                                <label class="labels1">FROM:</label>
                            </div>
                            <div class="col2_form_container">
                                
                                <input readonly="readonly" type="text" class="form-control date_picker" name="req_from" id="req_from"/>
                            </div>
                            <div class="clear_10"></div>
                            <div class="col1_form_container">
                                <label class="labels1">TO:</label>
                            </div>
                            <div class="col2_form_container">
                                 <input readonly="readonly" id="req_to" name="req_to" type="text" class="form-control date_picker" />
                            </div>
                        </div>

                        <div class="row2_form_container div_straight_no">
                            <div class="col4_form_container">
                                <label class="labels">No. of Days:</label>
                            </div>
                            <div class="col6_form_container">
                                 <input id="req_noOfDays" name="req_noOfDays" type="text" class="form-control" />
                            </div>
                        </div>


                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Area Coverage:</label>
                            </div>
                            <div class="col2_form_container">
                                <input id="req_area_coverage" name="req_area_coverage" type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Implementer:</label>
                            </div>
                            <div class="col2_form_container">
                               <select id='req_implementer' name='req_implementer' class="form-control" style="width: 241px">
                                    @foreach ($implementer as $sK=>$sImplementer)
                                        <option value="{{ $sImplementer }}">{{ $sImplementer }}</p>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Agency Name:</label>
                            </div>
                            <div class="col5_form_container agency_textbox">
                                 <input id="req_agency_name" name="req_agency_name" type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col7_form_container">
                                <label class="labels">Initiated By:</label>
                            </div>
                        </div>
                        
                        @foreach ($initiated_by as $sI=>$initiated_by)
                            @if (($sI%2) === 0)
                                 <div class="row3_form_container">
                            @endif    
                                    <div class="col10_form_container">
                                        <div class="col2_checkbox">
                                            <input type="checkbox" id="chk_{{ $initiated_by }}" name="chk_{{ $initiated_by }}"/>
                                        </div>
                                        <div class="checkbox4_label">
                                            <label class="labels" for="chkCoe">
                                                {{ $initiated_by }} 
                                                @if ($initiated_by === 'Others')
                                                    <input type="textbox" id="chk_others_desc" name="chk_others_desc" />
                                                @endif
                                            </label>
                                        </div>
                                     </div>
                            @if (($sI%2) === 2)
                                </div>
                            @endif
                        @endforeach
                        
                    </div><!-- end of form_container -->

                   
                    <div class="clear_20"></div>
                    <div class="clear_10"></div>
                    <label class="form_label pmarf_form_title">SPECIFICATIONS</label>

                    <div class="clear_1"></div>

                    <label class="pmarf_textarea_labels">PROPOSAL OBJECTIVES</label>
                    <div class="pmarf_textarea">
                        <textarea id="specs_proposal" name="specs_proposal" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">BRIEF DESCRIPTION OF PROPOSAL</label>
                    <div class="pmarf_textarea">
                        <textarea id="specs_proposal_desc" name="specs_proposal_desc" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">ADDITIONAL SUPPORT ACTIVITIES</label>
                    <div class="pmarf_textarea">
                        <textarea id="specs_additional_support" name="specs_additional_support" rows="2" cols="10" class="form-control"> </textarea>
                    </div>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">PARTICIPATING BRANDS</label>
                    <div class="pmarf_textarea">
                        <table border = "1" cellpadding = "0" class="" id="tbl_brands">
                            <th class="th_style quantity"></th>
                            <th class="th_style quantity">QUANTITY</th>
                            <th class="th_style quantity">UNIT</th>
                            <th class="th_style">PRODUCT NAME</th>
                    
                            <tr>
                                <td class="td_style"><i class="fa fa-asterisk" aria-hidden="true" style="color:red"></i></td>
                                <td class="td_style"><input type="textbox" name="qty_1" id="qty_1" class="quantity_textbox" /></td>
                                <td class="td_style"><input type="textbox" name="unit_1" id="unit_1" class="quantity_textbox" /></td>
                                <td class="td_style"><input type="textbox" name="product_1" id="product_1" class="product_textbox" /></td>
                            </tr>
                        </table>
                    </div>

                    <div class="pmarf_btns btn-group">
                        <button type="button" id="brands_add" name="brands_add"  class="btn btn2 btn-default" value="">Add</button>
                        <button type="button" id="brands_del" name="brands_del" class="btn btn2 btn-default" value="">Delete</button>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label pmarf_form_title">ATTACHMENTS</label>

                    <div class="clear_20"></div>

                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <button type="button" class="btn btnbrowse btn-default" id="btnAddAttachment">ADD</button><br/>
						<div id="attachments"></div>
                    </div>

            </div> <!-- end of form_container-->  
            <div class="clear_20"></div>
          
                        <div class="textarea_messages_container">
                            <div class="row">
                                <label class="textarea_inside_label">COMMENT:</label>
                                <textarea name="req_comments" id="req_comments" rows="3" class="form-control textarea_inside_width" placeholder="FORMAT: <First Name Middle Initial. Last Name> <MM/DD/YYYY> <HH:MM AM/PM>: <Comment> 
                                e.g. Myla Bregente 04/30/2015 2:16 PM: <Comment>"></textarea>
                            </div>
                        </div>

                        <div class="clear_10"></div>
                        <div class="row">
                            <div class="comment_container">
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                                </div> 
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                            <div class="comment_container">
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                                </div> 
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- end of form_container -->
 
        </form>
        <div class="clear_20"></div>

    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop
@section('js_ko')
	{{ HTML::script('/assets/js/pmarf.js') }}
@stop

