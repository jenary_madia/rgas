<div class="modal fade" id="addRecruitmentDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add recruitment details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels">CANDIDATE : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" v-model="toAddRecruitmentDetails.candidate" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="" class="labels">DATE ENDORSED : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div style="width : 200px;" class="pull-left input-group bootstrap-timepicker timepicker">
                                <input type="text" v-model="toAddRecruitmentDetails.dateEndorsed" readonly  id="from" class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="" class="labels">REMARKS : </label>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" v-model="toAddRecruitmentDetails.remarks"  class="form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>

                    <div class="col-md-12 pager text-center">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="addRecruitment">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editRecruitmentDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add recruitment details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels">CANDIDATE : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" v-model="toEditRecruitmentDetails.candidate" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="" class="labels">DATE ENDORSED : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div style="width : 200px;" class="pull-left input-group bootstrap-timepicker timepicker">
                                <input type="text" v-model="toEditRecruitmentDetails.dateEndorsed" readonly  id="from" class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="" class="labels">REMARKS : </label>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" v-model="toEditRecruitmentDetails.remarks"  class="form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>

                    <div class="col-md-12 pager text-center">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="saveEditRec">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



