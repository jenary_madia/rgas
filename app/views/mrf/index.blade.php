@extends('template/header')

@section('content')
    <div id="wrap">
        @if($type == "my-mrf")
            <div class="clear_20"></div>
            <div class="wrapper">
                {{ Form::open(array('url' => 'mrf/delete-mrf', 'method' => 'post', 'files' => true,'id' => 'formMyMRF')) }}
                <table id="myMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : center" role="columnheader" rowspan="1" colspan="5">My MRF</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                {{ Form::close() }}
            </div>
        @endif
        @if($type == "for-endorsement")
            <div class="clear_20"></div>
            <div class="wrapper">
                <table id="forEndorsementMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forEndorsementMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ENDORSEMENT</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        @endif
        @if($type == "for-processing")
            <div class="wrapper">
                <table id="forProcessingMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forProcessingMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR PROCESSING</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        @endif
        @if($type == "submitted")
            <div class="wrapper">
                <table id="approvedMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="approvedMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">APPROVED</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div class="wrapper">
                <table id="notedMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="notedMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">NOTED</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div class="wrapper">
                <table id="forAssessmentMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forAssessmentMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ASSESSMENT</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div class="wrapper">
                <table id="forApprovalMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApprovalMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR APPROVAL</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div class="wrapper">
                <table id="forFurtherMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forFurtherMRF">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR FURTHER ASSESSMENT</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        @endif
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/mrf/mrf-datatables.js') }}
@stop
