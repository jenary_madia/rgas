<div class="row actions">
    <div class="col-md-6">
        <label class="button_notes"><strong>SEND</strong> FOR PROCESSING</label>
    </div>
    <div class="col-md-6 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendForProcess">SEND</button>
    </div>
</div>
<div class="row actions">
    <div class="col-md-8">
        <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
    </div>
    <div class="col-md-4 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnProcess('toFiler')">RETURN</button>
    </div>
    <br>
    <br>
    <div class="col-md-10 col-md-offset-1 text-center">
        <a class="btn btn-default btndefault" href="{{ url('/mrf') }}">BACK</a>
    </div>
</div>