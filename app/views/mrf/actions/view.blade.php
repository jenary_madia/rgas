<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-6 text-center">
            <button value="cancel" {{ $mrfDetails->status != "FOR ENDORSEMENT" ? "disabled" : ""}} name="action" @click="cancelProcess" class="btn btn-default btndefault">CANCEL REQUEST</button>
        </div>
        <div class="col-md-6 text-center">
            <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
        </div>
    </div>
</div>