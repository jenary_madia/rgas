<div class="row actions">
    <div class="col-md-7">
        <label class="button_notes">
            <span class="pull-left"><strong>ASSIGN</strong> TO </span>
            <select v-model="recruitmentStaff" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                <option></option>
                @foreach($recruitmentStaffs as $staff)
                    <option value="{{ $staff->id }}">{{ $staff->firstname.' '.$staff->middlename.' '.$staff->lastname }}</option>
                @endforeach
            </select>
            <span class="pull-left"> FOR PROCESSING</span>
        </label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="assignProcess">ASSIGN</button>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        </div>
    </div>
</div>