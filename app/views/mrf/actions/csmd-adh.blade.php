<div class="row actions">
    <div class="col-md-7">
        <label class="button_notes"><strong>SAVE</strong> TO <strong>EDIT</strong> LATER</label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="processFor('save')">SAVE</button>
    </div>
    <div class="clear_10"></div>

    <div class="col-md-7">
        <label class="button_notes">
            <span class="pull-left"><strong>ASSIGN</strong> TO </span>
            <select name="" id="" v-model="csmdBSA" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                <option value=""></option>\
                @foreach($bsa as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                @endforeach
            </select>
            <span class="pull-left"> FOR ASSESSMENT</span>
        </label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="assignProcess">ASSIGN</button>
    </div>

    <div class="col-md-7">
        <label class="button_notes"><strong>SEND</strong> FOR APPROVAL</label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="processFor('toCsmdDH')">SEND</button>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-7">
        <label class="button_notes"><strong>RETURN</strong> TO CHRD</label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnProcess('toCHRDRecipient')">RETURN</button>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        </div>
    </div>
</div>