<div class="row actions">
    <div class="col-md-7">
        <label class="button_notes">
            <span class="pull-left">JOB LEVEL</span>
            <select v-model="jobLevel" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                <option value="1">AVP and up</option>
                <option value="2">Manager</option>
                <option value="3">Supervisor and below</option>
            </select>
        </label>
    </div>
    <div class="col-md-5 text-right">
        {{--<button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendForProcess">SEND</button>--}}
    </div>
    <div v-show="jobLevel != 1">
        <div class="col-md-7">
            <label class="button_notes"><strong>SEND</strong> TO CSMD FOR ASSESSMENT</label>
        </div>
        <div class="col-md-5 text-right">
            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendForProcess">SEND</button>
        </div>
    </div>
    @if($mrfDetails->company = "RBC-CORP")
        <div class="clear_10"></div>
        @if($mrfDetails->requestCode->name != "Additional Staff")
            <div class="col-md-7">
                <label class="button_notes">
                    <span class="pull-left"><strong>ASSIGN</strong> TO </span>
                    <select v-model="recruitmentStaff" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                        <option></option>
                        @foreach($recruitmentStaffs as $staff)
                            <option value="{{ $staff->employeeid }}">{{ $staff->employee->firstname.' '.$staff->employee->middlename.' '.$staff->employee->lastname }}</option>
                        @endforeach
                    </select>
                    <span class="pull-left"> FOR PROCESSING</span>
                </label>
            </div>
            <div class="col-md-5 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="assignProcess">ASSIGN</button>
            </div>
        @endif
    @endif
    <div class="col-md-8">
        <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
    </div>
    <div class="col-md-4 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnProcess('toFiler')">RETURN</button>
    </div>
    <br>
    <br>
    <div class="col-md-10 col-md-offset-1 text-center">
        <a class="btn btn-default btndefault" href="{{ url('/mrf') }}">BACK</a>
    </div>
</div>