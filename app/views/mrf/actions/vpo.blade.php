<div class="row actions">
    <div class="col-md-7">
        <label class="button_notes"> <strong>APPROVE</strong></label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendForProcess">APPROVE</button>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-7">
        <label class="button_notes"> <strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnProcess('toFiler')">DISAPPROVE</button>
    </div>
</div>