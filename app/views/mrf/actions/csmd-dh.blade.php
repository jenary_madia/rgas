<div class="row actions">
    <div class="col-md-7">
        <label class="button_notes"> <strong>SEND</strong> FOR APPROVAL</label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendForProcess">SEND</button>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-7">
        <label class="button_notes"> <strong>RETURN</strong>
            @if($mrfDetails->bsaassessedby)
                TO BSA
            @else
                TO CSMD ASSISTANT DEPARTMENT HEAD
            @endif
        </label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action"
        @if($mrfDetails->bsaassessedby)
            @click.prevent="returnProcess('toBSA')"
        @else
            @click.prevent="returnProcess('toCsmdADH')"
        @endif
        >RETURN</button>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        </div>
    </div>
</div>