<div>
    <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
    <div class="clear_20"></div>
    <div class="row">
        <div class="col-md-6">
            {{--MRAF--}}
            <label style="margin-right : 50px;">MRAF : </label>
                @if($mrfDetails->mraffile)
                    @foreach(json_decode($mrfDetails->mraffile) as $key)
                        <a style="display: block;" href="{{ URL::to('/mrf/download/'.$mrfDetails->reference_no.'/'.$key->random_filename.'/'.CIEncrypt::encode($key->original_filename)) }}">{{ $key->original_filename }}</a><br />
                    @endforeach
                @endif
            <div class="clear_10"></div>
            {{--PDQ--}}
            <label style="margin-right : 50px;">PDQ : </label>
                @if($mrfDetails->pdq)
                    @foreach(json_decode($mrfDetails->pdq) as $key)
                        <a style="display: block;" href="{{ URL::to('/mrf/download/'.$mrfDetails->reference_no.'/'.$key->random_filename.'/'.CIEncrypt::encode($key->original_filename)) }}">{{ $key->original_filename }}</a><br />
                    @endforeach
                @endif
            <div class="clear_10"></div>
            {{--OC--}}
            <label style="margin-right : 50px;">ORGANIZATIONAL CHART : </label>
                @if($mrfDetails->org_chart)
                    @foreach(json_decode($mrfDetails->org_chart) as $key)
                        <a style="display: block;" href="{{ URL::to('/mrf/download/'.$mrfDetails->reference_no.'/'.$key->random_filename.'/'.CIEncrypt::encode($key->original_filename)) }}">{{ $key->original_filename }}</a><br />
                    @endforeach
                @endif
        </div>
        <div class="col-md-6">
            @if($mrfDetails->status == 'FOR ASSESSMENT' && $method != "view")
                <label class="attachment_note"><strong>CSMD : </strong></label><br/>
                <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                <div id="CSMDattachments">
                    @foreach($mrfDetails['attachments'] as $key)
                        <p>
                            {{ $key->original_filename.' | '.$key->filesize  }}
                            <input type='hidden' class='CSMDattachmentData' value='{{json_encode($key)}}'>
                            <button class='btn btn-xs btn-danger'>DELETE</button>
                        </p>
                    @endforeach
                </div>
                <span class="btn btn-success btnbrowse fileinput-button">
                    <span>BROWSE</span>
                    <input id="CSMDfileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                </span>
            @else
                <label class="attachment_note"><strong>CSMD : </strong></label><br/>
                @foreach($mrfDetails['attachments'] as $key)
                    <a style="display: block"; href=""> {{ $key->original_filename }}</a>
                @endforeach
            @endif
        </div>

    </div>

    <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>
    <div class="clear_20"></div>
        @foreach($mrfDetails['signatories'] as $signatory)
            @if($mrfDetails['company'] == 'RBC-CORP')
                <div class="row">
                    <div class="col-md-6">
                        @if($signatory['signature_type'] == 1)
                            <label class="labels">
                                ENDORSED :
                            </label>
                        @elseif($signatory['signature_type'] == 2)
                            <label class="labels">
                                PROCESSED :
                            </label>
                        @elseif($signatory['signature_type'] == 3)
                            <label class="labels">
                                ASSESSED :
                            </label>
                        @elseif($signatory['signature_type'] == 4)
                            <label class="labels">
                                APPROVED :
                            </label>
                        @elseif($signatory['signature_type'] == 5)
                            <label class="labels">
                                DISAPPROVED :
                            </label>
                        @elseif($signatory['signature_type'] == 6)
                            <label class="labels">
                                FOR FURTHER ASSESSMENT :
                            </label>
                        @elseif($signatory['signature_type'] == 7)
                            <label class="labels">
                                NOTED :
                            </label>
                        @elseif($signatory['signature_type'] == 8)
                            <label class="labels">
                                RETURNED :
                            </label>
                        @endif
                        <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group pull-right" style="width: 200px">
                            <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                        </div>
                    </div>
                </div>
            @else()
                <div class="row">
                    <div class="col-md-6">
                        @if($signatory['signature_type'] == 1)
                            <label class="labels">
                                ENDORSED :
                            </label>
                        @elseif($signatory['signature_type'] == 2)
                            <label class="labels">
                                SUBMITTED FOR INITIAL ASSESSMENT
                            </label>
                        @elseif($signatory['signature_type'] == 3)
                            <label class="labels">
                                SUBMITTED FOR PROCESSING
                            </label>
                        @elseif($signatory['signature_type'] == 4)
                            <label class="labels">
                                RECEIVED FOR FINAL ASSESSMENT
                            </label>
                        @elseif($signatory['signature_type'] == 5)
                            <label class="labels">
                                SUBMITTED FOR REVIEW
                            </label>
                        @elseif($signatory['signature_type'] == 6)
                            <label class="labels">
                                SUBMITTED FOR APPROVAL
                            </label>
                        @elseif($signatory['signature_type'] == 7)
                            <label class="labels">
                                SUBMITTED FOR ACKNOWLEDGEMENT
                            </label>
                        @endif
                        <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group pull-right" style="width: 200px">
                            <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

        @if($mrfDetails->status == 'NOTED')
            <div class="container-header"><h5 class="text-center lined"><strong>RECRUITMENT DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>CANDIDATE</th>
                                <th>DATE ENDORSED</th>
                                <th>REMARKS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="rec in recruitmentDetails" v-bind:class="{ 'active' : rec.active }" @click="getForEdit($index,rec)">
                                <td>@{{ rec.candidate }}</td>
                                <td>@{{ rec.dateEndorsed }}</td>
                                <td>@{{ rec.remarks }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" value='{{ json_encode($mrfDetails['candidates']) }}' v-model="recruitmentDetailsOld">
                </div>
                <div class="clear_20"></div>
                @if($method != "view")
                    <div class="col-md-12">
                        <a class="btn btn-default btndefault" data-toggle="modal" data-target="#addRecruitmentDetails">ADD</a>
                        <a class="btn btn-default btndefault" @click="toggleEditRec">EDIT</a>
                        <a class="btn btn-default btndefault offsetDelete" @click="toggleDeleteRec">DELETE</a>
                    </div>
                @endif
                @include('mrf.modal.recruitment-staff')
            </div>
        @endif

</div>