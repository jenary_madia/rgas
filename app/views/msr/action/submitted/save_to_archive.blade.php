{{ Form::open(array('url' => array('msr/submitted_action',$MSRDetails['id']), 'method' => 'post', 'files' => true)) }}
<div class="clear_20"></div>
<span class="action-label labels">ACTION</span>
<div class="form_container msr-form-container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 comment-box">
            <label class="labels pull-left">MESSAGE:</label>
            <textarea rows="3" class="form-control pull-left" readonly>@foreach (explode('|',$MSRDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            <label class="labels pull-left">COMMENT:</label>
            <textarea rows="3" class="form-control pull-left" name="comment"></textarea>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div class="row actions">
                <div class="col-md-6">
                    <label class="button_notes"><strong>SAVE</strong> REQUEST TO ARCHIVE</label>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="toArchive">SAVE</button>
                </div>
            </div>
            <div class="row actions">
                <div class="col-md-12 text-center">
                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- end of form_container -->
{{ Form::close() }}