@extends('template/header')

@section('content')
    <div id="msr_create" v-cloak>
        {{ Form::open(['url' => ['msr/edit_action',$MSRDetails['id']], 'method' => 'post', 'files' => true]) }}
        <div class="form_container msr-form-container">
            <div class="container-header"><h5 class="text-center"><strong>MANPOWER SERVICE REQUEST FORM</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ Session::get('employee_name') }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" value="{{ $MSRDetails['reference_no'] }}" class="form-control pull-right"/>
                </div>
                <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ Session::get('employeeid') }}" />
                    </div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">DATE FILED:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" disabled value="{{$MSRDetails['datefiled'] }}"/>
                    </div>
                    <div class="clear_10"></div>
                        <div class="col-md-6">
                            <label class="labels required pull-left">COMPANY:</label>
                            <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ Session::get('company') }}" />
                        </div>
                        <div class="col-md-6">
                            <label class="labels required pull-left">STATUS:</label>
                            <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="{{ $MSRDetails['status']  }}" />
                        </div>
                    <div class="clear_10"></div>
                        <div class="col-md-6">
                            <label class="labels required pull-left">DEPARTMENT:</label>
                            <input readonly="readonly" type="text" class="form-control pull-right" name="department" value="{{ Session::get('dept_name') }}" />
                        </div>
                        <div class="col-md-6">
                            <label class="labels pull-left">CONTACT NUMBER:</label>
                            <input type="text" class="form-control pull-right" name="contactNumber" disabled maxlength="20" value="{{ Input::old('contactNumber',$MSRDetails['contactno']) }}"/>
                        </div>
                    <div class="clear_10"></div>
                        <div class="col-md-6">
                            <label class="labels pull-left">SECTION:</label>
                            <input type="text" class="form-control pull-right" maxlength="25" name="section" disabled value="{{ Input::old('section',($MSRDetails['sectionid'] ? $MSRDetails['section']['sect_name'] : $MSRDetails['othersection']))}}" />
                        </div>
                </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>REQUEST DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-4">
                    <label class="labels required">REQUEST TYPE:</label>
                    <div class="pull-right" style="width : 200px;display:inline-block;">
                        <div class="pull-left" style="width : 50%;">
                            {{ Form::radio('requestType', 'pooling', ( $MSRDetails['pooling'] == 1 ),['disabled']) }} <label class="radio-label">POOLING</label>
                        </div>
                        <div class="pull-left" style="width : 50%;">
                            {{ Form::radio('requestType', 'screening', ( $MSRDetails['screening'] == 1 ),['disabled']) }} <label class="radio-label">SCREENING</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">INITIATED BY:</label>
                    <input type="text" maxlength="100" style="max-width : 239px;" class="form-control pull-right" disabled name="initiatedBy" value="{{ $MSRDetails['initiatedby'] }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-5">
                    <label class="labels required">URGENCY:</label>
                    <div class="pull-right" style="width : 289px;display:inline-block;">
                        <div class="pull-left" style="width : 35%;">
                            {{ Form::radio('urgency', 'rush', ( $MSRDetails['rush'] == 1),['disabled'] ) }} <label class="radio-label">RUSH</label>
                        </div>
                        <div class="pull-left" style="width : 65%;">
                            {{ Form::radio('urgency', 'normal', ( $MSRDetails['normalprio'] == 1),['disabled'] ) }} <label class="radio-label">NORMAL PRIORITY</label>
                        </div>
                    </div>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-4">
                    <label class="labels required pull-left">REASON FOR REQUEST: &nbsp;</label>
                    <textarea id="" style="width :170px;" class="form-control" name="reason"  disabled>{{ $MSRDetails['reason'] }}</textarea>
                </div>
                <div class="col-md-3">
                    <label class="labels required pull-left">SCREENING DATE:</label>
                    <div style="width : 120px;" class="pull-right input-group bootstrap-timepicker timepicker">
                        <input type="text" readonly name="screeningDate" disabled value="{{ $MSRDetails['screeningdate'] }}" class="date_picker form-control input-small">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                    
                    <br>
                    <br>

                    <label class="labels required pull-left">BRIEFING DATE:</label>
                    <div style="width : 120px;" class="pull-right input-group bootstrap-timepicker timepicker">
                        <input type="text" readonly name="briefingDate" disabled value="{{$MSRDetails['briefingdate'] }}" class="date_picker form-control input-small">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                        
                </div>

                <div class="col-md-5">
                    <label class="labels required pull-left">WORKPLAN RATE:</label>
                    <textarea style="max-width : 239px;" name="workplan" id="" class="form-control pull-right" disabled cols="10">{{ $MSRDetails['workplanrate'] }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>PROJECT REQUIREMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>AREA</th>
                            <th>NUMBER OF PERSONNEL</th>
                            <th>NUMBER OF WORKING DAYS</th>
                            <th>JOB TITLE/JOB POSITION</th>
                            <th>DATE NEEDED</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="req in projectRequirements" v-bind:class="{ 'active' : req.active }" @click="getForEdit($index,req)">
                        <td>@{{ req.area }}</td>
                        <td>@{{ req.noOfPersonnel }}</td>
                        <td>@{{ req.noOfWorkingDays }}</td>
                        <td>@{{ req.jobTitle }}</td>
                        <td>@{{ req.dateNeeded }}</td>
                        </tr>
                        </tbody>
                        <input type="hidden" name="projectRequirements" value="@{{ projectRequirements | json }}">
                        <input type="hidden" v-model="projectRequirementsOld" value='{{ Input::old("projectRequirements",$MSRDetails['requirements']) }}'>
                        <input type="hidden" name="agencies" value="@{{ agencies | json }}">
                        <input type="hidden" v-model="agenciesOld"  value='{{ Input::old("agencies",$MSRDetails['agencies']) }}'>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="col-md-2">
                    <label class="labels required">OTHER INFORMATION:</label>
                </div>
                <div class="col-md-10">
                    <textarea class="form-control" id="" cols="30" name="otherInformation" disabled>{{ Input::old("otherInformation",$MSRDetails['otherinfo']) }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>RECOMMENDED MANPOWER AGENCIES</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>MANPOWER PROVIDER/AGENCY</th>
                            <th>CONTACT INFORMATION (ADDRESS/CONTACT NUMBER/EMAIL)</th>
                            <th>MANPOWER REMARKS (TO BE FILLED OUT BY THE FINAL RECIPIENT)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="agency in agencies" v-bind:class="{ 'active' : agency.active }" @click="getForEditAgency($index,agency)">
                        <td>@{{ agency.agency }}</td>
                        <td>@{{ agency.contactInfo }}</td>
                        <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear_20"></div>
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        <p><strong>FILER : </strong></p>
                        <div class="attachment_container">
                            @foreach($MSRDetails["attachments"] as $key)
                                <a href="{{ URL::to('/msr/download'.'/'.$MSRDetails['reference_no'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key['original_filename'])) }}">{{ $key["original_filename"] }}</a><br />
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            @if(count($MSRDetails['signatories']) > 0)
                <div>
                    <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>
                    <div class="clear_20"></div>
                    <?php $count = 0 ?>
                    @foreach($endorsed as $signatoryEndorsed)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col1_form_container">
                                    <label class="labels">
                                        @if ($count == 0 )
                                            ENDORSED :
                                        @endif
                                    </label>
                                </div>
                                <div class="col2_form_container">
                                    <input type="text" readonly value="{{ $signatoryEndorsed['employee']['firstname'].' '.$signatoryEndorsed['employee']['middlename'].' '.$signatoryEndorsed['employee']['lastname'] }}"class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pull-right" style="width: 200px">
                                    <input type="text" readonly value="{{ $signatoryEndorsed['approval_date'] }}"class="pull-right form-control placeholders"/>
                                </div>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <?php $count++ ?>
                    @endforeach

                    @foreach($MSRDetails['signatories'] as $signatory)
                        @if($signatory['signature_type'] == 2)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col1_form_container">
                                        <label class="labels">
                                            RECEIVED :
                                        </label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                        @elseif($signatory['signature_type'] == 3)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col1_form_container">
                                        <label class="labels">
                                            PROCESSED :
                                        </label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                        @endif
                    @endforeach
                </div>
            @endif

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    @if($MSRDetails['status'] == 'NEW')
                        <label class="labels pull-left">COMMENT:</label>
                        <textarea rows="3" class="form-control pull-left" disabled name="comment">@foreach (explode('|',$MSRDetails['comment']) as $message){{ json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                    @else
                        <label class="labels pull-left">MESSAGE:</label>
                        <textarea rows="3" class="form-control pull-left" readonly>@foreach (explode('|',$MSRDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                        <br>
                        <label class="labels pull-left">COMMENT:</label>
                        <textarea rows="3" {{ $MSRDetails['status'] == 'FOR APPROVAL' ? "" : "disabled" }} class="form-control pull-left" name="comment"></textarea>
                    @endif
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <button value="cancel" name="action" class="btn btn-default btndefault" {{ $MSRDetails['status'] == 'FOR APPROVAL' ? "" : "disabled" }}>CANCEL REQUEST</button>
                        </div>
                        <div class="col-md-6 text-center">
                            <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->
        {{ Form::close() }}
        @include("msr.modal.modal_create")
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/general.js') }}
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/msr/vue_create.js') }}
@stop
