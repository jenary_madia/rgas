<div class="modal fade" id="addProjectRequirement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD PROJECT REQUIREMENT</h4>
            </div>
            <div class="modal-body">
                <validator name="addProjReqValidator">
                    <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels required">AREA : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toAddProjectReq.area" v-validate:area="['required']">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">NUMBER OF PERSONNEL : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input min="1" type="number" max="99"  class="form-control numeric" v-model="toAddProjectReq.noOfPersonnel" v-validate:personnel="['required']">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">NUMBER OF WORKING DAYS : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input min="1" type="number" max="99"  class="form-control numeric" v-model="toAddProjectReq.noOfWorkingDays" v-validate:workingDays="['required']">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">JOB TITLE/JOB POSITION : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toAddProjectReq.jobTitle" v-validate:jobPosition="['required']">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">DATE NEEDED : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" value="" readonly name="" id="" class="date_picker form-control input-small" v-validate:dateNeeded="['required']"  v-model="toAddProjectReq.dateNeeded">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 pager">
                        <button class="btn btn-default btndefault" :disabled="! $addProjReqValidator.valid" data-dismiss="modal" @click="addProjectReq">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
                </validator>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editProjectRequirement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">EDIT PROJECT REQUIREMENT</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels required">AREA : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditProjectReq.area">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">NUMBER OF PERSONNEL : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditProjectReq.noOfPersonnel">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">NUMBER OF WORKING DAYS : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditProjectReq.noOfWorkingDays">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">JOB TITLE/JOB POSITION : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditProjectReq.jobTitle">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels required">DATE NEEDED : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" value="" readonly name="" id="" class="date_picker form-control input-small" v-model="toEditProjectReq.dateNeeded">
                            <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="col-md-12 pager">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="savePRUpdate">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addManPowerAgency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD RECOMMENDED MANPOWER AGENCY</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels required">AGENCY : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toAddAgency.agency">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">CONTACT INFORMATION : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" maxlength="20" v-model="toAddAgency.contactInfo">
                        </div>
                    </div>

                    <div class="col-md-12 pager">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="addAgency">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editManPowerAgency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD TIME OFFSET REFERENCE DETAILS</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels required">AGENCY : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditAgency.agency">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">CONTACT INFORMATION : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" maxlength="20" v-model="toEditAgency.contactInfo">
                        </div>
                    </div>

                    <div class="col-md-12 pager">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="saveAgencyUpdate">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

