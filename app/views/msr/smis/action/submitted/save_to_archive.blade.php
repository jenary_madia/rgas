{{ Form::open(array('url' => array('msr/smis/submitted_action',$MSRDetails['id']), 'method' => 'post', 'files' => true)) }}
<div class="clear_20"></div>
<span class="action-label labels">ACTION</span>
<div class="form_container msr-form-container">
    <div class="clear_20"></div>
    <div class="textarea_messages_container">
        <div class="row">
            <label class="textarea_inside_label">MESSAGE:</label>
            <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$MSRDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
        </div>
        <div class="clear_20"></div>
        <div class="row">
            <label class="textarea_inside_label">COMMENT:</label>
            <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
        </div>
    </div>
    <div class="clear_10"></div>
    <div class="row">
        <div>
            <div class="comment_container">
                <div class="comment_notes">
                    <label class="button_notes"><strong>SAVE</strong> REQUEST TO ARCHIVE</label>
                </div>
                <div class="comment_button">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="toArchive">SAVE</button>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="comment_container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- end of form_container -->
{{ Form::close() }}