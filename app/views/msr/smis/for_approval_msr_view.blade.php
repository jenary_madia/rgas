@extends('template/header')

@section('content')
    <div id="msr_create" v-cloak>
        <div class="form_container msr-form-container">
            <div class="container-header"><h5 class="text-center"><strong>MANPOWER SERVICE REQUEST FORM</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ $MSRDetails['owner']['firstname'].' '.$MSRDetails['owner']['middlename'].' '.$MSRDetails['owner']['lastname'] }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" value="{{ $MSRDetails['reference_no'] }}" class="form-control pull-right"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ $MSRDetails['owner']['employeeid'] }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DATE FILED:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" disabled value="{{$MSRDetails['datefiled'] }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">COMPANY:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ $MSRDetails['owner']['company'] }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">STATUS:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="{{ $MSRDetails['status']  }}" />
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DEPARTMENT:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="department"value="{{ $MSRDetails['department']['dept_name'] }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels pull-left">CONTACT NUMBER:</label>
                    <input type="text" class="form-control pull-right" name="contactNumber" disabled maxlength="20" value="{{ Input::old('contactNumber',$MSRDetails['contactno']) }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels pull-left">SECTION:</label>
                    <input type="text" class="form-control pull-right" maxlength="25" name="section" disabled value="{{  ( $MSRDetails['sectionid'] ? $MSRDetails['section']['sect_name'] : $MSRDetails['othersection']) }}" />
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>REQUEST DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-4">
                    <label class="labels required">SOURCE : </label>
                    <div class="pull-right" style="display:inline-block;">
                        {{ Form::radio('source', 'internal', ( $MSRDetails['internal'] == 1 ),['disabled']) }} <label class="radio-label">INTERNAL(I-QUEST)</label>
                        {{ Form::radio('source', 'external', ( $MSRDetails['external'] == 1 ),['disabled']) }} <label class="radio-label">EXTERNAL</label>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">PANELS : </label>
                    <textarea id="" style="max-width : 239px;" class="form-control pull-right" disabled name="panel" >{{ $MSRDetails['panels'] }}</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-4">
                    <label class="labels required">URGENCY:</label>
                    <div class="pull-right" style="display:inline-block;">
                        {{ Form::radio('urgency', 'rush', ( $MSRDetails['rush'] == 1),['disabled'] ) }} <label class="radio-label">RUSH</label>
                        {{ Form::radio('urgency', 'normal', ( $MSRDetails['normalprio'] == 1),['disabled'] ) }} <label class="radio-label">NORMAL PRIORITY</label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">PROJECT TYPE/NAME : &nbsp;</label>
                    <textarea id="" style="max-width : 239px;" class="form-control pull-right" disabled name="projType" >{{ $MSRDetails['projectname'] }}</textarea>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">SAMPLE SIZE PER PANEL : </label>
                    <textarea style="max-width : 239px;" name="sizePerPanel" id="" disabled class="form-control pull-right" cols="10">{{ $MSRDetails['panelsize'] }}</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">BACKGROUND / OBJECTIVE : &nbsp;</label>
                    <textarea id="" style="max-width : 239px;" class="form-control pull-right" disabled name="objective" >{{ $MSRDetails['objective'] }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>PROJECT REQUIREMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12" style="overflow-x: scroll;">
                    <table class="table table-bordered" style=" width : 1600px; ">
                        <thead>
                        <tr>
                            <th rowspan="2">JOB TITLE/JOB POSITION</th>
                            <th colspan="2">RECRUITMENT</th>
                            <th colspan="2">PREPARATION</th>
                            <th colspan="2">BRIEFING</th>
                            <th colspan="3">FIELD WORK</th>
                            <th colspan="2">EDITING</th>
                            <th colspan="2">CODING</th>
                            <th colspan="2">ENCODING</th>
                            <th colspan="2">DATA CLEANING</th>
                        </tr>
                        <tr>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>AREA</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                            <th>DATE NEEDED</th>
                            <th>NUMBER OF PAX</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="req in projectRequirements" v-bind:class="{ 'active' : req.active }" @click="getForEdit($index,req)">
                        <td>@{{ req.jobTitle }}</td>
                        <td>@{{ req.recruitment.from }} - @{{ req.recruitment.to }}</td>
                        <td>@{{ req.recruitment.pax }}</td>
                        <td>@{{ req.preparation.from }} - @{{ req.preparation.to }}</td>
                        <td>@{{ req.preparation.pax }}</td>
                        <td>@{{ req.briefing.from }} - @{{ req.briefing.to }}</td>
                        <td>@{{ req.briefing.pax }}</td>
                        <td>@{{ req.fieldWork.from }} - @{{ req.fieldWork.to }}</td>
                        <td>@{{ req.fieldWork.pax }}</td>
                        <td>@{{ req.fieldWork.area }}</td>
                        <td>@{{ req.editing.from }} - @{{ req.editing.to }}</td>
                        <td>@{{ req.editing.pax }}</td>
                        <td>@{{ req.coding.from }} - @{{ req.coding.to }}</td>
                        <td>@{{ req.coding.pax }}</td>
                        <td>@{{ req.encoding.from }} - @{{ req.encoding.to }}</td>
                        <td>@{{ req.encoding.pax }}</td>
                        <td>@{{ req.dataCleaning.from }} - @{{ req.dataCleaning.to }}</td>
                        <td>@{{ req.dataCleaning.pax }}</td>
                        </tr>
                        </tbody>
                        <input type="hidden" name="projectRequirements" value="@{{ projectRequirements | json }}">
                        <input type="hidden" v-model="projectRequirementsOld" value='{{ Input::old("projectRequirements",json_encode($SMISRequirements)) }}'>
                        <input type="hidden" name="agencies" value="@{{ agencies | json }}">
                        <input type="hidden" v-model="agenciesOld"  value='{{ Input::old("agencies",$MSRDetails['agencies']) }}'>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="col-md-2">
                    <label class="labels required">OTHER INFORMATION:</label>
                </div>
                <div class="col-md-10">
                    <textarea class="form-control" id="" cols="30" name="otherInformation" disabled>{{ Input::old("otherInformation",$MSRDetails['otherinfo']) }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>RECOMMENDED MANPOWER AGENCIES</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>MANPOWER PROVIDER/AGENCY</th>
                            <th>CONTACT INFORMATION (ADDRESS/CONTACT NUMBER/EMAIL)</th>
                            <th>MANPOWER REMARKS (TO BE FILLED OUT BY THE FINAL RECIPIENT)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="agency in agencies" v-bind:class="{ 'active' : agency.active }" @click="getForEditAgency($index,agency)">
                        <td>@{{ agency.agency }}</td>
                        <td>@{{ agency.contactInfo }}</td>
                        <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear_20"></div>
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        <p><strong>FILER : </strong></p>
                        <div class="attachment_container">
                            @foreach($MSRDetails["attachments"] as $key)
                                <a href="{{ URL::to('/msr/download'.'/'.$MSRDetails['reference_no'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key['original_filename'])) }}">{{ $key["original_filename"] }}</a><br />
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @if(count($MSRDetails['signatories']) > 0)
                <div>
                    <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>
                    <div class="clear_20"></div>
                    <?php $count = 0 ?>
                    @foreach($endorsed as $signatoryEndorsed)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col1_form_container">
                                    <label class="labels">
                                        @if ($count == 0 )
                                            ENDORSED :
                                        @endif
                                    </label>
                                </div>
                                <div class="col2_form_container">
                                    <input type="text" readonly value="{{ $signatoryEndorsed['employee']['firstname'].' '.$signatoryEndorsed['employee']['middlename'].' '.$signatoryEndorsed['employee']['lastname'] }}"class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pull-right" style="width: 200px">
                                    <input type="text" readonly value="{{ $signatoryEndorsed['approval_date'] }}"class="pull-right form-control placeholders"/>
                                </div>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <?php $count++ ?>
                    @endforeach

                    @foreach($MSRDetails['signatories'] as $signatory)
                        @if($signatory['signature_type'] == 2)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col1_form_container">
                                        <label class="labels">
                                            RECEIVED :
                                        </label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                        @elseif($signatory['signature_type'] == 3)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col1_form_container">
                                        <label class="labels">
                                            PROCESSED :
                                        </label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div><!-- end of form_container -->
        @if($method == 'view')
            @include('msr.action.view')
        @else
            @if($MSRDetails['external'])
                @if(Session::get("desig_level") == 'head')
                    @include('msr.action.for_approval.head_view')
                @elseif(Session::get("desig_level") != 'head')
                    @if(count($MSRDetails['signatories']))
                        @include('msr.action.for_approval.other_superior_view')
                    @else()
                        @include('msr.action.for_approval.superior_view')
                    @endif
                @endif
            @elseif($MSRDetails['internal'])
                @include('msr.smis.action.for_approval.iquest')
            @endif
        @endif

    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/general.js') }}
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/msr/vue_create.js') }}
@stop
