@extends('template/header')

@section('content')
    {{ Form::open(array('url' => 'msr/smis/export', 'method' => 'post', 'files' => true ,'id' => 'receiverSearch')) }}
    <div id="wrap">
        <div class="wrapper">
            DEPARTMENT : <select name="" id="department">
                <option value="0">ALL</option>
                @foreach($departments as $department)
                    <option value="{{ $department['id'] }}">{{ $department['dept_name'] }}</option>
                @endforeach
            </select>
            <table id="submittedMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="submittedMSR">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">SUBMITTED MSR</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project title/name</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
        </div>
        @include("msr.smis.modal.generate_report")
    </div>
    {{ Form::close() }}
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/general.js') }}
    {{ HTML::script('/assets/js/msr/smis_datatables.js') }}
@stop
