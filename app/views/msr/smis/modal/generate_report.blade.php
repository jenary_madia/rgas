<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">GENERATE REPORT</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels">FILE NAME : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" name="filename" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">DEPARTMENT : </label>
                    </div>
                    <div class="col-md-8">
                        <select name="department" id="department-inside" class="form-control">
                            <option value="0">ALL</option>
                            @foreach($departments as $department)
                                <option value="{{ $department['id'] }}">{{ $department['dept_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="" class="labels">STATUS : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="status" id="status" class="form-control">
                                <option value="ALL">ALL</option>
                                <option value="FOR PROCESSING">FOR PROCESSING</option>
                                <option value="PROCESSED">PROCESSED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">FROM : </label>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <div style="width : 200px;" class="pull-left input-group bootstrap-timepicker timepicker">
                                <input type="text" name="from" readonly id="from" class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="" class="labels">TO : </label>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <div style="width : 200px;" class="pull-left input-group bootstrap-timepicker timepicker">
                                <input type="text" name="to" readonly id="to" class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 pager text-center">
                        <button class="btn btn-default btndefault" name="action" value="generate">GENERATE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

