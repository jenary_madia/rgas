@extends('template/header')

@section('content')
    <div id="wrap">
        <div class="clear_20"></div>
        <div class="wrapper">
            {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMSR')) }}
            <table id="myMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMSR-SMIS">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1">My MSR</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project Type/Name</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            {{ Form::close() }}
        </div>

        <div class="clear_20"></div>
        <div class="wrapper">
            {{--            {{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formMyleaves')) }}--}}
            <table id="superiorMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorMSR-SMIS">
                <thead>
                <tr role="row">
                    <th class="" style="text-align : left" role="columnheader" rowspan="1">FOR APPROVAL</th>
                </tr>
                <tr role="row">
                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project Type/Name</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                </tr>
                </thead>
            </table>
            {{--{{ Form::close() }}--}}
        </div>
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/smis_datatables.js') }}
@stop
