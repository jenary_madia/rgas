@extends('template/header')

@section('content')
    <div id="msr_create" v-cloak>
        {{ Form::open(['url' => ['msr/edit_action',$MSRDetails['id']], 'method' => 'post', 'files' => true]) }}
        <div class="form_container msr-form-container">
            <div class="container-header"><h5 class="text-center"><strong>MANPOWER SERVICE REQUEST FORM</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ Session::get('employee_name') }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" value="{{ $MSRDetails['reference_no'] }}" class="form-control pull-right"/>
                </div>
                <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ Session::get('employeeid') }}" />
                    </div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">DATE FILED:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" value="{{ Input::old('contactNumber',$MSRDetails['datefiled']) }}"/>
                    </div>
                <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">COMPANY:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ Session::get('company') }}" />
                    </div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">STATUS:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="{{ $MSRDetails['status']  }}" />
                    </div>
                <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels required pull-left">DEPARTMENT:</label>
                        <input readonly="readonly" type="text" class="form-control pull-right" name="department" value="{{ Session::get('dept_name') }}" />
                    </div>
                    <div class="col-md-6">
                    <label class="labels pull-left">CONTACT NUMBER:</label>
                        <input type="text" class="form-control pull-right" name="contactNumber" maxlength="20" value="{{ Input::old('contactNumber',$MSRDetails['contactno']) }}"/>
                    </div>
                <div class="clear_10"></div>
                    <div class="col-md-6">
                        <label class="labels pull-left">SECTION:</label>
                        <input type="text" class="form-control pull-right" maxlength="25" name="section" value="{{ Input::old('section',($MSRDetails['sectionid'] ? $MSRDetails['section']['sect_name'] : $MSRDetails['othersection']))}}" />
                    </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>REQUEST DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-4">
                    <label class="labels required">REQUEST TYPE:</label>
                    <div class="pull-right" style="width : 200px;display:inline-block;">
                        <div class="pull-left" style="width : 50%;">
                            {{ Form::radio('requestType', 'pooling', ( Input::old('requestType') == 'pooling' || $MSRDetails['pooling'] == 1 )) }} <label class="radio-label">POOLING</label>
                        </div>
                        <div class="pull-left" style="width : 50%;">
                            {{ Form::radio('requestType', 'screening', ( Input::old('requestType') == 'screening' || $MSRDetails['screening'] == 1 )) }} <label class="radio-label">SCREENING</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">INITIATED BY:</label>
                    <input type="text" maxlength="100" style="max-width : 239px;" class="form-control pull-right" name="initiatedBy" value="{{ Input::old("initiatedBy",$MSRDetails['initiatedby']) }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-5">
                    <label class="labels required">URGENCY:</label>
                    <div class="pull-right" style="width : 289px;display:inline-block;">
                        <div class="pull-left" style="width : 35%;">
                            {{ Form::radio('urgency', 'rush', (Input::old('urgency') == 'rush' || $MSRDetails['rush'] == 1)) }} <label class="radio-label">RUSH</label>
                        </div>
                        <div class="pull-left" style="width : 65%;">
                            {{ Form::radio('urgency', 'normal', (Input::old('urgency') == 'normal' || $MSRDetails['normalprio'] == 1 )) }} <label class="radio-label">NORMAL PRIORITY</label>
                        </div>
                    </div>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-4">
                    <label class="labels required pull-left">REASON FOR REQUEST: &nbsp;</label>
                    <textarea id="" style="width :170px;" class="form-control" name="reason" >{{ Input::old("reason",$MSRDetails['reason']) }}</textarea>
                </div>
                <div class="col-md-3">
                    <label class="labels required pull-left">SCREENING DATE:</label>
                    <div style="width : 120px;" class="pull-right input-group bootstrap-timepicker timepicker">
                        <input type="text" readonly name="screeningDate" value="{{ Input::old("screeningDate",$MSRDetails['screeningdate']) }}" class="date_picker form-control input-small">
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                    
                    <br>
                    <br>

                    <label class="labels required pull-left">BRIEFING DATE:</label>
                    <div style="width : 120px;" class="pull-right input-group bootstrap-timepicker timepicker">
                        <input type="text" readonly name="briefingDate" value="{{ Input::old("briefingDate",$MSRDetails['briefingdate']) }}" class="date_picker form-control input-small">
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                        
                </div>

                <div class="col-md-5">
                    <label class="labels required pull-left">WORKPLAN RATE:</label>
                    <textarea style="max-width : 239px;" name="workplan" id="" class="form-control pull-right" cols="10">{{ Input::old("workplan",$MSRDetails['workplanrate']) }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>PROJECT REQUIREMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>AREA</th>
                                <th>NUMBER OF PERSONNEL</th>
                                <th>NUMBER OF WORKING DAYS</th>
                                <th>JOB TITLE/JOB POSITION</th>
                                <th>DATE NEEDED</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="req in projectRequirements" v-bind:class="{ 'active' : req.active }" @click="getForEdit($index,req)">
                                <td>@{{ req.area }}</td>
                                <td>@{{ req.noOfPersonnel }}</td>
                                <td>@{{ req.noOfWorkingDays }}</td>
                                <td>@{{ req.jobTitle }}</td>
                                <td>@{{ req.dateNeeded }}</td>
                            </tr>
                        </tbody>
                        <input type="hidden" name="projectRequirements" value="@{{ projectRequirements | json }}">
                        <input type="hidden" v-model="projectRequirementsOld" value='{{ Input::old("projectRequirements",$MSRDetails['requirements']) }}'>
                        <input type="hidden" name="agencies" value="@{{ agencies | json }}">
                        <input type="hidden" v-model="agenciesOld"  value='{{ Input::old("agencies",$MSRDetails['agencies']) }}'>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-12">
                    <a class="btn btn-default btndefault" data-toggle="modal" data-target="#addProjectRequirement">ADD</a>
                    <a class="btn btn-default btndefault" @click="toggleEditProjReq">EDIT</a>
                    <a class="btn btn-default btndefault offsetDelete" @click="toggleDeleteProjReq">DELETE</a>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="col-md-2">
                    <label class="labels required">OTHER INFORMATION:</label>
                </div>
                <div class="col-md-10">
                    <textarea class="form-control" id="" cols="30" name="otherInformation">{{ Input::old("otherInformation",$MSRDetails['otherinfo']) }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>RECOMMENDED MANPOWER AGENCIES</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>MANPOWER PROVIDER/AGENCY</th>
                            <th>CONTACT INFORMATION (ADDRESS/CONTACT NUMBER/EMAIL)</th>
                            <th>MANPOWER REMARKS (TO BE FILLED OUT BY THE FINAL RECIPIENT)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="agency in agencies" v-bind:class="{ 'active' : agency.active }" @click="getForEditAgency($index,agency)">
                        <td>@{{ agency.agency }}</td>
                        <td>@{{ agency.contactInfo }}</td>
                        <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-12">
                    <a class="btn btn-default btndefault" data-toggle="modal" data-target="#addManPowerAgency">ADD</a>
                    <a class="btn btn-default btndefault" @click="toggleEditAgency">EDIT</a>
                    <a class="btn btn-default btndefault offsetDelete" @click="toggleDeleteAgency">DELETE</a>
                </div>
                <div class="clear_20"></div>
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                        <div id="attachments">
                            @if(Input::old("files",$MSRDetails['attachments']))
                                @for ($i = 0; $i < count(Input::old("files",$MSRDetails['attachments'])); $i++)
                                    <p>
                                        {{ Input::old("files",$MSRDetails['attachments'])[$i]["original_filename"] }} | {{ Input::old("files",$MSRDetails['attachments'])[$i]["filesize"] }}KB
                                        <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files",$MSRDetails['attachments'])[$i]["random_filename"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files",$MSRDetails['attachments'])[$i]["original_filename"] }}'>
                                        <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files",$MSRDetails['attachments'])[$i]["filesize"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files",$MSRDetails['attachments'])[$i]["mime_type"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files",$MSRDetails['attachments'])[$i]["original_extension"] }}'>
                                        <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                    </p>
                                @endfor
                            @endif
                        </div>
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}">
                        </span>
                    </div>
                </div>
            </div>


        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    @if($MSRDetails['status'] == 'NEW')
                        <label class="labels pull-left">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment">{{ Input::old("comment",json_decode($MSRDetails['comment'],true)['message']) }}</textarea>
                    @else
                        <label class="labels pull-left">COMMENT:</label>
                        <textarea rows="3" class="form-control pull-left" name="comment">{{ Input::old("comment") }}</textarea>
                    @endif
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row actions">
                        <div class="col-md-6">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="row actions">
                        <div class="col-md-6">
                            <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->
        {{ Form::close() }}
        @include("msr.modal.modal_create")
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/general.js') }}
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/msr/vue_create.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
        @if(Input::old("files",$MSRDetails['attachments']))
            var file_counter = {{ $i }};
        @else
            var file_counter = 0;
        @endif
        var allowed_file_count = 10;
        var allowed_total_filesize = 20971520;
    </script>
    {{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop
