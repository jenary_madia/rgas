@extends('template/header')

@section('content')
    <div id="app">
        <div class="form_container">
            <div class="container-header"><h5 class="text-center"><strong>NOTIFICATION SLIP</strong></h5></div>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NAME:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="employeeName" value="{{ $notifDetails['firstname'].' '.$notifDetails['middlename'].' '.$notifDetails['lastname'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REFERENCE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" value="{{ $notifDetails['documentcode'].' '.$notifDetails['codenumber'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="employeeNumber" value="{{ $notifDetails['employeeid'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="dateFiled" value="{{ $notifDetails['datecreated'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">COMPANY:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="company" value="{{ $notifDetails['company'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">STATUS:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="status" value="{{ $notifDetails['status'] }}" />
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DEPARTMENT:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="department" value="{{ $notifDetails['department'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">CONTACT NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="contactNumber" maxlength="20" value="{{ $notifDetails['contact_no'] }}" readonly/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="section" value="{{ $notifDetails['section'] }}" readonly/>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">TYPE OF NOTIFICATION:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control text-uppercase" value="{{ $notifDetails['noti_type'] }}" readonly>
                    </div>
                </div>
            </div>
            @if($notifDetails['noti_type'] == "offset")
                @include("ns.layouts.notifications.offset")
            @elseif($notifDetails['noti_type'] == "undertime")
                @include("ns.layouts.notifications.undertime")
            @elseif($notifDetails['noti_type'] == "cut_time")
                @include("ns.layouts.notifications.cut_time")
            @elseif($notifDetails['noti_type'] == "TKCorction")
                @include("ns.layouts.notifications.TKCorction")
            @elseif($notifDetails['noti_type'] == "official")
                @include("ns.layouts.notifications.official")
            @endif
    {{ Form::open(array('url' => array('submitted/notification',$notifDetails['id']), 'method' => 'post', 'files' => true)) }}
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="attachment-box">
                        <p><strong>FILER : </strong></p>
                        @for ($i = 1; $i < 6; $i++)
                            <a href="{{ URL::to('/ns/download'.'/'.$notifDetails['referenceNo'].'/'.json_decode($notifDetails["attach$i"],true)["random_filename"]).'/'.CIEncrypt::encode(json_decode($notifDetails["attach$i"],true)["original_filename"])}}">{{ json_decode($notifDetails["attach$i"],true)["original_filename"] }}</a><br />
                        @endfor
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="attachment-box">
                        <p><strong>CHRD : </strong></p>
                        <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                        <div id="attachments">
                            @if(Input::old("files"))
                                {{--{{ var_dump(Input::old("files")) }}--}}
                                @for ($i = 1; $i <= count(Input::old("files")); $i++)
                                    <p>
                                        {{ Input::old("files")[$i]["original_filename"] }} | {{ Input::old("files")[$i]["filesize"] }}
                                        <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files")[$i]["random_filename"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files")[$i]["original_filename"] }}'>
                                        <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files")[$i]["filesize"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files")[$i]["mime_type"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files")[$i]["original_extension"] }}'>
                                        <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                    </p>
                                @endfor
                            @endif
                        </div>
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                    </div>
                </div>
            </div>
            <div {{ (count($notifSignatories) == 0 ? "hidden" : "") }} >
                <div class="clear_20"></div>
                <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>

                <br>
                @foreach($notifSignatories as $signatory)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col1_form_container">
                                <label class="labels">{{ ($signatory["signature_type"] == 1 ? "NOTED" : "APPROVED") }} :</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right" style="width: 200px">
                                <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                            </div>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                @endforeach
            </div>
        </div><!-- end of form_container -->
        <div class="clear_20"></div>
        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach ( explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <button {{ $method == 'process' ? "" : "disabled" }} class="text-center btn btn-default btndefault" name="action" value="return">RETURN TO FILER</button>
                </div>
                <div class="col-md-4 text-center">
<<<<<<< HEAD
                    <a class="btn btn-default btndefault" href="{{}}">BACK</a>
=======
                    <a class="btn btn-default btndefault" href="{{ url('submitted/leaves_notifications') }}">BACK</a>
>>>>>>> dae6d83fe481abd2c7c3bd7c68af1783bf7b9911
                </div>
                <div class="col-md-4 text-center">
                    <button {{ $method == 'process' ? "" : "disabled" }} type="submit" class="btn  URL::url() btn-default btndefault" name="action" value="cancel">CANCEL NOTIFICATION</button>
                </div>
            </div>
        </div><!-- end of form_container -->
    {{ Form::close() }}
    </div>
@stop
@section('js_ko')
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });

        @if(Input::old("files"))
            var file_counter = {{ $i }};
        @else
            var file_counter = 0;
        @endif

        var file_counter = 0;
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;
    </script>
    {{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop
