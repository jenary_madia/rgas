@extends('template.header')

@section('content')
    <div id="peISView">
        <div class="form_container">
            <h5 class="text-center"><strong>NOTIFICATION SLIP</strong></h5>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">DEPARTMENT</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['department']['dept_name']}}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">REFERENCE NUMBER</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="reference" readonly value="{{ $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']}}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['section']['sect_name']}}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['datecreated']}}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table border="1">
                            <thead>
                            <tr>
                                <th>Employee Name</th>
                                <th>Employee Number</th>
                                <th>Leave Type</th>
                                <th>Duration of Leave</th>
                                <th>Total Leave Days</th>
                                <th>Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            <input type="hidden" value='{{ $leaveBatch['batchDetails']}}' v-model="batchDetails">
                            <tr v-for="employee in employeesToFile">
                                <td>@{{ employee.employee_name }}</td>
                                <td>@{{ employee.employee_number }}</td>
                                <td>@{{ employee.leave_item.text }}</td>
                                <td>@{{ employee.duration }}</td>
                                <td>@{{ employee.noofdays }}</td>
                                <td>@{{ employee.reason }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clear_10"></div>
                </div>
            </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>
        {{ Form::open(array('url' => array('submitted/pe_leave',$leaveBatch['id']), 'method' => 'post', 'files' => true)) }}
        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach ( explode('|',$leaveBatch['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <button {{ $method == 'process' ? "" : "disabled" }} class="text-center btn btn-default btndefault" name="action" value="return">RETURN TO FILER</button>
                </div>
                <div class="col-md-4 text-center">
                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                </div>
                <div class="col-md-4 text-center">
                    <button {{ $method == 'process' ? "" : "disabled" }} type="submit" class="btn btn-default btndefault" name="action" value="cancel">CANCEL LEAVE</button>
                </div>
            </div>
        </div><!-- end of form_container -->

        {{ Form::close() }}
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/leaves/vue-pe-ISview.js') }}

@stop