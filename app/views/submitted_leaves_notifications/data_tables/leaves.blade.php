<div class="clear_20"></div>
{{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formProcessLeave')) }}
    <div class="datatable_holder">
        <table id="leaves_receiver" cellpadding="0" cellspacing="0" border="0" class="display dataTable submittedEntries" width="100%" aria-describedby="leaves_receiver">
            <thead>
            <tr role="row">
                <th class="" role="columnheader" rowspan="1">Submitted Leaves</th>
            </tr>
            <tr role="row">
                <th class="text-left sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Approved</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Employee Name</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Date Of Leave</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Total Leave Days</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Reason</th>
                <th class="text-center sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
            </tr>
            </thead>
        </table>
    </div>
{{ Form::close() }}