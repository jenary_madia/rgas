<div class="clear_20"></div>
{{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formProcessLeave')) }}
<div class="datatable_holder">
    <table id="notif_undertime" cellpadding="0" cellspacing="0" border="0" class="display dataTable submittedEntries" width="100%" aria-describedby="notif_undertime">
        <thead>
        <tr role="row">
            <th class="" role="columnheader" rowspan="1">submitted Notification - Undertime</th>
        </tr>
        <tr role="row">
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Approved</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Employee Name</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Date Of Undertime</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Reason</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
        </tr>
        </thead>
    </table>
</div>
{{ Form::close() }}