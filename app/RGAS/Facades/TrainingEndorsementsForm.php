<?php namespace app\RGAS\Facades;

use Illuminate\Support\Facades\Facade;

class TrainingEndorsementsForm extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'trainingendorsementsform'; }

}