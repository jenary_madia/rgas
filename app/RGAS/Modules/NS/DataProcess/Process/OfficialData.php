<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:28 PM
 */

namespace RGAS\Modules\NS\DataProcess\Process;


use RGAS\Modules\NS\DataProcess\Contracts\DataParseRepository;
use Notifications;
use Session;
use Redirect;
use RGAS\Modules\NS\NS;
use Input;
use RGAS\Modules\NS\Logs;
use Receivers;
use NotifSignatory;
use Employees;

class OfficialData implements DataParseRepository
{
    private $sessionParse;
    private $logs;
    private $NS;
    protected $superiorEmail;
    public function __construct()
    {
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
        $this->NS = new NS;
        $this->logs = new Logs;
        $this->sessionParse = array(
            'firstname' => Session::get('firstname'),
            'middlename' => Session::get('middlename'),
            'lastname' => Session::get('lastname'),
            'employeeid' => Session::get('employeeid'),
            'company' => Session::get('company'),
            'department' => Session::get('dept_name'),
            'ownerid' => Session::get('employee_id')
        );
    }

    public function forSend($inputs)
    {
        $dataParse = array(
            'documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'duration_type'=> Input::get('OBdurationType'),
            'from_inout'=> Input::get('OBfromStartTime'),
            'from_out'=> Input::get('OBfromEndTime'),
            'to_inout'=> Input::get('OBtoStartTime'),
            'to_out'=> Input::get('OBtoEndTime'),
            'reason'=> Input::get('OBreason'),
            'destination'=> Input::get('OBdestination'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            ),
            'curr_emp' => Session::get('superiorid')
        );

        if(Input::get('OBfrom_date')) {
            $dataParse['from_date'] = Input::get('OBfrom_date');
        }
        if(Input::get('OBto_date')) {
            $dataParse['to_date'] = Input::get('OBto_date');
        }
        if(Input::get('OBtotalDays')) {
            $dataParse['totaldays'] = Input::get('OBtotalDays');
        }

        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);

        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{

                return Redirect::back()
                    ->with("errorMessage","No Immediate superior declared");

            }

        }
        
        $creation = Notifications::create($dataGather);
        $files = Input::get("files");
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        if($files) {
            $this->logs->AU002($creation->id, 'notifications', 'id', json_encode($files), $dataGather['documentcode'].'-'.$dataGather['codenumber']);
        }

        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $dataGather['documentcode'].'-'.$dataGather['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail
        );
        
        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }

                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Successfully approved and sent to HR Receiver");
            }else{
                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Notification request successfully sent to Immediate Superior for approval");
            }
        }
    }
    
    public function forSave($inputs)
    {
        $dataParse = array(
            'documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'duration_type'=> Input::get('OBdurationType'),
            'from_inout'=> Input::get('OBfromStartTime'),
            'from_out'=> Input::get('OBfromEndTime'),
            'to_inout'=> Input::get('OBtoStartTime'),
            'to_out'=> Input::get('OBtoEndTime'),
            'reason'=> Input::get('OBreason'),
            'destination'=> Input::get('OBdestination'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            ),
        );

        if(Input::get('OBfrom_date')) {
            $dataParse['from_date'] = Input::get('OBfrom_date');
        }
        if(Input::get('OBto_date')) {
            $dataParse['to_date'] = Input::get('OBto_date');
        }
        if(Input::get('OBtotalDays')) {
            $dataParse['totaldays'] = Input::get('OBtotalDays');
        }
        
        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);
        $creation = Notifications::create($dataGather);
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        $files = Input::get("files");
        if($files) {
            $this->logs->AU002($creation->id, 'notifications', 'id', json_encode($files), $dataGather['documentcode'].'-'.$dataGather['codenumber']);
        }
        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            return Redirect::to("ns/notifications")
                ->with('successMessage',"Notification request successfully saved and can be edited later");
        }
    }

    public function reSave($inputs,$id) {
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));
        $notifDetails = $notification->first();
        if ($notifDetails['status'] != 'NEW') {

            $signatories = NotifSignatory::where('notification_id', $notifDetails["id"]);

            if ($signatories->count() > 0) {

                $signatories->delete();

            }

        }
        $dataParse = ['datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'duration_type'=> Input::get('OBdurationType'),
            'from_inout'=> Input::get('OBfromStartTime'),
            'from_out'=> Input::get('OBfromEndTime'),
            'to_inout'=> Input::get('OBtoStartTime'),
            'to_out'=> Input::get('OBtoEndTime'),
            'reason'=> Input::get('OBreason'),
            'destination'=> Input::get('OBdestination'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            )
        ];

        if(Input::get('OBfrom_date')) {
            $dataParse['from_date'] = Input::get('OBfrom_date');
        }
        if(Input::get('OBto_date')) {
            $dataParse['to_date'] = Input::get('OBto_date');
        }
        if(Input::get('OBtotalDays')) {
            $dataParse['totaldays'] = Input::get('OBtotalDays');
        }
        
        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);
        $creation = $notification->update($dataGather);
        $this->logs->AU004($notifDetails['id'],'notifications','id',json_encode($dataGather),$notifDetails['documentcode'].'-'.$notifDetails['codenumber'],json_encode($notifDetails));
        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            return Redirect::to("ns/notifications")
                ->with('successMessage',"Notification request successfully updated");
        }
    }
    public function reSend($inputs,$id) {
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $notifDetails = $notification->first();

        if ($notifDetails['status'] != 'NEW') {

            $signatories = NotifSignatory::where('notification_id', $notifDetails["id"]);

            if ($signatories->count() > 0) {

                $signatories->delete();

            }

        }

        $dataParse = ['datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'duration_type'=> Input::get('OBdurationType'),
            'from_inout'=> Input::get('OBfromStartTime'),
            'from_out'=> Input::get('OBfromEndTime'),
            'to_inout'=> Input::get('OBtoStartTime'),
            'to_out'=> Input::get('OBtoEndTime'),
            'reason'=> Input::get('OBreason'),
            'destination'=> Input::get('OBdestination'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))),
            'curr_emp' => Session::get("superiorid")
        ];

        if(Input::get('OBfrom_date')) {
            $dataParse['from_date'] = Input::get('OBfrom_date');
        }
        if(Input::get('OBto_date')) {
            $dataParse['to_date'] = Input::get('OBto_date');
        }
        if(Input::get('OBtotalDays')) {
            $dataParse['totaldays'] = Input::get('OBtotalDays');
        }
        
        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);

        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{

                return Redirect::back()
                    ->with("errorMessage","No Immediate superior declared");

            }

        }
        
        $creation = $notification->update($dataGather);

        $this->logs->AU004($notifDetails['id'],'notifications','id',json_encode($dataGather),$notifDetails['documentcode'].'-'.$notifDetails['codenumber'],json_encode($notifDetails));
        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $notifDetails['documentcode'].'-'.$notifDetails['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail
        );
        
        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }

                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Successfully approved and sent to HR Receiver");
            }else{
                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Notification request successfully sent to Immediate Superior for approval");
            }
        }
            
    }

}