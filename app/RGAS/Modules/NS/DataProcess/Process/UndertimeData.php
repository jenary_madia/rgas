<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 18/07/2016
 * Time: 5:22 PM
 */

namespace RGAS\Modules\NS\DataProcess\Process;
use Redirect;
use Session;
use RGAS\Modules\NS\DataProcess\Contracts\DataParseRepository;
use RGAS\Modules\NS\NS;
use RGAS\Modules\NS\Logs;
use Notifications;
use NotifSignatory;
use Input;
use Mail;
use Receivers;
use Employees;

class UndertimeData implements DataParseRepository
{
    private $NS;
    private $logs;
    protected $superiorEmail;
    public function __construct()
    {
        $this->NS = new NS;
        $this->logs = new Logs;
        $this->sessionParse = array(
            'firstname' => Session::get('firstname'),
            'middlename' => Session::get('middlename'),
            'lastname' => Session::get('lastname'),
            'employeeid' => Session::get('employeeid'),
            'company' => Session::get('company'),
            'department' => Session::get('dept_name'),
            'ownerid' => Session::get('employee_id')
        );
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
    }

    public function forSend($inputs)
    {
        $dataParse = ['documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'schedule_type' => Input::get('UTschedule_type'),
            'to_inout' => Input::get('UTtime_out'),
            'reason' => Input::get('UTreason'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            ),
            'curr_emp' => Session::get('superiorid')];

        if(Input::has("UTdate")) {
            $dataParse['from_date'] = Input::get('UTdate');
        }

        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);

        if (Input::get('UTschedule_type')== 'special') {
            $dataParse['schedule_time'] = Input::get('UTschedule_time_out');
            $dataParse['undertime_day'] = json_encode(Input::get('UTday'));
        }elseif (Input::get("UTschedule_type")=='compressed') {
            $dataParse['undertime_day'] = json_encode(["Saturday","Sunday"]);
        }elseif (Input::get("UTschedule_type")=='regular') {
            $dataParse['undertime_day'] = json_encode(["Sunday"]);
        }

        $dataGather = array_merge($this->sessionParse,$dataParse);
        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{

                return Redirect::back()
                    ->with("errorMessage","No Immediate superior declared");

            }

        }


        $creation = Notifications::create($dataGather);
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        $files = Input::get("files");
        if($files) {
            $this->logs->AU002($creation->id, 'notifications', 'id', json_encode($files), $dataGather['documentcode'].'-'.$dataGather['codenumber']);
        }
        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $dataGather['documentcode'].'-'.$dataGather['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
           $this->superiorEmail
        );

        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }

                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Successfully approved and sent to HR Receiver");
            }else{
                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Notification request successfully sent to Immediate Superior for approval");
            }
        }
    }

    public function forSave($inputs)
    {
        $dataParse = ['documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'schedule_type' => Input::get('UTschedule_type'),
            'to_inout' => Input::get('UTtime_out'),
            'reason' => Input::get('UTreason'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            )];

        if(Input::has("UTdate")) {
            $dataParse['from_date'] = Input::get('UTdate');
        }

        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);

        if (Input::get('UTschedule_type')== 'special') {
            $dataParse['schedule_time'] = Input::get('UTschedule_time_out');
            $dataParse['undertime_day'] = json_encode(Input::get('UTday'));
        }elseif (Input::get("UTschedule_type")=='compressed') {
            $dataParse['undertime_day'] = json_encode(["Saturday","Sunday"]);
        }elseif (Input::get("UTschedule_type")=='regular') {
            $dataParse['undertime_day'] = json_encode(["Sunday"]);
        }

        $dataGather = array_merge($this->sessionParse,$dataParse);
        $creation = Notifications::create($dataGather);
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        $files = Input::get("files");
        if($files) {
            $this->logs->AU002($creation->id, 'notifications', 'id', json_encode($files), $dataGather['documentcode'].'-'.$dataGather['codenumber']);
        }
        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            return Redirect::to("ns/notifications")
                ->with('successMessage',"Notification request successfully save");
        }
    }

    public function reSave($inputs,$id) {
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));
        $dataParse = ['datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'schedule_type' => Input::get('UTschedule_type'),
            'to_inout' => Input::get('UTtime_out'),
            'reason' => Input::get('UTreason'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            )];

        if(Input::has("UTdate")) {
            $dataParse['from_date'] = Input::get('UTdate');
        }

        if ($notification->first()['status'] != 'NEW') {

            $signatories = NotifSignatory::where('notification_id', $notification->first()["id"]);

            if ($signatories->count() > 0) {
                $signatories->delete();
            }
        }
        
        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);

        if (Input::get('UTschedule_type')== 'special') {
            $dataParse['schedule_time'] = Input::get('UTschedule_time_out');
            $dataParse['undertime_day'] = json_encode(Input::get('UTday'));
        }elseif (Input::get("UTschedule_type")=='compressed') {
            $dataParse['undertime_day'] = json_encode(["Saturday","Sunday"]);
        }elseif (Input::get("UTschedule_type")=='regular') {
            $dataParse['undertime_day'] = json_encode(["Sunday"]);
        }


        $dataGather = array_merge($this->sessionParse,$dataParse);

        $creation = $notification->update($dataGather);

        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            return Redirect::to("ns/notifications")
                ->with('successMessage',"Notification request successfully updated");
        }
    }
    public function reSend($inputs,$id) {
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $notifDetails = $notification->first();

        if ($notifDetails['status'] != 'NEW') {

            $signatories = NotifSignatory::where('notification_id', $notifDetails["id"]);

            if ($signatories->count() > 0) {
                $signatories->get();
            }
        }

        $dataParse = ['datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => Input::get('contactNumber'),
            'section' => Input::get('section'),
            'noti_type' => Input::get('notificationType'),
            'schedule_type' => Input::get('UTschedule_type'),
            'to_inout' => Input::get('UTtime_out'),
            'reason' => Input::get('UTreason'),
            'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get('comment'))
            ),
            'curr_emp' => Session::get('superiorid')];

        if(Input::has("UTdate")) {
            $dataParse['from_date'] = Input::get('UTdate');
        }

        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);

        if (Input::get('UTschedule_type')== 'special') {
            $dataParse['schedule_time'] = Input::get('UTschedule_time_out');
            $dataParse['undertime_day'] = json_encode(Input::get('UTday'));
        }elseif (Input::get("UTschedule_type")=='compressed') {
            $dataParse['undertime_day'] = json_encode(["Saturday","Sunday"]);
        }elseif (Input::get("UTschedule_type")=='regular') {
            $dataParse['undertime_day'] = json_encode(["Sunday"]);
        }

        $dataGather = array_merge($this->sessionParse,$dataParse);

        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{

                return Redirect::back()
                    ->with("errorMessage","No Immediate superior declared");

            }

        }

        $creation = $notification->update($dataGather);
        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $dataGather['documentcode'].'-'.$dataGather['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail
        );
        if(!$creation){
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon sending');
            exit;
        }else{
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }

                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Successfully approved and sent to HR Receiver");
            }else{
                return Redirect::to("ns/notifications")
                    ->with('successMessage',"Notification request successfully sent to Immediate Superior for approval");
            }
        }
    }

}