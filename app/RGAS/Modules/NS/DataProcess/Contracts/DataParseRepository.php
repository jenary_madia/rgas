<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 18/07/2016
 * Time: 5:18 PM
 */

namespace RGAS\Modules\NS\DataProcess\Contracts;


interface DataParseRepository
{
    public function forSave($inputs);
    public function forSend($inputs);
    public function reSave($inputs,$id);
    public function reSend($inputs,$id);
}