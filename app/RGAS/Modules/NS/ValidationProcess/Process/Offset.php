<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 9:30 AM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;

class Offset implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {

        $data = array(
            'schedule_type' => Input::get('schedule_type'),
            'duration_offset' => Input::get('durationType'),
            'reason' => Input::get('reason'),
            'date_from' => Input::get('dateFrom'),
            'period_from' => Input::get('periodFrom'),
            'hours_from' => Input::get('hoursFrom'),

            'total_days' => Input::get('totalDays'),
            'total_hours' => Input::get('totalHours'),
            'list_reference' => Input::get('listReference'),
        );
        $rules = array(
            'schedule_type' => 'required',
            'duration_offset' => 'required',
            'reason' => 'required',
            'date_from' => 'required',
            'period_from' => 'required',
            'hours_from' => 'required|numeric|min:1|max:12',
            'total_days' => 'required',
            'total_hours' => 'required',
            'list_reference' => 'required|not_in:[]',
        );
        $message = array(
            'list_reference.not_in' => 'List reference must be have atleast one',
        );
        if(Input::get("durationType") == "multiple")
        {
            $data['date_to'] = Input::get('dateTo');
            $data['period_to'] = Input::get('periodTo');
            $data['hours_to'] = Input::get('hoursTo');

            $rules['date_to'] = 'required';
            $rules['period_to'] = 'required';
            $rules['hours_to'] = 'required|numeric|min:1|max:12';
        }

        if(Input::get("totalListOffsetHours") < Input::get('totalHours')) {
             return Redirect::back()
                ->withInput()
                ->with('errorMessage', 'Check offset reference total hours');
            exit;
        }
        $validator = Validator::make($data,$rules,$message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('message', 'Some fields are incomplete.');
            exit;
        }
        return 1;
    }
}