<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:28 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;

class TimeKeep implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {

        $fields = array(
            'date_from' => Input::get("TKCDate"),
            'to_correct' => Input::get("toCorrect"),
            'reason' => Input::get("TKCreason")
        );
        $rules = array(
            'date_from' => 'required',
            'to_correct' => 'required',
            'reason' => 'required'
        );
        $message = [
                'date_from.required' => 'The date field is required'
             ];

        if(Input::has("toCorrect")) {
            if(Input::get("toCorrect") == 'no_time_in') {
                $fields['from_inout'] = Input::get("TKCTimeIn");
                $rules['from_inout'] = 'required';
            }elseif(Input::get("toCorrect") == 'no_time_out'){
                $fields['to_inout'] = Input::get("TKCTimeOut");
                $rules['to_inout'] ='required';
            }else{
                $fields['from_inout'] = Input::get("TKCTimeIn");
                $rules['from_inout'] = 'required';
                $fields['to_inout'] = Input::get("TKCTimeOut");
                $rules['to_inout'] = 'required';
            }
        }

        $validator = Validator::make(
            $fields,$rules,$message
        );

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('message', 'Some fields are incomplete.');
            exit;
        }
        return 1;
    }
}