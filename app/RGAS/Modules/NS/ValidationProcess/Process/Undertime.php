<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 18/07/2016
 * Time: 3:29 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;
use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;


class Undertime implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $params = [
            'schedule_type' => Input::get('UTschedule_type'),
            'date' => Input::get('UTdate'),
            'time_out' => Input::get('UTtime_out'),
            'reason' => Input::get('UTreason')
        ];

        $rules = [
            'schedule_type' => 'required',
            'date' => 'required',
            'time_out' =>'required',
            'reason' => 'required'
        ];

        if (Input::get('UTschedule_type') == "special") {
            $params['schedule_time_out'] = Input::get("UTschedule_time_out");
            $rules['schedule_time_out'] = "required";
        }

        $validator = Validator::make(
            $params,
            $rules
        );

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('message', 'Some fields are incomplete.');
            exit;
        }
        return 1;

    }
}