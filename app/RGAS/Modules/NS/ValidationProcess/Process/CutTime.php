<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:25 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;

class CutTime implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $validator = Validator::make(
            array(
                'total_days' => Input::get('CTTotalDays'),
                'total_hours' => Input::get('CTTotalHours'),
                'date_from' => Input::get('CTDateFrom'),
                'time_in/out_from' => Input::get('CTInOutFrom'),
                'date_to' => Input::get('CTDateTo'),
                'time_in/out_to' => Input::get('CTInOutTo'),
                'reason' => Input::get('CTReason'),
            ),
            array(
                'total_days' => 'required|numeric|between: 1,99',
                'total_hours' => 'required|numeric|between: 1,99',
                'date_from' => 'required',
                'time_in/out_from' => 'required',
                'date_to' => 'required',
                'time_in/out_to' => 'required',
                'reason' => 'required',
            )
        );

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('message', 'Some fields are incomplete.');
            exit;
        }
        return 1;
    }
}