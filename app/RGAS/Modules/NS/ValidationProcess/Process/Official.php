<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:29 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;

class Official implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $data = array(
            'duration_type'=> Input::get('OBdurationType'),
            'from_date'=> Input::get('OBfrom_date'),
            'from_inout'=> Input::get('OBfromStartTime'),
            'from_out'=> Input::get('OBfromEndTime'),
            'total_days'=> Input::get('OBtotalDays'),
            'reason'=> Input::get('OBreason'),
            'destination'=> Input::get('OBdestination')
        );

        $rules = array(
            'duration_type'=> 'required',
            'from_date'=> 'required|date',
            'from_inout'=> 'required',
            'from_out'=> 'required',
            'total_days'=> 'required|numeric|between: 0.50,99.99',
            'reason'=> 'required',
            'destination'=> 'required'
        );

        $message = array(
            'list_reference.not_in' => 'List reference must be have atleast one',
        );

        if(Input::get("OBdurationType") == "multiple")
        {
            $data['to_date'] = Input::get('OBto_date');
            $data['to_inout'] = Input::get('OBtoStartTime');
            $data['to_out'] = Input::get('OBtoEndTime');

            $rules['to_date'] = 'required';
            $rules['to_inout'] = 'required';
            $rules['to_out'] = 'required';
        }

        $validator = Validator::make($data,$rules,$message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('message', 'Some fields are incomplete.');
            exit;
        }
        return 1;
    }
}