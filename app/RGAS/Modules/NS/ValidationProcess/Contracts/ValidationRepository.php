<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 18/07/2016
 * Time: 3:26 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Contracts;


interface ValidationRepository
{
    public function validateInputs(array $inputs);
}