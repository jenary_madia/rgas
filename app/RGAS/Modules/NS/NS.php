<?php

namespace RGAS\Modules\NS;
use RGAS\Modules\NS\DataProcess\Contracts\DataParseRepository;
use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use RGAS\Libraries;
use Input;
use Receivers;
use Session;
use Notifications;
use NotificationDetails;
use NotifSignatory;
use NotificationBatch;
use NotificationBatchDetails;
use Redirect;
use DB;
use Validator;
use Employees;
use Config;
use Mail;

class NS
{
    protected $validationRepository;
    protected $dataParseRepository;
    protected $superiorEmail;
    protected $logs;
    public function __construct()
    {
        $this->logs = new Logs;
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
    }

    public function getStoragePath()
    {
        return Config::get('rgas.rgas_storage_path').'ns/';
    }

    public function sendProcess(ValidationRepository $validationRepository,DataParseRepository $dataParseRepository) {
        $validate = $validationRepository->validateInputs(Input::all());
        if ($validate === 1) {

        return $dataParseRepository->forSend(Input::all());

        }
        return $validate;
    }

    public function saveProcess(ValidationRepository $validationRepository,DataParseRepository $dataParseRepository) {
        return $dataParseRepository->forSave(Input::all());
        return $validate;
    }

    public function cancelProcess($id) {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $notifications = Notifications::where('id',$id)
        ->where('ownerid',Session::get('employee_id'));
        $notifDetails = $notifications->first();
        if (count($notifications->get()) == 0) {
            return Redirect::to('ns/notifications')
                ->with('errorMessage', 'Invalid Process');
            exit;
        }

        $cancelNotif = $notifications->update(array(
            'curr_emp' => 0,
            'status' => 'CANCELLED'
        ));

            $this->sendMail(
				'CANCELLATION',
				'CANCELLED',
                $notifDetails['documentcode'].'-'.$notifDetails['codenumber'],
                $notifDetails['firstname'].' '.$notifDetails['middlename'].' '.$notifDetails['lastname'],
                $notifDetails['department'],
                $this->superiorEmail
			);

        if ($cancelNotif) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', 'Notifications successfully cancelled');
        }
        return Redirect::to("ns/notifications")
            ->with('errorMessage', 'Something went wrong upon submitting');
    }

    public function reprocess(ValidationRepository $validationRepository,DataParseRepository $dataParseRepository,$id) {
        $action = Input::get("action");

            if ($action == 'resend') {
                $validate = $validationRepository->validateInputs(Input::all());
                if ($validate === 1) {
                    return $dataParseRepository->reSend(Input::all(),$id);
                }
            }else{
                return $dataParseRepository->reSave(Input::all(),$id);
            }
        return $validate;
    }

    public function returnToFiler($id) {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $notifications = Notifications::where('id',$id)
            ->where('curr_emp',Session::get("employee_id"));
        $notif_update = $notifications->update(
            array(
            'curr_emp' => 0,
            'status' => 'DISAPPROVED',
            'comment' => DB::raw("concat(comment,'|','".
                json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                ))
                ."')")
            )
        );

        $notificationsSignatory = new NotifSignatory;
        $notificationsSignatory->notification_id = $id;
        $notificationsSignatory->signature_type = 1;
        $notificationsSignatory->signature_type_seq = NotifSignatory::checkSignatorySeq($id);
        $notificationsSignatory->employee_id = Session::get('employee_id');
        $notificationsSignatory->approval_date = date('Y-m-d');
        $notificationsSignatory->save();

        if ($notif_update && $notificationsSignatory->save()) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', 'Notification successfully returned to Filer');
            exit;
        }
        return Redirect::to("ns/notifications")
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
    }

    public function request($id)
    {
        $validate = Validator::make(
            array(
                'otherSuperiors' => Input::get('otherSuperiors')
            ),
            array(
                'otherSuperiors' => 'required'
            )
        );
        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
            exit;

        }

        $notifications = Notifications::find($id);
        $notifications->curr_emp = Input::get('otherSuperiors');
        $notifications->comment = DB::raw("concat(comment,'|','".
            json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ))
        ."')");
        $notifications->save();
        
        $notificationsSignatory = new NotifSignatory;
        $notificationsSignatory->notification_id = $id;
        $notificationsSignatory->signature_type = 1;
        $notificationsSignatory->signature_type_seq = NotifSignatory::checkSignatorySeq($id);
        $notificationsSignatory->employee_id = Session::get('employee_id');
        $notificationsSignatory->approval_date = date('Y-m-d');
        $notificationsSignatory->save();

        if ($notifications->save() && $notificationsSignatory->save()) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', 'Notifications successfully send to other superior for approval');
            exit;
        }
        return Redirect::to("ns/notifications")
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
    }

    public function sendToHr($id) {
        $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
        if(! $hrReciever) {
            return Redirect::back()
                ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
        }
        $notifications = Notifications::with('owner')->find($id);
        $notifications->dateapproved = date('Y-m-d');
        $notifications->status = "APPROVED";
        $notifications->curr_emp = $hrReciever['employee']['id'];
        $notifications->comment = DB::raw("concat(comment,'|','".
            json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ))
        ."')");
        $notifications->save();

        $notificationsSignatory = new NotifSignatory;
        $notificationsSignatory->notification_id = $id;
        $notificationsSignatory->signature_type = 2;
        $notificationsSignatory->signature_type_seq = NotifSignatory::checkSignatorySeq($id);
        $notificationsSignatory->employee_id = Session::get('employee_id');
        $notificationsSignatory->approval_date = date('Y-m-d');
        $notificationsSignatory->save();
        if ($notifications->save() && $notificationsSignatory->save()) {
            $this->sendMail(
                'checking',
                'FOR PROCESSING',
                $notifications->documentcode.'-'.$notifications->codenumber,
                $notifications->firstname.' '.$notifications->middlename.' '.$notifications->lastname,
                $notifications->owner->department->dept_name,
                $hrReciever->employee->email
            );
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Successfully approved and sent to HR Receiver");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
    }

    public function processMyNS($data) {
        $parameters = explode("|",$data);
        if(! $parameters) {
            return Redirect::back()
                ->with('errorMessage', 'Invalid action');
            exit;
        }
        $action = $parameters[0];

        $id = (count($parameters) > 1 ? $parameters[1] : 0);

        if ($action == "softDelete") {
            $notification = Notifications::where('id',$id)
                ->where("ownerid",Session::get("employee_id"));
            $notifDetails = $notification->first();
            $this->logs->AU005($notifDetails['id'], json_encode($notifDetails), $notifDetails['documentcode'].'-'.$notifDetails['codenumber'],'notifications','id');
            $toUpdate = [
                "curr_emp" => Session::get("employee_id"),
                "isdeleted" => 1,
                "status" => $notifDetails['status']."/DELETED",
            ];

            $action = $notification->update($toUpdate);
            if($action) {
                return Redirect::back()
                    ->with("successMessage","Notification successfully deleted");
            }else{
                return Redirect::back()
                    ->with("errorMessage","Something went wrong");
            }
        }elseif ($action == "batchApprove"){
            if(Input::get("chosenNotifs")) {
                $comment = json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=>'BATCH APPROVED!'
                ));
                $paramsSignatory = array(
                    "notifID" => Input::get('chosenNotifs'),
                    "signatureType" => 2
                );
                $addSignatory = NotifSignatory::store($paramsSignatory);
                $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
                }

                $notifications = Notifications::whereIn("id", Input::get("chosenNotifs"))
                    ->where("curr_emp",Session::get("employee_id"));
                $parameters = array(
                    "status" => "APPROVED",
                    "curr_emp" => $hrReciever['employee']['id'],
                    "dateapproved" => date('Y-m-d'),
                    "comment" => DB::raw("concat(comment,'|','{$comment}')")
                );
                $action = $notifications->update($parameters);
                if ($action && $addSignatory) {
                    if(in_array(Session::get("company"),json_decode(CORPORATE,true))) {
                        return Redirect::to("ns/notifications")
                            ->with('successMessage', "Notification request successfully sent to CHRD for processing");
                        exit;
                    }
                    return Redirect::to("ns/notifications")
                        ->with('successMessage', "Notification request successfully sent to SHRD for processing");
                    exit;
                }else{
                    return Redirect::to("ns/notifications")
                        ->with('errorMessage', 'Something went wrong upon submitting notification.');
                }

            }else{
                return Redirect::to('lrf/leaves')
                    ->with('errorMessage', 'Please choose leave/s to approve');
            }
        }else{
            return Redirect::back()
                ->with('errorMessage', 'Invalid action');
            exit;
        }

    }

    public function PECreateNotif(){
        $validate = Validator::make(
            array(
                'department' => Input::get('department'),
                'employees' => Input::get('employeesToFile')
            ),
            array(
                'employees' => 'required|not_in:[]',
                'department' => 'required'
            ),
            array(
                'employees.not_in' => 'Must add employee atleast one',
                'department.required' => 'Choose Department'
            )

        );
        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
            exit;

        }

        $notificationBatch = new NotificationBatch();
        $notificationBatch->documentcode = date('Y-m');
        $notificationBatch->codenumber = (new NotificationBatch)->generateCodeNumber();
        $notificationBatch->company = Session::get("company");
        $notificationBatch->departmentid = Input::get("department");
        $notificationBatch->sectionid = Input::get("section");
        $notificationBatch->status = 'FOR APPROVAL';
        $notificationBatch->comment = json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
        ));
        $notificationBatch->datecreated = date('Y-m-d');
        $notificationBatch->ownerid = Session::get("employee_id");
        $notificationBatch->curr_emp = Session::get("superiorid");
        $notificationBatch->save();

        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            date('Y-m').'-'.(new NotificationBatch)->generateCodeNumber(),
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail
        );

        $batchDetails = (new NotificationBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$notificationBatch->id);


        if ($notificationBatch->save() && $batchDetails) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Successfully sent to Immediate Superior");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;

    }

    public function PESaveNotif($id)
    {
        if($id) {
            NotificationBatch::find($id)->delete();
            NotificationBatchDetails::where('notification_batch_id',$id)->delete();
        }

        $notifBatch = new NotificationBatch();
        $notifBatch->company = Session::get("company");
        $notifBatch->departmentid = Input::get("department");
        $notifBatch->sectionid = Input::get("section");
        $notifBatch->status = 'NEW';
        $notifBatch->comment = json_encode(array(
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get("comment")
        ));
        $notifBatch->datecreated = date('Y-m-d');
        $notifBatch->ownerid = Session::get("employee_id");
        $notifBatch->curr_emp = 0;
        $notifBatch->save();

        $batchDetails = (new NotificationBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$notifBatch->id);
        if ($notifBatch->save() && $batchDetails) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Notification Successfully save");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
    }

    public function PESend($id)
    {
        $validate = Validator::make(
            array('otherSuperior' => Input::get("otherSuperior")),
            array('otherSuperior' => 'required'),
            array('otherSuperior.required' => 'Please select approver')
        );
        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->with('errorMessage', 'Please select approver');
            exit;

        }

        DB::beginTransaction();
        try {
            $notifBatchDetails = NotificationBatchDetails::where("notification_batch_id",$id);
            $otherSuperiorsDetails = Employees::where('id',Input::get("otherSuperior"))
                ->first();
            $notifBatch =  NotificationBatch::where("id",$id)
                ->where('curr_emp',Session::get("employee_id"))
                ->whereIn('status',array("FOR APPROVAL","RETURNED"))
                ->with("owner")
                ->with("department")
                ->whereIn('status',array("FOR APPROVAL","RETURNED"))
                ->first();
            if(!$notifBatch) {
                goto error;
            }
            foreach($notifBatchDetails->get() as $key) {
                if ($key['noted'] == 0) {
                    $notifBatchDetails->update(array("noted"=>0));
                    DB::commit();
                    return Redirect::back()
                        ->withInput()
                        ->with('errorMessage', 'Required to note all employees');
                    exit;
                }
            }
            $notifBatch->update([
                'curr_emp'=>$otherSuperiorsDetails['id'],
                'status'=> 'FOR APPROVAL',
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ]);
            $notifBatchDetails->update(array("noted"=>0));

             $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $notifBatch['documentcode'].'-'.$notifBatch['codenumber'],
                $notifBatch['owner']['firstname'].' '.$notifBatch['owner']['middlename'].' '.$notifBatch['owner']['lastname'],
                $notifBatch['department']['dept_name'],
                $otherSuperiorsDetails['email']
            );

            DB::commit();
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Notification successfully sent to other approver");
            exit;


        }catch (Exception $e) {
            error:
            DB::rollback();
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon submitting');
            exit;
        }
    }

    public function PESendToHR($id) {
        DB::beginTransaction();
        $hrReciever = Receivers::get("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));

        try {
            $notificationBatchDetails = NotificationBatchDetails::where("notification_batch_id",$id);
            if (Session::get("desig_level") != "head") {
                foreach($notificationBatchDetails->get() as $key) {
                    if ($key['noted'] == 0) {
                        $notificationBatchDetails->update(array("noted"=>0));
                        DB::commit();
                        return Redirect::back()
                            ->withInput()
                            ->with('errorMessage', 'Required to note all employees');
                        exit;
                    }
                }
            }

            $notifBatch =  NotificationBatch::where("id",$id)
                ->with('batchDetails')
                ->where('curr_emp',Session::get("employee_id"))
                ->where('status',"FOR APPROVAL")
                ->first();


            $notificationBatchDetails->update(array("noted"=>1));
            $notifBatch->update([
                'curr_emp' => $hrReciever["employeeid"],
                'status' => 'APPROVED',
                'dateapproved' => date('Y-m-d'),
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ]);

//            $this->sendMail(
//                'processing',
//                'APPROVED',
//                $notifBatch['documentcode'].'-'.$notifBatch['codenumber'],
//                $notifBatch['owner']['firstname'].' '.$notifBatch['owner']['middlename'].' '.$notifBatch['owner']['lastname'],
//                $notifBatch['department']['dept_name'],
//                $hrReciever['employee']['email']
//            );


            DB::commit();
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Successfully sent to HR Receiver");
        }catch (Exception $e) {
            DB::rollback();
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon submitting');
        }
    }

    public function PECancelNotif($id) {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $notifications = NotificationBatch::where('id',$id)
            ->where('ownerid',Session::get('employee_id'));

        if (count($notifications->get()) == 0) {
            return Redirect::to('ns/notifications')
                ->with('errorMessage', 'Invalid Process');
            exit;
        }

        $cancelNotif = $notifications->update(array(
            'curr_emp' => 0,
            'status' => 'CANCELLED'
        ));

        if ($cancelNotif) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', 'Notifications successfully cancelled');
        }
        return Redirect::to("ns/notifications")
            ->with('errorMessage', 'Something went wrong upon submitting');
    }

    public function PEResend($id) {
        $validate = Validator::make(
            array(
                'department' => Input::get('department'),
                'employees' => Input::get('employeesToFile')
            ),
            array(
                'employees' => 'required|not_in:[]',
                'department' => 'required'
            ),
            array(
                'employees.not_in' => 'Must add employee atleast one',
                'department.required' => 'Choose Department'
            )

        );
        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
            exit;

        }

        $notificationBatch = NotificationBatch::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $dataParsed = ['company' => Session::get("company"),
            'departmentid' => Input::get("department"),
            'sectionid' => Input::get("section"),
            'status' => 'FOR APPROVAL',
            'curr_emp' => Session::get("superiorid"),
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            )),
            'datecreated' => date('Y-m-d')];

        $send = $notificationBatch->update($dataParsed);
        NotificationBatchDetails::where('notification_batch_id',$id)->delete();
        $batchDetails = (new NotificationBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$id);
        if ($send && $batchDetails) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Notification successfully sent to Immediate Superior");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
    }

    public function PEResave($id) {

        $notificationBatch = NotificationBatch::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $dataParsed = ['company' => Session::get("company"),
        'departmentid' => Input::get("department"),
        'sectionid' => Input::get("section"),
        'status' => 'NEW',
        'comment' => json_encode(array(
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get("comment")
        )),
        'datecreated' => date('Y-m-d')];

        $update = $notificationBatch->update($dataParsed);
        NotificationBatchDetails::where('notification_batch_id',$id)->delete();
        $batchDetails = (new NotificationBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$id);
        if ($update && $batchDetails) {
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Notification successfully updated");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
    }

    public function PEReturn($id) {

        DB::beginTransaction();
        try {
            $notificationBatchDetails = NotificationBatchDetails::where("notification_batch_id",$id);
            $notifBatch =  NotificationBatch::where("id",$id)
                ->where('curr_emp',Session::get("employee_id"))
                ->whereIn('status',array("FOR APPROVAL","RETURNED","APPROVED"))
                ->first();
            if(!$notifBatch) {
                DB::rollback();
                goto error;
            }

            if (Session::get("desig_level") != "head") {
                foreach($notificationBatchDetails->get() as $key) {
                    if ($key['noted'] == 0) {
                        $notificationBatchDetails->update(array("noted"=>0));
                        DB::commit();
                        return Redirect::back()
                            ->withInput()
                            ->with('errorMessage', 'Required to note all employees');
                        exit;
                    }
                }
            }


            $notifBatch->update([
                'status' => "RETURNED",
                'curr_emp'=> 0,
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ]);

            $notificationBatchDetails->update(array("noted"=>0));

            DB::commit();
            return Redirect::to("ns/notifications")
                ->with('successMessage', "Notification successfully returned");
            exit;


        }catch (Exception $e) {
            error:
            DB::rollback();
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon submitting');
            exit;
        }
    }

    public function PEProcessMyNS($data) {
        $parameters = explode("|",$data);
        if(! $parameters) {
            return Redirect::back()
                ->with('errorMessage', 'Invalid action');
            exit;
        }
        $action = $parameters[0];

        $id = (count($parameters) > 1 ? $parameters[1] : 0);

        if ($action == "softDelete") {
            $notification = NotificationBatch::where('id',$id)
                ->where("ownerid",Session::get("employee_id"));
            $toUpdate = [
                "curr_emp" => Session::get("employee_id"),
//                "isdeleted" => 1,
                "status" => "DELETED",
            ];

            $action = $notification->update($toUpdate);
            if($action) {
                return Redirect::back()
                    ->with("successMessage","Notification successfully deleted");
            }else{
                return Redirect::back()
                    ->with("errorMessage","Something went wrong");
            }
        }else{
            return Redirect::back()
                ->with("errorMessage","Invalid Action");
        }
    }

    public function sendMail($action,$status,$refno,$filer,$department,$email) {
        if ($action == "CANCELLATION") {
            Mail::send('ns.email.cancel', array(
                'status' => $status,
                'referenceNumber' => $refno,
                'filer' => $filer,
                'department' => $department,
            ),
                function($message) use ($email,$filer){
                    $message->to($email,$filer)->subject('RGAS Notification Alert: Notification Cancellation');
                }
            );
        }else{
            Mail::send('ns.email.notif', array(
                'action' => $action,
                'status' => $status,
                'referenceNumber' => $refno,
                'filer' => $filer,
                'department' => $department,
            ),
                function($message) use ($email,$filer,$action){
                    $message->to($email,$filer)->subject("RGAS Notification Alert: Notification for $action");
                }
            );
        }

    }

    public function attachments($refno) {
//        AU002($id,$tableName,$primaryKey,$new,$refNo)
        $files = Input::get("files");
        if($files) {
            $fm = new Libraries\FileManager;
            $i = 1;
            foreach($files as $file){

                if (Input::get("action") == 'send') {
                    if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                        $fm->move($file['random_filename'], $this->getStoragePath() . $refno);
                    }
                }
                
                $attachements['attach'.$i] = json_encode($file);
                $i++;

                if($i >= 6) {
                    break;
                }
            }

            for ($x = $i; $x < 6; $x++) {
                $attachements['attach'.$x] = null;
            }

        }else{
            for ($x = 1; $x < 6; $x++) {
                $attachements['attach'.$x] = null;
            }
        }
        
        return $attachements;

    }
}