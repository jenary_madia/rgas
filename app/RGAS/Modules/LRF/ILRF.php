<?php namespace RGAS\Modules\LRF;

interface ILRF
{
	public function sendLRF();
	public function saveLRF();
	public function processLeave($id);
	public function parseToInt($resDays);
	public function filterSearch();
	public function routing();
}
?>