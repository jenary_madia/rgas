<?php namespace RGAS\Modules\LRF\Requests;

use Input;
use Validator;
use Session;
use Redirect;

class Store extends RResult
{
	public function validate()
	{
        $inputs = [
            'EmployeeName' => Input::get('lrfEmployeeName'),
            'EmployeeId' => Input::get('lrfEmployeeId'),
            'DateFiled' => Input::get('lrfDateFiled'),
            'Company' => Input::get('lrfCompany'),
            'Department' => Input::get('lrfDepartment'),
            'LeaveType' => Input::get('lrfLeaveType'),
            'TypeOfSchedule' => Input::get('lrfTypeOfSchedule'),
            'DurationLeave' => Input::get('lrfDurationLeave'),
            'TotalLeaveDays' => Input::get('lrfTotalLeaveDays'),
            'RestDays' => Input::get('lrfRestDay'),
            'Reason' => Input::get('lrfReason'),
            'action' => Input::get('lrfAction')
        ];

        $rules = [
            'EmployeeName' => 'required',
            'EmployeeId' => 'required',
            'DateFiled' => 'required',
            'Company' => 'required',
            'Department' => 'required',
            'LeaveType' => 'required',
            'TypeOfSchedule' => 'required',
            'DurationLeave' => 'required',
            'DateFrom' => 'required',
            'LeavePeriodFrom' => 'required',
            'TotalLeaveDays' => 'required',
            'Reason' => 'required',
            'action' => 'required',
            'RestDays' => 'required'
        ];

        $messages = [
            'EmployeeName.required' => 'Employee name is required',
            'EmployeeId.required' => 'Employee ID is required',
            'DateFiled.required' => 'Date filed is required',
            'Company.required' => 'Company is required',
            'Department.required' => 'Department is required',
            'LeaveType.required' => 'Leave type is required',
            'TypeOfSchedule.required' => 'Type of schedule is required',
            'DurationLeave.required' => 'Duration of leave is required',
            'DateFrom.required' => 'Date leave start is required',
            'LeavePeriodFrom.required' => 'Leave From period is required',
            'DateTo.required' => 'Date leave end is required',
            'Reason.required' => 'Reason is required',
            'action.required' => 'Invalid action',
            'RestDays.required' => 'Rest day is required'
        ];

        if (Input::get('lrfLeaveType') == 'HomeVisit' ) {
            unset($inputs['TypeOfSchedule']);
            unset($inputs['RestDays']);
            unset($rules['TypeOfSchedule']);
            unset($rules['RestDays']);
            unset($messages['TypeOfSchedule.required']);
            unset($messages['RestDays.required']);
        }
        
        if (Input::get('lrfDurationLeave') == 'multiple' ) {
            $inputs['DateTo'] = Input::get('lrfDateTo');
            $inputs['LeavePeriodTo'] = Input::get('lrfLeavePeriodTo');
            $rules['DateTo'] = 'required';
            $rules['LeavePeriodTo'] = 'required';
            $messages['LeavePeriodTo.required'] = 'Leave To period is required';
            $messages['TotalLeaveDays.required'] = 'Total leave days is required';
        }


        if (Input::has('lrfDurationLeave')) {
            $inputs['DateFrom'] = Input::get('lrfDateFrom');
            $inputs['LeavePeriodFrom'] = Input::get('lrfLeavePeriodFrom');
        }


		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		$validator->sometimes('date_needed', 'required|date', function($input)
		{
			return Input::get('cb_urgency') == "Immediate Attention Needed";
		});
		
		if ($validator->fails()) {
			return Redirect::back()
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Some fields are incomplete.');
			exit;
		}

		return true;

	}
}
?>