<?php namespace RGAS\Modules\LRF;

use LeaveRequestForm;
use Session;
use RGAS\Libraries;
use Validator;
use Leaves;
use Input;
use Redirect;
use Signatory;
use Receivers;
use Employees;
use LeaveBatch;
use LeaveBatchDetails;
use DB;
use Config;
use Mail;

class LRF implements ILRF
{
	protected $LRFHelper;
    protected $superiorEmail;
	protected $logs;

	public function __construct()
	{
		$this->leaves = new Leaves;
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
        $this->logs = new Logs;
	}


	public function getStoragePath()
	{
		return Config::get('rgas.rgas_storage_path').'lrf/';
	}

	public function sendLRF($id = null)
	{

		if(Input::get('lrfLeaveType') == 'HomeVisit') {
            $homeVisitPerQuarter  = 7;
            $monthToday = Date("m");
            switch ($monthToday) {
                case (in_array($monthToday,['01','02','03'])):
                    $quarter = ['01','02','03'];
                    break;
                case (in_array($monthToday,['04','05','06'])):
                    $quarter = ['04','05','06'];
                    break;
                case (in_array($monthToday,['07','08','09'])):
                    $quarter = ['07','08','09'];
                    break;
                case (in_array($monthToday,['10','11','12'])):
                    $quarter = ['10','11','12'];
                    break;
            }
            $homeVisitSum = Leaves::getHomeVisitLeaves(Session::get("employee_id"),$quarter);
            $totalDaysToUse = Input::get("lrfTotalLeaveDays") + $homeVisitSum['sum'];
            if($totalDaysToUse > $homeVisitPerQuarter) {
                return Redirect::back()
                    ->withInput()
                    ->with('errorMessage', 'You\'ve exceed on days allowed per quarter');
                exit;
            }

		}

		$curr_emp = $this->routing();

		if (Session::get("company") != "RBC-CORP") {
			$nextApprover = (Input::get('lrfLeaveType') == "Sick" && Input::get('lrfTotalLeaveDays') > 3 || Input::get('lrfLeaveType') == "Mat/Pat" && Input::get('lrfTotalLeaveDays') > 3 ? "Clinic" : "Immediate Superior");
		}else{
			$nextApprover = "Immediate Superior";
		}

		$restDays = [];
		foreach (Input::get('lrfRestDay') as $key)
		{
			switch ($key) {
			    case 1:
			        array_push($restDays,'Monday');
			        break;
			    case 2:
			        array_push($restDays,'Tuesday');
			        break;
			    case 3:
			        array_push($restDays,'Wednesday');
			        break;
			    case 4:
			        array_push($restDays,'Thursday');
			        break;
			    case 5:
			        array_push($restDays,'Friday');
			        break;
			    case 6:
			        array_push($restDays,'Saturday');
			        break;
			    case 0:
			        array_push($restDays,'Sunday');
			        break;

			}				
		}

        if(empty($restDays)) {
            return Redirect::back()
                ->with('errorMessage', 'Please provide your rest days');
            exit;
        }

		$parameters = array(
				'employeeid' => Session::get('employeeid'),
				'firstname' => Session::get('firstname'),
				'middlename' => Session::get('middlename'),
				'lastname' => Session::get('lastname'),
				'company' => Session::get('company'),
				'date' => date('Y-m-d'),
				'appliedfor' => Input::get('lrfLeaveType'),
				'noofdays' => Input::get('lrfTotalLeaveDays'),
				'reason' => Input::get('lrfReason'),
				'status' => 'FOR APPROVAL',
				'ownerid' => Session::get('employee_id'),
				'comment' => json_encode(array(
										'name'=>Session::get('employee_name'),
										'datetime'=>date('m/d/Y g:i A'),
										'message'=>Input::get('lrfComment')
									)),
				'datecreated' => date('Y-m-d'),
				'department' => Session::get('dept_name'),
				'section' => Input::get('lrfSection'),
				'contact_no' => Input::get('lrfContactNumber'),
				'schedule_type' => Input::get('lrfTypeOfSchedule'),
				'rest_day' => json_encode($restDays),
				'duration' => Input::get('lrfDurationLeave'),
				'period_from' => (Input::get("lrfLeavePeriodFrom") == "half" ? 1 : 2),
				'period_to' => (Input::get("lrfLeavePeriodTo") == "half" ? 1 : 2));

        if(Input::has("lrfDateFrom")) {
            $parameters['from'] = Input::get('lrfDateFrom');
        }

        if(Input::has("lrfDateTo")) {
            $parameters['to'] = Input::get('lrfDateTo');
        }
        
        if(! $curr_emp['id']) {
            if (Session::get('desig_level') == "president") {
                $hrReciever = Receivers::get("notifications", "HR_NOTIFICATION_RECEIVER", Session::get("company"));
                if (!$hrReciever) {
                    return Redirect::back()
                        ->with("errorMessage", "No HR_NOTIFICATION_RECEIVER declared");
                }
                $parameters['curr_emp'] = $hrReciever['employee']['id'];
                $parameters['status'] = 'APPROVED';
            } else {

                return Redirect::back()
                    ->with("errorMessage", "No Immediate superior declared");

            }
        }else{
            $parameters['curr_emp'] = $curr_emp['id'];
        }

		$files = Input::get('files');

        if ($id) {

            $leave = Leaves::where('id', $id)
                ->where('ownerid',Session::get("employee_id"));

            if(count($leave) == 0) {
                return Redirect::to("lrf/leaves")
                    ->with('errorMessage', 'Something went wrong upon submitting');
                exit;
            }

        	// for clarification

            $leaveDetails = $leave->first();

            if ($leaveDetails['status'] != 'NEW') {

                $signatories = Signatory::where('leave_id', $leave->first(["id"]));

                    if ($signatories->count() > 0) {

                        $signatories->delete();

                    }

            }

            if (Session::get("company") != "RBC-CORP") {
                if(Input::get('lrfLeaveType') == "Sick" && Input::get('lrfTotalLeaveDays') > 3 || Input::get('lrfLeaveType') == "Mat/Pat" && Input::get('lrfTotalLeaveDays') > 3) {
                    $validate = Validator::make(
                        array('images' => Input::get('med_files')),
                        array('images' => 'required'),
                        array('images.required' => 'Please attach files')
                    );
                    if ($validate->fails())
                    {
                        return Redirect::back()
                            ->withInput()
                            ->withErrors($validate)
                            ->with('message', 'Some fields are incomplete.');
                        exit;

                    }

                    $fm = new Libraries\FileManager;
                    if(Input::get('med_files')) {
                        foreach(Input::get('med_files') as $file){
                            if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                                $fm->move($file['random_filename'], $this->getStoragePath() . $leaveDetails['documentcode'] . '-' . $leaveDetails['codenumber']);
                            }
                        }
                        $parameters['clinic_attachment'] = json_encode(Input::get('med_files'));
                    }
                }
            }


            if($files){

                $fm = new Libraries\FileManager;
                $i = 1;
                foreach($files as $file){
                    if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                        $fm->move($file['random_filename'], $this->getStoragePath() . $leaveDetails['documentcode'] . '-' . $leaveDetails['codenumber']);
                        $parameters['attach' . $i] = json_encode($file);
                        $i++;
                        if ($i >= 6) {
                            break;
                        }
                    }
                }

                for ($x = $i; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }

            }else{
                for ($x = 1; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }
            }

            $this->logs->AU004($leaveDetails['id'],'leaves','id',json_encode($parameters),$leaveDetails['documentcode'].'-'.$leaveDetails['codenumber'],json_encode($leaveDetails));
        	if($files) {
            	$this->logs->AU002($leaveDetails['id'],'leaves','id',json_encode($parameters),$leaveDetails['documentcode'].'-'.$leaveDetails['codenumber']);
        	}
            $action = $leave->update($parameters);
        }else{
			$parameters['documentcode'] = date('Y-m');
			$parameters['codenumber'] = ($this->leaves->generateCodeNumber());

            if (Session::get("company") != "RBC-CORP") {
                if(Input::get('lrfLeaveType') == "Sick" && Input::get('lrfTotalLeaveDays') > 3 || Input::get('lrfLeaveType') == "Mat/Pat" && Input::get('lrfTotalLeaveDays') > 3) {
                    $validate = Validator::make(
                        array('images' => Input::get('med_files')),
                        array('images' => 'required'),
                        array('images.required' => 'Please attach you medical certificate')
                    );
                    if ($validate->fails())
                    {
                        return Redirect::back()
                            ->withInput()
                            ->withErrors($validate)
                            ->with('message', 'Some fields are incomplete.');
                        exit;

                    }

                    $fm = new Libraries\FileManager;
                    if(Input::get('med_files')) {
                        foreach(Input::get('med_files') as $file){
                            if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                                $fm->move($file['random_filename'],$this->getStoragePath().$parameters['documentcode'].'-'.$parameters['codenumber']);
                            }
                        }
                        $parameters['clinic_attachment'] = json_encode(Input::get('med_files'));
                    }
                }
            }

            if($files){

                $fm = new Libraries\FileManager;
                $i = 1;
                foreach($files as $file){
                    if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                        $fm->move($file['random_filename'], $this->getStoragePath() . date('Y-m') . '-' . $this->leaves->generateCodeNumber());
                        $parameters['attach' . $i] = json_encode($file);
                        $i++;
                        if ($i >= 6) {
                            break;
                        }
                    }
                }

                for ($x = $i; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }

            }else{
                for ($x = 1; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }
            }
			$action = $this->leaves->store($parameters);
	        if($files) {
            	$this->logs->AU002($action['id'],'leaves','id',json_encode($action),$action['documentcode'].'-'.$action['codenumber']);
        	}
        }

        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            date('Y-m').'-'.$this->leaves->generateCodeNumber(),
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail
        );

        if ($action) {
            if(Session::get("desig_level") == "president") {
                return Redirect::to("lrf/leaves")
                    ->with('successMessage', "Successfully sent to HR Receiver");
                exit;
            }else{
                return Redirect::to('lrf/leaves')
                    ->with('successMessage',"Leave request successfully sent to {$nextApprover} for approval");
            }
		}else{
			return Redirect::to('lrf/create')
		           ->with('errorMessage', 'Something went wrong upon submitting leave.');
		}
	}

	public function saveLRF($id = null)
	{

        if (! Input::has('lrfRestDay')) {
            $numRestDays = [0,6];
        }else{
            $numRestDays = Input::get('lrfRestDay');
        }
        $restDays = [];
        foreach ($numRestDays as $key)
        {
            switch ($key) {
                case 1:
                    array_push($restDays,'Monday');
                    break;
                case 2:
                    array_push($restDays,'Tuesday');
                    break;
                case 3:
                    array_push($restDays,'Wednesday');
                    break;
                case 4:
                    array_push($restDays,'Thursday');
                    break;
                case 5:
                    array_push($restDays,'Friday');
                    break;
                case 6:
                    array_push($restDays,'Saturday');
                    break;
                case 0:
                    array_push($restDays,'Sunday');
                    break;

            }
        }
        $parameters = array(
            'employeeid' => Session::get('employeeid'),
            'firstname' => Session::get('firstname'),
            'middlename' => Session::get('middlename'),
            'lastname' => Session::get('lastname'),
            'company' => Session::get('company'),
            'date' => date('Y-m-d'),
            'appliedfor' => Input::get('lrfLeaveType'),
            'noofdays' => Input::get('lrfTotalLeaveDays'),
            'reason' => Input::get('lrfReason'),
            'status' => 'NEW',
            'ownerid' => Session::get('employee_id'),
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('lrfComment')
            )),
            'datecreated' => date('Y-m-d'),
            'department' => Session::get('dept_name'),
            'section' => Input::get('lrfSection'),
            'contact_no' => Input::get('lrfContactNumber'),
            'schedule_type' => Input::get('lrfTypeOfSchedule'),
            'rest_day' => json_encode($restDays),
            'curr_emp' => 0,
            'duration' => Input::get('lrfDurationLeave'),
            'period_from' => (Input::get("lrfLeavePeriodFrom") == "half" ? 1 : 2),
            'period_to' => (Input::get("lrfLeavePeriodTo") == "half" ? 1 : 2));

        if(Input::has("lrfDateFrom")) {
            $parameters['from'] = Input::get('lrfDateFrom');
        }

        if(Input::has("lrfDateTo")) {
            $parameters['to'] = Input::get('lrfDateTo');
        }


        $files = Input::get('files');

		if ($id){
            $leave = Leaves::where('id', $id)
            ->where('ownerid',Session::get("employee_id"));

             if(count($leave) == 0) {
                 return Redirect::to("lrf/leaves")
                     ->with('errorMessage', 'Something went wrong upon submitting');
                 exit;
             }
            if ($leave->first()['status'] != "NEW") {
                $signatories = Signatory::where('leave_id', $leave->first(["id"]));

                if ($signatories->count() > 0) {

                    $signatories->delete();

                }
            }

            if (Session::get("company") != "RBC-CORP") {
                if(Input::get('lrfLeaveType') == "Sick" && Input::get('lrfTotalLeaveDays') > 3 || Input::get('lrfLeaveType') == "Mat/Pat" && Input::get('lrfTotalLeaveDays') > 3) {

                    $fm = new Libraries\FileManager;
                    if(Input::get('med_files')) {
                        foreach(Input::get('med_files') as $file){
                            if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                                $fm->move($file['random_filename'], $this->getStoragePath() . $leave->first()['documentcode'] . '-' . $leave->first()['codenumber']);
                            }
                        }
                        $parameters['clinic_attachment'] = json_encode(Input::get('med_files'));
                    }
                }
            }

            if($files){
                $i = 1;
                foreach($files as $file){
                    $parameters['attach'.$i] = json_encode($file);
                    $i++;
                    if($i >= 6) {
                        break;
                    }
                }

                for ($x = $i; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }

            }else{
                for ($x = 1; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }
            }

            $this->logs->AU004($leave->first()['id'],'leaves','id',json_encode($parameters),$leave->first()['documentcode'].'-'.$leave->first()['codenumber'],json_encode($leave->first()));

            if($files) {
                $this->logs->AU002($leave->first()['id'],'leaves','id',json_encode($parameters),$leave->first()['documentcode'].'-'.$leave->first()['codenumber']);

            }
            $action = $leave->update($parameters);
		}else{
            $parameters['documentcode'] = date('Y-m');
            $parameters['codenumber'] = ($this->leaves->generateCodeNumber());

            if (Session::get("company") != "RBC-CORP") {
                if(Input::get('lrfLeaveType') == "Sick" && Input::get('lrfTotalLeaveDays') > 3 || Input::get('lrfLeaveType') == "Mat/Pat" && Input::get('lrfTotalLeaveDays') > 3) {

                    $fm = new Libraries\FileManager;
                    if(Input::get('med_files')) {
                        foreach(Input::get('med_files') as $file){
                            if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                                $fm->move($file['random_filename'], $this->getStoragePath() . $parameters['documentcode'] . '-' . $parameters['codenumber']);
                            }
                        }
                        $parameters['clinic_attachment'] = json_encode(Input::get('med_files'));
                    }
                }
            }

            if($files){
                $i = 1;
                foreach($files as $file){
                    $parameters['attach'.$i] = json_encode($file);
                    $i++;
                    if($i >= 6) {
                        break;
                    }
                }

                for ($x = $i; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }

            }else{
                for ($x = 1; $x < 6; $x++) {
                    $parameters['attach'.$x] = null;
                }
            }

            $action = $this->leaves->store($parameters);
            if($files) {
                $this->logs->AU002($action['id'],'leaves','id',json_encode($action),$action['documentcode'].'-'.$action['codenumber']);
            }
        }
       	if ($action) {
			$redirect = Redirect::to('lrf/leaves');
				if($id) {
					$redirect->with('successMessage','Leave request successfully updated');
				}else{
					$redirect->with('successMessage','Leave request successfully saved and can be edited later');
				}
			return $redirect;
		}else{
			return Redirect::back()
				   ->with('errorMessage', 'Something went wrong upon submitting leave.');
		}
	}

	public function cancelRequest($id) {
		$requestLeaves = Leaves::where('id',$id)
			->where('ownerid',Session::get('employee_id'))
			->with('employee')
			->first();
		$validate = Validator::make(
			array(
				'lrfComment' => Input::get("lrfComment"),
			),
			array(
				'lrfComment' => 'required',
			),
			array(
				'lrfComment.required' => 'Comment is required',
			));

		if ($validate->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validate)
				->with('message', 'Some fields are incomplete.');
		}

		if ($requestLeaves->status != "FOR APPROVAL") {
			goto invalidAction;
		}

			$this->sendMail(
				'CANCELLATION',
				'CANCELLED',
				$requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
				$requestLeaves['firstname'].' '.$requestLeaves['middlename'].' '.$requestLeaves['lastname'],
				$requestLeaves['department'],
                $requestLeaves['employee']['email']
			);

		$paramsLeave = array(
			"leaveId" => $id,
			"status" => "CANCELLED",
			"nextApprover" => 0,
			"comment" => json_encode(array(
				'name'=>Session::get('employee_name'),
				'datetime'=>date('m/d/Y g:i A'),
				'message'=> Input::get("comment")
			))
		);

		$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);
		if ($updateLeaveStatus) {
			return Redirect::to('lrf/leaves')
				->with('successMessage',"Leave request successfully cancelled");
		}else{
			invalidAction:
			return Redirect::back()
				->with('errorMessage', 'Something went wrong upon submitting leave.');
		}
	}
	
	public function processLeave($id)
	{
        if(! $id) {
            $id = explode('|',Input::get("lrfAction"))[1];
        }
		$action = Input::get('lrfAction');
		$otherSuperiors = Input::get('lrfOtherSuperiors');
        $otherSuperiorsEmail = Employees::where('id',$otherSuperiors)
            ->first()['email'];
        $requestLeaves = Leaves::where('id',$id)
            ->where('curr_emp',Session::get('employee_id'))
            ->with('employee')
            ->first();

        if(! $requestLeaves) {
            goto error;
        }

		if ($action == 'request') {
			$validate = Validator::make(
				    array(
				        'otherSuperiors' => Input::get('lrfOtherSuperiors')
				    ),
				    array(
				        'otherSuperiors' => 'required'
				    ),
                    array(
                        'otherSuperiors.required' => 'Please select Personnel from the drop-down list'
                    )
				);
			if ($validate->fails())
			{
				return Redirect::back()
			           ->withInput()
			           ->withErrors($validate)
			           ->with('message', 'Some fields are incomplete.');
				exit;

			}

			$paramsSignatory = array(
					"leaveId" => $id,
					"signatureType" => 1
				);

			$paramsLeave = array(		
					"leaveId" => $id,
					"status" => "FOR APPROVAL",
					"nextApprover" => $otherSuperiors,
					"comment" => json_encode(array(
											'name'=>Session::get('employee_name'),
											'datetime'=>date('m/d/Y g:i A'),
											'message'=>Input::get('lrfComment')
										))
				);

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].' '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $otherSuperiorsEmail
            );

            $addSignatory = Signatory::store($paramsSignatory);
			$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);

        	// $this->logs->AU001($leaveBatch->id,'leavebatch','id',json_encode($leaveBatch),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']);

			if ($addSignatory && $updateLeaveStatus) {
				return Redirect::to("lrf/leaves")
			           ->with('successMessage', 'Leave successfully sent to other superior for approval');
				exit;
			}
            error:
			return Redirect::to("lrf/leaves")
		           ->with('errorMessage', 'Something went wrong upon submitting');
			exit;
		}elseif ($action == 'send') {
			$hrReciever = Receivers::get("leaves","HR_LEAVE_RECEIVER",Session::get("company"));
            if(! $hrReciever) {
                return Redirect::back()
                    ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared");
            }
            $paramsSignatory = array(
					"leaveId" => $id,
					"signatureType" => 2
				);
			$paramsLeave = array(		
					"leaveId" => $id,
					"status" => "APPROVED",
					"dateApproved" => date('Y-m-d'),
					"nextApprover" => $hrReciever['employeeid'],
					"comment" => json_encode(array(
											'name'=>Session::get('employee_name'),
											'datetime'=>date('m/d/Y g:i A'),
											'message'=>Input::get('lrfComment')
										))
				);

            $this->sendMail(
                'processing',
                'APPROVED',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].' '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $hrReciever['employee']['email']
            );

			$addSignatory = Signatory::store($paramsSignatory);
			$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);
			
			if ($addSignatory && $updateLeaveStatus) {

				if(in_array(Session::get("company"),json_decode(CORPORATE,true))) {
					return Redirect::to("lrf/leaves")
						->with('successMessage', "Leave request successfully sent to CHRD for processing");
					exit;
				}
				return Redirect::to("lrf/leaves")
					->with('successMessage', "Leave request successfully sent to SHRD for processing");
				exit;

			}
			return Redirect::back()
		           ->with('errorMessage', 'Something went wrong upon submitting');
			exit;
		}elseif ($action == 'return') {
			$validate = Validator::make(
						array(
					        'lrfComment' => Input::get("lrfComment"),
					    ),
					    array(
					        'lrfComment' => 'required',
					    ),
					    array(
					        'lrfComment.required' => 'Comment is required',
					    ));

			if ($validate->fails())
		    {
		        return Redirect::back()
		           ->withInput()
		           ->withErrors($validate)
		           ->with('message', 'Some fields are incomplete.');
		    }

			$paramsSignatory = array(
					"leaveId" => $id,
					"signatureType" => 1
				);
			$paramsLeave = array(		
					"leaveId" => $id,
					"status" => "DISAPPROVED",
					"nextApprover" => Input::get("lrfOwnerID"),
					"comment" => json_encode(array(
											'name'=>Session::get('employee_name'),
											'datetime'=>date('m/d/Y g:i A'),
											'message'=>Input::get('lrfComment')
										))
				);

			$addSignatory = Signatory::store($paramsSignatory);
			$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);
			if ($addSignatory && $updateLeaveStatus) {

				return Redirect::to("lrf/leaves")
			           ->with('successMessage', "Leave successfully returned to filer");
				exit;
			}
			return Redirect::back()
		           ->with('errorMessage', 'Something went wrong upon submitting');
			exit;
		}elseif ($action == 'clinicToIS'){ //Satellite
			$paramsSignatory = array(
				"leaveId" => $id,
				"signatureType" => 1
			);
			$paramsLeave = array(		
				"leaveId" => $id,
				"ownerId" => Input::get("lrfOwnerID"),
				"status" => "FOR APPROVAL",
				"nextApprover" => "toClarify",
				"comment" => json_encode(array(
										'name'=>Session::get('employee_name'),
										'datetime'=>date('m/d/Y g:i A'),
										'message'=>Input::get('lrfComment')
									))
			);

            $updateLeaveStatus = Leaves::statusUpdate($paramsLeave);

            if($updateLeaveStatus === "ERROR003") {
                return Redirect::back()
                    ->with('errorMessage', 'No Immediate superior declared');
                exit;
            }

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].' '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $requestLeaves['employee']['email']
            );

            $addSignatory = Signatory::store($paramsSignatory);
            if ($addSignatory && $updateLeaveStatus) {
				return Redirect::to("lrf/leaves")
			           ->with('successMessage', 'Leave successfully sent to immediate superior for approval');
				exit;
			}
			return Redirect::to("lrf/leaves")
		           ->with('errorMessage', 'Something went wrong upon submitting');
			exit;
		}
	}

	public function parseToInt($restDays)
	{
		$explodedRestDay = explode("&", $restDays);
		$intRestDays = array();
		$restDayConvertion = array(
				"Sunday" => 0,
				"Monday" => 1,
				"Tuesday" => 2,
				"Wednesday" => 3,
				"Thursday" => 4,
				"Friday" => 5,
				"Saturday" => 6
			);
		foreach ($explodedRestDay as $key) {
				array_push($intRestDays, $restDayConvertion[$key]);
		}
		return $intRestDays;
	}

	public function filterSearch()
	{
		$hasValues = [];
		foreach (Input::all() as $key => $value) {
		 	if ($value) {
		 		$hasValues[$key] = $value;
		 	}
	 	};

	 	return $hasValues;
		// if (Input::has("notificationType")) $parameters['department'] = Input::get("notificationType"); hide ko muna po ito
		

//		return $parameters;
	}

	public function routing()
	{
		if (Session::get("company") != "RBC-CORP") {
			if (Input::get('lrfLeaveType') == "Sick" && Input::get('lrfTotalLeaveDays') > 3 || Input::get('lrfLeaveType') == "Mat/Pat"){
				$id = Receivers::get("leaves","CLINIC_LEAVE_RECEIVER",Session::get("company"))['employeeid'];
			}else{
                $id = Session::get("superiorid");
            }
		}else{
			$id = Session::get("superiorid");
		}
		return Employees::where('id',$id)->first();

	}

	public function PESaveLeave($id){
	    if($id) {
            $leaveBatch = LeaveBatch::find($id);
            LeaveBatchDetails::where('leave_batch_id',$id)->delete();
            $parameters = [
                'company' => Session::get("company"),
                'departmentid' => Input::get("department"),
                'sectionid' => Input::get("section"),
                'status' => 'NEW',
                'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                )),
                'datecreated' => date('Y-m-d'),
                'ownerid' => Session::get("employee_id"),
                'curr_emp' => 0
            ];
            $this->logs->AU004($leaveBatch->id,'leave_batch','id',json_encode($parameters),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],json_encode($leaveBatch));
            $leaveBatch->update($parameters);
        }else{
            $leaveBatch = new LeaveBatch();
            $leaveBatch->documentcode = date('Y-m');
            $leaveBatch->codenumber = (new LeaveBatch)->generateCodeNumber();
            $leaveBatch->company = Session::get("company");
            $leaveBatch->departmentid = Input::get("department");
            $leaveBatch->sectionid = Input::get("section");
            $leaveBatch->status = 'NEW';
            $leaveBatch->comment = json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ));
            $leaveBatch->datecreated = date('Y-m-d');
            $leaveBatch->ownerid = Session::get("employee_id");
            $leaveBatch->curr_emp = 0;
            $leaveBatch->save();

            $this->logs->AU001($leaveBatch->id,'leave_batch','id',json_encode($leaveBatch),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']);
        }


        $batchDetails = (new LeaveBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$leaveBatch->id);


        if ($leaveBatch->save() && $batchDetails) {
            return Redirect::to("lrf/leaves")
                ->with('successMessage', "Leave Successfully save");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting');
        exit;
	}

	public function PECreateLeave(){
		$validate = Validator::make(
			array(
				'department' => Input::get('department'),
				'employees' => Input::get('employeesToFile')
			),
			array(
				'employees' => 'required|not_in:[]',
				'department' => 'required'
			),
			array(
				'employees.not_in' => 'Must add employee atleast one',
				'department.required' => 'Choose Department'
			)

		);
		if ($validate->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validate)
				->with('message', 'Some fields are incomplete.');
			exit;

		}


		$leaveBatch = new LeaveBatch();
		$leaveBatch->documentcode = date('Y-m');
		$leaveBatch->codenumber = (new LeaveBatch)->generateCodeNumber();
		$leaveBatch->company = Session::get("company");
		$leaveBatch->departmentid = Input::get("department");
		$leaveBatch->sectionid = Input::get("section");
		$leaveBatch->status = 'FOR APPROVAL';
		$leaveBatch->comment = json_encode(array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=> Input::get("comment")
		));
		$leaveBatch->datecreated = date('Y-m-d');
		$leaveBatch->ownerid = Session::get("employee_id");
		$leaveBatch->curr_emp = Session::get("superiorid");
		$leaveBatch->save();

        $this->logs->AU001($leaveBatch->id,'leave_batch','id',json_encode($leaveBatch),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']);
		$batchDetails = (new LeaveBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$leaveBatch->id);
        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            date('Y-m').'-'.(new LeaveBatch)->generateCodeNumber(),
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            Session::get('email')
        );

        if ($leaveBatch->save() && $batchDetails) {
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Successfully sent to Immediate Superior");
			exit;
		}
		return Redirect::back()
			->with('errorMessage', 'Something went wrong upon submitting');
		exit;
	}

    public function PECancelLeave($id) {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $leave = LeaveBatch::where('id',$id)
            ->where('ownerid',Session::get('employee_id'))
            ->where('status','FOR APPROVAL');

        $this->logs->AU004(
            $leave->first()['id'],
            'leave_batch',
            'id',
            json_encode(array(
                'curr_emp' => 0,
                'status' => 'CANCELLED')),
            $leave->first()['documentcode'].'-'.$leave->first()['codenumber'],
            json_encode($leave->first())
        );

        if (count($leave->get()) == 0) {
            return Redirect::to('lrf/leaves')
                ->with('errorMessage', 'Invalid Process');
            exit;
        }
        $cancelLeave = $leave->update(array(
            'curr_emp' => 0,
            'status' => 'CANCELLED',
            'comment' => DB::raw("concat(comment,'|','".
                json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                ))
                ."')")
        ));
        if ($cancelLeave) {
            return Redirect::to("lrf/leaves")
                ->with('successMessage', 'Leave successfully cancelled');
        }
        return Redirect::to("lrf/leaves")
            ->with('errorMessage', 'Something went wrong upon submitting');
    }

    public function PEResend($id)
    {
		$validate = Validator::make(
			array(
				'department' => Input::get('department'),
				'employees' => Input::get('employeesToFile')
			),
			array(
				'employees' => 'required|not_in:[]',
				'department' => 'required'
			),
			array(
				'employees.not_in' => 'Must add employee atleast one',
				'department.required' => 'Choose Department'
			)

		);
		if ($validate->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validate)
				->with('message', 'Some fields are incomplete.');
			exit;

		}

		LeaveBatchDetails::where('leave_batch_id',$id)->delete();

		$leaveBatch = LeaveBatch::find($id);
        $parameters = [
                'documentcode' => date('Y-m'),
                'codenumber' => (new LeaveBatch)->generateCodeNumber(),
                'company' => Session::get("company"),
                'departmentid' => Input::get("department"),
                'sectionid' => Input::get("section"),
                'status' => 'FOR APPROVAL',
                'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                )),
                'datecreated' => date('Y-m-d'),
                'ownerid' => Session::get("employee_id"),
                'curr_emp' => Session::get("superiorid")
            ];

        $this->logs->AU004(
            $leaveBatch['id'],
            'leave_batch',
            'id',
            json_encode($parameters),
            $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
            json_encode($leaveBatch)
        );

		$leaveBatch->update($parameters);

		$batchDetails = (new LeaveBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$leaveBatch->id);

        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            date('Y-m').'-'.(new LeaveBatch)->generateCodeNumber(),
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            Session::get('email')
        );

        if ($leaveBatch->save() && $batchDetails) {
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Successfully sent to Immediate Superior");
			exit;
		}
		return Redirect::back()
			->with('errorMessage', 'Something went wrong upon submitting');
		exit;
    }

	public function PESend($id)
	{
        $validate = Validator::make(
            array('otherSuperior' => Input::get("otherSuperior")),
            array('otherSuperior' => 'required'),
            array('otherSuperior.required' => 'Please select approver')
        );
        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->with('errorMessage', 'Please select approver');
            exit;

        }

		DB::beginTransaction();
		try {
		    $leaveBatchDetails = LeaveBatchDetails::where("leave_batch_id",$id);
            $otherSuperiorsDetails = Employees::where('id',Input::get("otherSuperior"))
                ->first();
			$leaveBatch =  LeaveBatch::where("id",$id)
				->where('curr_emp',Session::get("employee_id"))
				->where('status',"FOR APPROVAL")
                ->with('department')
                ->with('owner')
				->first();
            if(!$leaveBatch) {
                goto error;
            }
            foreach($leaveBatchDetails->get() as $key) {
                if ($key['noted'] == 0) {
                    DB::commit();
                    return Redirect::back()
                        ->withInput()
                        ->with('errorMessage', 'Required to note all employees');
                    exit;
                }
            }
            $parameters = [
                'curr_emp'=>$otherSuperiorsDetails['id'],
                'status'=> 'FOR APPROVAL',
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);

            $leaveBatchDetails->update(array("noted"=>0));

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                $leaveBatch['owner']['firstname'].' '.$leaveBatch['owner']['middlename'].' '.$leaveBatch['owner']['lastname'],
                $leaveBatch['department']['dept_name'],
                $otherSuperiorsDetails['email']
            );

			DB::commit();
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Leave successfully sent to other approver");
			exit;


		}catch (Exception $e) {
		    error:
			DB::rollback();
			return Redirect::back()
				->with('errorMessage', 'Something went wrong upon submitting');
			exit;
		}
	}

	public function PESendToHR($id) {
		DB::beginTransaction();
        try {
            $hrReciever = Receivers::get("leaves","HR_LEAVE_RECEIVER",Session::get("company"));
            $leaveBatch =  LeaveBatch::where("id",$id)
				->with('batchDetails')
                ->with('owner')
                ->with('department')
				->where('curr_emp',Session::get("employee_id"))
				->where('status',"FOR APPROVAL")
				->first();

            if(!$leaveBatch) {
                goto error;
            }

            $leaveBatchDetails = LeaveBatchDetails::where("leave_batch_id",$id);
            if (Session::get("desig_level") != "head") {
                foreach($leaveBatchDetails->get() as $key) {
                    if ($key['noted'] == 0) {
                        DB::commit();
                        return Redirect::back()
                            ->withInput()
                            ->with('errorMessage', 'Required to note all employees');
                        exit;
                    }
                }
            }
            
            $parameters = [
                'curr_emp' => $hrReciever["employeeid"],
                'status' => 'APPROVED',
                'dateapproved' => date('Y-m-d'),
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);

            $this->sendMail(
                'processing',
                'APPROVED',
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                $leaveBatch['owner']['firstname'].' '.$leaveBatch['owner']['middlename'].' '.$leaveBatch['owner']['lastname'],
                $leaveBatch['department']['dept_name'],
                $hrReciever['employee']['email']
            );


			DB::commit();
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Successfully sent to HR Receiver");
			exit;
		}catch (Exception $e) {
            error :
			DB::rollback();
			return Redirect::back()
				->with('errorMessage', 'Something went wrong upon submitting');
			exit;
		}
	}

	public function PEReturn($id) {
        DB::beginTransaction();
        try {
            $leaveBatchDetails = LeaveBatchDetails::where("leave_batch_id",$id);
            $leaveBatch =  LeaveBatch::where("id",$id)
                ->where('curr_emp',Session::get("employee_id"))
                ->whereIn('status',array("FOR APPROVAL","RETURNED","APPROVED"))
                ->first();
            if(!$leaveBatch) {
                goto error;
            }

            $parameters = [
                'status' => "DISAPPROVED",
                'curr_emp'=> 0,
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);

            $leaveBatchDetails->update(array("noted"=>0));

            DB::commit();
            return Redirect::to("lrf/leaves")
                ->with('successMessage', "Leave successfully returned");
            exit;


        }catch (Exception $e) {
            error:
            DB::rollback();
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon submitting');
            exit;
        }
    }

    private function sendMail($action,$status,$refno,$filer,$department,$email) {
		if ($action == "CANCELLATION") {
			Mail::send('lrf.email.cancel', array(
				'status' => $status,
				'referenceNumber' => $refno,
				'filer' => $filer,
				'department' => $department,
			),
				function($message) use ($email,$filer){
					$message->to($email,$filer)->subject('RGAS Notification Alert: Leave Cancellation');
				}
			);
		}else{
			Mail::send('lrf.email.sample', array(
				'action' => $action,
				'status' => $status,
				'referenceNumber' => $refno,
				'filer' => $filer,
				'department' => $department,
			),
				function($message) use ($email,$filer,$action){
					$message->to($email,$filer)->subject("RGAS Notification Alert: Leave for $action");
				}
			);
		}

    }
}
?>
