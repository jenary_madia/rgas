<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/20/2016
 * Time: 4:04 PM
 */

namespace RGAS\Modules\MSR;
use Validator;
use Input;
use Redirect;

class MSRValidator
{
    public function send() {
        foreach (Input::all() as $key => $value) {
            $inputs[$key] = $value;
        }
        $rules = [
            "requestType" => "required",
            "initiatedBy" => "required",
            "urgency" => "required",
            "reason" => "required",
            "briefingDate" => "required",
            "screeningDate" => "required",
            "workplan" => "required",
            "projectRequirements" => "required|not_in:[]",
            "otherInformation" => "required",
        ];

        $message = [
            "requestType.required" => "Request Type is required",
            "initiatedBy.required" => "Initiated By is required",
            "briefingDate.required" => "Briefing Date is required",
            "screeningDate.required" => "Screening Date is required",
            "workplan.required" => "Workplan Rate is required",
            "projectRequirements.not_in" => "Please add at least one Project Requirement",
            "urgency.required" => "Urgency is required",
            "otherInformation.required" => "Other Information is required",
            "reason.required" => "Reason for request is required"
        ];
        
        $validate = Validator::make(
            $inputs,
            $rules,
            $message
        );

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        return true;

    }
}