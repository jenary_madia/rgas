<?php

namespace RGAS\Modules\MSR;
use RGAS\Libraries;
use Input;
use Session;
use Redirect;
use Validator;
use Config;
use Mail;
use Sections;
use ManpowerService;
use NPMDRequirements;
use ManpowerAgency;
use ManpowerserviceAttachments;
use ManpowerSignatories;
use Receivers;
use DB;
use Employees;

class MSR
{
    protected $logs;
    public function __construct()
    {
        $this->logs = new Logs;
    }

    public function getStoragePath()
    {
        return Config::get('rgas.rgas_storage_path').'msr/';
    }

    public function send() {

        /**** SETTING UP PARAMETERS *****/
        $manpowerParams = [
            'employeeid' => Session::get('employee_id'),
            'departmentid' => Session::get('dept_id'),
            'reference_no' => (new ManpowerService)->generateRefNo(),
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => 'FOR APPROVAL',
            'reason' => Input::get('reason'),
            'initiatedby' => Input::get('initiatedBy'),
            'screeningdate' => Input::get('screeningDate'),
            'briefingdate' => Input::get('briefingDate'),
            'workplanrate' => Input::get('workplan'),
            'otherinfo' => Input::get('otherInformation'),
            'curr_emp' => Session::get("superiorid"),
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            )),
        ];

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("requestType") == "pooling") {
            $manpowerParams['pooling'] = 1;
        }elseif (Input::get("requestType") == "screening") {
            $manpowerParams['screening'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("urgency") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }
        /******** INSERTING DATA ON MANPOWER SERVICES *********/
        $manpowerCreate = ManpowerService::create($manpowerParams);
        
        
        /******** INSERTING ATTACHMENTS ON MANPOWER SERVICES *********/
        $files = Input::get('files');

        if($files){
            $fm = new Libraries\FileManager;
            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                    $fm->move($file['random_filename'], $this->getStoragePath() . $manpowerParams['reference_no']);
                }
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $manpowerCreate['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::insert($attachParams);
        }

        $projectRequirements = json_decode(Input::get("projectRequirements"),true);
        $NPMDParams = [];
        foreach ($projectRequirements as $key) {
            array_push($NPMDParams,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'area' => $key['area'],
                'personno' => $key['noOfPersonnel'],
                'workingdays' => $key['noOfWorkingDays'],
                'jobtitle' => $key['jobTitle'],
                'dateneeded' => $key['dateNeeded'],
            ]);
        }

        if ($NPMDParams != []) {
            $NPMDReqInsert = NPMDRequirements::insert($NPMDParams);
        }

        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        if ($agenciesParam != []) {
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerCreate && $NPMDReqInsert) {
            $this->logs->AU001($manpowerCreate['id'],'MSR','id',json_encode($manpowerParams),$manpowerParams['reference_no']);

            if($files) {
                $this->logs->AU002($manpowerCreate['id'], 'MSR', 'id', json_encode($manpowerParams),$manpowerParams['reference_no']);
            }

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $manpowerParams['reference_no'],
                Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
                Session::get('dept_name'),
                $this->getEmail(Session::get("superiorid"))
            );
            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully sent to Immediate Superior for approval');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function save() {
        $manpowerParams = [
            'employeeid' => Session::get('employee_id'),
            'departmentid' => Session::get('dept_id'),
            'reference_no' => (new ManpowerService)->generateRefNo(),
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => 'NEW',
            'reason' => Input::get('reason'),
            'initiatedby' => Input::get('initiatedBy'),
            'workplanrate' => Input::get('workplan'),
            'otherinfo' => Input::get('otherInformation'),
            'curr_emp' => 0,
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            )),
        ];

        if (Input::get('screeningDate')) {
            $manpowerParams['screeningdate'] = Input::get('screeningDate');
        }

        if (Input::get('briefingDate')){
            $manpowerParams['briefingdate'] = Input::get('briefingDate');
        }

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("requestType") == "pooling") {
            $manpowerParams['pooling'] = 1;
        }elseif (Input::get("requestType") == "screening") {
            $manpowerParams['screening'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("urgency") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }

        $manpowerCreate = ManpowerService::create($manpowerParams);

        $files = Input::get('files');

        if($files){

            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $manpowerCreate['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::insert($attachParams);
        }

        $projectRequirements = json_decode(Input::get("projectRequirements"),true);
        $NPMDParams = [];
        foreach ($projectRequirements as $key) {
            array_push($NPMDParams,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'area' => $key['area'],
                'personno' => $key['noOfPersonnel'],
                'workingdays' => $key['noOfWorkingDays'],
                'jobtitle' => $key['jobTitle'],
                'dateneeded' => $key['dateNeeded'],
            ]);
        }

        if ($NPMDParams != []) {
            $NPMDReqInsert = NPMDRequirements::insert($NPMDParams);
        }

        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        if ($agenciesParam != []) {
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerCreate) {
            $this->logs->AU001($manpowerCreate['id'],'MSR','id',json_encode($manpowerParams),$manpowerParams['reference_no']);

            if($files) {
                $this->logs->AU002($manpowerCreate['id'], 'MSR', 'id', json_encode($manpowerParams),$manpowerParams['reference_no']);
            }

            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully saved and can be edited later');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function resend($id) {
        $msr = ManpowerService::where('id',$id)
            ->where('employeeid',Session::get("employee_id"))->first();

        $manpowerParams = [
            'employeeid' => Session::get('employee_id'),
            'departmentid' => Session::get('dept_id'),
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => 'FOR APPROVAL',
            'reason' => Input::get('reason'),
            'initiatedby' => Input::get('initiatedBy'),
            'screeningdate' => Input::get('screeningDate'),
            'briefingdate' => Input::get('briefingDate'),
            'workplanrate' => Input::get('workplan'),
            'otherinfo' => Input::get('otherInformation'),
            'curr_emp' => Session::get("superiorid"),
        ];

        if ($msr['status'] == 'NEW') {
            $manpowerParams['comment'] = json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            ));
        }else{
            $comment = json_encode([
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get('comment')
            ]);
            $manpowerParams['comment'] = $msr['comment'] .'|'. $comment;
        }

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("requestType") == "pooling") {
            $manpowerParams['pooling'] = 1;
        }elseif (Input::get("screening") == "screening") {
            $manpowerParams['screening'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("screening") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }

        $manpowerUpdate = $msr->update($manpowerParams);

        $files = Input::get('files');

        if($files){

            $fm = new Libraries\FileManager;
            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                    $fm->move($file['random_filename'], $this->getStoragePath() . $msr['reference_no']);
                }
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $msr['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
            ManpowerserviceAttachments::insert($attachParams);
        }else{
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
        }

        $projectRequirements = json_decode(Input::get("projectRequirements"),true);
        $NPMDParams = [];
        foreach ($projectRequirements as $key) {
            array_push($NPMDParams,[
                'manpowerserviceid' => $msr['id'],
                'area' => $key['area'],
                'personno' => $key['noOfPersonnel'],
                'workingdays' => $key['noOfWorkingDays'],
                'jobtitle' => $key['jobTitle'],
                'dateneeded' => $key['dateNeeded'],
            ]);
        }

        if ($NPMDParams != []) {
            NPMDRequirements::where('manpowerserviceid',$msr['id'])->delete();
            $NPMDReqInsert = NPMDRequirements::insert($NPMDParams);
        }

        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $msr['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        if ($agenciesParam != []) {
            ManpowerAgency::where('manpowerserviceid',$msr['id'])->delete();
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerUpdate && $NPMDReqInsert) {
            $this->logs->AU001($msr['id'],'MSR','id',json_encode($manpowerParams),$msr['reference_no']);

            if($files) {
                $this->logs->AU002($msr['id'], 'MSR', 'id', json_encode($manpowerParams),$msr['reference_no']);
            }

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $msr['reference_no'],
                Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
                Session::get('dept_name'),
                $this->getEmail(Session::get("superiorid"))
            );

            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully sent to Immediate Superior for approval');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function resave($id) {
        $msr = ManpowerService::where('id',$id)
            ->where('employeeid',Session::get("employee_id"))->first();

        $manpowerParams = [
            'employeeid' => Session::get('employee_id'),
            'departmentid' => Session::get('dept_id'),
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'reason' => Input::get('reason'),
            'initiatedby' => Input::get('initiatedBy'),
            'workplanrate' => Input::get('workplan'),
            'otherinfo' => Input::get('otherInformation'),
            'status' => 'NEW',
            'curr_emp' => 0,
        ];

        $manpowerParams['comment'] = json_encode(array(
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=>Input::get('comment')
        ));

        if (Input::get('screeningDate')) {
            $manpowerParams['screeningdate'] = Input::get('screeningDate');
        }

        if (Input::get('briefingDate')){
            $manpowerParams['briefingdate'] = Input::get('briefingDate');
        }

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("requestType") == "pooling") {
            $manpowerParams['pooling'] = 1;
        }elseif (Input::get("screening") == "screening") {
            $manpowerParams['screening'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("screening") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }

        $manpowerUpdate = $msr->update($manpowerParams);

        $files = Input::get('files');

        if($files){

            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $msr['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
            ManpowerserviceAttachments::insert($attachParams);
        }else{
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
        }

        $projectRequirements = json_decode(Input::get("projectRequirements"),true);
        $NPMDParams = [];
        foreach ($projectRequirements as $key) {
            array_push($NPMDParams,[
                'manpowerserviceid' => $msr['id'],
                'area' => $key['area'],
                'personno' => $key['noOfPersonnel'],
                'workingdays' => $key['noOfWorkingDays'],
                'jobtitle' => $key['jobTitle'],
                'dateneeded' => $key['dateNeeded'],
            ]);
        }

        if ($NPMDParams != []) {
            NPMDRequirements::where('manpowerserviceid',$msr['id'])->delete();
            $NPMDReqInsert = NPMDRequirements::insert($NPMDParams);
        }

        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $msr['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        if ($agenciesParam != []) {
            ManpowerAgency::where('manpowerserviceid',$msr['id'])->delete();
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerUpdate) {
            $this->logs->AU001($msr['id'],'MSR','id',json_encode($manpowerParams),$msr['reference_no']);

            if($files) {
                $this->logs->AU002($msr['id'], 'MSR', 'id', json_encode($manpowerParams),$msr['reference_no']);
            }
            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully updated');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function cancel($id) {
        $validate = Validator::make(
            ['comment' => Input::get('comment')],
            ['comment' => 'required']
        );

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }
        $msr = ManpowerService::where('id',$id)
            ->where('employeeid',Session::get("employee_id"))->first();
        if (!$msr) {
            return Redirect::back()
                ->with('errorMessage', 'Invalid action');
        }

        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $cancellation = $msr->update([
            'status' => 'CANCELLED',
            'curr_emp' => 0,
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        if($cancellation) {

            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully cancelled');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon cancelling MSR');
    }

//    FOR APPROVAL ACTIONS

    public function request($id) {
        /************VALIDATING INPUT***************/
        $validate = Validator::make(
            ['otherIS' => Input::get("otherSuperior")],
            ['otherIS' => 'required']
        );

        if ($validate->fails())
        {
            return Redirect::back()
                ->with('errorMessage', 'Please select Personnel from the drop-down list.');
        }

        $otherIs = Input::get("otherSuperior");
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR APPROVAL')
            ->where("curr_emp",Session::get("employee_id"));
        $msrDetails = $msr->with('owner')
            ->with('department')
            ->first();
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }

        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'FOR APPROVAL',
            'curr_emp' => $otherIs,
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        /************INSERTING SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 1,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => Session::get("employee_id"),
            'approval_date' => date('Y-m-d')
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            $msrDetails['reference_no'],
            $msrDetails['owner']['firstname'].' '.$msrDetails['owner']['middlename'].' '.$msrDetails['owner']['lastname'],
            $msrDetails['department']['dept_name'],
            $this->getEmail($otherIs)
        );
        
        return Redirect::to('msr/')
            ->with('successMessage', "MSR successfully sent for approval.");

    }

    public function returnToFiler($id) {
        /******************VALIDATION*****************/
        $validate = Validator::make(
            ['comment' => Input::get('comment')],
            ['comment' => 'required']
        );

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR APPROVAL')
            ->where("curr_emp",Session::get("employee_id"));
        $msrDetails = $msr->with('owner')
            ->with('department')
            ->first();
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }

        /************RETRIEVING AND DELETING SIGNATORIES***************/
        ManpowerSignatories::where('manpowerserviceid',$id)
            ->delete();


        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'DISAPPROVED',
            'curr_emp' => $msr->first()['employeeid'],
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        $this->sendMail(
            'DISAPPROVED',
            'DISAPPROVED',
            $msrDetails['reference_no'],
            $msrDetails['owner']['firstname'].' '.$msrDetails['owner']['middlename'].' '.$msrDetails['owner']['lastname'],
            $msrDetails['department']['dept_name'],
            $this->getEmail($msrDetails['employeeid'])
        );

        return Redirect::to('msr/')
            ->with('successMessage', 'MSR successfully returned to FILER.');
    }

    public function returnToIS($id) {
        /******************VALIDATION*****************/
        $validate = Validator::make(
            ['comment' => Input::get('comment')],
            ['comment' => 'required'],
            ['comment.required' => "Please indicate reason for disapproval"]
        );

        if ($validate->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Please indicate reason for disapproval.');
        }
        /************VALIDATING INPUT***************/
        $validate = Validator::make(
            ['retOtherSuperior' => Input::get("retOtherSuperior")],
            ['retOtherSuperior' => 'required']
        );

        if ($validate->fails())
        {
            return Redirect::back()
                ->with('errorMessage', 'Please select Personnel from the drop-down list.');
        }

        $otherIs = explode('|',Input::get("retOtherSuperior"));
        $toReturnID = $otherIs[0];
        $toReturnName = $otherIs[1];

        /************RETRIEVING AND DELETING SIGNATORIES***************/
        $SeqCheckpoint = ManpowerSignatories::where('manpowerserviceid',$id)
            ->where('employee_id',$toReturnID)
            ->first()['signature_type_seq'];

        ManpowerSignatories::where('manpowerserviceid',$id)
            ->where('signature_type_seq','>=',$SeqCheckpoint)
            ->delete();

        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR APPROVAL')
            ->where("curr_emp",Session::get("employee_id"));
        $msrDetails = $msr->with('owner')
            ->with('department')
            ->first();
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }

        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'FOR APPROVAL',
            'curr_emp' => $toReturnID,
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            $msrDetails['reference_no'],
            $msrDetails['owner']['firstname'].' '.$msrDetails['owner']['middlename'].' '.$msrDetails['owner']['lastname'],
            $msrDetails['department']['dept_name'],
            $this->getEmail($toReturnID)
        );

        return Redirect::to('msr/')
            ->with('successMessage', "MSR successfully returned to $toReturnName.");

    }

    public function sendToHr($id) {
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR APPROVAL')
            ->where("curr_emp",Session::get("employee_id"));
        $msrDetails = $msr->with('owner')
            ->with('department')
            ->first();
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }
        /************RETRIEVING RECEIVER***************/
        $receiver = Receivers::where("code",MSR_RECEIVER_CODE)->first();

        if (! $receiver) {
            return Redirect::back()
                ->with('errorMessage', 'No receiver declared');
        }


        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'FOR PROCESSING',
            'curr_emp' => $receiver['employeeid'],
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        /************INSERTING ENDORSED SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 1,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => Session::get("employee_id"),
            'approval_date' => date('Y-m-d')
        ]);

        /************INSERTING RECIEVED SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 2,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => $receiver['employeeid'],
            'approval_date' => date('Y-m-d')
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        $this->sendMail(
            'processing',
            'FOR PROCESSING',
            $msrDetails['reference_no'],
            $msrDetails['owner']['firstname'].' '.$msrDetails['owner']['middlename'].' '.$msrDetails['owner']['lastname'],
            $msrDetails['department']['dept_name'],
            $this->getEmail($receiver['employeeid'])
        );

        return Redirect::to('msr/')
            ->with('successMessage', 'MSR successfully sent to Purchasing for processing.');

    }

    public function saveToArchive($id) {
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR PROCESSING')
            ->where("curr_emp",Session::get("employee_id"));
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }
        /************RETRIEVING RECEIVER***************/
        $receiver = Receivers::where("code",MSR_RECEIVER_CODE)->first();

        if (! $receiver) {
            return Redirect::back()
                ->with('errorMessage', 'No receiver declared');
        }


        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'PROCESSED',
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        /************INSERTING SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 3,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => Session::get("employee_id"),
            'approval_date' => date('Y-m-d')
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        return Redirect::to('msr/submitted_msr')
            ->with('successMessage', 'MSR successfully archived.');
    }

//    LIST Action

    public function deleteMyMSR($id) {
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->whereIn('status',["NEW","PROCESSED","DISAPPROVED"])
            ->where("employeeid",Session::get('employee_id'))
            ->where("id",$id);
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }
        /************LOGS***************/

//        $this->logs->AU005($notifDetails['id'], json_encode($notifDetails), $notifDetails['documentcode'].'-'.$notifDetails['codenumber'],'MSR','id');
        /************CONSTRUCTING PARAMETERS***************/
        $toUpdate = [
            "status" => 'DELETED',
        ];
        /************UPDATING DATA***************/
        $action = $msr->update($toUpdate);
        if($action) {
            return Redirect::back()
                ->with("successMessage","Manpower Service Request successfully deleted");
        }else{
            return Redirect::back()
                ->with("errorMessage","Something went wrong");
        }
    }

    public function sendMail($action,$status,$refno,$filer,$department,$email) {
        if($email) {
            if ($action == "DISAPPROVED") {
                Mail::send('msr.email.disapprove', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer){
                        $message->to($email,$filer)->subject('RGAS Notification Alert: Disapproved MSR');
                    }
                );
            }else{
                Mail::send('msr.email.msr', array(
                    'action' => $action,
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer,$action){
                        $message->to($email,$filer)->subject("RGAS Notification Alert: MSR for $action");
                    }
                );
            }
        }

    }

    private function getEmail($id) {
        return Employees::where('id',$id)->first()['email'];
    }
}