<?php namespace RGAS\Modules\CBR;

use CompensationBenefits;
use CompensationBenefitsRequestedDocs;
use ReferenceNumbers;
use Session;
use RGAS\Libraries;

class CBR implements ICBR
{

	private $attachments_path = '/rgas/cbr/attachments';

	public function sendRequest($input)
	{
		$fm = new Libraries\FileManager;		
		$filenames = $fm->upload($this->attachments_path);
		
		$cb = new CompensationBenefits;
        $cb->cb_employees_id = Session::get('employee_id');
        $cb->cb_emp_name = Session::get('employee_name');
		$cb->cb_emp_id = Session::get('employeeid');
		$cb->cb_company = Session::get('company');
		$cb->cb_department = Session::get('dept_name');
		$cb->cb_section = Session::get('sect_name');
		$cb->cb_ref_num = ReferenceNumbers::get_current_reference_number('cbr');
        $cb->cb_date_filed = $input['cb_date_filed'];
        $cb->cb_status = 'New';
        $cb->cb_contact_no = Session::get('cb_contact_number');
        $cb->cb_urgency = $input['cb_urgency'];
        $cb->cb_date_needed = @$input['cb_date_needed'];
		$cb->cb_purpose_of_request = $input['cb_purpose_of_request'];
        $cb->cb_attachments = json_encode($filenames);
		if($input['comment'] != ""){
			$cb->cb_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>$input['comment']
					)
				)
			);
		}
		$cb->cb_issent = 1;
        $cb->save();
		
		$cbrd = new CompensationBenefitsRequestedDocs;
		$ctr = 1;
		$b = array();
		foreach($input['requested_documents'] as $a => $document){
			$a = array();
			if(array_key_exists('name',$document)){
				$a['cbrd_cb_id'] = $cb->cb_id;
				$a['cbrd_req_docs'] = json_encode($document);
				$a['cbrd_ref_num'] = $cb->cb_ref_num . '-' . $ctr;
				$a['cbrd_status'] = "New";
				$a['cbrd_date_filed'] = $input['cb_date_filed'];
				$b[] = $a;
				unset($a);
				$ctr++;
			}
		}
		if(count($b) > 0) $cbrd->insert($b);
		
		return true;
	}
	
	public function saveRequest($input)
	{
		$fm = new Libraries\FileManager;		
		$filenames = $fm->upload($this->attachments_path);
		
		$cb = new CompensationBenefits;
        $cb->cb_employees_id = Session::get('employee_id');
        $cb->cb_emp_name = Session::get('employee_name');
		$cb->cb_emp_id = Session::get('employeeid');
		$cb->cb_company = Session::get('company');
		$cb->cb_department = Session::get('dept_name');
		$cb->cb_section = Session::get('sect_name');
		$cb->cb_ref_num = ReferenceNumbers::get_current_reference_number('cbr');
        $cb->cb_date_filed = $input['cb_date_filed'];
        $cb->cb_status = 'New';
        $cb->cb_contact_no = Session::get('cb_contact_number');
        $cb->cb_urgency = $input['cb_urgency'];
        $cb->cb_date_needed = @$input['cb_date_needed'];
		$cb->cb_purpose_of_request = $input['cb_purpose_of_request'];
        $cb->cb_attachments = json_encode($filenames);
		if($input['comment'] != ""){
			$cb->cb_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>$input['comment']
					)
				)
			);
		}
		$cb->cb_issent = 0;
        $cb->save();
		
		$cbrd = new CompensationBenefitsRequestedDocs;
		$ctr = 1;
		$b = array();
		foreach($input['requested_documents'] as $a => $document){
			$a = array();
			if(array_key_exists('name',$document)){
				$a['cbrd_cb_id'] = $cb->cb_id;
				$a['cbrd_req_docs'] = json_encode($document);
				$a['cbrd_ref_num'] = $cb->cb_ref_num . '-' . $ctr;
				$a['cbrd_status'] = "New";
				$a['cbrd_date_filed'] = $input['cb_date_filed'];
				$b[] = $a;
				unset($a);
				$ctr++;
			}
		}
		if(count($b) > 0) $cbrd->insert($b);
		
		return true;
	}
	
	public function assignRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$cbrd->cbrd_assigned_to = $input['emp_id'];
		$cbrd->cbrd_assigned_date = date("Y-m-d");
		$cbrd->cbrd_current = $input['emp_id'];
		$cbrd->cbrd_status = 'For Processing';
		$cbrd->cbrd_remarks = '';
		$cbrd->save();
		
		return true;
	}
	
	public function returnRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		$cbrd->cbrd_assigned_to = '';
		$cbrd->cbrd_assigned_date = '';
		$cbrd->cbrd_current = $cb->cb_employees_id;
		$cbrd->cbrd_status = 'Returned';
		$cbrd->cbrd_remarks = $input['cbrd_remarks'];
		$cbrd->save();
		
		return true;
	}
	
	public function reSubmitRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$cbrd->cbrd_status = 'New';
		$cbrd->save();
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		$comments = (array) json_decode($cb->cb_comments);
		if($input['comment'] != ""){
			$comments[] = array(
				'name'=>Session::get('employee_name'),
				'datetime'=>date('m/d/Y g:i A'),
				'message'=>$input['comment']
			);
			$cb->cb_comments = json_encode($comments);
		}

		$fm = new Libraries\FileManager;		
		$filenames = $fm->upload($this->attachments_path);
		if(is_array($filenames) && count($filenames) > 0){
			$attachments = json_decode($cb->cb_attachments,'array');
			if( (is_array($attachments) && count($attachments) > 0)){
				$filenames = array_merge($filenames, $attachments);
			}
			$cb->cb_attachments = json_encode($filenames);
		}
		$cb->cb_issent = 1;
		$cb->save();
		
		return true;
	}
	
	public function forAcknowledgement($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_id","=",$input['cbrd_id'])->get();
		$cbrd = CompensationBenefitsRequestedDocs::find($cbrd[0]['cbrd_id']);
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		$comments = (array) json_decode($cb->cb_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>$input['comment']
		);

		$cb->cb_comments = json_encode($comments);
		$cb->save();
		
		$cbrd->cbrd_current = $cb->cb_employees_id;
		$cbrd->cbrd_status = 'For Acknowledgement';
		$cbrd->cbrd_endorsed_to_requestor_date = $input['date_endorsed_to_requestor'];
		$cbrd->save();
		
		return true;
	}
	
	public function acknowledgedRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$cbrd->cbrd_acknowledged_date = date("Y-m-d");
		$cbrd->cbrd_status = "Acknowledged";
		$cbrd->cbrd_completed_date = date("Y-m-d");
		$cbrd->save();
		
		return true;
	}
	
	public function doSomethings()
	{
		echo "trace";
	}
}
?>