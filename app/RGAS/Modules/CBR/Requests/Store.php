<?php namespace RGAS\Modules\CBR\Requests;

use Input;
use Validator;
use Session;
use Redirect;

class Store extends RResult
{
	public function validate()
	{
		$input = Input::all();

		// edit starts here
		$inputs['employee_name'] = $input['employee_name'];
		$rules['employee_name'] = 'required';
		$messages['employee_name.required'] = 'Employee Name is required';
		
		$inputs['employeeid'] = $input['employeeid'];
		$rules['employeeid'] = 'required';
		$messages['employeeid.required'] = 'Employee ID is required';
		
		$inputs['comp_name'] = $input['comp_name'];
		$rules['comp_name'] = 'required';
		$messages['comp_name.required'] = 'Company name is required';
		
		$inputs['dept_name'] = $input['dept_name'];
		$rules['dept_name'] = 'required';
		$messages['dept_name.required'] = 'Department name is required';
		
		$inputs['sect_name'] = $input['sect_name'];

		$rules['sect_name'] = '';
		$messages['sect_name.required'] = 'Section name is required';
		
		$inputs['cb_ref_num'] = $input['cb_ref_num'];
		$rules['cb_ref_num'] = 'required';
		$messages['cb_ref_num.required'] = 'Reference number is required';
		
		$inputs['cb_date_filed'] = $input['cb_date_filed'];
		$rules['cb_date_filed'] = 'required|date';
		$messages['cb_date_filed.required'] = 'Date filed is required';
		$messages['cb_date_filed.date'] = 'Date filed must be a valid date';
		
		$inputs['cb_status'] = $input['cb_status'];
		$rules['cb_status'] = 'required';
		$messages['cb_status.required'] = 'Status is required';
		
		$inputs['cb_contact_number'] = $input['cb_contact_number'];
		$rules['cb_contact_number'] = 'numeric';
		$messages['cb_contact_number.numeric'] = 'Contact number must be numeric';
		
		$inputs['cb_urgency'] = $input['cb_urgency'];
		$rules['cb_urgency'] = 'required';
		$messages['cb_urgency.required'] = 'Urgency field is required';
		
		$inputs['cb_purpose_of_request'] = $input['cb_purpose_of_request'];
		$rules['cb_purpose_of_request'] = 'max:5000';
		$messages['cb_purpose_of_request.max'] = 'Purpose of request length must not be greater 5000 characters';

		if(@$input['requested_documents']['pagibig']['name'] != ""){
			$inputs['pagibig_duration'] = $input['requested_documents']['pagibig']['duration'];
			$rules['pagibig_duration'] = 'required';
			$messages['pagibig_duration.required'] = 'Please select duration for Pag-Ibig';
		}
		
		if(@$input['requested_documents']['philhealth']['name'] != ""){
			$inputs['philhealth_duration'] = $input['requested_documents']['philhealth']['duration'];
			$rules['philhealth_duration'] = 'required';
			$messages['philhealth_duration.required'] = 'Please select duration for Philhealth';
		}
		
		if(@$input['requested_documents']['sss']['name'] != ""){
			$inputs['sss_duration'] = $input['requested_documents']['sss']['duration'];
			$rules['sss_duration'] = 'required';
			$messages['sss_duration.required'] = 'Please select duration for SSS';
		}

		if(@$input['requested_documents']['coe']['name'] != ""){
			$inputs['coe_number_of_copies'] = $input['requested_documents']['coe']['number_of_copies'];
			$rules['coe_number_of_copies'] = 'required';
			$messages['coe_number_of_copies.required'] = 'Please input number of copies for Cetificate of Employment';
		}
		
		if(@$input['requested_documents']['coec']['name'] != ""){
			$inputs['coec_number_of_copies'] = $input['requested_documents']['coec']['number_of_copies'];
			$rules['coec_number_of_copies'] = 'required';
			$messages['coec_number_of_copies.required'] = 'Please input number of copies for Certificate of Employment with Compensation';
		}		

		if(@$input['requested_documents']['emp_attendance']['name'] != ""){
			$inputs['emp_attendance_from'] = $input['requested_documents']['emp_attendance']['from'];
			$rules['emp_attendance_from'] = 'required|date';
			$messages['emp_attendance_from.required'] = 'Please input date(from) for Employee Attendance';
			$messages['emp_attendance_from.date'] = 'Invalid date(from) for Employee Attendance';
			
			$inputs['emp_attendance_to'] = $input['requested_documents']['emp_attendance']['to'];
			$rules['emp_attendance_to'] = 'required|date';
			$messages['emp_attendance_to.required'] = 'Please input date(to) for Employee Attendance';
			$messages['emp_attendance_to.date'] = 'Invalid date(to) for Employee Attendance';
		}
		
		if(@$input['requested_documents']['others']['name'] != ""){
			$inputs['others_value'] = $input['requested_documents']['others']['value'];
			$rules['others_value'] = 'required';
		}
		
		$doc_counter = 0;
		foreach($input['requested_documents'] as $document)
		{
			if(array_key_exists('name', $document))
			{
				$doc_counter++;
			}
		}

		$inputs['requested_documents_count'] = 	$doc_counter;
		$rules['requested_documents_count'] = 'required|numeric|size:1';
		$messages['requested_documents_count.size'] = 'Please select at least 1 document';		

		// edit until here
		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		// For CBR only
		$validator->sometimes('date_needed', 'required|date', function($input)
		{
			return $input['cb_urgency'] == "Immediate Attention Needed";
		});

		//--
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to('cbr/create')
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Some fields are incomplete.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
		
		//$messages = $validator->messages();
		
		//$this->setMessages($messages->all());
		
		//return $this->getMessages();
	}
}
?>