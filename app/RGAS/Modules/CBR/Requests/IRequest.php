<?php
namespace app\CBR;

interface IRequest
{
	// public function validate();
	public function getResult();
	public function getMessages();
}
?>