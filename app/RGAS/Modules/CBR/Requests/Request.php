<?php
namespace app\CBR;

use Input;
use Validator;

class Request implements IRequest
{
	public $rResult;
	public $rMessages;
	
	public function __construct()
	{
		
	}
	
	public function assign()
	{
		$input = Input::all();
		
		$validator = Validator::make(
			array(
				'emp_id' => $input['emp_id'],
			),
			array(
				'emp_id' => 'required|integer'
			)
		);
		
		$this->rResult = true;
		if ($validator->fails())
		{
			$this->rResult = false;
		}
		
		$messages = $validator->messages();
		
		$this->rMessages = $messages->all();
	}
	
	public function remarks()
	{
		$input = Input::all();
		
		$validator = Validator::make(
			array(
				'cbrd_remarks' => $input['cbrd_remarks'],
			),
			array(
				'cbrd_remarks' => 'required'
			)
		);
		
		$this->rResult = true;
		if ($validator->fails())
		{
			$this->rResult = false;
		}
		
		$messages = $validator->messages();
		
		$this->rMessages = $messages->all();
	}
	
	public function create()
	{
		$input = Input::all();
		$validator = Validator::make(
			array(
				'employee_name' => $input['employee_name'],
				'employeeid' => $input['employeeid'],
				'comp_name' => $input['comp_name'],
				'dept_name' => $input['dept_name'],
				'sect_name' => $input['sect_name'],
				'cb_ref_num' => $input['cb_ref_num'],
				'cb_date_filed' => $input['cb_date_filed'],
				'cb_status' => $input['cb_status'],
				'cb_contact_number' => $input['cb_contact_number'],
				'cb_urgency' => $input['cb_urgency'],
			),
			array(
				'employee_name' => 'required',
				'employeeid' => 'required',
				'comp_name' => 'required',
				'dept_name' => 'required',
				'sect_name' => 'required',
				'cb_ref_num' => 'required',
				'cb_date_filed' => 'required',
				'cb_status' => 'required',
				'cb_contact_number' => '',
				'cb_urgency' => 'required',
			)
		);
		
		$validator->sometimes('date_needed', 'required', function($input)
		{
			return $input['cb_urgency'] == "Immediate Attention Needed";
		});
		
		$validator->sometimes("requested_documents['pagibig']['duration']", 'required', function($input)
		{
			$input = Input::all();
			return array_key_exists('name',$input['requested_documents']['pagibig']);
		});
		
		$validator->sometimes('requested_documents[philhealth][duration]', 'required', function($input)
		{
			$input = Input::all();
			return array_key_exists('name',$input['requested_documents']['philhealth']);
		});
		
		$validator->sometimes('requested_documents[sss][duration]', 'required', function($input)
		{
			$input = Input::all();
			return array_key_exists('name',$input['requested_documents']['sss']);
		});
		
		$validator->sometimes('requested_documents[coe][number_of_copies]', 'required', function()
		{
			$input = Input::all();
			return array_key_exists('name',$input['requested_documents']['coe']);
		});
		
		$this->rResult = true;
		if ($validator->fails())
		{
			$this->rResult = false;
		}
		
		$messages = $validator->messages();
		
		$this->rMessages = $messages->all();
	}
	
	public function getResult()
	{
		return $this->rResult;
	}
	
	public function getMessages()
	{
		return $this->rMessages; 
	}
}
?>