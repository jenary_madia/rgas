

	public $attachments_path = 'te/';
	protected $module_id;
	private $te;

	private function getStoragePath()
	{
		return Config::get('rgas.rgas_storage_path').'te/';
	}
	
	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_te');
		return $this->module_id;
	}

	Creation of Request:

	//to save in db
	$te->te_attachments = @json_encode($input['files']);
	
	if($issent == 1){
			Mail::send('te.email_templates.te_for_assessment', array(
				'status' => $status,
				'ref_num' => $te->te_ref_num,
				'requestor' => $te->te_emp_name,
				'department' => $te->te_department),
				function($message){
					$message->to($this->getchrd()->email, $this->getchrd()->firstname." ".$this->getchrd()->lastname)->subject('RGTS Notification: TE Request for Assessment');
				}
			);
			
			//when clicked SEND , file will be moved to specified foler
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					//$fm->move($file['random_filename'], $this->attachments_path.$te->te_ref_num.'/'.$file['random_filename']);
					$fm->move($file['random_filename'], $this->getStoragePath().$te->te_ref_num); //glenn
				}
			}
		}
		
		//to call AuditLogs
		
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$this->AU004($te->te_id, $old, $new, $te->te_ref_num);
	}

	//for uploading file:
	
	