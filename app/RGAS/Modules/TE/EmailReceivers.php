<?php namespace RGAS\Modules\TE;

use Receivers;	
use Employees;
use TrainingEndorsements;

class EmailReceivers extends TELogs{

	function getrequestor($id)
	{
		return TrainingEndorsements::find($id);
	}
	
	function getchrd(){
		$receiver = Receivers::where('code','=','TE_CHRD')->where('module','=','te')->first();
		$employee = Employees::find($receiver->employeeid);
		
		return $employee;
	}

}

?>