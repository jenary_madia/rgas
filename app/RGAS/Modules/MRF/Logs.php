<?php
/**
 * Created by PhpStorm.
 * User: Jenary
 * Date: 23/12/2016
 * Time: 9:14 AM
 */

namespace RGAS\Modules\MRF;

use AuditTrails;
use Session;
use Modules;

class Logs
{
    public function __construct(){

        $this->moduleId = Modules::where("code","MRF")->first()["id"];

    }

    public function AU001($id,$tableName,$primaryKey,$new,$refNo)
    {
        $this->AT = new AuditTrails;
        $this->AT->user_id = Session::get("employee_id");
        $this->AT->date = date('Y-m-d');
        $this->AT->time = date('H:i:s');
        $this->AT->action_code = 'AU001';
        $this->AT->module_id = $this->moduleId;
        $this->AT->table_name = $tableName;
        $this->AT->params =  json_encode(array("{$tableName}.{$primaryKey}"=>$id));
        $this->AT->new = $new;
        $this->AT->ref_num = $refNo;
        $this->AT->save();
        return $this;
    }

    //Upload File
    public function AU002($id,$tableName,$primaryKey,$new,$refNo)
    {
        $this->AT = new AuditTrails;
        $this->AT->user_id = Session::get("employee_id");
        $this->AT->date = date('Y-m-d');
        $this->AT->time = date('H:i:s');
        $this->AT->action_code = 'AU002';
        $this->AT->module_id = $this->moduleId;
        $this->AT->table_name = $tableName;
        $this->AT->params =  json_encode(array("{$tableName}.{$primaryKey}"=>$id));
        $this->AT->new = $new;
        $this->AT->ref_num = $refNo;
        $this->AT->save();
        return $this;
    }

    //Retrieve
    public function AU003($id,$tableName,$primaryKey,$refNo)
    {
        $this->AT = new AuditTrails;
        $this->AT->user_id = Session::get("employee_id");
        $this->AT->date = date('Y-m-d');
        $this->AT->time = date('H:i:s');
        $this->AT->action_code = 'AU003';
        $this->AT->module_id = $this->moduleId;
        $this->AT->table_name = $tableName;
        $this->AT->params =  json_encode(array("{$tableName}.{$primaryKey}"=>$id));
        $this->AT->ref_num = $refNo;
        $this->AT->save();
        return $this;
    }

    //Edit and Update(workflow of request)
    public function AU004($id,$tableName,$primaryKey,$new,$refNo,$old)
    {
        $this->AT = new AuditTrails;
        $this->AT->user_id = Session::get("employee_id");
        $this->AT->date = date('Y-m-d');
        $this->AT->time = date('H:i:s');
        $this->AT->action_code = 'AU004';
        $this->AT->module_id = $this->moduleId;
        $this->AT->table_name = $tableName;
        $this->AT->params =  json_encode(array("{$tableName}.{$primaryKey}"=>$id));
        $this->AT->old = $old;
        $this->AT->new = $new;
        $this->AT->ref_num = $refNo;
        $this->AT->save();
        return $this;
    }

    //Delete
    public function AU005($id, $old, $refNo ,$tableName,$primaryKey)
    {
        $this->AT = new AuditTrails;
        $this->AT->user_id = Session::get("employee_id");
        $this->AT->date = date('Y-m-d');
        $this->AT->time = date('H:i:s');
        $this->AT->action_code = 'AU005';
        $this->AT->module_id = $this->moduleId;
        $this->AT->table_name = $tableName;
        $this->AT->params =  json_encode(array("{$tableName}.{$primaryKey}"=>$id));
        $this->AT->old = $old;
        $this->AT->ref_num = $refNo;
        $this->AT->save();
        return $this;
    }

    //Print
    public function AU006($te_id, $te_ref_num)
    {
        AuditTrail::store(
            Session::get('employee_id'),
            "AU006",
            $this->getModuleId(),
            "",
            json_encode(array("{$this->primaryKey}"=>$te_id)),
            "",
            "",
            $te_ref_num
        );
        return $this;
    }

    //Download
    public function AU008($id,$tableName,$primaryKey,$refNo)
    {
        $this->AT = new AuditTrails;
        $this->AT->user_id = Session::get("employee_id");
        $this->AT->date = date('Y-m-d');
        $this->AT->time = date('H:i:s');
        $this->AT->action_code = 'AU008';
        $this->AT->module_id = $this->moduleId;
        $this->AT->table_name = $tableName;
        $this->AT->params =  json_encode(array("{$tableName}.{$primaryKey}"=>$id));
        $this->AT->ref_num = $refNo;
        $this->AT->save();
        return $this;
    }
}