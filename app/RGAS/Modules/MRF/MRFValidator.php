<?php

namespace RGAS\Modules\MRF;
use Validator;
use Input;
use Redirect;
use Response;
use URL;

class MRFValidator
{
    public function send($data) {
        $inputs = [
            'requestFor' => $data['requestDetails']['requestFor'],
            'department' => $data['requestDetails']['department'],
            'positionTitle' => $data['requestDetails']['positionTitle'],
            'dateNeeded' => $data['requestDetails']['dateNeeded'],
            'nature' => $data['requestDetails']['nature'],
            'internal' => $data['requestDetails']['internal'],
            'external' => $data['requestDetails']['external'],
            'count' => $data['requestDetails']['count'],
            'otherCount' => $data['requestDetails']['otherCount'],
            'periodCovered' => $data['requestDetails']['periodCovered'],
            'reason' => $data['requestDetails']['reason'],
            'company' => $data['requestDetails']['company'],
        ];

        $rules = [
            'requestFor' => 'required',
            'department' => 'required',
            'positionTitle' => 'required',
            'dateNeeded' => 'required',
            'nature' => 'required',
            'reason' => 'required',
        ];
        
        if($data['requestDetails']['count'] == 0) {
            $rules['otherCount'] = 'required';
        }else{
            $rules['count'] = 'required';
        }

        if($data['requestDetails']['requestFor'] == 2) {
            $rules['company'] = 'required';
        }

        if(in_array($data['requestDetails']['nature'],[4,6])) {
            $rules['periodCovered'] = 'required';
        }

        if( ! $data['requestDetails']['internal'] && ! $data['requestDetails']['external']) {
            $rules['source'] = 'required';
        }


        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
    
    public function comment($data) {
        $inputs = [
            'comment' => $data['comment']
        ];

        $rules = [
            'comment' => 'required',
        ];

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];

    }
    
    public function recruitmentStaff($staffID) {
        $inputs = [
            'recruitmentStaff' => $staffID
        ];

        $rules = [
            'recruitmentStaff' => 'required',
        ];

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
                'message' => 'Something went wrong',
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function jobLevel($jobLevelID) {
        $inputs = [
            'jobLevel' => $jobLevelID
        ];

        $rules = [
            'jobLevel' => 'required',
        ];

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
                'message' => 'Something went wrong',
            ];
        }

        return [
            'success' => true,
        ];
    }
    
    public function validate($data) {
        $inputs = [];
        $rules = [];
        
        foreach ($data as $key => $value) {
            $inputs[$key] = $value;
            $rules[$key] = 'required';
        }

        $validate = Validator::make(
            $inputs,
            $rules
        );
        
        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
                'message' => 'Something went wrong',
            ];
        }

        return [
            'success' => true,
        ];
    }
    
}