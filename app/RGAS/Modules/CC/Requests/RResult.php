<?php
namespace RGAS\Modules\CC\Requests;

class RResult 
{
	private $rResult = false;
	private $rMessages;
	
	public function setResult($result)
	{
		$this->rResult = $result;
	}
	
	public function setMessages($messages)
	{
		$this->rMessages = $messages;
	}
	
	public function getResult()
	{
		return $this->rResult;
	}
	
	public function getMessages()
	{
		return $this->rMessages; 
	}
}
?>