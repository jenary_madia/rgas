<?php namespace RGAS\Modules\CC\Requests;

use Input;
use Validator;

class Store extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		// Put all your validation rules here
		// Sample validation code
		$inputs['employee_name'] = $input['employee_name'];
		$rules['employee_name'] = 'required';
		$messages['employee_name.required'] = 'Employee Name is required';
		
		
		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		$validator->sometimes('date_needed', 'required|date', function($input)
		{
			return $input['cb_urgency'] == "Immediate Attention Needed";
		});
		
		$this->setResult(true);
		if ($validator->fails())
		{
			$this->setResult(false);
		}
		
		$messages = $validator->messages();
		
		$this->setMessages($messages->all());
		
		return $this->getMessages();
	}
}
?>