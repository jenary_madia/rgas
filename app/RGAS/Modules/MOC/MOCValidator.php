<?php

namespace RGAS\Modules\MOC;
use Validator;
use Input;
use Redirect;
use Response;
use URL;

class MOCValidator
{
    public function send($data) {
        $inputs = [
            'requestTypes' => $data['mocDetails']['requestTypes'],
            'urgency' => $data['mocDetails']['urgency'],
            'changeIn' => $data['mocDetails']['changeIn'],
            'changeInOthersInput' => $data['mocDetails']['changeInOthersInput'],
            'changeType' => $data['mocDetails']['changeType'],
            'targetDate' => $data['mocDetails']['targetDate'],
            'departmentsInvolved' => $data['mocDetails']['departmentsInvolved'],
            'title' => $data['mocDetails']['title'],
            'backgroundInfo' => $data['mocDetails']['backgroundInfo'],
        ];
        
        $rules = [
            'requestTypes' => 'required',
            'urgency' => 'required',
            'title' => 'required',
            'backgroundInfo' => 'required',
        ];


        if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
            $rules['changeIn'] = 'required';
            $rules['changeType'] = 'required';
            $rules['targetDate'] = 'required';
            $rules['departmentsInvolved'] = 'required';
            if(in_array('others',$data['mocDetails']['changeIn'])) {
                $rules['changeInOthersInput'] = 'required';
            }
        }

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
    
    public function comment($data) {
        $inputs = [
            'comment' => $data['comment']
        ];

        $rules = [
            'comment' => 'required',
        ];

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];

    }

    public function DHtoADH($data) {
        $inputs = [
            'assessmentType' => $data['assessmentType']
        ];

        $rules = [
            'assessmentType' => 'required',
        ];

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
    
    public function ADHtoBSA($data) {
        $inputs = [
            'assigned_bsa' => $data['assignedBSA']
        ];

        $rules = [
            'assigned_bsa' => 'required',
        ];

        $messages = [
            'assigned_bsa:required' => 'Please select BSA',
        ];

        $validate = Validator::make(
            $inputs,
            $rules,
            $messages
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
}