<?php

namespace RGAS\Modules\MOC;
use RGAS\Libraries;
use Employees;
use MOCAttachments;
use MOCComments;
use MOCChangeInCodes;
use MOCRequests;
use MOCRequestCodes;
use MOCSignatories;
use Response;
use SelectItems;
use Session;
use URL;
use Config;
use Mail;
use DB;

class MOC
{
    protected $superiorID;
    protected $employeeid;
    protected $requested_by;
    protected $firstname;
    protected $middlename;
    protected $lastname;
    protected $company;
    protected $department;
    protected $desig_level;
    protected $dept_id;

    protected $signatories;

    protected $selectItems;
    protected $logs;

    public function __construct(){
        $this->logs = new Logs;
        $this->employeeid = Session::get('employeeid');
        $this->requested_by = Session::get('employee_id');
        $this->firstname = Session::get('firstname');
        $this->middlename = Session::get('middlename');
        $this->lastname = Session::get('lastname');
        $this->company = Session::get('company');
        $this->superiorID = Session::get("superiorid");
        $this->department = Session::get("dept_name");
        $this->desig_level = Session::get("desig_level");
        $this->dept_id = Session::get("dept_id");
        $this->dept_type = Session::get("dept_type");

        $this->selectItems = new SelectItems;
        $this->signatories = new MOCSignatories;
    }

    public function getStoragePath()
    {
        return Config::get('rgas.rgas_storage_path').'moc/';
    }

    /***********creation and own moc actions*************/

    public function createAction($data) {
        /**** SETTING UP PARAMETERS FOR moc_requests*****/
        $mocParams = [
            'employeeid' => $this->employeeid,
            'requested_by' => $this->requested_by,
            'firstname' => $this->firstname,
            'middlename' => $this->middlename,
            'lastname' => $this->lastname,
            'company' => $this->company,
            'date' => date('Y-m-d'),
            'dept_sec' => json_encode([$this->department,$data['formDetails']['section']]),
            'transaction_code' => (new MOCRequests)->generateRefNo(),
            'contact_no' => $data['formDetails']['contactNumber'],
            'urgency' => $data['mocDetails']['urgency'],
            'reason' => $data['mocDetails']['backgroundInfo'],
            'title' => $data['mocDetails']['title'],

        ];

        if($data['action'] == 'send') {
            if($this->company == 'RBC-CORP') {
                if($this->dept_id == CSMD_DEPT_ID) {
                    if (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD department head";
                    } elseif($this->desig_level == 'head') {
                        $mocParams['curr_emp'] = $this->getCSMD('supervisor');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $mocParams['assessment_type'] = $data['assessment'];
                        $successMessage = "PSR/MOC successfully sent to CSMD Assistant department head for ".($data['assessment'] == 1 ? 'initial assessment' : 'final assessment') ;
                    }
                }else {
                    if (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->getDeptHead($this->superiorID)->id;
                        $mocParams['status'] = 'FOR ENDORSEMENT';
                        $successMessage = "PSR/MOC successfully sent to department head";
                    } else {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD for processing";
                    }
                }
            }else{
                if($this->dept_id == SSMD_DEPT_ID) {
                    if($this->desig_level == 'head') {
                        $mocParams['curr_emp'] = $this->getAOM($this->dept_type);
                        $mocParams['status'] = 'FOR APPROVAL';
                        $successMessage = "PSR/MOC successfully sent to Assistant Operations Manager for approval";
                    }elseif (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->getSSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $mocParams['assessment_type'] = $data['assessment'];
                        $successMessage = "PSR/MOC successfully sent to SSMD department head for endorsement" ;
                    }elseif (in_array($this->desig_level, ['admin-aom', 'plant-aom'])) {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD department head";
                    }
                }else {
                    if (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->superiorID;
                        $mocParams['status'] = 'FOR ENDORSEMENT';
                        $successMessage = "PSR/MOC successfully sent to immediate superior";
                    }else{
                        $mocParams['curr_emp'] = $this->getSSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to SSMD for processing";
                    }
                }
            }
        }else{
            $mocParams['status'] = 'NEW';
            $successMessage = 'PSR/MOC successfully saved and can be edited later.';
        }


//        if (in_array(MOC_REQ_TYPES_ENABLE,$data['mocDetails']['requestTypes'])) {
        if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
            $mocParams['dept_involved'] = json_encode($data['mocDetails']['departmentsInvolved']);
            $mocParams['change_type'] = $data['mocDetails']['changeType'];
            $mocParams['implementation_date'] = ($data['mocDetails']['targetDate'] ? $data['mocDetails']['targetDate'] : null);
        }
        /*************INSERT INTO moc_requests*******************/
        $mocRequest = MOCRequests::create($mocParams);


        /**** SETTING UP PARAMETERS FOR moc_requestcodes*****/
        $RCodes = $this->selectItems->getDesc('moc','request_type',$data['mocDetails']['requestTypes']);
        $fetchRCodes = [];
        foreach ($RCodes as $key) $fetchRCodes[$key->item] = $key->text;
        $codesToInsert = [];
        foreach ($data['mocDetails']['requestTypes'] as $key) {
            array_push($codesToInsert,[
                'request_id' => $mocRequest['id'],
                'request_code' => $key,
                'request_desc' => $fetchRCodes[$key]
            ]);
        }

        /*************INSERT INTO moc_requestcodes*******************/
        if(count($codesToInsert) > 0) {
            MOCRequestCodes::insert($codesToInsert);
        }


        if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
            /**** SETTING UP PARAMETERS FOR moc_changeincodes*****/
            $CCodes = $this->selectItems->getDesc('moc','change_in',$data['mocDetails']['changeIn']);
            $fetchCCodes = [];
            foreach ($CCodes as $key) $fetchCCodes[$key->item] = $key->text;
            $CCodesToInsert = [];
            foreach ($data['mocDetails']['changeIn'] as $key) {
                array_push($CCodesToInsert, [
                    'request_id' => $mocRequest['id'],
                    'changein_code' => $key,
                    'changein_desc' => ($key == 'others' ? $data['mocDetails']['changeInOthersInput'] : $fetchCCodes[$key])
                ]);
            }
            /*************INSERT INTO moc_changeincodes*******************/
            if(count($CCodesToInsert) > 0) {
                MOCChangeInCodes::insert($CCodesToInsert);
            }
        }

        /**** SETTING UP PARAMETERS FOR moc_attachments*****/
        $fm = new Libraries\FileManager;
        $attachmentsToInsert = [];
        if($data['attachments']) {
            $this->logs->AU002($mocRequest['id'], 'MOC', 'id', json_encode($mocParams), $mocParams['transaction_code']);
            foreach ($data['attachments'] as $key) {
                if ($data['action'] == "send") {
                    $fm->move($key['random_filename'], $this->getStoragePath() . $mocParams['transaction_code']);
                }
                array_push($attachmentsToInsert, [
                    'attachment_type' => 0,
                    'employeeid' => $this->requested_by,
                    'fn' => json_encode($key),
                    'request_id' => $mocRequest['id']
                ]);
            }
        }

        /*************INSERT INTO moc_attachments*******************/
        if(count($attachmentsToInsert) > 0) {
            MOCAttachments::insert($attachmentsToInsert);
        }


        /**** SETTING UP PARAMETERS FOR moc_comments*****/
        $commentParam = [
            'request_id' => $mocRequest['id'],
            'employee_id' => $this->requested_by,
            'comment' => $data['comment']
        ];
        /*************INSERT INTO moc_comments*******************/
        MOCComments::create($commentParam);
        /*********EMAIL************/
        if($data['action'] == 'send') {
            $this->sendMail(
                'checking',
                $mocParams['status'],
                $mocParams['transaction_code'],
                $this->firstname . ' ' . $this->middlename . ' ' . $this->lastname,
                $this->department,
                $this->getEmail($mocParams['curr_emp'])
            );
        }
        /********LOGS**********/
        $this->logs->AU001($mocRequest['id'],'MOC','id',json_encode($mocParams),$mocParams['transaction_code']);

        /***************DISPLAY RESPONSE********************/
        return Response::json([
            'success' => true,
            'message' => $successMessage,
            'url' => URL::to("/moc"),
            200
        ]);
    }
        
    public function reCreateAction($data) {
        /**** GETTING DATA BY TRANSACTION CODE *****/
        $refno =  $data['formDetails']['referenceNo'];
        $moc = MOCRequests::where("transaction_code",$refno);
        $mocDetails = $moc->first();
        /**** SETTING UP PARAMETERS FOR moc_requests*****/
        $mocParams = [
           'date' => date('Y-m-d'),
           'dept_sec' => json_encode([$this->department,$data['formDetails']['section']]),
           'contact_no' => $data['formDetails']['contactNumber'],
           'urgency' => $data['mocDetails']['urgency'],
           'reason' => $data['mocDetails']['backgroundInfo'],
           'title' => $data['mocDetails']['title'],
        ];

        if($data['action'] == 'send') {
            if($this->company == 'RBC-CORP') {
                if($this->dept_id == CSMD_DEPT_ID) {
                    if (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD department head";
                    } elseif($this->desig_level == 'head') {
                        $mocParams['curr_emp'] = $this->getCSMD('supervisor');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $mocParams['assessment_type'] = $data['assessment'];
                        $successMessage = "PSR/MOC successfully sent to CSMD Assistant department head for ".($data['assessment'] == 1 ? 'initial assessment' : 'final assessment') ;
                    }
                }else {
                    if (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->getDeptHead($this->superiorID)->id;
                        $mocParams['status'] = 'FOR ENDORSEMENT';
                        $successMessage = "PSR/MOC successfully sent to department head";
                    } else {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD for processing";
                    }
                }
            }else{
                if($this->dept_id == SSMD_DEPT_ID) {
                    if($this->desig_level == 'head') {
                        $mocParams['curr_emp'] = $this->getAOM($this->dept_type);
                        $mocParams['status'] = 'FOR APPROVAL';
                        $successMessage = "PSR/MOC successfully sent to Assistant Operations Manager for approval";
                    }elseif (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->getSSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $mocParams['assessment_type'] = $data['assessment'];
                        $successMessage = "PSR/MOC successfully sent to SSMD department head for endorsement" ;
                    }elseif (in_array($this->desig_level, ['admin-aom', 'plant-aom'])) {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD department head";
                    }
                }else {
                    if (in_array($this->desig_level, ['employee', 'supervisor'])) {
                        $mocParams['curr_emp'] = $this->superiorID;
                        $mocParams['status'] = 'FOR ENDORSEMENT';
                        $successMessage = "PSR/MOC successfully sent to immediate superior";
                    }elseif (in_array($this->desig_level, ['admin-aom', 'plant-aom'])) {
                        $mocParams['curr_emp'] = $this->getCSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to CSMD department head";
                    }else{
                        $mocParams['curr_emp'] = $this->getSSMD('head');
                        $mocParams['status'] = 'FOR ASSESSMENT';
                        $successMessage = "PSR/MOC successfully sent to SSMD for processing";
                    }
                }
            }



        }else{
            $mocParams['status'] = 'NEW';
            $successMessage = 'Successfully updated';
        }

        if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
           $mocParams['dept_involved'] = json_encode($data['mocDetails']['departmentsInvolved']);
           $mocParams['change_type'] = $data['mocDetails']['changeType'];
           $mocParams['implementation_date'] = ($data['mocDetails']['targetDate'] != "" ? $data['mocDetails']['targetDate'] : null);;
        }
        /***************INSERT TO moc_requests********************/
        $moc->update($mocParams);

        /**** SETTING UP PARAMETERS FOR moc_requestcodes*****/
        $RCodes = $this->selectItems->getDesc('moc','request_type',$data['mocDetails']['requestTypes']);
        $fetchRCodes = [];
        foreach ($RCodes as $key) $fetchRCodes[$key->item] = $key->text;
        $codesToInsert = [];
        foreach ($data['mocDetails']['requestTypes'] as $key) {
            array_push($codesToInsert,[
                'request_id' => $mocDetails['request_id'],
                'request_code' => $key,
                'request_desc' => $fetchRCodes[$key]
            ]);
        }
        /*************DELETE moc_requestcodes*******************/
        $old = MOCRequestCodes::where('request_id',$mocDetails['request_id']);
        $old->delete();
        /*************INSERT INTO moc_requestcodes*******************/
        if(count($codesToInsert) > 0) {
            MOCRequestCodes::insert($codesToInsert);
        }

        if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
            /**** SETTING UP PARAMETERS FOR moc_changeincodes*****/
            $CCodes = $this->selectItems->getDesc('moc','change_in',$data['mocDetails']['changeIn']);
            $fetchCCodes = [];
            foreach ($CCodes as $key) $fetchCCodes[$key->item] = $key->text;
            $CCodesToInsert = [];
            foreach ($data['mocDetails']['changeIn'] as $key) {
                array_push($CCodesToInsert, [
                    'request_id' => $mocDetails['request_id'],
                    'changein_code' => $key,
                    'changein_desc' => ($key == 'others' ? $data['mocDetails']['changeInOthersInput'] : $fetchCCodes[$key])
                ]);
            }
            /*************DELETE moc_changeincodes*******************/
            $old = MOCChangeInCodes::where('request_id',$mocDetails['request_id']);
            $old->delete();
            /*************INSERT INTO moc_changeincodes*******************/
            if(count($CCodesToInsert) > 0) {
                MOCChangeInCodes::insert($CCodesToInsert);
            }
        }

        /**** SETTING UP PARAMETERS FOR moc_attachments*****/
        $fm = new Libraries\FileManager;
        $attachmentsToInsert = [];
        if($data['attachments']) {
            /********LOGS**********/
            $this->logs->AU002($mocDetails['request_id'], 'MOC', 'id', json_encode($mocParams), $mocDetails['transaction_code']);
            foreach ($data['attachments'] as $key) {
                if ($data['action'] == "send") {
                    $fm->move($key['random_filename'], $this->getStoragePath() . $refno);
                }
                array_push($attachmentsToInsert, [
                    'attachment_type' => 0,
                    'employeeid' => $this->requested_by,
                    'fn' => json_encode([
                        "random_filename" => $key['random_filename'],
                        "original_filename" => $key['original_filename'],
                        "filesize" => $key['filesize'],
                        "mime_type" => $key['mime_type'],
                        "original_extension" => $key['original_filename']
                    ]),
                    'request_id' => $mocDetails['request_id']
                ]);
            }
        }
        /*************DELETE moc_attachments*******************/
        $old = MOCAttachments::where('request_id',$mocDetails['request_id']);
        $old->delete();
        /*************INSERT INTO moc_attachments*******************/
        if(count($attachmentsToInsert) > 0) {
            MOCAttachments::insert($attachmentsToInsert);
        }


        /**** SETTING UP PARAMETERS FOR moc_comments*****/
        $commentParam = [
            'request_id' => $mocDetails['request_id'],
            'employee_id' => $this->requested_by,
            'comment' => $data['comment']
        ];

        /*************INSERT INTO moc_comments*******************/
        if($data['formDetails']['status'] == 'NEW') {
            $old = MOCComments::where('request_id',$mocDetails['request_id']);
            $old->update(['comment' => $data['comment'] ]);
        }else{
            MOCComments::create($commentParam);
        }

        /***************DELETE SIGNATORIES********************/
        MOCSignatories::where('moc_request_id',$mocDetails['request_id'])->delete();

        /********LOGS**********/
        $this->logs->AU001($mocDetails['request_id'],'MOC','id',json_encode($mocParams),$mocParams['transaction_code']);

        /**********EMAIL**************/
        if($data['action'] == 'send') {

            $this->sendMail(
                'checking',
                $mocParams['status'],
                $refno,
                $this->firstname.' '.$this->middlename.' '.$this->lastname,
                $this->department,
                $this->getEmail($mocParams['curr_emp'])
            );
        }

        /***************DISPLAY RESPONSE********************/
        return Response::json([
           'success' => true,
           'message' => $successMessage,
           'url' => URL::to("/moc"),
           200
        ]);
    }

    public function cancelMOC($data) {
        $mocDetails = MOCRequests::where('request_id',$data['id'])
            ->where('employeeid',Session::get("employeeid"));
        if(in_array($this->desig_level,['admin-aom','plant-aom'])) {
            $mocDetails->whereIn('status',['FOR ENDORSEMENT','FOR APPROVAL']);
        }else{
            $mocDetails->where('status','FOR ENDORSEMENT');
        }
        if(count($mocDetails->first())) {
            $mocDetails->update([
                'status' => 'CANCELLED',
                'curr_emp' => Session::get("employee_id")
            ]);

            /******EMAIL*****/
            $this->sendMail(
                'CANCEL',
                'CANCELLED',
                $mocDetails->first()['transaction_code'],
                $this->firstname.' '.$this->middlename.' '.$this->lastname,
                $this->department,
                $this->getEmail($mocDetails['curr_emp'])
            );

            return Response::json(array(
                'success' => true,
                'message' => 'MOC successfully canceled',
                'url' => URL::to("/moc"),
                200
            ));
        }
        
        return Response::json(array(
            'success' => false,
            'message' => 'Unknown action',
            400
        ));
    }

    /***********returning to filer*************/
    public function returnToFiler($data) {
        $mocID = $data['id'];
        $moc = MOCRequests::where("request_id",$mocID)
            ->where("curr_emp",Session::get('employee_id'));
        if ($moc->first()) {
            DB::beginTransaction();
            $mocDetails = $moc->first();
            /**** SETTING UP PARAMETERS FOR moc_request*****/
            $params = [
                'status' => 'DISAPPROVED',
                'curr_emp' => $moc->first()['requested_by']
            ];

            /*************INSERT INTO moc_request*******************/
            $request = $moc->update($params);
            if(! $request) {
                DB::rollback();
                goto error;
            }
            /**** SETTING UP PARAMETERS FOR moc_comments*****/
            $commentParam = [
                'request_id' => $mocID,
                'employee_id' => Session::get('employee_id'),
                'comment' => $data['comment']
            ];

            /*************INSERT INTO moc_comments*******************/
            MOCComments::create($commentParam);

            /**********EMAIL************/
            $this->sendMail(
                'DISAPPROVED',
                'DISAPPROVED',
                $mocDetails['transaction_code'],
                $this->firstname.' '.$this->middlename.' '.$this->lastname,
                $this->department,
                $this->getEmail($params['curr_emp'])
            );


            /***************DISPLAY RESPONSE********************/
            DB::commit();
            return Response::json([
                'success' => true,
                'message' => 'Successfully returned to Filer',
                'url' => URL::to("/moc"),
                200
            ]);

        }
        error :
        return Response::json(array(
            'success' => false,
            'errors' => ['Error upon sending request'],
            'message' => 'Something went wrong',
            400
        ));
    }

    /***********send to filer for acknowledgement*************/
    public function sendToFilerForAcknowledgement($data) {
        $mocID = $data['id'];
        $moc = MOCRequests::where("request_id",$mocID)
            ->where("curr_emp",Session::get('employee_id'));
        if ($moc->first()) {
            $mocDetails = $moc->first();
            DB::beginTransaction();
            /**** SETTING UP PARAMETERS FOR moc_request*****/
            $params = [
                'status' => 'FOR ACKNOWLEDGEMENT',
                'curr_emp' => $moc->first()['requested_by'],
            ];

            /*************INSERT INTO moc_request*******************/
            $request = $moc->update($params);
            if(! $request) {
                DB::rollback();
                goto error;
            }
            /**** SETTING UP PARAMETERS FOR moc_comments*****/
            $commentParam = [
                'request_id' => $mocID,
                'employee_id' => Session::get('employee_id'),
                'comment' => $data['comment']
            ];

            /*************INSERT INTO moc_comments*******************/
            MOCComments::create($commentParam);

            /*************INSERT INTO moc_signatories********/
            $this->signatories->addSignatories($mocID,7);

            /**********EMAIL************/
            $this->sendMail(
                'checking',
                $params['status'],
                $mocDetails['transaction_code'],
                $this->firstname.' '.$this->middlename.' '.$this->lastname,
                $this->department,
                $this->getEmail($params['curr_emp'])
            );

            /*****Audit Trails*****/
            $this->logs->AU004($mocDetails['request_id'],'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

            /***************DISPLAY RESPONSE********************/
            DB::commit();
            return Response::json([
                'success' => true,
                'message' => 'Successfully send to filer for acknowledgement',
                'url' => URL::to("/moc"),
                200
            ]);

        }
        error :
        return Response::json(array(
            'success' => false,
            'errors' => ['Error upon sending request'],
            'message' => 'Something went wrong',
            400
        ));
    }

    /***********acknowledge moc*************/
    public function acknowledgeMOC($data) {
        $mocID = $data['id'];
        $moc = MOCRequests::where("request_id",$mocID)
            ->where("curr_emp",Session::get('employee_id'));
        if ($moc->first()) {
            $mocDetails = $moc->first();
            DB::beginTransaction();
            /**** SETTING UP PARAMETERS FOR moc_request*****/
            $params = [
                'status' => 'ACKNOWLEDGED',
                'acknowledge_date' => date('Y-m-d'),
                'acknowledge_by' => Session::get('employee_id')
            ];

            /*************INSERT INTO moc_request*******************/
            $request = $moc->update($params);
            if(! $request) {
                DB::rollback();
                goto error;
            }
            /**** SETTING UP PARAMETERS FOR moc_comments*****/
            $commentParam = [
                'request_id' => $mocID,
                'employee_id' => Session::get('employee_id'),
                'comment' => $data['comment']
            ];

            /*************INSERT INTO moc_comments*******************/
            MOCComments::create($commentParam);

            /*************INSERT INTO moc_signatories********/
            $this->signatories->addSignatories($mocID,8);

            /*****Audit Trails*****/
            $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

            /***************DISPLAY RESPONSE********************/
            DB::commit();
            return Response::json([
                'success' => true,
                'message' => 'PSR/MOC successfully acknowledged',
                'url' => URL::to("/moc"),
                200
            ]);

        }
        error :
        return Response::json(array(
            'success' => false,
            'errors' => ['Error upon sending request'],
            'message' => 'Something went wrong',
            400
        ));
    }

    /***********ROUTING*************/
        
        /***GENERAL ROUTING****/
        public function sendToMD($data,$process = 1) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $params = [
                    'status' => 'FOR ASSESSMENT',
                    'endorsed_date' => date('Y-m-d'),
                ];
                /****--------------*******Updating data for MOC if status is 'for endorsement'****-----------*********/
                if( $mocDetails['status'] == 'FOR ENDORSEMENT' ) {

                        $params['urgency'] = $data['mocDetails']['urgency'];
                        $params['reason'] = $data['mocDetails']['backgroundInfo'];
                        $params['title'] = $data['mocDetails']['title'];

                    if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
                        $params['dept_involved'] = json_encode($data['mocDetails']['departmentsInvolved']);
                        $params['change_type'] = $data['mocDetails']['changeType'];
                        $params['implementation_date'] = ($data['mocDetails']['targetDate'] != "" ? $data['mocDetails']['targetDate'] : null);;
                    }

                    /**** SETTING UP PARAMETERS FOR moc_requestcodes*****/
                    $RCodes = $this->selectItems->getDesc('moc','request_type',$data['mocDetails']['requestTypes']);
                    $fetchRCodes = [];
                    foreach ($RCodes as $key) $fetchRCodes[$key->item] = $key->text;
                    $codesToInsert = [];
                    foreach ($data['mocDetails']['requestTypes'] as $key) {
                        array_push($codesToInsert,[
                            'request_id' => $mocDetails['request_id'],
                            'request_code' => $key,
                            'request_desc' => $fetchRCodes[$key]
                        ]);
                    }
                    /*************DELETE moc_requestcodes*******************/
                    $old = MOCRequestCodes::where('request_id',$mocDetails['request_id']);
                    $old->delete();
                    /*************INSERT INTO moc_requestcodes*******************/
                    if(count($codesToInsert) > 0) {
                        MOCRequestCodes::insert($codesToInsert);
                    }

                    if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
                        /**** SETTING UP PARAMETERS FOR moc_changeincodes*****/
                        $CCodes = $this->selectItems->getDesc('moc','change_in',$data['mocDetails']['changeIn']);
                        $fetchCCodes = [];
                        foreach ($CCodes as $key) $fetchCCodes[$key->item] = $key->text;
                        $CCodesToInsert = [];
                        foreach ($data['mocDetails']['changeIn'] as $key) {
                            array_push($CCodesToInsert, [
                                'request_id' => $mocDetails['request_id'],
                                'changein_code' => $key,
                                'changein_desc' => ($key == 'others' ? $data['mocDetails']['changeInOthersInput'] : $fetchCCodes[$key])
                            ]);
                        }
                        /*************DELETE moc_changeincodes*******************/
                        $old = MOCChangeInCodes::where('request_id',$mocDetails['request_id']);
                        $old->delete();
                        /*************INSERT INTO moc_changeincodes*******************/
                        if(count($CCodesToInsert) > 0) {
                            MOCChangeInCodes::insert($CCodesToInsert);
                        }
                    }

                    /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                    $fm = new Libraries\FileManager;
                    $attachmentsToInsert = [];
                    foreach ($data['attachments'] as $key) {
                        $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                        array_push($attachmentsToInsert,[
                            'attachment_type' => 0,
                            'employeeid' => $this->requested_by,
                            'fn' => json_encode([
                                "random_filename" => $key['random_filename'],
                                "original_filename" => $key['original_filename'],
                                "filesize" => $key['filesize'],
                                "mime_type" => $key['mime_type'],
                                "original_extension" => $key['original_filename']
                            ]),
                            'request_id' => $mocDetails['request_id']
                        ]);
                    }
                    /*************DELETE moc_attachments*******************/
                    $old = MOCAttachments::where('request_id',$mocDetails['request_id']);
                    $old->delete();
                    /*************INSERT INTO moc_attachments*******************/
                    if(count($attachmentsToInsert) > 0) {
                        MOCAttachments::insert($attachmentsToInsert);
                    }

                }
                /****--------------*******ENDDDDD Updating data for MOC if status is 'for endorsement'****-----------*********/

                $signatureType = 1;
                $md = 'CSMD';
                if($this->company === 'RBC-CORP') {
                    $params['curr_emp'] = $this->getCSMD("head");
                }else{
                    if ($process == 1) {
                        $params['curr_emp'] = $this->getSSMD("head");
                        $md = 'SSMD';
                    }else{
                        $params['curr_emp'] = $this->getCSMD("head");
                        $params['ssmd_action_plan'] = $data['SSMDActionPlan'];
                        $signatureType = 3;
                        /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                        $fm = new Libraries\FileManager;
                        $attachmentsToInsert = [];
                        foreach ($data['SSMDattachments'] as $key) {
                            $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                            array_push($attachmentsToInsert,[
                                'attachment_type' => 1,
                                'employeeid' => $this->requested_by,
                                'fn' => json_encode([
                                    "random_filename" => $key['random_filename'],
                                    "original_filename" => $key['original_filename'],
                                    "filesize" => $key['filesize'],
                                    "mime_type" => $key['mime_type'],
                                    "original_extension" => $key['original_filename']
                                ]),
                                'request_id' => $mocID
                            ]);
                        }

                        /*************DELETE moc_attachments*******************/
                        $old = MOCAttachments::where('request_id',$mocID)
                            ->where('attachment_type',1);
                        $old->delete();
                        /*************INSERT INTO moc_attachments*******************/
                        if(count($attachmentsToInsert) > 0) {
                            MOCAttachments::insert($attachmentsToInsert);
                        }
                    }
                }
    
                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];
    
                /*************INSERT INTO moc_comments*******************/
                    MOCComments::create($commentParam);
    
                /*************INSERT INTO moc_signatories********/
                    $this->signatories->addSignatories($mocID,$signatureType);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    $params['status'],
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "Successfully sent to $md",
                    'url' => URL::to("/moc"),
                    200
                ]);
    
            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }
    
        public function sendDHtoADH($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $adh = $this->getCSMD('supervisor');
                $params = [
                    'assessment_type' => $data['assessmentType'],
                    'curr_emp' => $adh,
                    'csmd_action_plan' => $data['CSMDActionPlan']
                ];
                if ($data['assessmentType'] == 1) {
                    $params['initial_assessment_date'] = date('Y-m-d');
                    $signatureType = 2;
                    $assessmentType = 'INITIAL';
                }elseif($data['assessmentType'] == 2) {
                    $params['assessment_date'] = date('Y-m-d');
                    $signatureType = 4;
                    $assessmentType = 'FINAL';
                }

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }


                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                foreach ($data['CSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                    array_push($attachmentsToInsert,[
                        'attachment_type' => 2,
                        'employeeid' => $this->requested_by,
                        'fn' => json_encode([
                            "random_filename" => $key['random_filename'],
                            "original_filename" => $key['original_filename'],
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_filename']
                        ]),
                        'request_id' => $mocID
                    ]);
                }

                /*************DELETE moc_attachments*******************/
                $old = MOCAttachments::where('request_id',$mocID)
                ->where('attachment_type',2);
                $old->delete();
                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MOCAttachments::insert($attachmentsToInsert);
                }


                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/

                $this->signatories->addSignatories($mocID,$signatureType,$adh);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));


                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR ASSESSMENT',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "Successfully sent to CSMD Assisstant Department Head for $assessmentType assessment",
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }

        public function sendADHtoDH($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $csmdHead = $this->getCSMD('head');
                $params = [
                    'curr_emp' => $csmdHead,
                    'csmd_action_plan' => $data['CSMDActionPlan']
                ];

                if ($data['assessmentType'] == 1) {
                    $signatureType = 3;
                }elseif($data['assessmentType'] == 2) {
                    $signatureType = 5;
                    $params['received_by'] = $csmdHead;
                }

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                foreach ($data['CSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                    array_push($attachmentsToInsert,[
                        'attachment_type' => 2,
                        'employeeid' => $this->requested_by,
                        'fn' => json_encode([
                            "random_filename" => $key['random_filename'],
                            "original_filename" => $key['original_filename'],
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_filename']
                        ]),
                        'request_id' => $mocID
                    ]);
                }

                /*************DELETE moc_attachments*******************/
                $old = MOCAttachments::where('request_id',$mocID)
                    ->where('attachment_type',2);
                $old->delete();
                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MOCAttachments::insert($attachmentsToInsert);
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,$signatureType);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));


                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR ASSESSMENT',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "PSR/MOC successfully sent to CSMD Department Head for review",
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }

        public function sendADHtoBSA($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $bsa = $data['assignedBSA'];
                $params = [
                    'curr_emp' => $bsa,
                    'assigned_to' => $bsa,
                    'assigned_date' => date('Y-m-d'),
                    'csmd_action_plan' => $data['CSMDActionPlan']
                ];

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                foreach ($data['CSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                    array_push($attachmentsToInsert,[
                        'attachment_type' => 2,
                        'employeeid' => $this->requested_by,
                        'fn' => json_encode([
                            "random_filename" => $key['random_filename'],
                            "original_filename" => $key['original_filename'],
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_filename']
                        ]),
                        'request_id' => $mocID
                    ]);
                }

                /*************DELETE moc_attachments*******************/
                $old = MOCAttachments::where('request_id',$mocID)
                    ->where('attachment_type',2);
                $old->delete();
                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MOCAttachments::insert($attachmentsToInsert);
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,4,$bsa);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR ASSESSMENT',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "PSR/MOC successfully sent to BSA for assessment",
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }

        public function returntoBSA($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $bsa = $data['assignedBSA'];
                $params = [
                    'curr_emp' => $bsa,
                    'assigned_to' => $bsa,
                    'assigned_date' => date('Y-m-d'),
                    'csmd_action_plan' => $data['CSMDActionPlan']
                ];

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,4,$bsa);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "PSR/MOC successfully sent to BSA for assessment",
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }

        public function sendBSAtoADH($data) {
        $mocID = $data['id'];
        $moc = MOCRequests::where("request_id",$mocID)
            ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $csmdADH = $this->getCSMD('supervisor');
                $params = [
                    'curr_emp' => $csmdADH,
                    'csmd_action_plan' => $data['CSMDActionPlan']
                ];

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                foreach ($data['CSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                    array_push($attachmentsToInsert,[
                        'attachment_type' => 2,
                        'employeeid' => $this->requested_by,
                        'fn' => json_encode([
                            "random_filename" => $key['random_filename'],
                            "original_filename" => $key['original_filename'],
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_filename']
                        ]),
                        'request_id' => $mocID
                    ]);
                }

                /*************DELETE moc_attachments*******************/
                $old = MOCAttachments::where('request_id',$mocID)
                    ->where('attachment_type',2);
                $old->delete();
                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MOCAttachments::insert($attachmentsToInsert);
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,5,Session::get('employee_id'));

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR ASSESSMENT',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "PSR/MOC successfully sent to CSMD Assistant Department Head for review",
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }

        public function sendtoDivHead($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'));
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/
                $divHead = $this->getDvHead(CSMD_CORP_DIVHEAD);
                $params = [
                    'curr_emp' => $divHead,
                    'status' => 'FOR APPROVAL',
                    'csmd_action_plan' => $data['CSMDActionPlan']
                ];
                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                foreach ($data['CSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                    array_push($attachmentsToInsert,[
                        'attachment_type' => 2,
                        'employeeid' => $this->requested_by,
                        'fn' => json_encode([
                            "random_filename" => $key['random_filename'],
                            "original_filename" => $key['original_filename'],
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_filename']
                        ]),
                        'request_id' => $mocID
                    ]);
                }

                /*************DELETE moc_attachments*******************/
                $old = MOCAttachments::where('request_id',$mocID)
                    ->where('attachment_type',2);
                $old->delete();
                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MOCAttachments::insert($attachmentsToInsert);
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,6);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR APPROVAL',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => "PSR/MOC successfully sent to Division Head for approval",
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }
        /***********Satellite actions*************/
        public function sendToAOM($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'))
                ->with("owner");
            if ($moc->first()) {
                DB::beginTransaction();
                $mocDetails = $moc->first();
                /**** SETTING UP PARAMETERS FOR moc_request*****/

                $params = [
                    'status' => 'FOR APPROVAL',
                    'curr_emp' => $this->getAOM($mocDetails['owner']['department']['dept_type']),
                    'ssmd_action_plan' => $data['SSMDActionPlan']
                ];

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                foreach ($data['SSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath().$mocDetails['transaction_code']);
                    array_push($attachmentsToInsert,[
                        'attachment_type' => 1,
                        'employeeid' => $this->requested_by,
                        'fn' => json_encode([
                            "random_filename" => $key['random_filename'],
                            "original_filename" => $key['original_filename'],
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_filename']
                        ]),
                        'request_id' => $mocID
                    ]);
                }

                /*************DELETE moc_attachments*******************/
                $old = MOCAttachments::where('request_id',$mocID)
                    ->where('attachment_type',1);
                $old->delete();
                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MOCAttachments::insert($attachmentsToInsert);
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,2);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR APPROVAL',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => 'Successfully sent to assistant operation management',
                    'url' => URL::to("/moc/submitted-moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }

        public function sendDivHeadtoSSMD($data) {
            $mocID = $data['id'];
            $moc = MOCRequests::where("request_id",$mocID)
                ->where("curr_emp",Session::get('employee_id'))
                ->with("owner");
            if ($moc->first()) {
                $mocDetails = $moc->first();
                DB::beginTransaction();
                /**** SETTING UP PARAMETERS FOR moc_request*****/

                $params = [
                    'status' => 'FOR ACKNOWLEDGEMENT',
                    'curr_emp' => $this->getSSMD("head")
                ];

                /*************INSERT INTO moc_request*******************/
                $request = $moc->update($params);
                if(! $request) {
                    DB::rollback();
                    goto error;
                }

                /**** SETTING UP PARAMETERS FOR moc_comments*****/
                $commentParam = [
                    'request_id' => $mocID,
                    'employee_id' => Session::get('employee_id'),
                    'comment' => $data['comment']
                ];

                /*************INSERT INTO moc_comments*******************/
                MOCComments::create($commentParam);

                /*************INSERT INTO moc_signatories********/
                $this->signatories->addSignatories($mocID,7);

                /*****Audit Trails*****/
                $this->logs->AU004($mocID,'MOC','id',json_encode($params),$mocDetails['transaction_code'],json_encode($mocDetails));

                /**********EMAIL************/
                $this->sendMail(
                    'checking',
                    'FOR ASSESSMENT',
                    $mocDetails['transaction_code'],
                    $this->firstname.' '.$this->middlename.' '.$this->lastname,
                    $this->department,
                    $this->getEmail($params['curr_emp'])
                );

                /***************DISPLAY RESPONSE********************/
                DB::commit();
                return Response::json([
                    'success' => true,
                    'message' => 'Successfully sent to SSMD for acknowledgement',
                    'url' => URL::to("/moc"),
                    200
                ]);

            }
            error :
            return Response::json(array(
                'success' => false,
                'errors' => ['Error upon sending request'],
                'message' => 'Something went wrong',
                400
            ));
        }


    /***END ROUTING****/
    /***************private functions*********************/
    private function getDeptHead($superiorId) {
        $personnel = Employees::where('id',$superiorId)->first(['desig_level','id']);
        if($personnel['desig_level'] != 'head') {
            while ($personnel['desig_level'] != 'head') {
                $personnel = Employees::where('id',$superiorId)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                $superiorId = $personnel['superiorid'];
            }
        }elseif ($personnel['desig_level'] == 'head'){
            $personnel = Employees::where('id',$superiorId)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
        }
        
        return ($personnel ? $personnel : null);
    }

    private function getCSMD($type) {
        return Employees::where('departmentid',CSMD_DEPT_ID)
            ->where('desig_level',$type)
            ->first()->id;
    }

    public function getSSMD($type) {
        return Employees::where('departmentid',SSMD_DEPT_ID)
            ->where('desig_level',$type)
            ->first()->id;
    }
    
    private function getDvHead($company_type) {

        $result = DB::table('receivers')
            ->join('employees','employees.id','=','receivers.employeeid')
            ->where('receivers.code' , $company_type)
            ->first()->id;

        return $result;
    }
    
    private function getAOM($type) {
        $currEmp = Employees::where('company','!=','RBC-CORP');
        if($type == 'admin') {
            $currEmp->where('desig_level','admin-aom');
        }elseif($type == 'plant') {
            $currEmp->where('desig_level','plant-aom');
        }

        return $currEmp->first()['id'];
    }

    private function sendMail($action,$status,$refno,$filer,$department,$email) {
        if($email) {
            if ($action == "DISAPPROVED") {
                Mail::send('moc.email.disapprove', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer){
                        $message->to($email,$filer)->subject('RGAS Notification Alert: Disapproved MOC');
                    }
                );
            }elseif ($action == "CANCEL") {
                Mail::send('moc.email.cancel', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer){
                        $message->to($email,$filer)->subject('RGAS Notification Alert: Cancelled MOC');
                    }
                );
            }else{
                Mail::send('moc.email.moc', array(
                    'action' => $action,
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer,$action){
                        $message->to($email,$filer)->subject("RGAS Notification Alert: MOC for $action");
                    }
                );
            }
        }

    }

    private function getEmail($id) {
        return Employees::where('id',$id)->first()['email'];
    }
}