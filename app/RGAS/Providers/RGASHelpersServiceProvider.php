<?php namespace app\RGAS\Providers;

use app\RGAS;
use Illuminate\Support\ServiceProvider;

class RGASHelpersServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['rgashelpers'] = $this->app->share(function($app)
        {
            return new RGAS\RGASHelpers;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('RGASHelpers', 'app\RGAS\Facades\RGASHelpers');
        });
    }
}