<?php namespace RGAS\Providers;

use RGAS\Libraries;
use Illuminate\Support\ServiceProvider;

class CIEncryptServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['ciencrypt'] = $this->app->share(function($app)
        {
            return new Libraries\CIEncrypt;
        });

        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('CIEncrypt', 'RGAS\Facades\CIEncrypt');
        });
    }
}