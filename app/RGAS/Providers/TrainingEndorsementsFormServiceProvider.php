<?php namespace app\RGAS\Providers;

use app\TE;
use Illuminate\Support\ServiceProvider;

class TrainingEndorsementsFormServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['trainingendorsementsform'] = $this->app->share(function($app)
        {
            return new TE\TrainingEndorsementsForm;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('TrainingEndorsementsForm', 'app\RGAS\Facades\TrainingEndorsementsForm');
        });
    }
}