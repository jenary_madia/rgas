<?php namespace RGAS\Providers;

use RGAS\Helpers;
use Illuminate\Support\ServiceProvider;

class EmployeesHelperServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['employeeshelper'] = $this->app->share(function($app)
        {
            return new Helpers\EmployeesHelper;
        });

        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('EmployeesHelper', 'RGAS\Facades\EmployeesHelper');
        });
    }
}