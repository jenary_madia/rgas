<?php namespace RGAS\Libraries;

interface ICIEncrypt
{
	public function decode($data);
	public function encode($string, $depth = null);
}
?>