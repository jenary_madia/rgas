<?php
namespace RGAS\Libraries;

interface IAuditTrail
{
	public static function store($user_id,$action_code,$module_id,$table_name,$params,$old,$new);
	
}
?>