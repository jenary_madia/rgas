<?php namespace RGAS\Libraries;

use AuditTrails;

class AuditTrail implements IAuditTrail
{
	public static function store($user_id,$action_code,$module_id,$table_name,$params,$old,$new,$ref_num="")
	{
		$AT = new AuditTrails;
		$AT->user_id = $user_id;
		$AT->date = date('Y-m-d');
		$AT->time = date('H:i:s');
		$AT->action_code = $action_code;
		$AT->module_id = $module_id;
		$AT->table_name = $table_name;
		$AT->params = $params;
		$AT->old = $old;
		$AT->new = $new;
		$AT->ref_num = $ref_num;
		$AT->save();
		return;
	}
	
}
?>