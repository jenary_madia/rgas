<?php namespace RGAS\Libraries;
use Response;
use Input;
use Config;
use Redirect;

class FileManager implements IFileManager
{	
	public function upload($path)
	{
		$attachments = Input::file('attachments');
		if($attachments){
			foreach($attachments as $key => $file){
				if($file){
					$filenames[$key]['random_filename'] = md5(rand().time());
					$filenames[$key]['original_filename'] = $file->getClientOriginalName();
					$filenames[$key]['filesize'] = $file->getSize();
					$filenames[$key]['mime_type'] = $file->getMimeType();
					$filenames[$key]['original_extension'] = $file->getClientOriginalExtension();
					$file->move($path, $filenames[$key]['random_filename']);
				}
			}
			return $filenames;
		}
		return;
	}

	public function download($filepath, $filename)
	{
        if(preg_match('/\//', $filename))  {
            $filename = 'attachment-'.date('Y-m-d');
        }
		if (file_exists($filepath))
		{

			return Response::download($filepath, $filename, [
				'Content-Length: '. filesize($filepath)
			]);
		}
		else
		{
			return 0;
			// exit('Requested file does not exist on our server!');
		}
	}
	
	public function move($random_filename, $new_path)
	{
		if(file_exists(Config::get('rgas.rgas_temp_storage_path').$random_filename)) {
			if(!file_exists($new_path))
			{
				mkdir($new_path, 0777, true);
			}
			rename(Config::get('rgas.rgas_temp_storage_path').$random_filename, $new_path.'/'.$random_filename);
			return;
		}
	}
}
?>