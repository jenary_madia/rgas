<?php
namespace RGAS\Libraries;

interface IFileManager
{
	public function upload($path);
	public function download($filepath, $filename);
}
?>