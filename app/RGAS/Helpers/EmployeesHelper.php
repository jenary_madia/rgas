<?php namespace RGAS\Helpers;

use Employees;

class EmployeesHelper
{
	public function getEmployeeNameById($id)
	{
		$employee = Employees::find($id);
		return $employee['firstname']." ".$employee['lastname'];
	}
}
?>