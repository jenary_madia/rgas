<?php
use RGAS\Libraries;

/*
|--------------------------------------------------------------------------
| Application Routes
|------------------------	--------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function(){

	Route::resource('file-uploader', 'FileUploaderController');
	
	
	// /* PMARF */
	Route::group(array('prefix' => 'pmarf'), function()
	{
		Route::get('npmd_execution_pmarf', 'PmarfController@npmd_execution_pmarf');
		/*Newly Added by Lucille*/
		
		Route::get('create', 'PmarfController@create');
		Route::get('view/my-requests/{pmarf_id}', 'PmarfController@view_my_request');
		Route::get('submitted-pmarf-requests', 'PmarfController@submittedPmarfRequests');
		Route::get('pmarf-requests', 'PmarfController@submittedReport');

		Route::get('approve/{pmarf_id}', 'PmarfController@immSupApprove');
		Route::post('approve/{pmarf_id}', 'PmarfController@immSupApprove');

		Route::get('dept-head-approve/{pmarf_id}', 'PmarfController@dhApprove');
		Route::post('dept-head-approve/{pmarf_id}', 'PmarfController@dhApprove');
		
		Route::get('npmd-head-approve/{pmarf_id}', 'PmarfController@npmdHeadAssigning');
		Route::post('npmd-head-approve/{pmarf_id}', 'PmarfController@npmdHeadAssigning');
		
		Route::get('npmd-personnel-process/{pmarf_id}', 'PmarfController@npmdPersonnelProcess');
		Route::post('npmd-personnel-process/{pmarf_id}', 'PmarfController@npmdPersonnelProcess');
		
		Route::get('npmd-head-acceptance/{pmarf_id}', 'PmarfController@npmdHeadAcceptance');
		Route::post('npmd-head-acceptance/{pmarf_id}', 'PmarfController@npmdHeadAcceptance');
		
		Route::get('npmd-personnel-execution/{pmarf_id}', 'PmarfController@npmdPersonnelExecution');
		Route::post('npmd-personnel-execution/{pmarf_id}', 'PmarfController@npmdPersonnelExecution');
		
		Route::get('npmd-head-notation/{pmarf_id}', 'PmarfController@npmdHeadNotation');
		Route::post('npmd-head-notation/{pmarf_id}', 'PmarfController@npmdHeadNotation');
		
		Route::get('confirmation/{pmarf_id}', 'PmarfController@requestorConfirmation');
		Route::post('confirmation/{pmarf_id}', 'PmarfController@requestorConfirmation');
		
		Route::get('pmarf-filer-requests', 'PmarfController@PmarfFilerDashboard');
		Route::get('pmarf-approver-requests', 'PmarfController@PmarfApproverDashboard');
		// Route::get('approver_dashboard', 'PmarfController@PmarfApproverDashboard');
		
		
		/*AJAX*/
		Route::get('my-pmarf-requests', 'PmarfController@MyPmarf');
		Route::post('my-pmarf-requests', 'PmarfController@MyPmarf');
		
		Route::get('for-approval-requests', 'PmarfController@ImmSupPmarf');
		Route::post('for-approval-requests', 'PmarfController@ImmSupPmarf');
		
		Route::get('approver-closed-requests', 'PmarfController@ApproverClosedReq');
		Route::post('approver-closed-requests', 'PmarfController@ApproverClosedReq');
		
		Route::get('approver-closed-requests-dh', 'PmarfController@ApproverClosedReqDh');
		Route::post('approver-closed-requests-dh', 'PmarfController@ApproverClosedReqDh');
		
		Route::get('for-head-approval-requests', 'PmarfController@HeadofReq');
		Route::post('for-head-approval-requests', 'PmarfController@HeadofReq');
		
		Route::get('npmd-head-requests', 'PmarfController@NmpdHeadReq');
		Route::post('npmd-head-requests', 'PmarfController@NmpdHeadReq');
		Route::get('npmd-head-requests-limit', 'PmarfController@NmpdHeadReq_limit');
		Route::post('npmd-head-requests-limit', 'PmarfController@NmpdHeadReq_limit');
		
		Route::get('all-pmarf-requests', 'PmarfController@AllPmarfReq');
		Route::post('all-pmarf-requests', 'PmarfController@AllPmarfReq');
		
		Route::get('npmd-personnel-requests', 'PmarfController@NmpdPersonnelReq');
		Route::post('npmd-personnel-requests', 'PmarfController@NmpdPersonnelReq');
		Route::get('npmd-personnel-requests-limit', 'PmarfController@NmpdPersonnelReq_limit');
		Route::post('npmd-personnel-requests-limit', 'PmarfController@NmpdPersonnelReq_limit');
		/*End of AJAX*/
		
		/*End of Newly Added*/
		
		
		//Route::get('create', 'PmarfController@create_pmarf');
		Route::post('create', 'PmarfController@action');
		
		/*Called via ajax*/
		Route::get('save_pmarf', 'PmarfController@get_my_save_pmarf');
		Route::post('save_pmarf', 'PmarfController@get_my_save_pmarf');
		/*End of Called via ajax*/
	
		Route::get('edit/{pmarf_id}', 'PmarfController@edit_pmarf');
		Route::post('edit/{pmarf_id}', 'PmarfController@edit_pmarf');
		
		Route::get('delete/{pmarf_id}', 'PmarfController@delete_pmarf');
		Route::post('delete/{pmarf_id}', 'PmarfController@delete_pmarf');
		
		// Route::get('send/{pmarf_id}', 'PmarfController@send_pmarf');
		// Route::post('send/{pmarf_id}', 'PmarfController@send_pmarf');
		
		Route::get('view/{pmarf_id}', 'PmarfController@view');
		Route::post('view/{pmarf_id}', 'PmarfController@view');
		
		Route::get('download/{pmarf_ref_num}/{random_filename}/{original_filename}', 'PmarfController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		Route::get('download_npmd/{pmarf_ref_num}/{random_filename}/{original_filename}', 'PmarfController@download_npmd')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		
		Route::get('print/{pmarf_id}', 'PmarfController@printPmarfRequest');
		
		Route::get('departments/for_npmd', function(){
			$departments = Departments::get_departments()->sortBy('dept_name')->groupBy('dept_name')->toJson();
			
			echo $departments;
		
		});
		
		Route::get('departments/for_npmd_per', function(){
			// 1
			$departments = Departments::get_departments()->sortBy('dept_name')->groupBy('dept_name')->toJson();
			
			echo $departments;
		
		});
		
		
	});
	
	
	
	/* Service Vehicle Service */
	Route::group(array('prefix' => 'svr'), function()
	{
		Route::get('create', 'ServiceController@create');
		Route::post('create/action', 'ServiceController@create_action');
		Route::get('edit/{id}', 'ServiceController@edit');
		Route::post('edit/action/{id}', 'ServiceController@edit_action');

		Route::get('view/{id}', 'ServiceController@view');
		Route::post('view/action/{id}', 'ServiceController@view_action');

		Route::get('approval/{id}', 'ServiceController@approval');
		Route::post('approval/action/{id}', 'ServiceController@approval_action');

		Route::get('accept/{id}', 'ServiceController@accept');
		Route::post('accept/action/{id}', 'ServiceController@accept_action');

		Route::get('process/{id}', 'ServiceController@process');
		Route::get('review/view/{id}/{status}', 'ServiceController@review_view');

		Route::get('review/delete/{id}', 'ServiceController@delete_review');
		Route::get('delete/{id}', 'ServiceController@deleterecord');

		Route::get('report', 'ServiceController@filer_report');

		Route::get('reviewer', 'ServiceController@reviewer_report');

		Route::post('delete/rows', 'ServiceController@delete_review_row');

		Route::get('get_review', 'ServiceController@review_get');
	});
	
	
	/* Art Design */
	Route::group(array('prefix' => 'art'), function()
	{
		Route::get('create', 'ARTController@create');
		Route::post('create/action', 'ARTController@create_action');
		Route::get('edit/{id}', 'ARTController@edit');
		Route::post('edit/action/{id}', 'ARTController@edit_action');
		Route::get('view/{id}', 'ARTController@view');
		Route::post('view/action/{id}', 'ARTController@view_action');
		Route::get('delete/{id}', 'ARTController@delete_record');
		Route::get('approval/{id}', 'ARTController@approval');
		Route::post('approval/action/{id}', 'ARTController@approval_action');
		
		Route::get('report', 'ARTController@filer_report');

		Route::get('reviewer/report', 'ARTController@art_department');
		Route::get('assign/{id}', 'ARTController@assign');
		Route::post('assign/action/{id}', 'ARTController@assign_action');
		Route::get('artist/{id}', 'ARTController@artist');
		Route::post('artist/action/{id}', 'ARTController@artist_action');

		Route::get('get_review', 'ARTController@review_get');
		Route::get('download/{ref_num}/{random_filename}/{original_filename}', 'ARTController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
	
	});
	//
	
	/* Office Sales */
	Route::group(array('prefix' => 'os'), function()
	{
		Route::get('food/create', 'OfficeController@create_food');
		Route::post('food/create/action', 'OfficeController@create_food_action');
		Route::get('food/edit/{id}', 'OfficeController@edit_food');
		Route::post('food/edit/action/{id}', 'OfficeController@edit_food_action');

		Route::get('non-food/create', 'OfficeController@create_non_food');
		Route::post('non-food/create/action', 'OfficeController@create_non_food_action');
		Route::get('non-food/edit/{id}', 'OfficeController@edit_non_food');
		Route::post('non-food/edit/action/{id}', 'OfficeController@edit_non_food_action');

		Route::get('create', 'OfficeController@create');
		Route::post('create/action', 'OfficeController@create_action');
		Route::get('edit/{id}', 'OfficeController@edit');
		Route::get('view/{id}', 'OfficeController@view');
		Route::post('edit/action/{id}', 'OfficeController@edit_action');
		Route::post('view/action/{id}', 'OfficeController@view_action');

		Route::get('process/{id}', 'OfficeController@process');
		Route::post('process/action/{id}', 'OfficeController@process_action');
		
		Route::get('food/report', 'OfficeController@food_report');
		Route::get('non-food/report', 'OfficeController@non_food_report');

		Route::get('receiver/report', 'OfficeController@receiver_report');
		Route::get('report', 'OfficeController@report');
		Route::get('delete/{id}', 'OfficeController@delete_record');

		Route::get('affiliate/view/{id}', 'OfficeController@view_affiliate');		
		Route::get('form/view/{id}', 'OfficeController@view_employees');

		Route::get('export/employees/food', 'OfficeController@export_employees_food');
		Route::post('export/employees/food/action', 'OfficeController@export_employees_food_action');
		Route::get('export/affiliate', 'OfficeController@export_affiliate');
		Route::post('export/affiliate/action', 'OfficeController@export_affiliate_action');

		Route::get('export/employees/non_food', 'OfficeController@export_employees_non_food');
		Route::post('export/employees/non_food/action', 'OfficeController@export_employees_non_food_action');

		Route::post('process/print', 'OfficeController@process_print');
		Route::post('get_cut_off_amount', 'OfficeController@get_cutoff_non');
		Route::post('get_cut_off_amount/{amount}', 'OfficeController@get_cutoff');
		Route::post('get_outstanding', 'OfficeController@get_outstanding');
		
	});
	
	/* Clearance Certification */
	Route::group(array('prefix' => 'cc'), function()
	{
		Route::get('download/{ref_num}/{random_filename}/{original_filename}', 'CCController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		
		Route::get('delete/{id}', 'CCController@delete_record');
		Route::get('print/{id}', 'CCController@chrd_print');
		Route::get('form/{id}', 'CCController@chrd_form');
		Route::get('report', 'CCController@chrd_list');
		Route::get('report/csdh', 'CCController@csdh_list');
		Route::get('report/resignee', 'CCController@resignee_list');
		Route::get('report/assesment', 'CCController@assesment_list');

		Route::post('logs/get/{id}', 'CCController@chrd_logs_get');
		Route::get('report/csdh/get', 'CCController@csdh_list_get');
		Route::get('report/resignee/get', 'CCController@resignee_list_get');
		Route::get('report/assesment/get', 'CCController@assesment_list_get');
		Route::get('report/filer/get', 'CCController@filer_list_get');

		Route::get('create', 'CCController@create');
		Route::post('create_action', 'CCController@create_action');
		Route::post('getemployee', 'CCController@getEmployeerecord');
		Route::get('acknowledge/{id}', 'CCController@acknowledge');
		Route::post('acknowledge_action/{id}', 'CCController@acknowledge_action');
		Route::get('edit/{id}', 'CCController@edit');
		Route::post('edit_action/{id}', 'CCController@edit_action');

		Route::get('csdh/{id}', 'CCController@csdh');
		Route::post('csdh/action/{id}', 'CCController@csdh_action');

		Route::get('chrd/{id}', 'CCController@chrd');
		Route::post('chrd/action/{id}', 'CCController@chrd_action');
		Route::get('chrd/{id}', 'CCController@chrd');
		Route::get('department/head/{id}', 'CCController@department_head');
		Route::post('department/head/action/{id}', 'CCController@department_head_action');
		Route::get('department/personnel/{id}', 'CCController@department_personnel');
		Route::post('department/personnel/action/{id}', 'CCController@department_personnel_action');
		Route::get('department/review/{id}', 'CCController@department_review');
		Route::post('department/review/action/{id}', 'CCController@department_review_action');

		Route::get('resignee/head/{id}', 'CCController@resignee_head');
		Route::post('resignee/head/action/{id}', 'CCController@resignee_head_action');
		Route::get('resignee/supervisor/{id}', 'CCController@resignee_supervisor');
		Route::post('resignee/supervisor/action/{id}', 'CCController@resignee_supervisor_action');
		Route::get('resignee/review/{id}', 'CCController@resignee_review');
		Route::post('resignee/review/action/{id}', 'CCController@resignee_review_action');

		Route::get('citd/technical/{id}', 'CCController@citd_technical');
		Route::post('citd/technical/action/{id}', 'CCController@citd_technical_action');
		Route::get('citd/staff/{id}', 'CCController@citd_staff');
		Route::post('citd/staff/action/{id}', 'CCController@citd_staff_action');
		Route::get('citd/technical/review/{id}', 'CCController@citd_technical_review');
		Route::post('citd/technical/review/action/{id}', 'CCController@citd_technical_review_action');
		Route::get('citd/head/review/{id}', 'CCController@citd_head_review');
		Route::post('citd/head/review/action/{id}', 'CCController@citd_head_review_action');

		Route::get('smdd/staff/{id}', 'CCController@smdd_staff');
		Route::post('smdd/staff/action/{id}', 'CCController@smdd_staff_action');
		Route::get('smdd/head/review/{id}', 'CCController@smdd_head_review');
		Route::post('smdd/head/review/action/{id}', 'CCController@smdd_head_review_action');

		Route::get('erpd/staff/{id}', 'CCController@erpd_staff');
		Route::post('erpd/staff/action/{id}', 'CCController@erpd_staff_action');
		Route::get('erpd/head/review/{id}', 'CCController@erpd_head_review');
		Route::post('erpd/head/review/action/{id}', 'CCController@erpd_head_review_action');

		Route::get('ssrv/staff/{id}', 'CCController@ssrv_staff');
		Route::post('ssrv/staff/action/{id}', 'CCController@ssrv_staff_action');
		Route::get('ssrv/head/review/{id}', 'CCController@ssrv_head_review');
		Route::post('ssrv/head/review/action/{id}', 'CCController@ssrv_head_review_action');

		Route::get('chrd/training1/{id}', 'CCController@chrd_training1');
		Route::post('chrd/training1/action/{id}', 'CCController@chrd_training1_action');
		Route::get('chrd/cbr/personnel1/{id}', 'CCController@chrd_cbr_personnel1');
		Route::post('chrd/cbr/personnel1/action/{id}', 'CCController@chrd_cbr_personnel1_action');
		Route::get('chrd/recruitment/{id}', 'CCController@chrd_recruitment');
		Route::post('chrd/recruitment/action/{id}', 'CCController@chrd_recruitment_action');
		Route::get('chrd/training2/{id}', 'CCController@chrd_training2');
		Route::post('chrd/training2/action/{id}', 'CCController@chrd_training2_action');
		Route::get('chrd/cbr/personnel2/{id}', 'CCController@chrd_cbr_personnel2');
		Route::post('chrd/cbr/personnel2/action/{id}', 'CCController@chrd_cbr_personnel2_action');
		Route::get('chrd/cbr/manager/{id}', 'CCController@chrd_cbr_manager');
		Route::post('chrd/cbr/manager/action/{id}', 'CCController@chrd_cbr_manager_action');
	});
	//-
	
	
	/* General Documents */
	Route::group(array('prefix' => 'gd'), function()
	{
		Route::get('reviewer/report', 'GDController@reviewer_report');
		Route::get('review/form/{id}', 'GDController@review_form');
		Route::post('review/form/action/{id}', 'GDController@review_action');

		Route::get('report', 'GDController@filer_report');
		Route::get('create', 'GDController@create');
		Route::post('create/action', 'GDController@create_action');

		Route::get('edit/{id}', 'GDController@edit');
		Route::post('edit/action/{id}', 'GDController@edit_action');

		Route::get('view/{id}', 'GDController@view');

		Route::get('get_review', 'GDController@review_get');

		Route::post('check_ref/{id}', 'GDController@check_ref');
		Route::post('view/action/{id}', 'GDController@view_action');
		Route::get('delete/{id}', 'GDController@delete_record');
		Route::get('download/{ref_num}/{random_filename}/{original_filename}', 'GDController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
	});
	//
	
	/* Online Attendance Monitoring */
	Route::group(array('prefix' => 'oam'), function()
	{
		Route::get('list', 'OAMController@report');
		Route::get('', 'OAMController@index');
		Route::get('get/list', 'OAMController@oamlist');
		Route::get('get/self', 'OAMController@oamself');
	});
	//--
		
	/* CBR 12192016 -- updated by Lucille */
	Route::group(array('prefix' => 'cbr'), function()
	{
		Route::get('create', 'CbrController@create');
		Route::post('create', 'CbrController@create');
		Route::get('submitted-cbr-requests', 'CbrController@SubmittedCBRRequests');
		// Called via Ajax
		Route::get('requests-dashboard-filer', 'CbrController@RequestsDashboardFiler');
		Route::get('requests-dashboard-staff', 'CbrController@RequestsDashboardStaff');
		
		Route::get('requests-new', 'CbrController@RequestsNew');
		Route::post('requests-new', 'CbrController@RequestsNew');
		
		Route::get('requests-my', 'CbrController@RequestsMy');
		Route::post('requests-my', 'CbrController@RequestsMy');
		
		Route::get('requests-in-process', 'CbrController@RequestsInProcess');
		Route::post('requests-in-process', 'CbrController@RequestsInProcess');
		
		Route::get('requests-acknowledged', 'CbrController@RequestsAcknowledged');
		Route::post('requests-acknowledged', 'CbrController@RequestsAcknowledged');
		//--
		// Request has been saved as draft
		Route::get('draft/view/{cb_ref_num}', 'CbrController@draftView');
		Route::get('draft/edit/{cb_ref_num}', 'CbrController@draftEdit');
		Route::post('draft/edit/{cb_ref_num}', 'CbrController@draftEdit');
		//--
		
		// Route::get('requests-new', 'CbrController@getRequestsNew');
		// Route::post('requests-new', 'CbrController@postRequestsNew');
		Route::get('view/{cb_id}', 'CbrController@view');
		
		Route::get('review-request/{cb_ref_num}/{view?}', 'CbrController@reviewRequest');
		Route::post('review-request/{cb_ref_num}', 'CbrController@reviewRequest');	
		Route::get('process/{cb_ref_num}', 'CbrController@process');
		Route::post('process/{cb_ref_num}', 'CbrController@forAcknowledgement');
		// Route::post('for-acknowledgement/{cbrd_ref_num}', 'CbrController@forAcknowledgement');
		
		Route::get('returned/view/{cb_id}', 'CbrController@returnedView');
		Route::get('returned/edit/{cb_id}', 'CbrController@returnedEdit');
		Route::post('returned/edit/{cb_id}', 'CbrController@returnedEdit');
		Route::get('received-transaction', 'CbrController@received_cbr_transaction');
		Route::post('acknowledged', 'CbrController@acknowledged');
		Route::get('delete/{status}/{cbrd_ref_num}', 'CbrController@delete');
		
		Route::get('download/{cb_ref_num}/{random_filename}/{original_filename}', 'CbrController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
	});
	/* End of CBR*/
	
	
	/* TE 12192016 - updated by Lucille */
	Route::group(array('prefix' => 'te'), function()
	{
		Route::get('create', 'TrainingEndorsementController@create');
		Route::post('create', 'TrainingEndorsementController@action');
		Route::get('submitted-te-requests', 'TrainingEndorsementController@SubmittedTERequests');
		//Called via AJAX
		Route::get('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		Route::post('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		
		Route::get('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		Route::post('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		
		Route::get('endorsements_chrd_vp_approval', 'TrainingEndorsementController@EndorsementsChrdVpApproval');
		Route::post('endorsements_chrd_vp_approval', 'TrainingEndorsementController@EndorsementsChrdVpApproval');
		
		Route::get('endorsements_svp_for_cs_approval', 'TrainingEndorsementController@EndorsementsSvpCsApproval');
		Route::post('endorsements_svp_for_cs_approval', 'TrainingEndorsementController@EndorsementsSvpCsApproval');
		
		Route::get('endorsements_pres_approval', 'TrainingEndorsementController@EndorsementsPresApproval');
		Route::post('endorsements_pres_approval', 'TrainingEndorsementController@EndorsementsPresApproval');
		
		Route::get('filer_dashboard', 'TrainingEndorsementController@FilerDashboard');
		Route::get('approver_dashboard', 'TrainingEndorsementController@ApproverDashboard');
		
		//Requestor
		Route::get('view/{te_id}', 'TrainingEndorsementController@view');
		Route::get('view-approved/{te_id}', 'TrainingEndorsementController@view_approved');
		Route::get('edit/{te_id}', 'TrainingEndorsementController@edit');
		Route::post('edit/{te_id}', 'TrainingEndorsementController@edit');
		Route::get('delete/{te_id}', 'TrainingEndorsementController@delete');
		
		//CHRD
		Route::get('assess/{te_id}', 'TrainingEndorsementController@assessChrd');
		Route::post('assess/{te_id}', 'TrainingEndorsementController@assessChrd');
		
		//vphr,svp,pres approval		
		Route::get('view-for-approval/{te_id}', 'TrainingEndorsementController@viewForApproval');
		Route::get('process-for-approval/{te_id}', 'TrainingEndorsementController@processForApproval');
		Route::post('process-for-approval/{te_id}', 'TrainingEndorsementController@processForApproval');
		
		Route::get('download/{te_ref_num}/{random_filename}/{original_filename}', 'TrainingEndorsementController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		Route::get('print/{te_id}', 'TrainingEndorsementController@printFile');
		Route::get('download/{te_id}', 'TrainingEndorsementController@downloadFile');
		
		Route::get('print-request/{te_id}', 'TrainingEndorsementController@printRequest');
		
		//where('te_level','=','CHRD-TOD')
		Route::get('departments/for_chrd', function(){
			$departments = TrainingEndorsements::whereRaw("(te_status = 'APPROVED' or te_level in ('CHRD-TOD','VPforCHR','SVPforCS','PRES') )")
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});

		Route::get('departments/for_vp', function(){
			$departments = TrainingEndorsements::where('te_level','=','VPforCHR')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});

		Route::get('departments/for_svp', function(){
			$departments = TrainingEndorsements::where('te_level','=','SVPforCS')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});

		Route::get('departments/for_pres', function(){
			$departments = TrainingEndorsements::where('te_level','=','PRES')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});
	});
	/* End of TE*/
	
	Route::get('employees/list', 'EmployeesController@employee_lists');
	Route::post('employees/get', 'EmployeesController@get_all_employees');
	// Route::get('notice', 'HomeController@notice_page');
	Route::get('hierarchyupdate', 'HomeController@update_hierarchy');

	
	/* PMF*/
	Route::group(array('before' => 'auth'), function()
	{
		Route::group(array('prefix' => 'pmf'), function()
		{
			Route::get('create', 'PmfController@create');
			Route::post('create/action', 'PmfController@create_action');
			Route::post('create/action/{id}', 'PmfController@create_action');

			Route::get('create/sub/{id}/{purpose}/{eid}', 'PmfController@create_sub_pmf');
			Route::get('create/sub/{id}/{purpose}/{eid}/{content}/{copy}/{sub}', 'PmfController@create_sub_pmf');

			Route::get('create/{id}/{purpose}', 'PmfController@create_pmf');
			Route::get('create/{id}/{purpose}/{content}/{copy}', 'PmfController@create_pmf');

			Route::post('create/action', 'PmfController@create_action');
			Route::get('edit/{id}', 'PmfController@edit');
			Route::post('edit/action/{id}', 'PmfController@edit_action');

			Route::get('approval/{id}', 'PmfController@approval');
			Route::post('approval/action/{id}', 'PmfController@approval_action');

			Route::get('check/sub/{id}/{purpose}/{eid}', 'PmfController@check_sub_record');
			Route::get('check/sub/{id}/{purpose}/{eid}/{content}/{copy}/{sub}', 'PmfController@check_sub_record');
			Route::get('check/{id}/{purpose}', 'PmfController@check_record');
			Route::get('check/{id}/{purpose}/{content}/{copy}', 'PmfController@check_record');
			
			Route::get('print/{id}', 'PmfController@pmf_print');
			Route::get('delete/{id}', 'PmfController@delete_record');

			Route::post('export/action', 'PmfController@export_report_action');
			Route::get('export', 'PmfController@export_report');

			Route::get('receiver/{id}', 'PmfController@receiver_record');
			Route::post('receiver/action/{id}', 'PmfController@receiver_record_action');

			Route::get('submitted', 'PmfController@submitted_report');
			Route::get('report', 'PmfController@filer_report');
			Route::get('download/{ref_num}/{random_filename}/{original_filename}', 'PmfController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		
			Route::get('get_review', 'PmfController@review_get');
		});
	});
	
	/*Added by Jenary*/
	
	/* Leave Request Form */
	Route::group(array('prefix' => 'lrf'), function()
	{
		Route::get('/', 'LRFController@index');
		Route::get('create','LRFController@create');
		Route::post('action/{id?}','LRFController@action');
		Route::post('processing/{id?}','LRFController@leaveAction');

		// For datatables
		Route::get('my_leaves','LRFController@getMyLeaves');
		Route::get('clinic_approval_leaves','LRFController@getSubmittedToClinic');
		Route::get('for_approval_leaves','LRFController@getForApproveLeaves');
		Route::get('my_pe_leaves','LRFController@PEMyRequests');
		Route::get('superior_pe_leaves','LRFController@PESuperiorRequests');

		// Receiver List
		Route::get('submitted_leaves','LRFController@submittedToReceiver');
		Route::post('search','LRFController@searchAction');


		// FILER , IS , DH List
		Route::get('leaves','LRFController@leaves');

		// IS List action
		Route::get('leave/{id}/{method?}','LRFController@viewLeave');

		// FILER List action
		Route::get('my_leaves/{id}/edit','LRFController@editMyLeave');
		Route::get('my_leaves/{id}/view','LRFController@viewMyLeave');

		// For Production Encoders
		Route::get('pe_create','LRFController@PECreateLeave');
		Route::post('pe_action/{id?}','LRFController@PEAction');

		// Show approved Leaves

		Route::get('show_leaves/{id}/{type?}','LRFController@showApprovedLeaves');

        //

        Route::post('validate_leave','LRFController@checkIfLeaveValid');
        
		// Production encoders

		Route::post('get_employees','LRFController@postParseEmployees');
		Route::get('pe_view/{id}/{method?}','LRFController@peView');
		Route::get('pe_view_my_request/{id}/{method?}','LRFController@peViewMyRequest');
		Route::post('submit/{id}','LRFController@peISAction');
		Route::post('note_batchdetails','LRFController@noteBatchDetails');

		// Clinic

		Route::get('{id}/clinic/{method?}','LRFController@clinic');

		//download attachments
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','LRFController@downloadAttachments');

		// For datatables at home page
		Route::get('home_my_leaves','LRFController@getHomeMyLeaves');
		Route::get('home_clinic_approval_leaves','LRFController@getHomeSubmittedToClinic');
		Route::get('home_for_approval_leaves/{usage?}','LRFController@getHomeForApproveLeaves');
		Route::get('home_superior_pe_leaves','LRFController@getHomePESuperiorRequests');
		Route::get('home_my_pe_leaves','LRFController@getHomeMyPELeaves');

	});

	Route::group(array('prefix' => 'ns'), function() {
        Route::get('/','NSController@index');
        Route::get('/create','NSController@create');
        Route::post('/action/{id?}','NSController@action');
        Route::post('/hierarchy_action/{id}','NSController@hierarchyAction');
        Route::get('/notifications','NSController@lists');

//        ajax
        Route::get('/my_notifications','NSController@getMyNotifications');
        Route::get('/superior_notifications/{usage?}','NSController@getSuperiorNotifications');
        Route::get('/my_pe_notifications','NSController@getMyPENotifications');
        Route::get('/superior_pe_notifications','NSController@getSuperiorPENotifications');
        Route::post('/sections','NSController@postSections');
        Route::post('/employees','NSController@postEmployees');
        Route::post('/parse_employees','NSController@postParseEmployees');
        Route::post('/offset_details','NSController@forEditOffsetDetails');

		// IS List action
		Route::get('/for_approval/{id}/{method?}','NSController@viewNotif');


//      FILER action
        Route::get('/my_notifications/{id}/{method}','NSController@myNotifications');
        Route::post('/my_notif_actions/{id}','NSController@myNotifAction');

//		Production Encoder
        Route::get('/pe_create','NSController@productionEncoders');
        Route::post('/pe_action/{id?}','NSController@peAction');
        Route::get('/pe_view/{id}/{method?}','NSController@peView');
        Route::post('/submit/{id}','NSController@peISAction');
        Route::get('pe_view_my_request/{id}/{method?}','NSController@peViewMyRequest');
        Route::post('note_batchdetails','NSController@noteBatchDetails');

        // Production Encoder list table action
        Route::post('/pe-process','NSController@peProcessMyNS');

		//download attachments
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','NSController@downloadAttachments');

		// list table action
        Route::post('/process','NSController@processMyNS');

		//		OCheck If exist on overtime

        Route::post('/check-date-validity','NSController@checkDateValidity');

        // For datatables at home page
        Route::get('home_my_notif','NSController@getHomeMynotif');
        Route::get('home_my_pe_notif','NSController@getHomeMyPEnotif');
        Route::get('home_superior_notif','NSController@getHomeForApproveNotif');
        Route::get('home_superior_pe_notif','NSController@getHomePEForApproveNotif');

	});

	Route::group(array('prefix' => 'submitted'),function() {
		Route::get('leaves_notifications','SubmittedLeavesAndNotifications@index');
		Route::get('leaves','SubmittedLeavesAndNotifications@getLeaves');
        Route::get('notif_undertime','SubmittedLeavesAndNotifications@getNotifUndertime');
        Route::get('notif_offset','SubmittedLeavesAndNotifications@getNotifOffset');
        Route::get('notif_timekeep','SubmittedLeavesAndNotifications@getNotifTimekeep');
        Route::get('notif_official','SubmittedLeavesAndNotifications@getNotifOfficial');
        Route::get('notif_cuttime','SubmittedLeavesAndNotifications@getNotifCuttime');
        Route::post('search','SubmittedLeavesAndNotifications@search');

		Route::post('print','SubmittedLeavesAndNotifications@printExcel');
		
        //        viewing each application
		Route::get('leave/{id}/{method?}','SubmittedLeavesAndNotifications@leaveView');
		Route::post('leave/{id}','SubmittedLeavesAndNotifications@leaveAction');
		Route::get('notification/{id}/{method?}','SubmittedLeavesAndNotifications@notifView');
		Route::post('notification/{id}','SubmittedLeavesAndNotifications@notifAction');

		/*Production Encoders*/
		Route::get('pe_leaves_notifications','SubmittedLeavesAndNotifications@PEIndex');
		Route::get('pe_leaves','SubmittedLeavesAndNotifications@PEGetLeaves');
		Route::get('pe_notifications','SubmittedLeavesAndNotifications@PEGetNotifications');
		Route::post('pe_search','SubmittedLeavesAndNotifications@PESearch');

		Route::post('pe_print','SubmittedLeavesAndNotifications@PEPrintExcel');

        //        viewing each application
        Route::get('pe_leave/{id}/{method?}','SubmittedLeavesAndNotifications@PELeaveView');
        Route::post('pe_leave/{id}','SubmittedLeavesAndNotifications@PELeaveAction');
        Route::get('pe_notification/{id}/{method?}','SubmittedLeavesAndNotifications@PENotifView');
        Route::post('pe_notification/{id}','SubmittedLeavesAndNotifications@PENotifAction');


	});

	/*end by Jenary*/

	/*added by Jenary 19/12/16*/
    Route::group(array('prefix' => 'msr'), function() {
        Route::get('/','MSRController@index');

//		creation
        Route::get('create','MSRController@create');
        Route::post('create','MSRController@createAction');

//		Viewing own MSR
        Route::get('/my_msr/{id}/{me}','MSRController@myMSR');
        Route::post('edit_action/{id}','MSRController@editAction');

//		Viewing For approval MSR

        Route::get('for_approval/{id}/{method}','MSRController@forApproval');
        Route::post('for_approval_action/{id}','MSRController@forApprovalAction');

//		Process on list table

        Route::post('process','MSRController@myMSRProcess');


//		for Datatables

        Route::get('get_my_msr/{id?}','MSRController@getMyMSR');
        Route::get('get_superior_msr/{id?}','MSRController@getSuperiorMSR');
        Route::get('get_submitted_msr','MSRController@getSubmittedMSR');

//		Submitted MSR
        Route::get('submitted_msr','MSRController@submittedMSR');
        Route::post('get_filtered_submitted_msr','MSRController@getFilteredSubmittedMSR');

//		View Submitted MSR
        Route::get('submitted/{id}/{method}','MSRController@viewSubmittedMSR');
        Route::post('submitted_action/{id}','MSRController@submittedMSRAction');

//		Download
        Route::get('download/{refNo}/{randomFilename}/{originalFilename}','MSRController@downloadAttachments');

//      Export to CSV
        Route::post('export','MSRController@exportToCSV');

    });

    Route::group(array('prefix' => 'msr/smis'), function() {
        //		creation
        Route::post('create','SMISController@createAction');

        //		reprocess
        Route::post('reprocess/{id}','SMISController@reprocessAction');

        //		For Approval
        Route::post('for_approval/{id?}','SMISController@forApprovalAction');

        //		Submitted MSR
        Route::get('submitted_msr','SMISController@submittedMSR');
        Route::post('get_filtered_submitted_msr','SMISController@getFilteredSubmittedMSR');

        //      Export to CSV
        Route::post('export','SMISController@exportToCSV');

        //		View Submitted MSR
        Route::get('submitted/{id}/{method}','SMISController@viewSubmittedMSR');
        Route::post('submitted_action/{id}','SMISController@submittedMSRAction');

        // Vi

        //		for Datatables
        Route::get('get_my_msr/{id?}','SMISController@getMyMSR');
        Route::get('get_superior_msr/{id?}','SMISController@getSuperiorMSR');
        Route::get('get_submitted_msr','SMISController@getSubmittedMSR');
    });

    /*ended by Jenary 19/12/16*/

	/* Notice To Explain */
	Route::group(array('prefix' => 'nte'), function()
	{
		Route::post('cdim', ['uses' => 'NTEController@_calculateDaysInMonth', 'as' => 'cdim']);

		Route::get('crr/list', ['uses' => 'NTEController@_revealCrrList', 'as' => 'crr.list']);
		Route::get('crr/create', ['uses' => 'NTEController@_revealCrr', 'as' => 'crr.create']);
		Route::post('crr/edit', ['uses' => 'NTEController@_revealCrrEdit', 'as' => 'crr.edit']);

		Route::post('crr/get', ['uses' => 'NTEController@_pullCrrBuildUpAll', 'as' => 'crr.get']);

		Route::post('add', ['uses' => 'NTEController@_push', 'as' => 'nte.add']);

		Route::get('nte/list', ['uses' => 'NTEController@_revealNteLists', 'as' => 'nte.list']);
		Route::get('nte/create', ['uses' => 'NTEController@_revealNte', 'as' => 'nte.create']);
		Route::get('nte/create_multi', ['uses' => 'NTEController@_revealNteMultiple', 'as' => 'nte.create_multi']);
		Route::post('nte/created_multi', ['uses' => 'NTEController@_deflectNte', 'as'=> 'nte.created_multi']);

		Route::post('nte/get_ebd', ['uses' => 'NTEController@_pullEmployeesByDepartment' , 'as' => 'nte.get_ebd']);
		Route::post('nte/get_adr', ['uses' => 'NTEController@_pullNteAllDrafted' , 'as' => 'nte.get_all_drafted']);
		Route::post('nte/get_hh', ['uses' => 'NTEController@_pullNteHistyoryEmployeeDepartment' , 'as' => 'nte.get_history_header']);
		Route::post('nte/get_hd', ['uses' => 'NTEController@_pullNteHistyoryByEmployeeIdAndYear' , 'as' => 'nte.get_history_details']);

		Route::post('nte/add_draft', ['uses' => 'NTEController@_pushNteDraft', 'as' => 'nte.add_draft']);
		Route::post('nte/delete', ['uses'=> 'NTEController@_destroyRow','as' => 'nte.destroy']);
		Route::post('nte/del', ['uses'=> 'NTEController@_desRow','as' => 'nte.des']);
		Route::post('nte/tvy', ['uses' => 'NTEController@_pullNteTotalViolatedYear', 'as' => 'nte.get_total_violated_year']);

		Route::post('nte/view', ['uses' => 'NTEController@_pullNteView', 'as' => 'nte.view']);
		Route::post('nte/approval', ['uses' => 'NTEController@_pullNteApprovalByDepartmentIdMonthAndYear', 'as' => 'nte.approval']); //view selected NTE's for approval by group to form
		Route::post('nte/returned', ['uses' => 'NTEController@_pullNteReturnedByNteId', 'as' => 'nte.returned']); //view selected returned NTE to form
		Route::post('nte/employee', ['uses' => 'NTEController@_pullNteEmployeeByNteId', 'as' => 'nte.employee']); //view selected NTE of employee to form
		Route::post('nte/department', ['uses' => 'NTEController@_pullNteDepartmentByNteId', 'as' => 'nte.department']); //view selected NTE of department to form
		Route::post('nte/request', ['uses' => 'NTEController@_pullNteAcknowledgementByNteId', 'as' => 'nte.request']);
		Route::post('nte/history', ['uses' => 'NTEController@_pullNteHistoryByNteId', 'as' => 'nte.history']);
		
		Route::post('nte/get', ['uses' => 'NTEController@_pullNteAll', 'as' => 'nte.get']); //getting all data from NTE table for requestor
		Route::get('nte/get_aa', ['uses' => 'NTEController@_pullNteAllApproval', 'as' => 'nte.get_all_approval']); //getting all NTE's for approval
		Route::get('nte/get_ad', ['uses' => 'NTEController@_pullNteAllDepartment', 'as' => 'nte.get_all_department']); //getting all NTE's by department
		Route::get('nte/get_ae', ['uses' => 'NTEController@_pullNteAllEmployee', 'as' => 'nte.get_all_employee']); //getting all NTE's by employee

		Route::post('nte/nda', ['uses' => 'NTEController@_revealNdaFromNte', 'as' => 'nte.nda']);
		Route::get('nda/create', ['uses' => 'NTEController@_revealNda', 'as' => 'nda.create']);

		Route::post('nda/get_bei', ['uses' => 'NTEController@_pullNdaByEmployeeId', 'as' =>'nda.get_bei']);
		Route::post('nda/get_beirn', ['uses' => 'NTEController@_pullNdaByEmployeeIdReferenceNumber', 'as' => 'nda.get_beirn']);

		Route::get('nda/list', ['uses' => 'NTEController@_revealNdaLists', 'as' => 'nda.list']);

		Route::post('nda/view', ['uses' => 'NTEController@_pullNdaView', 'as' => 'nda.view']);
		Route::post('nda/manager', ['uses' => 'NTEController@_pullNdaApprovalByNteIdManager', 'as' => 'nda.manager']);
		Route::post('nda/head', ['uses' => 'NTEController@_pullNdaApprovalByNteIdHead', 'as' => 'nda.head']);
		Route::post('nda/department', ['uses' => 'NTEController@_pullNdaApprovalByNteIdDepartment', 'as' => 'nda.department']);
		Route::post('nda/employee', ['uses' => 'NTEController@_pullNdaApprovalByNteIdEmployee', 'as' => 'nda.employee']);
		Route::post('nda/request', ['uses' => 'NTEController@_pullNdaRequestById', 'as' => 'nda.request']);
		Route::post('nda/acknowledged', ['uses' => 'NTEController@_pullNdaAcknowledgementById', 'as' => 'nda.acknowledged']);
		Route::post('nda/cbrm', ['uses' => 'NTEController@_pullNdaCBRMById', 'as' => 'nda.cbrm']);

		Route::post('nda/get', ['uses' => 'NTEController@_pullNdaAll', 'as' => 'nda.get']);
		Route::get('nda/get_am', ['uses' => 'NTEController@_pullNdaAllApprovalCHRDAERManager', 'as' => 'nda.get_all_approval_manager']);
		Route::get('nda/get_ah', ['uses' => 'NTEController@_pullNdaAllApprovalCHRDHead', 'as' => 'nda.get_all_approval_head']);
		Route::get('nda/get_ad', ['uses' => 'NTEController@_pullNdaAllDepartment', 'as' => 'nda.get_all_department']);
		Route::get('nda/get_ae', ['uses' => 'NTEController@_pullNdaAllEmployee', 'as' => 'nda.get_all_employee']);
		Route::get('nda/get_ac', ['uses' => 'NTEController@_pullNdaAllCBRM', 'as' => 'nda.get_all_cbrm']);

		Route::post('nte/print', ['uses' => 'NTEController@_revealNteReport', 'as' => 'nte.print']);
		Route::post('nda/print', ['uses' => 'NTEController@_revealNdaReport', 'as' => 'nda.print']);

		//For Cron
		Route::get('nte/mail/notification/cron', ['uses' => 'NTEController@_pushMailNteNotation', 'as' => 'nte.mail.notation']);
		Route::get('nda/mail/departmentheadnotation/cron', ['uses' => 'NTEController@_pushMailNdaDepartmentNotation', 'as' => 'nda.mail.department.notation']);
		Route::get('nda/mail/chrdmanagernotation/cron', ['uses' => 'NTEController@_pushMailNdaCHRDManagerNotation', 'as' => 'nte.mail.chrd.manager.notation']);
		Route::get('nda/mail/chrdheadnotation/cron', ['uses' => 'NTEController@_pushMailNdaCHRDHeadNotation', 'as' => 'nte.mail.chrd.head.notation']);
	});
	
	Route::get('sign-out', 'LoginController@sign_out');
	Route::post('sign-out2', 'LoginController@sign_out2');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|------------------------	--------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('employees_by_department/{dep_id}', function($dep_id){
	$employees = Employees::get_all($dep_id)->toJson();
	echo $employees;
});


Route::group(array('prefix' => 'moc'), function() {
        //list
        Route::get('/','MOCController@index');

        //Creation
        Route::get('create','MOCController@create');

		//Viewing/Editing Own MOC
        Route::get('{method}/my-moc/{id}','MOCController@myMOC');

		//SendingMOC		
		Route::post('send_moc','ajaxMOCController@ajaxSendMOC');

        //SavingMOC
        Route::post('save_moc','ajaxMOCController@ajaxSaveMOC');

		//ResendingMOC
		Route::post('resend_moc','ajaxMOCController@ajaxResendMOC');

		//resavingMOC
		Route::post('resave_moc','ajaxMOCController@ajaxResaveMOC');

		//cancelMOC
		Route::post('cancel_moc','ajaxMOCController@ajaxCancelMOC');

		//Download
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','MOCController@downloadAttachments');
		/*****For endorsement*****/
        //Viewing/Editing For Approval MOC
        Route::get('{method}/for-endorsement/{id}','MOCController@forEndorsement');

        //Viewing/Editing For Approval MOC
        Route::get('{method}/for-approval-moc/{id}','MOCController@forApprove');

		//Ajax feeding data
        /***for department**/
        Route::get('ajax_get_departments','ajaxMOCController@ajaxGetDepartment');
        /***for own MOC**/
        Route::post('ajax_feed_edit','ajaxMOCController@ajaxFeedEditMOC');
        /***for superior MOC**/
        Route::post('ajax_feed_superior','ajaxMOCController@ajaxFeedEditForApproveMOC');

        //Datatables
		Route::get('get_my_moc/{limit?}','MOCController@getMyMOC');
		Route::get('get_endorsement_moc/{limit?}','MOCController@getForEndorsementMoc');
		Route::get('get_assessment_moc','MOCController@getForAssessmentMoc');
		Route::get('get_approval_moc/{limit?}','MOCController@getForApprovalMoc');

		/******RETURN TO FILER******/
		Route::post('return_to_filer','ajaxMOCController@returnToFiler');

		/******SEND TO FILER FOR ACKNOWLEDGEMENT******/
		Route::post('send_to_filer','ajaxMOCController@sendToFiler');

		/******ACKNOWLEDGE MOC******/
		Route::post('acknowledge_moc','ajaxMOCController@acknowledgeMOC');


		/*******GENERAL ROUTING*******/
		//dept head to SSMD/CSMD
		Route::post('send_to_md','ajaxMOCController@ajaxDHtoMD');

		//CSMD head to CSMD Assistant head
		Route::post('head_to_adh','ajaxMOCController@ajaxDHtoADH');

		//CSMD Assistant head to CSMD head
		Route::post('adh_to_head','ajaxMOCController@ajaxADHtoDH');

		//Assign assessment to BSA
		Route::post('adh_to_bsa','ajaxMOCController@ajaxADHtoBSA');

        //Return assessment to BSA
        Route::post('return_to_bsa','ajaxMOCController@ajaxReturntoBSA');

        //Send BSA to ADH
        Route::post('send_bsa_to_adh','ajaxMOCController@ajaxSendBSAtoADH');

		//Send to Div Head
		Route::post('send_to_div_head','ajaxMOCController@ajaxSendtoDivHead');

        /************* CORPORATE ROUTING  **********/

		/************* SATELLITE ROUTING  **********/
		//ssmd to AOM
		Route::post('send_to_aom','ajaxMOCController@ajaxToAOM');

		//AOM to CSMD
		Route::post('send_aom_to_csmd','ajaxMOCController@ajaxAOMtoDH');

        //div head to ssmd
        Route::post('send_divhead_to_ssmd','ajaxMOCController@ajaxDivHeadtoSSMD');


        /************* SUBMITTED MOC  **********/
        Route::get('submitted-moc','MOCController@submittedMOC');

        /************ GET DEPARTMENTS ON COMPANY ***************/
        Route::post('get-departments','ajaxMOCController@ajaxGetDepartments');

        /************ GET DEPARTMENTS ON COMPANY ***************/
        Route::post('search-moc','ajaxMOCController@ajaxSearchMOC');

		/********* Export to CSV ************/
        Route::post('export','MOCController@exportToCSV');

        /********* Export to pdf ************/
        Route::get('print/{id}','MOCController@printToPDF');


    });

Route::resource('/', 'HomeController');
/* Login */
Route::group(array('before' => 'guest'), function() {
	Route::get('login', 'LoginController@index');
	Route::post('sign-in', 'LoginController@sign_in');
	Route::post('sign-in2', 'LoginController@sign_in2');	
});
	

/* Audit trail report */
	Route::group(array('prefix' => 'audit'), function()
	{
		Route::get('', 'AUDITController@index');
		Route::post('report', 'AUDITController@getRecordlist');    
        Route::get('export', 'AUDITController@exportAudit');    
	});
	//--	
	

//for downloading
Route::get('download/{te_ref_num}/{random_filename}/{original_filename}', ['uses' => 'NTEController@download', 'as' => 'nte.download'])->where('random_filename', '[A-Za-z0-9\-\_\.]+');
Route::get('mrf/download/{te_ref_num}/{random_filename}/{original_filename}', ['uses' => 'MRFController@download', 'as' => 'mrf.download'])->where('random_filename', '[A-Za-z0-9\-\_\.]+');








