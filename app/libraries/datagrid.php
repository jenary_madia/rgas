<?php

/*
 * This class is re-written from the class made by Franco Hora for RGAS (cakephp)
 * 
 */

class DataGrid
{
    public static function show($columns=array(), $result=array(), $table_attrib=array(), $tr_attrib=array(), $td_attrib=array())
    {
        // Initialize table
        $datagrid = '<table @tbl_attrib@>
                        @tr@@td@@/td/@@/tr/@
                     </table>';
        $trs = null;
        $headers = null;

        // creating headers so columns should not be left empty unless you want to print rows only
        if (!empty($columns))
        {
            if (!is_array($columns) && !is_array($td_attrib))
            {
                return false;
            }
            
            $headers .= '<tr>';
            $ctr=0;
            foreach ($columns as $column)
            {
                $tdopen='<td @att@ align="center"><strong>';
                //trying to make the header adjustable for the width
                if (!empty($td_attrib))
                {
                    foreach ($td_attrib as $att=>$value)
                    {
                        if ($att == 'width')
                        {
                            // check if width contain array
                            // false : change td and add width
                            if (is_array($value))
                            {
                                for ($i=0;$i<count($value);$i++)
                                {
                                    if ($ctr == $i)
                                    {
                                        $width_attrib = $att .'="'. $value[$i] .'"';
                                    }
                                }
                            }
                            else
                            {
                                $width_attrib = $att .'="'. $value .'"';
                            }
                        }
                    }
                    if (!empty($width_attrib))
                    {
                        $tdOpen = str_replace('@att@',$width_attrib,$tdopen);
                    }
                    else
                    {
                        $tdOpen = str_replace('@att@','',$tdopen);
                    }
                }
                else
                {
                    $tdOpen = str_replace('@att@','',$tdopen);
                }

                $trs .= $tdOpen.$column.'</strong></td>';
                $ctr++;
            }
            $headers .= '</tr>';
        }

        // creating the row displays
        if (!empty($result))
        {
            if (!is_array($result) && (!is_array($tr_attrib) && !is_array($td_attrib)))
            {
                return false;
            }
            
            // looping number of rows
            // for ($i=0;$i<count($result);$i++)
            // {
                // loop for arrays[table]
                // foreach ($result[$i] as $tableResult)
                foreach ($result as $tableResult)
                {
                    if (isset($tableResult['id']))
                    {
                        $id = $tableResult['id'];
                    }
                    else
                    {
                        $id = $i;
                    }
                    
                    // this row is for array which is not an array like field 
                    if (!is_array($tableResult))
                    { 
                        $trs.= '<tr style="display:none">';
                        $trs.= '<td>'.$tableResult.'</td>';
                        $trs.='</tr>';
                    }
                    else
                    {
                        $trs.='<tr @att@ id="'.$id.'">';
                        // Result content
                        foreach ($tableResult as $column => $data)
                        {
                            # collecting columns first loop
                            $add_attrib = ''; 
                            if (!empty($tr_attrib))
                            {
                                foreach ($tr_attrib as $att => $value)
                                {
                                    if ($att == 'onclick' || $att == 'onClick')
                                    {
                                        $tempval = $value;
                                        $list = substr($tempval,strpos($tempval,"(")+1);
                                        $jsContain = substr($list,0,strlen($list)-2);
                                        $jsContain = explode(',',$jsContain);
                                        
                                        foreach ($jsContain as $jsContent)
                                        {
                                            $jsContent = str_replace("'",'',$jsContent);
                                            if ($jsContent == $column)
                                            {
                                                $jsVar[$jsContent] = $data;
                                            } 
                                        }
                                        
                                        $add_attrib .= $att .'="'. $value .'"';
                                    }
                                    else
                                    {
                                        $add_attrib .= $att .'="'. $value .'" ';
                                    }
                                }
                            }
                            
                            if ($column == 'id')
                            {
                                // hide id field
                                $trs .= '<td style="display:none">'.$data.'</td>';
                            }
                            elseif (in_array($column, array('prod', 'uom')))
                            {
                                // hide prod at uom field for Officesales
                                $trs .= '<td style="display:none" >'.$data.'</td>';
                            }
                            elseif ($column == 'plantcode')
                            {
                                // hide plantcode for RS
                                $trs .= '<td style="display:none" >'.$data.'</td>'; 
                            }
                            elseif (in_array($column, array('pickupplace_id', 'pickupplace_location', 'pickupplace_location_id', 
                                                            'destination_id', 'destination_location', 'destination_location_id', 
                                                            'pickupplace2', 'destination2', 'vehicletype_id', 'vehicle_id')))
                            {
                                // hide other Service Vehicle fields
                                $trs .= '<td style="display:none" >'.$data.'</td>';
                            }
                            elseif (in_array($column, array('time_start', 'time_end')))
                            {
                                // to hide other fields for Notification 
                                $trs .= '<td style="display:none" >'.$data.'</td>';
                            }
                            else
                            {
                                $trs .= '<td @td_attrib@ >'.$data.'</td>'; 
                            }
                            
                            $tdatt = null;
                            if (!empty($td_attrib))
                            {
                                foreach ($td_attrib as $atrbt => $value)
                                {
                                    if ($atrbt != 'width')
                                    {
                                        $tdatt .= $atrbt .'="'. $value .'" ';
                                    }
                                }
                            }

                        }
                        
                        if (isset($jsVar))
                        {
                            $add_attrib = str_replace("'",'',$add_attrib);
                            foreach ($jsVar as $js => $varJs)
                            {
                                $add_attrib = str_replace($js,$varJs,$add_attrib);
                            }
                        }
                    }
                    
                    $trs.='</tr>';
                    $trs = str_replace('@att@',$add_attrib,$trs); // cleaning @att@
                    $trs = str_replace('@td_attrib@',$tdatt,$trs);// cleaning @td_attrib@ 
                }
            // }
        
        }

        $tbl_attrib = null;
        if (!empty($table_attrib) && is_array($table_attrib))
        {
            foreach ($table_attrib as $grid_attrib => $value)
            {
                $tbl_attrib .= $grid_attrib .'="'. $value .'" ';
            }
        }

        $datagrid = str_replace('@tbl_attrib@',$tbl_attrib,$datagrid); // cleaning @tbl_attrib@
        $tbody = $headers.$trs;
        $datagrid = str_replace('@tr@@td@@/td/@@/tr/@',$tbody,$datagrid);
        return $datagrid;
        
    }
}

/* EOF */