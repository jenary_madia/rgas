<?php

/*
 * This class is for common functions used by various modules
 * 
 */

class Common
{
    public static function get_seq_company_code($company='')
    {
        $comp = array(
                    "RBC-CORP" => "CORP"
                  , "RBC-SAT"  => "RBC"
                  , "SFI"      => "SFI"
                  , "PFI"      => "PFI"
                  , "MFC"      => "MFC"
                  , "BBFI"     => "BBF"
                  , "BUK"      => "BUK"
                  , "JBC"      => "JBC"
                  , "FBC"      => "FBC"
                );
        
        return $comp[$company];
    }

    public static function fixcase($name='')
    {
        $name = ucwords(strtolower($name));
        $name = str_replace(array('Ii', 'Iii', 'Mcmeans'),
                            array('II', 'III', 'McMeans'),
                            $name);
        
        return $name;
    }
}

/* EOF */