<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 21/09/2016
 * Time: 9:47 AM
 */
class NPMDRequirements extends \Eloquent
{
    protected $table = 'npmdrequirements';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;
}