<?php


class MrfSignatories extends \Eloquent {

    
    protected $table = "mrf_signatories";

    protected $primaryKey = "id";
    protected $hidden = array("password");

    public $timestamps = false;

//⦁	1 ENDORSE
//  2 PROCESSED - request is currently with CHRD Initial Recipient
//⦁	3 ASSESSED – request is currently with CSMD or Satellite Systems for assessment.
//⦁	4 APPROVED - request is currently with VP for Operations, SVP Corp Services and President for approval.
//⦁	5 DISAPPROVED - Approved requests delegated to Recruitment staff
//⦁	6 FOR FURTHER ASSESSMENT - Returned request by SVP Corp Services or President to either CHRD or CSMD Dept Head
//⦁	7 NOTED - Approved requests delegated to Recruitment staff
//⦁	8 RETURNED - Disapproved requests by Satellite Systems or CSMD


    public function addSignatories($mrfID,$type,$id = null) {
        $maxSeq = $this->where('manpowerrequestid',$mrfID)->max('signature_type_seq');
        if (! $maxSeq )
        {
            $maxSeq =  1;
        }else{
            $maxSeq += 1;
        }
        $this->manpowerrequestid = $mrfID;
        $this->signature_type = $type;
        $this->signature_type_seq = $maxSeq;
        $this->employee_id = (is_null($id) ? Session::get("employee_id") : $id);
        $this->approval_date = date('Y-m-d');
        $this->save();
    }

    public function rollbackSignatories($mrfID,$id) {
        $signatories = $this->where('manpowerrequestid',$mrfID)
            ->where('signature_type', '>=' ,$id);
        $signatories->delete();
    }

    public function employee() {
        return $this->hasOne('Employees','id','employee_id')->select(['id','firstname','lastname','middlename']);
    }
}