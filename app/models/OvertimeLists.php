<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 11/10/2016
 * Time: 9:55 AM
 */
class OvertimeLists extends \Eloquent {

    protected $table = 'overtimelists';
    public $incrementing = false;
    public $timestamps = false;

}