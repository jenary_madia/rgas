<?php

// use Employees;
// use Signatory;
 use RGAS\Modules\LRF;
class Leaves extends \Eloquent {
    

    protected $table = 'leaves';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;

    /*--------------------------for data table of "leaves of current logged in"-----------------------------*/
	public function myLeaves()
	{
		$requests = Leaves::with("employee")
							->where('ownerid',Session::get("employee_id"))
							->where("isdeleted",0)
							->where("isprocessed",0)
							->get();
		$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
		if ( count($requests) > 0){
            $ctr = 0;
				foreach($requests as $req) {
					$deletableNotifications = (in_array($req['status'], json_decode(DELETABLE,true)));
					$editableNotifications = (in_array($req['status'], json_decode(EDITABLE,true)));
		         	$btnDelete = "<button  ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' name='lrfAction' value='softDelete|{$req->id}'>Delete</button>";
		         	$btnView = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/my_leaves/'.$req->id.'/view').">View</a>";
		         	$btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/my_leaves/'.$req->id.'/edit').">Edit</a>";
					$result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
					$result_data["aaData"][$ctr][] = $req->datecreated;
					$result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from.' to '.$req->to : $req->from);
					$result_data["aaData"][$ctr][] = $req->appliedfor;
					$result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
					$result_data["aaData"][$ctr][] = $req->status;
					$result_data["aaData"][$ctr][] = ($req->employee ? $req->employee->firstname." ".$req->employee->middlename." ".$req->employee->lastname : "----");
					$result_data["aaData"][$ctr][] = $btnDelete.' '.$btnView.' '.$btnEdit;
					$ctr++;
				}
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}

/*--------------------------for data table of "for approve leaves"-----------------------------*/
	public function getForApproveLeaves()
	{	
		$forApproveLeaves = Leaves::forApproveLeaveSearch()
							->where('curr_emp', Session::get('employee_id'))
							->where('status', 'FOR APPROVAL')
                            ->where("isdeleted",0)
                            ->where("isprocessed",0)
							->get(); 

		$result_data["iTotalDisplayRecords"] = count($forApproveLeaves); //total count filtered query
		$result_data["iTotalRecords"] = count($forApproveLeaves);
		if ( count($forApproveLeaves) > 0){
            $ctr = 0;
             foreach($forApproveLeaves as $req) {				
                $result_data["aaData"][$ctr][] = '<input type="checkbox" name="chosenLeave[]" value="'.$req->id.'" class="chosenLeave"> '.$req->documentcode.'-'.$req->codenumber;
				$result_data["aaData"][$ctr][] = $req->datecreated;
				$result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from.' to '.$req->to : $req->from);
				$result_data["aaData"][$ctr][] = $req->appliedfor;
				$result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
				$result_data["aaData"][$ctr][] = $req->status;
				$result_data["aaData"][$ctr][] = $req->firstname.' '.$req->middlename.' '.$req->lastname;
				$result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/leave/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/leave/'.$req->id.'/approve').">Approve</a>";
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $forApproveLeaves;
        }
		return $result_data;
	}

    public function scopeForApproveLeaveSearch($query)
    {
    	if (Input::get('sSearch'))
		{	
			return $query->whereRaw('concat(firstname," ",middlename," ",lastname) like "%'.Input::get('sSearch').'%" ');
		}
    }

/*--------------------------for data table of "submitted leaves to receiver"-----------------------------*/
	public function submittedToReceiver()
	{	
		$hrLeaves = Leaves::where('curr_emp', Session::get('employee_id'))
		->where('isprocessed',0)
		->where('isdeleted', 0)
		->where('status','APPROVED')
		->get();
		
		$result_data["iTotalDisplayRecords"] = count($hrLeaves); //total count filtered query
		$result_data["iTotalRecords"] = count($hrLeaves);
		if ( count($hrLeaves) > 0){
            $ctr = 0;
             foreach($hrLeaves as $req) {				
                $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
				$result_data["aaData"][$ctr][] = $req->datecreated;
				$result_data["aaData"][$ctr][] = $req->dateapproved;
				$result_data["aaData"][$ctr][] = $req->lastname.' '.$req->firstname.' '.$req->middlename;
				$result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from.' to '.$req->to : $req->from);
				$result_data["aaData"][$ctr][] = $req->noofdays;
				$result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
				$result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/submitted/leave/'.$req->id.'/view').">View</a><a style='margin-left:3px' class='btn btnDataTables btn-default btn-xs' href=".url('/submitted/leave/'.$req->id.'/process').">Process</a>";
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $hrLeaves;
        }
		return $result_data;
	}

/*--------------------------for data table of "clinic approval leaves"-----------------------------*/
	public function submittedToClinic()
	{	
		$clinicLeaves = Leaves::forApproveLeaveSearch()
		->where("curr_emp", Session::get('employee_id'))
		->where("isprocessed",0)
		->where("status","FOR APPROVAL")
		->where("isdeleted", 0)
		->whereRaw("(appliedfor = 'Sick' and noofdays > 3 or appliedfor = 'Mat/Pat')")
		->get(); 

		$result_data["iTotalDisplayRecords"] = count($clinicLeaves); //total count filtered query
		$result_data["iTotalRecords"] = count($clinicLeaves);
		if ( count($clinicLeaves) > 0){
            $ctr = 0;
             foreach($clinicLeaves as $req) {				
 				$result_data["aaData"][$ctr][] = '<input type="checkbox" name="chosenLeave[]" value="'.$req->id.'" class="chosenLeave"> '.$req->documentcode.'-'.$req->codenumber;
				$result_data["aaData"][$ctr][] = $req->datecreated;
				$result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from.' to '.$req->to : $req->from);
				$result_data["aaData"][$ctr][] = $req->appliedfor;
				$result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
				$result_data["aaData"][$ctr][] = $req->status;
				$result_data["aaData"][$ctr][] = $req->lastname.' '.$req->firstname.' '.$req->middlename;
				$result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default' href=".url('/lrf/'.$req->id.'/clinic').">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default' href=".url('/lrf/'.$req->id.'/clinic/Contracts').">Approve</a>";
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $clinicLeaves;
        }
		return $result_data;
	}


/*----------For leave column codenumber---------*/ 
	public function generateCodeNumber()
	{
		$documentCodeToday = date('Y-m'); 
		$latestDocumentCode = Leaves::max('documentcode');
		if ($latestDocumentCode >= $documentCodeToday)
		{
			$latestCodenumber = Leaves::where('documentcode','=',$latestDocumentCode)->max('codenumber');
			if (! $latestCodenumber) {
				return '00000';
			}else{
				return str_pad($latestCodenumber + 1, 5, '0', STR_PAD_LEFT);
			}
		}else{

			return str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);
		
		}

	}

	public function store($parameters)
	{
		
		$storeLeave = Leaves::create($parameters);
		(new LRF\Logs())->AU001($storeLeave->id,'leaves','id',json_encode($parameters),$parameters['documentcode'].'-'.$parameters['codenumber']);
		if ($storeLeave)
		{
			return $storeLeave;
		}else{
			return 0;
		}
	}

	public function faker()
	{
		$faker = Faker\Factory::create();
		for ($i=0; $i < 100; $i++) {
			Leaves::create(
				array(
					'documentcode' => $faker->date('Y-m','now'),
					'dateapproved' => $faker->date('Y-m-d','now'),
					'datecreated' => $faker->date('Y-m-d','now'),
					'firstname' => Session::get('firstname'),
					'middlename' => Session::get('middlename'),
					'lastname' => Session::get('lastname'),
					'appliedfor' => 'Sick Leave',
					'noofdays' => 1.5,
					'from' => $faker->date('Y-m-d','now'),
					'to' => $faker->date('Y-m-d','now'),
					'reason' => 'Sick'
			));
			// return $data;
		}
	}


/*--------------------Getting specific Leave for-------------------*/ 
	
	public static function getLeave($id,$purpose)
	{
		/*
			PURPOSE
			1 = myleaves
			2 = forApproveLeaves
		*/
		$result = Leaves::purpose($purpose)
					->where("id",$id)->first();
		if (!$result) {
			return 0;
		}

		$restDays =  implode('&',json_decode($result->rest_day));
		return array(
				"id" => $result->id,
				"referenceNo" => $result->documentcode.'-'.$result->codenumber,
				"fullName" => $result->firstname.' '.$result->middlename.' '.$result->lastname,
				"employeeId" => $result->employeeid,
				"company" => $result->company,
				"dateCreated" => $result->datecreated,
				"leaveType" => $result->appliedfor,
				"reason" => $result->reason,
				"message" => explode('|',$result->comment),
				"from" => $result->from,
				"to" => $result->to,
				"department" => $result->department,
				"section" => $result->section,
				"contactNo" => $result->contact_no,
				"scheduleType" => $result->schedule_type,
				"restDay" => $restDays,
				"duration" => $result->duration,
				"totalLeaveDays" => $result->noofdays,
				"status" => $result->status,
				"periodTo" => $result->period_to,
				"periodFrom" => $result->period_from,
				"ownerId" => $result->ownerid,
				"attach1" => $result->attach1,
				"attach2" => $result->attach2,
				"attach3" => $result->attach3,
				"attach4" => $result->attach4,
				"attach5" => $result->attach5,
				"receiver_attachments" => json_decode($result->receiver_attachments,true),
				"clinic_attachment" => json_decode($result->clinic_attachment,true)
			);

	}

	public function scopePurpose($query,$purpose)
	{
		if ($purpose == 1)
		{
			return $query->where("ownerid", Session::get("employee_id"));
		}
			return $query->where("curr_emp", Session::get("employee_id"));
	}

/*--------------------Join with leave-------------------*/
	public function signatories()
	{
		return $this->hasMany('Signatory','leave_id','id');
	}

	public function employee()
	{
		return $this->hasOne('Employees','id','curr_emp')->select(array('firstname','middlename','lastname','id','email'));
	}


/*--------------Leave status update upon passing to other personnel---------------*/

	public static function statusUpdate($parameters)
	{
		$toUpdate = array(
			"status" => $parameters["status"],
			"curr_emp" => $parameters["nextApprover"],
			"comment" => DB::raw("concat(comment,'|','{$parameters['comment']}')")
		);
        if (array_key_exists("dateApproved", $parameters)) {
			$toUpdate["dateapproved"] = $parameters["dateApproved"];
		}

		if ($parameters["nextApprover"] === "toClarify") { // TODO
            $toUpdate["curr_emp"] = Employees::where("id",$parameters["ownerId"])->first()["superiorid"];

			if(! $toUpdate["curr_emp"]) {
				return "ERROR003";
			}
		}
        
        $leave = Leaves::where('id',$parameters["leaveId"]);
        (new LRF\Logs())->AU004($leave->first()["id"],'leaves','id',json_encode($toUpdate),$leave->first()['documentcode'].'-'.$leave->first()['codenumber'],json_encode($leave->first()));

        $action = Leaves::where("id", $parameters["leaveId"])->update($toUpdate);

		if ($action) {
			return 1;
		}

		return 0;
	}

/*-------------approve per batch------------------*/
	public static function batchApprove($leaveIDS,$comment)
	{
		$hrReciever = Receivers::get("leaves","HR_LEAVE_RECEIVER",Session::get("company"));
        $leaves = Leaves::whereIn("id", $leaveIDS)
        ->where("curr_emp",Session::get("employee_id"));

        $parameters = array(
            "status" => "APPROVED",
            "curr_emp" => $hrReciever['employee']['id'],
            "dateapproved" => date('Y-m-d'),
            "comment" => DB::raw("concat(comment,'|','{$comment}')")
        );
        // TODO
//        (new LRF\Logs())->AU004($leaves->first()['id'],'leaves','id',json_encode($parameters),$leaves->first()['documentcode'].'-'.$leaves->first()['codenumber'],json_encode($leaves->first()));

        $action = $leaves->update($parameters);
		if ($action) {
			return 1;
		}

		return 0;
	}

/*---------------soft delete--------------------*/

	public static function softDelete($id)
	{
		$leave = Leaves::where("id",$id);
        $parameters = array(
            "isdeleted" => 1,
			"status" => "DELETED",
			"curr_emp" => Session::get("superior_id")
        );
        (new LRF\Logs())->AU005($leave->first()['id'],'leaves','id',json_encode($parameters),$leave->first()['documentcode'].'-'.$leave->first()['codenumber'],'leaves','id');
		$action = $leave->update($parameters);
		if ($action) {
			return $leave->first(["codenumber","documentcode"]);
		}else{
			return 0;
		}
	}


/*---------------Contracts and send to chrd--------------------*/

	public static function processLeave($id)
	{
		$leave = Leaves::where("id",$id);

		$action = $leave->update(array(
					"isprocessed" => 1
				));
		if ($action) {
			return $leave->first(["codenumber","documentcode"]);
		}else{
			return 0;
		}
	}


/*----------------------Reprocessing Leave---------------------*/

	public static function reprocess($parameters)
	{
		$leave = Leaves::where('documentcode', $parameters["documentcode"])
								->where('codenumber', $parameters["codenumber"]);
		$signatories = Signatory::where('leave_id', $leave->first(["id"]));

		if ($signatories->count() > 0) {

			$signatories->delete();

		}

		$updateLeaveStatus = $leave->update($parameters);

		if ($updateLeaveStatus) {
			return 1;
		}else{
			return 0;
		}
	}

	public static function getRefNo($id)
	{
		$refNo = Leaves::where('id',$id)->first(["documentcode","codenumber"])->toArray();
		return implode('-', $refNo);
	}
	
	

/*------------Get approved leaves by ID--------------*/

	public static function getApprovedLeaves($id,$type = null)
	{
		return Leaves::selectRaw('appliedfor,sum(noofdays) as sum')
			->where('ownerid', $id)
			->whereRaw('year(date) = ?',[date('Y')])
			->homeVisit($type)
			 ->where('status', 'APPROVED')
			// ->where('isprocessed', 1)
			->groupBy('appliedfor')->get();
		// ->where('isprocessed',1)
	}

	public function scopeHomeVisit($query,$type = null)
	{
		if($type)
		{
			$query->where('appliedfor','!=','HomeVisit');
		}
	}

	public static function getHomeVisitLeaves($id,$quarter)
	{
		$quarter = implode(',', $quarter);
		return Leaves::selectRaw('appliedfor,sum(noofdays) as sum')
		->where('ownerid', $id)
		->whereRaw('year(now()) = ?',[date('Y')])
		->where('appliedfor','HomeVisit')
		->where('status','APPROVED')
		->whereRaw('month(now()) in ('.$quarter.')')->first();

	}

	/*--------------------Join with Notification-------------------*/
	public function department()
	{
		return $this->hasOne('Departments','dept_name','department');
	}
    /*--------------------For receiver filtering-------------------*/
    public function filterLeaveForReceiver() {
			$hrLeaves = Leaves::where('curr_emp', Session::get('employee_id'))
				->where('status','APPROVED')
				->forReceiver()
				->get();
        $leaveData = [];
        if ( count($hrLeaves) > 0){
            $ctr = 0;
            foreach($hrLeaves as $req) {
                $leaveData[$ctr][] = $req->documentcode.'-'.$req->codenumber;
                $leaveData[$ctr][] = $req->datecreated;
                $leaveData[$ctr][] = $req->dateapproved;
                $leaveData[$ctr][] = $req->lastname.' '.$req->firstname.' '.$req->middlename;
                $leaveData[$ctr][] = ($req->to != '0000-00-00' ? $req->from.' to '.$req->to : $req->from);
                $leaveData[$ctr][] = $req->noofdays;
                $leaveData[$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $leaveData[$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/submitted/leave/'.$req->id.'/view').">View</a><a style='margin-left:3px' class='btn btnDataTables btn-default btn-xs' href=".url('/submitted/leave/'.$req->id.'/process').">Process</a>";
                $ctr++;
            }
        }
        return $leaveData;
    }

    public function scopeForReceiver($query) {
        if (Input::has("department")) {
            if(Input::get("department") != "all") {
                $query->where("department",Input::get("department"));
            }
        }

        if (Input::has("dateFrom")) {
            if(Input::has("dateTo")) {
                $query->whereBetween("dateapproved",[Input::get("dateFrom"),Input::get("dateTo")]);
            }else{
                $query->where("dateapproved",Input::get("dateFrom"));
            }
        }

        if (Input::has("firstName")) {
            $query->where("firstname", 'LIKE' , '%'.Input::get('firstName').'%');
        }
        if (Input::has("middleName")) {
            $query->where("middlename", 'LIKE' , '%'.Input::get('middleName').'%');
        }
        if (Input::has("lastName")) {
            $query->where("lastname", 'LIKE' , '%'.Input::get('lastName').'%');
        }

        if (Input::has("leaveType")) {
            if(! in_array("on",Input::get("leaveType"))) {
                $query->whereIn("appliedfor",Input::get("leaveType"));
            }
        }
    }

}


