<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:42 PM
 */
class MOCRequestCodes extends \Eloquent {
    protected $table = 'moc_requestcodes';
    public $incrementing = true;
    public $timestamps = false;
    protected $fillable = ['request_id', 'request_code', 'request_desc'];
}
      