<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 11/10/2016
 * Time: 10:45 AM
 */
class Overtimes extends \Eloquent {

    protected $table = 'overtimes';
    public $incrementing = true;
    public $timestamps = false;

/*********************Join with overtimelists********************/
    public function OTLists()
    {
        return $this->hasMany('OvertimeLists','overtimeid','id')
            ->where("date",Input::get("OTDate"))
            ->where('otstart',date("H:i:s", strtotime(Input::get("OTStart"))))
            ->where('otend',date("H:i:s", strtotime(Input::get("OTEnd"))));
    }
}