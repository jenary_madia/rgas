<?php


class EmployeesPlant extends \Eloquent {


    protected $table = "employees_plant";

    protected $primaryKey = "id";
    protected $hidden = array("password");

    public $timestamps = false;
    
}