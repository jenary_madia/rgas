<?php


class TafActivityLogs extends \Eloquent {

    
	 protected $table = 'taf_activity_logs';
	 
	 public $incrementing = true;
	 public $timestamps = false;
	
	
	public static function insert_activity($activity, $employee_id)
	{
		$taf_activity = new TafActivityLogs;
		$taf_activity->activity = $activity;
		$taf_activity->employee_id = $employee_id;
		
		$taf_insert = $taf_activity->save();
		
		return $taf_insert;
	}
	
}
/* End of file */