<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 09/08/2016
 * Time: 2:46 PM
 */
class LeaveBatchDetails extends \Eloquent
{
    protected $table = 'leave_batch_details';
    protected $guarded = array('id');
    public $incrementing = false;
    public $timestamps = false;

    public function newBatchDetails($employeesData,$id)
    {
        $employeesData = json_decode($employeesData,true);
        $toInsert = [];
        foreach($employeesData as $key) {
            array_push($toInsert,array(
                'leave_batch_id' => $id,
                'employee_id' => $key["employeeID"],
                'employee_number' => $key["employeeNumber"],
                'employee_name' => $key["employeeName"],
                'appliedfor' => $key["leaveType"],
                'duration' => $key["duration"],
                'from_date' => $key["fromDate"],
                'from_duration' => $key["fromPeriod"],
                'to_date' => $key["toDate"],
                'to_duration' => $key["toPeriod"],
                'noofdays' => $key["totalDays"],
                'reason' => $key["reason"]
            ));
        }

        if (LeaveBatchDetails::insert($toInsert)) {
            return 1;
        }else{
            return 0;
        }
    }

    public function leaveItem() {
        return $this->hasOne("SelectItems","item","appliedfor");
    }
}