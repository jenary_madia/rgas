<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 03/08/2016
 * Time: 11:31 AM
 */
class NotificationBatchDetails extends \Eloquent
{
    protected $table = 'notification_batch_details';
    public $incrementing = false;
    public $timestamps = false;
    public $guarded =  array('id');

    public function newBatchDetails($employeesData,$id)
    {
        $employeesData = json_decode($employeesData,true);
        $toInsert = [];
        foreach($employeesData as $key) {
            array_push($toInsert,array(
               'notification_batch_id' => $id,
               'employee_id' => $key["employeeID"],
               'employee_number' => $key["employeeNumber"],
               'employee_name' => $key["employeeName"],
               'noti_type' => $key["notificationType"],
               'from_date' => $key["date"],
               'from_in' => $key["timeIn"],
               'from_out' => $key["timeOut"],
               'reason' => $key["reason"]
          ));
        }

        if (NotificationBatchDetails::insert($toInsert)) {
            return 1;
        }else{
            return 0;
        }
    }

    public function notif() {
        return $this->hasOne("SelectItems","item","noti_type");
    }
}
