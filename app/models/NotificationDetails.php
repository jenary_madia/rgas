<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 2:25 PM
 */
class NotificationDetails extends \Eloquent
{
    protected $table = 'notificationdetails';
    public $incrementing = false;
    public $timestamps = false;
    public $guarded =  array('id');

    public function parseDetails($id) {
        $notifAdditionalDetails = NotificationDetails::where("notificationid",$id)->get();

        $parsed = [];

        foreach ($notifAdditionalDetails as $key) {
            array_push($parsed,array(
                'OBDestination' => $key['destination'],
                'OTDate' => $key['date'],
                'OTDuration' => $key['total_time'],
                'active' => false,
                'checked' => false,
                'reason' => $key['reason'],
                'timeInStartTime' => date_format(date_create($key['time_start']),"H:i"),
                'timeOutEndTime' => date_format(date_create($key['time_end']),"H:i"),
                'type' => $key['type']
            ));

        }
        
        return json_encode($parsed);
    }
}