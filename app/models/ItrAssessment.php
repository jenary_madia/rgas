<?php

class ItrAssessment extends \Eloquent {
    
    protected $table = 'itr_assessment_reco';
    public $incrementing = false;
    public $timestamps = false;
    
    
    public static function get_itr_assessment($itr_no)
    {
        $itr_assessment = ItrAssessment::leftJoin("itr_requests as itr", function($join){
            
            $join->on("itr.itr_no", "=", "itr_assessment_reco.itr_no");
            
        })->leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr.bsa_curr_emp");
            
        })->where("itr_assessment_reco.itr_no", "=", "{$itr_no}")->first([
        
            "itr_assessment_reco.itr_no"
           ,"itr_assessment_reco.job_desig"
           ,"itr_assessment_reco.job_desig_remarks"
           ,"itr_assessment_reco.nature_work"
           ,"itr_assessment_reco.nature_work_remarks"
           ,"itr_assessment_reco.endorsement_request"
           ,"itr_assessment_reco.endorsement_request_remarks"
           ,"itr_assessment_reco.recommendation"
           ,"itr_assessment_reco.recommendation_remarks"
           ,"emp.firstname"
           ,"emp.middlename"
           ,"emp.lastname"
           ,"itr.bsa_assigned_date"
           ,"itr.smdd_approved_date" 
           
        ]);
        
        return $itr_assessment;
        
    }
    
 

    //get assigned itr for assessment
    public static function get_assessment_itr($where_condition, $order_by_col, $order_by_col2 ,$order_by_sort, $order_by_col3, $order_by_sort3)
    {
        
        $itr_lists = ItrRequests::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_requests.curr_emp");
            
        })->leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_requests.itr_status");
            
        })->leftJoin("itr_system_config as config2", function($join){
            
            $join->on("config2.system_code", "=", "itr_requests.request_type");
            
        })->leftJoin("itr_system_config as config3", function($join){
            
            $join->on("config3.system_code", "=", "itr_requests.request_type_desc");
            
        })->leftJoin("employees as emp2", function($join){
            
            $join->on("emp2.id", "=", "itr_requests.requestor"); 
            
        })->leftJoin("employees as emp3", function($join){
            
            $join->on("emp3.id", "=", "itr_requests.bsa_curr_emp"); 
            
        })->leftJoin("departments as dept", function($join){
            
            $join->on("dept.id", "=", "emp2.departmentid");
            
        })->leftJoin("companylists as comp", function($join){
            
            $join->on("comp.comp_code", "=", "emp2.company"); 
            
        })->leftJoin("itr_assessment_reco as assess", function($join){
            
            $join->on("assess.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("itr_assigned as assigned", function($join){
            
            $join->on("assigned.itr_no", "=", "itr_requests.itr_no");
                
        })->leftJoin("employees as assigned_emp", function($join){
            
            $join->on("assigned_emp.id", "=", "assigned.employee_id");
            
        })->leftJoin("itr_custom_request as custom_itr", function($join){
          
            $join->on("custom_itr.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("departments as dep2", function($join){
          
            $join->on("dep2.id", "=", "custom_itr.custom_dept_id");
            
        })->whereRaw("{$where_condition}")->orderBy($order_by_col3, $order_by_sort3)->orderBy($order_by_col, $order_by_sort)->orderBy($order_by_col2, $order_by_sort)->groupBy("itr_requests.itr_no")->get([
        
             "itr_requests.itr_no"
            ,"itr_requests.trans_date"
            ,"itr_requests.request_type"
            ,"itr_requests.request_type_desc"
            ,"itr_requests.rgs_status"
            ,"itr_requests.itr_status"
            ,"itr_requests.status"
            ,"itr_requests.curr_emp"
            ,"itr_requests.uss_curr_emp"
            ,"itr_requests.bsa_curr_emp"
            ,"itr_requests.bsa_assigned_date"
            ,"itr_requests.req_description"
            ,"itr_requests.uss_view"
            ,"itr_requests.request_type"
            ,"emp.firstname as current_fname"
            ,"emp.lastname as current_lname"
            ,"config.system_desc as itr_status_text"
            ,"config2.system_desc as request_type_text"
            ,"config3.system_desc"
            ,"emp2.firstname as requestor_fname"
            ,"emp2.lastname as requestor_lname"
            ,"comp.id as comp_id"
            ,"comp.comp_code"
            ,"comp.comp_name"
            ,"emp3.firstname as bsa_fname"
            ,"emp3.lastname as bsa_lname"
            ,"dept.dept_name"
            ,"assess.itr_no as assessment_itr_no"
            ,"custom_itr.requestor_name as custom_requestor_name"
            ,"custom_itr.custom_dept_id"
            ,"custom_itr.custom_designation"
            ,"dep2.dept_name as custom_dept_name"

        ]);
        
        return $itr_lists;
        
    }
    
 
 
    //
    public static function get_company_lists_assessment($employee_id, $csmd_adh_id="")
    {
        
        if ($csmd_adh_id == $employee_id)
        {
            
            $company_lists = ItrRequests::leftJoin("companylists as comp", function($join){
                
                $join->on("comp.comp_code", "=", "itr_requests.company");
                
            })->leftJoin("itr_custom_companies as custom_comp", function($join) {
                
                $join->on("custom_comp.custom_comp_id", "=", "itr_requests.company");
                
            })->where("itr_requests.bsa_curr_emp", "<>", "")->groupBy("comp.comp_name")->groupBy("custom_comp.custom_comp_nm")->orderBy("comp.comp_name", "asc")->get([
            
                 "comp.id as comp_id"
                ,"comp.comp_code"
                ,"comp.comp_name"
                ,"custom_comp.custom_comp_id"
                ,"custom_comp.custom_comp_nm"
                
            ]);
            
        }
        else
        {
            
            $company_lists = ItrRequests::leftJoin("employees as emp", function($join){
            
                $join->on("emp.id", "=", "itr_requests.requestor");
            
            })->leftJoin("companylists as comp", function($join){
                
                $join->on("comp.comp_code", "=", "emp.company");
                
            })->leftJoin("itr_custom_request as itr_custom", function($join) {
                
                $join->on("itr_custom.itr_no", "=", "itr_requests.itr_no");
                
            })->leftJoin("companylists as comp2", function($join){
                
                $join->on("comp2.comp_code", "=", "itr_custom.custom_comp_id");
                
            })->where("itr_requests.rgs_status", "=", "For Assessment")->orWhere("itr_requests.bsa_curr_emp", "=", "{$employee_id}")->groupBy("comp.comp_name")->groupBy("comp2.comp_name")->orderBy("comp.comp_name", "asc")->orderBy("comp2.comp_name", "asc")->get([
            
                 "comp.id as comp_id"
                ,"comp.comp_code"
                ,"comp.comp_name"
                ,"comp2.id as comp2_id"
                ,"comp2.comp_code as custom_comp_code"
                ,"comp2.comp_name as custom_comp_name"
                
            ]);
            
        }
        
        
        
        
        return $company_lists;
        
    }
    
    
    //
    public static function get_department_lists_assessment($employee_id, $csmd_adh_id="")
    {
        
        if ($csmd_adh_id == $employee_id)
        {
            
            
            $department_lists = ItrRequests::leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "itr_requests.departmentid");
                
            })->leftJoin("itr_custom_dept as custom_dept", function($join) {
                
                $join->on("custom_dept.custom_dept_id", "=", "itr_requests.departmentid");
                
            })->where("itr_requests.bsa_curr_emp", "<>", "")->where("itr_requests.is_deleted", "=", "0")
            ->groupBy("dept.dept_name")
            ->orderBy("dept.dept_name", "asc")
            ->get([
            
                 "dept.id as dept_id"
                ,"dept.dept_name"
                ,"custom_dept.custom_dept_id"
                ,"custom_dept.dept_nm as custom_dept_name"
                
            ]);
            
        }
        else
        {
            
            $department_lists = ItrRequests::leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "itr_requests.departmentid");
                
            })->leftJoin("itr_custom_dept as custom_dept", function($join) {
                
                $join->on("custom_dept.custom_dept_id", "=", "itr_requests.departmentid");
                
            })->where("itr_requests.bsa_curr_emp", "=", "{$employee_id}")->where("itr_requests.is_deleted", "=", "0")
            ->groupBy("dept.dept_name")
            ->orderBy("dept.dept_name", "asc")
            ->get([
            
                 "dept.id as dept_id"
                ,"dept.dept_name"
                ,"custom_dept.custom_dept_id"
                ,"custom_dept.dept_nm as custom_dept_name"
                
            ]);
            
        }
        
        
        
        
        
        return $department_lists;
        
    }
 
 
 
    //
    public static function get_itr_satellite($where_condition, $order_by_col, $order_by_col2 ,$order_by_sort)
    {
        
         $itr_lists = ItrRequests::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_requests.curr_emp");
            
        })->leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_requests.itr_status");
            
        })->leftJoin("itr_system_config as config2", function($join){
            
            $join->on("config2.system_code", "=", "itr_requests.request_type");
            
        })->leftJoin("itr_system_config as config3", function($join){
            
            $join->on("config3.system_code", "=", "itr_requests.request_type_desc");
            
        })->leftJoin("itr_system_config as config4", function($join){
            
            $join->on("config4.system_code", "=", "itr_requests.curr_emp");
            
        })->leftJoin("itr_assigned as assd", function($join){
            
            $join->on("assd.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("employees as emp2", function($join){
            
            $join->on("emp2.id", "=", "itr_requests.requestor"); 
            
        })->leftJoin("employees as emp3", function($join){
            
            $join->on("emp3.id", "=", "itr_requests.bsa_curr_emp"); 
            
        })->leftJoin("companylists as comp", function($join){
            
            $join->on("comp.comp_code", "=", "emp2.company"); 
            
        })->leftJoin("employees as assigned_emp", function($join){
            
            $join->on("assigned_emp.id", "=", "assd.employee_id");
            
        })->whereRaw("{$where_condition}")->orderBy($order_by_col, $order_by_sort)->orderBy($order_by_col2, $order_by_sort)->groupBy("itr_requests.itr_no")->get([
        
             "itr_requests.itr_no"
            ,"itr_requests.trans_date"
            ,"itr_requests.request_type"
            ,"itr_requests.request_type_desc"
            ,"itr_requests.rgs_status"
            ,"itr_requests.itr_status"
            ,"itr_requests.curr_emp"
            ,"itr_requests.uss_curr_emp"
            ,"itr_requests.bsa_curr_emp"
            ,"itr_requests.req_description"
            ,"itr_requests.uss_view"
            ,"itr_requests.request_type"
            ,"emp.firstname as current_fname"
            ,"emp.lastname as current_lname"
            ,"config.system_desc as itr_status_text"
            ,"config2.system_desc as request_type_text"
            ,"config3.system_desc"
            ,"config4.system_desc"
            ,"emp2.firstname as requestor_fname"
            ,"emp2.lastname as requestor_lname"
            ,"comp.id as comp_id"
            ,"comp.comp_code"
            ,"comp.comp_name"
            ,"emp3.firstname as bsa_fname"
            ,"emp3.lastname as bsa_lname"

            
        ]);
        
        
        return $itr_lists; 
        
    }
 
 
    //   
    public static function update_assessment_reco_remarks($itr_no, $assessment_remarks_job_designation, $assessment_remarks_nature_work, $assessment_remarks_signed, $assessment_remarks_approve_request)
    {
        
        DB::table("itr_assessment_reco")->where("itr_no", $itr_no)->update(array(
        
            'job_desig_remarks' => $assessment_remarks_job_designation,
            'nature_work_remarks' => $assessment_remarks_nature_work,
            'endorsement_request_remarks' => $assessment_remarks_signed,
            'recommendation_remarks' => $assessment_remarks_approve_request
        ));
        
    }
 
}