<?php


class ItrAssigned extends \Eloquent {
    
    protected $table = "itr_assigned";
    public $incrementing = true;
    public $timestamps = false; 
    
    
    //
    public static function get_assigned_lists($itr_no)
    {
        
        $assigned_lists = ItrAssigned::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_assigned.employee_id");
            
        })->where("itr_assigned.itr_no", "=", "{$itr_no}")->get([
        
           "itr_assigned.itr_no"
          ,"itr_assigned.employee_id"
          ,"itr_assigned.assigned_date"
          ,"itr_assigned.ts_assigned"
          ,"emp.firstname"
          ,"emp.lastname"
        
        ]);
        
        return $assigned_lists;
        
    }
    
    //
    public static function assigned_itr($itr_no, $employee_id, $assigned_date)
    {   
        $itr_assigned = new ItrAssigned;
        
        $itr_assigned->itr_no = $itr_no;
        $itr_assigned->employee_id = $employee_id;
        $itr_assigned->assigned_date = $assigned_date;
        
        $itr_assigned->save();
    }
    
    
    //Check if employee is assigned to ITR @return bool
    public static function check_assigned_itr($itr_no, $employee_id)
    {
        
        $assigned_check = ItrAssigned::where("itr_no", "=", "{$itr_no}")->where("employee_id", "=", "{$employee_id}")->first(["employee_id"]);
        
        if ( !empty($assigned_check) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    
    
    
    
}