<?php
use app\CBR;
use app\RGAS;
use RGAS\Modules\CBR;
use RGAS\Libraries;

class CompensationBenefits extends \Eloquent {
    
	protected $primaryKey = 'cb_id';
    protected $table = 'compensationbenefits';
    public $incrementing = true;
    public $timestamps = true;
	
	protected $attachments_path = '/cbr/attachments';
	
	public $_cb_status;
	
	public $_cb_issent;
	
	public function cbrd()
	{
		return $this->hasMany('CompensationBenefitsRequestedDocs','cbrd_cb_id');
	}
	
	public function __destruct()
	{
		// $queries = DB::getQueryLog();
		// print_r(end($queries));
	}
	
	public static function urgency( $key = 0 )
	{
		$urgency[1] = "Normal Priority";
		$urgency[2] = "Immediate Attention Needed";
		
		if($key){
			return $urgency[$key];
		}
		return $urgency;
	}
	
	public function setCbStatus($status)
	{
		$this->_cb_status = $status;
	}
	
	public function setCbIssent($issent)
	{
		$this->_cb_issent = $issent;
	}
	
	public function store()
	{
		$fm = new RGAS\FileManager;		
		$filenames = $fm->upload($this->attachments_path);
		
		// Collate first all the data for validation before calling the model for inserting data to table
        $this->cb_employees_id = Session::get('employee_id');
        $this->cb_emp_name = Session::get('employee_name');
		$this->cb_emp_id = Session::get('employeeid');
		$this->cb_company = Session::get('company');
		$this->cb_department = Session::get('dept_name');
		$this->cb_section = Session::get('sect_name');
		$this->cb_ref_num = Input::get('cb_ref_num');
        $this->cb_date_filed = Input::get('cb_date_filed');
        $this->cb_status = $this->_cb_status;
        $this->cb_contact_no = Session::get('cb_contact_number');
        $this->cb_urgency = Input::get('cb_urgency');
        $this->cb_date_needed = Input::get('cb_date_needed');
		$this->cb_purpose_of_request = Input::get('cb_purpose_of_request');
        $this->cb_attachments = json_encode($filenames);
		if(Input::get('comment') != ""){
			$this->cb_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>Input::get('comment')
					)
				)
			);
		}
		$this->cb_issent = $this->_cb_issent;
        $this->save();
		
		$cbrd = new CompensationBenefitsRequestedDocs;
		$cbrd->setCbrd_cb_id($this->cb_id);
		$cbrd->setParent_ref_num($this->cb_ref_num);
		if($this->_cb_status == "New"){
			$cbrd->setCbrdStatus("New");
		}
		$cbrd->store();
		
		return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Sucess');

	}
	
	public function getRequestsNew()
	{
		// $requests = $this->where('cb_status','=','New')->where('cb_issent','=',1)->get();
		
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'New')
				->where('cb_issent','=',1);
        })
		->groupBy('cbrd_cb_id')
		->get();
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->cb_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/review-request/'.$req->cb_ref_num).'/view">View</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/cbr/review-request/'.$req->cb_ref_num).'">Process</a>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getRequestsMy()
	{
		$requests = $this->with('cbrd')->where("cb_employees_id","=",Session::get("employee_id"))->get();
		echo "<pre>";
		print_r($requests);
		echo "</pre>";
		die();
		// $requests = $this->where('cb_employees_id','=',Session::get("employee_id"))->get();
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->cb_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = $req->cbrd_req_docs;
				$result_data["aaData"][$ctr][] = $req->cb_status;
				if(!$req->cb_issent && $req->cb_status == "New"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req['cb_ref_num']).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/edit/'.$req['cb_ref_num']).'">Edit</a>';
				}
				elseif($req->cb_issent && $req->cb_status == "Returned"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req['cb_ref_num']).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/edit/'.$req['cb_ref_num']).'">Edit</a>';
				}
				elseif($req->cb_issent && $req->cb_status == "For Acknowledgement"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req['cb_ref_num']).'">View</a>';
				}
				elseif($req->cb_issent && $req->cb_status == "Acknowledged"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req['cb_ref_num']).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/delete/'.$req['cb_ref_num']).'">Delete</a>';
				}
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getRequestsInProcess()
	{
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                 ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				 ->orWhere('compensationbenefitsrequesteddocs.cbrd_status','=','For Acknowledgement');
        })->groupBy('compensationbenefitsrequesteddocs.cbrd_ref_num')->get();
		/*
		$queries = DB::getQueryLog();
		print_r(end($queries));
        echo "<pre>";
		print_r($requests);
		echo "</pre>";
		die();
		*/
		$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = json_decode($req->cbrd_req_docs)->name;

				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->cbrd_assigned_to);
				$result_data["aaData"][$ctr][] = $req->cbrd_assigned_date;
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->cbrd_current);
				$result_data["aaData"][$ctr][] = $req->cbrd_status;
				if($req->cbrd_assigned_to == Session::get('employee_id') && $req->cbrd_status == "For Processing"){
				$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/cbr/Contracts/'.$req->cbrd_ref_num).'">Process</a>';
				}
				else{
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>';
				}
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		
		return $result_data;
	}
	
	public function getRequestsAcknowledged()
	{
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                 ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'Acknowledged');
        })
        ->get();
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
				$result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = json_decode($req->cbrd_req_docs)->name;

				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->cbrd_assigned_to);
				$result_data["aaData"][$ctr][] = $req->cbrd_completed_date;
				$result_data["aaData"][$ctr][] = $req->cbrd_acknowledged_date;
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }

		return $result_data;
	}
	
	public function getRequest($cb_ref_num)
	{
		// take note of this
		$request = $this->with('cbrd')->where("cb_ref_num","=",$cb_ref_num)->get();
		return $request;
	}
	
	public function getRequestByCbrdRefNum($cbrd_ref_num)
	{
		$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_ref_num","=",$cbrd_ref_num)->first();
		$requests = $this->with("cbrd")->where("cb_id","=",$cbrd->cbrd_cb_id)->first();

		/*
		$cbrd = DB::table('compensationbenefits')
			->join('compensationbenefitsrequesteddocs', function($join) use($cbrd_ref_num)
			{
				$join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
					->where('compensationbenefitsrequesteddocs.cbrd_ref_num', '=', '2016-00046-1');
			})
			->first();
		echo "<pre>",print_r($cbrd),"</pre>";
		*/

		$cbrd = $this->with("cbrd", function($query) use ($cbrd_ref_num){
			$query->where('cbrd_ref_num', '=', $cbrd_ref_num);
		})->get();




		// echo "<pre>",print_r($requests),"</pre>";
		// $request = $this->with('cbrd')->where("cbrd_ref_num","=",$cbrd_ref_num)->get();
		/*
		$queries = DB::getQueryLog();
		print_r(end($queries));
		die();
		*/
		return $requests;
	}
	
	// function getRequestForProcessing($cb_ref_num)
	// {		
		// $request = $this->with('cbrd')->where("cb_ref_num","=",$cb_ref_num)->get();
		// return $request;
	// }
}