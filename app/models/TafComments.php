<?php


class TafComments extends \Eloquent {

    
	 protected $table = 'taf_comments';
	 protected $primaryKey = 'comment_id';
	 
	 public $incrementing = true;
	 public $timestamps = false;
	 
	 
	 //
	public static function get_comments($taf_no)
	{
	
		$comment_list = TafComments::leftJoin("employees as emp", function($join){
		
			$join->on("emp.id", "=", "taf_comments.employee_id");
		
		})->where("taf_comments.taf_no", "=", "{$taf_no}")->get([
		
			 "taf_comments.employee_id"
			,"taf_comments.comment"
			,"emp.firstname"
			,"emp.lastname"
			
		]);
		
		return $comment_list;
	
	}
	
	
}
/* End of file */