<?php

use app\CBR;

class CompensationBenefitsRequestedDocs extends \Eloquent {
    
	protected $primaryKey = 'cbrd_id';
    protected $table = 'compensationbenefitsrequesteddocs';
    public $incrementing = true;
    public $timestamps = false;
	protected $fillable = array('cbrd_id','cbrd_cb_id', 'cbrd_req_docs', 'cbrd_ref_num', 'cbrd_assigned_to', 'cbrd_assigned_date', 'cbrd_current', 'cbrd_status', 'cbrd_remarks', 'cbrd_processed_date', 'cbrd_date_filed');
	
	public $_cbrd_id;
	public $_cbrd_ref_num;
	public $parent_ref_num;
	public $_cbrd_status;
	
	public function employees()
	{
		return $this->has('employees','id','cbrd_assigned_to');
	}
	
	public function cb()
	{
		return $this->belongsTo('CompensationBenefits','cbrd_cb_id','cb_id');
	}
	
	public function store()
	{
		$ctr = 1;
		$b = array();
		foreach(Input::get('requested_documents') as $a => $document){
			$a = array();
			if(array_key_exists('name',$document)){
				$a['cbrd_cb_id'] = $this->_cbrd_id;
				$a['cbrd_req_docs'] = json_encode($document);
				$a['cbrd_ref_num'] = $this->parent_ref_num . '-' . $ctr;
				// $a['cbrd_assigned_date'] = date('Y-m-d');
				$a['cbrd_status'] = $this->_cbrd_status;
				$a['cbrd_date_filed'] = Input::get('cb_date_filed');
				$b[] = $a;
				unset($a);
				
				$ctr++;
			}
		}
		if(count($b) > 0) $this->insert($b);
			
	}
	
	public function getRequestsMy()
	{
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                 // ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'New')
				 // ->whereOr('compensationbenefitsrequesteddocs.cbrd_status', '=', 'New')
				 ->where('cb_employees_id','=',Session::get("employee_id"));
        })->get();
        
		$queries = DB::getQueryLog();
		$last_query = end($queries);
		// echo "<pre>",print_r( $last_query ),"</pre>";
		
		$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
		if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cbrd_date_filed;
				$result_data["aaData"][$ctr][] = json_decode($req->cbrd_req_docs)->name;
				$result_data["aaData"][$ctr][] = $req->cbrd_status;
				if(!$req->cb_issent && $req->cbrd_status == "New"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/edit/'.$req->cbrd_ref_num).'">Edit</a>';
				}
				// elseif($req->cb_issent && $req->cbrd_status == "New"){
					// $result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>';
				// }
				elseif($req->cb_issent && $req->cbrd_status == "Returned"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/edit/'.$req->cbrd_ref_num).'">Edit</a>';
				}
				elseif($req->cb_issent && $req->cbrd_status == "For Acknowledgement"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>';
				}
				elseif($req->cb_issent && $req->cbrd_status == "Acknowledged"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/delete/'.$req->cbrd_ref_num).'">Delete</a>';
				}
				else{
					$result_data["aaData"][$ctr][] = '';
				}
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function setCbrd_cb_id($cbrd_id)
	{
		$this->_cbrd_id = $cbrd_id;
	}
	
	public function setParent_ref_num($ref_num)
	{
		$this->parent_ref_num = $ref_num;
	}
	
	public function setCbrdStatus($status)
	{
		$this->_cbrd_status = $status;
	}
	
	public function returned()
	{
		echo Input::get('cbrd_cb_id');
		die();
		$cbrd = $this->find(Input::get('cbrd_cb_id'));
		$cbrd->cbrd_status = 'Returned';
		$cbrd->cbrd_remarks = Input::get('cbrd_remarks');
		$cbrd->save();
	}
	
	
	public function getRequestForProcessing($cbrd_ref_num)
	{
		$this->_cbrd_ref_num = $cbrd_ref_num;
		// $requests = DB::table('compensationbenefits')
        // ->join('compensationbenefitsrequesteddocs', function($join)
        // {
            // $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                // ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				// ->where('compensationbenefitsrequesteddocs.cbrd_ref_num','=',$cbrd_ref_num)
				// ->where('compensationbenefitsrequesteddocs.cbrd_assigned_to','=',Session::get("employee_id"))
				
        // })->get();
		$request = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				->where('compensationbenefitsrequesteddocs.cbrd_ref_num','=',$this->_cbrd_ref_num)
				// ->where('compensationbenefits.cb_ref_num','=',$this->_cb_ref_num)
				->where('compensationbenefitsrequesteddocs.cbrd_assigned_to','=',Session::get("employee_id"));
        })->get();
		return $request;
	}
	
	public function getRequestedDocument($cbrd_id)
	{
		$cbrd = $this->where("cbrd_ref_num","=",$cbrd_id)->first();
		// echo "<pre>",print_r($cbrd),"</pre>";
		return $cbrd;
	}

	public function getRequestByCbrdRefNum($cbrd_ref_num)
	{
		$request = CompensationBenefitsRequestedDocs::with('cb')->where("cbrd_ref_num","=",$cbrd_ref_num)->first();

		return $request;
	}
	

}