<?php

class ItrRecoComments extends \Eloquent {
    
    protected $table = 'itr_reco_comments';
    
    public $incrementing = true;
    public $timestamps = false;
    
    
    public static function insert_reco_comment($reference_no, $reco_comment, $employee_id)
    {
       $comment = new ItrRecoComments;
       
       $comment->reference_no = $reference_no;
       $comment->comment = $reco_comment;
       $comment->employee_id = $employee_id;
       
       $comment->save();
    }
    
    
    //
    public static function get_reco_comment($reference_no)
    {
        
        $comment_list = ItrRecoComments::leftJoin("employees as emp", function($join){
		
			$join->on("emp.id", "=", "itr_reco_comments.employee_id");
		
		})->where("itr_reco_comments.reference_no", "=", "{$reference_no}")->get([
		
			 "itr_reco_comments.reference_no"
			,"itr_reco_comments.comment"
            ,"itr_reco_comments.ts_comment"
			,"emp.firstname"
			,"emp.lastname"
            
		]);
        
        return $comment_list;
        
    }
    
    
}