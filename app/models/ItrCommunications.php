<?php

class ItrCommunications extends \Eloquent {
    
    
    protected $table = 'itr_communications';
    public $incrementing = false;
    public $timestamps = false;
    
    
    public static function get_itr_communications($itr_no)
    { 
    
        $itr_communications = ItrCommunications::where("itr_no", "=", "{$itr_no}")->first([
            
            "request_code"
           ,"email_type"
           ,"email_account_type"
           ,"request_nature"
           ,"request_nature_oth"
           ,"temp_date_from"
           ,"temp_date_to"
           ,"transfer_from"
           ,"transfer_to"
           
        ]);
        
        return $itr_communications;
        
    }
    
    
    //
    public static function clear_communications($itr_no)
    {
        ItrCommunications::where("itr_no", "=", "{$itr_no}")->delete();
    }
     
}

// End of file