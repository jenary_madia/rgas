<?php

class ItrLogs extends \Eloquent {
    
    protected $table = 'itr_logs';
    public $incrementing = false;
    
    public $timestamps = false;
    
    //
    public static function insert_itr_logs($itr_no, $employee_id, $action)
    {
        try {
            
            $itr_logs = new ItrLogs;
            
            $itr_logs->itr_no = $itr_no;
            $itr_logs->employee_id = $employee_id;
            $itr_logs->actions = $action;
            
            $itr_logs->save();
            
        }catch(\Exception $e) {
            throw $e;
        }
        
    }
    
    
    //
    public static function get_employee_logs($itr_no)
    {
        
        $emp_logs = ItrLogs::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_logs.employee_id");
            
        })->where("itr_logs.itr_no", "=" , "{$itr_no}")->get([
             "itr_logs.employee_id"
            ,"emp.firstname"
            ,"emp.lastname"
        ]);
        
        return $emp_logs;
        
    }
 
 
}