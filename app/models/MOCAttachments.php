<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:41 PM
 */
class MOCAttachments extends \Eloquent {
    protected $table = 'moc_attachments';
    public $incrementing = true;
    public $guarded =  array('attachment_id');
    public $timestamps = false;

}