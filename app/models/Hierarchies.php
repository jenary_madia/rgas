<?php


class Hierarchies extends \Eloquent {

    
    protected $table = "hierarchies";

    protected $primaryKey = "employee_id";

    public $timestamps = false;

    
}
/* End of file */