<?php

class ItrSystemAccess extends \Eloquent {
    
    protected $table = 'itr_system_access';
    protected $primaryKey = 'system_access_id';
    
    public $incrementing = true;
    public $timestamps = false;
    
    
    //Get system access system lists and access type
    public static function get_system_access_list($itr_no)
    {
        $system_access_list = ItrSystemAccess::leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_system_access.system_code");
            
        })->where("itr_system_access.itr_no", "=", "{$itr_no}")->get(["itr_system_access.system_code","itr_system_access.access_type", "config.system_desc"]);
        
        return $system_access_list;   
    }
    
    
    
    //
    public static function clear_system_access_lists($itr_no)
    {
        ItrSystemAccess::where("itr_no", "=", "{$itr_no}")->delete();
    }
    
}