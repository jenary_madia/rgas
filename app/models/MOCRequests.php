<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:39 PM
 */
use RGAS\Modules\MOC\MOC;
class MOCRequests extends \Eloquent {
    protected $table = 'moc_requests';
    public $incrementing = true;
    public $guarded =  array('id');
    public $timestamps = false;

    public function generateRefNo()
    {
        $documentCodeToday = date('Y-m');
        $latestRefNo = MOCRequests::orderBy('transaction_code','DESC')
            ->limit(1)
            ->first()['transaction_code'];

        if ($latestRefNo) {
            $latestDocumentCode = explode('-',$latestRefNo)[0].'-'.explode('-',$latestRefNo)[1];
            $refno = explode('-',$latestRefNo)[2];
            if ($latestDocumentCode >= $documentCodeToday)
            {
                return $latestDocumentCode.'-'.str_pad($refno + 1, 5, '0', STR_PAD_LEFT);
            }
        }

        return $documentCodeToday.'-'.str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

    }

    public static function search() {
        $company = Input::get('company');
        $department = Input::get('department');
        $status = Input::get('status');
        $action = Input::get('action');
        $submittedMOC = MOCRequests::whereIn('status',['FOR ASSESSMENT','FOR ACKNOWLEDGEMENT'])
            ->with('owner')
            ->with('attachments')
            ->with('ccodes')
            ->with('comments')
            ->with('rcodes')
            ->with('signatories')
            ->with('current_emp');

        if (Session::get("company") != 'RBC-CORP') {
            $submittedMOC->where('requested_by', '!=', Session::get('employee_id'));
        }

        if (Session::get('dept_id') == SSMD_DEPT_ID && Session::get('desig_level') == 'head') {
            $submittedMOC->where('curr_emp', '=', Session::get('employee_id'));
        }elseif (Session::get('dept_id') == CSMD_DEPT_ID) {
            $submittedMOC->where('curr_emp', '!=', (new MOC)->getSSMD('head'));
        }

        if($company) {
            $submittedMOC->where('company',$company);
        }

        if($department) {
            $submittedMOC->where('dept_sec','like',"%$department%");
        }
        if($action == 'generate') {
            if($status) {
                $submittedMOC->where('status',$status);
            }

            if(Input::has('from')) {
                if(Input::has('to')) {
                    $submittedMOC->whereBetween('date',[Input::get('from'),Input::get('to')]);
                }else{
                    $submittedMOC->where('date',Input::get('from'));
                }
            }
        }

        return $submittedMOC->get();
    }
    /*************************JOIN WITH OTHER TABLE*****************************/

    public function owner(){
        return $this->hasOne('Employees','id','requested_by')
            ->with('department');
    }

    public function current_emp(){
        return $this->hasOne('Employees','id','curr_emp');
    }

    public function attachments(){
        return $this->hasMany('MOCAttachments','request_id','request_id');
    }

    public function ccodes(){
        return $this->hasMany('MOCChangeInCodes','request_id','request_id');
    }

    public function comments(){
        return $this->hasMany('MOCComments','request_id','request_id')
            ->with("employee");
    }

    public function rcodes(){
        return $this->hasMany('MOCRequestCodes','request_id','request_id');
    }

    public function signatories(){
        return $this->hasMany('MOCSignatories','moc_request_id','request_id')->with("employee");
    }

    public function departments(){
        return $this->hasMany('MOCSignatories','moc_request_id','request_id');
    }
}