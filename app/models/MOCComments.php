<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:42 PM
 */
class MOCComments extends \Eloquent {
    protected $table = 'moc_comments';
    public $incrementing = true;
    public $timestamps = false;
    protected $fillable = ['request_id', 'employee_id', 'comment'];

    public function employee(){
        return $this->hasOne('Employees','id','employee_id');
    }
}