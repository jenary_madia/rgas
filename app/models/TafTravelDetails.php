<?php


class TafTravelDetails extends \Eloquent {

    
	 protected $table = 'taf_travel_details';
	 protected $primaryKey = 'travel_details_id';
	 
	 public $incrementing = true;
	 public $timestamps = false;
	
	
	
	//
	public static function get_travel_details($taf_no)
	{
		
		$travel_details = TafTravelDetails::where("taf_travel_details.taf_no", "=", "{$taf_no}")->get([
		
			 "taf_travel_details.travel_details_id"
			,"taf_travel_details.taf_no"
			,"taf_travel_details.origin_date"
			,"taf_travel_details.origin_time_hour"
			,"taf_travel_details.origin_time_minute"
			,"taf_travel_details.origin_meridiem"
			,"taf_travel_details.origin_city"
			,"taf_travel_details.destination_date"
			,"taf_travel_details.destination_time_hour"
			,"taf_travel_details.destination_time_minute"
			,"taf_travel_details.destination_meridiem"
			,"taf_travel_details.destination_city"
		
		]);

		
		return $travel_details;
		
	}
	
}
/* End of file */