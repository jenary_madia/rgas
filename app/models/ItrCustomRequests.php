<?php


class ItrCustomRequests extends \Eloquent {
    
    protected $table = "itr_custom_request";
    
    public $incrementing = false;
    public $timestamps = false; 
    
    
    public static function get_custom_itr_details($itr_no)
    {
        
        $itr_custom = ItrCustomRequests::where("itr_no", "=", "{$itr_no}")->first([
             "itr_no"
            ,"requestor_name"
            ,"custom_comp_id"
        ]);
        
        
        return $itr_custom;
        
    }
    
    
}