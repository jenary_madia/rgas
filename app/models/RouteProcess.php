<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/6/2016
 * Time: 3:58 PM
 */
class RouteProcess extends \Eloquent
{
    protected $table = 'route_process';
    protected $guarded =  array('id');

    public function checkpoint($process,$module,$action,$transaction_id)
    {
        $save = RouteProcess::create(array(
            'process' => $process,
            'module' => $module,
            'action' => $action,
            'transaction_id' => $transaction_id
        ));

        if ($save) {
            return 1;
        }else{
            return 0;
        }

    }

    public function updateCheckPoint($process,$action,$transaction_id,$module) {
        $routeProcess = RouteProcess::where('transaction_id',$transaction_id)
        ->where('module', $module);
        $routeProcess->update([
            'process' => $process,
            'action' => $action
        ]);
    }
}