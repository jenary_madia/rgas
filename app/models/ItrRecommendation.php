<?php

class ItrRecommendation extends \Eloquent {
    
    protected $table = 'itr_recommendation';
    protected $primaryKey = 'reference_no';
    public $incrementing = false;
    public $timestamps = false;
    
    
    
    public static function check_reco($itr_no)
    {
        
        $reco = ItrRecommendation::where("itr_no", "=", "{$itr_no}")->first();
        
        if ( !empty($reco) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    
    //
    public static function get_reco($itr_no)
    {
        
        $reco = ItrRecommendation::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_recommendation.reco_to");    
            
        })->leftJoin("employees as emp2", function($join){
            
            $join->on("emp2.id", "=", "itr_recommendation.cc_to");    
            
        })->leftJoin("employees as emp3", function($join){
            
            $join->on("emp3.id", "=", "itr_recommendation.created_by");
            
        })->where("itr_recommendation.itr_no", "=", "{$itr_no}")->get([
        
           "itr_recommendation.reference_no"
          ,"itr_recommendation.remarks"
          ,"itr_recommendation.itr_no"
          ,"itr_recommendation.reco_to"
          ,"itr_recommendation.cc_to"
          ,"itr_recommendation.subject"
          ,"itr_recommendation.created_by"
          ,"itr_recommendation.created_date"
          ,"itr_recommendation.status"
          ,"itr_recommendation.curr_emp"
          ,"emp.id as reco_to_id"
          ,"emp.firstname as reco_to_firstname"
          ,"emp.middlename as reco_to_middlename"
          ,"emp.lastname as reco_to_lastname"
          ,"emp2.id as cc_to_id"
          ,"emp2.firstname as cc_to_firstname"
          ,"emp2.middlename as cc_to_middlename"
          ,"emp2.lastname as cc_to_lastname"
          ,"emp3.firstname as created_by_firstname"
          ,"emp3.middlename as created_by_middlename"
          ,"emp3.lastname as created_by_lastname"
          
        ]);
        
        return $reco;
        
    }
    
    
    //
    public static function get_reco_pdf($itr_no, $reference_no)
    {
        
        $reco = ItrRecommendation::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_recommendation.reco_to");    
            
        })->leftJoin("employees as emp2", function($join){
            
            $join->on("emp2.id", "=", "itr_recommendation.cc_to");    
            
        })->leftJoin("employees as emp3", function($join){
            
            $join->on("emp3.id", "=", "itr_recommendation.created_by");
            
        })->where("itr_recommendation.itr_no", "=", "{$itr_no}")->where("itr_recommendation.reference_no", "=", "{$reference_no}")->first([
        
           "itr_recommendation.reference_no"
          ,"itr_recommendation.remarks"
          ,"itr_recommendation.itr_no"
          ,"itr_recommendation.reco_to"
          ,"itr_recommendation.cc_to"
          ,"itr_recommendation.subject"
          ,"itr_recommendation.created_by"
          ,"itr_recommendation.created_date"
          ,"itr_recommendation.status"
          ,"itr_recommendation.curr_emp"
          ,"emp.id as reco_to_id"
          ,"emp.firstname as reco_to_firstname"
          ,"emp.middlename as reco_to_middlename"
          ,"emp.lastname as reco_to_lastname"
          ,"emp.designation as reco_to_designation"
          ,"emp2.id as cc_to_id"
          ,"emp2.firstname as cc_to_firstname"
          ,"emp2.middlename as cc_to_middlename"
          ,"emp2.lastname as cc_to_lastname"
          ,"emp2.designation as cc_to_designation"
          ,"emp3.firstname as created_by_firstname"
          ,"emp3.middlename as created_by_middlename"
          ,"emp3.lastname as created_by_lastname"
          
        ]);
        
        
        return $reco;
        
    }
    
    
}