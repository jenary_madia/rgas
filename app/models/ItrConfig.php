<?php

class ItrConfig extends \Eloquent {
    
    protected $table = 'itr_system_config';
    public $incrementing = false;
    public $timestamps = false;
    
    
    public static function get_system_desc($system_code)
    {
        $system_desc = "";
        $system_config = ItrConfig::where("system_code", "=", "{$system_code}")->first(["system_desc"]);
        
        if ( count($system_config) > 0 )
        {
            $system_desc = $system_config->system_desc;
        }
        
        return $system_desc;
        
    }
    
    //
    public static function get_request_type_option($request_type_val, $employee_id = "")
    {
        
        if ($employee_id != "" AND $request_type_val == "AD")
        {   
    
            $is_webdis_personnel = Receivers::is_webdis_personnel($employee_id);
            
            if($is_webdis_personnel)
            {
                $request_type_options = ItrConfig::where("system_type", "=", "RF")->where("system_grp", "=", "{$request_type_val}")->orWhere("for_webdis", "=", "1")->orderBy("for_webdis", "asc")->orderBy("system_desc", "asc")->get([
                     'system_code'
                    ,'system_desc'
                ]);
            }
            else
            {
                $request_type_options = ItrConfig::where("system_type", "=", "RF")->where("system_grp", "=", "{$request_type_val}")->where("for_webdis", "=", "0")->orderBy("system_desc", "asc")->get([
                     'system_code'
                    ,'system_desc'
                ]);    
            }
            
        }
        else
        {
             $request_type_options = ItrConfig::where("system_type", "=", "RF")->where("system_grp", "=", "{$request_type_val}")->where("for_webdis", "=", "0")->orderBy("system_desc", "asc")->get([
                 'system_code'
                ,'system_desc'
            ]);   
        }
        
        return $request_type_options;
        
    }
    
    
    //
    public static function get_application_lists($system_grp)
    {
        $application_lists = ItrConfig::where("system_type", "=", "AP")->where("system_grp", "=", "{$system_grp}")->orderBy("system_desc", "asc")->get([
            'system_code'
           ,'system_desc'
           ,'application_dd'
        ]);
        
        return $application_lists;
    }
 

    //
    public static function get_itr_status()
    {
        
          if ( Cache::has('itr_status') ) {
              
            $itr_status = Cache::get('itr_status');
            
          }
          else {
     
            $itr_status = ItrConfig::where("system_type", "=", "ST")->orderBy("system_desc", "asc")->get([
                 "system_code"
                ,"system_grp"
                ,"system_desc"
            ]);
            
            Cache::add('itr_status', $itr_status, 1440); //24 hours
        }
        
        return $itr_status;
        
    }
    
    
    //
    public static function get_itr_status_request_type($request_type)
    {
        
        if ($request_type == "AD")
        {
            $itr_status = ItrConfig::where("system_type", "=", "ST")->whereIn("system_grp", array("AD", "UA", "US"))->orderBy("system_desc", "asc")->get(["system_code","system_grp","system_desc"]);
            
        }
        else if ($request_type == "TS")
        {
            $itr_status = ItrConfig::where("system_type", "=", "ST")->whereIn("system_grp", array("TS", "UT", "US"))->orderBy("system_desc", "asc")->get(["system_code","system_grp","system_desc"]);
        }
        else if ($request_type == "CF")
        {
            $itr_status = ItrConfig::where("system_type", "=", "ST")->whereIn("system_grp", array("UT", "UA", "US"))->orderBy("system_desc", "asc")->get(["system_code","system_grp","system_desc"]);
        }
        
        
        return $itr_status;
        
    }

}
//End of file.