<?php

class Sections extends \Eloquent {
    
    protected $table = 'sections';
    public $incrementing = false;
    public $timestamps = false;

    //for MRF
    public function employees() {
        return $this->hasMany('Employees','id','sectionid');
    }
    
}