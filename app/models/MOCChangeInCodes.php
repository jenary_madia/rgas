<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:42 PM
 */
class MOCChangeInCodes extends \Eloquent {
    protected $table = 'moc_changeincodes';
    public $incrementing = true;
    public $guarded =  array('attachment_id');
    public $timestamps = false;

}