<?php


class Employees extends \Eloquent {

    
    protected $table = "employees";

    protected $primaryKey = "id";
    protected $hidden = array("password");

    public $timestamps = false;


    //Get all active employees
    public static function get_employees()
    {
        
        if (Cache::has("employee_lists") )
        {
            $employee_lists = Cache::get("employee_lists");
        }
        else
        {
            $employee_lists = Employees::where("active", "=", "1")->orderBy("firstname", "asc")->get([
                 "id"
                ,"firstname"
                ,"middlename"
                ,"lastname"
            ]);
             
            Cache::forever("employee_lists", $employee_lists);
        }
        
        return $employee_lists;
        
    }
    
    
    //Get all employees for admin
    public static function get_all_employees($where_condition, $order_by_col, $order_by_col2, $order_by_sort)
    {
        
        if (Cache::has("employee_lists") )
        {
            $employee_lists = Cache::get("employee_lists_all");
        }
        else
        {
            $employee_lists = Employees::whereRaw("{$where_condition}")->orderBy("{$order_by_col}", "{$order_by_sort}")->orderBy("{$order_by_col2}", "{$order_by_sort}")->leftJoin("companylists as comp", function($join){
                
                $join->on("comp.comp_code", "=", "employees.company");
                
            })->leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "employees.departmentid");
                
            })->orderBy("firstname", "asc")->get([
                 "employees.id"
                ,"employees.firstname"
                ,"employees.middlename"
                ,"employees.lastname"
                ,"employees.active"
                ,"employees.designation"
                ,"comp.comp_code"
                ,"comp.comp_name"
                ,"dept.id as dept_id"
                ,"dept.dept_name"
            ]);
             
            Cache::forever("employee_lists_all", $employee_lists);
        }
        
        return $employee_lists;
        
    }
     
     
    //For pop up
    public static function verify_user($tk)
    {
      
        Session::flush();
        $sess = SessionToken::leftJoin("employees as emp", function($join){
             
            $join->on("emp.id", "=", "session_token.emp_id");
             
        })->where("session_token.session_token", "=", "{$tk}")->first(["session_token.session_token", "session_token.emp_id", "emp.username", "emp.password"]);
         
        $result_arr = [];   
         
        if(!empty($sess))
        {
            $emp_id = $sess->emp_id;
            
            $emp = Employees::leftJoin("departments as dept", function($join){
		 
			$join->on("employees.departmentid", "=", "dept.id");
			
            })->leftJoin("sections as sect", function($join){
                
                $join->on("employees.sectionid", "=", "sect.id");            
                 
            })->where("employees.username", "=", $sess->username)->where("employees.password","=", $sess->password)->where("employees.active", "<>", "0")->first([
             
                 "employees.id as employee_id"
                ,"employees.employeeid"
                ,"employees.new_employeeid"
                ,"employees.firstname"
                ,"employees.middlename"
                ,"employees.lastname"
                ,"employees.desig_level"
                ,"employees.company"
                ,"employees.designation"
                ,"employees.superiorid"
                ,"dept.dept_name"
                ,"dept.dept_type"
                ,"dept.dept_code"
                ,"dept.id as dept_id"
                ,"sect.sect_name"
                
            ]);
            
            
            if(!empty($emp))
            {
                
                Session::put("logged_in", TRUE);
                Session::put("employee_id", $emp->employee_id);
                Session::put("employeeid", $emp->employeeid);
                Session::put("new_employeeid", $emp->new_employeeid);
                $employee_name = $emp->firstname . " " . ((!empty($emp->middlename)) ? $emp->middlename . ". " : "") . $emp->lastname;
                Session::put("employee_name", $employee_name);
                Session::put("full_name", $emp->firstname." ".$emp->lastname);
                Session::put("firstname", $emp->firstname);
                Session::put("lastname", $emp->lastname);
                Session::put("desig_level", $emp->desig_level);
                Session::put("company", $emp->company);
                Session::put("designation", $emp->designation);
                Session::put("superiorid", $emp->superiorid);
                Session::put("dept_name", $emp->dept_name);
                Session::put("dept_type", $emp->dept_type);
                Session::put("dept_code", $emp->dept_code);
                Session::put("dept_id", $emp->dept_id);
                Session::put("sect_name", $emp->sect_name);
                
                $is_taf_receiver = Receivers::is_taf_receiver($emp->employee_id);
                Session::put("is_taf_receiver", $is_taf_receiver);
                
                
                $is_itr_bsa = Receivers::is_itr_bsa($emp->employee_id);
                Session::put('is_itr_bsa', $is_itr_bsa);
                
                $is_citd_employee = ItrRequests::check_citd_employee($emp->employee_id);
                Session::put('is_citd_employee', $is_citd_employee);
                
                $is_system_satellite = ItrRequests::check_satellite_systems($emp->employee_id);
                Session::put('is_system_satellite', $is_system_satellite);
            
                # Set if Vietnam
                $vietnam_depts = array('VN-ADMIN', 'VN-EXEC', 'VN-SALES');
                Session::put('vietnam_depts', $vietnam_depts);
                if (in_array($emp->dept_code, $vietnam_depts))
                {
                    Session::put('isvietnam', true);
                }
                
                $result_arr["logged_in"] = TRUE;
            }
            else
            {
                Session::put("logged_in", FALSE);
                $result_arr["logged_in"] = FALSE;
            }
            
         }
         else
         {
            Session::put("logged_in", FALSE);
            $result_arr["logged_in"] = FALSE;
         }
         
         
        return $result_arr;
    }
	
    public static function get ($id)
    {
        $emp = Employees::leftJoin("departments as dept", function($join){
            $join->on("employees.departmentid", "=", "dept.id");        
        })->leftJoin("sections as sect", function($join){
            $join->on("employees.sectionid", "=", "sect.id");       
        })->where("employees.id", "=", "{$id}")->first([
            "employees.id"
           ,"employees.employeeid"
           ,"employees.new_employeeid"
           ,"employees.firstname"
           ,"employees.middlename"
           ,"employees.lastname"
           ,"employees.designation"
           ,"employees.superiorid"
           ,"employees.departmentid"
           ,"employees.departmentid2"
           ,"employees.company"
           ,"employees.desig_level"
           ,"employees.onleave"
           ,"employees.attendanceid"
           ,"employees.artapprovalid"
           ,"employees.msrapprovalid"
           ,"employees.presassignedrsmrf1"
           ,"employees.presassignedrsmrf1"
           ,"employees.active"
           ,"dept.dept_name"
           ,"sect.sect_name"
        ]);
        
        return $emp;
    }
    
    public static function get_superiors($employee_id, &$array, $designation = false)
    {
        $emp = Employees::get($employee_id);
        
        Employees::_get_superiors($employee_id, $array, $designation);
        
        return $array;
    }
    
    private static function _get_superiors($employee_id, &$array, $designation)
    {
        $emp = Employees::where("employees.id", "=", "{$employee_id}")->first([
            "employees.superiorid"
        ]);
        
        $sup = Employees::where("employees.id", "=", "{$emp->superiorid}")->first([
            "employees.id"
           ,"employees.firstname"
           ,"employees.middlename"
           ,"employees.lastname"
           ,"employees.desig_level"
        ]);
        
        if (!empty($sup) && intval($sup->id) !== 0)
        {
            $superior_id = intval($sup->id);
            
            $array[$superior_id] = $sup->firstname . " " . $sup->lastname . (($designation) ? " (" . $sup->desig_level . ")" : "");
            
            if ($superior_id !== 0)
            {
                Employees::_get_superiors($superior_id, $array, $designation);
            }
        }
    }
    
    public static function get_email_add($employee_id)
    {
    
        $email_add = "";
        
        $emp = new Employees;
        $emp_query = $emp->where("employees.id", "=", "{$employee_id}")->first(["employees.email"]);
        
        $email_add = $emp_query->email;
        
        return $email_add ;
    }
    
    //Get other superiors
    public static function get_other_superiors($employee_id, $department_id)
    {
        $superior_lists = Employees::where("id", "!=", $employee_id)
                                   ->where("departmentid", "=", $department_id)
                                   ->where("onleave", "=", "0")
                                   ->where("active", "=", "1")
                                   ->where("desig_level", "=", "supervisor")
                                   ->orderBy("firstname", "asc")->get([
                         "id"
                        ,"firstname"
                        ,"middlename"
                        ,"lastname"
                    ]);
        
        return $superior_lists;
        
    }
    
    
    public static function get_all($departmentid)
    {
        $emp = Employees::Join("employees as e2", function($join){
            $join->on("employees.superiorid", "=", "e2.id");   
        })->where("employees.departmentid", "=", "{$departmentid}")
          ->where("employees.active", "!=", "0")
          ->where("employees.new_employeeid", "!=", "")
          ->whereNotNull("employees.new_employeeid")->get([
            "employees.superiorid"
           ,"employees.id"
           ,"employees.departmentid"
           ,"employees.departmentid2"
		   ,"employees.designation" // take note of this. added field. lucilleteves <= search key
           ,DB::raw("CONCAT(employees.firstname, ' ', employees.lastname) AS name")
           ,DB::raw("CONCAT(e2.firstname, ' ', e2.lastname) AS superior")
           ,"e2.desig_level"
        ]);
        
		return $emp;
    }
    
    public static function get_department_head($deptId){
        
        $superior_lists = Employees::where("departmentid", "=", $deptId)
                                   ->where("desig_level", "=", "head")->first([
                                                                                 "id"
                                                                                ,"firstname"
                                                                                ,"lastname"
                                                                                ,"desig_level"
                                                                            ]);
        return $superior_lists;
    }

    /*----------Usually used for getting immediate superior---------*/ 
    public static function getEmployeeData($id)
    {
        $data = Employees::find($id);

        if ($data) 
        {
            return $data->firstname.' '.$data->middlename.' '.$data->lastname;
        }else{
            return 'Employee not specified';
        }
    }

    public static function getByDesigLevelOnDept($employee_id,$desigLevel, $department_id)
    {
        $superior_lists = Employees::where("id", "!=", $employee_id)
                                   ->where("departmentid", "=", $department_id)
                                   ->where("onleave", "=", "0")
                                   ->where("active", "=", "1")
                                   ->whereIn("desig_level",$desigLevel)
                                   ->orderBy("firstname", "asc")->get([
                         "id"
                        ,"firstname"
                        ,"middlename"
                        ,"lastname"
                        ,"desig_level"
                    ]);
        
        return $superior_lists;
        
    }

    public static function getByPositionOnCompany($desig_level = null)
    {
        if(is_null($desig_level))
        {
            $desig_level = ['vpo','plant-aom','om'];
        }
        $employees =  Employees::where("company",Session::get("company"));
        if (is_array($desig_level))
        {
            $employees->whereIn("desig_level",$desig_level);
        }else{
            $employees->where("desig_level",$desig_level);
        }

        return $employees->first();
    }

    //for MOC
    public function department() {
        return $this->hasOne('Departments','id','departmentid');
    }

}
/* End of file */