<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 21/09/2016
 * Time: 9:46 AM
 */
class ManpowerService extends \Eloquent
{

    protected $table = 'manpowerservices';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;

    public function generateRefNo()
    {
        $documentCodeToday = date('Y-m');
        $latestRefNo = ManpowerService::orderBy('reference_no','DESC')
        ->limit(1)
        ->first()['reference_no'];

        if ($latestRefNo) {
            $latestDocumentCode = explode('-',$latestRefNo)[0].'-'.explode('-',$latestRefNo)[1];
            $refno = explode('-',$latestRefNo)[2];
            if ($latestDocumentCode >= $documentCodeToday)
            {
                return $latestDocumentCode.'-'.str_pad($refno + 1, 5, '0', STR_PAD_LEFT);
            }
        }
        
        return $documentCodeToday.'-'.str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

    }

    /**********************RECEIVER SEARCH*****************************/
    public function filterMSRForReceiver($MSRType,$dept_id) {
        $hrLeaves = ManpowerService::where('curr_emp', Session::get('employee_id'))
            ->where('isdeleted', 0)
            ->whereIn('status',['FOR PROCESSING','PROCESSED'])
            ->with('owner')
            ->forReceiver($dept_id)
            ->get();
        $leaveData = [];
        if ( count($hrLeaves) > 0){
            $ctr = 0;
            if($MSRType == 1) {
                $middleColumn = 'reason';
            }else{
                $middleColumn = 'projectname';
            }
            foreach($hrLeaves as $req) {
                $leaveData[$ctr][] = $req['reference_no'];
                $leaveData[$ctr][] = $req['datefiled'];
                $leaveData[$ctr][] = $req[$middleColumn];
                $leaveData[$ctr][] = $req['status'];
                $leaveData[$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                if($MSRType == 1) {
                    $leaveData[$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/msr/submitted/'.$req['id'].'/view').">View</a><a ".($req['status'] == 'PROCESSED' ? 'disabled' : '')." style='margin-left:3px' class='btn btnDataTables btn-xs btn-default' href=".url('/msr/submitted/'.$req->id.'/process').">Process</a>";
                }else{
                    $leaveData[$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/msr/smis/submitted/'.$req['id'].'/view').">View</a><a ".($req['status'] == 'PROCESSED' ? 'disabled' : '')." style='margin-left:3px' class='btn btnDataTables btn-xs btn-default' href=".url('/msr/smis/submitted/'.$req->id.'/process').">Process</a>";
                }
                $ctr++;
            }
        }
        return $leaveData;
    }

    public function scopeForReceiver($query,$dept_id) {
        if (Input::has("department")) {
            if(Input::get("department") != 0) {
                $query->where("departmentid",Input::get("department"));
            }else{
                $query->whereIn('departmentid',$dept_id);
            }
        }
        if(Input::get("action") == 'generate') {
            if (Input::has("status")) {
                if(Input::get("status") == "ALL") {
                    $query->whereIn('status',['FOR PROCESSING','PROCESSED']);
                }else{
                    $query->where("status",Input::get("status"));
                }
            }

            if (Input::has("from")) {
                if(Input::has("to")) {
                    $query->whereBetween("datefiled",[Input::get("from"),Input::get("to")]);
                }else{
                    $query->where("datefiled",Input::get("from"));
                }
            }
        }
    }
/************************FOR PRINT**********************************/
    public function printMSRForReceiver($MSRType,$dept_id) {
        if($MSRType == 1) {
            $req = 'requirements';
        }else{
            $req = 'projectRequirements';
        }
        $hrLeaves = ManpowerService::where('curr_emp', Session::get('employee_id'))
            ->where('isdeleted', 0)
            ->with('owner')
            ->with('department')
            ->with('section')
            ->with('agencies')
            ->with($req)
            ->with('signatories')
            ->forReceiver($dept_id)
            ->get();
        return $hrLeaves;
    }
    /*************************JOIN WITH OTHER TABLE*****************************/

    public function owner(){
        return $this->hasOne('Employees','id','employeeid');
    }

    public function current_emp(){
        return $this->hasOne('Employees','id','curr_emp');
    }

    public function section(){
        return $this->hasOne('Sections','id','sectionid');
    }

    public function department(){
        return $this->hasOne('Departments','id','departmentid');
    }


    public function attachments(){
        return $this->hasMany('ManpowerserviceAttachments','mrf_id','id');
    }
    

    public function agencies(){
        return $this->hasMany('ManpowerAgency','manpowerserviceid','id')->select([
            'agency',
            DB::raw(
                'agency_info as contactInfo'
            ),
            'manpowerserviceid'
        ]);
    }

    public function requirements(){
        return $this->hasMany('NPMDRequirements','manpowerserviceid','id')->select([
            'area',
            DB::raw('
                personno as noOfPersonnel,
                workingdays as noOfWorkingDays,
                jobtitle as jobTitle,
                dateneeded as dateNeeded
            '),
            'manpowerserviceid'
        ]);
    }

    public function projectRequirements(){
        return $this->hasMany('ProjectRequirements','manpowerserviceid','id')->with('projectDesc');
    }
    
    public function signatories(){
        return $this->hasMany('ManpowerSignatories','manpowerserviceid','id')
            ->with('employee')
            ->orderBy('id','ASC')
//            ->groupBy('signature_type')
            ->select(['signature_type','manpowerserviceid','approval_date','employee_id']);
    }


}