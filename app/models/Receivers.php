<?php


class Receivers extends \Eloquent {


	protected $table = "receivers";
	public $incrementing = true;
	 
	public $timestamps = false;
	
	// For TE (temporary)
	
	public static function isHead($employeeid) //for head
	{
		$isHead = Employees::where("desig_level","=","head")
				->where("employeeid","=",$employeeid)
				->first();
		if($isHead) return true;
		return false;
	}
	
	public static function isCHRDTrainee($employeeid) //for CHRD Assessment
	{
		$isCHRDEmp = Employees::where("desig_level","=","employee")
				->where("departmentid2","=","CHRD")
				->where("employeeid","=",$employeeid)
				->first();
		if($isCHRDEmp) return true;
		return false;
	}
	
	// public static function isChrdHead($employeeid) //for CHRD Head
	// {
		// $isHeadHr = Employees::where("desig_level","=","head")
					// ->where("departmentid2","=","CHRD")
					// ->where("employeeid","=",$employeeid)
					// ->first();
		// if($isHeadHr)
			// return true;
		// return false;
	// }
	
	public static function isVpForChrd($employeeid) //for VP for corp services
	{
		$isChrdVp = Employees::where("designation","=","VP FOR CORP. HUMAN RESOURCE")
					->where("desig_level","=","head")
					->where("departmentid2","=","CHRD")
					->where("employeeid","=",$employeeid)
					->first();
		if($isChrdVp)
			return true;
		return false;
		//return $isChrdVp;
	}
	
	public static function isSvpForCorpServ($employeeid) //for SVP for corp services
	{
		$isSvpCs = Employees::where("designation","=","SVP FOR CORP. SERVICES")
					->where("employeeid","=",$employeeid)
					->first();
		if($isSvpCs)
			return true;
		return false;
	}	
	
	public static function isPresident($employeeid) //for President
	{
		$isPres = Employees::where("designation","=","PRESIDENT")
					->where("employeeid","=",$employeeid)
					->first();
		if($isPres)
			return true;
		return false; 
	}
	
	//For CBR
    public static function get_cbr_staff()
    {
        $cbr_list = Receivers::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.module", "=", "cbr")->where("emp.active", "=", 1)->orderBy("emp.firstname", "asc")->get([
        
             "emp.id"
            ,"emp.firstname"
            ,"emp.lastname"
        
        ]);
        
		return $cbr_list;
       
    }
	
	public static function is_cbr_receiver($employee_id)
    {
        
        $cbr_receiver = Receivers::where("code", "=", "CBR_RECEIVER")->where("module", "=", "cbr")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
		if(!empty($cbr_receiver))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
	
	public static function is_cbr_staff($employee_id)
    {
        
        $cbr_staff = Receivers::where("code", "=", "CBR_STAFF")->where("module", "=", "cbr")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
		if(!empty($cbr_staff))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
	
	//For TAF
	public static function is_taf_receiver($employee_id)
	{
        
		$taf_receiver = Receivers::where("employeeid", "=", "{$employee_id}")->where(function ($query){
	 
			$query->where("description", "=", "TAF-ACT")->orWhere("description", "=", "TAF-FNC");
			
		})->get(["employeeid"]);
		
		//
		if($taf_receiver->count() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}
    
    
    //For ITr
    public static function is_itr_bsa($employee_id)
    {
        
        $itr_bsa = Receivers::where("code", "=", "MOC-BSA")->where("module", "=", "moc")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
		if(!empty($itr_bsa))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
    
    
    //For ITR
    public static function is_webdis_personnel($employee_id)
    {
        
        $itr_webdis_receiver = Receivers::where("code", "=", "ITR-WEBDIS")->where("employeeid", "=", "{$employee_id}")->get(['employeeid']);
        
		if($itr_webdis_receiver->count() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
    
    
    //For ITR
    public static function get_user_support_primary()
    {
        
        $user_support = Receivers::where("code", "=", "ITR-USS")->where("value", "=", "pr")->first(["employeeid"]);
        
        $emp_id = $user_support->employeeid;
        
        return $emp_id;
    }
 
 
    //For ITR
    public static function get_user_support()
    {
        $user_support = Receivers::where("code", "=", "ITR-USS")->where("module", "=", "itr")->first(["employeeid"]);   
        return $user_support;  
    }
    
    
    //For ITR
    public static function get_manager($request_type)
    {
        if ($request_type == "AD")
        {
            $manager = Receivers::where("code", "=", "ITR-MAN")->where("module", "=", "itr")->where("title", "=" ,"Application Manager")->get(["employeeid"]);
        }
        else if ($request_type == "TS")
        {
            $manager = Receivers::where("code", "=", "ITR-MAN")->where("module", "=", "itr")->where("title", "=" ,"Technical Manager")->get(["employeeid"]);
        }
        return $manager;
    }
    
    
    //For ITR
    public static function check_manager($employee_id)
    {
        $manager = Receivers::where("code", "=", "ITR-MAN")->where("module", "=", "itr")->where("employeeid", "=", "{$employee_id}")->first(["employeeid", "title"]);
        
        return $manager;
    }
    
    
    //For ITR
    public static function get_csmd_adh_id()
    {
        $csmd_adh = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.module", "=" ,"moc")->where("receivers.code", "=", "MOC-ADH")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $csmd_adh;
    }
    
    //for ITR
    public static function get_avp_it()
    {
        $avp_it = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "ITR-ADH")->where("receivers.module", "=", "itr")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $avp_it;
    }
    
    //for ITR
    public static function get_vp_it()
    {
        $vp_it = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "ITR-DH")->where("receivers.module", "=", "itr")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $vp_it;
        
    }
    
    //for ITR
    public static function get_csdvh()
    {
        $csdvh = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "ITR-CSDVH")->where("receivers.module", "=", "itr")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $csdvh;
    }
    
    
    //For ITR
    public static function get_forward_itr_receivers($filter = "")
    {
        
        if ($filter == "")
        {
            $reciever_code = ['MOC-ADH', 'MOC-SMDDDH', 'ITR-MAN', 'ITR-DH', 'ITR-ADH', 'MOC-CSDVH'];
        }
        else
        {
            $reciever_code = $filter;
        }
        
        
        $reciever = Receivers::leftJoin("employees as emp", function($join){
           
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->whereIn("receivers.module", array('itr', 'moc'))->whereIn("receivers.code", $reciever_code)->where("emp.active", "=", "1")->groupBy("receivers.employeeid")->get([
        
            "receivers.employeeid"
           ,"receivers.code"
           ,"emp.firstname"
           ,"emp.lastname"
           
        ]);
        
        return $reciever;
        
    }
    

    //For ITR
    public static function get_bsa_lists()
    {
        
        $bsa_list = Receivers::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "MOC-BSA")->where("emp.active", "=", 1)->orderBy("emp.firstname", "asc")->get([
        
             "emp.id"
            ,"emp.firstname"
            ,"emp.lastname"
        
        ]);
        
       return $bsa_list;
       
    }
    
    
    
    //For ITR
    public static function get_csmd_head()
    {
        $csmd_head = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "MOC-SMDDDH")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $csmd_head;   
    }
    
    
    //For ITR
    public static function get_satellite_systems($company)
    {
        
        $receiver_code = "";
        
        if (strtolower($company) == "rbc-sat")
        {
            $receiver_code = "ITR-SD-RBC";
        }
        else if (strtolower($company) == "sfi")
        {
            $receiver_code = "ITR-SD-SFI";
        }
        else if (strtolower($company) == "pfi")
        {
            $receiver_code = "ITR-SD-PFI";
        }
        else if (strtolower($company) == "mfc")
        {
            $receiver_code = "ITR-SD-MFC";
        }
        else if (strtolower($company) == "bbfi")
        {
            $receiver_code = "ITR-SD-BBFI";
        }
        else if (strtolower($company) == "spi")
        {
            $receiver_code = "ITR-SD-SPI";
        }
        else if (strtolower($company) == "buk")
        {
            $receiver_code = "ITR-SD-BUK";
        }
        
        $systems_rec = Receivers::where("code", "=", "{$receiver_code}")->get(["employeeid"]);
        
        return $systems_rec;
        
    }
    
    public static function get($module, $code, $company)
    {
        $receiver = Receivers::where("module", "=", "{$module}"
                            )
                            ->with("employee")
                            ->where("code", "=", "{$code}"
                            )->where("company", "=", "{$company}"
                            )->first();
                            
        return $receiver;
    }



// Added by Jenary
    public static function is_lrf_receiver($employee_id)
    {
        $is_lrf_receiver = Receivers::where("code","HR_LEAVE_RECEIVER")->where("module", "leaves")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
        if(!empty($is_lrf_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public static function is_clinic_receiver($employee_id)
    {
        $is_clinic_receiver = Receivers::where("code","CLINIC_LEAVE_RECEIVER")->where("module", "leaves")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
        if(!empty($is_clinic_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // Added by Jenary
    public static function is_notif_receiver($employee_id)
    {
        $is_lrf_receiver = Receivers::where("code","HR_NOTIFICATION_RECEIVER")->where("module", "notifications")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        //
        if(!empty($is_lrf_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function employee()
    {
        return $this->hasOne('Employees','id','employeeid')->select(array('firstname','middlename','lastname','id','email'));
    }

    public static function is_msr_receiver($employee_id)
    {
        $is_receiver = Receivers::whereIn("code",[MSR_RECEIVER_CODE,MSR_IQUEST_RECEIVER_CODE])->where("module", "manpowerservices")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        //
        if(!empty($is_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    // added for moc
    public static function is_div_head($employee_id) {
        $is_div_head = Receivers::whereIn("code",[CSMD_CORP_DIVHEAD])->where("module", "MOC")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        if(!empty($is_div_head))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public static function is_bsa_receiver($employee_id) {
        $is_bsa = Receivers::whereIn("code",[CSMD_BSA])->where("module", "MOC")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        if(!empty($is_bsa))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    //end added moc
    
    //Added for MRF
    public static function mrf_is_chrd_head($employee_id)
    {
        $return = Receivers::where([
            'description' => 'HRMD Department Head',
            'employeeid' => $employee_id,
            'title' => 'Corporate MRF receiver',
            'code' => 'CORP_MRF_RECEIVER'
        ])->first();

        return $return ? TRUE : FALSE;
    }

    //JMA ADDED
     //For CLEARANCE
    public static function get_access($employee_id)
    {
        $clearance = Receivers::whereRaw("(module = 'clearancecertification' or code in('AUDIT_RECEIVER','CRT_ART_RECEIVER','SPRO_SERVICEVEHICLE_RECEIVER' , 'WH_SERVICEVEHICLE_RECEIVER','OFFICESALE_RECEIVER','OFFICESALE_RECEIVER_NON'))")
            ->where("employeeid", $employee_id)
            ->get(["code","company","value"]);


        foreach ($clearance as $key => $rs) 
        {
           switch ($rs->code) 
           {
               case 'CC_ERP_HEAD': Session::put("is_cc_epr_head", 1);break;               
               case 'CC_SSRV_HEAD':Session::put("is_cc_ssrv_head", 1);break;
               case 'CC_SMDD_HEAD':Session::put("is_cc_smdd_head", 1);break;
               case 'CC_ERP_STAFF':Session::put("is_cc_epr_staff", 1);break;
               case 'CC_SSRV_STAFF':Session::put("is_cc_ssrv_staff", 1);break;
               case 'CC_SMDD_STAFF':Session::put("is_cc_smdd_staff", 1);break;
               case 'CC_TOD1':Session::put("is_cc_training1", 1);break;
               case 'CC_TOD2':Session::put("is_cc_training2", 1);break;
               case 'CC_CBR_PERSONNEL1':Session::put("is_cc_cbr_personnel1", 1); break;
               case 'CC_CBR_PERSONNEL2': Session::put("is_cc_cbr_personnel2", 1); break;
               case 'CC_CBR_MANAGER':Session::put("is_cc_cbr_manager", 1);break;
               case 'CC_RECRUITMENT':Session::put("is_cc_recruitment", 1);break;
               case 'CC_CBR_FILER': Session::put("is_cc_cbr_filer", 1); break;
               case 'CC_CHRD_HEAD': Session::put("is_cc_chrd_head", 1); break;
               case 'CC_CSDH_HEAD': Session::put("is_cc_csdh_head", 1);break;
               case 'CC_IT_HEAD': Session::put("is_cc_it_head", 1);  break;
               case 'CC_CITD_STAFF':Session::put("is_cc_citd_staff", 1); break;
               case 'CC_CITD_HEAD':Session::put("is_cc_citd_head", 1);break;
               case 'SPRO_SERVICEVEHICLE_RECEIVER':Session::put("is_service_vehicle_receiver", 1);break;
               case 'WH_SERVICEVEHICLE_RECEIVER':Session::put("is_service_vehicle_receiver", 1);break;
               case 'OFFICESALE_RECEIVER':Session::put("is_office_receiver", $rs->company);break;
               case 'OFFICESALE_RECEIVER_NON':Session::put("is_office_receiver_non_food", $rs->company);break;
               case 'CRT_ART_RECEIVER':Session::put("is_art_receiver", $rs->value);break;
               case 'AUDIT_RECEIVER':Session::put("is_audit_receiver", 1);break;
           }
        }
 
    }

 
}
/* End of file */