<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 09/08/2016
 * Time: 2:45 PM
 */
class LeaveBatch extends \Eloquent
{
    protected $table = 'leave_batch';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;

    /*----------For leave column codenumber---------*/
    public function generateCodeNumber()
    {
        $documentCodeToday = date('Y-m');
        $latestDocumentCode = LeaveBatch::max('documentcode');
        if ($latestDocumentCode >= $documentCodeToday)
        {
            $latestCodenumber = LeaveBatch::where('documentcode','=',$latestDocumentCode)->max('codenumber');
            if (! $latestCodenumber) {
                return '00000';
            }else{
                return str_pad($latestCodenumber + 1, 5, '0', STR_PAD_LEFT);
            }
        }else{

            return str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

        }

    }

    /*--------------------------for data table of leaves of current logged in on Production Encoders-----------------------------*/
    public function myLeaves()
    {
        $requests = LeaveBatch::with("employee")
            ->where('ownerid', Session::get("employee_id"))
            ->with("department")
            ->with("section")
            ->get();
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
        $result_data["iTotalRecords"] = count($requests);
        if (count($requests) > 0) {
            $ctr = 0;
            foreach ($requests as $req) {
                $deletableNotifications = (in_array($req['status'], json_decode(DELETABLE,true)));
                $editableNotifications = (in_array($req['status'], json_decode(EDITABLE,true)));
                $btnDelete = "<button ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' name='lrfAction' value='softDelete|{$req['id']}'>Delete</button>";
                $btnView = "<a class='btn btnDataTables btn-default btn-xs' href=" . url('/lrf/pe_view_my_request/'.$req['id'].'/view').">View</a>";
                $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' href=" . url('/lrf/pe_view_my_request/'.$req['id'].'/edit') . ">Edit</a>";
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data["aaData"][$ctr][] = $req['status'];
                $result_data["aaData"][$ctr][] = ($req['employee']? $req['employee']['firstname']." ".$req['employee']['middlename']." ".$req['employee']['lastname']: "----");
                $result_data["aaData"][$ctr][] = array($btnDelete." ".$btnView." ".$btnEdit);
                $ctr++;
            }
        } else {
            $result_data["aaData"] = $requests;
        }
        return $result_data;
    }
    
    public function scopeForMyLeaves($query)
    {
        if (Input::get('sSearch'))
        {
            $skey = Input::get('sSearch');
            return $query->whereRaw("(reason like '%".$skey."%' or datecreated like '%".$skey."%' or concat(documentcode,'-',codenumber) like '%".$skey."%' or status like '%".$skey."%')");
        }
    }
    /*--------------------------for data table of for approve notifications"-----------------------------*/
    public function superiorLeaves()
    {
        $forApproveNotifications = LeaveBatch::where('curr_emp', Session::get('employee_id'))

            //        forApproveNotificationSearch() //TODO!
            ->whereIn('status', array('FOR APPROVAL','RETURNED'))
            ->with('department')
            ->with('section')
            ->get();
        $result_data["iTotalDisplayRecords"] = count($forApproveNotifications); //total count filtered query
        $result_data["iTotalRecords"] = count($forApproveNotifications);
        if ( count($forApproveNotifications) > 0){
            $ctr = 0;
            foreach($forApproveNotifications as $req) {
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data["aaData"][$ctr][] = $req['status'];
                $result_data["aaData"][$ctr][] = ($req['owner']? $req['owner']['firstname']." ".$req['owner']['middlename']." ".$req['owner']['lastname']: "----");
                $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/pe_view/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/pe_view/'.$req['id'].'/approve').">Approve</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $forApproveNotifications;
        }
        return $result_data;
    }
    
    /*--------------------------for data table of "for submitted notifications to Receiver"-----------------------------*/
    public function getLeaves()
    {
        $receiverLists = LeaveBatch::where('status', 'APPROVED')
            ->where('curr_emp', Session::get('employee_id'))
            ->get();
        return $receiverLists;
    }
    /*-------------------SEARCH PE RECEIVER ----------------*/
    public function searchPEReceiver()
    {
        $receiverLists = LeaveBatch::where('status', 'APPROVED')
            ->where('curr_emp', Session::get('employee_id'))
            ->with('owner')
            ->with('department')
            ->with('section')
            ->forReceiver()
            ->get();
        $result_data = [];
        if ( count($receiverLists) > 0){
            $ctr = 0;
            foreach($receiverLists as $req) {
                $result_data[$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data[$ctr][] = $req['datecreated'];
                $result_data[$ctr][] = $req['dateapproved'];
                $result_data[$ctr][] = $req['department']['dept_name'];
                $result_data[$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data[$ctr][] = $req['owner']['lastname'].' '.$req['owner']['firstname'].' '.$req['owner']['middlename'];
                $result_data[$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/submitted/pe_leave/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default btn-xs' href=".url('/submitted/pe_leave/'.$req['id'].'/process').">Process</a>";
                $ctr++;
            }
        }

        return $result_data;

    }

    public function scopeForReceiver($query) {
        if (Input::has("department")) {
            if(Input::get("department") != 0) {
                $query->where("departmentid",Input::get("department"));
            }
        }

        if (Input::has("section")) {
            $query->where("sectionid",Input::get("section"));
        }
        
        if (Input::has("dateFrom")) {
            if(Input::has("dateTo")) {
                $query->whereBetween("dateapproved",[Input::get("dateFrom"),Input::get("dateTo")]);
            }else{
                $query->where("dateapproved",Input::get("dateFrom"));
            }
        }

    }
    /*-------------------PRINT pe RECEIVER------------------------------*/

    public function printPEReceiver()
    {
        $receiverLists = LeaveBatch::where('status', 'APPROVED')
            ->where('curr_emp', Session::get('employee_id'))
            ->with('owner')
            ->with('department')
            ->with('section')
            ->with('batchDetails')
            ->forReceiver()
            ->get();
        
        
        return $receiverLists;

    }

    /*--------------------Join with Notification-------------------*/
    public function batchDetails()
    {
        return $this->hasMany('LeaveBatchDetails','leave_batch_id','id')->with("leaveItem");
    }

    public function department()
    {
        return $this->hasOne('Departments','id','departmentid');
    }

    public function section()
    {
        return $this->hasOne('Sections','id','sectionid');
    }

    public function employee()
    {
        return $this->hasOne('Employees','id','curr_emp')->select(array('firstname','middlename','lastname','id','email'));
    }

    public function owner()
    {
        return $this->hasOne('Employees','id','ownerid')->select(array('firstname','middlename','lastname','id'));
    }
    
}