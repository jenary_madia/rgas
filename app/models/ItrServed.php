<?php


class ItrServed extends \Eloquent {
    
    protected $table = "itr_served";
    public $incrementing = true;
    public $timestamps = false; 
    
    
    //
    public static function get_served_lists($itr_no)
    {
        
        $served_lists = ItrServed::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_served.employee_id");
            
        })->where("itr_served.itr_no", "=", "{$itr_no}")->get([
        
            "itr_served.itr_no"
           ,"itr_served.employee_id"
           ,"itr_served.ts_assigned"
           ,"itr_served.served_date"
           ,"emp.firstname"
           ,"emp.lastname"
           
        ]);
        
        return $served_lists;
        
    }
    
    
    
    public static function served_itr($itr_no, $served_id, $date)
    {
        
        $served = new ItrServed;
        
        $served->itr_no = $itr_no;
        $served->employee_id = $served_id;
        $served->served_date = $date;
        
        $served->save();
        
    }
    
    
}