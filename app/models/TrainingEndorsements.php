<?php
<<<<<<< HEAD
use app\RGAS;
=======
use RGAS\Libraries;
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb

class TrainingEndorsements extends \Eloquent {
    
	protected $primaryKey = 'te_id';
    protected $table = 'trainingendorsements';
    public $incrementing = true;
    public $timestamps = true;
	
	protected $attachments_path = '/te/attachments';
	
	public $_te_status;
	public $_te_issent;
	public $_te_level;
	
	public function tep()
	{
		return $this->hasMany('TrainingEndorsementsParticipants','tep_te_id');
	}
	
	public function __destruct()
	{
		// $queries = DB::getQueryLog();
		// print_r(end($queries));
	}
	
	public function setTeStatus($status)
	{
		$this->_te_status = $status;
	}
	
	public function setTeIssent($issent)
	{
		$this->_te_issent = $issent;
	}
	
	public function setLevel($level)
	{
		$this->_te_level = $level;
	}
<<<<<<< HEAD
	
	
	public function store()
	{
		$fu = new RGAS\FileManager;
=======

	public function store()
	{
		$fu = new Libraries\FileManager;
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
		$filenames = $fu->upload($this->attachments_path);
		
		// Collate first all the data for validation before calling the model for inserting data to table
        $this->te_employees_id = Session::get('employee_id');
        $this->te_emp_name = Session::get('employee_name');
        $this->te_emp_id = Session::get('employeeid');
		$this->te_company = Session::get('company');
		$this->te_department = Session::get('dept_name');
		$this->te_emp_designation = Session::get('designation');
<<<<<<< HEAD
		$this->te_ref_num = Input::get('te_ref_num');
=======
		//$this->te_ref_num = Input::get('te_ref_num');
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        $this->te_date_filed = Input::get('te_date_filed');
        $this->te_contact_no = Input::get('te_contact_number');
        $this->te_training_title = Input::get('te_training_title');
        $this->te_training_date = Input::get('te_training_date');
        $this->te_total_amount = Input::get('te_total_amount');
        $this->te_amount_range = Input::get('te_amount_range');
        $this->te_vendor = Input::get('te_vendor');
        $this->te_training_venue = Input::get('te_training_venue');
        $this->te_attachments = json_encode($filenames);
		if(Input::get('comment') != ""){
			$this->te_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>Input::get('comment')
					)
				)
			);
		}
		$this->te_issent = $this->_te_issent;
		$this->te_status = $this->_te_status;
		$this->te_level = $this->_te_level;
		$this->save();
		
		$tep = new TrainingEndorsementsParticipants;
		$tep->setTep_te_id($this->te_id);
		$tep->setParent_ref_num($this->te_ref_num);
		$tep->store(); 
	}
	
<<<<<<< HEAD
=======
	
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
	public static function get_companies()
    {
       if ( Cache::has("companies") )
        {
            $companies = Cache::get("companies");
        }
        else
        {
            $companies = Companies::where("active", "=", "1")->get([
                "id"
               ,"comp_code"
               ,"comp_name"
            ]);
            Cache::add("companies", $companies, 1440);
        }       
        return $companies;
    }
	
	public static function get_departments()
    {
       if ( Cache::has("departments") )
        {
            $departments = Cache::get("departments");
        }
        else
        {
            $departments = Departments::where("active", "=", "1")->get([
                "id"
               ,"dept_code"
               ,"dept_name"
            ]);
            Cache::add("departments", $departments, 1440);
        }       
        return $departments;
    }
	
	public function getRequest($te_id)
	{
		// take note of this
		$request = $this->find($te_id);
				
		return $request;
	}
	
	public function getVPforCHR()
	{
		$vpforchr = Employees::where("designation","=","VP FOR CORP. HUMAN RESOURCE")
					->first(["id","firstname", "middlename", "lastname"]);
				
		return $vpforchr;
				
	}
	
	public function getSVPforCS()
	{
		$svpforcs = Employees::where("designation","=","SVP FOR CORP. SERVICES")
					->first(["id","employeeid", "firstname", "middlename", "lastname"]);
					
		return $svpforcs;
	}
	
	public function getPres()
	{
		// take note of this
		// $request = $this->find($te_id);
				
		// return $request;
		$pres = Employees::where("designation","=","PRESIDENT")
					->first(["id","employeeid", "firstname", "middlename", "lastname"]);
					
		return $pres;
	}
	
	public function getParticipants($te_id)
	{
		$this->te_id = $te_id;
		
		$tepRecord = DB::table('trainingendorsements')
        ->join('trainingendorsementsparticipants', function($join)
        {
            $join->on('trainingendorsements.te_id', '=', 'trainingendorsementsparticipants.tep_te_id')
				 ->where('trainingendorsementsparticipants.tep_te_id',"=",$this->te_id);
        })->get();
        
		$queries = DB::getQueryLog();
		$last_query = end($queries);
		
		return $tepRecord;
	}
	
	public function getEndorsementsForAssessment()
	{
<<<<<<< HEAD
		
		$requests = $this->where('te_status','=','For Assessment')->where('te_level','=','CHRD-TOD')->get();		
=======
		$requests = $this->where('te_level','=','CHRD-TOD')->get();	
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				// $result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = $req->te_status;
<<<<<<< HEAD
				$result_data["aaData"][$ctr][] = RGASHelpers::getEmployeeNameById($req->te_current);
=======
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
				
				if($req->te_status == "For Assessment"){
					$result_data["aaData"][$ctr][] = 
						'<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id']).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/te/assess/'.$req['te_id']).'">Assess</a>';
<<<<<<< HEAD
						//<a class="btn btndefault btn-default" href="'.URL::to('/te/process/'.$req['te_ref_num'].'/'.csrf_token())	.'">Process</a>
				}
				else{
					$result_data["aaData"][$ctr][] = 
						'<a class="btn btndefault btn-default" href="'.URL::to('/te/assess/'.$req['te_id']).'">Assess</a>';
						//<a class="btn btndefault btn-default" href="'.URL::to('/te/process/'.$req['te_ref_num'].'/'.csrf_token())	.'">Process</a>
				}
				
=======
				}
				if($req->te_status == "Disapproved"){
					$result_data["aaData"][$ctr][] = 
						'<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id']).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/te/assess/'.$req['te_id']).'">Edit</a>';
				}
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsChrdVpApproval()
	{
<<<<<<< HEAD
		$requests = $this->where('te_level','=','VPforCHR')->get();		
=======
		$requests = $this->where('te_level','=','VPforCHR')->where('te_status','=','For Approval')->get();		
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = $req->te_status;
<<<<<<< HEAD
				$result_data["aaData"][$ctr][] = RGASHelpers::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] = 
				'<a class="btn btndefault btn-default" href="'.URL::to('/te/process-endorsement/'.$req['te_id']).'">Process</a>';
				
=======
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] = 
					'<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">View</a>
					<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">Process</a>';
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsSvpCsApproval()
	{
<<<<<<< HEAD
		$requests = $this->where('te_level','=','SVPforCS')->get();		
=======
		$requests = $this->where('te_level','=','SVPforCS')->where('te_status','=','For Approval')->get();		
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = $req->te_status;
<<<<<<< HEAD
				$result_data["aaData"][$ctr][] = RGASHelpers::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] = 
					'<a class="btn btndefault btn-default" href="'.URL::to('/te/process-endorsement/'.$req['te_id'])	.'">Process</a>';
					 // <a class="btn btndefault btn-default" href="'.URL::to('/te/process/'.$req['te_ref_num'].'/'.csrf_token())	.'">Process</a>';
                $ctr++;
=======
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] = 
					'<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">View</a>
					<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">Process</a>';
				$ctr++;
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsPresApproval()
	{
<<<<<<< HEAD
		$requests = $this->where('te_level','=','PRES')->get();		
=======
		$requests = $this->where('te_level','=','PRES')->where('te_status','=','For Approval')->get();		
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = $req->te_status;
<<<<<<< HEAD
				$result_data["aaData"][$ctr][] = RGASHelpers::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] = 
					'<a class="btn btndefault btn-default" href="'.URL::to('/te/process-endorsement/'.$req['te_id'])	.'">Process</a>';
					 // <a class="btn btndefault btn-default" href="'.URL::to('/te/process/'.$req['te_ref_num'].'/'.csrf_token())	.'">Process</a>';
=======
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] = 
					'<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">View</a>
					<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">Process</a>';
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsMy()
	{
<<<<<<< HEAD
		$requests = $this->where("te_employees_id","=",Session::get("employee_id"))->get();
=======
		$requests = $this->where("te_employees_id","=",Session::get("employee_id"))->where("te_isdeleted","=",0)->get();
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_training_title;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = $req->te_status;
<<<<<<< HEAD
				$result_data["aaData"][$ctr][] = RGASHelpers::getEmployeeNameById($req->te_current);
=======
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
				
					if(!$req->te_issent && $req->te_status == "New"){
						$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">View</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/edit/'.$req['te_id'])	.'">Edit</a>';
					}
					elseif($req->te_status == "For Assessment"){
						$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">View</a>';
					}
					elseif($req->te_issent && $req->te_status == "For Approval"){
						$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">View</a>';
					}
<<<<<<< HEAD
					elseif($req->te_issent && $req->te_status == "Approved"){
						$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">View</a>
=======
					elseif($req->te_status == "Approved"){
						$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/te/view-approved/'.$req['te_id'])	.'">View</a>
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
							<a class="btn btndefault btn-default" href="'.URL::to('/te/delete/'.$req['te_id']).'">Delete</a>';
					}
					elseif(!$req->te_issent && $req->te_status == "Disapproved"){
						$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">View</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/edit/'.$req['te_id']).'">Edit</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/delete/'.$req['te_id']).'">Delete</a>';
					}
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
<<<<<<< HEAD
	
=======
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
}