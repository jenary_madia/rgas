<?php


class MrfComments extends \Eloquent {

    
    protected $table = "mrf_comments";

    protected $primaryKey = "id";
    protected $hidden = array("password");
    protected $guarded = ['id'];

    public $timestamps = false;

    public function employee() {
        return $this->hasOne('Employees','id','commentor_id')->select(['id','firstname','lastname','middlename']);
    }

}