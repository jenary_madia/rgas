<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/24/2016
 * Time: 8:21 PM
 */
class ManpowerSignatories extends \Eloquent
{
    protected $table = 'msr_signatories';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;

    public function employee() {
        return $this->hasOne('Employees','id','employee_id')->select(['id','firstname','lastname','middlename']);
    }
}