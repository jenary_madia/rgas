<?php

class TafRouteLog extends \Eloquent {

    
	protected $table = 'taf_route_log';
	public $incrementing = true;
	 
	public $timestamps = false;
	 
	
	
	public static function insert_route($employee_id, $taf_id)
	{
		$taf_route = new TafRouteLog;
		$taf_route->employee_id = $employee_id;
		$taf_route->taf_id = $taf_id;
		$taf_route->save();
	}
	
	
	public static function get_taf_route($taf_id)
	{
		
		$taf_route = TafRouteLog::leftJoin("employees as emp", function($join){
		
			$join->on("emp.id", "=", "taf_route_log.employee_id");
		
		})->where("taf_route_log.taf_id", "=", "{$taf_id}")->get([
			 "taf_route_log.employee_id"
			,"emp.firstname"
			,"emp.middlename"
			,"emp.lastname"
			,"emp.desig_level"
		]);
		
		return $taf_route;
	}

}	 	 
//End of File