<?php


class ItrAttachments extends \Eloquent {

    
	 protected $table = 'itr_attachments';
	 protected $primaryKey = 'attachment_id';
	 
	 public $incrementing = true;
	 public $timestamps = false;
	
	
	//
	public static function get_attachments($itr_no)
	{
	
		$attachment_list = ItrAttachments::leftJoin("itr_requests as itr", function($join){
		
			$join->on("itr.itr_no", "=", "itr_attachments.itr_no");
			
		})->where("itr_attachments.itr_no", "=", "{$itr_no}")->get([
		
			 "itr_attachments.attachment_id"
			,"itr_attachments.itr_no"
			,"itr_attachments.fn"
			,"itr_attachments.employee_id"
			,"itr_attachments.attachment_type"
            
        ]);
	
		return $attachment_list;
        
	}
	
    
    //
    public static function remove_attachment($attachment_id)
    {
        $delete_attachment = ItrAttachments::where("attachment_id", "=", "{$attachment_id}")->delete();   
        return $delete_attachment;
    }
    
}
/* End of file */