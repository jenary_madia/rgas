<?php

class Companies extends \Eloquent {
    
    protected $table = 'companylists';
    public $incrementing = false;
    public $timestamps = false;
    
    
    public static function get_companies()
    {
        
       if ( Cache::has("companies") )
        {
            $companies = Cache::get("companies");
        }
        else
        {
            $companies = Companies::where("active", "=", "1")->get([
                "id"
               ,"comp_code"
               ,"comp_name"
            ]);
        
            Cache::add("companies", $companies, 1440);
        }
        
       
        return $companies;
        
    }
    
    
}