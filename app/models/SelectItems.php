<?php

class SelectItems extends \Eloquent {
    
    protected $table = 'selectitems';
    public $incrementing = false;
    public $timestamps = false;
    
    public function getItems($module,$modulesField){
        return SelectItems::where("module",$module)
            ->where("modulesfield",$modulesField)
            ->orderBy("text","asc")
            ->expatriates()
            ->get(['item','text']);
    }

    public function scopeExpatriates($query) {
        if(! in_array(Session::get("employee_id"),json_decode(EXPATRIATES,true))){
            $query->where('item','!=','HomeVisit');
        }
    }
    // public static function get_companies()
    // {
        
    //    if ( Cache::has("companies") )
    //     {
    //         $companies = Cache::get("companies");
    //     }
    //     else
    //     {
    //         $companies = Companies::where("active", "=", "1")->get([
    //             "id"
    //            ,"comp_code"
    //            ,"comp_name"
    //         ]);
        
    //         Cache::add("companies", $companies, 1440);
    //     }
        
       
    //     return $companies;
        
    // }

    /*********FOR MOC***********/

    public function getList($module,$modulesField){
        return SelectItems::where("module",$module)
            ->where("modulesfield",$modulesField)
            ->get(['item','text']);
    }

    public function getDesc($module,$type,$data){
        return SelectItems::where('module',$module)
            ->where('modulesfield',$type)
            ->whereIn('item',$data)
            ->get();
    }
    
    
}