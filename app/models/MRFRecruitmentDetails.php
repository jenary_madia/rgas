<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 09/03/2017
 * Time: 3:11 PM
 */
class MRFRecruitmentDetails extends \Eloquent{
    protected $table = "candidates";
    public $timestamps = false;
    
    public function insertRecruitmentDetails($MRFid,$data) {
        $toInsert = [];
        foreach($data as $rec) {
            array_push($toInsert,[
                'manpowerrequestid' => $MRFid,
                'candidate' => $rec['candidate'],
                'date_endorsed' => $rec['dateEndorsed'],
                'remarks' => $rec['remarks'],
            ]);
        }
        MRFRecruitmentDetails::where('manpowerrequestid',$MRFid)->delete();
        MRFRecruitmentDetails::insert($toInsert);
    }
    
}