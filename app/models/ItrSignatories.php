<?php

class ItrSignatories extends \Eloquent {
    
    protected $table = 'itr_reco_signatories';
    protected $primaryKey = 'sig_id';
    
    public $incrementing = true;
    public $timestamps = false;
    
    
    //
    public static function insert_signatories($reference_no, $signature_type, $signature_type_seq, $employee_id, $approval_date = "", $signed)
    {
        
        $itr_sig = new ItrSignatories;
            
        $itr_sig->reference_no = $reference_no;
        $itr_sig->signature_type = $signature_type;
        $itr_sig->signature_type_seq = $signature_type_seq;
        $itr_sig->employee_id = $employee_id;
        if ($approval_date != "")
        {
            $itr_sig->approval_date = $approval_date;
        }
        $itr_sig->signed = $signed;
        
        $itr_sig->save();
        
    }
    
    //
    public static function get_signatories($reference_no)
    {
        
        $signatories = ItrSignatories::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_reco_signatories.employee_id");
            
        })->where("reference_no", "=", "{$reference_no}")->get([
        
             "itr_reco_signatories.sig_id"
            ,"itr_reco_signatories.signature_type"
            ,"itr_reco_signatories.signature_type_seq"
            ,"itr_reco_signatories.employee_id"
            ,"itr_reco_signatories.approval_date"
            ,"itr_reco_signatories.signed"
            ,"emp.firstname"
            ,"emp.middlename"
            ,"emp.lastname"
            ,"emp.departmentid2"
        
        ]);
        
        return $signatories;
            
    }
    
    
    //
    public static function get_reco_disapprove_lists($reference_no)
    {
        
        $disapprove_lists = ItrSignatories::leftJoin("employees as emp", function($join){
         
            $join->on("emp.id", "=", "itr_reco_signatories.employee_id");
            
        })->where("itr_reco_signatories.reference_no", "=", "{$reference_no}")->where("signed", "=", 1)->get([
        
             "itr_reco_signatories.sig_id"
            ,"itr_reco_signatories.signature_type"
            ,"itr_reco_signatories.signature_type_seq"
            ,"itr_reco_signatories.employee_id"
            ,"itr_reco_signatories.approval_date"
            ,"itr_reco_signatories.signed"
            ,"emp.id"
            ,"emp.firstname"
            ,"emp.middlename"
            ,"emp.lastname"
            
        ]);
        
        return $disapprove_lists;
        
    }
    
}