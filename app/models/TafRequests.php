<?php

class TafRequests extends \Eloquent {

    
     protected $table = 'taf_requests';
     public $incrementing = true;
     
     public $timestamps = false;
     
     
    public static function get_taf_details($id)
    {
        
        $taf_requests_data = TafRequests::leftJoin("employees as emp", function($join){
        
            $join->on('emp.id', '=', 'taf_requests.requestor');     
            
        })->leftJoin("employees as emp2", function($join){
        
            $join->on('emp2.id', '=', 'taf_requests.endorsed_by');  
            
        })->leftJoin("employees as emp3", function($join){
        
            $join->on('emp3.id', '=', 'taf_requests.endorsed2_by');
            
        })->leftJoin("employees as emp4", function($join){
        
            $join->on('emp4.id', '=', 'taf_requests.endorsed3_by'); 
            
        })->leftJoin("departments as dept", function($join){
        
            $join->on('emp.departmentid', '=', 'dept.id');      
        
        })->where("taf_requests.id", "=", "{$id}")->first([
            
             'taf_requests.id'
            ,'taf_requests.taf_no'
            ,'taf_requests.requestor'
            ,'taf_requests.request_date'
            ,'taf_requests.purpose'
            ,'taf_requests.travel_type'
            ,'taf_requests.travel_advances'
            ,'taf_requests.currency'
            ,'taf_requests.currency_others'
            ,'taf_requests.date_required'
            ,'taf_requests.hotel'
            ,'taf_requests.exceptions'
            ,'taf_requests.curr_emp'
            ,'taf_requests.status'
            ,'taf_requests.status_code'
            ,'taf_requests.endorsed_by'
            ,'taf_requests.endorsed2_by'
            ,'taf_requests.endorsed3_by'
            ,'taf_requests.endorsed_date'
            ,'taf_requests.endorsed2_date'
            ,'taf_requests.endorsed3_date'
            ,'emp.firstname as emp_firstname'
            ,'emp.middlename as emp_middlename'
            ,'emp.lastname as emp_lastname'
            ,'emp.company'
            ,'emp2.firstname'
            ,'emp2.middlename'
            ,'emp2.lastname'
            ,'emp3.firstname'
            ,'emp3.middlename'
            ,'emp3.lastname'
            ,'emp4.firstname'
            ,'emp4.middlename'
            ,'emp4.lastname'
            ,'dept.id as department_id'
            ,'dept.dept_name'
            ,'dept.dept_code'
            
        ]);
        
        return $taf_requests_data;
        
    }
     
     
     
    public static function get_my_taf_request($where_condition, $order_by_col, $order_by_sort)
    {
     
         $taf_requests = TafRequests::leftJoin('employees as curr_emp', function($join){
     
            $join->on('curr_emp.id', '=', 'taf_requests.curr_emp'); 
            
         })->leftJoin('employees as fnc_curr_emp', function($join){
         
            $join->on('fnc_curr_emp.id', '=', 'taf_requests.fnc_curr_emp');
            
         })->leftJoin('taf_config as config', function($join){
         
            $join->on('config.code_desc', '=', 'taf_requests.travel_type');
            
         })->whereRaw("{$where_condition}")->orderBy($order_by_col, $order_by_sort)->get([
         
            'taf_requests.id'
           ,'taf_requests.taf_no'
           ,'taf_requests.travel_type'
           ,'taf_requests.status'
           ,'taf_requests.curr_emp'
           ,'taf_requests.fnc_curr_emp'
           ,'curr_emp.firstname as curr_emp_firstname'
           ,'curr_emp.lastname as curr_emp_lastname'
           ,'fnc_curr_emp.firstname as fnc_curr_emp_firstname'
           ,'fnc_curr_emp.lastname as fnc_curr_emp_lastname'
           ,'config.description'
            
        ]);
         
        return $taf_requests;
     
    }
    
    
    
    //
    public static function get_for_aproval_taf_request($where_condition, $order_by_col, $order_by_sort)
    {
        
        $taf_requests = TafRequests::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "taf_requests.requestor");
        
        })->whereRaw($where_condition)->orderBy($order_by_col, $order_by_sort)->get([
           "taf_requests.id as taf_id"
          ,"taf_requests.taf_no"
          ,"taf_requests.travel_type"
          ,"taf_requests.status"
          ,"taf_requests.status_code"
          ,"taf_requests.curr_emp"
          ,"emp.firstname"
          ,"emp.lastname"
        ]);
        
        return $taf_requests;
        
    }
    
    
    //
    public static function get_submitted_taf_request($where_condition, $order_by_col, $order_by_sort)
    {
    
    
        $taf_requests = TafRequests::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "taf_requests.requestor");
        
        })->leftJoin("employees as curr_emp", function($join){
        
            $join->on("curr_emp.id", "=", "taf_requests.curr_emp");
        
        })->leftJoin("employees as fnc_curr_emp", function($join){
        
            $join->on("fnc_curr_emp.id", "=", "taf_requests.fnc_curr_emp");
        
        })->leftJoin("departments as dept", function($join){
        
            $join->on("dept.id", "=", "emp.departmentid");
        
        })->whereRaw($where_condition)->orderBy($order_by_col, $order_by_sort)->get([
        
           "taf_requests.id as taf_id"
          ,"taf_requests.taf_no"
          ,"taf_requests.travel_type"
          ,"taf_requests.status"
          ,"taf_requests.status_code"
          ,"taf_requests.curr_emp"
          ,"taf_requests.fnc_curr_emp"
          ,"emp.firstname as emp_firstname"
          ,"emp.lastname as emp_lastname"
          ,"curr_emp.firstname as curr_emp_firstname"
          ,"curr_emp.lastname as curr_emp_lastname"
          ,"fnc_curr_emp.firstname as fnc_curr_emp_firstname"
          ,"fnc_curr_emp.lastname as fnc_curr_emp_lastname"
          ,"dept.id"
          
        ]);
        
        return $taf_requests;

    }
    
    
    //
    public static function get_taf_per_dept($employee_id)
    {
        
        $taf_request = TafRequests::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "taf_requests.requestor");
        
        })->leftJoin("departments as dept", function($join){
        
            $join->on("dept.id", "=", "emp.departmentid");
        
        })->where("taf_requests.curr_emp", "=", "{$employee_id}")->orWhere("taf_requests.fnc_curr_emp", "=", "{$employee_id}")->groupBy("dept.id")->get([
             DB::raw('count(taf_no) as taf_count')
           ,"dept.id as dept_id"
           ,"dept.dept_name"
        ]);

        return $taf_request;
    }
    
    
    //
    public static function get_employees_details($employee_id)
    {
        $emp = new Employees;
        
        $emp_details = $emp->leftJoin("departments as dept", function($join){
        
            $join->on("employees.departmentid", "=", "dept.id");        
            
        })->leftJoin("sections as sect", function($join){
        
            $join->on("employees.sectionid", "=", "sect.id");       
            
        })->where("employees.id", "=", "{$employee_id}")->first([
            
            "employees.id"
           ,"employees.employeeid"
           ,"employees.new_employeeid"
           ,"employees.firstname"
           ,"employees.middlename"
           ,"employees.lastname"
           ,"employees.designation"
           ,"employees.departmentid"
           ,"employees.departmentid2"
           ,"employees.company"
           ,"employees.desig_level"
           ,"dept.dept_name"
           ,"sect.sect_name"
           
        ]);
        
        return $emp_details;
        
    }
    
    
    //
    public static function get_superior_id($employee_id)
    {
    
        $superior_id = "";
        
        $emp = new Employees;
        
        $superior_id_data = $emp->leftJoin("employees as emp2", function($join){
        
            $join->on("emp2.id", "=", "employees.superiorid");      
            
        })->where("employees.id", "=", "{$employee_id}")->orderBy("employees.superiorid", "asc")->first([
            "employees.superiorid"
           ,"emp2.onleave"
           ,"emp2.artapprovalid"
        ]);
        
        if($superior_id_data->onleave != 1)
        {
            $superior_id = $superior_id_data->superiorid;
        }
        else
        {
            $superior_id = $superior_id_data->artapprovalid;
        }
        
        
        return $superior_id;
        
    }
    

    //
    public static function get_superior_id_orig($employee_id)
    {
    
        $superior_id = "";
        $emp = new Employees;
        
        $superior_id_data = $emp->where("employees.id", "=", "{$employee_id}")->orderBy("employees.superiorid", "asc")->first([
            "employees.superiorid"
        ]);
        
        if(!empty($superior_id_data))
        {
            $superior_id = $superior_id_data->superiorid;
        }
  
        return $superior_id;
        
    }
    
    
    //
    public static function get_approvers_superior_desig_level($superior_id)
    {
        
        $desig_level = "";
        $emp = new Employees;
            
        $emp_query = $emp->where("id", "=", "{$superior_id}")->orderBy("id", "asc")->first(["employees.desig_level"]);
        
        $desig_level = $emp_query->desig_level;
        
        return $desig_level;
        
    }
    
    
    
    //
    public static function check_for_taf_approval($employee_id)
    {
        /* 1-For Endorsement, 2-For Approval, 3- Disapproved, 4-Approved, 5-Forwarded to Finance */
        
        $taf_request_for_approval = TafRequests::whereRaw("(taf_requests.curr_emp = {$employee_id} || taf_requests.fnc_curr_emp = {$employee_id}) AND taf_requests.status_code IN(1,2,3,4,5) AND taf_requests.requestor <> '{$employee_id}'")->count();
        
        
        return $taf_request_for_approval;
    }


    //
    public static function get_requestor_company($employee_id)
    {
    
        $emp_company = "";
        $emp = new Employees;
            
        $emp_company_query = $emp->where("id", "=", "{$employee_id}")->first(["employees.company"]);
        
        $emp_company = $emp_company_query->company;
        
        return $emp_company;
        
    }
    
    
    
    //
    public static function get_acct_rec()
    {
        $act_receiver = "";
        $receivers = new Receivers;
        $receivers_query = $receivers->where("receivers.code", "=", "TAF-ACT")->first(["receivers.employeeid"]);
        
        $act_receiver = $receivers_query->employeeid;
        
        return $act_receiver;
    }
    
    
    //
    public static function get_acct_rec_sat($company)
    {
    
        $act_receiver = "";
        $sat_act = "";
        
        if ($company == "MFC")
        {
            $sat_act = "TAF-ACT-MFC";
        }
        else if ($company == "RBC-SAT")
        {
            $sat_act = "TAF-ACT-SAT";
        }
        else if ($company == "SFI")
        {
            $sat_act = "TAF-ACT-SFI";
        }
        else if ($company == "PFI")
        {
            $sat_act = "TAF-ACT-PFI";
        }
        else if ($company == "SPI")
        {
            $sat_act = "TAF-ACT-SPI";
        }
        else if ($company == "BUK")
        {
            $sat_act = "TAF-ACT-BUK";
        }
        else if ($company == "BBFI")
        {
            $sat_act = "TAF-ACT-BBFI";
        }
        else if ($company == "BUK-MFC")
        {
             $sat_act = "TAF-ACT-MFC";
        }
        
        
        $receivers = new Receivers;
        $receivers_query = $receivers->where("receivers.code", "=", "{$sat_act}")->first(["receivers.employeeid"]);
        
        $act_receiver = $receivers_query->employeeid;
        
        return $act_receiver;
        
    }
    
    
    
    public static function get_finance_rec()
    {
    
        $finance_receiver = "";
        $fnc_rec = new Receivers;
        
        $fnc_rec_query = $fnc_rec->where("receivers.code", "=", "TAF-FNC")->first(["receivers.employeeid"]);
        
        $finance_receiver = $fnc_rec_query->employeeid;
        
        return $finance_receiver;
    }
    
    
    
    //Get company AOM (For Satellite)
    public static function get_comp_aom($company)
    {
    
        $plant_aom = "";
        
        $emp = new Employees;
        $plant_aom_query = $emp->where("employees.company", "=", "{$company}")->where("desig_level", "=", "plant-aom")->where("active", "<>", "0")->first(["employees.id"]);
        
        $plant_aom = $plant_aom_query->id;
        
        return $plant_aom ;
    
    }
    
    
    //Get company OM (For Satellite)
    public static function get_comp_om($company)
    {
    
        $plant_om = "";
        
        $emp = new Employees;
        $plant_om_query = $emp->where("employees.company", "=", "{$company}")->where("desig_level", "=", "om")->where("active", "<>", "0")->first(["employees.id"]);
        
        $plant_om = $plant_om_query->id;
        
        return $plant_om ;
    
    }
    
    
    //
	public static function check_plant_aom($company)
	{
    
        $emp = new Employees;
        $has_plant_aom = $emp->where("employees.desig_level", "=", "plant-aom")->where("employees.company", "=", "{$company}")->where("active", "<>", "0")->get(["employees.desig_level"]);
        
		if($has_plant_aom->count() > 0)
		{
            return true;
		}
		else
		{
            return false;
		}
	
	}


	//
	public static function check_plant_om($company)
	{
	
        $emp = new Employees;
        $has_plant_om = $emp->where("employees.desig_level", "=", "om")->where("employees.company", "=", "{$company}")->where("active", "<>", "0")->get(["employees.desig_level"]);
		
		
		if($has_plant_om->count() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	
	
	}
    
    
    
    //
    public static function get_comp_vpo($company)
    {

        $company_vpo = "";
        
        $emp = new Employees;
        $company_vpo_query = $emp->where("employees.company", "=", "{$company}")->where("desig_level", "=", "vpo")->first(["employees.id"]);
        
        $company_vpo = $company_vpo_query->id;
        
        return $company_vpo;
    }
    
    
    
    
    
    //
    public static function check_acct_rec($employee_id)
    {
    
        $acct_rec  = new Receivers;
        $receivers_query = $acct_rec->where("receivers.code", "=", "TAF-ACT")->where("receivers.employeeid", "=", "{$employee_id}")->first(["receivers.employeeid"]);
        
        if(!empty($receivers_query))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
        //
    public static function check_fnc_rec($employee_id)
    {
        
        $fnc_rec  = new Receivers;
        
        $receivers_query = $fnc_rec->where("receivers.code", "=", "TAF-FNC")->where("receivers.employeeid", "=", "{$employee_id}")->first(["receivers.employeeid"]);
        
        if( !empty($receivers_query) )
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    
    //
    public static function get_email_add($employee_id)
    {
    
        $email_add = "";
        
        $emp = new Employees;
        $emp_query = $emp->where("employees.id", "=", "{$employee_id}")->first(["employees.email"]);
        
        $email_add = $emp_query->email;
        
        return $email_add ;
    }
    
    
    //
    public static function get_taf_report_csv_dept($dept_id, $date_from, $date_to)
    {
    
        $where_condition =  "";
        
        if(intval($dept_id))
		{
			$where_condition = "dep.id = {$dept_id} and taf_requests.request_date Between '{$date_from}' and '{$date_to}'";
		}
		else
		{
			$where_condition = "taf_requests.request_date Between '{$date_from}' and '{$date_to}'";
		}
        
        
        
        
        $taf_query = TafRequests::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "taf_requests.requestor");
        
        })->leftJoin("departments as dep", function($join){
        
            $join->on("dep.id", "=", "emp.departmentid");
        
        })->whereRaw("{$where_condition}")->get([
        
              "taf_requests.taf_no"
             ,"emp.firstname"
             ,"emp.lastname"
             ,"taf_requests.request_date"
             ,"taf_requests.purpose"
             ,"taf_requests.travel_type"
             ,"taf_requests.currency"
             ,"taf_requests.currency_others"
             ,"taf_requests.travel_advances"
             ,"taf_requests.date_required"
             ,"taf_requests.hotel"
             ,"taf_requests.exceptions"
             ,"taf_requests.status"
             ,"taf_requests.endorsed_by"
             ,"taf_requests.endorsed_date"
             ,"taf_requests.endorsed2_by"
             ,"taf_requests.endorsed2_date"
             ,"taf_requests.endorsed3_by"
             ,"taf_requests.endorsed3_date"
             ,"dep.id"
        
        ]);
        
 
        return $taf_query;
        
        //09228563379
    }
    
    
    //
    public static function check_satellite_approval($company, $dept_code)
    {
        
        //RBC-Satellite:
        if (strtolower($company) == "rbc-sat")
        {
            $om_arr_approval = ["HRD-SAT", "PPIC-SAT", "PPIC-SAT2", "PMG-SAT", "SD-SAT", "PD-SAT", "PD2-SAT", "OM-SAT", "PMGT-SAT"];
            
            $aom_arr_approval = ["EC-SAT", "EC-SAT", "EEF1-SAT", "EEF2-SAT", "EF-SAT", "EHVAC-SAT", "EM1-SAT", "EM2-SAT", "EMS-SAT", "ENGR-SAT", "GSCD", "LU-SAT", "MTPL-SAT", "MWWPM-SAT", "OAG-SAT", "WHSEFGRM-SAT", "WHSEGM1-SAT", "WHSEGM2-SAT", "WL-SAT", "PL2-SAT", "PL3-SAT", "PL4-SAT", "PL5-SAT", "PL6-SAT"];
            
            //Vpo approval after department head
            $vpo_approval = ["ACCT-SAT", "QC-SAT", "R&D-SAT"];
            
            if (in_array($dept_code, $om_arr_approval)){
               return "om";
            }
            else if (in_array($dept_code, $aom_arr_approval)){
                return "plant-aom";
            }
            else{
                return "vpo";
            }
        }
        else if (strtolower($company) == "sfi") //SFI
        {
        
            //Vpo approval after department head
            $vpo_approval = ["ACCT-SFI", "QC-SFI", "R&D-SFI"];
            
            if (in_array($dept_code, $vpo_approval))
            {
                return "vpo";
            }
            else
            {
                return "plant-aom";
            }
            
        }
        else if (strtolower($company) == "pfi") //PFI
        {
            //Vpo approval after department head
            $vpo_approval = ["ACCT-PFI", "QC-PFI", "R&D-PFI"];
            
            if (in_array($dept_code, $vpo_approval))
            {
                return "vpo";
            }
            else
            {
                return "plant-aom";
            }
        }
        else if (strtolower($company) == "mfc")
        {
            $om_arr_approval = ["HRD-MFC", "LD-MFC", "MLU", "OM-MFC", "OP-MFC", "PD-BM", "PD-MFC", "PMG-MFC", "SD-MFC"];
            
            $aom_arr_approval = ["ENGR-BM", "ENGR-MFC", "MAINT-BM", "MFCPROD-BM", "MIX-BM", "PFIPROD-BM", "PMGT-MFC", "PMGT2-MFC", "PROD-BM", "PPMG-BM", "PPIC-MFC", "SFIPROD-BM", "SFIPROD-BM", "UNION-BM", "WHSE-BM"];
            
            //Vpo approval after department head
            $vpo_approval = ["ACCT-MFC", "QA-MFC", "R&D-MFC"];
            
            if (in_array($dept_code, $om_arr_approval)){
               return "om";
            }
            else if (in_array($dept_code, $aom_arr_approval)){
                return "plant-aom";
            }
            else{
                return "vpo";
            }
        }
        else if (strtolower($company) == "bbfi")
        {
            $om_arr_approval = ["BLU", "HRD-BBFI", "LD-BBFI", "OM-BBFI", "OP-BBFI", "PD-BBFI", "PMG-BBFI", "SD-BBFI"];
            
            $aom_arr_approval = ["PMGT-BBFI", "PPIC-BBFI"];
            
            //Vpo approval after department head
            $vpo_approval = ["ACCT-BBFI", "QA-BBFI", "R&D-BBFI"];
            
            if (in_array($dept_code, $om_arr_approval)){
               return "om";
            }
            else if (in_array($dept_code, $aom_arr_approval)){
                return "plant-aom";
            }
            else{
                return "vpo";
            }
        }
        else if (strtolower($company) == "spi")
        {
            $om_arr_approval = ["HRD-SPI", "OM-SAT", "PD-SPI", "SD-SAT"];
            
            $aom_arr_approval = ["PMGT-SPI", "PPIC-SPI", "PROD-SPI"];
            
            //Vpo approval after department head
            $vpo_approval = ["ACCT-SPI", "OAG-SPI", "QA-SPI", "R&D-SPI"];
            
            if (in_array($dept_code, $om_arr_approval)){
               return "om";
            }
            else if (in_array($dept_code, $aom_arr_approval)){
                return "plant-aom";
            }
            else{
                return "vpo";
            }
        }
        else if (strtolower($company) == "buk")
        { 
            $om_arr_approval = ["HRD-BUK", "IT-BUK", "LD-BUK", "OM-BM", "OM-BUK", "OP-BUK", "PD-BM", "PD-BUK", "S&A-BUK"];
            
            $aom_arr_approval = ["ENGR-BM", "FORM-BUK", "MAINT-BM", "MFCPROD-BM", "MIX-BM", "PFIPROD-BM", "PPMG-BM", "PROD-BM", "PROD-BUK", "SFIPROD-BM", "WHSE-BUK", "WRAP-BUK", "WHSE2-BUK", "UNION-BM", "UNION-BUK"];
            
            //Vpo approval after department head
            $vpo_approval = ["QC-BUK", "R&D-BUK", "SSD-BUK", "ACCT-BUK"];
            
            if (in_array($dept_code, $om_arr_approval)){
               return "om";
            }
            else if (in_array($dept_code, $aom_arr_approval)){
                return "plant-aom";
            }
            else{
                return "vpo";
            }
        }
        else if (strtolower($company) == "buk-mfc")
        { 
            return "plant-aom";
        }
        
    }
    
    
}
/* End of file */