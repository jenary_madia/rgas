<?php


class MrfSectionHeadAccesses extends \Eloquent {

    
    protected $table = "mrf_section_head_accesses";

    protected $primaryKey = "id";
    protected $hidden = array("password");

    public $timestamps = false;

    public static function isMrfFiler($employeeid) //for head
	{
		$isMrfFiler = MrfSectionHeadAccesses::where("employeeid","=", $employeeid)
				->first();
		if($isMrfFiler) return true;
		return false;
	}

}
