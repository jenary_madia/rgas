<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 21/09/2016
 * Time: 4:30 PM
 */
class ManpowerserviceAttachments extends \Eloquent
{
    protected $table = 'manpowerservices_attachments';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;
}