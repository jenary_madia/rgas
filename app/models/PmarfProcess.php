<?php

use app\RGAS;

class PmarfProcess extends \Eloquent {

	protected $table = "pmarf_requests";
	public $incrementing = true;
	public $timestamps = false;

	protected $attachments_path = '/pmarf/attachments';
	
	public function process_request(){
	    $input = Input::all();
	    $fm = new RGAS\FileManager;		
		$filenames = $fm->upload($this->attachments_path);
	
		$aParticipatingBranch['qty'] = $this->obj_group($input,'qty_');
		$aParticipatingBranch['unit'] = $this->obj_group($input,'unit_');
		$aParticipatingBranch['product'] = $this->obj_group($input,'product_');
<<<<<<< HEAD
	    
	    //Prep Data - request header
        $this->ref_no                           = Input::get('pmarf_refno');
        $this->ref_date	                        = Input::get('pmarf_date_filed');
        $this->ref_status                       = Input::get('pmarf_status');
        $this->req_emp_id                       = Input::get('emp_id');
        $this->req_emp_info                     = $this->obj_group($input,'emp_');
        $this->req_contact_nos                  = Input::get('emp_contactno');
        $this->activity_type                    = Input::get('req_activity_type');
        $this->activity_details                 = Input::get('req_activity_details');
        $this->activity_dates                   = Input::get('req_activity_date');
        $this->activity_from                    = Input::get('req_from');
        $this->activity_to                      = Input::get('req_to');
        $this->activity_no_of_days              = Input::get('req_noOfDays');
        $this->activity_area                    = Input::get('req_area_coverage');
        $this->imp_type                         = Input::get('req_implementer');
        $this->imp_agency_name                  = Input::get('req_agency_name');
        $this->imp_initiate_by                  = $this->obj_group($input,'chk_');
        $this->proposal_objective               = Input::get('specs_proposal');	
        $this->proposal_description             = Input::get('specs_proposal_desc');
        $this->additional_support_activities    = Input::get('specs_additional_support');
        $this->participating_brands             = json_encode($aParticipatingBranch);
        $this->attachments                      = json_encode($filenames);
        $this->comments                         = Input::get('req_comments');
        $this->created_date                     = date('Y-m-d');
        $this->created_by                       = Input::get('emp_id');
        
        $this->save();
        ReferenceNumbers::increment_reference_number('pmarf');
        
        unset($aParticipatingBranch);
	}
	

	public function get_save(){
	    $requests = $this->where('ref_status','=','New')->get(['ref_no',
                	                                           'ref_date',
                	                                           'req_emp_info',
                	                                           'activity_type',
                	                                           'ref_status',
                	                                           'id']);
=======
	    $aExistngAttachments = json_decode($this->obj_group($input,'existing_attachment'),'array');
        $oSupervisor = json_encode($this->get_supervisor(Input::get('emp_id')));
	    $oDeptDead = json_encode($this->get_department_head(Input::get('emp_id')));
	   
	    if(Input::get('action') == 'update' or Input::get('action') == 'is_update'){
	        $oProcessPmarf = $this->find(Input::get('pmarf_id'));
	       
	        $aUpdatedAttachments = array();
	        $attachments = json_decode($oProcessPmarf->attachments,'array');
	        if(count($attachments) > 0){
               foreach($attachments as $n=>$aFile){
                  foreach($aExistngAttachments['existing_attachment'] as $n=>$sRandomFilename){
                    if($sRandomFilename === $aFile['random_filename']){
                         $aUpdatedAttachments[] = $aFile;
                         //unlink files that was deleted
                    }
                  }
               }
	        }
	        
	        if(count($filenames) > 0){
	            foreach($filenames as $n=>$aAdditional){
	                $aUpdatedAttachments[] = $aAdditional;
	            }    
	        }
	        
	        $oProcessPmarf->req_contact_nos                  = Input::get('emp_contactno');
	        $oProcessPmarf->activity_type                    = Input::get('req_activity_type');
            $oProcessPmarf->activity_details                 = Input::get('req_activity_details');
            $oProcessPmarf->activity_dates                   = Input::get('req_activity_date');
            $oProcessPmarf->activity_from                    = Input::get('req_from');
            $oProcessPmarf->activity_to                      = Input::get('req_to');
            $oProcessPmarf->activity_no_of_days              = Input::get('req_noOfDays');
            $oProcessPmarf->activity_area                    = Input::get('req_area_coverage');
            $oProcessPmarf->imp_type                         = Input::get('req_implementer');
            $oProcessPmarf->imp_agency_name                  = Input::get('req_agency_name');
            $oProcessPmarf->imp_initiate_by                  = $this->obj_group($input,'chk_');
            $oProcessPmarf->proposal_objective               = Input::get('specs_proposal');	
            $oProcessPmarf->proposal_description             = Input::get('specs_proposal_desc');
            $oProcessPmarf->additional_support_activities    = Input::get('specs_additional_support');
            $oProcessPmarf->participating_brands             = json_encode($aParticipatingBranch);
            $oProcessPmarf->attachments                      = json_encode($aUpdatedAttachments);
            $oProcessPmarf->comments                         = Input::get('req_comments');
	        $oProcessPmarf->modified_date                    = date('Y-m-d');
            $oProcessPmarf->modified_by                      = Input::get('emp_id');
	        
	        
	        $oProcessPmarf->save();
	        
	        
	    }else if(Input::get('action') == 'save'){
	    
	        //Prep Data - request header
            $this->ref_no                           = Input::get('pmarf_refno');
            $this->ref_date	                        = Input::get('pmarf_date_filed');
            $this->ref_status                       = Input::get('pmarf_status');
            $this->req_emp_id                       = Input::get('emp_id');
            $this->req_emp_info                     = $this->obj_group($input,'emp_');
            $this->req_contact_nos                  = Input::get('emp_contactno');
            $this->activity_type                    = Input::get('req_activity_type');
            $this->activity_details                 = Input::get('req_activity_details');
            $this->activity_dates                   = Input::get('req_activity_date');
            $this->activity_from                    = Input::get('req_from');
            $this->activity_to                      = Input::get('req_to');
            $this->activity_no_of_days              = Input::get('req_noOfDays');
            $this->activity_area                    = Input::get('req_area_coverage');
            $this->imp_type                         = Input::get('req_implementer');
            $this->imp_agency_name                  = Input::get('req_agency_name');
            $this->imp_initiate_by                  = $this->obj_group($input,'chk_');
            $this->proposal_objective               = Input::get('specs_proposal');	
            $this->proposal_description             = Input::get('specs_proposal_desc');
            $this->additional_support_activities    = Input::get('specs_additional_support');
            $this->participating_brands             = json_encode($aParticipatingBranch);
            $this->attachments                      = json_encode($filenames);
            $this->comments                         = Input::get('req_comments');
            $this->created_date                     = date('Y-m-d');
            $this->created_by                       = Input::get('emp_id');
            $this->supervisor_info                  = $oSupervisor;
            $this->departmenthead_info              = $oDeptDead;
            
            $this->save();
            ReferenceNumbers::increment_reference_number('pmarf');
            unset($aParticipatingBranch);
	    }
	}
	
   
    public function send_request($pmarfId,$process){
        $requests = $this->find($pmarfId);
        $supInfo  = trim($requests->supervisor_info) != '' ? json_decode($requests->supervisor_info) : false;
        $supId    = $supInfo != false ? $supInfo->id : 0;
        $deptInfo = trim($requests->departmenthead_info) != '' ? json_decode($requests->departmenthead_info) : false;
        $deptId    = $deptInfo != false ? $deptInfo->id : 0;
    
        switch($requests->request_stage){
            case 'R':
                    if($process === 'approval'){
                        $requests->request_stage        = 'IS';
                        $requests->ref_status           = 'For Approval'; 
                        $requests->request_assigned_to  = $supId;
                    }
                    
                break;
            case 'IS':
                    if($process === 'is_return'){
                        $requests->request_stage        = 'R';
                        $requests->ref_status           = 'Disapprove'; 
                        $requests->request_assigned_to  = $supId;
                    }
                
                    if($process === 'is_send'){
                        $requests->request_stage        = 'DH';
                        $requests->ref_status           = 'For Approval'; 
                        $requests->request_assigned_to  = $deptId;
                    }
                break;
            case 'DH':
                
                break;
        }
        $requests->modified_date   = date('Y-m-d');
        $requests->modified_by     = Session::get('employee_id');
        $requests->save();
        unset($supInfo,$supId,$requests);
    }
    
	public function get_save(){
	    $requests = $this->where('req_emp_id','=',Session::get('employee_id'))
	                     ->where('request_stage','=','R')
	                     ->where('delete_date','=',NULL)
	                     ->where('delete_by','=',NULL)
	                     ->get(['ref_no',
                                'ref_date',
                                'req_emp_info',
                                'activity_type',
                                'ref_status',
                                'id',
                                'request_stage',
                                'supervisor_info']);
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
		if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {		
<<<<<<< HEAD
                $empInfo = json_decode($req->req_emp_info);
=======
                $empInfo          = json_decode($req->req_emp_info);
                $supInfo          = trim($req->supervisor_info) != '' ? json_decode($req->supervisor_info) : false;
                $supName          = $supInfo != false ? $supInfo->firstname." ".$supInfo->lastname : "";
                $sEditDisable     = $req->request_stage === 'R' ? '' : 'disabled';
                $sDeleteDisable   = $req->ref_status === 'Closed' ? '' : 'disabled';
                $sDeleteDisable   = $sDeleteDisable === 'disabled' ? ($req->request_stage === 'R' ? '' : 'disabled') : $sDeleteDisable;
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                
                $result_data["aaData"][$ctr][] = $req->ref_no;
				$result_data["aaData"][$ctr][] = $req->ref_date;
				$result_data["aaData"][$ctr][] = $empInfo->emp_name;
				$result_data["aaData"][$ctr][] = $req->activity_type;
				$result_data["aaData"][$ctr][] = $req->ref_status;

<<<<<<< HEAD
				$result_data["aaData"][$ctr][] = $req->ref_status;
				$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/edit/'.$req->id).'">EDIT</a> 
				                                  <a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">SEND</a>';
				
				
				
				/*$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/edit/'.$req->cbrd_ref_num.'/'.csrf_token()).'">Edit</a>';
				}
				elseif($req->cb_issent && $req->cbrd_status == "New"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>';
				}
				elseif($req->cb_issent && $req->cbrd_status == "Returned"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/edit/'.$req->cbrd_ref_num.'/'.csrf_token()).'">Edit</a>';
				}
				elseif($req->cb_issent && $req->cbrd_status == "For Acknowledgement"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>';
				}
				elseif($req->cb_issent && $req->cbrd_status == "Acknowledged"){
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">View</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/delete/'.$req->cbrd_ref_num.'/'.csrf_token()).'">Delete</a>';
				}*/
=======
				$result_data["aaData"][$ctr][] = $supName;
				$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req->id).'">VIEW</a>
				                                  <a class="btn btndefault btn-default" href="'.URL::to('/pmarf/edit/'.$req->id).'"'.$sEditDisable.'>EDIT</a> 
				                                  <a class="btn btndefault btn-default" href="'.URL::to('/pmarf/delete/'.$req->id).'"'.$sDeleteDisable.'>DELETE</a>';
				
				unset($sEditDisable);
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	
	public function ISview(){
	    $requests = $this->where('request_assigned_to','=',Session::get('employee_id'))
	                     ->where('request_stage','=','IS')
	                     ->where('delete_date','=',NULL)
	                     ->where('delete_by','=',NULL)
	                     ->get(['ref_no',
                                'ref_date',
                                'req_emp_info',
                                'activity_type',
                                'ref_status',
                                'id',
                                'request_stage',
                                'supervisor_info']);

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
		if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {		
                $empInfo          = json_decode($req->req_emp_info);
                $supInfo          = trim($req->supervisor_info) != '' ? json_decode($req->supervisor_info) : false;
                $supName          = $supInfo != false ? $supInfo->firstname." ".$supInfo->lastname : "";
                $sEditDisable     = $req->request_stage === 'R' ? '' : 'disabled';
                $sDeleteDisable   = $req->ref_status === 'Closed' ? '' : 'disabled';
                $sDeleteDisable   = $sDeleteDisable === 'disabled' ? ($req->request_stage === 'R' ? '' : 'disabled') : $sDeleteDisable;
                
                $result_data["aaData"][$ctr][] = $req->ref_no;
				$result_data["aaData"][$ctr][] = $req->ref_date;
				$result_data["aaData"][$ctr][] = $empInfo->emp_name;
				$result_data["aaData"][$ctr][] = $req->activity_type;
				$result_data["aaData"][$ctr][] = $req->ref_status;

				$result_data["aaData"][$ctr][] = $supName;
				$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req->id).'">VIEW</a>
				                                  <a class="btn btndefault btn-default" href="'.URL::to('/pmarf/approve/'.$req->id).'">APPROVE</a>';
				
				unset($sEditDisable);
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function DHview(){
	    $requests = $this->where('request_assigned_to','=',Session::get('employee_id'))
	                     ->where('request_stage','=','DH')
	                     ->where('delete_date','=',NULL)
	                     ->where('delete_by','=',NULL)
	                     ->get(['ref_no',
                                'ref_date',
                                'req_emp_info',
                                'activity_type',
                                'ref_status',
                                'id',
                                'request_stage',
                                'supervisor_info']);

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
		if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {		
                $empInfo          = json_decode($req->req_emp_info);
                $supInfo          = trim($req->supervisor_info) != '' ? json_decode($req->supervisor_info) : false;
                $supName          = $supInfo != false ? $supInfo->firstname." ".$supInfo->lastname : "";
                $sEditDisable     = $req->request_stage === 'R' ? '' : 'disabled';
                $sDeleteDisable   = $req->ref_status === 'Closed' ? '' : 'disabled';
                $sDeleteDisable   = $sDeleteDisable === 'disabled' ? ($req->request_stage === 'R' ? '' : 'disabled') : $sDeleteDisable;
                
                $result_data["aaData"][$ctr][] = $req->ref_no;
				$result_data["aaData"][$ctr][] = $req->ref_date;
				$result_data["aaData"][$ctr][] = $empInfo->emp_name;
				$result_data["aaData"][$ctr][] = $req->activity_type;
				$result_data["aaData"][$ctr][] = $req->ref_status;

				$result_data["aaData"][$ctr][] = $supName;
				$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req->id).'">VIEW</a>
				                                  <a class="btn btndefault btn-default" href="'.URL::to('/pmarf/approve/'.$req->id).'">APPROVE</a>';
				
				unset($sEditDisable);
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function edit_pmarf($id_pmarf){
        $requests = $this->where('id', '=', $id_pmarf)->first();
        return $requests;
	}
<<<<<<< HEAD
    
=======
	
	public function delete_pmarf($id_pmarf){
        $requests = $this->where('id', '=', $id_pmarf)->first();
        $requests->delete_date = date("Y-m-d");
        $requests->delete_by = Session::get('employee_id');
        $requests->save();
	}
	
    public function get_supervisor($nEmpId){
        $oEmp  = Employees::get($nEmpId);
        $oSup  = Employees::get($oEmp->superiorid);
        
        if(count($oSup)>0)
        {
           return array("id"=>$oSup->id,
                        "firstname"=>$oSup->firstname,
                        "middlename"=>$oSup->middlename,
                        "lastname"=>$oSup->lastname,
                        "designation"=>$oSup->designation);   
        }else{
           return false; 
        }
    }
    
    public function get_department_head($nEmpId){
        $oEmp       = Employees::get($nEmpId);
        $oDepthead  = Employees::get_department_head($oEmp->departmentid);
       
        if(count($oDepthead)>0)
        {
           return array("id"=>$oDepthead->id,
                        "firstname"=>$oDepthead->firstname,
                        "middlename"=>$oDepthead->middlename,
                        "lastname"=>$oDepthead->lastname,
                        "designation"=>$oDepthead->designation);   
        }else{
           return false; 
        }
    }
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
    
    function get_initiate_checked($imp_initiate_by){
        $initiated = PmarfLibrary::defined_lib('initiated_by');
        foreach($initiated as $sKey=>$sIni)
        {
            $isChecked   = "";
            $str_checked = $this->jsonDecodeMe($imp_initiate_by);
            $str         = 'chk_'.str_replace(" ", "_", $sIni);
            
            foreach($str_checked as $sIniChecked=>$sVal){
                if($sIniChecked === $str ){
                    $isChecked   = "checked";
                }
            }
            $aIni[$sIni] = $isChecked;
        }
        return $aIni;
    }
    
    function get_initiate_other_desc($imp_initiate_by){
        $str_checked = $this->jsonDecodeMe($imp_initiate_by);
        return $str_checked->chk_others_desc;
    }
    
    function rebuild_brands($participating_brands){
        $brands = json_decode($participating_brands);
        $aQty       = explode(",",$this->cleanMe($brands->qty));
        $aUnit      = explode(",",$this->cleanMe($brands->unit));
        $aProduct   = explode(",",$this->cleanMe($brands->product));
        
    
        for($a=1;$a<=count($aQty);$a++){
            foreach($brands as $sTitle=>$sData){
                $sTitled = $sTitle.'_'.$a;
                if(strpos($sData,$sTitled) >= 0){
                    $aData = json_decode($sData);
                    $aBrands[$a][$sTitled] = $aData->$sTitled;  
                }
            }
        } 
        return $aBrands;
    }
    
    function cleanMe($str){
        $str = str_replace('{','',$str);
        $str = str_replace('}','',$str);
        $str = str_replace('"','',$str);
        
        return $str;
    }
    
    
    
    function jsonDecodeMe($aData){
        return json_decode($aData);
    }
    
	//Populate & group input datas. Return formated data as object.
    public function obj_group($aInputs,$sGroup){
        if(is_array($aInputs) && $sGroup !== ''){
            $aGroupReturn = array();
            foreach($aInputs as $sInputID=>$sValue){
                if(strpos($sInputID,$sGroup) !== false){
                    $aGroupReturn[$sInputID] = $sValue;
                }                
            }
            return json_encode($aGroupReturn);
        }else{return false;}
    }

}
/* End of file */