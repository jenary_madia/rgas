<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::resource('/', 'HomeController');
Route::get('notice', 'HomeController@notice_page');


/* Login */
Route::get('login', 'LoginController@index');
Route::post('sign-in', 'LoginController@sign_in');
Route::post('sign-in2', 'LoginController@sign_in2');
Route::get('sign-out', 'LoginController@sign_out');


/* Travel Authorization Module */
Route::post('travel-authorization/employee-lists', 'TravelAuthorizationController@get_employee_lists');
Route::post('travel-authorization/employee-details', 'TravelAuthorizationController@get_employee_details');
Route::any('travel-authorization/upload-batch/{id?}', 'TravelAuthorizationController@upload_batch');
Route::post('travel-authorization/store', 'TravelAuthorizationController@store');
Route::post('travel-authorization/load-my-taf', 'TravelAuthorizationController@my_taf_list');
Route::post('travel-authorization/load-for-approval-taf', 'TravelAuthorizationController@load_for_approval_taf');
Route::get('travel-authorization/show/{id}', 'TravelAuthorizationController@show');
Route::get('travel-authorization/edit/{id}', 'TravelAuthorizationController@edit');
Route::get('travel-authorization/approve/{id}', 'TravelAuthorizationController@approve');
Route::post('travel-authorization/return-request', 'TravelAuthorizationController@return_request');
Route::post('travel-authorization/update/{id}', 'TravelAuthorizationController@update');
Route::post('travel-authorization/approve-request/{id}', 'TravelAuthorizationController@approve_submit');
Route::get('travel-authorization/submitted-taf', 'TravelAuthorizationController@submitted_travel_authorization');
Route::post('travel-authorization/load-submitted-taf/{id?}', 'TravelAuthorizationController@load_submitted_taf');
Route::post('travel-authorization/submit-request-finance/{id}', 'TravelAuthorizationController@submit_request_finance');
Route::get('travel-authorization/print-taf-pdf/{id}/{taf_no?}', 'TravelAuthorizationController@print_taf_pdf');
Route::get('travel-authorization/taf-pdf-layout/{id}', 'TravelAuthorizationController@taf_pdf_layout');
Route::get('travel-authorization/send-mail', 'TravelAuthorizationController@send_mail');
Route::get('travel-authorization/print-csv/{start_date}/{end_date}/{dept_id?}', 'TravelAuthorizationController@generate_csv_report');
Route::post('travel-authorization/remove-attachment', 'TravelAuthorizationController@remove_attachment');

Route::resource('travel-authorization', 'TravelAuthorizationController');