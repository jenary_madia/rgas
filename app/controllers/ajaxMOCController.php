<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 08/12/2016
 * Time: 10:17 AM
 */
use RGAS\Modules\MOC;
use RGAS\Libraries;

class ajaxMOCController extends \BaseController
{
    protected $MOCvalidator;
    protected $MOC;

    public function __construct() {
        $this->MOCvalidator = new MOC\MOCValidator();
        $this->MOC = new MOC\MOC();
    }

    /*******************POPULATING DATA ON CREATION AND MY OWN MOC**************************/
    public function ajaxGetDepartment() {
        if(Request::ajax()) {
            return Departments::get();
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxFeedEditMOC() {
        if(Request::ajax()) {
            $mocDetails = MOCRequests::where("request_id",Input::get("id"))
                ->with('attachments')
                ->with('ccodes')
                ->with('comments')
                ->with('rcodes')
                ->with('signatories')
                ->first();
            for ($i = 0; $i < count($mocDetails['attachments']); $i++) {
                $imageDetails = json_decode($mocDetails['attachments'][$i]['fn']);
                $mocDetails['attachments'][$i]['urlDownload'] = URL::to('/moc/download'.'/'.$mocDetails['transaction_code'].'/'.$imageDetails->random_filename.'/'.CIEncrypt::encode($imageDetails->original_filename));
            }

            return $mocDetails;
        }else{
            return Redirect::to("/");
        }
    }

    /***********For AJAX Create and My MOC action*************/

    public function ajaxSendMOC() {
        if(Request::ajax()) {
            $validate = $this->MOCvalidator->send(Input::all());
            if ($validate['success']) {
                return $this->MOC->createAction(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxSaveMOC() {
        if(Request::ajax()) {
            return $this->MOC->createAction(Input::all());
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxResendMOC() {
        if(Request::ajax()) {
            $validate = $this->MOCvalidator->send(Input::all());
            if ($validate['success']) {
                return $this->MOC->reCreateAction(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxResaveMOC() {
        if(Request::ajax()) {
            return $this->MOC->reCreateAction(Input::all());
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxCancelMOC() {
        if(Request::ajax()) {
            $validate = $this->MOCvalidator->comment(Input::all());
            if ($validate['success']) {
                return $this->MOC->cancelMOC(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    /*******************POPULATING DATA ON FOR APPROVAL MOC**************************/
    public function ajaxFeedEditForApproveMOC() {
        if(Request::ajax()) {
            $mocDetails = MOCRequests::where("request_id",Input::get("id"))
                ->where('curr_emp',Session::get("employee_id"))
                ->with('attachments')
                ->with('ccodes')
                ->with('comments')
                ->with('rcodes')
                ->with('signatories')
                ->first();
            for ($i = 0; $i < count($mocDetails['attachments']); $i++) {
                $imageDetails = json_decode($mocDetails['attachments'][$i]['fn']);
                $mocDetails['attachments'][$i]['urlDownload'] = URL::to('/moc/download'.'/'.$mocDetails['transaction_code'].'/'.$imageDetails->random_filename.'/'.CIEncrypt::encode($imageDetails->original_filename));
            }
            return $mocDetails;
        }else{
            return Redirect::to("/");
        }
    }

    /***********For AJAX for approval MOC action*************/
    
        /******RETURN TO FILER******/
        public function returnToFiler() {
            if(Request::ajax()) {
                $validate = $this->MOCvalidator->comment(Input::all());
                if ($validate['success']) {
                    return $this->MOC->returnToFiler(Input::all());
                }else{
                    return Response::json(array(
                        'success' => false,
                        'errors' => $validate['errors'],
                        'message' => 'Some fields are incomplete',
                        400
                    ));
                }
            }else{
                return Redirect::to("/");
            }
        }

        /******SEND TO FILER FOR ACKNOWLEDGEMENT******/
        public function sendToFiler() {
            if(Request::ajax()) {
                return $this->MOC->sendToFilerForAcknowledgement(Input::all());
            }else{
                return Redirect::to("/");
            }
        }

        /******ACKNOWLEDGE MOC******/
        public function acknowledgeMOC() {
            if(Request::ajax()) {
                return $this->MOC->acknowledgeMOC(Input::all());
            }else{
                return Redirect::to("/");
            }
        }


    /*******GENERAL ROUTING*******/
        public function ajaxDHtoMD() {
    
            if(Request::ajax()) {
    
                if(Input::get('action') == 'sendToMD') {

                    return $this->MOC->sendToMD(Input::all());
                }
    
            }else{
                return Redirect::to("/");
            }
            
        }
    
        public function ajaxDHtoADH() {
            $validate = $this->MOCvalidator->DHtoADH(Input::all());
            if ($validate['success']) {
                return $this->MOC->sendDHtoADH(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }

        public function ajaxADHtoDH() {
            return $this->MOC->sendADHtoDH(Input::all());
        }
    
        public function ajaxADHtoBSA() {
            $validate = $this->MOCvalidator->ADHtoBSA(Input::all());
            if ($validate['success']) {
                return $this->MOC->sendADHtoBSA(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }

        public function ajaxSendBSAtoADH() {
            return $this->MOC->sendBSAtoADH(Input::all());
        }

        public function ajaxReturntoBSA() {
            return $this->MOC->returntoBSA(Input::all());
        }

        public function ajaxSendtoDivHead() {
            return $this->MOC->sendtoDivHead(Input::all());
        }
        /************* CORPORATE ROUTING  **********/
    
        /************* SATELLITE ROUTING  **********/
        public function ajaxToAOM() {
            if(Input::get('action') == 'sendToAOM'){
                return $this->MOC->sendToAOM(Input::all());
            }
        }

        //aom to csmd dh
        public function ajaxAOMtoDH() {
            return $this->MOC->sendToMD(Input::all(),2);
        }

        public function ajaxDivHeadtoSSMD() {
            return $this->MOC->sendDivHeadtoSSMD(Input::all());
        }

        /**** GET DEPARTMENTS ON COMPANY******/

        public function ajaxGetDepartments() {
            return Departments::where('comp_code',Input::get('company'))->select(['dept_name','id'])->get();
        }

        /**** submitted MOC******/

        public function ajaxSearchMOC() {
            $submittedMOC = MOCRequests::search();
            $leaveData = [];
            if ( count($submittedMOC) > 0){
                $ctr = 0;
                foreach($submittedMOC as $req) {
                    $leaveData[$ctr][] = $req['transaction_code'];
                    $leaveData[$ctr][] = $req['title'];
                    $leaveData[$ctr][] = $req['status'];
                    $leaveData[$ctr][] = $req['firstname'].' '.$req['middlename'].' '.$req['lastname'];
                    $leaveData[$ctr][] = $req['current_emp']['firstname'].' '.$req['current_emp']['middlename'].' '.$req['current_emp']['lastname'];
                    $leaveData[$ctr][] = $req['assigned_date'];
                    $leaveData[$ctr][] = $req['acknowledge_date'];
                    if($req['curr_emp'] == Session::get("employee_id")) {
                        $leaveData[$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-xs btn-default' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">assess</a>";
                    }else{
                        $leaveData[$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' disabled href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' disabled class='btn btnDataTables btn-xs btn-default' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">assess</a>";
                    }

                    $ctr++;
                }
            }
            return $leaveData;

        }

}