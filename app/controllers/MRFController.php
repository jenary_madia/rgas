<?php

use RGAS\Modules\MRF;
use RGAS\Libraries;
use RGAS\Modules\MRF\Logs;
class MRFController extends \BaseController {

	protected $MRFvalidator;
	protected $MRF;
	protected $logs;

	public function __construct() {
		$this->logs = new Logs();
		$this->MRFvalidator = new MRF\MRFValidator();
		$this->MRF = new MRF\MRF();
	}

//	public function index()
//	{
//        $data["base_url"] = URL::to("/");
//        return View::make("mrf.index", $data);
//	}

	public function listView($type) {
		$data['type'] = $type;
		$data["base_url"] = URL::to("/");
		return View::make("mrf.index", $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['MRFRequests'] = MrfRequests::where('group_id',Session::get('company') == 'RBC-CORP' ? 1 : 2)
			->get()
			->lists('name','id');
		$data["base_url"] = URL::to("/");
		return View::make("mrf.create",$data);
	}

	public function showMyMRF($method,$id)
	{
		$data["base_url"] = URL::to("/");
		$data["mrfID"] = $id;
		$data["method"] = $method;
		$data['MRFRequests'] = MrfRequests::where('group_id',Session::get('company') == 'RBC-CORP' ? 1 : 2)
			->get()
			->lists('name','id');
		$data['mrfDetails'] = ManpowerRequest::with('owner')
			->with('department')
			->with('section')
			->with('requestCode')
			->with('attachments')
			->with('signatories')
			->find($id);
//		return $data['mrfDetails'];
		$data['lastSig'] = MrfSignatories::where('manpowerrequestid',$id)->max('signature_type');

		if($data['mrfDetails']) {
			if($method == 'view') {
				return View::make("mrf.view", $data);
			}else{
				if(in_array($data['mrfDetails']['status'],json_decode(MOC_EDITABLE))) {
					return View::make("mrf.edit", $data);
				}else{
					return Redirect::to('/mrf')
						->with('errorMessage', 'This MRF form is not editable');
				}
			}

		}else{
			return Redirect::to('/mrf')
				->with('errorMessage', 'Invalid MRF');
		}
	}

	public function forEndorsement($method,$id) {
		$data["base_url"] = URL::to("/");
		$data["mrfID"] = $id;
		$data["method"] = $method;
		$data['mrfDetails'] = ManpowerRequest::with('owner')
			->with('department')
			->with('section')
			->with('requestCode')
			->with('attachments')
			->with('signatories')
			->find($id);
		if($data['mrfDetails']) {
			return View::make("mrf.view", $data);
		}else{
			return Redirect::to('/mrf')
				->with('errorMessage', 'Invalid MRF');
		}
	}

	public function forProcessing($method,$id) {
		$data["base_url"] = URL::to("/");
		$data["mrfID"] = $id;
		$data["method"] = $method;
		$data['mrfDetails'] = ManpowerRequest::with('owner')
			->with('department')
			->with('section')
			->with('requestCode')
			->with('attachments')
			->with('candidates')
			->find($id);
		$data['lastSig'] = MrfSignatories::where('manpowerrequestid',$id)->max('signature_type');
		$data['csmdHead'] = $this->getCSMD('head');
		$data['chrdHead'] =$this->getCHRDHead();
		$data['recruitmentStaffs'] = $this->getCHRDRecruitmentStaff();
		$data['bsa'] = DB::table('receivers')
			->join('employees','employees.id','=','receivers.employeeid')
			->where('receivers.code' , CSMD_BSA)
			->select((DB::raw("receivers.employeeid,concat(employees.firstname,' ',employees.middlename,' ',employees.lastname) as fullname")))
			->lists('fullname','employeeid');
		if($data['mrfDetails']) {
			return View::make("mrf.view", $data);
		}else{
			return Redirect::to('/mrf')
				->with('errorMessage', 'Invalid MRF');
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function getCHRDRecruitmentStaff()
	{
		return Sections::where('sect_code', Session::get("company") == "RBC-CORP" ? "HRR" : "HRR-SAT")->join('employees', 'employees.departmentid', '=', 'sections.departmentid')->get();
	}

	private function getCSMD($type) {
		return Employees::where('departmentid',CSMD_DEPT_ID)
			->where('desig_level',$type)
			->first();
	}

	private function getCHRDHead()
	{
		return Receivers::where([
			'description' => 'HRMD Department Head',
			'title' => 'Corporate MRF receiver',
			'code' => 'CORP_MRF_RECEIVER'
		])
			->with('employee')
			->first();
	}

	/*****************for downloading attachments*****/
	public function downloadAttachments($refNum, $random_filename, $original_filename) {
		$dataID = ManpowerRequest::where("reference_no",$refNum)
			->first(['id']);
		$fm = new Libraries\FileManager;
//		$this->logs->AU008($dataID['id'],'MRF','id',$refNum);
		$filepath = $this->MRF->getStoragePath() . "$refNum/" . $random_filename;
		if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
			$filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
		}
		return $fm->download($filepath, CIEncrypt::decode($original_filename));
	}
}
