public function download($te_ref_num, $random_filename, $original_filename)
{
// $te = new TERepository;
// $request = $te->getRequestByRefNum($te_ref_num);

// $te_id = $request->te_id;
	$fm = new Libraries\FileManager;
	$filepath = Config::get('rgas.rgas_storage_path') . $this->TE->attachments_path . "$te_ref_num/" . $random_filename;
	if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){

	$filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
	}
	return $fm->download($filepath, CIEncrypt::decode($original_filename));
}
