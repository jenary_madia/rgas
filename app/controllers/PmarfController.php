<?php

class PmarfController extends \BaseController {


	public function index() {
		//
	}
    
    public function create_pmarf()
    {
        $data["base_url"] = URL::to("/");
        $data["reference_number"] = ReferenceNumbers::get_current_reference_number('pmarf');
        $data["activity_type"] = PmarfLibrary::defined_lib('activity_type');
        $data["activity_date"] = PmarfLibrary::defined_lib('activity_date');
        $data["implementer"] = PmarfLibrary::defined_lib('implementer');
        $data["initiated_by"] = PmarfLibrary::defined_lib('initiated_by');
        
<<<<<<< HEAD
        
=======
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        return View::make("pmarf/create_pmarf", $data);
    }
    
    public function action()
    {
        $pmamrf = new PmarfProcess;
        $pmamrf->process_request();
            
        $data["base_url"] = URL::to("/");
        $data["reference_number"] = ReferenceNumbers::get_current_reference_number('pmarf');
        $data["activity_type"] = PmarfLibrary::defined_lib('activity_type');
        $data["activity_date"] = PmarfLibrary::defined_lib('activity_date');
        $data["implementer"] = PmarfLibrary::defined_lib('implementer');
        $data["initiated_by"] = PmarfLibrary::defined_lib('initiated_by');
        return View::make("pmarf/create_pmarf", $data);
    }
    
    public function view_pmarf(){
        $pmamrf = new PmarfProcess;
<<<<<<< HEAD
        $data["base_url"] = URL::to("/");
     
        return View::make("pmarf/save_pmarf", $data);
    }
    
    public function get_my_save_pmarf(){
        $pmamrf = new PmarfProcess;
        $aData = $pmamrf->get_save();
=======
        $oEmp   = Employees::get(Session::get('employee_id'));
	    $data["base_url"] = URL::to("/");
	    
	    switch($oEmp->desig_level){
	        case "employee"://R
                return View::make("pmarf/save_pmarf", $data);
	            break;
	        case "supervisor"://IS
                return View::make("pmarf/isview_pmarf", $data);
	            break;
	        case "head"://IS
                return View::make("pmarf/dhview_pmarf", $data);
	            break;
	        default:
	        break;
	    }
    }
    
   public function get_my_save_pmarf(){
        $pmamrf = new PmarfProcess;
        $oEmp   = Employees::get(Session::get('employee_id'));
        switch($oEmp->desig_level){
	        case "employee"://IS
	            $aData = $pmamrf->get_save();
	        break;
	        case "supervisor"://IS
	            $aData = $pmamrf->ISview();
	        break;
	        case "head"://IS
	            $aData = $pmamrf->DHview();
	        break;
        }
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
        return json_encode($aData);
    }
    
    public function edit_pmarf($pmarf_id){
<<<<<<< HEAD
        $pmamrf      = new PmarfProcess;
        $rec         = $pmamrf->edit_pmarf($pmarf_id);
        
       
        $data["base_url"]       = URL::to("/");
        $data["activity_type"]  = PmarfLibrary::defined_lib('activity_type');
        $data["activity_date"]  = PmarfLibrary::defined_lib('activity_date');
        $data["implementer"]    = PmarfLibrary::defined_lib('implementer');
        $data["rec"]            = $rec;
        $data["empInfo"]        = json_decode($rec->req_emp_info);
        
        $data["initiated_by"]           = $pmamrf->get_initiate_checked($rec->imp_initiate_by);
        $data["initiated_other_desc"]   = $pmamrf->get_initiate_other_desc($rec->imp_initiate_by);
        $data["brands"]                 = $pmamrf->rebuild_brands($rec->participating_brands);
        
        //echo ($rec->participating_brands);
        //print_r($data["brands"]);
        
    
       
        
        return View::make("pmarf/edit_pmarf", $data);
    }
    
=======
        $pmamrf = new PmarfProcess;
        $data["base_url"] = URL::to("/");
        
        if(Input::get('action') == 'update'){
            $pmamrf->process_request();
        }
        
        if(Input::get('action') == 'send'){
            $pmamrf->send_request($pmarf_id,'approval');
            return Redirect::action("PmarfController@view_pmarf");
        }
        
        if(Input::get('action') == 'back'){
           return Redirect::action("PmarfController@view_pmarf");
        }else{
            $pmamrf                         = new PmarfProcess;
            $rec                            = $pmamrf->edit_pmarf($pmarf_id);
            $data["base_url"]               = URL::to("/");
            $data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
            $data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
            $data["implementer"]            = PmarfLibrary::defined_lib('implementer');
            $data["rec"]                    = $rec;
            $data["empInfo"]                = json_decode($rec->req_emp_info);
            $data["initiated_by"]           = $pmamrf->get_initiate_checked($rec->imp_initiate_by);
            $data["initiated_other_desc"]   = $pmamrf->get_initiate_other_desc($rec->imp_initiate_by);
            $data["brands"]                 = $pmamrf->rebuild_brands($rec->participating_brands);
            $data["attachments"]            = $attachments=json_decode($rec->attachments,'array');
            return View::make("pmarf/edit_pmarf", $data);    
        }
    }
    
    public function approve_pmarf($pmarf_id){
        $pmamrf = new PmarfProcess;
        $data["base_url"] = URL::to("/");
        
        if(Input::get('action') == 'is_update'){
            $pmamrf->process_request();
        }
        
        if(Input::get('action') == 'is_return' || Input::get('action') == 'is_send'){
            $pmamrf->send_request($pmarf_id,Input::get('action'));
            return Redirect::action("PmarfController@view_pmarf");
        }
        
        if(Input::get('action') == 'back'){
           return Redirect::action("PmarfController@view_pmarf");
        }else{
            $pmamrf                         = new PmarfProcess;
            $rec                            = $pmamrf->edit_pmarf($pmarf_id);
            $data["base_url"]               = URL::to("/");
            $data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
            $data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
            $data["implementer"]            = PmarfLibrary::defined_lib('implementer');
            $data["rec"]                    = $rec;
            $data["empInfo"]                = json_decode($rec->req_emp_info);
            $data["initiated_by"]           = $pmamrf->get_initiate_checked($rec->imp_initiate_by);
            $data["initiated_other_desc"]   = $pmamrf->get_initiate_other_desc($rec->imp_initiate_by);
            $data["brands"]                 = $pmamrf->rebuild_brands($rec->participating_brands);
            $data["attachments"]            = $attachments=json_decode($rec->attachments,'array');
            return View::make("pmarf/approval_edit_pmarf", $data);    
        }
    }
    
    public function delete_pmarf($pmarf_id){
        $pmamrf = new PmarfProcess;
        $data["base_url"] = URL::to("/");
        $pmamrf->delete_pmarf($pmarf_id);
        
        return Redirect::action("PmarfController@view_pmarf");
    }
    
    public function view($pmarf_id){
        $pmamrf = new PmarfProcess;
        $data["base_url"] = URL::to("/");
        
        if(Input::get('action') == 'update'){
            $pmamrf->process_request();
        }
    
        if(Input::get('action') == 'back'){
           return Redirect::action("PmarfController@view_pmarf");
        }else{
            $pmamrf                         = new PmarfProcess;
            $rec                            = $pmamrf->edit_pmarf($pmarf_id);
            $data["base_url"]               = URL::to("/");
            $data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
            $data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
            $data["implementer"]            = PmarfLibrary::defined_lib('implementer');
            $data["rec"]                    = $rec;
            $data["empInfo"]                = json_decode($rec->req_emp_info);
            $data["initiated_by"]           = $pmamrf->get_initiate_checked($rec->imp_initiate_by);
            $data["initiated_other_desc"]   = $pmamrf->get_initiate_other_desc($rec->imp_initiate_by);
            $data["brands"]                 = $pmamrf->rebuild_brands($rec->participating_brands);
            $data["attachments"]            = $attachments=json_decode($rec->attachments,'array');
            return View::make("pmarf/view_pmarf", $data);    
        }
    }

    public function send_pmarf($pmarf_id){
        $pmamrf = new PmarfProcess;
        $pmamrf->send_request($pmarf_id,'approval');

        $data["base_url"] = URL::to("/");
        return View::make("pmarf/save_pmarf", $data);
    }
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
    
    function pick_brand_info($sCol,$sColParam,$row){
        $oCol = json_decode($sCol);
       
        foreach($sCol as $sName){
             print_r($sName);
            die();
            if($sName === $sColParam.'_'.$row){
                $aBrands[$row][$sName]=$stVal;
            }
        }
        return $stVal;
    }
    
    
    public function department_head_pmarf_approval()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/department_head_pmarf_approval", $data);    
    }

    public function submitted_pmarf()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/submitted_pmarf", $data);    
    }

    public function for_processing_pmarf()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/for_processing_pmarf", $data);    
    }

    public function npmd_acceptance_pmarf()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/npmd_acceptance_pmarf", $data);   
    }

    public function npmd_execution_pmarf()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/npmd_execution_pmarf", $data);   
    }

    public function npmd_notation_pmarf()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/npmd_notation_pmarf", $data);   
    }

    public function for_conforme_pmarf()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmarf/for_conforme_pmarf", $data);   
    }
}