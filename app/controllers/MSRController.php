<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/17/2016
 * Time: 8:34 AM
 */
use RGAS\Modules\MSR;
use RGAS\Libraries;
use RGAS\Modules\MSR\Logs;
class MSRController extends \BaseController
{
    protected $MSRvalidator;
    protected $MSR;
    protected $logs;

    public function __construct() {
        $this->logs = new Logs();
        $this->MSRvalidator = new MSR\MSRValidator();
        $this->MSR = new MSR\MSR();
    }

    public function index(){
        $data["base_url"] = URL::to("/");
        if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true))) {
            return View::make("msr.index", $data);
        }elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true))) {
            return View::make("msr.smis.index", $data);
        }

    }
    
    public function myMSRProcess(){
        $data = explode('|',Input::get("lrfAction"));
        if(count($data) < 2) {
            return Redirect::to("msr")
                ->with("errorMessage","Invalid action");
        }else{
            $action = $data[0];
            $id = $data[1];
        }
        if($action == 'softDelete') {
            return $this->MSR->deleteMyMSR($id);
        }

    }
// ********************* creation process *****************************/
    public function create() {
        if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true))) {
            $data["base_url"] = URL::to("/");
            return View::make("msr.create", $data);
        }elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true))) {
            $data['jobTitle'] = (new SelectItems)->getItems('msr', 'jobTitle');
            $data["base_url"] = URL::to("/");
            return View::make("msr.smis.create", $data);
        }
    }

    public function createAction() {
        if(Input::get("action") == 'send') {
            $validate = $this->MSRvalidator->send();
            if($validate === true) {
                return $this->MSR->send();
            }else{
                return $validate;
            }

        }elseif(Input::get("action") == 'save'){
            return $this->MSR->save();
        }
    }
    
    public function editAction($id) {
        if(Input::get("action") == 'send') {
            $validate = $this->MSRvalidator->send();
            if($validate === true) {
                return $this->MSR->resend($id);
            }else{
                return $validate;
            }
        }elseif(Input::get("action") == 'save'){
            return $this->MSR->resave($id);
        }elseif (Input::get("action") == 'cancel'){
            return $this->MSR->cancel($id);
        }
    }
// ********************* viewing own MSR *****************************/
    public function myMSR($id,$method) {
        if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true))) {
            $data['MSRDetails'] = ManpowerService::where('id', $id)
                ->where('employeeid', Session::get("employee_id"))
                ->with('attachments')
                ->with('agencies')
                ->with('requirements')
                ->with('signatories')
                ->first();

            if (!$data['MSRDetails']) {
                return Redirect::to('msr')
                    ->with("errorMessage", "Invalid MSR");
            }
            foreach ($data['MSRDetails']['requirements'] as $key) {
                $key['active'] = false;
            }

            foreach ($data['MSRDetails']['agencies'] as $key) {
                $key['active'] = false;
            }

            if ($method == 'view') {
                $data['endorsed'] = [];
                if ($data['MSRDetails']['signatories']) {
                    foreach ($data['MSRDetails']['signatories'] as $signatory) {
                        if ($signatory['signature_type'] == 1) {
                            array_push($data['endorsed'], $signatory);
                        }
                    }
                }
                $data["base_url"] = URL::to("/");
                return View::make("msr.my_msr_view", $data);
            } elseif ($method == 'edit') {
                $data["base_url"] = URL::to("/");
                return View::make("msr.my_msr_edit", $data);
            } else {
                return Redirect::to("msr")
                    ->with("errorMessage", "Invalid action");
            }
        }elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true))) {
            $data['MSRDetails'] = ManpowerService::where('id', $id)
                ->where('employeeid', Session::get("employee_id"))
                ->with('attachments')
                ->with('agencies')
                ->with('signatories')
                ->first();
            $data['SMISRequirements'] = (new ProjectRequirements())->fetch_req($id);
            $data['jobTitle'] = [];
            $selectItems = (new SelectItems)->getItems('msr', 'jobTitle');
            foreach ($selectItems as $key) $data['jobTitle'][$key->item] = $key->text;
            if (!$data['MSRDetails']) {
                return Redirect::to('msr')
                    ->with("errorMessage", "Invalid MSR");
            }
            foreach ($data['MSRDetails']['requirements'] as $key) {
                $key['active'] = false;
            }

            foreach ($data['MSRDetails']['agencies'] as $key) {
                $key['active'] = false;
            }
            if ($method == 'view') {
                $data['MSRDetails']['signatories'];
                $data['endorsed'] = [];
                if ($data['MSRDetails']['signatories']) {
                    foreach ($data['MSRDetails']['signatories'] as $signatory) {
                        if ($signatory['signature_type'] == 1) {
                            array_push($data['endorsed'], $signatory);
                        }
                    }
                }
                $data["base_url"] = URL::to("/");
                return View::make("msr.smis.my_msr_view", $data);
            } elseif ($method == 'edit') {
                $data["base_url"] = URL::to("/");
                return View::make("msr.smis.my_msr_edit", $data);
            } else {
                return Redirect::to("msr")
                    ->with("errorMessage", "Invalid action");
            }
        }
    }

// ********************* viewing superior MSR *****************************/

    public function forApproval($id,$method) {
        if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true))) {
            if(Session::get("desig_level") != 'head') {
                $curr_emp = Session::get("superiorid");
                $data['otherIs'] = [];
                $tas = Employees::where('id',$curr_emp)->first(['desig_level','id']);
                if($tas['desig_level'] != 'head') {
                    while ($tas['desig_level'] != 'head') {
                        $tas = Employees::where('id',$curr_emp)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                        array_push($data['otherIs'],$tas);
                        $curr_emp = $tas['superiorid'];
                    }
                }elseif ($tas['desig_level'] == 'head'){
                    $tas = Employees::where('id',$curr_emp)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                    array_push($data['otherIs'],$tas);
                }
            }
            $data['method'] = $method;
            $data['MSRDetails'] = ManpowerService::where('id',$id)
                ->where('curr_emp',Session::get('employee_id'))
                ->with('owner')
                ->with('attachments')
                ->with('agencies')
                ->with('requirements')
                ->with('section')
                ->with('department')
                ->with('signatories')
                ->first();

            $data['MSRDetails']['signatories'];
            $data['endorsed'] = [];
            if ($data['MSRDetails']['signatories']) {
                foreach ($data['MSRDetails']['signatories'] as $signatory){
                    if($signatory['signature_type'] == 1) {
                        array_push($data['endorsed'],$signatory);
                    }
                }
            }
            if (! $data['MSRDetails']) {
                return Redirect::to('msr')
                    ->with("errorMessage","Invalid MSR");
            }
            foreach($data['MSRDetails']['requirements'] as $key) {
                $key['active'] = false;
            }

            foreach($data['MSRDetails']['agencies'] as $key) {
                $key['active'] = false;
            }

            if (in_array($method,['view','approve'])) {
                $data["base_url"] = URL::to("/");
                return View::make("msr.for_approval_msr_view", $data);
            }else{
                return Redirect::to("msr")
                    ->with("errorMessage","Invalid action");
            }
        }elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true))){
            if(Session::get("desig_level") != 'head') {
                $curr_emp = Session::get("superiorid");
                $data['otherIs'] = [];
                $tas = Employees::where('id',$curr_emp)->first(['desig_level','id']);
                if($tas['desig_level'] != 'head') {
                    while ($tas['desig_level'] != 'head') {
                        $tas = Employees::where('id',$curr_emp)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                        array_push($data['otherIs'],$tas);
                        $curr_emp = $tas['superiorid'];
                    }
                }elseif ($tas['desig_level'] == 'head'){
                    $tas = Employees::where('id',$curr_emp)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                    array_push($data['otherIs'],$tas);
                }
            }
            $data['method'] = $method;
            $data['MSRDetails'] = ManpowerService::where('id', $id)
                ->where('curr_emp', Session::get("employee_id"))
                ->with('attachments')
                ->with('agencies')
                ->with('signatories')
                ->first();
            $data['SMISRequirements'] = (new ProjectRequirements())->fetch_req($id);
            $data['endorsed'] = [];
            if ($data['MSRDetails']['signatories']) {
                foreach ($data['MSRDetails']['signatories'] as $signatory){
                    if($signatory['signature_type'] == 1) {
                        array_push($data['endorsed'],$signatory);
                    }
                }
            }
            if (!$data['MSRDetails']) {
                return Redirect::to('msr')
                    ->with("errorMessage", "Invalid MSR");
            }
            foreach ($data['MSRDetails']['requirements'] as $key) {
                $key['active'] = false;
            }

            foreach ($data['MSRDetails']['agencies'] as $key) {
                $key['active'] = false;
            }
            if (in_array($method,['view','approve'])) {
                $data["base_url"] = URL::to("/");
                return View::make("msr.smis.for_approval_msr_view", $data);
            }else{
                return Redirect::to("msr")
                    ->with("errorMessage","Invalid action");
            }
        }
    }
    
    public function forApprovalAction($id) {
        $action = Input::get('action');
        if ($action == 'request') {
            return $this->MSR->request($id);
        }elseif($action == 'toHR') {
            return $this->MSR->sendToHr($id);
        }elseif($action == 'toFiler') {
            return $this->MSR->returnToFiler($id);
        }elseif($action == 'returnToIS') {
            return $this->MSR->returnToIS($id);
        }
    }
    
    //  ****************************************** LIST SUBMITTED MSR
    public function submittedMSR() {
        $data["base_url"] = URL::to("/");
        $data["departments"] = Departments::whereIn('dept_code',json_decode(MSR_NPMD_DEPARTMENTS,true))->get();
        return View::make("msr.submitted_msr", $data);
    }
    // ***************************************VIEWING SUBMITTED MSR
    public function viewSubmittedMSR($id,$method) {
        $data['method'] = $method;
        $data['MSRDetails'] = ManpowerService::where('id',$id)
            ->with('owner')
            ->with('attachments')
            ->with('agencies')
            ->with('requirements')
            ->with('section')
            ->with('department')
            ->with('signatories')
            ->first();

        $data['MSRDetails']['signatories'];
        $data['endorsed'] = [];
        if ($data['MSRDetails']['signatories']) {
            foreach ($data['MSRDetails']['signatories'] as $signatory){
                if($signatory['signature_type'] == 1) {
                    array_push($data['endorsed'],$signatory);
                }
            }
        }
        
        if (! $data['MSRDetails']) {
            return Redirect::to('msr')
                ->with("errorMessage","Invalid MSR");
        }
        foreach($data['MSRDetails']['requirements'] as $key) {
            $key['active'] = false;
        }

        foreach($data['MSRDetails']['agencies'] as $key) {
            $key['active'] = false;
        }

        if (in_array($method,['view','process'])) {
            if($method == 'process' && $data['MSRDetails']['status'] == 'PROCESSED') {
                return Redirect::to("msr/submitted_msr")
                    ->with("errorMessage","Unable to process this MSR");
            }
            $data["base_url"] = URL::to("/");
            return View::make("msr.submitted_msr_view", $data);
        }else{
            return Redirect::to("msr/submitted_msr")
                ->with("errorMessage","Invalid action");
        }
    }

    public function submittedMSRAction($id) {
        return $this->MSR->saveToArchive($id);
    }


//  ******************************************  ajax for list
    public function getMyMSR($limit = null) {
        if(Request::ajax()) {
            $result = ManpowerService::where('employeeid', Session::get('employee_id'))
                ->with('current_emp')
                ->where('status','!=',"DELETED");
            if($limit) {
                $result = $result->limit($limit);
            }
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $deletableNotifications = (in_array($req['status'], json_decode(MSR_DELETABLE,true)));
                    $editableNotifications = (in_array($req['status'], json_decode(MSR_EDITABLE,true)));
                    $btnDelete = "<button  ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' name='lrfAction' value='softDelete|{$req->id}'>Delete</button>";
                    $btnView = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/msr/my_msr/'.$req->id.'/view').">View</a>";
                    $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' href=".url('/msr/my_msr/'.$req->id.'/edit').">Edit</a>";
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = $req['reason'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['current_emp']['firstname'].' '.$req['current_emp']['lastname'];
                    $result_data["aaData"][$ctr][] = $btnDelete.' '.$btnView.' '.$btnEdit;
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getSuperiorMSR($limit = null) {
        if(Request::ajax()) {
            $result = ManpowerService::where('curr_emp', Session::get('employee_id'))
                ->with('owner')
                ->where('status','FOR APPROVAL');
            if($limit) {
                $result = $result->limit($limit);
            }
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = $req['reason'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                    $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/msr/for_approval/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-xs btn-default' href=".url('/msr/for_approval/'.$req['id'].'/approve').">approve</a>";
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getSubmittedMSR() {
        if(Request::ajax()) {
            $result = ManpowerService::where('curr_emp', Session::get('employee_id'))
                ->whereIn('departmentid', $this->getDeptID(json_decode(MSR_NPMD_DEPARTMENTS,true)))
                ->whereIn('status',['FOR PROCESSING','PROCESSED'])
                ->with('owner')
                ->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = $req['reason'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                    $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/msr/submitted/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' ".($req['status']=='PROCESSED' ? 'disabled' : '')." class='btn btnDataTables btn-xs btn-default' href=".url('/msr/submitted/'.$req['id'].'/process').">process</a>";
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getFilteredSubmittedMSR(){
        return $result = (new ManpowerService())->filterMSRForReceiver(1,$this->getDeptID(json_decode(MSR_NPMD_DEPARTMENTS,true)));
    }
    
    
    /*****************for downloading attachments*****/
    public function downloadAttachments($refNum, $random_filename, $original_filename) {
        $dataID = ManpowerService::where("reference_no",$refNum)
        ->first(['id']);
        $fm = new Libraries\FileManager;
        $this->logs->AU008($dataID['id'],'MSR','id',$refNum);
        $filepath = $this->MSR->getStoragePath() . "$refNum/" . $random_filename;
        if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
            $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
        }
        return $fm->download($filepath, CIEncrypt::decode($original_filename));
    }

    /**********************FOR EXPORTING TO CSV*******/
    public function exportToCSV() {
        include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $start_row = 2;

        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'SUBMITTED MSR');
        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'EMPLOYEE NAME');
        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'EMPLOYEE NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'COMPANY');
        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'DEPARTMENT');
        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'SECTION');
        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'REFERENCE NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'DATE FILED');
        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'STATUS');
        $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'CONTACT NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", 'REQUEST');
        $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", 'URGENCY');
        $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", 'REASON');
        $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", 'REQUIREMENTS AREA');
        $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", 'REQUIREMENTS PERSON #');
        $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", 'REQUIREMENTS WORKING DAYS');
        $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", 'REQUIREMENTS JOBTITLE');
        $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", 'REQUIREMENTS DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", 'AGENCY NAME');
        $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", 'AGENCY INFO');
        $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", 'SIGNATORIES SIGNATURE TYPE');
        $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", 'SIGNATORIES EMPLOYEE SIGNED');

        $objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getFont()->setBold(true);

        $request = (new ManpowerService())->printMSRForReceiver(1,$this->getDeptID(json_decode(MSR_NPMD_DEPARTMENTS,true)));
        foreach ($request as $req) {
            $countList = [
                count($req['agencies']),
                count($req['requirements']),
                count($req['signatories'])
            ];
            $rounds = max($countList);

            for ($x = 0; $x <= $rounds - 1; $x++)
            {
                $start_row++;
                $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req['owner']['firstname'].' '.$req['owner']['middlename'].' '.$req['owner']['lastname']);
                $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req['owner']['employeeid']);
                $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req['owner']['company']);
                $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req['department']['dept_name']);
                $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req['section']['sect_name']);
                $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req['reference_no']);
                $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req['datefiled']);
                $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req['status']);
                $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $req['contactno']);
                $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", ($req['pooling'] ? "POOLING" : "SCREENING"));
                $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", ($req['rush'] ? "RUSH" : "NORMAL PRIORITY"));
                $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", $req['reason']);

                if(array_key_exists($x,json_decode($req['requirements'],true))) {
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", $req['requirements'][$x]['area']);
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", $req['requirements'][$x]['personno']);
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", $req['requirements'][$x]['workingdays']);
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", $req['requirements'][$x]['jobtitle']);
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", $req['requirements'][$x]['dateneeded']);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", '*');
                }

                if(array_key_exists($x,json_decode($req['agencies'],true))) {
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req['agencies'][$x]['agency']);
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", $req['agencies'][$x]['agency_info']);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", '*');
                }

                if(array_key_exists($x,json_decode($req['signatories'],true))) {
                    switch ($req['signatories'][$x]['signature_type']) {
                        case 1:
                            $signatureType = 'endorsed';
                            break;
                        case 2:
                            $signatureType = 'received';
                            break;
                        case 3:
                            $signatureType = 'processed';
                            break;
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", $signatureType);
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", $this->fetchFullName($req['signatories'][$x]['employee']));
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", '*');
                }

            }
        }
        //WRITE AND DOWNLOAD EXCEL FILE
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();

        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get("employee_id");
        $fname = (Input::get('action') == "generate" ? Input::get('filename') : (Input::get('filename') ? Input::get('filename') : date('Y-m-d')."-MSR"));
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$fname.xls");
        return $objWriter->save('php://output');
        $objPHPExcel->disconnectWorksheets();
    }

    private function getDeptID($dept_code) {
        $data = Departments::where('dept_code',$dept_code)->get(['id']);
        $ids  = [];
        if ($data) {
            foreach($data as $key) {
                array_push($ids,$key['id']);
            }
        }

        return $ids;
    }

    private function fetchFullName($data) {
        return $data['firstname']." ".$data['middlename']." ".$data['lastname'];
    }

}