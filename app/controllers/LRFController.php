<?php

use RGAS\Modules\LRF;
use RGAS\Libraries;
class LRFController extends \BaseController {
	
	private $LRF;
	private $leaves;
	private $logs;
    private $clinic_receiver;
	
	public function __construct()
	{
        $this->LRF = new LRF\LRF;
		$this->leaves = new Leaves;
        $this->logs = new LRF\Logs();
        $this->clinic_receiver = Receivers::where('employeeid',Session::get("employee_id"))
            ->where('code',CLINIC_LEAVE_RECEIVER)
            ->where('company',Session::get('company'))->first()['id'];

	}

	public function index()
	{
        $monthToday = Date("m");
        switch ($monthToday) {
            case (in_array($monthToday,['01','02','03'])):
                $quarter = ['01','02','03'];
                break;
            case (in_array($monthToday,['04','05','06'])):
                $quarter = ['04','05','06'];
                break;
            case (in_array($monthToday,['07','08','09'])):
                $quarter = ['07','08','09'];
                break;
            case (in_array($monthToday,['10','11','12'])):
                $quarter = ['10','11','12'];
                break;
        }
        return Leaves::getHomeVisitLeaves(Session::get("employee_id"),$quarter);
	}
	public function create()
	{
        $data["leaveTypes"] = ( new SelectItems())->getItems("lrf","leaveType");
		$data["base_url"] = URL::to("/");
		return View::make("lrf/create", $data);  
	}

	public function leaves()
	{
		$data["base_url"] = URL::to("/");
		return View::make("lrf/index",$data);
	}

	public function viewLeave($id,$method = null)
	{
		$data["base_url"] = URL::to("/");
		$data["leaveDetails"] = Leaves::getLeave($id,2);
        $this->logs->AU003($id,'leaves','id',$data["leaveDetails"]['referenceNo']);
		if (! $data["leaveDetails"]) {
			return Redirect::to('lrf/leaves')
			->with('errorMessage', 'You\'ve looking for invalid leave');
		}
		$data["method"] = $method;
        $data["clinic_receiver"] = $this->clinic_receiver;
		$data["leaveSignatories"] = Leaves::find($id)->signatories()->with('employee')->get();
		if (in_array(Session::get("company"),json_decode(CORPORATE,true))) {
			if(Session::get("dept_name") == "OFFICE OF THE EXECUTIVES")
            {
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),json_decode(LEAVE_CORP_EXECUTIVES_APPROVERS,true),Session::get("dept_id"));
            }else{
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),json_decode(LEAVE_CORP_OTHER_DEPT_APPROVERS,true),Session::get("dept_id"));
            }
		}elseif(in_array(Session::get("company"),json_decode(MFC_ETC,true))) {
            $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),json_decode(LEAVE_APPROVERS_MFC_ETC,true),Session::get("dept_id"));
		}else{
            if(Session::get("dept_name") == "OPERATION MANAGEMENT")
            {
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),json_decode(LEAVE_APPROVERS_OM,true),Session::get("dept_id"));
            }else{
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),json_decode(LEAVE_APPROVERS_OTHER_ETC,true),Session::get("dept_id"));
            }
		}
        return View::make("lrf.view",$data);
	}

	public function viewMyLeave($id)
	{
		$data["base_url"] = URL::to("/");
		$data["leaveDetails"] = Leaves::getLeave($id,1);
        $this->logs->AU003($id,'leaves','id',$data["leaveDetails"]['referenceNo']);
		if (! $data["leaveDetails"]) {
			return Redirect::to('lrf/leaves')
			->with('errorMessage', 'You\'ve looking for invalid leave');
		}

		$data["leaveSignatories"] = Leaves::find($id)->signatories()->with('employee')->get();
		return View::make("lrf.my_leave_view",$data);
	}

	public function editMyLeave($id)
	{
		$data["base_url"] = URL::to("/");
		$data["leaveDetails"] = Leaves::getLeave($id,1);
        $intRestDays = [];
        foreach (explode('&',$data["leaveDetails"]['restDay']) as $key) {
            switch ($key) {
                case 'Sunday':
                    array_push($intRestDays,0);
                    break;
                case 'Monday':
                    array_push($intRestDays,1);
                    break;
                case 'Tuesday':
                    array_push($intRestDays,2);
                    break;
                case 'Wednesday':
                    array_push($intRestDays,3);
                    break;
                case 'Thursday':
                    array_push($intRestDays,4);
                    break;
                case 'Friday':
                    array_push($intRestDays,5);
                    break;
                case 'Saturday':
                    array_push($intRestDays,6);
                    break;
            }
        }
		$allowedStatus = array('NEW',"DISAPPROVED","RETURNED");
		if (! $data["leaveDetails"]) {
			invalid:
			return Redirect::to('lrf/leaves')
			->with('errorMessage', 'You\'ve looking for invalid leave');
		}

		if (! in_array($data["leaveDetails"]["status"], $allowedStatus)) {
			return Redirect::to('lrf/leaves')
			->with('errorMessage', 'You\'ve chosen uneditable leave');
		}

        $data["leaveDetails"]["restDay"] = $this->LRF->parseToInt($data["leaveDetails"]["restDay"]);
		$data["leaveTypes"] = SelectItems::where("module","lrf")->where("modulesfield","leaveType")->get();
		$data["leaveSignatories"] = Leaves::find($id)->signatories()->with('employee')->get();
		return View::make("lrf.my_leave_edit",$data);
	}

	public function searchAction()
	{
		$parameters =  $this->LRF->filterSearch();
		return Leaves::search($parameters,Session::get("employee_id"));
	}

	public function showApprovedLeaves($id,$type = null)
	{
		$homeVisit = [];
		$data["LeaveApprovedTotal"] = Leaves::getApprovedLeaves($id,$type);
		$data["leaveTypes"] = SelectItems::where("module","lrf")
											->where("modulesfield","leaveType")->get();
		if($type == "hv"){

			$month = [1,2,3];

			for ($i=0; $i < 4; $i++) {
				array_push($homeVisit, Leaves::getHomeVisitLeaves($id,$month)["sum"]);
				$last = $month[2] + 1;
				$month = [$last+1,$last+2,$last+3];
			}

		}else{
			unset($data["leaveTypes"][2]);
		}
		$data["HomeVisits"] = $homeVisit;
		$data['type'] = $type;
		return View::make("lrf.show_leaves",$data);
	}
    


/* ----------------DATA TABLES API----------------*/
	// For leaves
	public function getMyLeaves()
	{
		return $this->leaves->myLeaves();
	}

	public function getForApproveLeaves()
	{
		return $this->leaves->getForApproveLeaves();
	}

	public function getSubmittedToReceiver()
	{
		return $this->leaves->submittedToReceiver();
	}

	public function getSubmittedToClinic()
	{
		return $this->leaves->submittedToClinic();
	}

	public function getEmployees()
	{
		return Employees::all();
	}
/* ----------------FORM ACTION---------------------- */
	// for submitting form of lrf.create
	public function action($id = null)
	{
        if(Input::get('lrfAction') == "save"){
            return $this->LRF->saveLRF();
        }elseif(Input::get('lrfAction') == "resave"){
            return $this->LRF->saveLRF($id);
        }else{
            $request = new LRF\Requests\Store;
            $validate = $request->validate();
            if ($validate === true) {
                if (Input::get('lrfAction') == "send") {
                    return $this->LRF->sendLRF();
                }elseif(Input::get('lrfAction') == "resend"){
                    return $this->LRF->sendLRF($id);
                }
            }else{
                return $validate;
            }
        }
	}

	public function leaveAction($id = null)
	{
        if($id) {
            if(Input::get("lrfAction") == 'cancelRequest') {
                return $this->LRF->cancelRequest($id);
            }else{
                return $this->LRF->processLeave($id);
            }
        }else{
            if(Input::get("lrfAction") == "batchApprove") {
//                return Input::all();
                if(Input::get("chosenLeave")) {
                    $comment = json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=>'BATCH APPROVED!'
                    ));
                    
                    $paramsSignatory = array(
                        "leaveId" => Input::get('chosenLeave'),
                        "signatureType" => 2
                    );

                    $addSignatory = Signatory::store($paramsSignatory);
                    $leaves = Leaves::batchApprove(Input::get("chosenLeave"),$comment);
                    if ($leaves && $addSignatory) {
                        if(in_array(Session::get("company"),json_decode(CORPORATE,true))) {
                            return Redirect::to("lrf/leaves")
                                ->with('successMessage', "Leave request successfully sent to CHRD for processing");
                            exit;
                        }
                            return Redirect::to("lrf/leaves")
                                ->with('successMessage', "Leave request successfully sent to SHRD for processing");
                            exit;
                    }else{
                        return Redirect::to('lrf/leaves')
                            ->with('errorMessage', 'Something went wrong upon submitting leave.');
                    }

                }else{
                    return Redirect::to('lrf/leaves')
                        ->with('errorMessage', 'Please choose leave/s to approve');
                }
            }else{
                $leaveID = explode("|", Input::get("lrfAction"))[1];
                $listViewAction = explode("|", Input::get("lrfAction"))[0];
                if($listViewAction == 'softDelete') {
                    $process = Leaves::softDelete($leaveID);

                    if ($process) {
                        return Redirect::to('lrf/leaves')
                            ->with('successMessage',"Leave successfully deleted");
                    }else{
                        return Redirect::to('lrf/leaves')
                            ->with('errorMessage', 'Something went wrong upon submitting leave.');
                    }
                }
            }
        }
	}
    
	public function PECreateLeave()
	{
        $data['departments'] = [];
        $departmentsToParse = Departments::getDepartmentsByCompany(Session::get("company"));
        foreach ($departmentsToParse as $key) $data['departments'][$key->id] = $key->dept_name;

        $data['leaveTypes'] = [];
        $selectItems = (new SelectItems)->getItems('lrf','leaveType');
        foreach ($selectItems as $key) $data['leaveTypes'][$key->item] = $key->text;
        $data["base_url"] = URL::to("/");
		return View::make("lrf.pe_create_leave", $data);
	}

	public function PEAction($id = null)
	{
        if (Input::get("action") == "toSave") {
            return $this->LRF->PESaveLeave($id);
        }elseif(Input::get("action") == "toSend"){
            return $this->LRF->PECreateLeave();
        }elseif(Input::get("action") == "cancel"){
            return $this->LRF->PECancelLeave($id);
        }elseif(Input::get("action") == "resend"){
            return $this->LRF->PEResend($id);
        }elseif (Input::get("action") == "return") {
            return $this->LRF->PEReturn($id);
        }
	}

    public function peView($id,$method = null) {
		$data['approvers'] = Employees::where("company",Session::get("company"))
            ->whereIn("desig_level",array("head","supervisor"))
            ->where("id","!=",Session::get("employee_id"))
            ->orderBy('desig_level', 'DESC')
            ->get();
        $data['leaveBatch'] = LeaveBatch::where("id", $id)
            ->with('batchDetails')
            ->with('department')
            ->with('section')
            ->where('curr_emp',Session::get("employee_id"))
            ->where('status', 'FOR APPROVAL')
            ->first();
        $data['leaveTypes'] = [];
        $data['method'] = $method;
        $selectItems = (new SelectItems)->getItems('lrf','leaveType');
        foreach ($selectItems as $key) $data['leaveTypes'][$key->item] = $key->text;
        if (! $data["leaveBatch"]) {
            return Redirect::to('lrf/leaves')
                ->with('errorMessage', 'You\'ve looking for invalid leave');
        }

        if(! Input::old()){
            LeaveBatchDetails::where('leave_batch_id',$id)->update(["noted" => 0]);
        }

        if (Request::ajax())
        {
            $parsedData= array();
            for ($i=0; $i < count($data['leaveBatch']['batchDetails']); $i++) {
                $parsedData[$i]["employeeID"] =  $data['leaveBatch']['batchDetails'][$i]['employee_id'];
                $parsedData[$i]["employeeName"] =  $data['leaveBatch']['batchDetails'][$i]['employee_name'];
                $parsedData[$i]["employeeNumber"] =  $data['leaveBatch']['batchDetails'][$i]['employee_number'];
                $parsedData[$i]["leaveType"] =  $data['leaveBatch']['batchDetails'][$i]['appliedfor'];
                $parsedData[$i]["leaveName"] =  $data['leaveBatch']['batchDetails'][$i]['leave_item']['text'];
                $parsedData[$i]["reason"] =  $data['leaveBatch']['batchDetails'][$i]['reason'];
                $parsedData[$i]["fromDate"] =  $data['leaveBatch']['batchDetails'][$i]['from_date'];
                $parsedData[$i]["fromPeriod"] =  $data['leaveBatch']['batchDetails'][$i]['from_duration'];
                $parsedData[$i]["toDate"] =  $data['leaveBatch']['batchDetails'][$i]['to_date'];
                $parsedData[$i]["toPeriod"] =  $data['leaveBatch']['batchDetails'][$i]['to_duration'];
                $parsedData[$i]["totalDays"] =  $data['leaveBatch']['batchDetails'][$i]['noofdays'];
                $parsedData[$i]["duration"] =  $data['leaveBatch']['batchDetails'][$i]['duration'];
                $parsedData[$i]['active'] = false;
                if (Session::get("desig_level") == "head") {
                    $parsedData[$i]['noted'] = true;
                }else{
                    $parsedData[$i]['noted'] = false;
                }
            }
            return $parsedData;
        }else{
            $this->logs->AU003($id,'leave_batch','id', $data['leaveBatch']['documentcode'].'-'.$data['leaveBatch']['codenumber']);
        }
        $data["base_url"] = URL::to("/");
        return View::make("lrf.pe_view", $data);
    }

    public function peViewMyRequest($id,$method = null) {
        $data['leaveBatch'] = LeaveBatch::where("id", $id)
            ->with('batchDetails')
            ->with('department')
            ->with('section')
            ->where('ownerid',Session::get("employee_id"))
            ->first();
        if (! $data["leaveBatch"]) {
            return Redirect::to('lrf/leaves')
                ->with('errorMessage', 'You\'ve looking for invalid leave');
        }
        if (Request::ajax())
        {
            $parsedData= array();
            for ($i=0; $i < count($data['leaveBatch']['batchDetails']); $i++) {
                $parsedData[$i]["date"] =  $data['leaveBatch']['date'];
                $parsedData[$i]["employeeID"] =  $data['leaveBatch']['batchDetails'][$i]['employee_id'];
                $parsedData[$i]["employeeName"] =  $data['leaveBatch']['batchDetails'][$i]['employee_name'];
                $parsedData[$i]["employeeNumber"] =  $data['leaveBatch']['batchDetails'][$i]['employee_number'];
                $parsedData[$i]["leaveType"] =  $data['leaveBatch']['batchDetails'][$i]['appliedfor'];
                $parsedData[$i]["leaveName"] =  $data['leaveBatch']['batchDetails'][$i]['leave_item']['text'];
                $parsedData[$i]["reason"] =  $data['leaveBatch']['batchDetails'][$i]['reason'];
                $parsedData[$i]["fromDate"] =  $data['leaveBatch']['batchDetails'][$i]['from_date'];
                $parsedData[$i]["fromPeriod"] =  $data['leaveBatch']['batchDetails'][$i]['from_duration'];
                $parsedData[$i]["toDate"] =  $data['leaveBatch']['batchDetails'][$i]['to_date'];
                $parsedData[$i]["toPeriod"] =  $data['leaveBatch']['batchDetails'][$i]['to_duration'];
                $parsedData[$i]["totalDays"] =  $data['leaveBatch']['batchDetails'][$i]['noofdays'];
                $parsedData[$i]["duration"] =  $data['leaveBatch']['batchDetails'][$i]['duration'];
                $parsedData[$i]['active'] = 'false';
            }
            return $parsedData;
        }else{
            $this->logs->AU003($id,'leave_batch','id', $data['leaveBatch']['documentcode'].'-'.$data['leaveBatch']['codenumber']);
        }
        $data['method'] = $method;
        $data["base_url"] = URL::to("/");
        if ($method == 'view') {
            return View::make("lrf.pe_view_my_request", $data);
        }else{
            $data['departments'] = [];
            $departmentsToParse = Departments::getDepartmentsByCompany(Session::get("company"));
            foreach ($departmentsToParse as $key) $data['departments'][$key->id] = $key->dept_name;

            $data['leaveTypes'] = [];
            $selectItems = (new SelectItems)->getItems('lrf','leaveType');
            foreach ($selectItems as $key) $data['leaveTypes'][$key->item] = $key->text;
            return View::make("lrf.pe_edit_my_request", $data);
        }
    }

    public function peISAction($id){
        if (Input::get("action") == "request") {
            return $this->LRF->PESend($id,Input::get("otherSuperior"));
        }elseif(Input::get("action") == "toHR") {
            return $this->LRF->PESendToHR($id);
        }elseif(Input::get("action") == "return") {
            $validate = Validator::make(
                array('comment' => Input::get("comment")),
                array('comment' => 'required'),
                array('comment.required' => 'Comment is required')
            );
            if ($validate->fails())
            {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validate)
                    ->with('message', 'Some fields are incomplete.');
                exit;

            }
            return $this->LRF->PEReturn($id);
        }
    }

	public function postParseEmployees()
	{
		if (Request::ajax()) {

			switch (Input::get("leaveType")) {
				case "Birthday":
					$leaveName = "Birthday";
					break;
				case "Emergency":
					$leaveName = "Emergency";
					break;
				case "HomeVisit":
					$leaveName = "Home Visitation";
					break;
				case "Mat/Pat":
					$leaveName = "Maternity/Paternity";
					break;
				case "Sick":
					$leaveName = "Sick";
					break;
                case "Vacation":
					$leaveName = "Vacation";
					break;
			}
			return array_merge(Input::all(), array('leaveName' => $leaveName));
		}
	}

    public function PEMyRequests() {
        if (Request::ajax())
        {
            return (new LeaveBatch())->myLeaves();
        }
    }

    public function PESuperiorRequests() {
        if (Request::ajax())
        {
            return (new LeaveBatch())->superiorLeaves();
        }
    }

    public function noteBatchDetails() {
        if (Input::get("note")) {
            $batchDetails = LeaveBatchDetails::where("employee_number",Input::get("employeeNumber"))
                ->where("leave_batch_id",Input::get("batchID"));
            $batchDetails->update(array(
                "noted" => 1
            ));
        }else{
            $batchDetails = LeaveBatchDetails::where("employee_number",Input::get("employeeNumber"))
                ->where("leave_batch_id",Input::get("batchID"));
            $batchDetails->update(array(
                "noted" => 0
            ));
        }
    }

    public function downloadAttachments($refNum, $random_filename, $original_filename) {
        $parsedRefno = explode('-',$refNum);
         $dataID = Leaves::where("documentcode",$parsedRefno[0].'-'.$parsedRefno[1])
            ->where("codenumber",$parsedRefno[2])->first(['id']);
        $fm = new Libraries\FileManager;
        $this->logs->AU008($dataID['id'],'leaves','id',$refNum);
        $filepath = $this->LRF->getStoragePath() . "$refNum/" . $random_filename;
        if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
            $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
        }
        return $fm->download($filepath, CIEncrypt::decode($original_filename));
    }

    /* ----------------DATA TABLES API----------------*/
    public function getHomeMyLeaves()
    {
        if (Request::ajax()) {
            $requests = Leaves::with("employee")
                ->where('ownerid',Session::get("employee_id"))
                ->where("isdeleted",0)
                ->where("isprocessed",0)
                ->orderBy('id','DESC')
                ->limit(5)
                ->get();

            $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
            $result_data["iTotalRecords"] = count($requests);
            if ( count($requests) > 0){
                $ctr = 0;
                foreach($requests as $req) {
                    $deletableNotifications = (in_array($req['status'], json_decode(DELETABLE,true)));
                    $editableNotifications = (in_array($req['status'], json_decode(EDITABLE,true)));
                    $btnDelete = "<button  ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' name='lrfAction' value='softDelete|{$req->id}'>Delete</button>";
                    $btnView = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/my_leaves/'.$req->id.'/view').">View</a>";
                    $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/my_leaves/'.$req->id.'/edit').">Edit</a>";
                    $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
                    $result_data["aaData"][$ctr][] = $req->datecreated;
                    $result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from.' to '.$req->to : $req->from);
                    $result_data["aaData"][$ctr][] = $req->appliedfor;
                    $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                    $result_data["aaData"][$ctr][] = $req->status;
                    $result_data["aaData"][$ctr][] = ($req->employee ? $req->employee->firstname." ".$req->employee->middlename." ".$req->employee->lastname : "----");
                    $result_data["aaData"][$ctr][] = $btnDelete.' '.$btnView.' '.$btnEdit;
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $requests;
            }
            return $result_data;
        }
    }

    public function getHomeForApproveLeaves()
    {
        if (Request::ajax()) {
            $forApproveLeaves = Leaves::where('curr_emp', Session::get('employee_id'))
                ->where('status', 'FOR APPROVAL')
                ->where("isprocessed", 0)
                ->where("isdeleted", 0)
                ->limit(5)
                ->get();

            $result_data["iTotalDisplayRecords"] = count($forApproveLeaves); //total count filtered query
            $result_data["iTotalRecords"] = count($forApproveLeaves);
            if (count($forApproveLeaves) > 0) {
                $ctr = 0;
                foreach ($forApproveLeaves as $req) {
                    $result_data["aaData"][$ctr][] = '<input type="checkbox" name="chosenLeave[]" value="' . $req->id . '" class="chosenLeave"> ' . $req->documentcode . '-' . $req->codenumber;
                    $result_data["aaData"][$ctr][] = $req->datecreated;
                    $result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from . ' to ' . $req->to : $req->from);
                    $result_data["aaData"][$ctr][] = $req->appliedfor;
                    $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason, 0, 100) : $req->reason);
                    $result_data["aaData"][$ctr][] = $req->status;
                    $result_data["aaData"][$ctr][] = $req->firstname . ' ' . $req->middlename . ' ' .$req->lastname;
                    $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=" . url('/lrf/leave/' . $req->id . '/view') . ">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default btn-xs' href=" . url('/lrf/leave/' . $req->id . '/approve') . ">Approve</a>";
                    $ctr++;
                }
            } else {
                $result_data["aaData"] = $forApproveLeaves;
            }
            return $result_data;
        }
    }

    public function getHomeSubmittedToClinic()
    {
        if (Request::ajax()) {
            $clinicLeaves = Leaves::where("curr_emp", Session::get('employee_id'))
                ->where("isprocessed", 0)
                ->where("status", "FOR APPROVAL")
                ->where("isdeleted", 0)
                ->whereRaw("(appliedfor = 'Sick' and noofdays > 3 or appliedfor = 'Mat/Pat')")
                ->limit(5)
                ->get();

            $result_data["iTotalDisplayRecords"] = count($clinicLeaves); //total count filtered query
            $result_data["iTotalRecords"] = count($clinicLeaves);
            if (count($clinicLeaves) > 0) {
                $ctr = 0;
                foreach ($clinicLeaves as $req) {
                    $result_data["aaData"][$ctr][] = '<input type="checkbox" name="chosenLeave[]" value="' . $req->id . '" class="chosenLeave"> ' . $req->documentcode . '-' . $req->codenumber;
                    $result_data["aaData"][$ctr][] = $req->datecreated;
                    $result_data["aaData"][$ctr][] = ($req->to != '0000-00-00' ? $req->from . ' to ' . $req->to : $req->from);
                    $result_data["aaData"][$ctr][] = $req->appliedfor;
                    $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason, 0, 100) : $req->reason);
                    $result_data["aaData"][$ctr][] = $req->status;
                    $result_data["aaData"][$ctr][] = $req->lastname . ' ' . $req->firstname . ' ' . $req->middlename;
                    $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=" . url('/lrf/' . $req->id . '/clinic') . ">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default btn-xs' href=" . url('/lrf/' . $req->id . '/clinic/Contracts') . ">Approve</a>";
                    $ctr++;
                }
            } else {
                $result_data["aaData"] = $clinicLeaves;
            }
            return $result_data;
        }
    }

    public function getHomePESuperiorRequests() {
        if (Request::ajax())
        {
            $forApproveNotifications = LeaveBatch::where('curr_emp', Session::get('employee_id'))
                ->whereIn('status', array('FOR APPROVAL','RETURNED'))
                ->with('department')
                ->with('section')
                ->limit(5)
                ->get();
            $result_data["iTotalDisplayRecords"] = count($forApproveNotifications); //total count filtered query
            $result_data["iTotalRecords"] = count($forApproveNotifications);
            if ( count($forApproveNotifications) > 0){
                $ctr = 0;
                foreach($forApproveNotifications as $req) {
                    $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                    $result_data["aaData"][$ctr][] = $req['datecreated'];
                    $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                    $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['owner']['firstname']." ".$req['owner']['middlename']." ".$req['owner']['lastname'];
                    $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/pe_view/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-default btn-xs' href=".url('/lrf/pe_view/'.$req['id'].'/approve').">Approve</a>";
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $forApproveNotifications;
            }
            return $result_data;
        }
    }

    public function checkIfLeaveValid() {
        if (Request::ajax()) {
            $homeVisitPerQuarter  = 7;
            $monthToday = Date("m");
            switch ($monthToday) {
                case (in_array($monthToday,['01','02','03'])):
                   $quarter = ['01','02','03'];
                    break;
                case (in_array($monthToday,['04','05','06'])):
                    $quarter = ['04','05','06'];
                    break;
                case (in_array($monthToday,['07','08','09'])):
                    $quarter = ['07','08','09'];
                    break;
                case (in_array($monthToday,['10','11','12'])):
                    $quarter = ['10','11','12'];
                    break;
            }
            $homeVisitSum = Leaves::getHomeVisitLeaves(Session::get("employee_id"),$quarter)["sum"];
            if ($homeVisitSum  >= $homeVisitPerQuarter) {
                return 0;
            }else{
                return 1;
            }
        }

//        return $leaves;
    }
}
