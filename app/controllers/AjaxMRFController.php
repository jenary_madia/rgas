<?php

use RGAS\Modules\MRF;
use RGAS\Libraries;

class AjaxMRFController extends \BaseController {

	protected $MRFvalidator;
	protected $MRF;

	public function __construct() {
		$this->MRFvalidator = new MRF\MRFValidator();
		$this->MRF = new MRF\MRF();
	}
	
	/***********For AJAX Create and My MOC action*************/

	public function sendMRF() {
		if(Request::ajax()) {
			$validate = $this->MRFvalidator->send(Input::all());
			if ($validate['success']) {
				return $this->MRF->createAction(Input::all());
			}else{
				return Response::json(array(
					'success' => false,
					'errors' => $validate['errors'],
					'message' => 'Some fields are incomplete',
					400
				));
			}
		}else{
			return Redirect::to("/");
		}
	}

    /***********For AJAX Create and My MOC action*************/

    public function saveMRF() {
        if(Request::ajax()) {
            return $this->MRF->createAction(Input::all());
        }else{
            return Redirect::to("/");
        }
    }


    /***********From For endorsement to Process*************/

    public function processMRF() {
        if(Request::ajax()) {
            if(in_array(Input::get("action"), ["forProcess","assign","save","disapprove","toCsmdDH"])) {
                if(Input::get("action") == "disapprove") {
                    $validate = $this->MRFvalidator->comment(Input::all());
                    if($validate['success']) {
                        goto proceed;
                    }else{
                        return Response::json(array(
                            'success' => false,
                            'errors' => $validate['errors'],
                            'message' => 'Some fields are incomplete',
                            400
                        ));
                    }
                }
                proceed :
                return $this->MRF->processAction(Input::all());
            }
        }else{
            return Redirect::to("/");
        }
    }

    public function deleteMyMRF() {
        $mrf = ManpowerRequest::where([
            'id' => Input::get('mrfID'),
            'requestedby' => Session::get('employee_id'),
        ])
        ->whereIn('status',['NEW','CANCELLED','DISAPPROVED','RETURNED'])
        ->first();

        MrfSignatories::where('manpowerrequestid',Input::get('mrfID'))->delete();
        MrfAttachments::where('manpowerrequestid',Input::get('mrfID'))->delete();
        $mrf->delete();

        return Redirect::to("/mrf/list-view/my-mrf")
        ->with("successMessage", "MRF successfully deleted");
    }
    
    /***********From For endorsement to Process*************/

    public function returnProcess() {
        if(Request::ajax()) {
            $validate = $this->MRFvalidator->comment(Input::all());
            if($validate['success']) {
                return $this->MRF->returnAction(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    /***********From For endorsement to Process*************/

    public function cancelMRF() {
        if(Request::ajax()) {
            $validate = $this->MRFvalidator->comment(Input::all());
            if($validate['success']) {
                return $this->MRF->cancelAction(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }


    /***********save Recruitment*************/

    public function saveRecruitment() {
        if(Request::ajax()) {
            return $this->MRF->saveRecruitment(Input::all());
        }else{
            return Redirect::to("/");
        }
    }
    
	//Datatables
    public function fetchMyMRF($limit = null)
    {
        if(Request::ajax()) {
            $result = ManpowerRequest::with('current_emp')
                ->where('status','!=',"DELETED")
				->where('employeeid',Session::get("employee_id"))
                ->limit($limit);
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $deletableNotifications = (in_array($req['status'], json_decode(MOC_DELETABLE,true)));
                    $editableNotifications = (in_array($req['status'], json_decode(MOC_EDITABLE,true)));
                    $btnDelete = "<button  ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' name='mrfID' value='{$req->id}'>Delete</button>";
                    $btnView = "<a class='btn btnDataTables btn-default btn-xs' href=".url('/mrf/view/my-mrf/'.$req->id).">View</a>";
                    $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btnDataTables btn-default btn-xs' href=".url('/mrf/edit/my-mrf/'.$req->id).">Edit</a>";
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = $req['position_jobtitle'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['current_emp']['firstname'].' '.$req['current_emp']['lastname'];
                    $result_data["aaData"][$ctr][] = $btnDelete.' '.$btnView.' '.$btnEdit;
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function fetchForEndorsementApproval($type)
    {
        if(Request::ajax()) {
            $result = ManpowerRequest::with('owner')
                ->where('curr_emp',Session::get("employee_id"));

            if($type == 1) {
                $result->where('status','FOR ENDORSEMENT');
            }elseif($type == 2) {
                $result->where('status','FOR APPROVAL');
            }elseif($type == 3) {
                $result->where('status','NOTED');
            }elseif($type == 4) {
                $result->where('status','FOR PROCESSING');
            }elseif($type == 5) {
                $result->where('status','FOR ASSESSMENT');
            }elseif($type == 6) {
                $result->where('status','FOR FURTHER ASSESSMENT');
            }elseif($type == 8) {
                $result->where('status','APPROVED');
            }

            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = $req['position_jobtitle'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                    if($type == 1) {
                        $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/mrf/view/for-endorsement/'.$req->id).">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-xs btn-default' href=".url('/mrf/process/for-endorsement/'.$req->id).">endorse</a>";
                    }else{
                        $result_data["aaData"][$ctr][] = "<a class='btn btnDataTables btn-xs btn-default' href=".url('/mrf/view/for-processing/'.$req->id).">View</a><a style='margin-left: 3px' class='btn btnDataTables btn-xs btn-default' href=".url('/mrf/process/for-processing/'.$req->id).">Approve</a>";
                    }
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

}
