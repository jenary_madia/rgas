<?php

class LoginController extends \BaseController {
    
    
	//
	public function index()
	{
        //
		if(Session::has("logged_in"))
        {
			return Redirect::to("");
			die;
		}
        
		$data["base_url"] = URL::to("/");
		return View::make("login", $data);
	}
	
	
	//For login page original
	public function sign_in()
	{
	
		$result_arr = array();
		$username = Input::get("username", null, true);
		$password = md5(Input::get("password", null, true));
        
		$emp = Employees::leftJoin("departments as dept", function($join){
		 
			$join->on("employees.departmentid", "=", "dept.id");
			
		 })->leftJoin("sections as sect", function($join){
		 
			$join->on("employees.sectionid", "=", "sect.id");
			
		 })->where("employees.username", "=", $username)->where("employees.password","=", $password)->where("employees.active", "<>", "0")->first([
			 "employees.id as employee_id"
			,"employees.employeeid"
			,"employees.new_employeeid"
			,"employees.firstname"
			,"employees.middlename"
			,"employees.lastname"
			,"employees.desig_level"
			,"employees.company"
			,"employees.designation"
			,"employees.superiorid"
			,"employees.email"
			,"employees.username"
			,"dept.dept_name"
			,"dept.dept_type"
			,"dept.id as dept_id"
			,"sect.sect_name"
			,"sect.id as sect_id"

			,"dept.dept_code as dept_code"
			//FOR MOC
				,"dept.dept_type as dept_type"
			//END FOR MOC
		 ]);
		 
        
        
		if(!empty($emp))
		{
            
			Session::put("logged_in", TRUE);
			Session::put("employee_id", $emp->employee_id);
			Session::put("username", $emp->username);
			Session::put('email',$emp->email);
			Session::put("employeeid", $emp->employeeid);
			Session::put("new_employeeid", $emp->new_employeeid);
            $employee_name = $emp->firstname . " " . ((!empty($emp->middlename)) ? $emp->middlename . ". " : "") . $emp->lastname;
            Session::put("employee_name", $employee_name);
            Session::put("full_name", $emp->firstname." ".$emp->lastname);
			Session::put("firstname", $emp->firstname);
			Session::put("lastname", $emp->lastname);
			Session::put("middlename", $emp->middlename);
			Session::put("desig_level", $emp->desig_level);
			Session::put("company", $emp->company);
			Session::put("designation", $emp->designation);
			Session::put("superiorid", $emp->superiorid);
			Session::put("dept_name", $emp->dept_name);
			Session::put("dept_type", $emp->dept_type);
			Session::put("dept_id", $emp->dept_id);
			Session::put("sect_name", $emp->sect_name);
			Session::put("sect_id", $emp->sect_id);

			//FOR MOC
			Session::put("dept_type", $emp->dept_type);
			//END FOR MOC

			$is_taf_receiver = Receivers::is_taf_receiver($emp->employee_id);
			Session::put("is_taf_receiver", $is_taf_receiver);
			
            $is_itr_bsa = Receivers::is_itr_bsa($emp->employee_id);
            Session::put('is_itr_bsa', $is_itr_bsa);
            
            $is_citd_employee = ItrRequests::check_citd_employee($emp->employee_id);
            Session::put('is_citd_employee', $is_citd_employee);
            
            $is_system_satellite = ItrRequests::check_satellite_systems($emp->employee_id);
            Session::put('is_system_satellite', $is_system_satellite);
			
			$is_cbr_receiver = Receivers::is_cbr_receiver($emp->employee_id);
            Session::put('is_cbr_receiver', $is_cbr_receiver);
			
			$is_cbr_staff = Receivers::is_cbr_staff($emp->employee_id);
            Session::put('is_cbr_staff', $is_cbr_staff);
			
			$is_head = Receivers::isHead($emp->employeeid);
			Session::put('is_head',$is_head);
			
			$is_chrd_trainee = Receivers::isCHRDTrainee($emp->employeeid);
			Session::put('is_chrd_trainee',$is_chrd_trainee);
            
			$is_chrd_vp = Receivers::isVpForChrd($emp->employeeid);
			Session::put('is_chrd_vp',$is_chrd_vp);
			
			$is_svp_for_cs = Receivers::isSvpForCorpServ($emp->employeeid);
			Session::put('is_svp_for_cs',$is_svp_for_cs);
			
			$is_pres = Receivers::isPresident($emp->employeeid);
			Session::put('is_pres',$is_pres);

			// Added by Jenary

			$is_lrf_receiver = Receivers::is_lrf_receiver($emp->employee_id);
			Session::put('is_lrf_receiver',$is_lrf_receiver);

			$is_clinic_receiver = Receivers::is_clinic_receiver($emp->employee_id);
			Session::put('is_clinic_receiver',$is_clinic_receiver);

			$is_notif_receiver = Receivers::is_notif_receiver($emp->employee_id);
			Session::put('is_lrf_receiver',$is_lrf_receiver);

			// for MSR
			Session::put("dept_code", $emp->dept_code);
			$is_msr_receiver = Receivers::is_msr_receiver($emp->employee_id);
			Session::put('is_msr_receiver',$is_msr_receiver);
			
			// end for MSR
				// for moc
					$is_div_head = Receivers::is_div_head($emp->employee_id);
					Session::put('is_div_head',$is_div_head);

					$is_bsa = Receivers::is_bsa_receiver($emp->employee_id);
					Session::put('is_bsa',$is_bsa);
				// end for moc
			// End Add by Jenary
			//get access for clearance JMA
			Receivers::get_access($emp->employee_id);
			$result_arr["logged_in"] = TRUE;

            // for MRF
            $mrf_is_chrd_head = Receivers::mrf_is_chrd_head($emp->employee_id);
            Session::put('mrf_is_chrd_head',$mrf_is_chrd_head);



		}
		else
		{
			Session::put("logged_in", FALSE);
			$result_arr["logged_in"] = FALSE;
			$result_arr["message"] = "Invalid Username or Password";
		}
		
		echo json_encode($result_arr);
		
	}
	
    
    //for CURL
    public function sign_in2()
    {
        
        $result = false;
        
		$username = Input::get("username", null, true);
		$password = md5(Input::get("password", null, true));
        $employee_id = intval(Input::get("employee_id", null, true));
        
		$token = Input::get("token", null, true);
        
		$emp = Employees::leftJoin("departments as dept", function($join){
		 
			$join->on("employees.departmentid", "=", "dept.id");
			
		 })->leftJoin("sections as sect", function($join){
		 
			$join->on("employees.sectionid", "=", "sect.id");
			
		 })->where("employees.username", "=", $username)->where("employees.password","=", $password)->where("employees.active", "<>", "0")->first([
			 "employees.id as employee_id"
			,"employees.employeeid"
			,"employees.new_employeeid"
			,"employees.firstname"
			,"employees.middlename"
			,"employees.lastname"
			,"employees.desig_level"
			,"employees.company"
			,"employees.designation"
			,"employees.superiorid"
			,"dept.dept_name"
			,"dept.dept_type"
			,"dept.id as dept_id"
			,"sect.sect_name"
		 ]);
		
        
		if(!empty($emp))
		{
            
            $employee_id = $emp->employee_id;    
            SessionToken::where("emp_id", "=", "{$employee_id}")->delete();
            
            $session_token = new SessionToken;
            $session_token->session_token = $token;
            $session_token->emp_id = $employee_id;
            $session_token->save();
            
            $result = TRUE;
		}
        
        
		return $result;
    }
    
	
	//
	public function sign_out()
	{
		Session::flush();
		return Redirect::to("login");
	}
    
    
    public function sign_out2()
	{
        $result = false;
		$tk = Input::get("token", null, true);
        
        try {
            
            SessionToken::where("session_token", "=", "{$tk}")->delete();
        
        if($affected_rows > 0)
        {
            Session::flush(); //Clear session for laravel;
            $result = true;
            return $result;
        }
        else
        {
           return $result;
        }
        }
        catch (\Exception $e) {
            
            return FALSE;
            throw $e;
            
        }
        
        
	}
	
}
//End of File