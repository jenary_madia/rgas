<?php

use RGAS\Modules\NS\NS;
use RGAS\Modules\NS\ValidationProcess\Process as ValidationProcess;
use RGAS\Modules\NS\DataProcess\Process as DataProcess;
use RGAS\Libraries;
use RGAS\Modules\NS\Logs;


class NSController extends \BaseController {

    private $undertimeValidation;
    private $undertimeData;

    private $offsetValidation;
    private $offsetData;

	private $cutTimeValidation;
	private $cutTimeData;

    private $officialValidation;
    private $officialData;

    private $timeKeepValidation;
    private $timeKeepData;

    private $notifications;
    private $NS;
    public function __construct()
    {
        $this->logs = new Logs;
        $this->undertimeValidation = new ValidationProcess\Undertime;
        $this->undertimeData = new DataProcess\UndertimeData;

        $this->offsetValidation = new ValidationProcess\Offset;
        $this->offsetData = new DataProcess\OffsetData;

        $this->cutTimeValidation = new ValidationProcess\CutTime;
        $this->cutTimeData = new DataProcess\CutTimeData;

        $this->officialValidation = new ValidationProcess\Official;
        $this->officialData = new DataProcess\OfficialData;

        $this->timeKeepValidation = new ValidationProcess\TimeKeep;
        $this->timeKeepData = new DataProcess\TimeKeepData;

        $this->notifications = new Notifications;
        $this->NS = new NS;
    }

    public function index()
	{
        Notifications::whereRaw("CONCAT(documentcode,'-',codenumber) = $refno")
            ->first()['id'];
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['notificationType'] = [];
        $data['UTRestDays'] = ["Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ];
        $selectItems = (new SelectItems)->getItems('notif','notifType');
        foreach ($selectItems as $key) $data['notificationType'][$key->item] = $key->text;
        $data["base_url"] = URL::to("/");
        return View::make("ns/create", $data);
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function action($id = null)
	{
        if(Input::has("notificationType")) {
            switch (Input::get("notificationType")) {
                case "undertime":
                    $validation = $this->undertimeValidation;
                    $dataParse = $this->undertimeData;
                    break;
                case "offset":
                    $validation = $this->offsetValidation;
                    $dataParse = $this->offsetData;
                    break;
                case "cut_time":
                    $validation = $this->cutTimeValidation;
                    $dataParse = $this->cutTimeData;
                    break;
                case "TKCorction":
                    $validation = $this->timeKeepValidation;
                    $dataParse = $this->timeKeepData;
                    break;
                case "official":
                    $validation = $this->officialValidation;
                    $dataParse = $this->officialData;
                    break;

            }
            $action = Input::get("action");
            if ($action == "send") {
                return $this->NS->sendProcess($validation,$dataParse);
            }elseif ($action == 'save') {
                return $this->NS->saveProcess($validation,$dataParse);
            }else{
                return $this->NS->reprocess($validation, $dataParse, $id);
            }

        }else{
            return Redirect::back()
                ->withInput()
                ->with('errorMessage', 'Select a notification type');
            exit;
        }

    }
    
    public function myNotifAction($id) {
        $action = Input::get("action");
        if ($action == 'cancel') {
            
            $validate = Validator::make(
                ['comment' => Input::get("comment")],
                ['comment' => 'required']
            );

            if ($validate->fails())
            {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validate)
                    ->with('message', 'Some fields are incomplete.');
            }
            
            return $this->NS->cancelProcess($id);
        }
    }
    
    public function hierarchyAction($id) {
        $action = Input::get("action");
        if ($action == 'sendToHR') {
            return $this->NS->sendToHr($id);
        }elseif($action == "return") {
            return $this->NS->returnToFiler($id);
        }elseif($action == "request") {
            return $this->NS->request($id);
        }
    }

    public function lists()
    {
        switch (Input::get("action")) {
            case "undertime":
                $validation = $this->undertimeValidation;
                $dataParse = $this->undertimeData;
                break;
            case "offset":
                $validation = $this->offsetValidation;
                $dataParse = $this->offsetData;
                break;
        }
        $data["base_url"] = URL::to("/");
        return View::make("ns.lists",$data);
    }

    public function myNotifications($id,$method)
    {
        $data["base_url"] = URL::to("/");
        $data["notifDetails"] = Notifications::getNotifications($id,1);

        if (! $data["notifDetails"])
        {
            return Redirect::to('ns/notifications')
                ->with('errorMessage', 'You\'ve looking for invalid notification');
        }
        $data["method"] = $method;
        $data["notifSignatories"] = Notifications::find($id)->signatories()->with('employee')->get();
        if ($method == 'view') {
            $data["notifAdditionalDetails"] = NotificationDetails::where("notificationid",$id)->get();
            $details_total = [];
            foreach ($data["notifAdditionalDetails"] as $key) {
                array_push($details_total,$key['total_time']);
            }

            $data['total'] = array_sum($details_total);
            return View::make("ns.my_notifications.view",$data);
        }elseif($method == 'edit') {
            $data['UTRestDays'] = ["Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            ];
            if (! (in_array($data["notifDetails"]['status'],json_decode(EDITABLE,true))))
            {
                return Redirect::to('ns/notifications')
                    ->with('errorMessage', 'Unable to edit this notification');
            }
            $data['notificationType'] = [];
            $selectItems = (new SelectItems)->getItems('notif','notifType');
            foreach ($selectItems as $key) $data['notificationType'][$key->item] = $key->text;
            $data["base_url"] = URL::to("/");
            $data["notifAdditionalDetails"] = (new NotificationDetails())->parseDetails($id);
            return View::make("ns.my_notifications.edit_my_notif",$data);
        }

        return Redirect::to('ns/notifications');

    }

    public function processMyNS() {
        return $this->NS->processMyNS(Input::get("action"));
    }

    public function viewNotif($id,$method=null)
	{
        $data["base_url"] = URL::to("/");
        $data["notifDetails"] = Notifications::getNotifications($id,2);
        $data["notifAdditionalDetails"] = NotificationDetails::where("notificationid",$id)->get();
        $details_total = [];
            foreach ($data["notifAdditionalDetails"] as $key) {
                array_push($details_total,$key['total_time']);
            }

            $data['total'] = array_sum($details_total);
        if (! $data["notifDetails"])
        {
            return Redirect::to('ns/notifications')
                ->with('errorMessage', 'You\'ve looking for invalid notification');
        }
        $data["method"] = $method;
        $data["notifSignatories"] = Notifications::find($id)->signatories()->with('employee')->get();
        if (in_array(Session::get("company"),json_decode(CORPORATE,true)))
        {
            $data['allowed'] = json_decode(APPROVERS_CORP,true);
            if (Session::get("dept_id") == 2)
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),array('top mngt','president'),Session::get("dept_id"));
            else{
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),array('supervisor','head'),Session::get("dept_id"));
            }

        }
        elseif(Session::get("company") == "SFI-MM")
        {
            $data['allowed'] = array("om","admin-aom","plant-aom","vpo","vp-ppic");
            $data["otherApprovers"] = Employees::where("id",Session::get("superiorid"))->get();
        }
        else
        {
            $data['allowed'] = array("om","admin-aom","plant-aom","vpo","vp-ppic");
            if (Session::get("dept_id") == 2)
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),array('top mngt','president'),Session::get("dept_id"));
            else{
                $data["otherApprovers"] = Employees::getByDesigLevelOnDept(Session::get("employee_id"),array('supervisor','head'),Session::get("dept_id"));
            }

        }

        return View::make("ns.is",$data);
    }
    
    public function productionEncoders()
    {
        $data['departments'] = [];
        $departmentsToParse = Departments::getDepartmentsByCompany(Session::get("company"));
        foreach ($departmentsToParse as $key) $data['departments'][$key->id] = $key->dept_name;
        
        $data['notificationType'] = [];
        $selectItems = (new SelectItems)->getItems('notif','notifType');

        for ($i=0; $i < count($selectItems); $i++) {
            if($selectItems[$i]['item'] == "undertime") {
                $selectItems = [$selectItems[$i]];
            }
        }

        foreach ($selectItems as $key) $data['notificationType'][$key->item] = $key->text;

        $data["base_url"] = URL::to("/");
        return View::make("ns.pe_create", $data);
    }

    public function peAction($id = null){
        if (Input::get("action") == "toSave") {
            return $this->NS->PESaveNotif($id);
        }elseif(Input::get("action") == "toSend"){
            return $this->NS->PECreateNotif();
        }elseif(Input::get("action") == "cancel"){
            return $this->NS->PECancelNotif($id);
        }elseif(Input::get("action") == "resend"){
            return $this->NS->PEResend($id);
        }elseif (Input::get("action") == "resave") {
            return $this->NS->PEResave($id);
        }elseif (Input::get("action") == "return") {
            return $this->NS->PEReturn($id);
        }
    }
    
    public function peISAction($id){
        if (Input::get("action") == "request") {
            return $this->NS->PESend($id);
        }elseif(Input::get("action") == "toHR") {
            return $this->NS->PESendToHR($id);
        }elseif(Input::get("action") == "return") {
            $validate = Validator::make(
                array('comment' => Input::get("comment")),
                array('comment' => 'required'),
                array('comment.required' => 'Comment is required')
            );

            if ($validate->fails())
            {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validate)
                    ->with('message', 'Some fields are incomplete.');
                exit;

            }
            return $this->NS->PEReturn($id);
        }
    }

    public function peView($id,$method = null)
    {
        $data['approvers'] = Employees::where("company",Session::get("company"))
            ->whereIn("desig_level",array("head","supervisor"))
            ->where("id","!=",Session::get("employee_id"))
            ->orderBy('desig_level', 'DESC')
            ->get();
        $data['departments'] = [];
        $data['departments'][0] = '--SELECT DEPARTMENT--';
        $departmentsToParse = Departments::getDepartmentsByCompany(Session::get("company"));
        foreach ($departmentsToParse as $key) $data['departments'][$key->id] = $key->dept_name;

        $data['notificationType'] = [];

        $selectItems = (new SelectItems)->getItems('notif','notifType');
        for ($i=0; $i < count($selectItems); $i++) {
            if($selectItems[$i]['item'] == "undertime") {
                $selectItems = [$selectItems[$i]];
            }
        }
        foreach ($selectItems as $key) $data['notificationType'][$key->item] = $key->text;
        $data['method'] = $method;
        $data['notification'] = NotificationBatch::where("id", $id)
            ->with('batchDetails')
            ->with('department')
            ->with('section')
            ->where('curr_emp',Session::get("employee_id"))
            ->whereIn('status',array("FOR APPROVAL","RETURNED"))
            ->first();
        $data['process'] = RouteProcess::where("transaction_id",$id)
            ->where("module",2)
            ->get(['process']);
        if (Request::ajax())
        {

            NotificationBatchDetails::where('notification_batch_id',$id)->update(["noted" => 0]);
            return $data['notification']['batchDetails'];
            
        }
        if (! $data["notification"]) {
            return Redirect::to('ns/notifications')
                ->with('errorMessage', 'You\'ve looking for invalid leave');
        }
        $data["base_url"] = URL::to("/");
        return View::make("ns.pe_view", $data);
    }

    public function peViewMyRequest($id,$method = null) {
        $data['notifDetails'] = NotificationBatch::where("id", $id)
            ->with('batchDetails')
            ->with('department')
            ->with('section')
            ->where('ownerid',Session::get("employee_id"))
            ->first();
        if (! $data["notifDetails"]) {
            return Redirect::to('ns/notifications')
                ->with('errorMessage', 'You\'ve looking for invalid leave');
        }
        if (Request::ajax())
        {
            $parsedData= array();
            for ($i=0; $i < count($data['notifDetails']['batchDetails']); $i++) {
                $parsedData[$i]['employeeName'] = $data['notifDetails']['batchDetails'][$i]['employee_name'];
                $parsedData[$i]['employeeNumber'] = $data['notifDetails']['batchDetails'][$i]['employee_number'];
                $parsedData[$i]['notificationType'] = $data['notifDetails']['batchDetails'][$i]['noti_type'];
                $parsedData[$i]['notifName'] = $data['notifDetails']['batchDetails'][$i]['notif']['text'];
                $parsedData[$i]['date'] = $data['notifDetails']['batchDetails'][$i]['from_date'];
                $parsedData[$i]['timeIn'] = $data['notifDetails']['batchDetails'][$i]['from_in'];
                $parsedData[$i]['timeOut'] = $data['notifDetails']['batchDetails'][$i]['from_out'];
                $parsedData[$i]['reason'] = $data['notifDetails']['batchDetails'][$i]['reason'];
                $parsedData[$i]['employeeID'] = $data['notifDetails']['batchDetails'][$i]['employee_id'];
                $parsedData[$i]['active'] = 'false';
            }
            return $parsedData;
        }
        $data['method'] = $method;
        $data["base_url"] = URL::to("/");
        if ($method == 'view') {
            return View::make("ns.pe_view_my_request", $data);
        }else{
            if (! (in_array($data["notifDetails"]['status'],json_decode(EDITABLE,true))))
            {
                return Redirect::to('ns/notifications')
                    ->with('errorMessage', 'Unable to edit this notification');
            }
            $data['departments'] = [];
            $departmentsToParse = Departments::getDepartmentsByCompany(Session::get("company"));
            foreach ($departmentsToParse as $key) $data['departments'][$key->id] = $key->dept_name;

            $data['notificationType'] = [];
            $selectItems = (new SelectItems)->getItems('notif','notifType');
            for ($i=0; $i < count($selectItems); $i++) {
                if($selectItems[$i]['item'] == "undertime") {
                    $selectItems = [$selectItems[$i]];
                }
            }
            foreach ($selectItems as $key) $data['notificationType'][$key->item] = $key->text;
            return View::make("ns.pe_edit_my_request", $data);
        }
    }

    public function peProcessMyNS() {
        return $this->NS->PEProcessMyNS(Input::get("action"));
    }

//    AJAX
    public function getMyNotifications()
    {
        if (Request::ajax())
        {
            return $this->notifications->myNotifications();
        }
    }

    public function getMyPENotifications()
    {
        if (Request::ajax())
        {
            return (new NotificationBatch())->myNotifications();
        }
    }

    public function getSuperiorNotifications()
    {
        if (Request::ajax())
        {
            return $this->notifications->superiorNotifications();
        }
    }

    public function getSuperiorPENotifications()
    {
        if (Request::ajax())
        {
            return (new NotificationBatch())->superiorNotifications();
        }
    }

    public function postSections(){
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        if (Request::ajax())
        {
            return Departments::find(Input::get("dept_id"))->sections()->get();
        }
    }

    public function postEmployees(){
        if (Request::ajax())
        {
            $employees = EmployeesPlant::where("departmentid",Input::get("dept_id"));
            if (Input::get("sect_id")) {
                $employees->where("sectionid",Input::get("sect_id"))->get();
            }
            return $employees->get();
        }
    }
    
    public function postParseEmployees(){
        if (Request::ajax()) {
            switch (Input::get("notificationType")) {
                case "offset":
                    $notifName = "Offset";
                    break;
                case "TKCorction":
                    $notifName = "Time Keeping Correction";
                    break;
                case "undertime":
                    $notifName = "Undertime";
                    break;
                case "cut_time":
                    $notifName = "Cut time";
                    break;
                case "official":
                    $notifName = "Official Business";
                    break;
            }
            return array_merge(Input::all(), array('notifName' => $notifName));
        }
    }

    public function noteBatchDetails() {
        if (Input::get("note")) {
            $batchDetails = NotificationBatchDetails::where("employee_number",Input::get("employeeNumber"))
                ->where("notification_batch_id",Input::get("batchID"));
            $batchDetails->update(array(
                "noted" => 1
            ));
        }else{
            $batchDetails = NotificationBatchDetails::where("employee_number",Input::get("employeeNumber"))
                ->where("notification_batch_id",Input::get("batchID"));
            $batchDetails->update(array(
                "noted" => 0
            ));
        }
    }

    public function downloadAttachments($refNum, $random_filename, $original_filename) {
        $parsedRefno = explode('-',$refNum);
        $dataID = Notifications::where("documentcode",$parsedRefno[0].'-'.$parsedRefno[1])
            ->where("codenumber",$parsedRefno[2])->first(['id']);
        $fm = new Libraries\FileManager;
        $this->logs->AU008($dataID['id'],'notifications','id',$refNum);
        $filepath = $this->NS->getStoragePath() . "$refNum/" . $random_filename;
        if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
            $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
        }
        return $fm->download($filepath, CIEncrypt::decode($original_filename));
    }

    public function forEditOffsetDetails() {
        if(Request::ajax()) {
            $results = NotificationDetails::where('notificationid',Input::get("notif_id"))->get();
            $parsedResult = [];
            for ($i=0; $i < count($results); $i++) {
                $parsedResult[$i] = [
                    "OTDate" => $results[$i]['date'],
                    "type" => $results[$i]['type'],
                    "timeInStartTime" => date('H:i',strtotime($results[$i]['time_start'])),
                    "timeOutEndTime" => date('H:i',strtotime($results[$i]['time_end'])),
                    "OTDuration" => $results[$i]['total_time'],
                    "reason" => $results[$i]['reason'],
                    "active" => false,
                    "OBDestination" => $results[$i]['destination']
                ];
            }
            return $parsedResult;
        }
    }

    public function checkDateValidity() {
        if(Request::ajax()) {
            $overtimes = overtimes::where("employeeid",Session::get("employee_id"))
                ->where("status","APPROVED")
                ->with("OTLists")
                ->get();

            // return $overtimes;

            $container = [];
            foreach ($overtimes as $ot) {

                if(count($ot['OTLists']) >= 1) {
                    array_push($container,$ot['OTLists']);
                }

            }

            if ($container) {
                return 0;
            }

            return 1;

        }
    }

}
