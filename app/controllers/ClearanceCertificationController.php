<?php

class ClearanceCertificationController extends \BaseController 
{    

	public function __construct()
	{
		//
	}

    public function create()
	{
        $data["base_url"] = URL::to("/");		
        return View::make("cc/screen2_create", $data);    
    }
	
	public function review_clearance(){
		$data["base_url"] = URL::to("/");		
        return View::make("cc/review_clearance", $data);  
	}
	
	
}