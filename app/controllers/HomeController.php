<?php

class HomeController extends BaseController {
    
    
	public function index()
	{
	
		if(!Session::has('logged_in')){
			return Redirect::to('login');
			die;
		}
		$data["base_url"] = URL::to("/");
		return View::make("home", $data);
	}
    
    
    //For pop up only
    public function notice_page()
    {
        $data["base_url"] = URL::to("/");
        return View::make("notice", $data);    
    }
    
    // Update Hierarchy table
	public function update_hierarchy()
    {
        $department_lists = Departments::get_departments();
        
        if (!empty ($department_lists))
        {
            DB::table('hierarchies')->truncate();
        
            foreach ($department_lists as $depts)
            {
                $rs_emps = Employees::get_all($depts->id);
                
                if (!empty($rs_emps))
                {
                    foreach ($rs_emps as $emps)
                    {
                        $h = new Hierarchies;
                        $h->employee_id = $emps->id;
                        $h->superior_id = $emps->superiorid;
                        $h->department_id = $emps->departmentid;
                        $h->department_hrms_cd = $emps->departmentid2;
                        $h->name = $this->mb_convert($emps->name);
                        $h->superior = $this->mb_convert($emps->superior);
                        $h->designation = $this->mb_convert($emps->desig_level);
                        $h->save();
                    }
                }
                
                unset ($rs_emps);
            }
        }
    }
    
    function mb_convert($string, $mode = MB_CASE_TITLE)
    {
        $string = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
        $string = mb_convert_case($string, $mode, 'UTF-8');

        return trim($string);
    }
}
