<?php

use RGAS\Modules\CBR;

class CbrController extends \BaseController 
{    
	private $CBR;
	public function __construct()
	{
		$this->CBR = new CBR\CBR;
	}

    public function create()
	{
        $data["base_url"] = URL::to("/");
		$data["urgency"] = CompensationBenefits::urgency();
		$data["reference_number"] = ReferenceNumbers::get_current_reference_number('cbr');
		
        return View::make("cbr/create", $data);    

    }
	
	public function action()
	{
		$input = Input::all();

		if($input['action'] == 'save')
		{
			$is_save = $this->CBR->saveRequest($input);
			if($is_save === true){
				ReferenceNumbers::increment_reference_number('cbr');
				return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been saved!');
			}
		}
		
		if($input['action'] == 'send')
		{
			$request = new CBR\Requests\Store;

			$is_valid = $request->validate();
			if($is_valid === true){
				$is_sent = $this->CBR->sendRequest($input);
				if($is_sent === true){
					ReferenceNumbers::increment_reference_number('cbr');
					return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been sent!');
				}
			}
			else {
				return $is_valid;
			}
		}
	}
	
	public function SubmittedCBRRequests()
	{
		$data["base_url"] = URL::to("/");
		return View::make("cbr/submitted_cbr_requests", $data);
	}
	
	public function RequestsNew()
	{
		$base_url = URL::to('/');
		$cb = new CompensationBenefits;
		$requests = $cb->getRequestsNew();
		
		return json_encode($requests);
	}
	
	public function RequestsMy()
	{
		$base_url = URL::to('/');
		$cbrd = new CompensationBenefitsRequestedDocs;
		$requests = $cbrd->getRequestsMy();
		
		return json_encode($requests);
	}
	
	public function RequestsInProcess()
	{
		$base_url = URL::to('/');
		$cb = new CompensationBenefits;
		$requests = $cb->getRequestsInProcess();
		
		return json_encode($requests);
	}
	
	public function RequestsAcknowledged()
	{
		$base_url = URL::to('/');
		$cb = new CompensationBenefits;
		$requests = $cb->getRequestsAcknowledged();
		
		return json_encode($requests);
	}

	public function view($cbrd_id)
	{
		$cbrd = new CompensationBenefitsRequestedDocs();
		$request = $cbrd->getRequestByCbrdRefNum($cbrd_id);

		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
		return View::make("cbr/request_view",$data);
	}
	
	public function process($cbrd_ref_num)
	{
		if (Request::isMethod('post'))
		{	
			$input = Input::all();
			
			$request = new CBR\Request;
			if($input['action'] == 'return')

			{				
				$request->remarks();
				
				if(!$request->getResult())
				{
					$data['messages'] = $request->getMessages();
				}
				else{
					// $cbrd = new CompensationBenefitsRequestedDocs;
					// $cbrd->returned();
					$this->CBR->returnRequest($input);
				}
			}

			elseif($input['action'] == 'assign')
			{	
				$request->assign();

				if(!$request->getResult())
				{
					$data['messages'] = $request->getMessages();
				}
				else{

					$this->CBR->assignRequest($input);
					return Redirect::to('cbr/review-request/'.$cb_ref_num)->with('message', 'Assigned');

				}
			}
		}
		
		$data["base_url"] = URL::to("/");
		
		$cb = new CompensationBenefitsRequestedDocs();
		$request = $cb->getRequestByCbrdRefNum($cbrd_ref_num);
		$data["request"] = $request;
		
		$chrd = Employees::get_all(16);
		$data["chrd"] = $chrd;
		
		// return View::make("cbr/request_for_processing",$data);
		// return View::make("cbr/for_acknowledge_cbr",$data);
		return View::make("cbr/request_for_acknowledgement",$data);
		// return View::make("cbr/received_cbr",$data);
	}
	
	public function reviewRequest($cb_ref_num, $view="")
	{
		if (Request::isMethod('post'))
		{	
			$input = Input::all();
			

			
			if($input['action'] == 'return')
			{
				$request = new CBR\Requests\SendBack;
				$is_valid = $request->validate();
				if($is_valid === true)
				{
					$this->CBR->returnRequest($input);
				}
				return $is_valid;
			}
			elseif($input['action'] == 'assign')
			{
				$request = new CBR\Requests\Assign;
				$is_valid = $request->validate();
				if($is_valid === true)
				{
					$this->CBR->assignRequest($input);
					return Redirect::to('cbr/review-request/'.$cb_ref_num)->with('message', 'Assigned');
				}
				return $is_valid;
			}
		}
		
		
		$data["base_url"] = URL::to("/");
		
		$cb = new CompensationBenefits;
		
		$request = $cb->getRequest($cb_ref_num);
		
		$data["request"] = $request[0];
		$data["view"] = false;
		if($view) $data["view"] = true;


		$chrd = Employees::get_all(16);
		$data["chrd"] = $chrd;
		
		return View::make("cbr/request_review",$data);
		// return View::make("cbr/received_cbr",$data);
	}


	public function forAcknowledgement($cbrd_ref_num)
	{
		$input = Input::all();
		$request = new CBR\Requests\ForAcknowledgement;
		$request->validate();
		
		if(!$request->getResult())
		{
			$data['messages'] = $request->getMessages();
			echo "<pre>";
			print_r($data);
			echo "</pre>";
			return Redirect::to('cbr/Contracts/'.$cbrd_ref_num)->with('data', $data);
		}
		else{
			$this->CBR->forAcknowledgement($input);
			return Redirect::to('cbr/submitted-cbr-requests');
		}
	}
	
	public function acknowledged()
	{
		$input = Input::all();

		// echo "<pre>",print_r($input),"</pre>";
		$this->CBR->acknowledgedRequest($input);
		return Redirect::to('cbr/submitted-cbr-requests');
	}
	
	public function edit($cbrd_id)
	{
		$cbrd = new CompensationBenefitsRequestedDocs();
		$request = $cbrd->getRequestByCbrdRefNum($cbrd_id);

		$data["base_url"] = URL::to("/");
		$data["request"] = $request;

		return View::make("cbr/request_edit", $data);    
	}
	
	public function resubmit()
	{
		$input = Input::all();
		echo "<pre>",print_r($input),"</pre>";
		$this->CBR->reSubmitRequest($input);
		return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been re-submitted');
	}
	
}