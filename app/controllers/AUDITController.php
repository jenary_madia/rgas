<?php

use RGAS\Modules\AUDIT;

class AUDITController extends \BaseController 
{

	public function __construct()
	{		
		$this->AUDIT = new AUDIT\AUDIT;	
	}

        
	public function index()
	{
		if(!Session::get("is_audit_receiver")) return Redirect::to('/');

		$data["base_url"] = URL::to("/");
        $data["module"] =  AuditTrails::modules();
        $data["employees"] =  AuditTrails::employees();
		return View::make("audit/index" , $data);   
	}

        
	public function getRecordlist()
	{	
            return AuditTrails::records();
	}
        
        
	public function exportAudit()
	{	
            return $this->AUDIT->auditlist();
	}        
        
}
