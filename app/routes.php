<?php
use RGAS\Libraries;
/*
|--------------------------------------------------------------------------
| Application Routes
|-------------------------------------------------------------------------
<<<<<<< HEAD
=======

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
>>>>>>> 6c6f9bd1e296b3c4d12f2b6b443abb5140da0cbc
=======
>>>>>>> 5b637679bb4c577b0b807f01d9c189988b098eeb
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



//App::bind('RGAS\Modules\NS\Contracts\ProcessFlowRepository', function($app) {
//    return [
//        App::make('RGAS\Modules\NS\Process\CorporateFlow'),
//    ];
//});
//
//App::bind('NSController', function($app) {
//    return new NSController(
//        App::make('RGAS\Modules\NS\Contracts\ProcessFlowRepository')
//    );
//});



//add these on your routes:

//for file upload
Route::resource('file-uploader', 'FileUploaderController');


//for downloading
Route::get('download/{te_ref_num}/{random_filename}/{original_filename}', 'TrainingEndorsementController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');




Route::get('employees_by_department/{dep_id}', function($dep_id){
	$employees = Employees::get_all($dep_id)->toJson();
	echo $employees;
});

// Route::group(array('before' => 'auth'), function()
// {
	//--
	Route::group(array('prefix' => 'cbr'), function()
	{
		Route::get('create', 'CbrController@create');
		Route::post('create', 'CbrController@action');
		Route::get('submitted-cbr-requests', 'CbrController@SubmittedCBRRequests');
		// Called via Ajax
		Route::get('requests-new', 'CbrController@RequestsNew');
		Route::post('requests-new', 'CbrController@RequestsNew');
		
		Route::get('requests-my', 'CbrController@RequestsMy');
		Route::post('requests-my', 'CbrController@RequestsMy');
		
		Route::get('requests-in-Contracts', 'CbrController@RequestsInProcess');
		Route::post('requests-in-Contracts', 'CbrController@RequestsInProcess');
		
		Route::get('requests-acknowledged', 'CbrController@RequestsAcknowledged');
		Route::post('requests-acknowledged', 'CbrController@RequestsAcknowledged');
		//--
		
		// Route::get('requests-new', 'CbrController@getRequestsNew');
		// Route::post('requests-new', 'CbrController@postRequestsNew');
		Route::get('view/{cb_id}', 'CbrController@view');
		Route::get('review-request/{cb_ref_num}', 'CbrController@reviewRequest');
		Route::post('review-request/{cb_ref_num}', 'CbrController@reviewRequest');	
		Route::get('Contracts/{cb_ref_num}', 'CbrController@Contracts');
		Route::post('for-acknowledgement/{cbrd_ref_num}', 'CbrController@forAcknowledgement');
		
		Route::get('edit/{cb_id}', 'CbrController@edit');
		Route::get('received-transaction', 'CbrController@received_cbr_transaction');
		
		Route::get('download/attachment/{random_filename}/{original_filename}', function($random_filename, $original_filename)
		{

			$fm = new Libraries\FileManager;
			$filepath = storage_path() . '/rgas/cbr/attachments/' . $random_filename;
			return $fm->download($filepath, CIEncrypt::decode($original_filename));
		}) 
		->where('random_filename', '[A-Za-z0-9\-\_\.]+');
	});
	
	/* Training Endorsement*/
	Route::group(array('prefix' => 'te'), function()
	{
		Route::get('create', 'TrainingEndorsementController@create');
		Route::post('create', 'TrainingEndorsementController@action');
		Route::get('submitted-te-requests', 'TrainingEndorsementController@SubmittedTERequests');
		//Called via AJAX
		Route::get('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		Route::post('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		
		Route::get('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		Route::post('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		

		Route::get('endorsements_chrd_vp_approval', 'TrainingEndorsementController@EndorsementsChrdVpApproval');
		Route::post('endorsements_chrd_vp_approval', 'TrainingEndorsementController@EndorsementsChrdVpApproval');
		
		Route::get('endorsements_svp_for_cs_approval', 'TrainingEndorsementController@EndorsementsSvpCsApproval');
		Route::post('endorsements_svp_for_cs_approval', 'TrainingEndorsementController@EndorsementsSvpCsApproval');
		
		Route::get('endorsements_pres_approval', 'TrainingEndorsementController@EndorsementsPresApproval');
		Route::post('endorsements_pres_approval', 'TrainingEndorsementController@EndorsementsPresApproval');
		

		//Requestor's
		Route::get('view/{te_id}', 'TrainingEndorsementController@view');
		Route::get('view-approved/{te_id}', 'TrainingEndorsementController@view_approved');
		Route::get('edit/{te_id}', 'TrainingEndorsementController@edit');
		Route::post('edit/{te_id}', 'TrainingEndorsementController@editAction');

		//CHRD
		Route::get('assess/{te_id}', 'TrainingEndorsementController@assessChrd');
		Route::post('assess/{te_id}', 'TrainingEndorsementController@workflow');
		
		//vphr,svp,pres approval		
		Route::get('view-for-approval/{te_id}', 'TrainingEndorsementController@viewForApproval');
		
		Route::get('Contracts-for-approval/{te_id}', 'TrainingEndorsementController@processForApproval');
		Route::post('Contracts-for-approval/{te_id}', 'TrainingEndorsementController@actionForApproval');
		
		Route::get('delete/{te_id}', 'TrainingEndorsementController@delete');
		Route::get('delete-participant/{tep_id}', 'TrainingEndorsementController@delete_participant');

		
		Route::get('download/attachment/{random_filename}/{original_filename}', function($random_filename, $original_filename)
		{
			$fm = new RGAS\FileManager;
			$filepath = storage_path() . '/te/attachments/' . $random_filename;
			return $fm->download($filepath, $original_filename);
		})
		->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		
	});


	// /* PMARF */
	Route::group(array('prefix' => 'pmarf'), function()
	{

		Route::get('create', 'PmarfController@create_pmarf');
		Route::post('create', 'PmarfController@action');
		Route::get('view_save', 'PmarfController@view_pmarf');
		Route::post('view_save', 'PmarfController@view_pmarf');
		
		Route::get('save_pmarf', 'PmarfController@get_my_save_pmarf');
		Route::post('save_pmarf', 'PmarfController@get_my_save_pmarf');
		
		Route::get('approve/{pmarf_id}', 'PmarfController@approve_pmarf');
		Route::post('approve/{pmarf_id}', 'PmarfController@approve_pmarf');
		
		Route::get('edit/{pmarf_id}', 'PmarfController@edit_pmarf');
		Route::post('edit/{pmarf_id}', 'PmarfController@edit_pmarf');
		
		Route::get('delete/{pmarf_id}', 'PmarfController@delete_pmarf');
		Route::post('delete/{pmarf_id}', 'PmarfController@delete_pmarf');
		
		Route::get('send/{pmarf_id}', 'PmarfController@send_pmarf');
		Route::post('send/{pmarf_id}', 'PmarfController@send_pmarf');
		
		Route::get('view/{pmarf_id}', 'PmarfController@view');
		Route::post('view/{pmarf_id}', 'PmarfController@view');
		/*Route::post('create', 'TrainingEndorsementController@action');
		Route::get('submitted-te-requests', 'TrainingEndorsementController@SubmittedTERequests');
		//Called via AJAX
		Route::get('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		Route::post('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		Route::get('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		Route::post('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');*/
	});
	
	Route::get('employees/list', 'EmployeesController@employee_lists');
	Route::post('employees/get', 'EmployeesController@get_all_employees');
	// Route::get('notice', 'HomeController@notice_page');
	Route::get('hierarchyupdate', 'HomeController@update_hierarchy');

	
	/* Performance Management */
	Route::group(array('prefix' => 'pmf'), function()
	{
		Route::get('create', 'PmfController@create');
		Route::post('create', 'PmfController@action');
	});
	
	/* Clearance Certification */
	Route::group(array('prefix' => 'cc'), function()
	{
		Route::get('create', 'ClearanceCertificationController@create');
		Route::get('review_clearance', 'ClearanceCertificationController@review_clearance');
	});
		Route::get('index', 'CCController@index');
	//--
	
	/* Notice To Explain */
	Route::group(array('prefix' => 'nte'), function()
	{
		Route::get('index', 'NTEController@index');
	});
	//--
	
	/* Online Attendance Monitoring */
	Route::group(array('prefix' => 'oam'), function()
	{
		Route::get('index', 'OAMController@index');
	});
	//--

	
	/* Leave Request Form */
	Route::group(array('prefix' => 'lrf'), function()
	{
		Route::get('/', 'LRFController@index');
		Route::get('create','LRFController@create');
		Route::post('action/{id?}','LRFController@action');
		Route::post('processing/{id?}','LRFController@leaveAction');

		// For datatables
		Route::get('my_leaves','LRFController@getMyLeaves');
		Route::get('clinic_approval_leaves','LRFController@getSubmittedToClinic');
		Route::get('for_approval_leaves','LRFController@getForApproveLeaves');
		Route::get('my_pe_leaves','LRFController@PEMyRequests');
		Route::get('superior_pe_leaves','LRFController@PESuperiorRequests');

		// Receiver List
		Route::get('submitted_leaves','LRFController@submittedToReceiver');
		Route::post('search','LRFController@searchAction');


		// FILER , IS , DH List
		Route::get('leaves','LRFController@leaves');

		// IS List action
		Route::get('leave/{id}/{method?}','LRFController@viewLeave');

		// FILER List action
		Route::get('my_leaves/{id}/edit','LRFController@editMyLeave');
		Route::get('my_leaves/{id}/view','LRFController@viewMyLeave');

		// For Production Encoders
		Route::get('pe_create','LRFController@PECreateLeave');
		Route::post('pe_action/{id?}','LRFController@PEAction');

		// Show approved Leaves

		Route::get('show_leaves/{id}/{type?}','LRFController@showApprovedLeaves');

        //

        Route::post('validate_leave','LRFController@checkIfLeaveValid');
        
		// Production encoders

		Route::post('get_employees','LRFController@postParseEmployees');
		Route::get('pe_view/{id}/{method?}','LRFController@peView');
		Route::get('pe_view_my_request/{id}/{method?}','LRFController@peViewMyRequest');
		Route::post('submit/{id}','LRFController@peISAction');
		Route::post('note_batchdetails','LRFController@noteBatchDetails');

		// Clinic

		Route::get('{id}/clinic/{method?}','LRFController@clinic');

		//download attachments
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','LRFController@downloadAttachments');

		// For datatables at home page
		Route::get('home_my_leaves','LRFController@getHomeMyLeaves');
		Route::get('home_clinic_approval_leaves','LRFController@getHomeSubmittedToClinic');
		Route::get('home_for_approval_leaves','LRFController@getHomeForApproveLeaves');
		Route::get('home_superior_pe_leaves','LRFController@getHomePESuperiorRequests');

	});

	Route::group(array('prefix' => 'ns'), function() {
        Route::get('/','NSController@index');
        Route::get('/create','NSController@create');
        Route::post('/action/{id?}','NSController@action');
        Route::post('/hierarchy_action/{id}','NSController@hierarchyAction');
        Route::get('/notifications','NSController@lists');

//        ajax
        Route::get('/my_notifications','NSController@getMyNotifications');
        Route::get('/superior_notifications','NSController@getSuperiorNotifications');
        Route::get('/my_pe_notifications','NSController@getMyPENotifications');
        Route::get('/superior_pe_notifications','NSController@getSuperiorPENotifications');
        Route::post('/sections','NSController@postSections');
        Route::post('/employees','NSController@postEmployees');
        Route::post('/parse_employees','NSController@postParseEmployees');
        Route::post('/offset_details','NSController@forEditOffsetDetails');

		// IS List action
		Route::get('/for_approval/{id}/{method?}','NSController@viewNotif');


//      FILER action
        Route::get('/my_notifications/{id}/{method}','NSController@myNotifications');
        Route::post('/my_notif_actions/{id}','NSController@myNotifAction');

//		Production Encoder
        Route::get('/pe_create','NSController@productionEncoders');
        Route::post('/pe_action/{id?}','NSController@peAction');
        Route::get('/pe_view/{id}/{method?}','NSController@peView');
        Route::post('/submit/{id}','NSController@peISAction');
        Route::get('pe_view_my_request/{id}/{method?}','NSController@peViewMyRequest');
        Route::post('note_batchdetails','NSController@noteBatchDetails');

        // Production Encoder list table action
        Route::post('/pe-process','NSController@peProcessMyNS');

		//download attachments
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','NSController@downloadAttachments');

		// list table action
        Route::post('/process','NSController@processMyNS');

		//		OCheck If exist on overtime

        Route::post('/check-date-validity','NSController@checkDateValidity');

	});
	Route::group(array('prefix' => 'submitted'),function() {
		Route::get('leaves_notifications','SubmittedLeavesAndNotifications@index');
		Route::get('leaves','SubmittedLeavesAndNotifications@getLeaves');
        Route::get('notif_undertime','SubmittedLeavesAndNotifications@getNotifUndertime');
        Route::get('notif_offset','SubmittedLeavesAndNotifications@getNotifOffset');
        Route::get('notif_timekeep','SubmittedLeavesAndNotifications@getNotifTimekeep');
        Route::get('notif_official','SubmittedLeavesAndNotifications@getNotifOfficial');
        Route::get('notif_cuttime','SubmittedLeavesAndNotifications@getNotifCuttime');
        Route::post('search','SubmittedLeavesAndNotifications@search');

		Route::post('print','SubmittedLeavesAndNotifications@printExcel');
		
        //        viewing each application
		Route::get('leave/{id}/{method?}','SubmittedLeavesAndNotifications@leaveView');
		Route::post('leave/{id}','SubmittedLeavesAndNotifications@leaveAction');
		Route::get('notification/{id}/{method?}','SubmittedLeavesAndNotifications@notifView');
		Route::post('notification/{id}','SubmittedLeavesAndNotifications@notifAction');

		/*Production Encoders*/
		Route::get('pe_leaves_notifications','SubmittedLeavesAndNotifications@PEIndex');
		Route::get('pe_leaves','SubmittedLeavesAndNotifications@PEGetLeaves');
		Route::get('pe_notifications','SubmittedLeavesAndNotifications@PEGetNotifications');
		Route::post('pe_search','SubmittedLeavesAndNotifications@PESearch');

		Route::post('pe_print','SubmittedLeavesAndNotifications@PEPrintExcel');

        //        viewing each application
        Route::get('pe_leave/{id}/{method?}','SubmittedLeavesAndNotifications@PELeaveView');
        Route::post('pe_leave/{id}','SubmittedLeavesAndNotifications@PELeaveAction');
        Route::get('pe_notification/{id}/{method?}','SubmittedLeavesAndNotifications@PENotifView');
        Route::post('pe_notification/{id}','SubmittedLeavesAndNotifications@PENotifAction');


	});

    Route::group(array('prefix' => 'msr'), function() {
		Route::get('/','MSRController@index');
		
//		creation
        Route::get('create','MSRController@create');
        Route::post('create','MSRController@createAction');

//		Viewing own MSR
        Route::get('/my_msr/{id}/{me}','MSRController@myMSR');
        Route::post('edit_action/{id}','MSRController@editAction');

//		Viewing For approval MSR

		Route::get('for_approval/{id}/{method}','MSRController@forApproval');
		Route::post('for_approval_action/{id}','MSRController@forApprovalAction');

//		Process on list table

		Route::post('process','MSRController@myMSRProcess');


//		for Datatables

        Route::get('get_my_msr/{id?}','MSRController@getMyMSR');
        Route::get('get_superior_msr/{id?}','MSRController@getSuperiorMSR');
        Route::get('get_submitted_msr','MSRController@getSubmittedMSR');

//		Submitted MSR
		Route::get('submitted_msr','MSRController@submittedMSR');
		Route::post('get_filtered_submitted_msr','MSRController@getFilteredSubmittedMSR');

//		View Submitted MSR
        Route::get('submitted/{id}/{method}','MSRController@viewSubmittedMSR');
        Route::post('submitted_action/{id}','MSRController@submittedMSRAction');

//		Download
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','MSRController@downloadAttachments');

//      Export to CSV
        Route::post('export','MSRController@exportToCSV');

    });

    Route::group(array('prefix' => 'msr/smis'), function() {
		//		creation
		Route::post('create','SMISController@createAction');
		
		//		reprocess
		Route::post('reprocess/{id}','SMISController@reprocessAction');

        //		For Approval
        Route::post('for_approval/{id?}','SMISController@forApprovalAction');

        //		Submitted MSR
        Route::get('submitted_msr','SMISController@submittedMSR');
		Route::post('get_filtered_submitted_msr','SMISController@getFilteredSubmittedMSR');

		//      Export to CSV
		Route::post('export','SMISController@exportToCSV');

        //		View Submitted MSR
        Route::get('submitted/{id}/{method}','SMISController@viewSubmittedMSR');
        Route::post('submitted_action/{id}','SMISController@submittedMSRAction');

		// Vi
		
		//		for Datatables
		Route::get('get_my_msr/{id?}','SMISController@getMyMSR');
		Route::get('get_superior_msr/{id?}','SMISController@getSuperiorMSR');
		Route::get('get_submitted_msr','SMISController@getSubmittedMSR');
    });

    Route::group(array('prefix' => 'moc'), function() {
        //list
        Route::get('/','MOCController@index');

        //Creation
        Route::get('create','MOCController@create');

		//Viewing/Editing Own MOC
        Route::get('{method}/my-moc/{id}','MOCController@myMOC');

		//SendingMOC		
		Route::post('send_moc','ajaxMOCController@ajaxSendMOC');

        //SavingMOC
        Route::post('save_moc','ajaxMOCController@ajaxSaveMOC');

		//ResendingMOC
		Route::post('resend_moc','ajaxMOCController@ajaxResendMOC');

		//resavingMOC
		Route::post('resave_moc','ajaxMOCController@ajaxResaveMOC');

		//cancelMOC
		Route::post('cancel_moc','ajaxMOCController@ajaxCancelMOC');

		//Download
		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','MOCController@downloadAttachments');
		/*****For endorsement*****/
        //Viewing/Editing For Approval MOC
        Route::get('{method}/for-endorsement/{id}','MOCController@forEndorsement');

        //Viewing/Editing For Approval MOC
        Route::get('{method}/for-approval-moc/{id}','MOCController@forApprove');

		//Ajax feeding data
        /***for department**/
        Route::get('ajax_get_departments','ajaxMOCController@ajaxGetDepartment');
        /***for own MOC**/
        Route::post('ajax_feed_edit','ajaxMOCController@ajaxFeedEditMOC');
        /***for superior MOC**/
        Route::post('ajax_feed_superior','ajaxMOCController@ajaxFeedEditForApproveMOC');

        //Datatables
		Route::get('get_my_moc/{limit?}','MOCController@getMyMOC');
		Route::get('get_endorsement_moc/{limit?}','MOCController@getForEndorsementMoc');
		Route::get('get_assessment_moc','MOCController@getForAssessmentMoc');
		Route::get('get_approval_moc/{limit?}','MOCController@getForApprovalMoc');

		/******RETURN TO FILER******/
		Route::post('return_to_filer','ajaxMOCController@returnToFiler');

		/******SEND TO FILER FOR ACKNOWLEDGEMENT******/
		Route::post('send_to_filer','ajaxMOCController@sendToFiler');

		/******ACKNOWLEDGE MOC******/
		Route::post('acknowledge_moc','ajaxMOCController@acknowledgeMOC');


		/*******GENERAL ROUTING*******/
		//dept head to SSMD/CSMD
		Route::post('send_to_md','ajaxMOCController@ajaxDHtoMD');

		//CSMD head to CSMD Assistant head
		Route::post('head_to_adh','ajaxMOCController@ajaxDHtoADH');

		//CSMD Assistant head to CSMD head
		Route::post('adh_to_head','ajaxMOCController@ajaxADHtoDH');

		//Assign assessment to BSA
		Route::post('adh_to_bsa','ajaxMOCController@ajaxADHtoBSA');

        //Return assessment to BSA
        Route::post('return_to_bsa','ajaxMOCController@ajaxReturntoBSA');

        //Send BSA to ADH
        Route::post('send_bsa_to_adh','ajaxMOCController@ajaxSendBSAtoADH');

		//Send to Div Head
		Route::post('send_to_div_head','ajaxMOCController@ajaxSendtoDivHead');

        /************* CORPORATE ROUTING  **********/

		/************* SATELLITE ROUTING  **********/
		//ssmd to AOM
		Route::post('send_to_aom','ajaxMOCController@ajaxToAOM');

		//AOM to CSMD
		Route::post('send_aom_to_csmd','ajaxMOCController@ajaxAOMtoDH');

        //div head to ssmd
        Route::post('send_divhead_to_ssmd','ajaxMOCController@ajaxDivHeadtoSSMD');

        /************* SUBMITTED MOC  **********/
        Route::get('submitted-moc','MOCController@submittedMOC');

        /************ GET DEPARTMENTS ON COMPANY ***************/
        Route::post('get-departments','ajaxMOCController@ajaxGetDepartments');

        /************ GET DEPARTMENTS ON COMPANY ***************/
        Route::post('search-moc','ajaxMOCController@ajaxSearchMOC');

		/********* Export to CSV ************/
        Route::post('export','MOCController@exportToCSV');

        /********* Export to pdf ************/
        Route::get('print/{id}','MOCController@printToPDF');
    });

	Route::group(array('prefix' => 'mrf'), function() {
		//list
		Route::get('/list-view/{type}','MRFController@listView');

		//Create
		Route::get('create','MRFController@create');

        //View Own
        Route::get('{method}/my-mrf/{id}','MRFController@showMyMRF');

		//View For Endorsement
        Route::get('{method}/for-endorsement/{id}','MRFController@forEndorsement');
		//View For other status next to endorsement
		Route::get('{method}/for-processing/{id}','MRFController@forProcessing');
		//AJAX Action
		Route::post('send','AjaxMRFController@sendMRF');
		Route::post('save','AjaxMRFController@saveMRF');
		Route::post('process','AjaxMRFController@processMRF');
		Route::post('save-recruitment','AjaxMRFController@saveRecruitment');

		//Datatable
        Route::get('fetch-my-mrf/{limit?}','AjaxMRFController@fetchMyMRF');
        Route::get('fetch-for-endorsement-approval-mrf/{type}/{limit?}','AjaxMRFController@fetchForEndorsementApproval');

		//Return MRF
		Route::post('return','AjaxMRFController@returnProcess');

		//Return MRF
		Route::post('cancel-mrf','AjaxMRFController@cancelMRF');
		
		Route::post('delete-mrf','AjaxMRFController@deleteMyMRF');

		Route::get('download/{refNo}/{randomFilename}/{originalFilename}','MRFController@downloadAttachments');

	});

	/* Audit trail report */
	Route::group(array('prefix' => 'audit'), function()
	{
		Route::get('', 'AUDITController@index');
		Route::post('report', 'AUDITController@getRecordlist');    
        Route::get('export', 'AUDITController@exportAudit');    
	});
	//--	

Route::resource('/', 'HomeController');
/* Login */
Route::get('login', 'LoginController@index');
Route::post('sign-in', 'LoginController@sign_in');
Route::post('sign-in2', 'LoginController@sign_in2');
Route::get('sign-out', 'LoginController@sign_out');
Route::post('sign-out2', 'LoginController@sign_out2');
	

/*
|--------------------------------------------------------------------------
| Application Routes
|------------------------	--------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('employees_by_department/{dep_id}', function($dep_id){
	$employees = Employees::get_all($dep_id)->toJson();
	echo $employees;
});

Route::resource('/', 'HomeController');
/* Login */
Route::get('login', 'LoginController@index');
Route::post('sign-in', 'LoginController@sign_in');
Route::post('sign-in2', 'LoginController@sign_in2');
Route::get('sign-out', 'LoginController@sign_out');
Route::post('sign-out2', 'LoginController@sign_out2');