<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnLeaves extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('leaves', function(Blueprint $table)
		{
			$table->string('department');
			$table->string('section',25);
			$table->string('contact_no',20);
			$table->string('schedule_type');
			$table->string('rest_day');
			$table->string('duration');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leaves', function(Blueprint $table)
		{
		 	$table->dropColumn(array(
		 		'department', 
		 		'section', 
		 		'contact_no',
		 		'schedule_type',
		 		'rest_day',
		 		'duration'
	 		));
		});
	}

}
