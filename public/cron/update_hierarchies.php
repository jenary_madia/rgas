<?php

set_time_limit(0);
error_reporting(E_ALL);

define('SEP', DIRECTORY_SEPARATOR);
define('BASEPATH', ((PHP_OS !== 'WINNT') ? '/var/www/html/rgasbl/public' : 'C:\xampp\htdocs\rgasbl\public') . SEP);
define('APPPATH', BASEPATH . 'cron' . SEP);

require APPPATH . 'config/adodb.php';
require APPPATH . 'libraries/adodb/adodb.inc.php';
require APPPATH . 'helpers/formatting_helper.php';

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');

if (mb_regex_encoding() !== 'UTF-8')
{
  die('Invalid encoding!');
}

/* */
$db_rgas = &ADONewConnection('mysqli://root:rebisco@localhost/rgasbl');
// $db_rgas = &ADONewConnection('mysqli://root:sherwin@localhost/rgas2');
// $db_rgas->Execute('SET SESSION time_zone = "' . $config['adodb']['time_zone'] . '"');echo '1';die;
// $db_rgas->Execute('SET NAMES ' . $config['adodb']['charset']);

$db_rgas->SetFetchMode(ADODB_FETCH_ASSOC);
$db_rgas->debug = true;
echo '1';die;
$department_ids = $db_rgas->GetCol('select distinct departmentid from employees');
echo '<pre>'; print_r($department_ids);die;
$db_rgas->Execute('truncate `hierarchies`');

foreach ($department_ids as $department_id)
{
  $rs_employees = $db_rgas->Execute('
    select e.superiorid
         , e.id
         , e.departmentid
         , e.departmentid2
         , concat(e.firstname, " ", e.lastname) as name
         , concat(e2.firstname, " ", e2.lastname) as superior
         , e2.designation
      from employees as e
     inner join employees as e2
        on e2.id = e.superiorid
     where 1 = 1
       and e.departmentid = ' . intval($department_id) . '
       and e.status = "1"
       and e.new_employeeid is not null
       ');

  $employees = $rs_employees->GetAll();

  if (!empty($employees))
  {
    unset($row, $rows, $sql);

    $row = $rows = $sql = array();

    /* extended insert into `hierarchies` by department */
    $sql[] = 'insert into `hierarchies` (`employee_id`, `superior_id`, `department_id`, `department_hrms_cd`, `name`, `superior`, `designation`) values';

    foreach ($employees as $employee)
    {
      $row = array($employee['id'], $employee['superiorid'], $employee['departmentid'], $employee['departmentid2'], $employee['name'], $employee['superior'], $employee['designation']);
      $row = array_map(array($db_rgas, 'qstr'), $row);

      $rows[] = '(' . implode(', ', $row) . ')';
    }

    $sql[] = implode(', ', $rows);

    $db_rgas->Execute(implode(' ', $sql));
  }

  unset($employees, $rs_employees);
}

function mb_convert($string, $mode = MB_CASE_TITLE)
{
  $string = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
  $string = mb_convert_case($string, $mode, 'UTF-8');

  return trim($string);
}
