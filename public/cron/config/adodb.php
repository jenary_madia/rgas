<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

#$config['adodb']['dsn']           = 'mysqli://sherwin:tY0rFG9f@localhost/rbc_intranet';
$config['adodb']['dsn']           = 'mysqli://root:rebisco@localhost/rgasbl';
$config['adodb']['db_var']        = true;
$config['adodb']['debug']         = false;
$config['adodb']['show_errors']   = true;
$config['adodb']['active_record'] = false;
$config['adodb']['time_zone']     = '+8:00'; // Asia/Manila
$config['adodb']['charset']       = 'UTF8';

/* End of file adodb.php */
/* Location: ./system/application/config/adodb.php */