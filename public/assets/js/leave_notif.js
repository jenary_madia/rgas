$(document).ready(function(){
    var base_url = $("base").attr("href");

    if ( $("#myPELeaves").length != 0 ) {
        var requests_my_table = $('#myPELeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/my_pe_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%","bSortable" : false},
                { "sWidth": "15%","bSortable" : false},
                { "sWidth": "15%","bSortable" : false},
                { 'bSortable': false },
                { 'bSortable': false },
                { "sWidth": "20%",'bSortable': false},
                { "sWidth": "22%",'bSortable': false},

            ],
            "bPaginate": false,
            "bFilter": false

        });

    }

    if ( $("#myLeaves").length != 0 ) {
        var requests_my_table = $('#myLeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_my_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [5, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { 'bSortable': false },
                { 'bSortable': false },
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false


        });

    }

    if ( $("#clinicApprovalLeaves").length != 0 ) {
        var requests_my_table = $('#clinicApprovalLeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_clinic_approval_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search :"
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "sWidth": "180px"},

            ],
            "bPaginate": false,
            "bFilter": false

        });

    }

    if ( $("#forApproveLeaves").length != 0 ) {
        var requests_my_table = $('#forApproveLeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_for_approval_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search:"
            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                null,
                null,
                { "sWidth": "5%"},
                { "sWidth": "15%"},
                { "sWidth": "18%"},

            ],
            "bPaginate": false,
            "bFilter": false
        });

    }

    if ( $("#superiorPELeaves").length != 0 ) {
        var requests_my_table = $('#superiorPELeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_superior_pe_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                null,
                null,
                { "sWidth": "20%"},
                { "sWidth": "18%"},

            ],
            "bPaginate": false,
            "bFilter": false

        });

    }

});

    

