$(document).ready(function () {
    $('.date_picker').datepicker({
        dateFormat : 'yy-mm-dd'
    });

    $('.time_picker').timepicker({
        defaultTime : false,
        showMeridian : false
    });

    $('body').on("click",".time_picker",function () {
        $(this).timepicker('showWidget');
    });


    var restDay = [];
    $('body').on('click','.toggleDatePicker',function () {
        $(this).closest( "div" ).find(".date_picker").datepicker("show");
    });
    $('body').on('change', '#schedule_typeOffset', function() {
        if ($(this).val() == "compressed"){
            $('.date_picker').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != 6 && day != 0)];
            });
        }else if ($(this).val() == "regular") {
            $('.date_picker').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != 6 && day != 20)];
            });
        }else{
            $('.date_picker').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != 22 && day != 20)];
            });
        }
    });

    $('body').on('change', '.durationType', function() {
        if ($(this).val() == "multiple") {
            $('.divTo').fadeIn()
        }else{
            $('.divTo').fadeOut()
        }
    });

    var restDays = $('#UTDay').val();
    var schedType = $('#schedule_typeUndertime').val();
    if(schedType == "regular") {
        changeDisableDays("Sunday","---");
    }else if(schedType == "compressed"){
        changeDisableDays("Saturday","Sunday");
    }else if(schedType == "special"){
        changeDisableDays(restDays[0],restDays[1]);
    }
    $("#schedule_typeUndertime").on("change",function () {

        if ($(this).val() === 'special') {
            $('#UTDay').val(['Saturday','Sunday']);
            $("#UTDay").multiselect('enable');
            changeDisableDays(restDays[0],restDays[1]);
        }else if($(this).val() === 'regular'){
            $('#UTDay').val(['Sunday'])
            $("#UTDay").multiselect('disable');
            changeDisableDays("Sunday","---");
        }else if($(this).val() === 'compressed'){
            $('#UTDay').val(['Saturday','Sunday']);
            $("#UTDay").multiselect('disable');
            changeDisableDays("Saturday","Sunday");
        }
        $("#UTDay").multiselect("refresh");
    });

    $('#UTDay').multiselect({
        onInitialized: function () {
            // changeDisableDays(restDays[0],restDays[1]);
            var selectedOptions = $('#UTDay option:selected');

            if (selectedOptions.length >= 2) {
                // Disable all other checkboxes.
                var nonSelectedOptions = $('#UTDay option').filter(function() {
                    return !$(this).is(':selected');
                });

                nonSelectedOptions.each(function() {
                    var input = $('input[value="' + $(this).val() + '"]');
                    input.prop('disabled', true);
                    input.parent('li').addClass('disabled');
                });
            }
        },
        onChange: function(option, checked) {
            restDays = $('#UTDay').val();
            changeDisableDays(restDays[0],restDays[1]);
            var selectedOptions = $('#UTDay option:selected');

            if (selectedOptions.length >= 2) {
                // Disable all other checkboxes.
                var nonSelectedOptions = $('#UTDay option').filter(function() {
                    return !$(this).is(':selected');
                });

                nonSelectedOptions.each(function() {
                    var input = $('input[value="' + $(this).val() + '"]');
                    input.prop('disabled', true);
                    input.parent('li').addClass('disabled');
                });
            }
            else {
                // Enable all checkboxes.
                $('#UTDay option').each(function() {
                    var input = $('input[value="' + $(this).val() + '"]');
                    input.prop('disabled', false);
                    input.parent('li').addClass('disabled');
                });
            }

            $("#UTdate").val("");
        },
        onDropdownShown: function () {
            var selectedOptions = $('#UTDay option:selected');

            if (selectedOptions.length >= 2) {
                // Disable all other checkboxes.
                var nonSelectedOptions = $('#UTDay option').filter(function() {
                    return !$(this).is(':selected');
                });

                nonSelectedOptions.each(function() {
                    var input = $('input[value="' + $(this).val() + '"]');
                    input.prop('disabled', true);
                    input.parent('li').addClass('disabled');
                });
            }
        }
    });

    function changeDisableDays(day1,day2)
    {
        switch(day1) {
            case "Sunday":
                day1 = 0;
                break;
            case "Monday":
                day1 = 1;
                break;
            case "Tuesday":
                day1 = 2;
                break;
            case "Wednesday":
                day1 = 3;
                break;
            case "Thursday":
                day1 = 4;
                break;
            case "Friday":
                day1 = 5;
                break;
            case "Saturday":
                day1 = 6;
                break;
        }

        switch(day2) {
            case "Sunday":
                day2 = 0;
                break;
            case "Monday":
                day2 = 1;
                break;
            case "Tuesday":
                day2 = 2;
                break;
            case "Wednesday":
                day2 = 3;
                break;
            case "Thursday":
                day2 = 4;
                break;
            case "Friday":
                day2 = 5;
                break;
            case "Saturday":
                day2 = 6;
                break;
        }

        $('#UTdate').datepicker('option', 'beforeShowDay', function(date) {
            var day = date.getDay();
            return [(day != day1 && day != day2)];
        });

    }

});

