/**
 * Created by Jenary on 22/07/2016.
 */
$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#myNotifications").length != 0 ) {
        var requests_my_table = $('#myNotifications').dataTable( {

            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/my_notifications",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "20%","bSortable": true},
                { "sWidth": "15%","bSortable": true},
                { "sWidth": "15%","bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                { "sWidth": "20%","bSortable": false},
                { "sWidth": "30%","bSortable": false},

            ]

        });

    }

});

$("#formMyNotifications").on("submit", function(e){
    var r = confirm("Delete this notification?");
    if (r == true) {
        $("#formMyNotifications").submit;
    }else{
        e.preventDefault();
    }
});

