var base_url = $("base").attr("href");
new Vue({
    el : "#peISView",
    data : {
        toEdit : {},
        employeesToFile : [],
        employeesToFileOld : "",
        editable : false
    },
    methods : {
        saveEdit : function (id,employee_number,batch_id) {
            this.employeesToFile[id]["noted"] = this.toEdit.noted;
            Vue.http.post(base_url+'/ns/note_batchdetails',{"note": this.employeesToFile[id]["noted"],"employeeNumber" : employee_number,"batchID" : batch_id }).then(function (response) {
                    console.log(response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
            $('#editNotifDetails').modal('hide');
        },

        chooseEmployee : function (index,reference) {
            for (var i = 0; i < this.employeesToFile.length; i++) {
                if(index != i) {
                    this.employeesToFile[i]['active'] = false;
                }else{
                    this.employeesToFile[i]['active'] = true;
                }
            }

            this.$set("toEdit", JSON.parse(JSON.stringify(reference)));
            console.log(reference);
            this.$set("toEdit.index",index);
            this.$set("editable",true);

        },

        markAsNoted : function () {
            var empId = this.toEdit.index;
            if(this.toEdit.noted != this.employeesToFile[empId]['noted']){
                if (this.employeesToFile[empId]['noted'] == true) {
                    this.employeesToFile[empId]['noted'] = false;
                }else{
                    this.employeesToFile[empId]['noted'] = true;
                }
            }
        }
    },
    watch : {
        "employeesToFile" : function () {
            if (this.employeesToFile.length == 0) {
                this.$set("editable",false);
                this.$set("deletable",false);
            }
        }

    },

    ready : function () {
        var that = this;
        if(this.employeesToFileOld)  {
            this.$set("employeesToFile",JSON.parse(this.employeesToFileOld));
            for (var i = 0; i < this.employeesToFile.length; i++) {
                this.employeesToFile[i]['active'] = false;
            }
        }else{
            Vue.http.get(window.location.href, {
                headers: {
                    'Cache-Control': 'no-cache'
                }
            }).then(function (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        response.data[i]["active"] = false;
                        response.data[i]["noted"] = false;
                        that.employeesToFile.push(response.data[i]);
                    }
                },
                function () {
                    console.log("Something went wrong");
                });
        }
    },
})