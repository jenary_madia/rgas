/**
 * Created by Jenary on 22/07/2016.
 */
$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#myPENotifications").length != 0 ) {
        var requests_my_table = $('#myPENotifications').dataTable( {

            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/my_pe_notifications",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%","bSortable": true},
                { "sWidth": "15%","bSortable": true},
                { "sWidth": "15%","bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                { "sWidth": "20%","bSortable": false},
                { "sWidth": "30%","bSortable": false},

            ]

        });

    }

});

$("#formPEMyNotifications").on("submit", function(e){
    var r = confirm("Delete this notification?");
    if (r == true) {
        $("#formPEMyNotifications").submit;
    }else{
        e.preventDefault();
    }
});
