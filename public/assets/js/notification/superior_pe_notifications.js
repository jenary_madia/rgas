/**
 * Created by Jenary on 22/07/2016.
 */
$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#superiorPENotification").length != 0 ) {
        var requests_my_table = $('#superiorPENotification').dataTable( {

            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/superior_pe_notifications",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                null,
                null,
                { "sWidth": "20%"},
                { "sWidth": "20%"},

            ]

        });

    }

});
