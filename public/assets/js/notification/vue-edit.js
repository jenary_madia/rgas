"use strict";
var base_url = $("base").attr("href");
new Vue({
    el: '#app',
    data: {
        notificationType: '',
        offsetReference : {
            OTDate : "",
            type : "",
            timeInStartTime : "",
            timeOutEndTime : "",
            OTDuration : "",
            reason : "",
            OBDestination : ""
        },
        listOffsetReferences : [],
        listOffsetReferenceInputOld : "",
        forEditReference : {},
        offsetHoursFrom : 0,
        offsetHoursTo : 0,
        offsetDateFrom : "",
        offsetDateTo : "",
        offsetDuration : "",
        offsetScheduleType : "",
        offsetDateReady : false,
        offsetPeriodTo : "",
        offsetPeriodFrom : "",

        undertime : {
            scheduleType : "",
            day : "",
            scheduleTimeout : "",
            date : "",
            timeout : ""
        },
        timeKeeping : {
            toCorrect : "",
            timeIn : "",
            timeOut: ""
        },
        officialBusiness : {
            dateFrom : "",
            dateTo : "",
            duration : ""
        },
        cutTime : {
            dateFrom : "",
            dateTo : ""
        },
        editable : false,
        deletable : false

    },
    computed : {
        getTotalListOffsetHours : function () {
            var listOffsetHours = 0;
            for (var i = 0; i < this.listOffsetReferences.length; i++) {
                listOffsetHours += parseInt(this.listOffsetReferences[i]['OTDuration']);
            }
            return listOffsetHours;
        },
        getASD: function () {
            return JSON.stringify(this.listOffsetReferences);
        },
        // getOTDuration: function () {
        //     var startTime = new Date(this.offsetReference.OTDate+' '+this.offsetReference.timeInStartTime);
        //     var endTime = new Date(this.offsetReference.OTDate+' '+this.offsetReference.timeOutEndTime);
        //     if  (startTime < endTime) {
        //         var diff = (endTime - startTime) / 60000; //dividing by seconds and milliseconds

        //         var minutes = diff % 60;
        //         var hours = (diff - minutes) / 60;
        //         if (hours != NaN || minutes != NaN) {
        //             this.$set('offsetReference.OTDuration', hours+'.'+minutes);
        //             return hours+'.'+minutes;
        //         }
        //     }
        // },

        // getEditOTDuration: function () {
        //     var startTime = new Date(this.forEditReference.OTDate+' '+this.forEditReference.timeInStartTime);
        //     var endTime = new Date(this.forEditReference.OTDate+' '+this.forEditReference.timeOutEndTime);
        //     if  (startTime < endTime) {
        //         var diff = (endTime - startTime) / 60000; //dividing by seconds and milliseconds

        //         var minutes = diff % 60;
        //         var hours = (diff - minutes) / 60;
        //         if (hours != NaN || minutes != NaN) {
        //             this.$set('forEditReference.OTDuration', hours+'.'+minutes);
        //             return hours+'.'+minutes;
        //         }
        //     }
        // },

        getOffsetTotalHours: function() {
            var hoursFrom = (this.offsetHoursFrom == "" ? 0 : parseInt(this.offsetHoursFrom));
            var hoursTo = (this.offsetHoursTo == "" ? 0 : parseInt(this.offsetHoursTo));
            var total = hoursFrom + hoursTo;
            return (isNaN(total) ? 0 : total) ;
        },

        getTotalDays: function() {
            var duration = this.offsetDuration;
            var startDate = new Date(this.offsetDateFrom);
            var endDate = new Date(this.offsetDateTo);
            var restDays = [22,22];
            if(this.offsetScheduleType == "compressed") {
                restDays = [6,0];
            }else if(this.offsetScheduleType == "regular") {
                restDays = [0,22];
            }
            if (duration == "multiple")
                if ((startDate >= endDate) || (startDate == endDate)) {
                    return 0;
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        diffDays = Math.round(Math.abs((a - b)/(c)));
                    var counter = 1;
                    for (var i = 0; i < diffDays; i++) {
                        startDate.setDate(startDate.getDate() + 1); //number  of days to add, e.x. 15 days
                        var dateFormated = startDate.toISOString().substr(0,10);
                        var finalDate = new Date(dateFormated);
                        var day = finalDate.getDay();
                        var checkIfrestDay = restDays.indexOf(day);
                        if (checkIfrestDay == -1)
                        {
                            counter += 1;
                        }
                        startDate = new Date(dateFormated);
                    }
                    return counter;
                }
            else if(duration == "one") {
                return 1;
            }else{
                return .5;
            }
        },

        getOBTotalDays : function () {
            var duration = this.officialBusiness.duration;
            var startDate = new Date(this.officialBusiness.dateFrom);
            var endDate = new Date(this.officialBusiness.dateTo);
            if (duration == "multiple"){
                if(startDate == 'Invalid Date'|| endDate == 'Invalid Date') {
                    return 0;
                }
                var total = 0;
                if ((startDate >= endDate)) {
                    // alert("Check “to inout” field and “to out”field");
                    // this.$set("officialBusiness.dateFrom","");
                    // this.$set("officialBusiness.dateTo","");
                    return total;
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        total = Math.round(Math.abs((a - b)/(c)));
                }
                return total + 1;
            }
            else if(duration == "one") {
                return 1;
            }else if(duration == "half"){
                return .5;
            }else{
                return 0;
            }
        }
    },
    methods : {
        deleteOffsetReference : function () {
            var r = confirm("ARE YOU SURE YOU WANT TO DELETE?");
            if (r == true) {
                var id = this.forEditReference.id;
                this.listOffsetReferences.splice(id,1);
                alert("OFFSET REFERENCE DETAILS SUCCESSFULLY DELETED");
            }
        },
        saveEdit : function () {
            var dateFrom = this.offsetDateFrom.split("-");
            var dateTo = this.offsetDateTo.split("-");
            var duration = this.offsetDuration;
            var validity = [];
            var offsetDate = new Date(this.offsetReference.OTDate);
            // var offsetDate = new Date("2016-10-09");

            if(offsetDate.getMonth() >= 0 && offsetDate.getMonth() <= 2) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1 ) , to : offsetDate.getFullYear()+'-7' };
            }
            if(offsetDate.getMonth() >= 3 && offsetDate.getMonth() <= 5) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1 ), to : offsetDate.getFullYear()+'-10' };
            }
            if(offsetDate.getMonth() >= 6 && offsetDate.getMonth() <= 8) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1), to : parseInt(offsetDate.getFullYear()) + 1 +'-1' };
            }
            if(offsetDate.getMonth() >= 9 && offsetDate.getMonth() <= 11) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1), to : parseInt(offsetDate.getFullYear()) + 1 +'-4' };
            }
            var fromYearMonth = new Date(dateFrom[0]+'-'+dateFrom[1]);
            var toYearMonth = new Date(dateTo[0]+'-'+dateTo[1]);
            var offsetFrom = new Date(validity['from']);
            var offsetTo = new Date(validity['to']);

            if(duration == "multiple") {
                if ((fromYearMonth >= offsetFrom && fromYearMonth <= offsetTo) && (toYearMonth >= offsetFrom && toYearMonth <= offsetTo)) {
                    var refId = this.forEditReference.id;
                    var that = this;
                    Vue.http.post(base_url+'/ns/check-date-validity',{
                        OTDate : this.forEditReference.OTDate,
                        OTStart : this.forEditReference.timeInStartTime,
                        OTEnd : this.forEditReference.timeOutEndTime
                    }, {
                        headers: {
                            'Cache-Control': 'no-cache'
                        }
                    }).then(function (response) {
                        if(response.data == 0) {
                            alert("This offset reference is been filed as Overtime");
                            that.$set("forEditReference.OTDate","");
                            that.$set("forEditReference.timeInStartTime","");
                            that.$set("forEditReference.timeOutEndTime","");
                        }else{
                            that.listOffsetReferences[refId]["active"] =  false;
                            that.listOffsetReferences[refId]["OTDate"] =  that.forEditReference.OTDate;
                            that.listOffsetReferences[refId]["type"] =  that.forEditReference.type;
                            that.listOffsetReferences[refId]["timeInStartTime"] =  that.forEditReference.timeInStartTime;
                            that.listOffsetReferences[refId]["timeOutEndTime"] =  that.forEditReference.timeOutEndTime;
                            that.listOffsetReferences[refId]["OTDuration"] =  that.forEditReference.OTDuration;
                            that.listOffsetReferences[refId]["reason"] =  that.forEditReference.reason;
                            that.listOffsetReferences[refId]["OBDestination"] =  that.forEditReference.OBDestination;
                            $("#offsetEditModal").modal("hide");
                        }
                    });
                }else{

                    alert("Check Time Offset Reference");

                }
            }else{
                if(fromYearMonth >= offsetFrom && fromYearMonth <= offsetTo) {
                    var that = this;
                    Vue.http.post(base_url+'/ns/check-date-validity',{
                        OTDate : this.forEditReference.OTDate,
                        OTStart : this.forEditReference.timeInStartTime,
                        OTEnd : this.forEditReference.timeOutEndTime
                    }, {
                        headers: {
                            'Cache-Control': 'no-cache'
                        }
                    }).then(function (response) {
                        if(response.data == 0) {
                            alert("This offset reference is been filed as Overtime");
                            that.$set("forEditReference.OTDate","");
                            that.$set("forEditReference.timeInStartTime","");
                            that.$set("forEditReference.timeOutEndTime","");
                        }else{
                            that.listOffsetReferences[refId]["active"] =  false;
                            that.listOffsetReferences[refId]["OTDate"] =  that.forEditReference.OTDate;
                            that.listOffsetReferences[refId]["type"] =  that.forEditReference.type;
                            that.listOffsetReferences[refId]["timeInStartTime"] =  that.forEditReference.timeInStartTime;
                            that.listOffsetReferences[refId]["timeOutEndTime"] =  that.forEditReference.timeOutEndTime;
                            that.listOffsetReferences[refId]["OTDuration"] =  that.forEditReference.OTDuration;
                            that.listOffsetReferences[refId]["reason"] =  that.forEditReference.reason;
                            that.listOffsetReferences[refId]["OBDestination"] =  that.forEditReference.OBDestination;
                            $("#offsetEditModal").modal("hide");
                        }
                    });
                }else{

                    alert("Check Time Offset Reference");

                }
            }
        },
        chooseOffsetReference : function (reference,id) {
            for (var i = 0; i < this.listOffsetReferences.length; i++) {
                this.listOffsetReferences[i]['active'] = false;
                if(this.listOffsetReferences[i] === reference) {
                    this.listOffsetReferences[i]['active'] = true;
                }
            }
            this.$set("forEditReference", JSON.parse(JSON.stringify(reference)));
            this.$set("forEditReference.id",id)
            this.$set("editable",true);
            this.$set("deletable",true);
        },
        gatherData : function () {
            var dateFrom = this.offsetDateFrom.split("-");
            var dateTo = this.offsetDateTo.split("-");
            var duration = this.offsetDuration;
            var validity = [];
            var offsetDate = new Date(this.offsetReference.OTDate);
            // var offsetDate = new Date("2016-10-09");

            if(offsetDate.getMonth() >= 0 && offsetDate.getMonth() <= 2) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1 ) , to : offsetDate.getFullYear()+'-7' };
            }
            if(offsetDate.getMonth() >= 3 && offsetDate.getMonth() <= 5) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1 ), to : offsetDate.getFullYear()+'-10' };
            }
            if(offsetDate.getMonth() >= 6 && offsetDate.getMonth() <= 8) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1), to : parseInt(offsetDate.getFullYear()) + 1 +'-1' };
            }
            if(offsetDate.getMonth() >= 9 && offsetDate.getMonth() <= 11) {
                validity = { from : offsetDate.getFullYear()+'-'+parseInt(offsetDate.getMonth() + 1), to : parseInt(offsetDate.getFullYear()) + 1 +'-4' };
            }
            var fromYearMonth = new Date(dateFrom[0]+'-'+dateFrom[1]);
            var toYearMonth = new Date(dateTo[0]+'-'+dateTo[1]);
            var offsetFrom = new Date(validity['from']);
            var offsetTo = new Date(validity['to']);

            if(duration == "multiple") {
                if ((fromYearMonth >= offsetFrom && fromYearMonth <= offsetTo) && (toYearMonth >= offsetFrom && toYearMonth <= offsetTo)) {
                    var that = this;
                    Vue.http.post(base_url+'/ns/check-date-validity',{
                        OTDate : this.offsetReference.OTDate,
                        OTStart : this.offsetReference.timeInStartTime,
                        OTEnd : this.offsetReference.timeOutEndTime
                    }, {
                        headers: {
                            'Cache-Control': 'no-cache'
                        }
                    }).then(function (response) {
                        if(response.data == 0) {
                            alert("This offset reference is been filed as Overtime");
                            that.$set("offsetReference.OTDate","");
                            that.$set("offsetReference.timeInStartTime","");
                            that.$set("offsetReference.timeOutEndTime","");
                        }else{
                            that.listOffsetReferences.push({
                                "active" : false,
                                "OTDate": that.offsetReference.OTDate,
                                "type": that.offsetReference.type,
                                "timeInStartTime": that.offsetReference.timeInStartTime,
                                "timeOutEndTime": that.offsetReference.timeOutEndTime,
                                "OTDuration": that.offsetReference.OTDuration,
                                "reason": that.offsetReference.reason,
                                "OBDestination": that.offsetReference.OBDestination
                            });
                            $("#offsetAddModal").modal("hide");
                        }
                    });
                }else{

                    alert("Check Time Offset Reference");

                }
            }else{
                if(fromYearMonth >= offsetFrom && fromYearMonth <= offsetTo) {
                    var that = this;
                    Vue.http.post(base_url+'/ns/check-date-validity',{
                        OTDate : this.offsetReference.OTDate,
                        OTStart : this.offsetReference.timeInStartTime,
                        OTEnd : this.offsetReference.timeOutEndTime
                    }, {
                        headers: {
                            'Cache-Control': 'no-cache'
                        }
                    }).then(function (response) {
                        if(response.data == 0) {
                            alert("This offset reference is been filed as Overtime");
                            that.$set("offsetReference.OTDate","");
                            that.$set("offsetReference.timeInStartTime","");
                            that.$set("offsetReference.timeOutEndTime","");
                        }else{
                            that.listOffsetReferences.push({
                                "active" : false,
                                "OTDate": that.offsetReference.OTDate,
                                "type": that.offsetReference.type,
                                "timeInStartTime": that.offsetReference.timeInStartTime,
                                "timeOutEndTime": that.offsetReference.timeOutEndTime,
                                "OTDuration": that.offsetReference.OTDuration,
                                "reason": that.offsetReference.reason,
                                "OBDestination": that.offsetReference.OBDestination
                            });
                            $("#offsetAddModal").modal("hide");
                        }
                    });
                }else{

                    alert("Check Time Offset Reference");

                }
            }
        },
        clearData : function () {
            this.$set("offsetReference.OTDate","");
            this.$set("offsetReference.type","");
            this.$set("offsetReference.timeInStartTime","");
            this.$set("offsetReference.timeOutEndTime","");
            this.$set("offsetReference.OTDuration","");
            this.$set("offsetReference.reason","");
            this.$set("offsetReference.OBDestination","");
        },

    },
    watch : {
        'officialBusiness.dateFrom' : function() {
            if(this.notificationType == "official") {
                if(this.officialBusiness.dateTo != "") {
                    if(this.officialBusiness.dateFrom >= this.officialBusiness.dateTo) {
                        alert("Please check your dates");
                        this.$set("officialBusiness.dateFrom","");
                    }
                }
            }
        },
        'officialBusiness.dateTo' : function() {
            if(this.notificationType == "official") {
                if(this.officialBusiness.dateTo != "") {
                    if(this.officialBusiness.dateTo <= this.officialBusiness.dateFrom) {
                        alert("Please check your dates");
                        this.$set("officialBusiness.dateTo","");
                    }
                }
            }
        },
        'cutTime.dateFrom' : function() {
            if(this.notificationType == "cut_time") {
                if(this.cutTime.dateTo != "") {
                    if(this.cutTime.dateFrom >= this.cutTime.dateTo) {
                        alert("Please check your dates");
                        this.$set("cutTime.dateFrom","");
                    }
                }
            }
        },
        'cutTime.dateTo' : function() {
            if(this.notificationType == "cut_time") {
                if(this.cutTime.dateTo != "") {
                    if(this.cutTime.dateTo <= this.cutTime.dateFrom) {
                        alert("Please check your dates");
                        this.$set("cutTime.dateTo","");
                    }
                }
            }
        },
        'offsetDateFrom' : function () {
            if(this.notificationType == "offset") {
                if(this.offsetDuration == "multiple") {
                    if(this.offsetDateTo != "") {
                        if(this.offsetDateTo <= this.offsetDateFrom) {
                            alert("Please check your dates");
                            this.$set("offsetDateFrom","");
                        }
                    }
                }
            }
        },
        'offsetDateTo' : function () {
            if(this.notificationType == "offset") {
                if(this.offsetDuration == "multiple") {
                    if(this.offsetDateTo != "") {
                        if(this.offsetDateTo <= this.offsetDateFrom) {
                            alert("Please check your dates");
                            this.$set("offsetDateTo","");
                        }
                    }
                }
            }
        },
        'notificationType' : function () {
            if (this.notificationType != "") {
                $('.date_picker').datepicker({
                    dateFormat : 'yy-mm-dd'
                });

                $('.time_picker').timepicker({
                    defaultTime : false,
                    showMeridian : false
                });
            }
        },
        'offsetScheduleType': function () {
            if(this.offsetScheduleType != "") {
                this.$set("offsetDateReady",true);
                this.$set("offsetHoursFrom","");
                this.$set("offsetHoursTo","");
                this.$set("offsetDateFrom","");
                this.$set("offsetDateTo","");
                this.$set("offsetPeriodTo","");
                this.$set("offsetPeriodFrom","");
                this.$set("offsetDuration","");
            }else{
                this.$set("offsetDateReady",false);
            }
        },
        'listOffsetReferenceInputOld' : function () {
            if(this.listOffsetReferenceInputOld.length != 0) {
                this.$set("listOffsetReferences",JSON.parse(this.listOffsetReferenceInputOld));
            }
        },
        'timeKeeping.toCorrect' : function () {
            if(this.timeKeeping.toCorrect == 'no_time_in' ){
                this.timeKeeping.timeOut = "";
            }else if(this.timeKeeping.toCorrect == 'no_time_out' ){
                this.timeKeeping.timeIn = "";
            }
        },
        'forEditReference.type' : function () {
            if (this.forEditReference.type == 'OT') {
                this.$set("forEditReference.OBDestination","")
            }
        },
        'undertime.scheduleType' : function () {
            this.$set("undertime.scheduleTimeout","");
            this.$set("undertime.date","");
            this.$set("undertime.timeout","");
        }
    },
    ready : function () {
        var that = this;
        Vue.http.post(dataURL,{"notif_id" : notif_id}, {
            headers: {
                'Cache-Control': 'no-cache'
            }
        }).then(function (response) {
                if(that.listOffsetReferenceInputOld.length == 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        that.listOffsetReferences.push(response.data[i]);
                    }
                }
            },
            function () {
                console.log("Something went wrong");
            });
    },
})/**
 * Created by octal on 27/07/2016.
 */
