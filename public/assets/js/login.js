$(document).ready(function(){

	$("#login_form").validationEngine();
	
    
	var base_url = $("base").attr("href");
	
	$(".btn_login").on("click", this, function(){
	
	
		if($("#login_form").validationEngine('validate'))
		{
			$(".preload_container").fadeIn();
			$(".alert-danger").hide();
		
			$.post(base_url + "/sign-in", $("#login_form").serialize(), function(data){
			
                
				if(data.logged_in) //login successful
				{
					window.location = base_url;
				}
				else
				{
					$(".preload_container").hide();
					$(".alert-danger").html(data.message);
					$(".alert-danger").fadeIn();
				} 
			
			}, "json");
		}
		
	
	});
	
	
	
	
	$("#login_form").keypress(function(event){
	 
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			
			if($("#login_form").validationEngine('validate'))
			{
				$(".preload_container").fadeIn();
				$(".alert-danger").hide();
			
				$.post(base_url + "/sign-in", $("#login_form").serialize(), function(data){
				
					if(data.logged_in) //login successful
					{
						window.location = base_url;
					}
					else
					{
						$(".alert-danger").html(data.message);
						$(".alert-danger").fadeIn();
						$(".preload_container").hide();
					} 
				
				}, "json");
			}
			
		}
	 
	});
    
    
});