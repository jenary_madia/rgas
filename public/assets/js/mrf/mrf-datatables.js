var base_url = $("base").attr("href");
$(document).ready(function(){
    if ( $("#myMRF").length != 0 ) {
        var requests_my_table = $('#myMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-my-mrf",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#forEndorsementMRF").length != 0 ) {
        var forEndorsementMRF = $('#forEndorsementMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/1",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#forApprovalMRF").length != 0 ) {
        var forApprovalMRF = $('#forApprovalMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/2",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#notedMRF").length != 0 ) {
        var notedMRF = $('#notedMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/3",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#forProcessingMRF").length != 0 ) {
        var forProcessingMRF = $('#forProcessingMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/4",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#forAssessmentMRF").length != 0 ) {
        var forAssessmentMRF = $('#forAssessmentMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/5",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#forFurtherMRF").length != 0 ) {
        var forFurtherMRF = $('#forFurtherMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/6",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#approvedMRF").length != 0 ) {
        var approvedMRF = $('#approvedMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/8",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }
    
    // if ($("#forApprovalMOC").length != 0 ) {
    //     var forApprovalMOC = $('#forApprovalMOC').dataTable( {
    //         "bDestroy": true,
    //         "sPaginationType": "full_numbers",
    //         "sAjaxSource": base_url + "/moc/get_approval_moc",
    //         "oLanguage": {
    //             "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
    //             "sSearch" : "Search : "
    //         },
    //         // fnFilter($(this).val())
    //
    //         "fnDrawCallback": function( oSettings ) {
    //             if(! $('.generate').length) {
    //                 $('div#submittedMOC_filter').append(' <button class="btn btndefault btn-default generate pull-left" id="btnGenerate" style="margin-right: 10px;" id="approveSelected" name="action" value="">GENERATE REPORT</button>');
    //             }
    //
    //             if(! $('.export').length) {
    //                 $('div#submittedMOC_filter').append(' <button class="btn btndefault btn-default export pull-left" style="margin-right: 10px;" id="approveSelected" name="action" value="">EXPORT TABLE TO CSV</button>');
    //             }
    //
    //             $('div#submittedMOC_length label').hide();
    //         },
    //
    //         "aLengthMenu": [
    //             [10, 20, 50, 100, -1],
    //             [10, 20, 50, 100, "All"]
    //         ],
    //         "aoColumns": [
    //             { "sWidth": "17%",'bSortable': true},
    //             { "sWidth": "15%",'bSortable': true},
    //             { "sWidth": "15%",'bSortable': false},
    //             { "sWidth": "15%",'bSortable': false},
    //             { "sWidth": "30%",'bSortable': false}
    //         ],
    //
    //     });
    //
    // }

    $('body').on('submit','#formMyMRF',function (e) {
        var r = confirm("Delete this MRF?");
        if (r != true) {
            e.preventDefault();
        }
    });

});


// var hello = 'asdasd';
// if ( $("#forAssessmentMOC").length != 0 ) {
//     var forAssessmentMOC = $('#forAssessmentMOC').dataTable( {
//         "bDestroy": true,
//         "sPaginationType": "full_numbers",
//         "sAjaxSource": base_url + "/moc/get_assessment_moc",
//         "oLanguage": {
//             "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
//             "sSearch" : "Search : "
//         },
//         // fnFilter($(this).val())
//         "fnDrawCallback": function( oSettings ) {
//             if(! $('.generate').length) {
//                 $('div#forAssessmentMOC_filter').append(' <button class="btn btndefault btn-default generate pull-left" id="btnGenerate" style="margin-right: 10px;" id="approveSelected" name="action" value="">GENERATE REPORT</button>');
//             }
//
//             if(! $('.export').length) {
//                 $('div#forAssessmentMOC_filter').append(' <button class="btn btndefault btn-default export pull-left" style="margin-right: 10px;" id="approveSelected" name="action" value="export">EXPORT TABLE TO</button>');
//             }
//         },
//         "aLengthMenu": [
//             [10, 20, 50, 100, -1],
//             [10, 20, 50, 100, "All"]
//         ],
//         "aoColumns": [
//             { "sWidth": "17%",'bSortable': true},
//             { "sWidth": "15%",'bSortable': false},
//             { "sWidth": "15%",'bSortable': false},
//             { "sWidth": "15%",'bSortable': false},
//             { "sWidth": "30%",'bSortable': false},
//             { "sWidth": "30%",'bSortable': false},
//             { "sWidth": "15%",'bSortable': true},
//             { "sWidth": "15%",'bSortable': true}
//         ],
//
//     });
//
// }


