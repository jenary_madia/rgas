var base_url = $("base").attr("href");
new Vue({
    el : "#mrf_create",
    filters: {
        uppercase : function (value) {
            if (!value) return '';
            value = value.toString();
            return value.toUpperCase();
        }
    },
    data : {
        recruitmentStaff : "",
        deptHead : "",
        csmdBSA : "",
        jobLevel : "",
        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },
        toAddRecruitmentDetails : {
            candidate : "",
            dateEndorsed : "",
            remarks : ""
        },
        toEditRecruitmentDetails : {

        },
        recruitmentDetails : [],
        status : true,
        CSMDattachments : [],
        comment : "",
        recruitmentEdit : false,
        recruitmentDelete : false,
        recruitmentDetailsOld : ""

    },
    methods : {
        addRecruitment : function () {
            this.recruitmentDetails.push({
                candidate : this.toAddRecruitmentDetails.candidate,
                dateEndorsed : this.toAddRecruitmentDetails.dateEndorsed,
                remarks : this.toAddRecruitmentDetails.remarks,
                active : false
            });
            this.$set("toAddRecruitmentDetails.candidate","");
            this.$set("toAddRecruitmentDetails.dateEndorsed","");
            this.$set("toAddRecruitmentDetails.remarks","");
        },

        getForEdit : function(index,rec) {
            for (var i=0; i < this.recruitmentDetails.length; i++) {
                this.recruitmentDetails[i]['active'] = false
            }

            this.recruitmentDetails[index]['active'] = true;
            this.$set("toEditRecruitmentDetails", JSON.parse(JSON.stringify(rec)));
            this.$set("toEditRecruitmentDetails.id",index);
            this.$set("recruitmentEdit",true);
            this.$set("recruitmentDelete",true);
        },

        saveEditRec : function () {
            var id = this.toEditRecruitmentDetails.id;
            this.recruitmentDetails[id]["active"] =  false,
            this.recruitmentDetails[id]["candidate"] =  this.toEditRecruitmentDetails.candidate;
            this.recruitmentDetails[id]["dateEndorsed"] =  this.toEditRecruitmentDetails.dateEndorsed;
            this.recruitmentDetails[id]["remarks"] =  this.toEditRecruitmentDetails.remarks;
            this.$set("recruitmentEdit",false);
            this.$set("recruitmentEdit",false);
        },

        toggleEditRec : function () {
            if(! this.recruitmentEdit) {
                alert("Please select specific project requirement to edit.");
            }else{
                $("#editRecruitmentDetails").modal('show');
            }
        },

        toggleDeleteRec : function () {
            if(! this.recruitmentDelete) {
                alert("Please select specific project requirement to delete.");
            }else{
                var r = confirm("ARE YOU SURE YOU WANT TO DELETE?");
                if (r == true) {
                    var id = this.toEditRecruitmentDetails.id;
                    this.recruitmentDetails.splice(id,1);
                    alert("RECRUITMENT DETAIL DELETED");
                }
                this.$set("recruitmentEdit",false);
                this.$set("recruitmentDelete",false);
            }
        },

        returnProcess : function (process) {
            var that = this;
            this.getCSMDAttachments();
            var params = {
                id : mrfID,
                comment : this.comment,
                CSMDattachments : that.CSMDattachments,
                process : process
            };
            this.fetch('/mrf/return',params);
        },

        sendForProcess : function () {
            var that = this;
            this.getCSMDAttachments();
            var sendParams = {
                action : "forProcess",
                id : mrfID,
                recruitmentStaff : this.recruitmentStaff,
                recruitmentDetails : this.recruitmentDetails,
                CSMDattachments : this.CSMDattachments,
                jobLevel : this.jobLevel,
                comment : this.comment,
            };

            this.fetch('/mrf/process',sendParams);
        },

        assignProcess : function () {
            var that = this;
            this.getCSMDAttachments();
            var sendParams = {
                action : "assign",
                id : mrfID,
                recruitmentStaff : this.recruitmentStaff,
                deptHead : this.deptHead,
                jobLevel : this.jobLevel,
                csmdBSA : this.csmdBSA,
                comment : this.comment,
                CSMDattachments : that.CSMDattachments
            };

            this.fetch('/mrf/process',sendParams);
        },

        processFor : function (action) {
            var that = this;
            this.getCSMDAttachments();
            var sendParams = {
                action : action,
                id : mrfID,
                recruitmentStaff : that.recruitmentStaff,
                jobLevel : that.jobLevel,
                csmdBSA : that.csmdBSA,
                comment : that.comment,
                CSMDattachments : that.CSMDattachments
            };

            this.fetch('/mrf/process',sendParams);
        },

        saveRecruitment : function () {
            var saveParams = {
                id : mrfID,
                recruitmentDetails : this.recruitmentDetails,
                comment : this.comment,
            };

            this.fetch('/mrf/save-recruitment',saveParams);
        },
        
        fetch : function (actionUrl,params) {
            var that = this;
            Vue.http.post(base_url+actionUrl,params,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('CSMDattachments',[]);
                        // that.$set('PDQattachments',[]);
                        // that.$set('OCattachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        
        getCSMDAttachments : function() {
            var that = this;
            $(".CSMDattachmentData").each(function() {
                that.CSMDattachments.push(JSON.parse($(this).val()));
            });
        }
    },
    computed : {

    },
    watch : {

    },
    ready : function () {
        for(var candidate in JSON.parse(this.recruitmentDetailsOld)) {
            var person = JSON.parse(this.recruitmentDetailsOld)[candidate];
            this.recruitmentDetails.push({
                candidate : person['candidate'],
                dateEndorsed : person['date_endorsed'],
                remarks : person['remarks'],
                active : false
            });
        }
    },
});