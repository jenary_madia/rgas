var base_url = $("base").attr("href");
new Vue({
    el : "#mrf_create",
    filters: {
        uppercase : function (value) {
            if (!value) return '';
            value = value.toString();
            return value.toUpperCase();
        }
    },
    data : {
        formDetails : {
            company : "",
            contactNumber : "",
            dateFiled : "",
            department : "",
            employeeName : "",
            employeeNumber : "",
            referenceNo : "",
            section : "",
            status : ""
        },
        requestDetails : {
            requestFor : "",
            company : "",
            department : "",
            positionTitle: "",
            dateNeeded: "",
            nature: "",
            internal: "",
            external: "",
            count: "",
            otherCount: "",
            periodCovered: "",
            reason: "",
        },
        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },
        status : true,
        MRAFattachments : [],
        PDQattachments : [],
        OCattachments : [],
        comment : "",
        messages : "",
        mrfID : mrfID

    },
    methods : {
        returnMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                id : mocID,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/return_to_filer',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        createMRF : function (action) {
            var that = this;
            $(".attachmentData").each(function() {
                that.MRAFattachments.push(JSON.parse($(this).val()));
            });

            $(".PDQattachmentData").each(function() {
                that.PDQattachments.push(JSON.parse($(this).val()));
            });

            $(".OCattachmentData").each(function() {
                that.OCattachments.push(JSON.parse($(this).val()));
            });
            var createParams = {
                action : action,
                mrfID : this.mrfID,
                formDetails : this.formDetails,
                requestDetails : this.requestDetails,
                MRAFattachments : this.MRAFattachments,
                PDQattachments : this.PDQattachments,
                OCattachments : this.OCattachments,
                comment : this.comment
            };
            if(action == "send") {
                this.fetch('/mrf/send',createParams);
            }else if(action == "save") {
                this.fetch('/mrf/save',createParams);
            }
        },

        cancelProcess : function () {
            var saveParams = {
                id : mrfID,
                comment : this.comment
            };

            this.fetch('/mrf/cancel-mrf',saveParams);
        },

        fetch : function (actionUrl,params) {
            var that = this;
            Vue.http.post(base_url+actionUrl,params,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('MRAFattachments',[]);
                        that.$set('PDQattachments',[]);
                        that.$set('OCattachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        }
    },
    computed : {

    },
    watch : {

    },
    ready : function () {
        
    },
});