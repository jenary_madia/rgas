$(document).ready(function(){
	var base_url = $("base").attr("href");
    $('#saved').dataTable( {
		  "sPaginationType": "full_numbers",
		  "bProcessing": true,
		  "bServerSide": true,
		  "sAjaxSource": base_url + "/pmarf/save_pmarf",
		  "fnServerData": fnDataTablesPipeline,
		  "oLanguage": {
			"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
		  },
		  "fnDrawCallback": function( oSettings ) {
              
		   },
		  "aLengthMenu": [
			[10, 20, 50, 100, 200, -1],
			[10, 20, 50, 100, 200, "All"]
			], 
		  "aoColumns": [
			{ "sClass": "td_center", "bSortable": true },
			{ "sClass": "td_center", "bSortable": true },
			{ "sClass": "td_center", "bSortable": true },
			{ "sClass": "td_center", "bSortable": false },			
			{ "sClass": "td_center", "bSortable": false },
			{ "sClass": "td_center", "bSortable": false },			
			{ "sClass": "td_center", "bSortable": false },
		]
	});
});

