var base_url = $("base").attr("href");
$(document).ready(function(){
    if ( $("#myMOC").length != 0 ) {
        var requests_my_table = $('#myMOC').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/moc/get_my_moc/5",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#forEndorsementMOC").length != 0 ) {
        var forEndorsementMOC = $('#forEndorsementMOC').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/moc/get_endorsement_moc/5",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ($("#forApprovalMOC").length != 0 ) {
        var forApprovalMOC = $('#forApprovalMOC').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/moc/get_approval_moc/5",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())

            "fnDrawCallback": function( oSettings ) {
                if(! $('.generate').length) {
                    $('div#submittedMOC_filter').append(' <button class="btn btndefault btn-default generate pull-left" id="btnGenerate" style="margin-right: 10px;" id="approveSelected" name="action" value="">GENERATE REPORT</button>');
                }

                if(! $('.export').length) {
                    $('div#submittedMOC_filter').append(' <button class="btn btndefault btn-default export pull-left" style="margin-right: 10px;" id="approveSelected" name="action" value="">EXPORT TABLE TO CSV</button>');
                }

                $('div#submittedMOC_length label').hide();
            },

            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    $('body').on('submit','#formMyMOC',function (e) {
        var r = confirm("Delete this leave?");
        if (r == true) {
            $("#formMyleaves").submit;
        }else{
            e.preventDefault();
        }
    });

});


var hello = 'asdasd';
if ( $("#forAssessmentMOC").length != 0 ) {
    var forAssessmentMOC = $('#forAssessmentMOC').dataTable( {
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "sAjaxSource": base_url + "/moc/get_assessment_moc",
        "oLanguage": {
            "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
            "sSearch" : "Search : "
        },
        // fnFilter($(this).val())
        "fnDrawCallback": function( oSettings ) {
            if(! $('.generate').length) {
                $('div#forAssessmentMOC_filter').append(' <button class="btn btndefault btn-default generate pull-left" id="btnGenerate" style="margin-right: 10px;" id="approveSelected" name="action" value="">GENERATE REPORT</button>');
            }

            if(! $('.export').length) {
                $('div#forAssessmentMOC_filter').append(' <button class="btn btndefault btn-default export pull-left" style="margin-right: 10px;" id="approveSelected" name="action" value="export">EXPORT TABLE TO</button>');
            }
        },
        "aLengthMenu": [
            [10, 20, 50, 100, -1],
            [10, 20, 50, 100, "All"]
        ],
        "aoColumns": [
            { "sWidth": "17%",'bSortable': true},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "30%",'bSortable': false},
            { "sWidth": "30%",'bSortable': false},
            { "sWidth": "15%",'bSortable': true},
            { "sWidth": "15%",'bSortable': true}
        ],

    });

}


