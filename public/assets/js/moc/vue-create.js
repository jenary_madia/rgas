var base_url = $("base").attr("href");
new Vue({
    el : "#moc_create",
    data : {
        formDetails : {
            employeeName : "",
            employeeNumber : "",
            company : "",
            department : "",
            section : "",
            contactNumber : "",
        },
        mocDetails : {
            requestTypes : [],
            urgency : "",
            changeIn : [],
            changeInOthersInput : "",
            changeType : "",
            targetDate : "",
            departmentsInvolved : [],
            title : "",
            backgroundInfo : ""
        },
        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },
        status : true,
        attachments : [],
        assessment : "",
        comment : "",

        /*******FOR REQUEST TABLE DISABLING*********/

        enabledForRequest : false,
        
        /*****FOR PARSING DEPARTMENT INVOLVED****/
        departments : [],
        selectedDepartment : {
            id : "",
            data : {}
        }

    },
    methods : {
        addDeptInvolved : function () {
            this.$set("selectedDepartment.data",this.departments[this.selectedDepartment.id]);
            this.departments.splice(this.selectedDepartment.id, 1);
            this.mocDetails.departmentsInvolved.push(this.selectedDepartment.data);
            this.departments.sort();
        },
        removeDeptInvolved : function (deptId) {
            this.departments.push(this.mocDetails.departmentsInvolved[deptId]);
            this.mocDetails.departmentsInvolved.splice(deptId, 1);
            this.departments.sort();
        },

        sendMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                    action : "send",
                    formDetails : this.formDetails,
                    mocDetails : this.mocDetails,
                    attachments : this.attachments,
                    assessment : this.assessment,
                    comment : this.comment
            };
            Vue.http.post(base_url+'/moc/send_moc',sendParams).then(function (response) {
                that.$set("submitStatus.submitted",true);
                that.$set("submitStatus.success",response.data['success']);
                that.$set("submitStatus.message",response.data['message']);
                that.$set("submitStatus.errors",response.data['errors']);
                window.scrollTo(0,0);
                if(response.data["success"] == true) {
                    setTimeout(function(){window.location = response.data["url"]}, 1000)
                }else{
                    that.$set('attachments',[]);
                }
            },
            function () {
                alert("Something went wrong");
            });
        },

        saveMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var saveParams = {
                action : "save",
                formDetails : this.formDetails,
                mocDetails : this.mocDetails,
                attachments : this.attachments,
                successMessage : "Successfully saved",
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/save_moc',saveParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
    },
    computed : {

    },
    watch : {
        'mocDetails.requestTypes' : function () {
            if (this.mocDetails.requestTypes.length > 0) {
                for (i = 0; i < this.mocDetails.requestTypes.length; i++) {
                    if(this.mocDetails.requestTypes[i] == 'process_design' || this.mocDetails.requestTypes[i] == 'review_change') {
                        this.$set("enabledForRequest",true);
                        break;
                    }else{
                        this.$set("enabledForRequest",false);
                    }
                }
            }else{
                this.$set("enabledForRequest",false);
            }
        }
    },
    ready : function () {
        var that = this;
        Vue.http.get(base_url+'/moc/ajax_get_departments').then(function (response) {
                that.$set("departments",response.data);
                that.departments.sort();
            },
            function () {
                console.log("Something went wrong");
            });
    },
});