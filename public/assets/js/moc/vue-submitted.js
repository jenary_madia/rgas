/**
 * Created by user on 12/6/2016.
 */
var base_url = $("base").attr("href");
new Vue({
    el : "#submitted-moc",
    filters: {
        uppercase : function (value) {
            if (!value) return '';
            value = value.toString();
            return value.toUpperCase();
        }
    },
    data : {
        company : "0",
        department : "0",
        departmentList : [],
        status : "",
        from : "",
        to : ""
    },
    methods : {
        generateReport : function () {
            var that = this;
            var searchParams = {
                company : this.company,
                department : this.department,
            };
            Vue.http.post(base_url+'/moc/search-moc',searchParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    forAssessmentMOC.fnClearTable();
                    for (var i = 0; i < response.data.length; i++) {
                        forAssessmentMOC.fnAddData([
                            response.data[i][0],
                            response.data[i][1],
                            response.data[i][2],
                            response.data[i][3],
                            response.data[i][4],
                            response.data[i][5],
                            response.data[i][6],
                            response.data[i][7],
                            response.data[i][8]
                        ]);
                    }
                    $('#generateModal').modal('hide');
                },
                function () {
                    alert("Something went wrong");
                });
        },
    },
    computed : {

    },
    watch : {
        "company" : function () {
            var that = this;
            var searchParams = {
                company : this.company,
            };
            Vue.http.post(base_url+'/moc/get-departments',searchParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set('departmentList',response.data);
                },
                function () {
                    alert("Something went wrong");
                });
            // alert(hello);

            Vue.http.post(base_url+'/moc/search-moc',searchParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    forAssessmentMOC.fnClearTable();
                    for (var i = 0; i < response.data.length; i++) {
                        forAssessmentMOC.fnAddData([
                            response.data[i][0],
                            response.data[i][1],
                            response.data[i][2],
                            response.data[i][3],
                            response.data[i][4],
                            response.data[i][5],
                            response.data[i][6],
                            response.data[i][7],
                            response.data[i][8]
                        ]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        "department" : function () {
            var that = this;
            var searchParams = {
                company : this.company,
                department : this.department,
            };
            Vue.http.post(base_url+'/moc/search-moc',searchParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    forAssessmentMOC.fnClearTable();
                    for (var i = 0; i < response.data.length; i++) {
                        forAssessmentMOC.fnAddData([
                            response.data[i][0],
                            response.data[i][1],
                            response.data[i][2],
                            response.data[i][3],
                            response.data[i][4],
                            response.data[i][5],
                            response.data[i][6],
                            response.data[i][7],
                            response.data[i][8]
                        ]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        }
    },
    ready : function () {

    },
});