var base_url = $("base").attr("href");
new Vue({
    el : "#moc_edit",
    filters: {
        uppercase : function (value) {
            if (!value) return '';
            value = value.toString();
            return value.toUpperCase();
        }
    },
    data : {
        formDetails : {
            company : "",
            contactNumber : "",
            dateFiled : "",
            department : "",
            employeeName : "",
            employeeNumber : "",
            referenceNo : "",
            section : "",
            status : ""
        },
        mocDetails : {
            requestTypes : [],
            urgency : "",
            changeIn : [],
            changeInOthersInput : "",
            changeType : "",
            targetDate : "",
            departmentsInvolved : [],
            title : "",
            backgroundInfo : ""
        },
        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },
        status : true,
        attachments : [],
        attachmentsOld : [],
        SSMDattachments : [],
        SSMDattachmentsOld : [],
        CSMDattachments : [],
        CSMDattachmentsOld : [],
        assessment : "",
        comment : "",
        messages : "",

        /*******FOR REQUEST TABLE DISABLING*********/

        enabledForRequest : false,

        /*****FOR PARSING DEPARTMENT INVOLVED****/
        departments : [],
        selectedDepartment : {
            id : "",
            data : {}
        }

    },
    methods : {
        returnMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                id : mocID,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/return_to_filer',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        addDeptInvolved : function () {
            this.$set("selectedDepartment.data",this.departments[this.selectedDepartment.id]);
            this.departments.splice(this.selectedDepartment.id, 1);
            this.mocDetails.departmentsInvolved.push(this.selectedDepartment.data);
            this.departments.sort();
        },
        removeDeptInvolved : function (deptId) {
            this.departments.push(this.mocDetails.departmentsInvolved[deptId]);
            this.mocDetails.departmentsInvolved.splice(deptId, 1);
            this.departments.sort();
        },

        sendMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                action : "send",
                formDetails : this.formDetails,
                mocDetails : this.mocDetails,
                attachments : this.attachments.concat(that.attachmentsOld),
                assessment : this.assessment,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/resend_moc',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                that.$set("submitStatus.submitted",true);
                that.$set("submitStatus.success",response.data['success']);
                that.$set("submitStatus.message",response.data['message']);
                that.$set("submitStatus.errors",response.data['errors']);
                window.scrollTo(0,0);
                if(response.data["success"] == true) {
                    setTimeout(function(){window.location = response.data["url"]}, 1000)
                }else{
                    that.$set('attachments',[]);
                }
            },
            function () {
                alert("Something went wrong");
            });
        },

        saveMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var saveParams = {
                action : "save",
                formDetails : this.formDetails,
                mocDetails : this.mocDetails,
                attachments : this.attachments.concat(that.attachmentsOld),
                assessment : this.assessment,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/resave_moc',saveParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },

        cancelMOC : function () {
            var that = this;
            var cancelParams = {
                action : "cancel",
                id : mocID,
                comment : this.comment,
                successMessage : "Successfully cancelled",
            };
            Vue.http.post(base_url+'/moc/cancel_moc',cancelParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },

        acknowledgeMOC : function () {
            var that = this;
            var sendParams = {
                id: mocID,
                comment: this.comment
            };
            Vue.http.post(base_url + '/moc/acknowledge_moc', sendParams, {
                headers: {
                    "Cache-Control": "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }
            }).then(function (response) {
                    that.$set("submitStatus.submitted", true);
                    that.$set("submitStatus.success", response.data['success']);
                    that.$set("submitStatus.message", response.data['message']);
                    that.$set("submitStatus.errors", response.data['errors']);
                    window.scrollTo(0, 0);
                    if (response.data["success"] == true) {
                        setTimeout(function () {
                            window.location = response.data["url"]
                        }, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });

        },

        sendToMD : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                action : "sendToMD",
                id : mocID,
                formDetails : this.formDetails,
                mocDetails : this.mocDetails,
                attachments : this.attachments.concat(that.attachmentsOld),
                assessment : this.assessment,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/send_to_md',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        removeAttachment : function (index) {
            this.attachmentsOld.splice(index,1);
        },
    },
    computed : {

    },
    watch : {
        'mocDetails.requestTypes' : function () {
            if (this.mocDetails.requestTypes.length > 0) {
                for (var i = 0; i < this.mocDetails.requestTypes.length; i++) {
                    if(this.mocDetails.requestTypes[i] == 'process_design' || this.mocDetails.requestTypes[i] == 'review_change') {
                        this.$set("enabledForRequest",true);
                        break;
                    }else{
                        this.$set("enabledForRequest",false);
                        // break;
                    }
                }
            }else{
                this.$set("enabledForRequest",false);
            }
        },
        'enabledForRequest' : function () {
            if(this.enabledForRequest == false) {
                this.$set("mocDetails.changeIn",[]);
                this.$set("mocDetails.changeInOthersInput","");
                this.$set("mocDetails.changeType","");
                this.$set("mocDetails.targetDate","");
                this.$set("mocDetails.departmentsInvolved",[]);
            }
        }
    },
    ready : function () {
        var that = this;
        Vue.http.get(base_url+'/moc/ajax_get_departments').then(function (response) {
                that.$set("departments",response.data);
                that.departments.sort();
            },
            function () {
                console.log("Something went wrong");
            });

        Vue.http.post(base_url+'/moc/ajax_feed_edit',{ id : mocID }).then(function (response) {
            that.$set('formDetails.employeeName',response.data['firstname']+' '+response.data['middlename']+' '+response.data['lastname']);
            that.$set('formDetails.referenceNo',response.data['transaction_code']);
            that.$set('formDetails.employeeNumber',response.data['employeeid']);
            that.$set('formDetails.company',response.data['company']);
            that.$set('formDetails.department',JSON.parse(response.data['dept_sec'])[0]);
            that.$set('formDetails.section',JSON.parse(response.data['dept_sec'])[1]);
            that.$set('formDetails.contactNumber',response.data['contact_no']);
            that.$set('formDetails.dateFiled',response.data['date']);
            that.$set('formDetails.status',response.data['status']);

            for(var i in response.data['rcodes']) {
                that.mocDetails.requestTypes.push(response.data['rcodes'][i]['request_code']);
            }

            that.$set('mocDetails.urgency',response.data['urgency']);
                var rcodesValues = [];
                for(var i = 0; i < response.data['rcodes'].length; i++)
                {
                    rcodesValues.push(response.data['rcodes'][i]['request_code']);
                }

            if (rcodesValues.indexOf("review_change") != -1 || rcodesValues.indexOf("process_design") != -1) {
                for(var i in response.data['ccodes']) {
                    that.mocDetails.changeIn.push(response.data['ccodes'][i]['changein_code']);
                    if(response.data['ccodes'][i]['changein_code'] == "others") {
                        that.$set('mocDetails.changeInOthersInput',response.data['ccodes'][i]['changein_desc']);
                    }
                }
                that.$set('mocDetails.changeType',response.data['change_type']);
                that.$set('mocDetails.targetDate',response.data['implementation_date']);
                for(var i in JSON.parse(response.data['dept_involved'])) {
                    that.mocDetails.departmentsInvolved.push(JSON.parse(response.data['dept_involved'])[i]);
                }
            }
            that.$set('mocDetails.title',response.data['title']);
            that.$set('mocDetails.backgroundInfo',response.data['reason']);
            that.$set('mocDetails.ssmdActionPlan',response.data['ssmd_action_plan']);
            that.$set('mocDetails.csmdActionPlan',response.data['csmd_action_plan']);
            for(var i in response.data['attachments']) {
                $attachDetails = JSON.parse(response.data['attachments'][i]['fn']);
                $attachDetails['urlDownload'] = response.data['attachments'][i]['urlDownload'];
                if(response.data['attachments'][i]['attachment_type'] == 0) {
                    that.attachmentsOld.push($attachDetails);
                }else if(response.data['attachments'][i]['attachment_type'] == 1) {
                    that.SSMDattachmentsOld.push($attachDetails);
                }else if(response.data['attachments'][i]['attachment_type'] == 2) {
                    that.CSMDattachmentsOld.push($attachDetails);
                }
            }

            if(response.data['status'] == 'NEW') {
                that.$set("comment",response.data['comments'][0]['comment']);
            }else {
                var messages = [];
                for (var i in response.data['comments']) {
                     messages.push(response.data['comments'][i]['employee']['firstname']+' '+response.data['comments'][i]['employee']['middlename']+' '+response.data['comments'][i]['employee']['lastname']+' '+response.data['comments'][i]['ts_comment']+' : '+response.data['comments'][i]['comment']);
                }
                that.$set("messages",messages.join('\n'));
            }

        },
        function () {
            alert("Something went wrong");
        });
    },
});