var base_url = $("base").attr("href");
new Vue({
    el : "#msr_create",
    data : {
        //For project requirements
        toAddProjectReq : {
            jobTitle : "",
            recruitment : {
                from : "",
                to : "",
                pax : ""

            },
            preparation : {
                from : "",
                to : "",
                pax : ""

            },
            briefing : {
                from : "",
                to : "",
                pax : ""

            },
            fieldWork : {
                from : "",
                to : "",
                pax : "",
                area : ""

            },
            editing : {
                from : "",
                to : "",
                pax : ""

            },
            coding : {
                from : "",
                to : "",
                pax : ""

            },
            encoding : {
                from : "",
                to : "",
                pax : ""

            },
            dataCleaning : {
                from : "",
                to : "",
                pax : ""

            },
            active : false
        },

        toEditProjectReq : {

        },

        projectRequirements : [],
        projectRequirementsOld : "",
        projReqEditable : false,
        projReqDeletable : false,

        //For agency
        toAddAgency : {
            agency : "",
            contactInfo : "",
            active : false
        },

        toEditAgency : {

        },

        agencies : [],
        agenciesOld : [],
        agencyEditable : false,
        agencyDeletable : false,
        
        jobPositions : "",
        latestJobPositions : []
    },
    methods : {
        // FOR PROJECT REQUIREMENTS
        addProjectReq : function () {
            var errors = 0;
            if(this.toAddProjectReq.recruitment.from != "" && this.toAddProjectReq.recruitment.to != "") {
                if(this.toAddProjectReq.recruitment.from > this.toAddProjectReq.recruitment.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.preparation.from != "" && this.toAddProjectReq.preparation.to != "") {
                if(this.toAddProjectReq.preparation.from > this.toAddProjectReq.preparation.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.briefing.from != ""  && this.toAddProjectReq.briefing.to  != "" ) {
                if(this.toAddProjectReq.briefing.from > this.toAddProjectReq.briefing.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.fieldWork.from != ""  && this.toAddProjectReq.fieldWork.to != "" ) {
                if(this.toAddProjectReq.fieldWork.from > this.toAddProjectReq.fieldWork.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.editing.from != ""  && this.toAddProjectReq.editing.to != "" ) {
                if(this.toAddProjectReq.editing.from > this.toAddProjectReq.editing.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.coding.from != "" && this.toAddProjectReq.coding.to != "" ) {
                if(this.toAddProjectReq.coding.from > this.toAddProjectReq.coding.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.encoding.from != "" && this.toAddProjectReq.encoding.to != "" ) {
                if(this.toAddProjectReq.encoding.from > this.toAddProjectReq.encoding.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.dataCleaning.from != ""  && this.toAddProjectReq.dataCleaning.to != ""  ) {
                if(this.toAddProjectReq.dataCleaning.from > this.toAddProjectReq.dataCleaning.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if (errors == 0) {

                var addProjectReq = this.toAddProjectReq;
                var jCode = this.toAddProjectReq.jobTitle;
                var positions = this.latestJobPositions;
                for (var i=0; i < positions.length; i++) {
                    if(jCode == positions[i]['item']) {
                        addProjectReq['jobTitle'] = positions[i]['text'];
                        this.latestJobPositions.splice(i, 1);
                    }
                }
                var jsonData = {};
                for(var data in addProjectReq) {
                    jsonData[data] = addProjectReq[data];
                }
                var id = this.projectRequirements.length ;
                this.projectRequirements.push(jsonData);
                $('#addProjectRequirement').modal('hide');
                this.$set('toAddProjectReq.jobTitle','');
                this.$set('toAddProjectReq.recruitment',{
                    from : "",
                    to : "",
                    pax : ""

                });
                this.$set('toAddProjectReq.preparation',{
                    from : "",
                    to : "",
                    pax : ""

                });
                this.$set('toAddProjectReq.briefing',{
                    from : "",
                    to : "",
                    pax : ""

                });
                this.$set('toAddProjectReq.fieldWork',{
                    from : "",
                    to : "",
                    pax : "",
                    area : ""

                });

                this.$set('toAddProjectReq.editing',{
                    from : "",
                    to : "",
                    pax : ""

                });

                this.$set('toAddProjectReq.coding',{
                    from : "",
                    to : "",
                    pax : ""

                });

                this.$set('toAddProjectReq.encoding',{
                    from : "",
                    to : "",
                    pax : ""

                });

                this.$set('toAddProjectReq.encoding',{
                    from : "",
                    to : "",
                    pax : ""

                });

                this.$set("latestJobPositions",positions);
                this.$set('toAddProjectReq.active',false);
            }
            
        },

        getForEdit : function(index,req) {
            for (var i=0; i < this.projectRequirements.length; i++) {
                this.projectRequirements[i]['active'] = false
            }

            this.projectRequirements[index]['active'] = true;
            this.$set("toEditProjectReq", JSON.parse(JSON.stringify(req)));
            this.$set("toEditProjectReq.id",index);
            this.$set("projReqEditable",true);
            this.$set("projReqDeletable",true);
        },

        savePRUpdate : function () {

            var errors = 0;
            if(this.toAddProjectReq.recruitment.from != "" && this.toAddProjectReq.recruitment.to != "") {
                if(this.toAddProjectReq.recruitment.from > this.toAddProjectReq.recruitment.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.preparation.from != "" && this.toAddProjectReq.preparation.to != "") {
                if(this.toAddProjectReq.preparation.from > this.toAddProjectReq.preparation.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.briefing.from != ""  && this.toAddProjectReq.briefing.to  != "" ) {
                if(this.toAddProjectReq.briefing.from > this.toAddProjectReq.briefing.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.fieldWork.from != ""  && this.toAddProjectReq.fieldWork.to != "" ) {
                if(this.toAddProjectReq.fieldWork.from > this.toAddProjectReq.fieldWork.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.editing.from != ""  && this.toAddProjectReq.editing.to != "" ) {
                if(this.toAddProjectReq.editing.from > this.toAddProjectReq.editing.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.coding.from != "" && this.toAddProjectReq.coding.to != "" ) {
                if(this.toAddProjectReq.coding.from > this.toAddProjectReq.coding.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.encoding.from != "" && this.toAddProjectReq.encoding.to != "" ) {
                if(this.toAddProjectReq.encoding.from > this.toAddProjectReq.encoding.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(this.toAddProjectReq.dataCleaning.from != ""  && this.toAddProjectReq.dataCleaning.to != ""  ) {
                if(this.toAddProjectReq.dataCleaning.from > this.toAddProjectReq.dataCleaning.to ) {
                    alert("Recruitment start date is greater the end date");
                    errors++;
                }
            }

            if(errors == 0) {}
            var editProjectReq = this.toEditProjectReq;
            var id = this.toEditProjectReq.id;
            for(var data in editProjectReq) {
                this.projectRequirements[id][data] = editProjectReq[data];
            }
            console.log(this.projectRequirements[id]);
            this.$set("projReqEditable",false);
            this.$set("projReqDeletable",false);
        },

        toggleEditProjReq : function () {
            if(! this.projReqEditable) {
                alert("Please select specific project requirement to edit.");
            }else{
                $("#editProjectRequirement").modal('show');
            }
        },

        toggleDeleteProjReq : function () {
            if(! this.projReqEditable) {
                alert("Please select specific project requirement to delete.");
            }else{
                var r = confirm("ARE YOU SURE YOU WANT TO DELETE?");
                if (r == true) {
                    var id = this.toEditProjectReq.id;
                    this.projectRequirements.splice(id,1);
                    alert("PROJECT REQUIREMENT DELETED");
                }
                this.$set("projReqEditable",false);
                this.$set("projReqDeletable",false);
            }
        },

        //    FOR AGENCIES
        addAgency : function () {
            var id = this.agencies.length ;
            this.agencies.push({
                agency: this.toAddAgency.agency,
                contactInfo: this.toAddAgency.contactInfo,
                active: this.toAddAgency.active,
            });
            $('#addManPowerAgency').modal('hide');
            this.$set('toAddAgency.agency','');
            this.$set('toAddAgency.contactInfo','');
        },

        getForEditAgency : function(index,req) {
            for (var i=0; i < this.agencies.length; i++) {
                this.agencies[i]['active'] = false
            }

            this.agencies[index]['active'] = true
            this.$set("toEditAgency", JSON.parse(JSON.stringify(req)));
            this.$set("toEditAgency.id",index);
            this.$set("agencyEditable",true);
            this.$set("agencyDeletable",true);
        },

        saveAgencyUpdate : function () {
            var id = this.toEditAgency.id;
            console.log(id);
            this.agencies[id]["active"] =  false,
                this.agencies[id]["agency"] =  this.toEditAgency.agency;
            this.agencies[id]["contactInfo"] =  this.toEditAgency.contactInfo;
            this.$set("agencyEditable",false);
            this.$set("agencyDeletable",false);
        },

        toggleEditAgency : function () {
            if(! this.agencyEditable) {
                alert("Please select specific manpower agency to edit.");
            }else{
                $("#editManPowerAgency").modal('show');
            }
        },

        toggleDeleteAgency : function () {
            if(! this.agencyDeletable) {
                alert("Please select specific manpower agency to delete.");
            }else{
                var r = confirm("ARE YOU SURE YOU WANT TO DELETE?");
                if (r == true) {
                    var id = this.toEditAgency.id;
                    this.agencies.splice(id,1);
                    alert("AGENCY DELETED");
                }
                this.$set("agencyEditable",false);
                this.$set("agencyDeletable",false);
            }
        }
    },
    watch : {
        'projectRequirementsOld' : function () {

            this.$set("projectRequirements",JSON.parse(this.projectRequirementsOld));

        },
        'agenciesOld' : function () {
            this.$set("agencies",JSON.parse(this.agenciesOld));


        },
        'jobPositions' :  function () {
            this.$set("latestJobPositions",JSON.parse(this.jobPositions));
        }
    }
})