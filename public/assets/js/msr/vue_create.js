var base_url = $("base").attr("href");
new Vue({
    el : "#msr_create",
    data : {
        //For project requirements
        toAddProjectReq : {
            area : "",
            noOfPersonnel : "",
            noOfWorkingDays : "",
            jobTitle : "",
            dateNeeded : "",
            active : false
        },

        toEditProjectReq : {

        },

        projectRequirements : [],
        projectRequirementsOld : "",
        projReqEditable : false,
        projReqDeletable : false,
        //For agency
        toAddAgency : {
            agency : "",
            contactInfo : "",
            active : false
        },

        toEditAgency : {

        },

        agencies : [],
        agenciesOld : [],
        agencyEditable : false,
        agencyDeletable : false,
    },
    methods : {
    // FOR PROJECT REQUIREMENTS
        addProjectReq : function () {
            var id = this.projectRequirements.length ;
            this.projectRequirements.push({
                area: this.toAddProjectReq.area,
                noOfPersonnel: this.toAddProjectReq.noOfPersonnel,
                noOfWorkingDays: this.toAddProjectReq.noOfWorkingDays,
                jobTitle: this.toAddProjectReq.jobTitle,
                dateNeeded: this.toAddProjectReq.dateNeeded,
                active: this.toAddProjectReq.active,
            });
            $('#addProjectRequirement').modal('hide');
            this.$set('toAddProjectReq.area','');
            this.$set('toAddProjectReq.noOfPersonnel','');
            this.$set('toAddProjectReq.noOfWorkingDays','');
            this.$set('toAddProjectReq.jobTitle','');
            this.$set('toAddProjectReq.dateNeeded','');
        },

        getForEdit : function(index,req) {
            for (var i=0; i < this.projectRequirements.length; i++) {
                this.projectRequirements[i]['active'] = false
            }

            this.projectRequirements[index]['active'] = true;
            this.$set("toEditProjectReq", JSON.parse(JSON.stringify(req)));
            this.$set("toEditProjectReq.id",index);
            this.$set("projReqEditable",true);
            this.$set("projReqDeletable",true);
        },

        savePRUpdate : function () {
            var id = this.toEditProjectReq.id;
            this.projectRequirements[id]["active"] =  false,
            this.projectRequirements[id]["area"] =  this.toEditProjectReq.area;
            this.projectRequirements[id]["noOfPersonnel"] =  this.toEditProjectReq.noOfPersonnel;
            this.projectRequirements[id]["noOfWorkingDays"] =  this.toEditProjectReq.noOfWorkingDays;
            this.projectRequirements[id]["jobTitle"] =  this.toEditProjectReq.jobTitle;
            this.projectRequirements[id]["dateNeeded"] =  this.toEditProjectReq.dateNeeded;
            this.$set("projReqEditable",false);
            this.$set("projReqDeletable",false);
        },

        toggleEditProjReq : function () {
            if(! this.projReqEditable) {
                alert("Please select specific project requirement to edit.");
            }else{
                $("#editProjectRequirement").modal('show');
            }
        },

        toggleDeleteProjReq : function () {
            if(! this.projReqEditable) {
                alert("Please select specific project requirement to delete.");
            }else{
                var r = confirm("ARE YOU SURE YOU WANT TO DELETE?");
                if (r == true) {
                    var id = this.toEditProjectReq.id;
                    this.projectRequirements.splice(id,1);
                    alert("PROJECT REQUIREMENT DELETED");
                }
                this.$set("projReqEditable",false);
                this.$set("projReqDeletable",false);
            }
        },
        
    //    FOR AGENCIES
        addAgency : function () {
            var id = this.agencies.length ;
            this.agencies.push({
                agency: this.toAddAgency.agency,
                contactInfo: this.toAddAgency.contactInfo,
                active: this.toAddAgency.active,
            });
            $('#addManPowerAgency').modal('hide');
            this.$set('toAddAgency.agency','');
            this.$set('toAddAgency.contactInfo','');
        },

        getForEditAgency : function(index,req) {
            for (var i=0; i < this.agencies.length; i++) {
                this.agencies[i]['active'] = false
            }

            this.agencies[index]['active'] = true
            this.$set("toEditAgency", JSON.parse(JSON.stringify(req)));
            this.$set("toEditAgency.id",index);
            this.$set("agencyEditable",true);
            this.$set("agencyDeletable",true);
        },

        saveAgencyUpdate : function () {
            var id = this.toEditAgency.id;
            console.log(id);
            this.agencies[id]["active"] =  false,
            this.agencies[id]["agency"] =  this.toEditAgency.agency;
            this.agencies[id]["contactInfo"] =  this.toEditAgency.contactInfo;
            this.$set("agencyEditable",false);
            this.$set("agencyDeletable",false);
        },

        toggleEditAgency : function () {
            if(! this.agencyEditable) {
                alert("Please select specific manpower agency to edit.");
            }else{
                $("#editManPowerAgency").modal('show');
            }
        },

        toggleDeleteAgency : function () {
            if(! this.agencyDeletable) {
                alert("Please select specific manpower agency to delete.");
            }else{
                var r = confirm("ARE YOU SURE YOU WANT TO DELETE?");
                if (r == true) {
                    var id = this.toEditAgency.id;
                    this.agencies.splice(id,1);
                    alert("AGENCY DELETED");
                }
                this.$set("agencyEditable",false);
                this.$set("agencyDeletable",false);
            }
        }
    },
    watch : {
        'projectRequirementsOld' : function () {

            this.$set("projectRequirements",JSON.parse(this.projectRequirementsOld));

        },
        'agenciesOld' : function () {
            this.$set("agencies",JSON.parse(this.agenciesOld));

            // for (var i = 0; i < this.agencies.length; i++) {
            //     if(! ('active' in this.agencies[i])) {
            //         this.agencies[i]['active'] = false;
            //     }
            // }

        },

        'toAddProjectReq.noOfPersonnel' : function () {

        },

        'toAddProjectReq.noOfWorkingDays' : function () {

        }
    }
})