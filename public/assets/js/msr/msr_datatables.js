$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#myMSR").length != 0 ) {
        var requests_my_table = $('#myMSR').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/get_my_msr",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ( $("#superiorMSR").length != 0 ) {
        var requests_my_table = $('#superiorMSR').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/get_superior_msr",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    if ($("#submittedMSR").length != 0 ) {
        var submittedMSR = $('#submittedMSR').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/get_submitted_msr",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())

            "fnDrawCallback": function( oSettings ) {
                if(! $('.generate').length) {
                    $('div#submittedMSR_filter').append(' <button class="btn btndefault btn-default generate pull-left" id="btnGenerate" style="margin-right: 10px;" id="approveSelected" name="action" value="">GENERATE REPORT</button>');
                }

                if(! $('.export').length) {
                    $('div#submittedMSR_filter').append(' <button class="btn btndefault btn-default export pull-left" style="margin-right: 10px;" id="approveSelected" name="action" value="export">EXPORT TABLE TO</button>');
                }

                $('div#submittedMSR_length label').hide();
            },

            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

        });

    }

    $('body').on('submit','#formMyMSR',function (e) {
        var r = confirm("Delete this MSR?");
        if (r == true) {
            $("#formMyMSR").submit;
        }else{
            e.preventDefault();
        }
    });

    $('body').on('click','#btnGenerate',function (e) {
        e.preventDefault();
        $('#generateModal').modal('show');
    });

    $('body').on('change','#department',function () {
        $.ajax({
            url :  base_url + "/msr/get_filtered_submitted_msr",
            dataType : 'JSON',
            data : { department : $("#department").val() },
            method : 'POST'
        }).done(function(data) {
            submittedMSR.fnClearTable();
            for (var i = 0; i < data.length; i++) {
                submittedMSR.fnAddData([
                    data[i][0],
                    data[i][1],
                    data[i][2],
                    data[i][3],
                    data[i][4],
                    data[i][5]
                ]);
            }
        });
    });

    $('body').on('click','#generateReport',function () {
        $.ajax({
            url :  base_url + "/msr/get_filtered_submitted_msr",
            dataType : 'JSON',
            data : {
                department : $("#department-inside").val(),
                status : $("#status").val(),
                from : $("#from").val(),
                to : $("#to").val()
             },
            method : 'POST'
        }).done(function(data) {
            submittedMSR.fnClearTable();
            for (var i = 0; i < data.length; i++) {
                submittedMSR.fnAddData([
                    data[i][0],
                    data[i][1],
                    data[i][2],
                    data[i][3],
                    data[i][4],
                    data[i][5]
                ]);
            }
        });
    });
});

