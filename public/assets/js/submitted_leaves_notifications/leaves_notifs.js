$(document).ready(function(){
    var base_url = $("base").attr("href");
    
    //FOR LEAVES
    if ( $("#leaves_receiver").length != 0 ) {
        var receiver_leave_table = $('#leaves_receiver').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                {"bSortable" : true},
                {"bSortable" : true},
                {"bSortable" : false},
                {"bSortable" : false},
                {"bSortable" : false},
                {"bSortable" : false},
                {"bSortable" : false},
                { "sWidth": "15%","bSortable" : false}
            ]

        });
    }



    // FOR NOTIF OFFICIAL BUSINESS
    if ( $("#notif_official").length != 0 ) {
        var notif_official_table = $('#notif_official').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/notif_official",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "13%","bSortable" : true },
                { "sWidth": "10%","bSortable" : true },
                { "sWidth": "10%","bSortable" : false },
                { "sWidth": "15%","bSortable" : false },
                { "sWidth": "8%","bSortable" : false },
                { "sWidth": "10%","bSortable" : false },
                { "sWidth": "8%","bSortable" : false },
                { "bSortable" : false },
                { "sWidth": "15%","bSortable" : false},

            ]

        });

    }
    
//    FOR NOTIF OFFSET

    if ( $("#notif_offset").length != 0 ) {
        var notif_offset_table = $('#notif_offset').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/notif_offset",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "bSortable" : true },
                { "bSortable" : true },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "sWidth": "15%"}
            ]

        });

    }
    
//    FOR NOTIF TIME KEEPING CORRECTION

    if ( $("#notif_TKCorrection").length != 0 ) {
        var notif_TKCorrection_table = $('#notif_TKCorrection').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/notif_timekeep",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "bSortable" : true },
                { "bSortable" : true },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false,"sWidth": "15%"},

            ]

        });

    }

//    FOR NOTIF UNDERTIME
    
    if ( $("#notif_undertime").length != 0 ) {
        var notif_undertime_table = $('#notif_undertime').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/notif_undertime",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                {"bSortable" : true },
                {"bSortable" : true },
                {"bSortable" : false },
                {"bSortable" : false },
                {"bSortable" : false },
                {"bSortable" : false },
                { "sWidth": "15%" , "bSortable" : false},

            ]

        });

    }
   
//    FOR NOTIF CUT TIME

    if ( $("#notif_cuttime").length != 0 ) {
        var notif_cuttime_table = $('#notif_cuttime').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/notif_cuttime",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "sWidth": "15%","bSortable" : false },

            ]

        });

    }
    //for corporate and satellite
    $('#btnSearch').on('click',function (e) {
        receiver_leave_table.fnClearTable();
        notif_offset_table.fnClearTable();
        notif_undertime_table.fnClearTable();
        notif_cuttime_table.fnClearTable();
        notif_TKCorrection_table.fnClearTable();
        notif_official_table.fnClearTable();
        e.preventDefault();
        var form = $('#receiverSearch').serialize();
        $.ajax({
            url :  base_url + "/submitted/search",
            dataType : 'JSON',
            data : form,
            method : 'POST'
        }).done(function(data) {
            console.log(form);
            for (var i = 0; i < data['leave'].length; i++) {
                receiver_leave_table.fnAddData([
                    data['leave'][i][0],
                    data['leave'][i][1],
                    data['leave'][i][2],
                    data['leave'][i][3],
                    data['leave'][i][4],
                    data['leave'][i][5],
                    data['leave'][i][6],
                    data['leave'][i][7]
                ]);
            }

            for (var i = 0; i < data['notification']['TKCorction'].length; i++) {
                notif_TKCorrection_table.fnAddData([
                    data['notification']['TKCorction'][i][0],
                    data['notification']['TKCorction'][i][1],
                    data['notification']['TKCorction'][i][2],
                    data['notification']['TKCorction'][i][3],
                    data['notification']['TKCorction'][i][4],
                    data['notification']['TKCorction'][i][5],
                    data['notification']['TKCorction'][i][6],
                    data['notification']['TKCorction'][i][7],
                    data['notification']['TKCorction'][i][8],
                    data['notification']['TKCorction'][i][9]
                ]);
            }

            for (var i = 0; i < data['notification']['official'].length; i++) {
                notif_official_table.fnAddData([
                    data['notification']['official'][i][0],
                    data['notification']['official'][i][1],
                    data['notification']['official'][i][2],
                    data['notification']['official'][i][3],
                    data['notification']['official'][i][4],
                    data['notification']['official'][i][5],
                    data['notification']['official'][i][6],
                    data['notification']['official'][i][7],
                    data['notification']['official'][i][8],
                    data['notification']['official'][i][9]
                ]);
            }

            for (var i = 0; i < data['notification']['offset'].length; i++) {
                notif_offset_table.fnAddData([
                    data['notification']['offset'][i][0],
                    data['notification']['offset'][i][1],
                    data['notification']['offset'][i][2],
                    data['notification']['offset'][i][3],
                    data['notification']['offset'][i][4],
                    data['notification']['offset'][i][5],
                    data['notification']['offset'][i][6],
                    data['notification']['offset'][i][7],
                    data['notification']['offset'][i][8],
                    data['notification']['offset'][i][9],
                    data['notification']['offset'][i][10]
                ]);
            }

            for (var i = 0; i < data['notification']['undertime'].length; i++) {
                notif_undertime_table.fnAddData([
                    data['notification']['undertime'][i][0],
                    data['notification']['undertime'][i][1],
                    data['notification']['undertime'][i][2],
                    data['notification']['undertime'][i][3],
                    data['notification']['undertime'][i][4],
                    data['notification']['undertime'][i][5],
                    data['notification']['undertime'][i][6],
                    data['notification']['undertime'][i][7]
                ]);
            }

            for (var i = 0; i < data['notification']['cut_time'].length; i++) {
                notif_cuttime_table.fnAddData([
                    data['notification']['cut_time'][i][0],
                    data['notification']['cut_time'][i][1],
                    data['notification']['cut_time'][i][2],
                    data['notification']['cut_time'][i][3],
                    data['notification']['cut_time'][i][4],
                    data['notification']['cut_time'][i][5],
                    data['notification']['cut_time'][i][6],
                    data['notification']['cut_time'][i][7],
                    data['notification']['cut_time'][i][8],
                    data['notification']['cut_time'][i][9]
                ]);
            }
            

        });
    });

//  FOR PRODUCTION ENCODERS LEAVES
    if ( $("#PE_receiver_leaves").length != 0 ) {
        var pe_leave_datatable = $('#PE_receiver_leaves').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/pe_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "sWidth": "15%","bSortable" : false },

            ]

        });

    }

//  FOR PRODUCTION ENCODERS NOTIFICATION

    if ( $("#PE_receiver_notif").length != 0 ) {
        var pe_notif_datatable = $('#PE_receiver_notif').DataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/submitted/pe_notifications",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "sWidth": "15%","bSortable" : false },

            ]

        });

    }


    // for production encoders
    $('#btnSearchPE').on('click',function (e) {
        e.preventDefault();
        var form = $('#receiverSearchPE').serialize();
        $.ajax({
            url :  base_url + "/submitted/pe_search",
            dataType : 'JSON',
            data : form,
            method : 'POST'
        }).done(function(data) {
            pe_leave_datatable.fnClearTable();
            for (var i = 0; i < data['leaves'].length; i++) {
                pe_leave_datatable.fnAddData([
                    data['leaves'][i][0],
                    data['leaves'][i][1],
                    data['leaves'][i][2],
                    data['leaves'][i][3],
                    data['leaves'][i][4],
                    data['leaves'][i][5],
                    data['leaves'][i][6],
                    data['leaves'][i][7]
                ]);
            }

            pe_notif_datatable.fnClearTable();
            for (var i = 0; i < data['notifications'].length; i++) {
                pe_notif_datatable.fnAddData([
                    data['notifications'][i][0],
                    data['notifications'][i][1],
                    data['notifications'][i][2],
                    data['notifications'][i][3],
                    data['notifications'][i][4],
                    data['notifications'][i][5],
                    data['notifications'][i][6],
                    data['notifications'][i][7]
                ]);
            }
        });
    });
});