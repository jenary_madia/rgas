var base_url = $("base").attr("href");
new Vue({
    el : "#divPEReceiver",
    data : {
        departmentID : "",
        sectionID : "",
        sections  : [],
    },
    watch : {
        "departmentID" : function () {
            var that = this;
            Vue.http.post(
                base_url+'/ns/sections',
                { "dept_id" : this.departmentID },{
                    headers: {
                        // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).then(function (response) {
                    that.$set("sections",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
        }
    }
})