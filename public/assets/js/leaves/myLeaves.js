$(document).ready(function(){
	var base_url = $("base").attr("href");
    if ( $("#myLeaves").length != 0 ) {
        var requests_my_table = $('#myLeaves').dataTable( {
			"bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/my_leaves",
            "oLanguage": {
            "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
            "sSearch" : "Search : "
                },
                // fnFilter($(this).val())
                "fnDrawCallback": function( oSettings ) {

                },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { 'bSortable': false },
                { 'bSortable': false },
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],

		});

    }
    
});

$("#formMyleaves").on("submit", function(e){
	var r = confirm("Delete this leave?");
	if (r == true) {
	    $("#formMyleaves").submit;
	}else{
		e.preventDefault();
	}
});
    
