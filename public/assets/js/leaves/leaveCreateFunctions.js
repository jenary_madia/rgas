var base_url = $("base").attr("href");
$(document).ready(function () {
        var restDays = $('#lrfRestDay').val();

        $('#lrfRestDay').multiselect({
            onInitialized: function () {
                changeDisableDays(restDays[0],restDays[1]);
                var selectedOptions = $('#lrfRestDay option:selected');

                if (selectedOptions.length >= 2) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
            },
            onChange: function(option, checked) {
                restDays = $('#lrfRestDay').val();
                changeDisableDays(restDays[0],restDays[1]);
                var selectedOptions = $('#lrfRestDay option:selected');

                if (selectedOptions.length >= 2) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('#lrfRestDay option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
                $('#lrfDateTo').val("");
                $('#lrfDateFrom').val("");
                $('#lrfTotalLeaveDays').val(0)
                computeTotalLeave();
            },
            onDropdownShown: function () {
                var selectedOptions = $('#lrfRestDay option:selected');

                if (selectedOptions.length >= 2) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });

    $("#lrfTypeOfSchedule").on("change",function () {
        
        if ($(this).val() === 'special') {
            $('.labelRestday').addClass("required");
            $('#lrfRestDay').val([6,0]);
            $("#lrfRestDay").multiselect('enable');
            changeDisableDays(6,0);
        }else if($(this).val() === 'regular'){
            $('.labelRestday').removeClass("required");
            $('#lrfRestDay').val(0);
            $("#lrfRestDay").multiselect('disable');
            changeDisableDays(0,9);
        }else if($(this).val() === 'compressed'){
            $('.labelRestday').removeClass("required");
            $('#lrfRestDay').val([6,0]);
            $("#lrfRestDay").multiselect('disable');
            changeDisableDays(6,0);
        }

        $('#lrfDateFrom').val("");
        $('#lrfDateTo').val("");

        $("#lrfRestDay").multiselect("refresh");
        computeTotalLeave();
    });

    $("#lrfDateTo").on("change",function () {
        var startDate = new Date($('#lrfDateFrom').val());
        var endDate = new Date($('#lrfDateTo').val());
        if (startDate >= endDate){
            if ($('#lrfDurationLeave').val() == 'multiple') {
                alert("Please check your leave dates!");
            }
            $('#lrfDateTo').val("");
        }
    });

    $('body').on('click','#home-visit-btn-error',function () {
        $("#homeVisitError").modal("hide");
        $('.LRFAdditionalFields').fadeOut();
        $('#divLeaveDetails').fadeOut();
        $('.titleLeaveType').fadeIn();
    });

});
$(function() {

	$('.date_picker').datepicker({
		dateFormat : 'yy-mm-dd',
		beforeShowDay : function(date) {
		    var day = date.getDay();
		    return [(day != 6 && day != 0)];
		}
	})
	.on('change',function(){
        computeTotalLeave();
	});



	$('#lrfDateFrom').on('change',function(){
        var startDate = new Date($('#lrfDateFrom').val());
        var endDate = new Date($('#lrfDateTo').val());
        if (startDate >= endDate){
            if ($('#lrfDurationLeave').val() == 'multiple') {
                alert("Please check your leave dates!");
            }
            $('#lrfDateFrom').val("");
        }
	});
	

	$('.LRFLeaveType').on('change',function(){
        if ($(this).val() == "HomeVisit") {
            $.ajax({
                url: base_url+'/lrf/validate_leave',
                method: "POST",
                dataType: "JSON",
            }).done(function(data) {
                if(! data) {
                    $("#homeVisitError").modal("show");
                }else{
                    $('.LRFAdditionalFields').fadeOut();
                    changeDisableDays(8,9);
                }
            });

		}else{
		    if($(this).val() != "") {
                $('.LRFAdditionalFields').fadeIn();
            }else{
                $('.LRFAdditionalFields').fadeOut();
            }
		}
        if($(this).val() != "") {
            $('.titleLeaveType').fadeOut();
            $('#divLeaveDetails').fadeIn();
        }else{
            $('.titleLeaveType').fadeIn();
            $('#divLeaveDetails').fadeOut();
        }

        computeTotalLeave();

	});

});

function ChooseDurationOfLeave(input)
{
	$('#lrfDateFrom').removeAttr("disabled");

	if (input.value == 'whole') {
		$('#lrfTotalLeaveDays').val(1);
        checkIfNeedMedCert(1);
		$('.divTo').fadeOut();
		$('#lrfDateTo').attr('disabled',true);
		$('#lrfLeavePeriodFrom').val("whole");
		$('#lrfLeavePeriodFrom').attr("disabled",true);
	}else if (input.value == 'half') {
		$('#lrfTotalLeaveDays').val(0.5);
        checkIfNeedMedCert(0.5);
		$('.divTo').fadeOut();
		$('#lrfDateTo').attr('disabled',true);
		$('#lrfLeavePeriodFrom').val("half");
		$('#lrfLeavePeriodFrom').attr("disabled",true);
	}else if (input.value == 'multiple'){
		$('.divTo').fadeIn();
		$('#lrfLeavePeriodFrom').removeAttr("disabled");
		$('#lrfDateTo').removeAttr("disabled");
        computeTotalLeave();

    }
}

function computeTotalLeave() {
	if ($('#lrfDateFrom').val()) {
        var leavePeriodFrom = $('#lrfLeavePeriodFrom').val();
        var leavePeriodTo = $('#lrfLeavePeriodTo').val();
        var counter = 1;
		if ($('#lrfDateTo').val())
		{
	    	var startDate = new Date($('#lrfDateFrom').val());
			var endDate = new Date($('#lrfDateTo').val());
			var restDays = $('#lrfRestDay').val();
            if($(".LRFLeaveType").val() == "HomeVisit") {
                restDays = ['8','9'];
            }
            if (! restDays) {
                alert("Rest days are required");
            }else{
                var dateFrom = $("#lrfDateFrom").datepicker().val();
                var a = $("#lrfDateFrom").datepicker('getDate').getTime(),
                    b = $("#lrfDateTo").datepicker('getDate').getTime(),
                    c = 24*60*60*1000,
                    diffDays = Math.round(Math.abs((a - b)/(c)));

                for (var i = 0; i < diffDays; i++) {
                    startDate.setDate(startDate.getDate() + 1); //number  of days to add, e.x. 15 days
                    var dateFormated = startDate.toISOString().substr(0,10);
                    var finalDate = new Date(dateFormated);
                    var day = finalDate.getDay();
                    var checkIfrestDay = restDays.indexOf(day.toString());
                    if (checkIfrestDay == -1)
                    {
                        counter += 1;
                    }
                    startDate = new Date(dateFormated);

                };

                if (leavePeriodFrom == 'half') {
                    counter -= .5;
                }
                if (leavePeriodTo == 'half') {
                    counter -= .5;
                }

                $('#lrfTotalLeaveDays').val(counter);
                // console.log($.inArray($(".LRFLeaveType").val(),["Mat\/Pat","Sick"]));

                checkIfNeedMedCert(counter);
            }

		}else{
            if (leavePeriodFrom == 'half') {
                counter -= .5;
            }
            $('#lrfTotalLeaveDays').val(counter);
        }

	}
}

function checkIfNeedMedCert(counter) {
    if ( $.inArray(company,JSON.parse(corporateCompanies))  === -1 && counter > 3 && $.inArray($(".LRFLeaveType").val(),["Mat\/Pat","Sick"]) != -1) {
        $("#med_cert").show();
    }else{
        $("#med_cert").hide();
    }
}

function changeDisableDays(day1,day2)
{
	$('#lrfDateFrom').datepicker('option', 'beforeShowDay', function(date) {
	    var day = date.getDay();
	    return [(day != day1 && day != day2)];
	});

	$('#lrfDateTo').datepicker('option', 'beforeShowDay', function(date) {
	    var day = date.getDay();
	    return [(day != day1 && day != day2)];
	});
}

$('#btnSend').on('click',function(e){
	$('#lrfLeavePeriodFrom').removeAttr('disabled');
	$('#lrfRestDay').removeAttr('disabled');
	$('#lrfLeavePeriodFrom').attr('readonly',true);
    $('#lrfTotalLeaveDays').removeAttr('disabled');
});