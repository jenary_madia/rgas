var base_url = $("base").attr("href");
new Vue({
    el : "#peISView",
    data : {
        departmentID : "",
        sectionID : "",
        sections  : [],
        employees : [],
        employeeIndex : "",
        toSend : [],
        toAdd : {
            employeeName : "",
            employeeNumber : "",
            notificationType : "",
            date : "",
            timeIn : "",
            timeOut : "",
            reason : "",
            employeeID : "",
            active : false,
        },
        toEdit : {},
        employeesToFile : [],
        employeesToFileOld : "",
        editable : false
    },
    methods : {
        saveEdit : function (id,employee_number,batch_id) {
            this.employeesToFile[id]["noted"] = this.toEdit.noted;
            $('#EditLeaveDetails').modal('hide');
            Vue.http.post(base_url+'/lrf/note_batchdetails',{"note": this.employeesToFile[id]["noted"],"employeeNumber" : employee_number,"batchID" : batch_id }).then(function (response) {
                    console.log(response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
        },

        chooseEmployee : function (reference,index) {
            for (var i = 0; i < this.employeesToFile.length; i++) {
                if(index != i) {
                    this.employeesToFile[i]['active'] = false;
                }else{
                    this.employeesToFile[i]['active'] = true;
                }
            }

            this.$set("toEdit", JSON.parse(JSON.stringify(reference)));
            this.$set("toEdit.id",index);
            this.$set("editable",true);

        },

        getTotalDays : function () {
            var duration = this.toAdd.duration;
            var startDate = new Date(this.toAdd.fromDate);
            var endDate = new Date(this.toAdd.toDate);
            if (duration == "multiple"){
                if(startDate == 'Invalid Date'|| endDate == 'Invalid Date') {
                    this.$set("toAdd.totalDays", 0);
                }
                var total = 0;
                if ((startDate >= endDate)) {
                    this.$set("toAdd.totalDays", total);
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        total = Math.round(Math.abs((a - b)/(c)));
                }
                this.$set("toAdd.totalDays",total + 1);
            }
        },
        parseToAddData : function () {
            var chosenEmployee = this.employees[this.employeeIndex];
            var fullname = chosenEmployee['firstname']+' '+chosenEmployee['middlename']+' '+chosenEmployee['lastname']
            this.$set('toAdd.employeeNumber',chosenEmployee['employeeid']);
            this.$set('toAdd.employeeName',fullname);
            this.$set('toAdd.employeeID',chosenEmployee['id']);
        },

        getToAddData : function () {
            var that = this;
            Vue.http.post(base_url+'/lrf/get_employees',this.toAdd).then(function (response) {
                    that.employeesToFile.push(response.data);
                    $('#AddLeaveDetails').modal('hide');
                },
                function () {
                    console.log("Something went wrong");
                });
        },
        clear : function () {
            this.$set('toAdd.employeeName',"");
            this.$set('toAdd.employeeNumber',"");
            this.$set('toAdd.leaveType',"");
            this.$set('toAdd.fromDate',"");
            this.$set('toAdd.fromPeriod',"");
            this.$set('toAdd.toDate',"");
            this.$set('toAdd.toPeriod',"");
            this.$set('toAdd.reason',"");
            this.$set('toAdd.employeeID',"");
            this.$set('toAdd.totalDays',"");
            this.$set('toAdd.duration',"");
            this.$set("employeeIndex","");;
        }
    },
    watch : {
        "departmentID" : function () {
            var that = this;
            Vue.http.post(base_url+'/ns/sections',{ "dept_id" : this.departmentID }).then(function (response) {
                    that.$set("sections",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });

            Vue.http.post(base_url+'/ns/employees',{"dept_id" : this.departmentID }).then(function (response) {
                    that.$set("employees",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
        },
        "sectionID" : function () {
            var that = this;
            Vue.http.post(base_url+'/ns/employees',{"dept_id" : this.departmentID ,"sect_id" : this.sectionID }).then(function (response) {
                    that.$set("employees",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
        },
        "toAdd.duration" : function() {
            var duration = this.toAdd.duration;
            this.$set("toAdd.toDate","");
            this.$set("toAdd.toPeriod","");
            if(duration == 'half') {
                this.$set("toAdd.totalDays", 0.5);
                this.$set("toAdd.fromPeriod", "half");
            }else if(duration == 'whole') {
                this.$set("toAdd.totalDays", 1);
                this.$set("toAdd.fromPeriod", "whole");
            }else{
                $('.date_picker').datepicker({
                    dateFormat : 'yy-mm-dd'
                });
            }
        },
        "toAdd.fromDate" : function () {
            this.getTotalDays();
        },
        "toAdd.toDate" : function () {
            this.getTotalDays();
        },
        "toAdd.fromPeriod" : function () {
            if (this.toAdd.duration == "multiple") {
                if(this.toAdd.fromPeriod == "half") {
                    this.$set("toAdd.totalDays",this.toAdd.totalDays - .5);
                }else{
                    if(this.toAdd.toPeriod == "half") {
                        this.$set("toAdd.totalDays",this.toAdd.totalDays + .5);
                    }else{
                        this.getTotalDays();
                    }
                }
            }
        },
        "toAdd.toPeriod" : function () {
            if (this.toAdd.duration == "multiple") {
                if(this.toAdd.toPeriod == "half") {
                    this.$set("toAdd.totalDays",this.toAdd.totalDays - .5);
                }else{
                    if(this.toAdd.fromPeriod == "half") {
                        this.$set("toAdd.totalDays",this.toAdd.totalDays + .5);
                    }else{
                        this.getTotalDays();
                    }
                }
            }
        },
        "employeesToFile" : function () {
            if(this.employeesToFile.length == 0) {
                this.$set("editable",false);
            }
        }
    },

    ready : function () {
        var that = this;
        if(this.employeesToFileOld)  {
            this.$set("employeesToFile",JSON.parse(this.employeesToFileOld));
            for (var i = 0; i < this.employeesToFile.length; i++) {
                this.employeesToFile[i]['active'] = false;
            }
        }else{
            Vue.http.get(window.location.href, {
                headers: {
                    'Cache-Control': 'no-cache'
                }
            }).then(function (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        that.employeesToFile.push(response.data[i]);
                    }
                },
                function () {
                    console.log("Something went wrong");
                });
        }
    },
})