$(document).ready(function(){
	var base_url = $("base").attr("href");
    if ( $("#clinicApprovalLeaves").length != 0 ) {
        var requests_my_table = $('#clinicApprovalLeaves').dataTable( {
				"bDestroy": true,
			  "sPaginationType": "full_numbers",
			  "sAjaxSource": base_url + "/lrf/clinic_approval_leaves",
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
				"sSearch" : "Search :"
			  },
			  "fnDrawCallback": function( oSettings ) {

			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, -1],
				[10, 20, 50, 100, "All"]
				], 
				"aoColumns": [
					{ "sWidth": "15%","bSortable" : true},
					{ "sWidth": "15%","bSortable" : true},
					{ "sWidth": "15%","bSortable" : false},
					{ "bSortable" : false},
					{ "bSortable" : false},
					{ "sWidth": "5%","bSortable" : false},
					{ "sWidth": "15%","bSortable" : false},
					{ "sWidth": "18%","bSortable" : false},
				    
				]

		});
        
    }
    
});

