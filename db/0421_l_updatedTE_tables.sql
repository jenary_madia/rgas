-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4170
-- Date/time:                    2016-04-21 21:44:32
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `rgasbl`;


-- Dumping structure for table rgasbl.trainingendorsements
DROP TABLE IF EXISTS `trainingendorsements`;
CREATE TABLE IF NOT EXISTS `trainingendorsements` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_employees_id` int(11) NOT NULL DEFAULT '0',
  `te_emp_name` varchar(50) NOT NULL,
  `te_emp_id` varchar(20) NOT NULL,
  `te_ref_num` varchar(50) NOT NULL COMMENT 'random chars',
  `te_status` varchar(50) NOT NULL COMMENT '1=New,2=For Assessment,3=For Approval,4=Approved,5=Disapproved',
  `te_issent` tinyint(4) DEFAULT NULL,
  `te_total_amount` int(11) NOT NULL,
  `te_current` varchar(100) DEFAULT NULL,
  `te_level` varchar(50) DEFAULT NULL COMMENT 'CHRD-TOD,VPHR,SVPCS,PRES',
  `te_flag` tinyint(4) DEFAULT NULL COMMENT 'flagging of workflow: 0-start,1-continue, 2-stop',
  `te_company` varchar(50) NOT NULL,
  `te_department` varchar(50) NOT NULL,
  `te_emp_designation` varchar(100) NOT NULL,
  `te_date_filed` date NOT NULL COMMENT 'current date',
  `te_training_title` varchar(200) NOT NULL,
  `te_contact_no` varchar(50) DEFAULT NULL,
  `te_training_date` date NOT NULL,
  `te_amount_range` varchar(100) NOT NULL COMMENT '1-5000, 5001-50000, 50001 and above',
  `te_vendor` varchar(200) NOT NULL,
  `te_training_venue` varchar(200) NOT NULL,
  `te_echo_training_date` date NOT NULL,
  `te_attachments` varchar(255) DEFAULT NULL COMMENT 'Filename in JSON Array',
  `te_comments` text,
  `teq_target_performance` text,
  `teq_target_competency` text,
  `teq_newfunction_description` text,
  `teq_reg_emp` varchar(50) DEFAULT NULL,
  `teq_submitted_req` varchar(50) NOT NULL,
  `teq_new_seminar` varchar(50) DEFAULT NULL,
  `teq_idp` varchar(50) DEFAULT NULL,
  `teq_tga` varchar(50) DEFAULT NULL,
  `teq_target_2_higher` varchar(50) DEFAULT NULL,
  `teq_result_1_higher` varchar(50) DEFAULT NULL,
  `teq_required` varchar(50) DEFAULT NULL,
  `teq_overall_recom` varchar(50) DEFAULT NULL,
  `teq_eta` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`te_id`),
  KEY `te_emp_id` (`te_emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rgasbl.trainingendorsements: ~0 rows (approximately)
/*!40000 ALTER TABLE `trainingendorsements` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainingendorsements` ENABLE KEYS */;


-- Dumping structure for table rgasbl.trainingendorsementsparticipants
DROP TABLE IF EXISTS `trainingendorsementsparticipants`;
CREATE TABLE IF NOT EXISTS `trainingendorsementsparticipants` (
  `tep_id` int(11) NOT NULL AUTO_INCREMENT,
  `tep_te_id` varchar(50) DEFAULT '0',
  `tep_ref_num` varchar(100) DEFAULT NULL,
  `tep_emp_name` varchar(50) NOT NULL,
  `tep_dep_name` varchar(50) NOT NULL,
  `tep_designation` varchar(50) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`tep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rgasbl.trainingendorsementsparticipants: ~0 rows (approximately)
/*!40000 ALTER TABLE `trainingendorsementsparticipants` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainingendorsementsparticipants` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
