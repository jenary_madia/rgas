-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for rgasbl
DROP DATABASE IF EXISTS `rgasbl`;
CREATE DATABASE IF NOT EXISTS `rgasbl` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rgasbl`;


-- Dumping structure for table rgasbl.arts
DROP TABLE IF EXISTS `arts`;
CREATE TABLE IF NOT EXISTS `arts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(50) NOT NULL,
  `requestedby` varchar(100) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `daterequested` date NOT NULL,
  `endorsedby` varchar(100) DEFAULT NULL,
  `dateendorsed` date DEFAULT NULL,
  `company` varchar(50) NOT NULL COMMENT 'rbc, sfi, pfi, jbc, mfc, others',
  `duedate` date DEFAULT NULL,
  `contactno` varchar(15) DEFAULT NULL,
  `requestfor` varchar(20) NOT NULL COMMENT 'design, mockup, actualproduct',
  `wrapper` int(11) DEFAULT NULL,
  `carton` int(11) DEFAULT NULL,
  `barcodebag` int(11) DEFAULT NULL,
  `gondola` int(11) DEFAULT NULL,
  `banners` int(11) DEFAULT NULL,
  `header` int(11) DEFAULT NULL,
  `othermaterials` varchar(100) DEFAULT NULL,
  `productname` varchar(50) DEFAULT NULL,
  `netweight` varchar(20) DEFAULT NULL,
  `length` varchar(15) DEFAULT NULL,
  `width` varchar(15) DEFAULT NULL,
  `height` varchar(15) DEFAULT NULL,
  `ingredients` int(11) DEFAULT NULL,
  `nutritionfacts` int(11) DEFAULT NULL,
  `barcode` int(11) DEFAULT NULL,
  `otherspecifications` text,
  `briefdescription` text,
  `note` text,
  `duedatestatus` varchar(10) DEFAULT NULL COMMENT 'onschedule, onhold, delayed',
  `reason` text,
  `status` varchar(20) NOT NULL,
  `ownerid` int(11) NOT NULL,
  `attachpb` text,
  `attach1` text,
  `attach2` text,
  `attach3` text,
  `attach4` text,
  `attach5` text,
  `comment` text,
  `datecreated` date NOT NULL,
  `curr_emp` int(11) NOT NULL,
  `assignto` int(11) DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `createdarts` (`ownerid`,`status`),
  KEY `forapprovalarts` (`curr_emp`,`status`),
  KEY `assignedarts` (`curr_emp`,`assignto`,`status`),
  KEY `completedarts` (`curr_emp`,`assignto`,`status`,`isdeleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.assestments
DROP TABLE IF EXISTS `assestments`;
CREATE TABLE IF NOT EXISTS `assestments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(150) NOT NULL,
  `act_desc` text NOT NULL,
  `req` varchar(50) NOT NULL,
  `specificday` int(11) NOT NULL,
  `no_of_outputs` int(11) NOT NULL,
  `time_per_output` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `manpowerassestmentid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manpowerassestmentid` (`manpowerassestmentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_code` varchar(10) NOT NULL,
  `auth_name` varchar(20) NOT NULL,
  `auth_desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.cake_sessions
DROP TABLE IF EXISTS `cake_sessions`;
CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.candidates
DROP TABLE IF EXISTS `candidates`;
CREATE TABLE IF NOT EXISTS `candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manpowerrequestid` int(11) NOT NULL,
  `candidate` varchar(50) DEFAULT NULL,
  `dateendorsed` date DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`id`),
  KEY `manpowerrequestid` (`manpowerrequestid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.clearancecertification
DROP TABLE IF EXISTS `clearancecertification`;
CREATE TABLE IF NOT EXISTS `clearancecertification` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_emp_name` varchar(50) NOT NULL,
  `cc_emp_id` int(11) NOT NULL,
  `cc_div_id` int(11) NOT NULL COMMENT 'tmp',
  `cc_emp_departmentid` int(11) DEFAULT NULL,
  `cc_emp_sectionid` int(11) NOT NULL,
  `cc_positionid` varchar(50) NOT NULL,
  `cc_division_head` varchar(50) DEFAULT NULL COMMENT 'tmp',
  `cc_dept_head` int(11) DEFAULT NULL,
  `cc_immediate_head` int(11) DEFAULT NULL COMMENT 'tmp',
  `cc_date_hired` date DEFAULT NULL COMMENT 'tmp',
  `cc_resignation_date` date DEFAULT NULL,
  `cc_ref_num` varchar(50) DEFAULT NULL,
  `cc_attachments` varchar(255) DEFAULT NULL COMMENT 'JSON Array',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.clearancecertificationcomments
DROP TABLE IF EXISTS `clearancecertificationcomments`;
CREATE TABLE IF NOT EXISTS `clearancecertificationcomments` (
  `ccc_id` int(11) NOT NULL AUTO_INCREMENT,
  `ccc_cc_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`ccc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.clearancecertificationdepartments
DROP TABLE IF EXISTS `clearancecertificationdepartments`;
CREATE TABLE IF NOT EXISTS `clearancecertificationdepartments` (
  `ccd_id` int(11) NOT NULL AUTO_INCREMENT,
  `ccd_cc_id` int(11) NOT NULL,
  `ccd_dep_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`ccd_id`),
  KEY `FK_clearancecertificationdepartments_clearancecertification` (`ccd_cc_id`),
  CONSTRAINT `FK_clearancecertificationdepartments_clearancecertification` FOREIGN KEY (`ccd_cc_id`) REFERENCES `clearancecertification` (`cc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL,
  `comp_code` varchar(5) NOT NULL,
  `comp_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `comp_seq` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `comp_telno` varchar(50) DEFAULT NULL,
  `comp_tin` varchar(12) DEFAULT NULL,
  `dmg_code` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `comp_code` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.companylists
DROP TABLE IF EXISTS `companylists`;
CREATE TABLE IF NOT EXISTS `companylists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(10) NOT NULL,
  `comp_name` varchar(50) NOT NULL,
  `affiliate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: RGFC; 1: Ng Affiliate;',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_1` (`comp_code`,`comp_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.companyplants
DROP TABLE IF EXISTS `companyplants`;
CREATE TABLE IF NOT EXISTS `companyplants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plantcode` varchar(4) NOT NULL,
  `company` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company` (`company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='company relation to plants';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.compensationbenefits
DROP TABLE IF EXISTS `compensationbenefits`;
CREATE TABLE IF NOT EXISTS `compensationbenefits` (
  `cb_id` int(11) NOT NULL AUTO_INCREMENT,
  `cb_employees_id` int(11) NOT NULL DEFAULT '0',
  `cb_emp_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_emp_id` varchar(20) NOT NULL,
  `cb_company` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_department` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_section` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_ref_num` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'random chars',
  `cb_date_filed` date NOT NULL COMMENT 'current date',
  `cb_status` varchar(50) NOT NULL,
  `cb_contact_no` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `cb_urgency` varchar(50) NOT NULL,
  `cb_date_needed` date DEFAULT NULL,
  `cb_purpose_of_request` text CHARACTER SET latin1 NOT NULL,
  `cb_attachments` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Filename in JSON Array',
  `cb_comments` text CHARACTER SET latin1,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`cb_id`),
  KEY `cb_emp_id` (`cb_emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.compensationbenefitsrequesteddocs
DROP TABLE IF EXISTS `compensationbenefitsrequesteddocs`;
CREATE TABLE IF NOT EXISTS `compensationbenefitsrequesteddocs` (
  `cbrd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cbrd_cb_id` varchar(50) DEFAULT '0',
  `cbrd_req_docs` varchar(100) DEFAULT NULL,
  `cbrd_ref_num` varchar(100) DEFAULT NULL,
  `cbrd_assigned_to` varchar(100) DEFAULT '0',
  `cbrd_assigned_date` date DEFAULT NULL,
  `cbrd_current` varchar(100) DEFAULT NULL COMMENT 'Name of the employee who currently has the access in updating details of the request',
  `cbrd_status` varchar(100) DEFAULT NULL,
  `cbrd_processed_date` date DEFAULT NULL,
  `cbrd_endorsed_to_requestor_date` date DEFAULT NULL,
  PRIMARY KEY (`cbrd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.consorequisitions
DROP TABLE IF EXISTS `consorequisitions`;
CREATE TABLE IF NOT EXISTS `consorequisitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consorefno` varchar(10) NOT NULL,
  `requestdate` date NOT NULL,
  `deliverydate` date NOT NULL,
  `plantcode` varchar(4) NOT NULL,
  `materialcode` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(5) NOT NULL,
  `purchasinggroup` varchar(3) NOT NULL,
  `materialtype` varchar(4) NOT NULL,
  `accountassignment` varchar(3) NOT NULL,
  `isprocessed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `consorefno` (`consorefno`),
  KEY `isprocessed` (`isprocessed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.departments
DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_code` varchar(20) NOT NULL COMMENT 'Please make this code stable or not changing because some codes refer to this as primary key instead of id in searching employees base on the department they belong',
  `dept_name` varchar(50) NOT NULL,
  `dept_desc` text,
  `dept_type` varchar(10) NOT NULL DEFAULT 'admin' COMMENT 'admin or plant',
  `dept_head` int(11) DEFAULT NULL,
  `comp_code` varchar(10) NOT NULL,
  `numbercode` varchar(2) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `dept_code` (`dept_code`),
  KEY `comp_code` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.depts
DROP TABLE IF EXISTS `depts`;
CREATE TABLE IF NOT EXISTS `depts` (
  `dept_id` varchar(15) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.documentcodes
DROP TABLE IF EXISTS `documentcodes`;
CREATE TABLE IF NOT EXISTS `documentcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(30) NOT NULL,
  `code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.documents
DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `employee_no` varchar(15) NOT NULL,
  `current_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `completed_date` date DEFAULT NULL,
  `document_name` varchar(200) NOT NULL,
  `document_type_id` tinyint(4) NOT NULL,
  `other_document_type` varchar(50) NOT NULL,
  `policy` char(1) DEFAULT NULL,
  `confidential` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: non-confidential; 1: confidential;',
  `checker_id` int(11) NOT NULL DEFAULT '0' COMMENT 'id of whom the document was forwarded for checking',
  `comments` text NOT NULL,
  `comments_ea` text NOT NULL,
  `ts_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ts_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` tinyint(4) NOT NULL COMMENT '1: New, 2: For Review, 3: For Acknowledgement, 4: For Approval, 5: Approved, 6: Disapproved, 7: Published, 8: Pending at PRES, 9: For Clarification or Revision',
  `hidden` tinyint(4) NOT NULL COMMENT '0: visible; 1: hidden;',
  `batchemail` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'for batch emailing; 0: default; 1: for batchemail; 2:emailed;',
  `itr_no` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `reference_no` (`reference_no`),
  KEY `for_approval_list` (`current_id`,`created_by`,`status_id`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.document_attachments
DROP TABLE IF EXISTS `document_attachments`;
CREATE TABLE IF NOT EXISTS `document_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: document, 2: attachment',
  `fn` varchar(150) NOT NULL,
  `attached_by` int(11) NOT NULL,
  `attached_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.document_dept_lists
DROP TABLE IF EXISTS `document_dept_lists`;
CREATE TABLE IF NOT EXISTS `document_dept_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_hrms_code` varchar(15) NOT NULL,
  `department_id` int(15) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT '333: Liza Dabu; 334: Rachelle Ong;',
  `other_ea` varchar(50) NOT NULL DEFAULT '0' COMMENT 'For Office of the VPO 333: Liza Dabu; 334: Rachelle Ong;',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.document_signatories
DROP TABLE IF EXISTS `document_signatories`;
CREATE TABLE IF NOT EXISTS `document_signatories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) NOT NULL,
  `signature_type` tinyint(4) NOT NULL COMMENT '1: prepared; 2: reviewed; 3: acknowledged; 4: approved;',
  `signature_type_seq` tinyint(4) NOT NULL COMMENT 'prepared, reviewed, acknowledged, approved',
  `employee_id` int(11) NOT NULL,
  `approval_date` date DEFAULT NULL,
  `signed` char(1) NOT NULL DEFAULT '0',
  `notify_id` int(11) DEFAULT NULL COMMENT 'id of whom to notify',
  `notify_view` tinyint(4) DEFAULT NULL COMMENT '0: prevent viewing of document; 1: allow viewing of document;',
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `document_id` (`document_id`),
  KEY `notify_id` (`notify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.document_types
DROP TABLE IF EXISTS `document_types`;
CREATE TABLE IF NOT EXISTS `document_types` (
  `id` tinyint(4) NOT NULL,
  `document_type_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` varchar(20) NOT NULL,
  `new_employeeid` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `desig_level` varchar(30) NOT NULL COMMENT 'employee, supervisor, head, top mngt',
  `heirarchy` varchar(30) NOT NULL DEFAULT 'employee' COMMENT 'employee, manager, top management',
  `superiorid` int(11) DEFAULT NULL,
  `departmentid2` varchar(15) NOT NULL COMMENT 'From HRMS(depts) -  used in PMF',
  `departmentid` int(11) NOT NULL COMMENT 'For RGAS forms use',
  `departmentid3` smallint(5) unsigned DEFAULT NULL COMMENT 'Department in RGTS',
  `departmentid4` smallint(5) unsigned DEFAULT NULL COMMENT 'Department for RGTS-Incidents',
  `sectionid` int(11) DEFAULT NULL,
  `rgts_sectionid` int(11) DEFAULT NULL COMMENT 'Section ID for RGTS',
  `company` varchar(15) NOT NULL,
  `onleave` int(11) NOT NULL DEFAULT '0' COMMENT '1 or 0',
  `attendanceid` int(11) NOT NULL DEFAULT '0',
  `mainattendanceid` int(11) NOT NULL DEFAULT '0',
  `artapprovalid` int(11) NOT NULL DEFAULT '0',
  `mainartapprovalid` int(11) NOT NULL DEFAULT '0',
  `msrapprovalid` int(11) NOT NULL DEFAULT '0',
  `mainmsrapprovalid` int(11) NOT NULL DEFAULT '0',
  `presassignedrsmrf1` int(11) NOT NULL DEFAULT '0',
  `presassignedrsmrf2` int(11) NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `active` (`active`),
  KEY `desig_level` (`desig_level`),
  KEY `msrapprovalid` (`msrapprovalid`),
  KEY `index_1` (`departmentid`,`desig_level`,`active`),
  KEY `index_2` (`company`,`desig_level`,`active`),
  KEY `index_3` (`onleave`,`desig_level`,`active`),
  KEY `index_4` (`sectionid`,`active`),
  KEY `company` (`company`,`username`,`active`),
  KEY `employeeid` (`employeeid`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`departmentid`) REFERENCES `departments` (`id`),
  CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`company`) REFERENCES `companylists` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.hierarchies
DROP TABLE IF EXISTS `hierarchies`;
CREATE TABLE IF NOT EXISTS `hierarchies` (
  `employee_id` int(11) unsigned NOT NULL,
  `superior_id` int(10) unsigned NOT NULL DEFAULT '0',
  `department_id` int(11) unsigned NOT NULL,
  `department_hrms_cd` varchar(20) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT 'Name of Employee',
  `superior` varchar(65) DEFAULT NULL COMMENT 'Name of Superior',
  `designation` varchar(15) DEFAULT 'Employee' COMMENT 'Designation of Superior',
  KEY `employee_id` (`employee_id`),
  KEY `superior_id` (`superior_id`),
  KEY `department_id` (`department_id`),
  KEY `department_hrms_cd` (`department_hrms_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='RGAS hierarchies';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.leaves
DROP TABLE IF EXISTS `leaves`;
CREATE TABLE IF NOT EXISTS `leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documentcode` varchar(20) NOT NULL,
  `codenumber` varchar(5) NOT NULL,
  `employeeid` varchar(20) DEFAULT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `company` varchar(10) NOT NULL,
  `date` date DEFAULT NULL,
  `dept_sect` varchar(50) NOT NULL,
  `appliedfor` varchar(10) NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `noofdays` decimal(4,2) NOT NULL,
  `reason` text NOT NULL,
  `emp_sign` varchar(100) DEFAULT NULL,
  `clinic` varchar(100) DEFAULT NULL,
  `sectionhead` varchar(100) DEFAULT NULL,
  `supervisor` varchar(100) DEFAULT NULL,
  `dept_mngr` varchar(100) DEFAULT NULL,
  `plant_mngr` varchar(100) DEFAULT NULL,
  `svp` varchar(90) DEFAULT NULL,
  `evp_pres` varchar(100) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'new' COMMENT 'new, for approval, approved, disapproved, approved/deleted, disapproved/deleted, canceled, deleted, cancelled, cancelled/deleted',
  `ownerid` int(11) NOT NULL,
  `dateapproved` date DEFAULT NULL,
  `attach1` text,
  `attach2` text,
  `attach3` text,
  `attach4` text,
  `attach5` text,
  `comment` text,
  `datecreated` date NOT NULL,
  `curr_emp` int(11) NOT NULL,
  `approvers` varchar(20) NOT NULL COMMENT 'array list of those who approved the request.',
  `isprocessed` int(11) DEFAULT '0',
  `isdeleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_2` (`curr_emp`,`status`),
  KEY `index_3` (`status`,`company`,`isprocessed`),
  KEY `index_4` (`status`,`company`,`isprocessed`,`isdeleted`),
  KEY `index_5` (`status`,`company`,`isdeleted`),
  KEY `index_6` (`ownerid`,`status`,`from`,`to`),
  KEY `index_7` (`company`,`from`,`to`,`status`),
  KEY `index_1` (`ownerid`,`status`),
  KEY `index_8` (`documentcode`,`codenumber`,`firstname`,`lastname`,`appliedfor`,`from`,`status`),
  CONSTRAINT `leaves_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companylists` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.manpowerassesments
DROP TABLE IF EXISTS `manpowerassesments`;
CREATE TABLE IF NOT EXISTS `manpowerassesments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preparedby` int(11) NOT NULL,
  `endorsedby` int(11) NOT NULL,
  `mraf_code` varchar(20) NOT NULL,
  `datefiled` date NOT NULL,
  `departmentid` int(11) NOT NULL,
  `contact` varchar(11) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `new` int(11) DEFAULT '0',
  `existing` int(11) DEFAULT '0',
  `assignedbsa` int(11) NOT NULL,
  `smddmanager` int(11) NOT NULL,
  `manpowerrequestid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manpowerrequestid` (`manpowerrequestid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.manpowerrequests
DROP TABLE IF EXISTS `manpowerrequests`;
CREATE TABLE IF NOT EXISTS `manpowerrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) NOT NULL,
  `datefiled` date NOT NULL,
  `dateneeded` date NOT NULL,
  `company` varchar(10) NOT NULL,
  `departmentid` int(11) NOT NULL,
  `sectionid` int(11) DEFAULT NULL,
  `position_jobtitle` varchar(50) NOT NULL,
  `periodcovered` varchar(50) NOT NULL,
  `newposition` int(11) DEFAULT NULL,
  `addstaff` int(11) DEFAULT NULL,
  `replacement` int(11) DEFAULT NULL,
  `internal` int(11) DEFAULT '0',
  `external` int(11) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `reason` text,
  `requestedby` int(11) NOT NULL,
  `notedby` int(11) NOT NULL,
  `conforme` int(11) NOT NULL,
  `bsaassessedby` tinyint(4) NOT NULL DEFAULT '0',
  `csmdadhassessedby` tinyint(4) NOT NULL DEFAULT '0',
  `csmddhassessedby` tinyint(4) NOT NULL DEFAULT '0',
  `vpoapprovedby` int(11) NOT NULL,
  `svpapprovedby` int(11) NOT NULL,
  `presapprovedby` int(11) NOT NULL,
  `dateapproved` date NOT NULL,
  `receivedby` int(11) DEFAULT NULL,
  `receiptdate` date DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `curr_emp` int(11) NOT NULL,
  `curr_dept` int(11) NOT NULL,
  `comment` text NOT NULL,
  `mraffile` text,
  `hiddenmraffile` text COMMENT 'hidden mraffile',
  PRIMARY KEY (`id`),
  KEY `index_1` (`employeeid`,`status`),
  KEY `index_2` (`curr_emp`,`status`),
  KEY `index_3` (`datefiled`,`status`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.manpowerservices
DROP TABLE IF EXISTS `manpowerservices`;
CREATE TABLE IF NOT EXISTS `manpowerservices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) NOT NULL,
  `departmentid` int(11) NOT NULL,
  `requestedby` int(11) DEFAULT NULL,
  `endorsedby` varchar(50) DEFAULT NULL,
  `sectionid` int(11) DEFAULT NULL,
  `datefiled` date DEFAULT NULL,
  `internal` int(11) DEFAULT NULL,
  `external` int(11) DEFAULT NULL,
  `rush` int(11) DEFAULT NULL,
  `normalprio` int(11) DEFAULT NULL,
  `pooling` int(11) DEFAULT NULL,
  `screening` int(11) DEFAULT NULL,
  `purrequestno` varchar(50) DEFAULT NULL,
  `purreceivedby` varchar(50) DEFAULT NULL,
  `purreceiptdate` date DEFAULT NULL,
  `purnotedby` varchar(50) DEFAULT NULL,
  `projectname` varchar(50) DEFAULT NULL,
  `objective` varchar(50) DEFAULT NULL,
  `panels` text,
  `panelsize` varchar(50) DEFAULT NULL,
  `reason` varchar(50) DEFAULT NULL,
  `initiatedby` varchar(50) DEFAULT NULL,
  `screeningdate` date DEFAULT NULL,
  `briefingdate` date DEFAULT NULL,
  `workplanrate` varchar(50) DEFAULT NULL,
  `otherinfo` varchar(50) DEFAULT NULL,
  `contactno` varchar(50) DEFAULT NULL,
  `endorseddate` date DEFAULT NULL,
  `curr_emp` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `attach1` varchar(50) DEFAULT NULL,
  `comment` varchar(50) DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `purrequestno` (`purrequestno`),
  KEY `index_1` (`status`,`curr_emp`,`isdeleted`,`departmentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.manpowerservicesagencies
DROP TABLE IF EXISTS `manpowerservicesagencies`;
CREATE TABLE IF NOT EXISTS `manpowerservicesagencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manpowerserviceid` int(11) NOT NULL,
  `agency` varchar(200) NOT NULL,
  `agency_info` varchar(200) NOT NULL,
  `remarks` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manpowerserviceid` (`manpowerserviceid`),
  CONSTRAINT `manpowerservicesagencies_ibfk_1` FOREIGN KEY (`manpowerserviceid`) REFERENCES `manpowerservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.materials
DROP TABLE IF EXISTS `materials`;
CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materialcode` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `plantcode` varchar(4) NOT NULL,
  `unit` varchar(5) NOT NULL,
  `purchasinggroup` varchar(3) NOT NULL,
  `materialtype` varchar(4) NOT NULL,
  `modified_by` varchar(30) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `description` (`description`),
  KEY `materialcode` (`materialcode`),
  KEY `plantcode` (`plantcode`),
  KEY `materialfilterlists` (`description`,`plantcode`,`purchasinggroup`),
  KEY `purchasinggroup` (`purchasinggroup`),
  KEY `index_2` (`materialcode`,`plantcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='RS material build-up';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.moc_attachments
DROP TABLE IF EXISTS `moc_attachments`;
CREATE TABLE IF NOT EXISTS `moc_attachments` (
  `attachment_id` int(10) NOT NULL AUTO_INCREMENT,
  `attachment_type` char(1) CHARACTER SET latin1 NOT NULL COMMENT '0 - background information, 1-SMDD assessment attachment',
  `request_id` int(10) NOT NULL,
  `fn` varchar(250) DEFAULT NULL,
  `employeeid` int(10) NOT NULL,
  `ts_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`attachment_id`),
  KEY `request_id` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.moc_changeincodes
DROP TABLE IF EXISTS `moc_changeincodes`;
CREATE TABLE IF NOT EXISTS `moc_changeincodes` (
  `request_id` int(10) NOT NULL,
  `changein_code` varchar(50) CHARACTER SET latin1 NOT NULL,
  `changein_desc` varchar(50) CHARACTER SET latin1 NOT NULL,
  `ts_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.moc_comments
DROP TABLE IF EXISTS `moc_comments`;
CREATE TABLE IF NOT EXISTS `moc_comments` (
  `request_id` int(10) NOT NULL,
  `employee_id` int(10) NOT NULL,
  `comment` text CHARACTER SET latin1 NOT NULL,
  `ts_comment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.moc_requestcodes
DROP TABLE IF EXISTS `moc_requestcodes`;
CREATE TABLE IF NOT EXISTS `moc_requestcodes` (
  `request_id` int(10) NOT NULL,
  `request_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `request_desc` varchar(50) CHARACTER SET latin1 NOT NULL,
  `ts_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `request_id` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.moc_requests
DROP TABLE IF EXISTS `moc_requests`;
CREATE TABLE IF NOT EXISTS `moc_requests` (
  `request_id` int(10) NOT NULL AUTO_INCREMENT,
  `transaction_code` varchar(50) CHARACTER SET latin1 NOT NULL,
  `date` date NOT NULL,
  `employeeid` varchar(20) CHARACTER SET latin1 NOT NULL,
  `firstname` varchar(30) CHARACTER SET latin1 NOT NULL,
  `middlename` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `lastname` varchar(30) CHARACTER SET latin1 NOT NULL,
  `dept_sec` varchar(100) CHARACTER SET latin1 NOT NULL,
  `company` varchar(100) CHARACTER SET latin1 NOT NULL,
  `requested_by` int(10) NOT NULL,
  `endorsed_by` int(10) DEFAULT NULL,
  `contact_no` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `urgency` varchar(50) CHARACTER SET latin1 NOT NULL,
  `change_type` varchar(20) CHARACTER SET latin1 NOT NULL,
  `temp_orig_date` date DEFAULT NULL,
  `dept_involved` text CHARACTER SET latin1 NOT NULL,
  `implementation_date` date NOT NULL,
  `reason` text CHARACTER SET latin1 NOT NULL,
  `assessment` text CHARACTER SET latin1,
  `endorsed_date` date DEFAULT NULL,
  `initial_assessment_date` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL COMMENT 'BSA will fill this',
  `close_out_date` date DEFAULT NULL,
  `received_by` int(10) DEFAULT NULL,
  `psrf_no` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `mocr_no` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `noted_by` int(10) DEFAULT NULL,
  `for_acknowledgement_date` date DEFAULT NULL,
  `acknowledge_by` int(10) DEFAULT NULL,
  `acknowledge_date` date NOT NULL,
  `assigned_to` int(10) DEFAULT NULL,
  `assigned_date` date DEFAULT NULL,
  `return1_date` date DEFAULT NULL,
  `return2_date` date DEFAULT NULL,
  `return3_date` date DEFAULT NULL,
  `curr_emp` int(10) DEFAULT NULL,
  `assessment_type` char(1) NOT NULL,
  `request_perform` int(10) DEFAULT NULL COMMENT '0 - Will not perform, 1 - Request will be perform',
  `is_processed` int(10) NOT NULL,
  `is_deleted` int(10) NOT NULL,
  `status` varchar(25) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `date` (`date`),
  KEY `dept_sec` (`dept_sec`),
  KEY `is_deleted` (`is_deleted`),
  KEY `assigned_to` (`assigned_to`),
  KEY `status` (`status`),
  KEY `employeeid` (`employeeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.notificationdetails
DROP TABLE IF EXISTS `notificationdetails`;
CREATE TABLE IF NOT EXISTS `notificationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notificationid` int(11) NOT NULL,
  `date` date NOT NULL,
  `type` varchar(15) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `total_time` decimal(10,2) NOT NULL,
  `reason` varchar(150) NOT NULL,
  `destination` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.notifications
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documentcode` varchar(20) NOT NULL,
  `codenumber` varchar(5) NOT NULL,
  `employeeid` varchar(20) DEFAULT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `company` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `dept_sect` varchar(50) NOT NULL,
  `shift` varchar(30) DEFAULT NULL,
  `noti_type` varchar(30) NOT NULL COMMENT 'undertime, offset, officialbusiness, cuttime, timekeeping',
  `duration_type` varchar(15) NOT NULL COMMENT 'halfday, oneday, multipledays',
  `schedule_type` varchar(15) NOT NULL COMMENT 'compressed, regular, special',
  `undertime_day` varchar(15) NOT NULL COMMENT 'monday - sunday',
  `schedule_time` varchar(15) NOT NULL COMMENT 'The schedule time out of the day when undertime is incurred',
  `totaldays` decimal(4,2) DEFAULT NULL,
  `totalhours` decimal(4,2) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `from_inout` varchar(30) DEFAULT NULL,
  `from_out` varchar(30) DEFAULT NULL,
  `from_absentperiod` varchar(15) DEFAULT '' COMMENT 'Period when the employee is absent',
  `from_absenthours` decimal(3,1) DEFAULT '0.0' COMMENT 'Number of hours absent',
  `to_date` date DEFAULT NULL,
  `to_inout` varchar(30) DEFAULT NULL,
  `to_out` varchar(30) DEFAULT NULL,
  `to_absentperiod` varchar(15) DEFAULT '' COMMENT 'Period when the employee is absent',
  `to_absenthours` decimal(3,1) DEFAULT '0.0' COMMENT 'Number of hours absent',
  `reason` text NOT NULL,
  `destination` text,
  `emp_sign` varchar(100) DEFAULT NULL,
  `nurse` varchar(100) DEFAULT NULL,
  `sectionhead` varchar(100) DEFAULT NULL,
  `supervisor` varchar(100) DEFAULT NULL,
  `dept_mngr` varchar(100) DEFAULT NULL,
  `plant_mngr` varchar(100) DEFAULT NULL,
  `divisionhead` varchar(100) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'new' COMMENT 'new, for approval, approved, disapproved, approved/deleted, disapproved/deleted, canceled, deleted, cancelled, cancelled/deleted',
  `ownerid` int(11) NOT NULL,
  `dateapproved` date DEFAULT NULL,
  `attach1` text,
  `attach2` text,
  `attach3` text,
  `attach4` text,
  `attach5` text,
  `comment` text,
  `datecreated` date NOT NULL,
  `curr_emp` int(11) NOT NULL,
  `approvers` varchar(20) NOT NULL COMMENT 'array list of those who approved the request.',
  `isprocessed` int(11) DEFAULT '0',
  `isdeleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company` (`company`),
  KEY `index_2` (`ownerid`,`status`),
  KEY `index_3` (`curr_emp`,`status`),
  KEY `index_1` (`company`,`status`,`isprocessed`,`isdeleted`,`noti_type`),
  KEY `index_4` (`documentcode`,`codenumber`,`firstname`,`lastname`,`status`,`company`,`noti_type`),
  CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companylists` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.npmdagencies
DROP TABLE IF EXISTS `npmdagencies`;
CREATE TABLE IF NOT EXISTS `npmdagencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manpowerserviceid` int(11) NOT NULL,
  `npmdagency` varchar(500) DEFAULT NULL,
  `npmdagencyinfo` varchar(500) DEFAULT NULL,
  `npmdremarks` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manpowerserviceid` (`manpowerserviceid`),
  CONSTRAINT `npmdagencies_ibfk_1` FOREIGN KEY (`manpowerserviceid`) REFERENCES `manpowerservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.npmdrequirements
DROP TABLE IF EXISTS `npmdrequirements`;
CREATE TABLE IF NOT EXISTS `npmdrequirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(500) DEFAULT NULL,
  `personno` int(11) DEFAULT NULL,
  `workingdays` int(11) DEFAULT NULL,
  `jobtitle` varchar(500) DEFAULT NULL,
  `dateneeded` date DEFAULT NULL,
  `manpowerserviceid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manpowerserviceid` (`manpowerserviceid`),
  CONSTRAINT `npmdrequirements_ibfk_1` FOREIGN KEY (`manpowerserviceid`) REFERENCES `manpowerservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.officesale_attachments
DROP TABLE IF EXISTS `officesale_attachments`;
CREATE TABLE IF NOT EXISTS `officesale_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderentryid` int(11) NOT NULL,
  `fn` varchar(80) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `attachment_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: User; 2: Payment;',
  `ts_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.officesale_payments
DROP TABLE IF EXISTS `officesale_payments`;
CREATE TABLE IF NOT EXISTS `officesale_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderentryid` int(11) NOT NULL,
  `total_amount` decimal(8,2) NOT NULL,
  `payment_type` varchar(12) NOT NULL COMMENT 'Cash, Check, Bank-to-bank',
  `payment_amount` decimal(8,2) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_ref_no` varchar(15) NOT NULL,
  `remarks` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Editable, 1: Closed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.orderdetails
DROP TABLE IF EXISTS `orderdetails`;
CREATE TABLE IF NOT EXISTS `orderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderentryid` int(11) NOT NULL,
  `owner_code` varchar(2) NOT NULL,
  `prod_code` varchar(5) NOT NULL,
  `uom_code` varchar(5) NOT NULL,
  `order_qnty` int(11) NOT NULL,
  `allocated_qnty` int(11) NOT NULL,
  `served_qnty` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `dtl_seq` int(11) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderentryid` (`orderentryid`),
  CONSTRAINT `orderdetails_ibfk_1` FOREIGN KEY (`orderentryid`) REFERENCES `orderentries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.orderentries
DROP TABLE IF EXISTS `orderentries`;
CREATE TABLE IF NOT EXISTS `orderentries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(19) NOT NULL,
  `owner_code` varchar(2) NOT NULL,
  `deliver_to` varchar(150) DEFAULT NULL,
  `delivery_add` varchar(255) DEFAULT NULL,
  `delivery_tel` varchar(30) DEFAULT NULL,
  `delivery_remarks` varchar(255) DEFAULT NULL,
  `cust_code` varchar(7) NOT NULL,
  `cust_name` varchar(150) NOT NULL,
  `company` varchar(10) NOT NULL,
  `trans_date` date NOT NULL,
  `so_code` varchar(5) DEFAULT NULL,
  `order_type` varchar(15) NOT NULL,
  `status` varchar(1) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `total_amount` decimal(12,2) NOT NULL,
  `paid_amount` decimal(12,2) DEFAULT NULL,
  `transmittal` varchar(1) DEFAULT NULL,
  `ordertypeno` int(11) DEFAULT NULL,
  `submit_date` date DEFAULT NULL,
  `ownerid` int(11) NOT NULL,
  `curr_emp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_1` (`status`,`curr_emp`),
  KEY `index_2` (`cust_code`,`status`),
  KEY `company` (`company`),
  CONSTRAINT `orderentries_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companylists` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.overtimelists
DROP TABLE IF EXISTS `overtimelists`;
CREATE TABLE IF NOT EXISTS `overtimelists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `overtimeid` int(11) NOT NULL,
  `ot_date` date NOT NULL,
  `ot_type` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `otstart` time NOT NULL,
  `otend` time NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `noted_by` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `overtimeid` (`overtimeid`),
  CONSTRAINT `overtimelists_ibfk_1` FOREIGN KEY (`overtimeid`) REFERENCES `overtimes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.overtimes
DROP TABLE IF EXISTS `overtimes`;
CREATE TABLE IF NOT EXISTS `overtimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(19) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `departmentid` int(11) NOT NULL,
  `company` varchar(10) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `section` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'new' COMMENT 'new, for approval, approved, return, disapproved, approved/deleted, return/deleted, disapproved/deleted, canceled, deleted, cancelled, cancelled/deleted',
  `datefiled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateapproved` date NOT NULL DEFAULT '0000-00-00',
  `curr_emp` int(11) NOT NULL,
  `approvers` varchar(20) NOT NULL COMMENT 'array list of those who approved the request.',
  `requestedby` int(11) DEFAULT NULL,
  `recomendedby` varchar(50) DEFAULT NULL,
  `isprocessed` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_1` (`company`,`isprocessed`,`status`),
  KEY `index_2` (`employeeid`,`status`),
  KEY `index_3` (`curr_emp`,`status`),
  CONSTRAINT `overtimes_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companylists` (`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.overtime_signatories
DROP TABLE IF EXISTS `overtime_signatories`;
CREATE TABLE IF NOT EXISTS `overtime_signatories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `overtime_id` int(11) NOT NULL,
  `signature_type` tinyint(4) NOT NULL COMMENT '1: recommended; 2: approved;',
  `signature_type_seq` tinyint(4) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `approval_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.plants
DROP TABLE IF EXISTS `plants`;
CREATE TABLE IF NOT EXISTS `plants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plantcode` varchar(4) NOT NULL,
  `plantname` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plantcode` (`plantcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf
DROP TABLE IF EXISTS `pmf`;
CREATE TABLE IF NOT EXISTS `pmf` (
  `pmf_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `year` year(4) NOT NULL,
  `period` varchar(40) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '0: Inactive; Disallow any changes, 1: Active',
  PRIMARY KEY (`pmf_id`),
  KEY `year` (`year`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_core_ratings
DROP TABLE IF EXISTS `pmf_core_ratings`;
CREATE TABLE IF NOT EXISTS `pmf_core_ratings` (
  `rating_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `rating` char(1) NOT NULL,
  `description` varchar(20) NOT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `pmf_id` (`pmf_id`),
  CONSTRAINT `pmf_core_ratings_ibfk_1` FOREIGN KEY (`pmf_id`) REFERENCES `pmf` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core Values Rating Scale';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_core_values
DROP TABLE IF EXISTS `pmf_core_values`;
CREATE TABLE IF NOT EXISTS `pmf_core_values` (
  `core_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_core_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pmf_id` smallint(5) unsigned NOT NULL,
  `value` text NOT NULL COMMENT 'Core Value or Key Action / Behavior',
  PRIMARY KEY (`core_id`),
  KEY `parent_core_id` (`parent_core_id`),
  KEY `pmf_id` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core Values';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_disciplines
DROP TABLE IF EXISTS `pmf_disciplines`;
CREATE TABLE IF NOT EXISTS `pmf_disciplines` (
  `pmf_disciplines_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) NOT NULL,
  `employee_id` smallint(5) NOT NULL,
  `purpose_id` smallint(5) NOT NULL,
  `rating` decimal(5,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`pmf_disciplines_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employees
DROP TABLE IF EXISTS `pmf_employees`;
CREATE TABLE IF NOT EXISTS `pmf_employees` (
  `pmf_employee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) unsigned NOT NULL,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `department_id` smallint(5) unsigned NOT NULL,
  `department_hrms_cd` varchar(20) NOT NULL,
  `position` varchar(64) DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL COMMENT 'employees.employee_id',
  `current_approver_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'employees.employee_id OR 65535',
  `current_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'employees.employee_id',
  `updated_by` int(10) unsigned NOT NULL COMMENT 'employees.employee_id',
  `acknowledged_by` varchar(65) NOT NULL COMMENT 'employee signature upon acknowledgement',
  `acknowledged_date` date NOT NULL,
  `noted_by` varchar(65) NOT NULL COMMENT 'immediate superior signature prior to completion',
  `completed_date` date NOT NULL,
  `reviewed_by` varchar(65) NOT NULL COMMENT 'department head signature upon submission to division head or hr',
  `reviewed_date` date NOT NULL,
  `recommended_by` varchar(65) NOT NULL COMMENT 'division head signature upon submission to hr',
  `recommended_date` date NOT NULL,
  `purpose` text,
  `score` decimal(5,2) NOT NULL DEFAULT '0.00',
  `classification` varchar(2) DEFAULT NULL,
  `approved` char(1) DEFAULT NULL,
  `ts_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ts_updated` timestamp NULL DEFAULT NULL,
  `status_id` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1: Open, 2: Completed, 3: For Approval, 4: Submitted, 5: Ongoing Review, 6: Approved, 7: Returned, 8: For Acknowledgement, 9: Acknowledged, 10: For HROD''s Review',
  PRIMARY KEY (`pmf_employee_id`),
  KEY `employee_id` (`employee_id`),
  KEY `pmf_id` (`pmf_id`),
  KEY `department_id` (`department_id`),
  KEY `department_hrms_cd` (`department_hrms_cd`),
  KEY `current_approver_id` (`current_approver_id`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_comments
DROP TABLE IF EXISTS `pmf_employee_comments`;
CREATE TABLE IF NOT EXISTS `pmf_employee_comments` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `designation` varchar(15) NOT NULL,
  `comment` text,
  `ts_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `pmf_employee_id_2` (`pmf_employee_id`,`designation`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_core_values
DROP TABLE IF EXISTS `pmf_employee_core_values`;
CREATE TABLE IF NOT EXISTS `pmf_employee_core_values` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `core_id` smallint(5) unsigned NOT NULL,
  `critical_incident` text,
  `rating` decimal(5,2) NOT NULL DEFAULT '0.00',
  KEY `pmf_employee_id` (`pmf_employee_id`),
  KEY `core_id` (`core_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_discipline
DROP TABLE IF EXISTS `pmf_employee_discipline`;
CREATE TABLE IF NOT EXISTS `pmf_employee_discipline` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `demerit` decimal(5,2) NOT NULL DEFAULT '0.00',
  `score` decimal(5,2) NOT NULL DEFAULT '0.00',
  KEY `pmf_employee_id` (`pmf_employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_kpis
DROP TABLE IF EXISTS `pmf_employee_kpis`;
CREATE TABLE IF NOT EXISTS `pmf_employee_kpis` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `cluster_id` smallint(5) unsigned NOT NULL,
  `kra_id` int(10) unsigned NOT NULL,
  `kpi_id` int(10) unsigned NOT NULL,
  `general_measures` varchar(10) DEFAULT NULL,
  `specific_measures` varchar(5000) NOT NULL,
  `actual_performances` varchar(5000) DEFAULT NULL,
  `weights` decimal(5,2) NOT NULL DEFAULT '0.00',
  `rating` decimal(5,2) NOT NULL DEFAULT '0.00',
  `weighted` decimal(5,2) NOT NULL DEFAULT '0.00',
  KEY `pmf_employee_id` (`pmf_employee_id`),
  KEY `cluster_id` (`cluster_id`),
  KEY `kra_id` (`kra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_kras
DROP TABLE IF EXISTS `pmf_employee_kras`;
CREATE TABLE IF NOT EXISTS `pmf_employee_kras` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `cluster_id` smallint(5) unsigned NOT NULL,
  `kra_id` int(10) unsigned NOT NULL,
  `kra` varchar(500) NOT NULL,
  KEY `pmf_employee_id` (`pmf_employee_id`),
  KEY `cluster_id` (`cluster_id`),
  KEY `kra_id` (`kra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_policies_and_procedures
DROP TABLE IF EXISTS `pmf_employee_policies_and_procedures`;
CREATE TABLE IF NOT EXISTS `pmf_employee_policies_and_procedures` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `rating` decimal(5,2) NOT NULL DEFAULT '0.00',
  `score` decimal(5,2) NOT NULL DEFAULT '0.00',
  KEY `pmf_employee_id` (`pmf_employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_purposes
DROP TABLE IF EXISTS `pmf_employee_purposes`;
CREATE TABLE IF NOT EXISTS `pmf_employee_purposes` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `purpose_id` smallint(5) unsigned NOT NULL,
  KEY `purpose_id` (`purpose_id`),
  KEY `pmf_employee_purposes_ibfk_1` (`pmf_employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_recommendations
DROP TABLE IF EXISTS `pmf_employee_recommendations`;
CREATE TABLE IF NOT EXISTS `pmf_employee_recommendations` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `recommendation_id` smallint(5) unsigned NOT NULL,
  `recommendation` varchar(100) DEFAULT NULL,
  `approved` char(1) NOT NULL DEFAULT '0',
  KEY `pmf_employee_id` (`pmf_employee_id`),
  KEY `recommendation_id` (`recommendation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_employee_totals
DROP TABLE IF EXISTS `pmf_employee_totals`;
CREATE TABLE IF NOT EXISTS `pmf_employee_totals` (
  `pmf_employee_id` int(10) unsigned NOT NULL,
  `method` varchar(50) NOT NULL,
  `total` decimal(5,2) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  KEY `pmf_employee_id` (`pmf_employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_hierarchies
DROP TABLE IF EXISTS `pmf_hierarchies`;
CREATE TABLE IF NOT EXISTS `pmf_hierarchies` (
  `employee_id` int(10) unsigned NOT NULL,
  `superior_id` int(10) unsigned NOT NULL DEFAULT '0',
  `department_id` smallint(5) unsigned DEFAULT NULL,
  `department_hrms_cd` varchar(15) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT 'Name of Employee',
  `superior` varchar(65) DEFAULT NULL COMMENT 'Name of Superior',
  `designation` varchar(15) DEFAULT 'Employee',
  UNIQUE KEY `employee_id_2` (`employee_id`),
  KEY `superior_id` (`superior_id`),
  KEY `department_id` (`department_id`),
  KEY `department_hrms_cd` (`department_hrms_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Hierarchy overrides for PMF';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_kra_clusters
DROP TABLE IF EXISTS `pmf_kra_clusters`;
CREATE TABLE IF NOT EXISTS `pmf_kra_clusters` (
  `cluster_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `cluster` char(1) NOT NULL,
  `description` text,
  PRIMARY KEY (`cluster_id`),
  KEY `pmf_id` (`pmf_id`),
  KEY `cluster` (`cluster`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='KRA (Key Result Area) Clusters';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_objective_measures
DROP TABLE IF EXISTS `pmf_objective_measures`;
CREATE TABLE IF NOT EXISTS `pmf_objective_measures` (
  `measure_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `measure` varchar(20) NOT NULL,
  `code` varchar(2) NOT NULL,
  `description` text,
  PRIMARY KEY (`measure_id`),
  KEY `pmf_id` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Performance Objective Gen. Measure (KPI for Meets Targets)';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_policies_and_procedures
DROP TABLE IF EXISTS `pmf_policies_and_procedures`;
CREATE TABLE IF NOT EXISTS `pmf_policies_and_procedures` (
  `pmf_policies_and_procedures_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) NOT NULL,
  `department_id` smallint(5) NOT NULL,
  `purpose_id` smallint(5) NOT NULL,
  `rating` decimal(5,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`pmf_policies_and_procedures_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_purposes
DROP TABLE IF EXISTS `pmf_purposes`;
CREATE TABLE IF NOT EXISTS `pmf_purposes` (
  `purpose_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `purpose` varchar(30) NOT NULL,
  PRIMARY KEY (`purpose_id`),
  KEY `pmf_id` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_ratings
DROP TABLE IF EXISTS `pmf_ratings`;
CREATE TABLE IF NOT EXISTS `pmf_ratings` (
  `rating_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `min` decimal(5,2) NOT NULL,
  `max` decimal(5,2) NOT NULL,
  `classification` varchar(2) NOT NULL,
  `description` varchar(50) NOT NULL,
  `remarks` text,
  PRIMARY KEY (`rating_id`),
  KEY `pmf_id` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Performance Objective Rating Scale';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_recommendations
DROP TABLE IF EXISTS `pmf_recommendations`;
CREATE TABLE IF NOT EXISTS `pmf_recommendations` (
  `recommendation_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pmf_id` smallint(5) unsigned NOT NULL,
  `recommendation` varchar(100) NOT NULL,
  `helper_text` varchar(200) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`recommendation_id`),
  KEY `pmf_id` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_users
DROP TABLE IF EXISTS `pmf_users`;
CREATE TABLE IF NOT EXISTS `pmf_users` (
  `pmf_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(4) NOT NULL,
  `name` varchar(150) NOT NULL,
  `lvl` char(1) NOT NULL DEFAULT '1' COMMENT '0: Head / Admin, 1: OD Staff',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '0: Disabled, 1: Enabled',
  PRIMARY KEY (`pmf_user_id`),
  UNIQUE KEY `employee_id` (`employee_id`),
  KEY `lvl` (`lvl`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.pmf_weights
DROP TABLE IF EXISTS `pmf_weights`;
CREATE TABLE IF NOT EXISTS `pmf_weights` (
  `weight_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_pmf_weight_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pmf_id` smallint(5) unsigned NOT NULL,
  `component` varchar(50) NOT NULL,
  `method` varchar(50) DEFAULT NULL COMMENT 'method to call in Pmf_weights controller',
  `weight` decimal(4,2) NOT NULL,
  `comments` text,
  PRIMARY KEY (`weight_id`),
  KEY `parent_pmf_weight_id` (`parent_pmf_weight_id`),
  KEY `pmf_id` (`pmf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weight Distribution';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.positions
DROP TABLE IF EXISTS `positions`;
CREATE TABLE IF NOT EXISTS `positions` (
  `positionid` varchar(15) NOT NULL,
  `position_name` varchar(50) NOT NULL,
  PRIMARY KEY (`positionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.productprices
DROP TABLE IF EXISTS `productprices`;
CREATE TABLE IF NOT EXISTS `productprices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(5) NOT NULL,
  `prod_code` varchar(5) NOT NULL,
  `uom_code` varchar(5) NOT NULL,
  `factory_price` decimal(8,2) NOT NULL,
  `selling_price` decimal(8,2) NOT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(30) DEFAULT NULL,
  `comp_group` varchar(10) NOT NULL DEFAULT 'RBC-CORP',
  PRIMARY KEY (`id`),
  KEY `index_1` (`prod_code`,`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_code` varchar(5) NOT NULL,
  `comp_code` varchar(5) NOT NULL,
  `prod_name` varchar(50) NOT NULL,
  `def_uom_code` varchar(5) NOT NULL,
  `status` varchar(1) NOT NULL,
  `critical_per` decimal(5,2) DEFAULT NULL,
  `ois_uom_code` varchar(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(30) NOT NULL,
  `modified_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(30) DEFAULT NULL,
  `markup_per` decimal(5,2) DEFAULT NULL,
  `markup_price` decimal(5,2) DEFAULT NULL,
  `comp_group` varchar(10) NOT NULL DEFAULT 'RBC-CORP',
  PRIMARY KEY (`id`),
  KEY `comp_group` (`comp_group`),
  KEY `index_1` (`status`,`comp_group`,`comp_code`),
  KEY `index_2` (`comp_group`,`prod_name`,`comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.projectrequirements
DROP TABLE IF EXISTS `projectrequirements`;
CREATE TABLE IF NOT EXISTS `projectrequirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manpowerserviceid` int(11) NOT NULL,
  `position_jobtitle` varchar(150) DEFAULT NULL,
  `rec_dateneeded` date DEFAULT NULL,
  `rec_dateneeded_to` date DEFAULT NULL,
  `rec_pax` int(11) DEFAULT NULL,
  `prep_dateneeded` date DEFAULT NULL,
  `prep_dateneeded_to` date DEFAULT NULL,
  `prep_pax` int(11) DEFAULT NULL,
  `brif_dateneeded` date DEFAULT NULL,
  `brif_dateneeded_to` date DEFAULT NULL,
  `brif_pax` int(11) DEFAULT NULL,
  `fw_dateneeded` date DEFAULT NULL,
  `fw_dateneeded_to` date DEFAULT NULL,
  `fw_pax` int(11) DEFAULT NULL,
  `fw_area` varchar(250) DEFAULT NULL,
  `edit_dateneeded` date DEFAULT NULL,
  `edit_dateneeded_to` date DEFAULT NULL,
  `edit_pax` int(11) DEFAULT NULL,
  `code_dateneeded` date DEFAULT NULL,
  `code_dateneeded_to` date DEFAULT NULL,
  `code_pax` int(11) DEFAULT NULL,
  `enc_dateneeded` date DEFAULT NULL,
  `enc_dateneeded_to` date DEFAULT NULL,
  `enc_pax` int(11) DEFAULT NULL,
  `dc_dateneeded` date DEFAULT NULL,
  `dc_dateneeded_to` date DEFAULT NULL,
  `dc_pax` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manpowerserviceid` (`manpowerserviceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.receivers
DROP TABLE IF EXISTS `receivers`;
CREATE TABLE IF NOT EXISTS `receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `module` varchar(100) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `description` text,
  `value` varchar(2) DEFAULT NULL,
  `company` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employeeid` (`employeeid`),
  KEY `index_1` (`code`,`company`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.referencenumbers
DROP TABLE IF EXISTS `referencenumbers`;
CREATE TABLE IF NOT EXISTS `referencenumbers` (
  `rn_module` varchar(50) DEFAULT NULL,
  `rn_sequence` int(5) unsigned zerofill DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.requisitiondetails
DROP TABLE IF EXISTS `requisitiondetails`;
CREATE TABLE IF NOT EXISTS `requisitiondetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requisitionid` int(11) NOT NULL,
  `quantity` decimal(11,2) NOT NULL,
  `unit` varchar(15) DEFAULT NULL,
  `materialcode` varchar(18) DEFAULT NULL,
  `plantcode` varchar(4) DEFAULT NULL,
  `description` text NOT NULL,
  `application` text,
  `isprocessed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `requisitionid` (`requisitionid`),
  KEY `index_1` (`isprocessed`,`materialcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.requisitions
DROP TABLE IF EXISTS `requisitions`;
CREATE TABLE IF NOT EXISTS `requisitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(4) NOT NULL,
  `deptcode` varchar(2) NOT NULL,
  `rsno` varchar(6) NOT NULL,
  `division` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `estimated_amount` varchar(30) DEFAULT NULL,
  `date` date NOT NULL,
  `deliverydate` date DEFAULT NULL,
  `requestedby` varchar(150) DEFAULT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) NOT NULL,
  `company` varchar(10) NOT NULL,
  `recommendedby` varchar(100) DEFAULT NULL,
  `approvedby` varchar(100) DEFAULT NULL,
  `receivedby` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `ownerid` int(11) NOT NULL,
  `filename` text,
  `attach1` text,
  `attach2` text,
  `attach3` text,
  `attach4` text,
  `attach5` text,
  `attach6` text,
  `attach7` text,
  `attach8` text,
  `attach9` text,
  `attach10` text,
  `comment` text,
  `datecreated` date NOT NULL,
  `curr_emp` int(11) NOT NULL,
  `curr_emp2` int(11) NOT NULL DEFAULT '0',
  `recipient` int(11) NOT NULL DEFAULT '0',
  `dateapproved` date DEFAULT NULL,
  `isprocessed` int(11) DEFAULT '0',
  `isdeleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_1` (`isdeleted`,`curr_emp`,`curr_emp2`),
  KEY `status` (`status`),
  KEY `index_2` (`ownerid`,`status`),
  KEY `index_3` (`curr_emp`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.rsconsolidations
DROP TABLE IF EXISTS `rsconsolidations`;
CREATE TABLE IF NOT EXISTS `rsconsolidations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consolidationrefno` varchar(10) NOT NULL,
  `exporttype` varchar(20) NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `plantcode` varchar(4) DEFAULT NULL,
  `materialtype` varchar(4) DEFAULT NULL,
  `purchasinggroup` varchar(3) DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `consolidationrefno` (`consolidationrefno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='records of consolidated rs to SAP';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.rsexceptions
DROP TABLE IF EXISTS `rsexceptions`;
CREATE TABLE IF NOT EXISTS `rsexceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employeeid` (`employeeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='list of employees that dont have access in RGAS';

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departmentid` int(11) NOT NULL,
  `sect_code` varchar(20) NOT NULL,
  `sect_name` varchar(50) NOT NULL,
  `sect_desc` text,
  `sect_head` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `departmentid` (`departmentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.selectitems
DROP TABLE IF EXISTS `selectitems`;
CREATE TABLE IF NOT EXISTS `selectitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(20) NOT NULL,
  `modulesfield` varchar(30) NOT NULL,
  `item` varchar(50) NOT NULL,
  `text` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_1` (`module`,`modulesfield`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.sequences
DROP TABLE IF EXISTS `sequences`;
CREATE TABLE IF NOT EXISTS `sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(3) NOT NULL COMMENT 'Module Code',
  `company` varchar(4) NOT NULL COMMENT 'Company Code',
  `period` varchar(2) NOT NULL DEFAULT '00' COMMENT 'Year Covered',
  `sequence` varchar(5) NOT NULL DEFAULT '00000' COMMENT 'Sequence Number',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.servicevehicledetails
DROP TABLE IF EXISTS `servicevehicledetails`;
CREATE TABLE IF NOT EXISTS `servicevehicledetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicevehicleid` int(11) NOT NULL,
  `date` date NOT NULL,
  `pickuptime` varchar(10) NOT NULL,
  `pickupplace` varchar(50) NOT NULL COMMENT 'Company, Plant, or Entity',
  `pickupplace_id` int(11) NOT NULL DEFAULT '0',
  `pickupplace_location` varchar(50) NOT NULL,
  `pickupplace_location_id` int(11) NOT NULL DEFAULT '0',
  `destination` varchar(50) NOT NULL COMMENT 'Company, Plant, or Entity',
  `destination_id` int(11) NOT NULL DEFAULT '0',
  `destination_location` varchar(50) NOT NULL,
  `destination_location_id` int(11) NOT NULL DEFAULT '0',
  `noofpassengers` int(11) DEFAULT NULL,
  `vehicletype` varchar(30) DEFAULT NULL,
  `plateno` varchar(10) DEFAULT NULL,
  `vehicle_id` int(10) NOT NULL DEFAULT '0',
  `status_id` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Default,1: Submitted to RGTS',
  PRIMARY KEY (`id`),
  KEY `servicevehicleid` (`servicevehicleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.servicevehicles
DROP TABLE IF EXISTS `servicevehicles`;
CREATE TABLE IF NOT EXISTS `servicevehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `requestfor` varchar(20) DEFAULT NULL,
  `controlno` varchar(10) DEFAULT NULL,
  `requestor` varchar(100) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) NOT NULL,
  `department` varchar(100) NOT NULL,
  `company` varchar(10) NOT NULL,
  `purpose` text,
  `dept_mngr` varchar(100) DEFAULT NULL,
  `sprm` varchar(100) DEFAULT NULL,
  `plant_mngr` varchar(100) DEFAULT NULL,
  `ownerid` int(11) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `submittedto` int(11) DEFAULT NULL COMMENT 'employee.id of SPRM',
  `acceptanceof` int(11) DEFAULT NULL COMMENT 'employee.id of CDU receiver',
  `attach1` text,
  `attach2` text,
  `attach3` text,
  `attach4` text,
  `attach5` text,
  `comment` text,
  `superiors_comment` text,
  `datecreated` date NOT NULL,
  `datereceived` date DEFAULT NULL,
  `curr_emp` int(11) NOT NULL,
  `isdeleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_1` (`status`,`company`,`submittedto`),
  KEY `index_2` (`ownerid`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.servicevehicle_companies
DROP TABLE IF EXISTS `servicevehicle_companies`;
CREATE TABLE IF NOT EXISTS `servicevehicle_companies` (
  `company_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `company` varchar(20) NOT NULL,
  `location_id` tinyint(3) unsigned NOT NULL,
  `status` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`company_id`),
  KEY `status` (`status`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.servicevehicle_locations
DROP TABLE IF EXISTS `servicevehicle_locations`;
CREATE TABLE IF NOT EXISTS `servicevehicle_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(150) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.servicevehicle_vehicles
DROP TABLE IF EXISTS `servicevehicle_vehicles`;
CREATE TABLE IF NOT EXISTS `servicevehicle_vehicles` (
  `vehicle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_type_id` smallint(5) unsigned NOT NULL,
  `plate_no` varchar(6) NOT NULL,
  `km_per_liter` varchar(7) DEFAULT NULL,
  `status_id` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`vehicle_id`),
  UNIQUE KEY `plate_no` (`plate_no`),
  KEY `servicevehicle_vehicles_ibfk_1` (`vehicle_type_id`),
  CONSTRAINT `servicevehicle_vehicles_ibfk_1` FOREIGN KEY (`vehicle_type_id`) REFERENCES `servicevehicle_vehicletypes` (`vehicle_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.servicevehicle_vehicletypes
DROP TABLE IF EXISTS `servicevehicle_vehicletypes`;
CREATE TABLE IF NOT EXISTS `servicevehicle_vehicletypes` (
  `vehicle_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_class_id` char(1) NOT NULL DEFAULT '1' COMMENT '1: Service, 2: Truck',
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`vehicle_type_id`),
  UNIQUE KEY `vehicle_class_id` (`vehicle_class_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.session_token
DROP TABLE IF EXISTS `session_token`;
CREATE TABLE IF NOT EXISTS `session_token` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_token` text NOT NULL,
  `emp_id` int(11) NOT NULL,
  `ts_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.sotypes
DROP TABLE IF EXISTS `sotypes`;
CREATE TABLE IF NOT EXISTS `sotypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `so_code` varchar(5) NOT NULL,
  `so_name` varchar(50) DEFAULT NULL,
  `gp_validity` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `so_limit` decimal(12,2) DEFAULT NULL,
  `group` smallint(6) NOT NULL DEFAULT '1' COMMENT '1: RGFC; 2: Affiliate',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.sourcesites
DROP TABLE IF EXISTS `sourcesites`;
CREATE TABLE IF NOT EXISTS `sourcesites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manpowerrequestid` int(11) DEFAULT NULL,
  `sitename` varchar(150) DEFAULT NULL,
  `male` int(11) DEFAULT '0',
  `female` int(11) DEFAULT '0',
  `total` int(11) DEFAULT '0',
  `totalrequest` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `manpowerrequestid` (`manpowerrequestid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.systemconfigs
DROP TABLE IF EXISTS `systemconfigs`;
CREATE TABLE IF NOT EXISTS `systemconfigs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `syscon_code` varchar(5) NOT NULL,
  `system_type` varchar(5) NOT NULL,
  `syscon_desc` varchar(255) NOT NULL,
  `active_flag` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uomlists` (`system_type`,`active_flag`,`syscon_code`),
  KEY `syscon_code` (`syscon_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.systemowners
DROP TABLE IF EXISTS `systemowners`;
CREATE TABLE IF NOT EXISTS `systemowners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_code` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.trainingendorsements
DROP TABLE IF EXISTS `trainingendorsements`;
CREATE TABLE IF NOT EXISTS `trainingendorsements` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_emp_name` varchar(50) DEFAULT NULL,
  `te_com_id` int(11) DEFAULT NULL,
  `te_dep_id` int(11) DEFAULT NULL,
  `te_emp_designation` varchar(100) DEFAULT NULL,
  `te_ref_num` varchar(50) DEFAULT NULL,
  `te_date_filed` date DEFAULT NULL,
  `te_status` tinyint(4) DEFAULT NULL COMMENT '1=New,2=For Assessment,3=For Approval,4=Approved,5=Disapproved',
  `te_contact_no` varchar(50) DEFAULT NULL,
  `te_training_title` varchar(200) DEFAULT NULL,
  `te_training_date` date DEFAULT NULL,
  `te_total_amount` int(11) DEFAULT NULL,
  `te_vendor` varchar(200) DEFAULT NULL,
  `te_training_venue` varchar(200) DEFAULT NULL,
  `te_echo_training_date` date DEFAULT NULL,
  `te_attachments` varchar(100) DEFAULT NULL COMMENT 'JSON Array',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.trainingendorsementscomments
DROP TABLE IF EXISTS `trainingendorsementscomments`;
CREATE TABLE IF NOT EXISTS `trainingendorsementscomments` (
  `tec_id` int(11) NOT NULL AUTO_INCREMENT,
  `tec_te_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`tec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.trainingendorsementsparticipants
DROP TABLE IF EXISTS `trainingendorsementsparticipants`;
CREATE TABLE IF NOT EXISTS `trainingendorsementsparticipants` (
  `tep_id` int(11) NOT NULL AUTO_INCREMENT,
  `tep_te_id` int(11) NOT NULL,
  `tep_dep_id` int(11) NOT NULL,
  `tep_emp_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`tep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.unitofmeasurements
DROP TABLE IF EXISTS `unitofmeasurements`;
CREATE TABLE IF NOT EXISTS `unitofmeasurements` (
  `id` int(11) NOT NULL,
  `unit` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unit` (`unit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
