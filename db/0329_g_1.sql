-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;



-- Dumping structure for table rgasbl.compensationbenefits
DROP TABLE IF EXISTS `compensationbenefits`;
CREATE TABLE IF NOT EXISTS `compensationbenefits` (
  `cb_id` int(11) NOT NULL AUTO_INCREMENT,
  `cb_employees_id` int(11) NOT NULL DEFAULT '0',
  `cb_emp_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_emp_id` varchar(20) NOT NULL,
  `cb_company` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_department` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_section` varchar(50) CHARACTER SET latin1 NOT NULL,
  `cb_ref_num` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'random chars',
  `cb_date_filed` date NOT NULL COMMENT 'current date',
  `cb_status` varchar(50) NOT NULL,
  `cb_contact_no` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `cb_urgency` varchar(50) NOT NULL,
  `cb_date_needed` date DEFAULT NULL,
  `cb_purpose_of_request` text CHARACTER SET latin1 NOT NULL,
  `cb_attachments` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Filename in JSON Array',
  `cb_comments` text CHARACTER SET latin1,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`cb_id`),
  KEY `cb_emp_id` (`cb_emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rgasbl.compensationbenefitsrequesteddocs
DROP TABLE IF EXISTS `compensationbenefitsrequesteddocs`;
CREATE TABLE IF NOT EXISTS `compensationbenefitsrequesteddocs` (
  `cbrd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cbrd_cb_id` varchar(50) DEFAULT '0',
  `cbrd_req_docs` varchar(100) DEFAULT NULL,
  `cbrd_ref_num` varchar(100) DEFAULT NULL,
  `cbrd_assigned_to` varchar(100) DEFAULT '0',
  `cbrd_assigned_date` date DEFAULT NULL,
  `cbrd_current` varchar(100) DEFAULT NULL COMMENT 'Name of the employee who currently has the access in updating details of the request',
  `cbrd_status` varchar(100) DEFAULT NULL,
  `cbrd_completed_date` date DEFAULT NULL,
  `cbrd_acknowledged_date` date DEFAULT NULL,
  `cbrd_endorsed_to_requestor_date` date DEFAULT NULL,
  PRIMARY KEY (`cbrd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
