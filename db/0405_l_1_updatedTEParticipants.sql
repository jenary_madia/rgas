-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4170
-- Date/time:                    2016-04-05 15:49:48
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rgasbl.trainingendorsementsparticipants
DROP TABLE IF EXISTS `trainingendorsementsparticipants`;
CREATE TABLE IF NOT EXISTS `trainingendorsementsparticipants` (
  `tep_id` int(11) NOT NULL AUTO_INCREMENT,
  `tep_te_id` int(11) NOT NULL,
  `tep_emp_name` varchar(50) NOT NULL,
  `tep_dep_name` varchar(50) NOT NULL,
  `tep_designation` varchar(50) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`tep_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rgasbl.trainingendorsementsparticipants: ~2 rows (approximately)
/*!40000 ALTER TABLE `trainingendorsementsparticipants` DISABLE KEYS */;
INSERT INTO `trainingendorsementsparticipants` (`tep_id`, `tep_te_id`, `tep_emp_name`, `tep_dep_name`, `tep_designation`, `updated_at`, `created_at`) VALUES
	(1, 0, '0', '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 0, '0', '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `trainingendorsementsparticipants` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
