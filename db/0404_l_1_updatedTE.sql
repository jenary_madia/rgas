-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4170
-- Date/time:                    2016-04-04 17:46:14
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping structure for table rgasbl.trainingendorsements
DROP TABLE IF EXISTS `trainingendorsements`;
CREATE TABLE IF NOT EXISTS `trainingendorsements` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_employees_id` int(11) NOT NULL DEFAULT '0',
  `te_emp_name` varchar(50) NOT NULL,
  `te_company` varchar(50) NOT NULL,
  `te_department` varchar(50) NOT NULL,
  `te_emp_designation` varchar(100) NOT NULL,
  `te_ref_num` varchar(50) NOT NULL COMMENT 'random chars',
  `te_date_filed` date NOT NULL COMMENT 'current date',
  `te_status` varchar(50) NOT NULL COMMENT '1=New,2=For Assessment,3=For Approval,4=Approved,5=Disapproved',
  `te_contact_no` varchar(50) DEFAULT NULL,
  `te_training_title` varchar(200) NOT NULL,
  `te_training_date` date NOT NULL,
  `te_total_amount` int(11) NOT NULL,
  `te_vendor` varchar(200) NOT NULL,
  `te_training_venue` varchar(200) NOT NULL,
  `te_echo_training_date` date NOT NULL,
  `te_attachments` varchar(255) DEFAULT NULL COMMENT 'Filename in JSON Array',
  `te_comments` text,
  `te_issent` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rgasbl.trainingendorsements: ~2 rows (approximately)
/*!40000 ALTER TABLE `trainingendorsements` DISABLE KEYS */;
INSERT INTO `trainingendorsements` (`te_id`, `te_employees_id`, `te_emp_name`, `te_company`, `te_department`, `te_emp_designation`, `te_ref_num`, `te_date_filed`, `te_status`, `te_contact_no`, `te_training_title`, `te_training_date`, `te_total_amount`, `te_vendor`, `te_training_venue`, `te_echo_training_date`, `te_attachments`, `te_comments`, `te_issent`, `updated_at`, `created_at`) VALUES
	(1, 2, 'HARRY C. MIJARES', 'RBC-CORP', 'SALES SERVICES DEPARTMENT', 'supervisor', '2016-00043', '2016-04-04', 'New', '0909099232222', 'qw', '0000-00-00', 0, 'qwef', 'qwe', '0000-00-00', '["1db49fe205d030c2b9716bd55f67040d.txt"]', '{"name":"HARRY C. MIJARES","datetime":"04\\/04\\/2016 9:36 AM","message":"df"}', 0, '2016-04-04 09:36:52', '2016-04-04 09:36:52'),
	(2, 5, 'FELICIA S. TAGORDA', 'RBC-CORP', 'INFORMATION TECHNOLOGY', 'employee', '2016-00043', '2016-04-04', 'New', '0909099232222', 'Management Training', '0000-00-00', 10000, 'Bora', 'Cebu', '0000-00-00', '["1d748de5ff84c6c92d6eb1d61206a5ca.txt"]', '{"name":"FELICIA S. TAGORDA","datetime":"04\\/04\\/2016 9:39 AM","message":"dfhjk"}', 1, '2016-04-04 09:39:36', '2016-04-04 09:39:36');
/*!40000 ALTER TABLE `trainingendorsements` ENABLE KEYS */;


-- Dumping structure for table rgasbl.trainingendorsementsparticipants
DROP TABLE IF EXISTS `trainingendorsementsparticipants`;
CREATE TABLE IF NOT EXISTS `trainingendorsementsparticipants` (
  `tep_id` int(11) NOT NULL AUTO_INCREMENT,
  `tep_te_id` int(11) NOT NULL,
  `tep_dep_id` int(11) NOT NULL,
  `tep_emp_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`tep_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rgasbl.trainingendorsementsparticipants: ~2 rows (approximately)
/*!40000 ALTER TABLE `trainingendorsementsparticipants` DISABLE KEYS */;
INSERT INTO `trainingendorsementsparticipants` (`tep_id`, `tep_te_id`, `tep_dep_id`, `tep_emp_id`, `updated_at`, `created_at`) VALUES
	(1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `trainingendorsementsparticipants` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
