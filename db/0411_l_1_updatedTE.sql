-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4170
-- Date/time:                    2016-04-11 15:15:48
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for rgasbl
DROP DATABASE IF EXISTS `rgasbl`;
CREATE DATABASE IF NOT EXISTS `rgasbl` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rgasbl`;


-- Dumping structure for table rgasbl.trainingendorsements
DROP TABLE IF EXISTS `trainingendorsements`;
CREATE TABLE IF NOT EXISTS `trainingendorsements` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_employees_id` int(11) NOT NULL DEFAULT '0',
  `te_emp_name` varchar(50) NOT NULL,
  `te_emp_id` varchar(20) NOT NULL,
  `te_company` varchar(50) NOT NULL,
  `te_department` varchar(50) NOT NULL,
  `te_emp_designation` varchar(100) NOT NULL,
  `te_ref_num` varchar(50) NOT NULL COMMENT 'random chars',
  `te_date_filed` date NOT NULL COMMENT 'current date',
  `te_status` varchar(50) NOT NULL COMMENT '1=New,2=For Assessment,3=For Approval,4=Approved,5=Disapproved',
  `te_contact_no` varchar(50) DEFAULT NULL,
  `te_training_title` varchar(200) NOT NULL,
  `te_training_date` date NOT NULL,
  `te_total_amount` int(11) NOT NULL,
  `te_amount_range` varchar(100) NOT NULL COMMENT '1-5000, 5001-50000, 50001 and above',
  `te_vendor` varchar(200) NOT NULL,
  `te_training_venue` varchar(200) NOT NULL,
  `te_echo_training_date` date NOT NULL,
  `te_attachments` varchar(255) DEFAULT NULL COMMENT 'Filename in JSON Array',
  `te_comments` text,
  `teq_target_performance` text,
  `teq_target_competency` text,
  `teq_reg_emp` varchar(50) DEFAULT NULL,
  `teq_newfunction_description` text,
  `teq_submitted_req` varchar(50) NOT NULL,
  `teq_new_seminar` varchar(50) DEFAULT NULL,
  `teq_idp` varchar(50) DEFAULT NULL,
  `teq_tga` varchar(50) DEFAULT NULL,
  `teq_target_2_higher` varchar(50) DEFAULT NULL,
  `teq_result_1_higher` varchar(50) DEFAULT NULL,
  `teq_required` varchar(50) DEFAULT NULL,
  `teq_overall_recom` varchar(50) DEFAULT NULL,
  `teq_eta` varchar(50) DEFAULT NULL,
  `te_issent` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`te_id`),
  KEY `te_emp_id` (`te_emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rgasbl.trainingendorsements: ~2 rows (approximately)
/*!40000 ALTER TABLE `trainingendorsements` DISABLE KEYS */;
INSERT INTO `trainingendorsements` (`te_id`, `te_employees_id`, `te_emp_name`, `te_emp_id`, `te_company`, `te_department`, `te_emp_designation`, `te_ref_num`, `te_date_filed`, `te_status`, `te_contact_no`, `te_training_title`, `te_training_date`, `te_total_amount`, `te_amount_range`, `te_vendor`, `te_training_venue`, `te_echo_training_date`, `te_attachments`, `te_comments`, `teq_target_performance`, `teq_target_competency`, `teq_reg_emp`, `teq_newfunction_description`, `teq_submitted_req`, `teq_new_seminar`, `teq_idp`, `teq_tga`, `teq_target_2_higher`, `teq_result_1_higher`, `teq_required`, `teq_overall_recom`, `teq_eta`, `te_issent`, `updated_at`, `created_at`) VALUES
	(1, 7, 'FELICIA S. TAGORDA', '05-0317', 'RBC-CORP', 'INFORMATION TECHNOLOGY', 'SR. USER SUPPORT ASSOCIATE', '2016-00047', '2016-04-10', 'New', '', '', '0000-00-00', 0, '', '', '', '0000-00-00', 'null', '{"name":"FELICIA S. TAGORDA","datetime":"04\\/10\\/2016 5:56 PM","message":""}', ' ', ' ', '', ' ', '', '', '', '', '', '', '', '', ' ', 0, '2016-04-10 17:56:14', '2016-04-10 17:56:14'),
	(2, 7, 'FELICIA S. TAGORDA', '05-0317', 'RBC-CORP', 'INFORMATION TECHNOLOGY', 'SR. USER SUPPORT ASSOCIATE', '2016-00047', '2016-04-10', 'For Assessment', '', '', '0000-00-00', 0, '', '', '', '0000-00-00', 'null', '{"name":"FELICIA S. TAGORDA","datetime":"04\\/10\\/2016 5:57 PM","message":""}', ' ', ' ', '', ' ', '', '', '', '', '', '', '', '', ' ', 1, '2016-04-10 17:57:21', '2016-04-10 17:57:21');
/*!40000 ALTER TABLE `trainingendorsements` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
