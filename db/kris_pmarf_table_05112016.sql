-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: rgasbl
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pmarf_requests`
--

DROP TABLE IF EXISTS `pmarf_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pmarf_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(12) NOT NULL,
  `ref_date` date NOT NULL,
  `ref_status` varchar(50) NOT NULL COMMENT 'pmart_libraries.id where lib_type =''ref_status''',
  `req_emp_id` int(11) NOT NULL COMMENT 'requestor = employees.id',
  `req_emp_info` longtext NOT NULL,
  `req_contact_nos` varchar(200) NOT NULL,
  `activity_type` varchar(150) NOT NULL COMMENT 'pmart_libraries.id where lib_type =''activity_type''',
  `activity_details` varchar(250) NOT NULL COMMENT 'pmart_libraries.id where lib_type =''activity_date''',
  `activity_dates` varchar(75) NOT NULL,
  `activity_from` date NOT NULL,
  `activity_to` date NOT NULL,
  `activity_no_of_days` int(11) NOT NULL,
  `activity_area` varchar(200) NOT NULL COMMENT '{array}',
  `imp_type` enum('Internal','External') NOT NULL DEFAULT 'Internal' COMMENT 'implementer',
  `imp_agency_name` varchar(200) NOT NULL COMMENT '{array}',
  `imp_initiate_by` longtext NOT NULL COMMENT '{array}',
  `proposal_objective` longtext NOT NULL,
  `proposal_description` longtext NOT NULL,
  `additional_support_activities` longtext NOT NULL,
  `participating_brands` longtext NOT NULL,
  `attachments` longtext NOT NULL,
  `comments` longtext NOT NULL,
  `request_stage` enum('R','IS','DH') NOT NULL DEFAULT 'R',
  `request_assigned_to` int(11) NOT NULL COMMENT 'emp.id',
  `supervisor_info` longtext NOT NULL,
  `departmenthead_info` longtext NOT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_date` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `delete_date` date DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `req_emp_id` (`req_emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pmarf_requests`
--

LOCK TABLES `pmarf_requests` WRITE;
/*!40000 ALTER TABLE `pmarf_requests` DISABLE KEYS */;
INSERT INTO `pmarf_requests` VALUES (1,'2016-00012','2016-04-21','For Approval',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"dsfsdfs\",\"emp_section\":\"\"}','dsfsdfs','In Store Display','','One Day','2016-04-22','2016-04-23',2,'sdf','Internal','','{\"chk_Distributor\":\"on\",\"chk_Brand_Management\":\"on\",\"chk_Ad_and_Media\":\"on\",\"chk_Trade\":\"on\",\"chk_others_desc\":\"\"}','sdfs',' sdf',' sdf','{\"qty\":\"{\\\"qty_1\\\":\\\"3\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"3\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"dsfsfsdfsdf\\\"}\"}','[{\"random_filename\":\"5ddf42f079da55107e43cf792d1bcd69\",\"original_filename\":\"wpspin_light.gif\"}]','dsfsdfsdfsdf','IS',0,'','','2016-04-21',1,'2016-04-25',1,NULL,NULL),(2,'2016-00013','2016-04-25','For Approval',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"\",\"emp_section\":\"\"}','xxxdsdadasd','In Store Promo','','One Day','2016-04-25','2016-04-26',2,'dsf','External','sfs','{\"chk_Distributor\":\"on\",\"chk_Brand_Management\":\"on\",\"chk_Ad_and_Media\":\"on\",\"chk_Trade\":\"on\",\"chk_others_desc\":\"\"}','        fsdfcxcxzcxz','         sdf        ','         fsdf        ','{\"qty\":\"{\\\"qty_1\\\":\\\"2\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"3\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"sadsa\\\"}\"}','[{\"random_filename\":\"aa4cc0d7e02a68a4b078b76cd74d6e59\",\"original_filename\":\"desktop.ini\"}]','','IS',19,'{\"id\":19,\"employeeid\":\"04-0404\",\"new_employeeid\":\"B04-0012-A\",\"firstname\":\"KIMBERLY\",\"middlename\":\"C\",\"lastname\":\"SANTOS\",\"designation\":\"INFORMATION SYSTEMS MANAGER II\",\"superiorid\":32,\"departmentid\":9,\"departmentid2\":\"CSMD\",\"company\":\"RBC-CORP\",\"desig_level\":\"supervisor\",\"onleave\":0,\"attendanceid\":0,\"artapprovalid\":0,\"msrapprovalid\":0,\"presassignedrsmrf1\":0,\"active\":1,\"dept_name\":\"SYSTEMS MANAGEMENT AND DEVT\",\"sect_name\":\"APPLICATION DEVELOPMENT\"}','','2016-04-25',1,'2016-04-29',19,NULL,NULL),(3,'2016-00014','2016-04-25','New',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"\",\"emp_section\":\"\"}','','In Store Display','','One Day','0000-00-00','0000-00-00',0,'','Internal','','{\"chk_others_desc\":\"\"}','     vbndfgdg','     vnvbn    ','         ','{\"qty\":\"{\\\"qty_1\\\":\\\"2\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"2\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"cvbcvb\\\"}\"}','[{\"random_filename\":\"0051b4bb006bca4769d6673055ace9e3\",\"original_filename\":\"wpspin_light.gif\"}]','','R',0,'{\"id\":19,\"firstname\":\"KIMBERLY\",\"middlename\":\"C\",\"lastname\":\"SANTOS\",\"designation\":\"INFORMATION SYSTEMS MANAGER II\"}','','2016-04-25',1,'2016-04-26',1,NULL,NULL),(4,'2016-00015','2016-04-29','New',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"\",\"emp_section\":\"\"}','','In Store Display','','One Day','0000-00-00','0000-00-00',0,'','Internal','','{\"chk_others_desc\":\"\"}',' ',' ',' ','{\"qty\":\"{\\\"qty_1\\\":\\\"\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"\\\"}\"}','null','','R',0,'{\"id\":19,\"firstname\":\"KIMBERLY\",\"middlename\":\"C\",\"lastname\":\"SANTOS\",\"designation\":\"INFORMATION SYSTEMS MANAGER II\"}','','2016-04-29',1,'0000-00-00',0,NULL,NULL),(5,'2016-00016','2016-04-29','For Approval',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"\",\"emp_section\":\"\"}','','In Store Display','','One Day','0000-00-00','0000-00-00',0,'','Internal','','{\"chk_others_desc\":\"\"}',' ',' ',' ','{\"qty\":\"{\\\"qty_1\\\":\\\"\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"\\\"}\"}','null','','DH',4,'{\"id\":19,\"firstname\":\"KIMBERLY\",\"middlename\":\"C\",\"lastname\":\"SANTOS\",\"designation\":\"INFORMATION SYSTEMS MANAGER II\"}','{\"id\":4,\"firstname\":\"SONNY\",\"middlename\":null,\"lastname\":\"ONG\",\"designation\":null}','2016-04-29',1,'2016-04-29',19,NULL,NULL),(6,'2016-00016','2016-04-29','For Approval',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"\",\"emp_section\":\"\"}','','In Store Display','','One Day','0000-00-00','0000-00-00',0,'','Internal','','{\"chk_others_desc\":\"\"}',' ',' ',' ','{\"qty\":\"{\\\"qty_1\\\":\\\"\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"\\\"}\"}','null','','DH',4,'{\"id\":19,\"firstname\":\"KIMBERLY\",\"middlename\":\"C\",\"lastname\":\"SANTOS\",\"designation\":\"INFORMATION SYSTEMS MANAGER II\"}','{\"id\":4,\"firstname\":\"SONNY\",\"middlename\":null,\"lastname\":\"ONG\",\"designation\":null}','2016-04-29',1,'2016-04-29',19,NULL,NULL),(7,'2016-00016','2016-04-29','New',1,'{\"emp_name\":\"JOHNNY C. NG\",\"emp_id\":\"1\",\"emp_company\":\"RBC-CORP\",\"emp_department\":\"OFFICE OF THE EXECUTIVES\",\"emp_contactno\":\"\",\"emp_section\":\"\"}','','In Store Display','','One Day','0000-00-00','0000-00-00',0,'','Internal','','{\"chk_others_desc\":\"\"}',' ',' ',' ','{\"qty\":\"{\\\"qty_1\\\":\\\"\\\"}\",\"unit\":\"{\\\"unit_1\\\":\\\"\\\"}\",\"product\":\"{\\\"product_1\\\":\\\"\\\"}\"}','null','','R',0,'{\"id\":19,\"firstname\":\"KIMBERLY\",\"middlename\":\"C\",\"lastname\":\"SANTOS\",\"designation\":\"INFORMATION SYSTEMS MANAGER II\"}','{\"id\":4,\"firstname\":\"SONNY\",\"middlename\":null,\"lastname\":\"ONG\",\"designation\":null}','2016-04-29',1,'0000-00-00',0,NULL,NULL);
/*!40000 ALTER TABLE `pmarf_requests` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-11  5:59:13
