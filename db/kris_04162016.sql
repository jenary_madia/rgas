INSERT INTO `rgasbl`.`referencenumbers` (`rn_module`, `rn_sequence`) VALUES ('pmarf', '1');

CREATE TABLE IF NOT EXISTS `pmarf_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(12) NOT NULL,
  `ref_date` date NOT NULL,
  `ref_status` varchar(50) NOT NULL COMMENT 'pmart_libraries.id where lib_type =''ref_status''',
  `req_emp_id` int(11) NOT NULL COMMENT 'requestor = employees.id',
  `req_emp_info` longtext NOT NULL,
  `req_contact_nos` varchar(200) NOT NULL,
  `activity_type` varchar(150) NOT NULL COMMENT 'pmart_libraries.id where lib_type =''activity_type''',
  `activity_details` varchar(250) NOT NULL COMMENT 'pmart_libraries.id where lib_type =''activity_date''',
  `activity_dates` varchar(75) NOT NULL,
  `activity_from` date NOT NULL,
  `activity_to` date NOT NULL,
  `activity_no_of_days` int(11) NOT NULL,
  `activity_area` varchar(200) NOT NULL COMMENT '{array}',
  `imp_type` enum('Internal','External') NOT NULL DEFAULT 'Internal' COMMENT 'implementer',
  `imp_agency_name` varchar(200) NOT NULL COMMENT '{array}',
  `imp_initiate_by` longtext NOT NULL COMMENT '{array}',
  `proposal_objective` longtext NOT NULL,
  `proposal_description` longtext NOT NULL,
  `additional_support_activities` longtext NOT NULL,
  `participating_brands` longtext NOT NULL,
  `attachments` longtext NOT NULL,
  `comments` longtext NOT NULL,
  `activity_status` int(11) NOT NULL COMMENT 'activity_status.id',
  `activity_assigned_person` int(11) NOT NULL COMMENT 'activity_status.assigned_to',
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_date` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `req_emp_id` (`req_emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Truncate table before insert `pmarf_requests`
--

TRUNCATE TABLE `pmarf_requests`;
--
-- Dumping data for table `pmarf_requests`
--

INSERT INTO `pmarf_requests` (`id`, `ref_no`, `ref_date`, `ref_status`, `req_emp_id`, `req_emp_info`, `req_contact_nos`, `activity_type`, `activity_details`, `activity_dates`, `activity_from`, `activity_to`, `activity_no_of_days`, `activity_area`, `imp_type`, `imp_agency_name`, `imp_initiate_by`, `proposal_objective`, `proposal_description`, `additional_support_activities`, `participating_brands`, `attachments`, `comments`, `activity_status`, `activity_assigned_person`, `created_date`, `created_by`, `modified_date`, `modified_by`) VALUES
(1, '2016-00001', '2016-04-14', 'New', 93, '{"emp_name":"JOHNNY C. NG","emp_id":"93-109","emp_company":"RBC-CORP","emp_department":"OFFICE OF THE EXECUTIVES","emp_contactno":"fsdfsdf","emp_section":""}', 'fsdfsdf', 'In Store Display', '', 'One Day', '2016-04-14', '2016-04-15', 2, 'fdsf', 'Internal', 'fdsf', '{"chk_Distributor":"on","chk_Brand_Management":"on","chk_Ad_and_Media":"on","chk_Trade":"on","chk_others_desc":""}', ' fsd', ' fsdf', ' sdfsdf', '{"qty":"{\\"qty_1\\":\\"2\\"}","unit":"{\\"unit_1\\":\\"3\\"}","product":"{\\"product_1\\":\\"dfdssdfsdf\\"}"}', '[{"random_filename":"0f8b8d0a31c6241fb1d7637a599a70c6","original_filename":"trigger.sql"}]', 'sdfsdfdsf', 0, 0, '2016-04-14', 93, '0000-00-00', 0),
(2, '2016-00001', '2016-04-14', 'New', 93, '{"emp_name":"JOHNNY C. NG","emp_id":"93-109","emp_company":"RBC-CORP","emp_department":"OFFICE OF THE EXECUTIVES","emp_contactno":"fsdfsdf","emp_section":""}', 'fsdfsdf', 'In Store Display', '', 'One Day', '2016-04-14', '2016-04-15', 2, 'fdsf', 'Internal', 'fdsf', '{"chk_Distributor":"on","chk_Brand_Management":"on","chk_Ad_and_Media":"on","chk_Trade":"on","chk_others_desc":""}', ' fsd', ' fsdf', ' sdfsdf', '{"qty":"{\\"qty_1\\":\\"2\\"}","unit":"{\\"unit_1\\":\\"3\\"}","product":"{\\"product_1\\":\\"dfdssdfsdf\\"}"}', '[{"random_filename":"d1a35c1f3a7436bc1f4f2fd382364bf9","original_filename":"trigger.sql"}]', 'sdfsdfdsf', 0, 0, '2016-04-14', 93, '0000-00-00', 0),
(3, '2016-00003', '2016-04-15', 'New', 93, '{"emp_name":"JOHNNY C. NG","emp_id":"93-109","emp_company":"RBC-CORP","emp_department":"OFFICE OF THE EXECUTIVES","emp_contactno":"","emp_section":""}', '', 'In Store Display', '', 'One Day', '0000-00-00', '0000-00-00', 0, '', 'Internal', '', '{"chk_Strategic_Corporate_Development":"on","chk_Others":"on","chk_others_desc":"sdasdasd"}', ' asdas', ' asd', ' ', '{"qty":"{\\"qty_1\\":\\"\\"}","unit":"{\\"unit_1\\":\\"\\"}","product":"{\\"product_1\\":\\"\\"}"}', 'null', '', 0, 0, '2016-04-15', 93, '0000-00-00', 0),
(4, '2016-00004', '2016-04-15', 'New', 93, '{"emp_name":"JOHNNY C. NG","emp_id":"93-109","emp_company":"RBC-CORP","emp_department":"OFFICE OF THE EXECUTIVES","emp_contactno":"","emp_section":""}', '', 'In Store Display', '', 'One Day', '0000-00-00', '0000-00-00', 0, '', 'Internal', '', '{"chk_others_desc":""}', ' ', ' ', ' ', '{"qty":"{\\"qty_1\\":\\"1\\",\\"qty_2\\":\\"2\\",\\"qty_3\\":\\"3\\",\\"qty_4\\":\\"4\\"}","unit":"{\\"unit_1\\":\\"1\\",\\"unit_2\\":\\"2\\",\\"unit_3\\":\\"3\\",\\"unit_4\\":\\"5\\"}","product":"{\\"product_1\\":\\"1\\",\\"product_2\\":\\"2\\",\\"product_3\\":\\"3\\",\\"product_4\\":\\"5\\"}"}', 'null', '', 0, 0, '2016-04-15', 93, '0000-00-00', 0);

