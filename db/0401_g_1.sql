CREATE TABLE `compensationbenefits` (
	`cb_id` INT(11) NOT NULL AUTO_INCREMENT,
	`cb_employees_id` INT(11) NOT NULL DEFAULT '0',
	`cb_emp_name` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`cb_emp_id` VARCHAR(20) NOT NULL,
	`cb_company` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`cb_department` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`cb_section` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`cb_ref_num` VARCHAR(50) NOT NULL COMMENT 'random chars' COLLATE 'latin1_swedish_ci',
	`cb_date_filed` DATE NOT NULL COMMENT 'current date',
	`cb_status` VARCHAR(50) NOT NULL,
	`cb_contact_no` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`cb_urgency` VARCHAR(50) NOT NULL,
	`cb_date_needed` DATE NULL DEFAULT NULL,
	`cb_purpose_of_request` TEXT NOT NULL COLLATE 'latin1_swedish_ci',
	`cb_attachments` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Filename in JSON Array' COLLATE 'latin1_swedish_ci',
	`cb_comments` TEXT NULL COLLATE 'latin1_swedish_ci',
	`cd_issent` TINYINT(4) NULL DEFAULT '0',
	`updated_at` DATETIME NOT NULL,
	`created_at` DATETIME NOT NULL,
	PRIMARY KEY (`cb_id`),
	INDEX `cb_emp_id` (`cb_emp_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

CREATE TABLE `compensationbenefitsrequesteddocs` (
	`cbrd_id` INT(11) NOT NULL AUTO_INCREMENT,
	`cbrd_cb_id` VARCHAR(50) NULL DEFAULT '0',
	`cbrd_req_docs` VARCHAR(100) NULL DEFAULT NULL,
	`cbrd_ref_num` VARCHAR(100) NULL DEFAULT NULL,
	`cbrd_assigned_to` VARCHAR(100) NULL DEFAULT NULL,
	`cbrd_assigned_date` DATE NULL DEFAULT NULL,
	`cbrd_current` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Name of the employee who currently has the access in updating details of the request',
	`cbrd_status` VARCHAR(100) NULL DEFAULT NULL,
	`cbrd_remarks` VARCHAR(100) NULL DEFAULT NULL,
	`cbrd_completed_date` DATE NULL DEFAULT NULL,
	`cbrd_acknowledged_date` DATE NULL DEFAULT NULL,
	`cbrd_endorsed_to_requestor_date` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`cbrd_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;
